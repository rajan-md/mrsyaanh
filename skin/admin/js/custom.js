var _URL = window.URL || window.webkitURL;
jQuery("#featured_img").change(function (e)
{
  var file, img;
  if ((file = this.files[0]))
  {
    img = new Image();
    img.onload = function () { };
    img.src = _URL.createObjectURL(file); 
  }
});

function checkPhoto(target,id='blah')
{
  if(target.files[0].type.indexOf("image") == -1)
  {
    jQuery('#'+id).attr('src', '#'); 
    return false;
  }
  if (target.files && target.files[0])
  {
    var reader = new FileReader();
    reader.onload = function (e)
    {
      jQuery('#'+id).attr('src', e.target.result);
      jQuery('#'+id+'_container').css('display','block'); 
    }
    reader.readAsDataURL(target.files[0]);
  }
}

$(document).on('click','#setpassword', function() {
	if (jQuery(this).is(':checked')) {
    	$('#resetpassword').show();
	}
	else
	{
	     $('#resetpassword').hide();
	}	
});

$(document).on("click", ".open-financial-popup", function () {
    var serviceProviderId = $(this).data('id');
    $("#statement_form #service_provider_id").val(serviceProviderId);
	$.ajax({
	    type: 'post',
	    url: $('#baseurl').val()+'admin/ajax-serviceprovider-info',
	    data: {'serviceProviderId':serviceProviderId},
	    success: function(response_data)   {
	        if($("#service_provider_details").length==1)
			{
				$("#statement_table tbody #service_provider_details").html(response_data);
			}
			else
			{
				$("#statement_table tbody").prepend('<tr id="service_provider_details">'+response_data+'</tr>');
			}
		}
	});
	$('#statement_output').html('');
});

$(document).on("change", "#statement_duration", function () {
   var duration = $(this).val();
   if(duration=='other')
   {
      $("#date_selector").show();
   }
   else
   {
      $("#date_selector").hide();
   }
});


$(document).on("change", "#currency", function ()
{
     var currency = $(this).val();
     $.ajax({
        type: 'post',
        url: $('#baseurl').val()+'admin/services/ajax_currency_symbol',
        data: {'currency_id':currency},
        success: function(response_data)
        {
          $(".curr_symbol").html(response_data);
        }
    });
});

$(document).on("change", "#service_duration_after", function ()
{
     var hours = $(this).val();
     $("#service_additional_charge_label").html('Charges  after '+hours+' hours');
});

$(document).on("change", "#weekend_duration_after", function ()
{
     var hours = $(this).val();
     $("#weekend_additional_charge_label").html('Charges  after '+hours+' hours');
});

jQuery("input[name=business]").click(function(){
    var business=jQuery(this).val();
    if(business=='yes')
    {
      jQuery("#businessDetails").css('display','block');
      jQuery("#business_name").attr("required","required");
      jQuery("#business_type").attr("required","required");
    }
    else
    {
      jQuery("#businessDetails").css('display','none');
      jQuery("#business_name").removeAttr("required");
      jQuery("#business_type").removeAttr("required");
    }
}); 

jQuery("input[name=business]").click(function(){
    var business=jQuery(this).val();
    if(business=='yes')
    {
      jQuery("#businessDetails").css('display','block');
      jQuery("#business_name").attr("required","required");
      jQuery("#business_type").attr("required","required");
      jQuery("#business_country").attr("required","required");
      jQuery("#business_state").attr("required","required");
      jQuery("#business_street_no").attr("required","required");
      jQuery("#business_street_name").attr("required","required");
      jQuery("#business_street_type").attr("required","required");   
      jQuery("#business_city").attr("required","required");
      jQuery("#service_zip_code").attr("required","required");
      jQuery("#business_mobile").attr("required","required");
      jQuery("#business_email").attr("required","required");
    }
    else
    {
      jQuery("#businessDetails").css('display','none');
      jQuery("#business_name").removeAttr("required");
      jQuery("#business_type").removeAttr("required");
      jQuery("#business_country").removeAttr("required");
      jQuery("#business_state").removeAttr("required");
      jQuery("#business_street_no").removeAttr("required");
      jQuery("#business_street_name").removeAttr("required");
      jQuery("#business_street_type").removeAttr("required");
      jQuery("#business_city").removeAttr("required");
      jQuery("#service_zip_code").removeAttr("required");
      jQuery("#business_mobile").removeAttr("required");
      jQuery("#business_email").removeAttr("required");

    }
}); 

jQuery("input[name=license]").click(function(){
    var license=jQuery(this).val();
    if(license=='yes')
    {
      jQuery("#licenseDetails").css('display','block');
      jQuery("#license_details").attr("required","required");
    } else {
       jQuery("#licenseDetails").css('display','none');
       jQuery("#license_details").removeAttr("required");
    }
}); 

jQuery("input[name=business-info]").click(function(){
    var license=$(this).is(':checked');
    if(license)
    {
      jQuery("#business-logic").css('display','block');
    } else {
       jQuery("#business-logic").css('display','none');
    }
}); 

$(document).on("submit", "#statement_form", function (e) {
  	//e.preventDefault();  	
  	var statement_duration  = $('#statement_duration').val();
  	var from_date  = $('#from_date').val();
  	var to_date  = $('#to_date').val();
 	if(statement_duration==0 || statement_duration=="")
 	{
 		$('#popup_message_box').html('<div class="alert alert-danger">Please choose duration !!</div>');
    return false;
 	}
 	else if(statement_duration=="other" && from_date=="")
 	{
 		$('#popup_message_box').html('<div class="alert alert-danger">Please choose FROM date !!</div>');
    return false;
 	}
 	else if(statement_duration=="other" && to_date=="")
 	{
 		$('#popup_message_box').html('<div class="alert alert-danger">Please choose TO date !!</div>');
    return false;
 	}
 	else
 	{
    return true;
 		/*$('#popup_message_box').html('');
 		$.ajax({
		    type: 'post',
		    url: $('#baseurl').val()+'admin/ajax-find-statements',
		    data: $('#statement_form').serialize(),
		    success: function(response_data){
		    	$('#statement_output').html(response_data);
		    }
		});*/
 	}
   	
});

$(document).on('change','#country',function (e) {
    e.preventDefault();
    var country_id = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-states',
      data: { 'country_id': country_id },
      success: function (data) {
        $('#administrative_area_level_1').html(data); 
      }
    });
    return false;
});

$(document).on('change','#business_country',function (e) {
    e.preventDefault();
    var country_id = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-states',
      data: { 'country_id': country_id },
      success: function (data) {
        $('#business_state').html(data); 
      }
    });
    return false;
});

if( $('#phonecode').length > 0 )
{
    var phoneNumq = $("#phonecode");
    var input = document.querySelector("#phonecode");
    var iti = window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      //geoIpLookup: function(callback) {
      //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       // var countryCode = (resp && resp.country) ? resp.country : "";
       // callback(countryCode);
      //});
      //},
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      nationalMode: true,
      onlyCountries: ['us','in','ca'],
      //placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/admin/js/utils.js",
    });

    $(function() {
        var txt=$('.personal_phonebox .selected-dial-code').text();
        var ph=phoneNumq.val();
        $("input[name=mobbox]").val(txt+ph);
    });

    $('.personal_phonebox #country-listbox li').on("click", function () {
        var txt=$(this).find('.dial-code').text();
        var ph=phoneNumq.val();
        $("input[name=mobbox]").val(txt+ph);
    });

    phoneNumq.on("keyup", function () {
        var intlNumber = $("#phonecode").val();
        var txt=$('.personal_phonebox .selected-dial-code').text();
        $("input[name=mobbox]").val(txt+intlNumber);
    });
}

if( $('#bussphonecode').length > 0 )
{
    /* 2nd phone field */
    var bussphoneNumq = $("#bussphonecode");
    var bussinput = document.querySelector("#bussphonecode");
    var bussiti = window.intlTelInput(bussinput, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      //geoIpLookup: function(callback) {
      //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       // var countryCode = (resp && resp.country) ? resp.country : "";
       // callback(countryCode);
      //});
      //},
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      nationalMode: true,
      onlyCountries: ['us','in','ca'],
      //placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/admin/js/utils.js",
    });

    $(function() {
        var busstxt=$('.bussiness_phonebox .selected-dial-code').text();
        var bussph=bussphoneNumq.val();
        $("input[name=bussmobbox]").val(busstxt+bussph);
    });

    $('.bussiness_phonebox #country-listbox li').on("click", function () {
        var busstxt=$(this).find('.dial-code').text();
        var bussph=bussphoneNumq.val();
        $("input[name=bussmobbox]").val(busstxt+bussph);
    });

    bussphoneNumq.on("keyup", function () {
        var bussintlNumber = $("#bussphonecode").val();
        var busstxt=$('.bussiness_phonebox .selected-dial-code').text();
        $("input[name=bussmobbox]").val(busstxt+bussintlNumber);
    });
}

$(document).on('change','#role',function (e) {
    e.preventDefault();
    var val = $(this).val();
    alert(val);
    if(val==5)
    {
      $('#companybox').show();
      $('#namelabel').html('Contact name');
    }
    else
    {
      $('#companybox').hide();
      $('#namelabel').html('Full name');
    }
});