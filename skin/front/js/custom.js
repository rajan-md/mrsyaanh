$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
  loop: true,
  margin: 10,
  responsiveClass: true,
  nav:true,
  dots:false,
     navText : ["<span><i class='fas fa-long-arrow-alt-left'></i> </span>","<span> <i class='fas fa-long-arrow-alt-right'></i></span>"],
  responsive: {
    0: {
    items: 1,
    nav: true
    },
    768: {
    items: 3,
    nav: false
    },
    1000: {
    items: 4,
    nav: false,
    loop: true,
    margin:0
    }
  }
  });
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

$(document).ready(function(){
    $('#services_type').on('change',function(){
      var service = $(this).val();
      $('#service_category_code').val(service);
    }); 
});


$(document).on('submit','#zipcodeSearch',function (e) {  
    $('#popupservice').modal({backdrop: 'static', keyboard: false});  
    $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-find-services',
        data: $('#zipcodeSearch').serialize(),
        beforeSend: function() {   
           $('#popupservice').modal(); 
           $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
        },
        success: function(data)   {
            
            //$('#popupservice').modal({backdrop: 'static', keyboard: false});
            $('#zipcodeMsg').html(data);   
        },
        error: function() {
          $('#popupservice').modal();
          $('#zipcodeMsg').html('<div><i class="fa fa-exclamation-triangle" style="color:red"></i> Your request status is filed</div>');
        }
    });
    return false; 
});


$(document).on('click','#verifyotp',function (e) { 
  e.preventDefault();
  var otp = $('#otp').val();
  var ordid = $(this).attr('data-attr-id');
  if(otp=='')
  {
    if($('#msgbox').length)
    {
      $('#msgbox').html('<div class="alert alert-danger">OTP required !!</div>');
    }
    else
    {
      $('#otpwrapper').append('<div id="msgbox"><div class="alert alert-danger">OTP required !!</span></div>');
    }    
  }
  else
  {
    if($('#msgbox').length)
    {
      $('#msgbox').remove();
    }
    $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-otp-verify',
        data: {otp:otp,ordid:ordid},
        beforeSend: function()
        {   
           $('#verifyotp').val('Checking...'); 
           $('#verifyotp').attr('disabled','disabled');
        },
        success: function(response)
        {
          $('#verifyotp').val('Verify OTP');
          $('#verifyotp').removeAttr('disabled');
          if(response.trim()=='invalid')
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-danger">Invalid OTP !!</div>');
              }
              else
              {
                $('#otpwrapper').append('<div id="msgbox"><div class="alert alert-danger">Invalid OTP !!</span></div>');
              }
          }
          else
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-success">Verified successfully.</div>');
              }
              else
              {
                $('#otpwrapper').append('<div id="msgbox"><div class="alert alert-success">Verified successfully.</span></div>');
                location.reload().delay(7000);
              }

              
          }
        }
    });
  }
    return false; 
  });


$(document).on('click','#start_job',function (e) { 
  e.preventDefault();
  var ordid = $(this).attr('data-attr-id');
    if($('#msgbox').length)
    {
      $('#msgbox').remove();
    }
    $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-start-service',
        data: {ordid:ordid},
        beforeSend: function()
        {   
           $('#start_job').val('Starting...'); 
           $('#start_job').attr('disabled','disabled');
        },
        success: function(response)
        {
          $('#start_job').removeAttr('disabled');
          if(response.trim()=='success')
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-success">Started successfully.</div>');
              }
              else
              {
                $('#startwrapper').append('<div id="msgbox"><div class="alert alert-success">Started successfully.</span></div>');
              }

              location.reload().delay(7000);
          }
          else
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-danger">Something going wrong.</div>');
              }
              else
              {
                $('#startwrapper').append('<div id="msgbox"><div class="alert alert-danger">Something going wrong.</span></div>');
              }
          }
        }
    });
    return false; 
});


$(document).on('click','#end_job',function (e) { 
  e.preventDefault();
  var ordid = $(this).attr('data-attr-id');
    if($('#msgbox').length)
    {
      $('#msgbox').remove();
    }
    $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-end-service',
        data: {ordid:ordid},
        beforeSend: function()
        {   
           $('#end_job').val('Ending...'); 
           $('#end_job').attr('disabled','disabled');
        },
        success: function(response)
        {
          $('#end_job').removeAttr('disabled');
          if(response.trim()=='success')
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-success">Stopped successfully.</div>');
              }
              else
              {
                $('#endwrapper').append('<div id="msgbox"><div class="alert alert-success">Stopped successfully.</span></div>');
              }

              location.reload().delay(7000);
          }
          else
          {
              if($('#msgbox').length)
              {
                $('#msgbox').html('<div class="alert alert-danger">Something going wrong.</div>');
              }
              else
              {
                $('#endwrapper').append('<div id="msgbox"><div class="alert alert-danger">Something going wrong.</span></div>');
              }
          }
        }
    });
    return false; 
});

$(document).on('click','#backtoServices',function (e) {  
    $('#popupservice').modal({backdrop: 'static', keyboard: false});  
    $.ajax({
        type: 'post',
        url:  $('#baseurl').attr("href")+'ajax-find-services',
        data: $('#zipcodeSearch').serialize(),
        beforeSend: function() {   
           $('#popupservice').modal(); 
           $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
        },
        success: function(data) {            
            $('#zipcodeMsg').html(data);   
        },
        error: function() {
          $('#popupservice').modal();
          $('#zipcodeMsg').html('<div><i class="fa fa-exclamation-triangle" style="color:red"></i> Your request status is filed</div>');
        }
    });
    return false; 
  });

$(document).on('click','#submitQuery, #sendMyQuery',function (e) {  
    $('#popupservice').modal({backdrop: 'static', keyboard: false});  
    $.ajax({
        type: 'post',
        url:  $('#baseurl').attr("href")+'send_services_not_found_message',
        data: $('#sendMyQuery').serialize(),
        beforeSend: function() {   
          $('#popupservice').modal(); 
          var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
          var inputname=$('#inputNameQuery').val();
          var inputemail=$('#inputEmailQuery').val();
          if((inputname=="") || (inputemail=="")) {
            $('#errorQueryMsg').html('<div class="QueryErr"><strong>Error ! </strong> please enter the required field</div><div style="height:20px"></div>');
            return false ;
          } else if(!inputemail.match(mailformat)) {
            $('#errorQueryMsg').html('<div class="QueryErr"><strong>Error ! </strong> please enter the valid email address</div><div style="height:20px"></div>');
            return false ;
          }
          $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are sending your services request...</div><div style="height:20px"></div>');
        },
        success: function(data) {            
            $('#zipcodeMsg').html(data);   
        },
        error: function() {
          $('#popupservice').modal();
          $('#zipcodeMsg').html('<div><i class="fa fa-exclamation-triangle" style="color:red"></i> Your request status is filed</div>');
        }
    });
    return false; 
  });

$(document).on('click','.services_data',function (e) {
    document.getElementById("zipcodeMsg").innerHTML ='<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-price-listing',
      data: { service: service },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','.footer_services_data',function (e) {
   
    $('#popupservice').modal({backdrop: 'static', keyboard: false}); 
    var back_allowed='yes';
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-price-listing',
      data: { service: service , back_allowed:back_allowed },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('change','#additional_operator',function (e) {
    var service = $('#service').val();
    var back_allowed='yes';
    if ($(this).is(':checked'))
    {
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-additional-operator-charges',
        data: { service: service },
        success: function (response_data) {
          $('#price_container').html(response_data); 
        }
      });
    }
    else
    {
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-service-charges',
        data: { service: service },
        success: function (response_data) {
          $('#price_container').html(response_data); 
        }
      });
    }
    return false;
});

$(document).on('click','#backtoprices',function (e) {
    document.getElementById("zipcodeMsg").innerHTML ='<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-price-listing',
      data: { service: service },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','#backtoaddress',function (e) {    
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-choose-address',
      data: { service: service },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});


$(document).on('click','#loginMe',function (e) {

    var userId = $(this).attr('userId');
    if(userId==''){
      var ajaxurl ='ajax-login-info';
    } else {
      var ajaxurl ='ajax-choose-address';
    }

    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+ajaxurl,
      data: $('#installatioServices').serialize(),
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','#backtoLogin',function (e) {
    document.getElementById("zipcodeMsg").innerHTML ='<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-login-info',
      data: { service: service },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','#backtophone',function (e) {
    document.getElementById("zipcodeMsg").innerHTML ='<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'new-customer',
      data: { service: service },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','#backtoregistration',function (e) {
    document.getElementById("zipcodeMsg").innerHTML ='<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    var service = $(this).attr('service-data');
    var userphone = $(this).attr('phone-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'phone-login',
      data: { service: service, userphone: userphone },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('click','.deleteMe',function (e) {
    
    var delId = $(this).attr('delId');    
    if (confirm('Are you sure want to delete this address ?')) {
      $(this).closest("li").remove();
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'delete-address',
        data: { id: delId },
        success: function (data) {
          //$('#zipcodeMsg').html(data); 
        }
      });
    }
});

$(document).on('click','#choosertopayment',function (e) {
      var did=2;  
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'choose-payment-method',
        data: { id: did },
        success: function (data) {
          $('#zipcodeMsg').html(data); 
        }
      });
});

$(document).on('click','#applyCoupon',function (e) {
    e.preventDefault();
    var coupon_code = $('#coupon_code').val();

    if($('.couponbox .price_detail #coupon_msgbox').length==0)
    {
      $('.couponbox .price_detail').append('<div class="row"><div id="coupon_msgbox" class="col-md-12 text-left"></div></div>');
    }

    $('#coupon_msgbox').html('');

    if(coupon_code=='')
    {
      $('#coupon_msgbox').html('<div class="alert alert-danger"><strong>Oops:</strong> Please enter coupon code !!</div>');
    }
    else
    {
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-apply-coupon',
        data: { coupon_code: coupon_code },
        beforeSend: function() {   
           $('#coupon_msgbox').html('<div class="alert alert-primary"> Checking....</div>');
        },
        success: function (data)
        {

          if(data.trim()=='success')
          {
            $.ajax({
              type: 'post',
              url: $('#baseurl').attr("href")+'service-order-review',
              success: function (resultdata) {        
                  $('#zipcodeMsg').html(resultdata); 
              }
            });
            $('#coupon_msgbox').html('<div class="alert alert-success"><strong>Great:</strong> Coupon applied successfully.</div>');         
          } 
          else if(data.trim()=='expired')
          {
            $('#coupon_msgbox').html('<div class="alert alert-danger"><strong>Oops:</strong> Applied Coupon has been expired.</div>');
          }
          else if(data.trim()=='unreached_limit')
          {
            $('#coupon_msgbox').html('<div class="alert alert-danger"><strong>Oops:</strong> Applied Coupon is Unreachable to minimum limit.</div>');
          }
          else if(data.trim()=='invalid_cat')
          {
            $('#coupon_msgbox').html('<div class="alert alert-danger"><strong>Oops:</strong> Applied Coupon not valid for this Service.</div>');
          }
          else
          {
            $('#coupon_msgbox').html('<div class="alert alert-danger"><strong>Oops:</strong> Applied Coupon is Invalid.</div>');
          }
          
        }
      });
    }    
    return false;
});

function countryCode(){

    var input = document.querySelector("#userphone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      geoIpLookup: function(callback) {
      $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        var countryCode = (resp && resp.country) ? resp.country : "";
        callback(countryCode);
      });
      },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      onlyCountries: ['us','in','ca'],
      placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/front/js/utils.js",
    });
}

$(document).on('click','#newCustomer',function (e) {
    
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'new-customer',
      data: { service: service },
      beforeSend: function() {   
         $('#zipcodeMsg').html('<div class="pop-loader"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are finding nearest services...</div><div style="height:20px"></div>');
      },
      success: function (data) {
        $('#zipcodeMsg').html(data);
        //countryCode(); 
      }
    });
    return false;
});

$(document).on('click','#repeatCustomer,#backtorepeatphone',function (e) {
    
    var service = $(this).attr('service-data');
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'repeat-customer',
      data: { service: service },
      success: function (data) {
        $('#zipcodeMsg').html(data); 
      }
    });
    return false;
});

$(document).on('submit','#jserilize',function (e) {
  var obj =$(this);   
    var service = obj.find("#pLogincheck").attr('service-data');
    var userphone = obj.find("#userphone").val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'phone-login',
      data: { service: service , userphone: userphone },
      success: function (dataphone) {
        if(dataphone=='2'){
          $('#infoExist').html('<div class="error">Applicant mobile number already in use</div>'); 
        } else {
          $('#zipcodeMsg').html(dataphone); 
        }
      }
    });
    return false;
});

$(document).on('submit','#repeatLogin',function (e) {
  var obj =$(this);   
    var service = obj.find("#pLogincheck").attr('service-data');
    var userphone = obj.find("#userphone").val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'repeat-login',
      data: { service: service , userphone: userphone },
      beforeSend: function() {   
         $('#infoExist').html('<div class="preloader">Please wait....</div>');
      },
      success: function (dataphone) {
        
        if(dataphone=='3'){
          $('#infoExist').html('<div class="error">Entered mobile number is not exists in our system.</div>'); 
        } else {
          $('#zipcodeMsg').html(dataphone); 
        }
      }
    });
    return false;
});


var timer = null;

$(document).on('keydown','#servicesarea',function (e) {
  clearTimeout(timer); 
  timer = setTimeout(validateZipcode, 300)
});

$(document).on('click','#forgotpass',function (e) {
  //$('#popupservice').modal('toggle');
  $('#popupservice').modal('hide');
});


function validateZipcode() {
    var zipcode = $('#servicesarea').val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-validate-zipcode-search',
      data: { zipcode: zipcode },
      beforeSend: function() {
        //$('#zipcodeSearch button').html('<i class="fas fa-search"></i>Search');
        $('#zipcodeSearch button').attr('disabled','disabled');
      },
      success: function (response) {
        //$('#zipcodeSearch button').html('<i class="fas fa-search"></i>Search');
        if(response.trim()=='invalid')
        {
          $('#servicesarea').addClass('errormsg');
          $('#zipcodeSearch button').addClass('errormsg');
          $('#zipcodeSearch button').attr('disabled','disabled');

        }
        else
        {
          $('#servicesarea').removeClass('errormsg');
          $('#zipcodeSearch button').removeClass('errormsg');
          $('#zipcodeSearch button').removeAttr('disabled');
        }
      }
    });
    return false;
}


$(document).on('keyup','#servicesarea2',function (e) {
  var zipcode = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-validate-zipcode-search',
      data: { zipcode: zipcode },
      beforeSend: function() {
        //$('#zipcodeSearch button').html('<i class="fas fa-search"></i>Search');
        $('#zipcodeSearch button').attr('disabled','disabled');
      },
      success: function (response) {
        //$('#zipcodeSearch button').html('<i class="fas fa-search"></i>Search');
        if(response.trim()=='invalid')
        {
          $('#servicesarea').addClass('errormsg');
          $('#zipcodeSearch button').addClass('errormsg');
          $('#zipcodeSearch button').attr('disabled','disabled');

        }
        else
        {
          $('#servicesarea').removeClass('errormsg');
          $('#zipcodeSearch button').removeClass('errormsg');
          $('#zipcodeSearch button').removeAttr('disabled');
        }
      }
    });
    return false;
});

$(document).on('submit','#validateotp',function (e) {
    
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'validateotp',
      data: $('#validateotp').serialize(),
      success: function (resultdata) {
        if(resultdata==3){          
          $('#infoExist').html('<div class="error">Please enter correct one time password</div>');
        } else {
          $('#zipcodeMsg').html(resultdata); 
        }
      }
    });
    return false;
});

$(document).on('submit','#ajxRegistration',function (e) {
    
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-user-registration',
      data: $('#ajxRegistration').serialize(),
      success: function (resultdata) {
        if(resultdata==2){          
          $('#emailExist').html('<div class="error">Applicant email address already in use</div>');
        } else {
          $('#zipcodeMsg').html(resultdata); 
        }
      }
    });
    return false;
});

$(document).on('submit','#forgotpassword',function (e)
{
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-forgot-password',
      data: $('#forgotpassword').serialize(),
      success: function (response)
      {
        $('#fpMsg').html(response);
      }
    });
    return false;
});


$(document).on('submit','#getAddress',function (e) {
    
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'add-new-address',
      data: $('#getAddress').serialize(),
      success: function (resultdata) {        
        $('#addresListMsg').append(resultdata);         
      }
    });
    return false;
});



$(document).on('click','#gotoaddrespic',function (e)
{
    var radioValue = $("input[name='radio']:checked").val();
    if(radioValue == '' && $('#postal_code').val()=='')
    {
        if( $('#addressZipValidation').length == 0)
        {
          $('#addDatatime').prepend('<div id="addressZipValidation"></div>');
        }
        $('#addressZipValidation').html('<div class="alert alert-danger"><strong>Oops:</strong> Service address has not selected or selected invalid address. </div>');
        return false;
    }
    else
    {
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'choose-date-time',
        data: $('#addDatatime').serialize(),
        beforeSend: function()
        {
          if( $('#addressZipValidation').length == 0)
          {
            $('#addDatatime').prepend('<div id="addressZipValidation"></div>');
          }
        },
        success: function (resultdata) {
          if(resultdata.trim()=='unmatched')
          {
            $('#addressZipValidation').html('<div class="alert alert-danger"><strong>Oops:</strong> Selected service is not available on this address. Please change address and try to choose address which belong to searched zipcode area.</div>');
            return false;
          }
          else
          {
            $('#zipcodeMsg').html(resultdata);
          }        
        }
      });
    }
    
    return false;
});

$(document).on('click','#addhomeaddress',function (e) {
    var datetime=$('#result').val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'choose-date-time',
      data: {datetime:datetime},
      success: function (resultdata) {        
          $('#zipcodeMsg').html(resultdata); 
      }
    });
    return false;
});

$(document).on('click','#addServiceOrder',function (e) {   
    var datetime=$('#result').val(); 
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'service-order-review',
      data: {datetime:datetime},
      success: function (resultdata) {        
          $('#zipcodeMsg').html(resultdata); 
      }
    });
    return false;
});

$(document).on('click','#confirmpayment',function (e) {      
    $("#gotoPayment").submit();
});

$(document).on('click','#backtodatetime',function (e) {    
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'choose-date-time',
      data: $('#addDatatime').serialize(),
      success: function (resultdata) {
        if(resultdata.trim()=='unmatched')
        {
          $('#addressZipValidation').html('<div class="alert alert-danger"><strong>Oops:</strong> Selected service is not available on this address. Please change address and try to choose address which belong to searched zipcode area.</div>');
          return false;
        }
        else
        {
          $('#zipcodeMsg').html(resultdata);
        }
      }
    });
    return false;
});

$(document).on('submit','#myLogin',function (e) {
    document.getElementById("loginMsg").innerHTML ='<div><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are login...</div><div style="height:20px"></div>';
    $.ajax({
      type: 'post',
      url: $('body').attr("data-url")+'login/frontlogin',
      data: $('#myLogin').serialize(),
      success: function (resultdata) {
        if(resultdata==1){
          //window.location.href='dashboard';
          location.reload();
        } else if(resultdata==2){
          $('#loginMsg').html('<div class="alert alert-danger">Your account is not active to access.</div>');
        
        } else if(resultdata==6){
          $('#loginMsg').html('<div class="alert alert-danger">Your account is temporarily rejected. Please contact administrator.</div>');
        
        } else {
          $('#loginMsg').html('<div class="alert alert-danger">Invalid username or password</div>');
        }
      }
    });
    return false;
});


$(document).on('change','#role',function (e) {
    e.preventDefault();
    var val = $(this).val();
    if(val==5)
    {
      $('#companybox').show();
      $('#name').attr('placeholder','Contact name');
    }
    else
    {
      $('#companybox').hide();
      $('#name').attr('placeholder','Full name');
    }
});


$(document).on('submit','#myRegistration',function (e) {
    document.getElementById("registrationMsg").innerHTML ='<div><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are register...</div><div style="height:20px"></div>';
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: $('body').attr("data-url")+'login/register',
      data: $('#myRegistration').serialize(),
      success: function (data) {
        $('#registrationMsg').html(data);

        setTimeout(function(){
           window.location.reload();
        }, 3000);
      }
    });
    return false;
});





$('#assignbtn').click(function () {
    var id = $('#operator').attr('data-id');
    var operator = $('#operator').val();
    if(operator=='')
    {
      $('.alert-msg').html('<div class="alert alert-danger"><strong>Error: </strong>Please choose any operator.</div></div>');
      return false;
    }
    else
    {
        $.ajax({
            url: $('#baseurl').attr("href")+'ajax-assign-operator',
            type: 'POST',
            data: {id:id,operator:operator},
            beforeSend: function() {   
              $('.alert-msg').html('<div class="alert alert-info">Processing.....</div></div>');
            },
            success: function (response)
            {
              if(response.trim()=='success')
              {
                $('.alert-msg').html('<div class="alert alert-success"><strong>Success: </strong>Operator assign to this service booking successfully.</div></div>'); 
              }
              else
              {
                $('.alert-msg').html('<div class="alert alert-danger"><strong>Oops: </strong>Something going wrong. Please try again.</div>');
              }
              
            },
            error: function (response) {
              $('.alert-msg').html('<div class="alert alert-danger"><strong>Error: </strong>'+response+'</div>');
            }
        });
    }
});

$(document).on('click','.changeby',function (e) {
    $( "#filterDetails" ).submit();
});

$(document).on('change','#support_type',function (e) {
    e.preventDefault();
    var support_type = $(this).val();
	  var ajax_url = $('#baseurl').attr("href")+'ajax-order-options';
    $.ajax({
      type: 'post',
      url: ajax_url,
      data: { support_type: support_type },
      success: function (data)
      {
        $('#orders').html(data); 
      }
    });
    return false;
});

$(document).on('click', '#rescheduleMe', function() {     
    $('#rescheduleBox').slideToggle(1000);
});

$(document).on('click', '#assignToOperator', function() {     
    $('#assignBox').slideToggle(1000);
});

$(document).on('change','#country',function (e) {
    e.preventDefault();
    var country_id = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-states',
      data: { 'country_id': country_id },
      success: function (data) {
        $('#administrative_area_level_1').html(data); 
      }
    });
    return false;
});

$(document).on('change','#home_country',function (e) {
    e.preventDefault();
    var country_id = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-states',
      data: { 'country_id': country_id },
      success: function (data) {
        $('#home_state').html(data); 
      }
    });
    return false;
});

$(document).on('change','#business_country',function (e) {
    e.preventDefault();
    var country_id = $(this).val();
    $.ajax({
      type: 'post',
      url: $('#baseurl').attr("href")+'ajax-states',
      data: { 'country_id': country_id },
      success: function (data) {
        $('#business_state').html(data); 
      }
    });
    return false;
});

$(document).on('change','#same_address',function (e) {
    e.preventDefault();

    if ($(this).is(":checked"))
    {
      var country_id = $("#home_country").val();
      $("#business_country").val(country_id);
      $.ajax({
        type: 'post',
        url: $('#baseurl').attr("href")+'ajax-states',
        data: { 'country_id': country_id },
        success: function (data) {
          $('#business_state').html(data);          
        }
      }); 
      setTimeout(function(){ $('#business_state').val($("#home_state").val()); }, 500);   
      $("#business_aptno").val($("#aptno").val());
      $("#business_street_no").val($("#street_number").val());
      $("#business_street_name").val($("#street_name").val());
      $("#business_street_type").val($("#street_type").val());
      $("#business_direction").val($("#direction").val());   
      $("#business_city").val($("#locality").val());
      $("#service_zip_code").val($("#postal_code").val());
      $("#bussphonecode").val($("#telephone").val());
    }
    else
    {
      $("#business_country").val(0);
      $("#business_state").val(0);
      $("#business_aptno").val('');
      $("#business_street_no").val('');
      $("#business_street_name").val('');
      $("#business_street_type").val('');
      $("#business_direction").val('');   
      $("#business_city").val('');
      $("#service_zip_code").val('');
      $("#bussphonecode").val('');
    }
    return false;
});

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(regex.test(email)) {
    return true;
  }else{
    return false;
  }
}

$(document).on('click', '#chooseAnyServices', function(){ 
  if ($(".service-popup-links form").length > 0)
  {
    $('#chooseserviceMsg').html('');
    var inputNameQuery = $('#inputNameQuery').val();
    var inputEmailQuery = $('#inputEmailQuery').val();
    var custome_area = $('input[name="custome_area"]').val();
    if(inputNameQuery=='' || inputEmailQuery=='')
    {
      $('#errorQueryMsg').html('<div class="QueryErr"><strong>Error ! </strong> Please enter the required field</div><div style="height:20px"></div>');
    }
    if(!isEmail(inputEmailQuery))
    {
      $('#errorQueryMsg').html('<div class="QueryErr"><strong>Error ! </strong> Please enter valid email Id.</div><div style="height:20px"></div>');
    }
    else
    {
        $.ajax({
          url: $('#baseurl').attr("href")+'ajax-subscribe-service',
          type: 'POST',
          data: {'group':1,'name':inputNameQuery,'email':inputEmailQuery,'zipcode':custome_area},
          success: function (response_data)
          {
            if(response_data.trim()=='exist')
            {
              $('#errorQueryMsg').html('<div class="alert alert-warning"><strong>Oops ! </strong> Email Id is already subscribed for zipcode "'+custome_area+'".</div>');
            }
            else
            {
              $('#errorQueryMsg').html('<div class="alert alert-success"><strong>Success ! </strong> We have noted your service search. we will update you once service will be availabe in this zipcode.</div>');
            }
          }
        });
    }
  }
  else
  {
    $('#chooseserviceMsg').html('<span style="color:#ff0000">Please choose any one service</span>');
  }    
});

$(document).on('click', '#subscribebtn', function(){ 
    var subscriber_email = $('#subscriber_email').val();
    if(subscriber_email=='')
    {
      $('#subscription-message').html('<div class="alert alert-danger"><strong>Error ! </strong> Email Id required !!</div>');
    }
    else
    {
        $.ajax({
          url: $('#baseurl').attr("href")+'ajax-subscribe-service',
          type: 'POST',
          data: {'group':2,'email':subscriber_email},
          success: function (response_data) {
            if(response_data.trim()=='exist')
            {
                $('#subscription-message').html('<div class="alert alert-warning"><strong>Warning ! </strong> Already subscribed !!</div>');
            }
            else if(response_data.trim()=='success')
            {
              $('#subscription-message').html('<div class="alert alert-success"><strong>Success ! </strong> Successfully subscribed.</div>');
            }
            else
            {
                $('#subscription-message').html('<div class="alert alert-danger"><strong>Oops ! </strong> Something going wrong !!</div>');
            }
          }
        });
    } 
});

$(document).on('click', '#isTypeOfUser', function() {     
    $('#chooseserviceMsg').html('<span style="color:#ff0000">Please choose are you new customer or return customer</span>');
});

$(document).on('click', '.addtowishlist', function() {     
  var obj=$(this); 
  var product_id=obj.attr("product_id");
  $.ajax({
  url: $('#baseurl').attr("href")+'addtowishlist',
  type: 'POST',
  data: {product_id:product_id},
  beforeSend: function() {   
    $('#wishlist_responce').html('<div><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Please wait we are sending request...</div><div style="height:20px"></div>');
    
  },
  success: function (data) {
    if(data!=2){
      $('#wishlist_responce').html(data); 
      $("#wishlist_popup").modal(); 
    } else {
      $("#login").modal(); 
    }
  },
  error: function (data) {
    $('#wishlist_responce').html(data); 
  }
  }) ;
});

jQuery("input[name=business]").click(function(){
    var business=jQuery(this).val();
    if(business=='yes')
    {
      jQuery("#businessDetails").css('display','block');
      jQuery("#business_name").attr("required","required");
      jQuery("#business_type").attr("required","required");
    }
    else
    {
      jQuery("#businessDetails").css('display','none');
      jQuery("#business_name").removeAttr("required");
      jQuery("#business_type").removeAttr("required");
    }
}); 

jQuery("input[name=business]").click(function(){
    var business=jQuery(this).val();
    if(business=='yes')
    {
      jQuery("#businessDetails").css('display','block');
      jQuery("#business_name").attr("required","required");
      jQuery("#business_type").attr("required","required");
      jQuery("#business_country").attr("required","required");
      jQuery("#business_state").attr("required","required");
      jQuery("#business_street_no").attr("required","required");
      jQuery("#business_street_name").attr("required","required");
      jQuery("#business_street_type").attr("required","required");   
      jQuery("#business_city").attr("required","required");
      jQuery("#service_zip_code").attr("required","required");
      jQuery("#business_mobile").attr("required","required");
      jQuery("#business_email").attr("required","required");
    }
    else
    {
      jQuery("#businessDetails").css('display','none');
      jQuery("#business_name").removeAttr("required");
      jQuery("#business_type").removeAttr("required");
      jQuery("#business_country").removeAttr("required");
      jQuery("#business_state").removeAttr("required");
      jQuery("#business_street_no").removeAttr("required");
      jQuery("#business_street_name").removeAttr("required");
      jQuery("#business_street_type").removeAttr("required");
      jQuery("#business_city").removeAttr("required");
      jQuery("#service_zip_code").removeAttr("required");
      jQuery("#business_mobile").removeAttr("required");
      jQuery("#business_email").removeAttr("required");

    }
}); 

jQuery("input[name=license]").click(function(){
    var license=jQuery(this).val();
    if(license=='yes')
    {
      jQuery("#licenseDetails").css('display','block');
      jQuery("#license_details").attr("required","required");
    } else {
       jQuery("#licenseDetails").css('display','none');
       jQuery("#license_details").removeAttr("required");
    }
}); 

jQuery("input[name=business-info]").click(function(){
    var license=$(this).is(':checked');
    if(license)
    {
      jQuery("#business-logic").css('display','block');
    } else {
       jQuery("#business-logic").css('display','none');
    }
}); 

jQuery(window).scroll(function(){ 
  var scroll = jQuery(window).scrollTop();
  if (scroll>=100){ 
    jQuery('header').addClass('fix-top');
  }
  else{
    jQuery('header').removeClass('fix-top');
  }
});


function customRadio(radioName){
        var radioButton = $('input[name="'+ radioName +'"]');
        $(radioButton).each(function(){
            $(this).wrap( "<span class='custom-radio'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
        });
        $(radioButton).click(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
            $(radioButton).not(this).each(function(){
                $(this).parent().removeClass("selected");
            });
        });
}

$(document).ready(function (){
    customRadio("gender");
    customRadio("business");
    customRadio("license");
});

function customCheckbox(checkboxName){
    var checkBox = $('input[name="'+ checkboxName +'"]');
    $(checkBox).each(function(){
        $(this).wrap( "<span class='custom-checkbox'></span>" );
        if($(this).is(':checked')){
            $(this).parent().addClass("selected");
        }
    });
    $(checkBox).click(function(){
        $(this).parent().toggleClass("selected");
    });
}

$(document).ready(function (){
    customCheckbox("useful");
    customCheckbox("business-info");
    customCheckbox("rememberme");       
});

if( $('#phonecode').length > 0 )
{
    var phoneNumq = $("#phonecode");
    var input = document.querySelector("#phonecode");
    var iti = window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      //geoIpLookup: function(callback) {
      //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       // var countryCode = (resp && resp.country) ? resp.country : "";
       // callback(countryCode);
      //});
      //},
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      nationalMode: true,
      onlyCountries: ['us','in','ca'],
      //placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/front/js/utils.js",
    });

    $(function() {
        var txt=$('.personal_phonebox .selected-dial-code').text();
        var ph=phoneNumq.val();
        $("input[name=mobbox]").val(txt+ph);
    });

    $('.personal_phonebox #country-listbox li').on("click", function () {
        var txt=$(this).find('.dial-code').text();
        var ph=phoneNumq.val();
        $("input[name=mobbox]").val(txt+ph);
    });

    phoneNumq.on("keyup", function () {    
        var txt=$('.personal_phonebox .selected-dial-code').text();
        var ph=phoneNumq.val();
        $("input[name=mobbox]").val(txt+ph);
    });
}

if( $('#bussphonecode').length > 0 )
{
    /* 2nd phone field */
    var bussiness_phoneNumq = $("#bussphonecode");
    var bussiness_input = document.querySelector("#bussphonecode");
    var bussiness_iti = window.intlTelInput(bussiness_input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      //geoIpLookup: function(callback) {
      //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       // var countryCode = (resp && resp.country) ? resp.country : "";
       // callback(countryCode);
      //});
      //},
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      nationalMode: true,
      onlyCountries: ['us','in','ca'],
      //placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/front/js/utils.js",
    });

    $(function() {
        var bussiness_txt=$('.bussiness_phonebox .selected-dial-code').text();
        var bussiness_ph=bussiness_phoneNumq.val();
        $("input[name=bussmobbox]").val(bussiness_txt+bussiness_ph);
    });

    $('.bussiness_phonebox #country-listbox li').on("click", function () {
        var bussiness_txt=$(this).find('.dial-code').text();
        var bussiness_ph=bussiness_phoneNumq.val();
        $("input[name=bussmobbox]").val(bussiness_txt+bussiness_ph);
    });

    bussiness_phoneNumq.on("keyup", function () {
        var bussiness_txt=$('.bussiness_phonebox .selected-dial-code').text();
        var bussiness_ph=bussiness_phoneNumq.val();
        $("input[name=bussmobbox]").val(bussiness_txt+bussiness_ph);
    });
}

if( $('#telephone').length > 0 )
{
    /* 2nd phone field */
    var telephone = $("#telephone");
    var telephone_input = document.querySelector("#telephone");
    var telephone_iti = window.intlTelInput(telephone_input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      //geoIpLookup: function(callback) {
      //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       // var countryCode = (resp && resp.country) ? resp.country : "";
       // callback(countryCode);
      //});
      //},
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      nationalMode: true,
      onlyCountries: ['us','in','ca'],
      //placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "skin/front/js/utils.js",
    });

    $(function() {
        var telephone_txt=$('.telephone_box .selected-dial-code').text();
        var telephone_number=telephone.val();
        $("input[name=telephonebox]").val(telephone_txt+telephone_number);
    });

    $('.telephone_box #country-listbox li').on("click", function () {
        var telephone_txt=$(this).find('.dial-code').text();
        var telephone_number=telephone.val();
        $("input[name=telephonebox]").val(telephone_txt+telephone_number);
    });

    telephone.on("keyup", function () {
        var telephone_txt=$('.telephone_box .selected-dial-code').text();
        var telephone_number=telephone.val();
        $("input[name=telephonebox]").val(telephone_txt+telephone_number);
    });
}