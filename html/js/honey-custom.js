$(document).ready(function() {
    $("#slider").owlCarousel({
		items : 1,
		nav:true,
		dots:true,
		loop:true,
		autoplay:false,
		mouseDrag:true,
		autoplayTimeout:3000,smartSpeed:1000,
		autoplayHoverPause:true,
	navText : ["<span><i class='fa fa-angle-left'></i></span>","<span><i class='fa fa-angle-right'></i></span>"]
      });
    });
$(document).ready(function() {
    $("#work-slider01").owlCarousel({
		nav:true,
		dots:false,
		loop:true,
		autoplay:true,
		mouseDrag:true,
		autoplayHoverPause:true,
		navText : ["<span><i class='fa fa-long-arrow-left'></i></span>","<span><i class='fa fa-long-arrow-right'></i></span>"],
		responsiveClass: true,
		responsive: {
	  0: {
		items: 1,
		nav: true
	  },
	  600: {
		items: 3,
		nav: false
	  },
	  1000: {
		items: 2,
		nav: false,
		loop: true,
		margin:30
	  }
	}
      });
    });

$(document).ready(function() {
    $("#client").owlCarousel({
		nav:true,
		dots:true,
		loop:true,
		autoplay:true,
		mouseDrag:true,
		autoplayHoverPause:true,
		navText : ["<span><i class='fa fa-angle-left'></i></span>","<span><i class='fa fa-angle-right'></i></span>"],
		responsiveClass: true,
		responsive: {
	  0: {
		items: 1,
		nav: true
	  },
	  600: {
		items: 5,
		nav: false
	  },
	  1000: {
		items: 5,
		nav: false,
		loop: true,
		margin:30
	  }
	}
      });
    });

$(document).ready(function() {
    $("#work-slider").owlCarousel({
		nav:true,
		dots:false,
		loop:true,
		autoplay:true,
		mouseDrag:true,
		autoplayHoverPause:true,
		navText : ["<span><i class='fas fa-chevron-left'></i></span>","<span><i class='fas fa-chevron-right'></i></span>"],
		responsiveClass: true,
		responsive: {
	  0: {
		items: 1,
		nav: true
	  },
	  600: {
		items: 3,
		nav: false
	  },
	  1000: {
		items: 3,
		nav: false,
		loop: true,
		margin:30
	  }
	}
      });
    });


$(document).ready(function() {
    $("#testimonials").owlCarousel({
		nav:true,
		dots:false,
		loop:true,
		autoplay:true,
		mouseDrag:true,
		autoplayHoverPause:true,
		navText : ["<span><i class='fas fa-chevron-left'></i></span>","<span><i class='fas fa-chevron-right'></i></span>"],
		responsiveClass: true,
		responsive: {
	  0: {
		items: 1,
		nav: true
	  },
	  600: {
		items:1,
		nav: false
	  },
	  1000: {
		items: 1,
		nav: false,
		loop: true,
		margin:30
	  }
	}
      });
    });


$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
	loop: true,
	responsiveClass: true,
	responsive: {
	  0: {
		items: 1,
		nav: true
	  },
	  600: {
		items: 3,
		nav: false
	  },
	  1000: {
		items: 3,
		nav: false,
		loop: true,
		margin:30
	  }
	}
  })
})


$(document).ready(function(){
$(window).scroll(function(){
if($(this).scrollTop() > 200){
	$('.up').fadeIn(400);
}else{ $('.up').fadeOut(400); }	
});

$('.up').click(function(event){
event.preventDefault();
$('html, body').animate({scrollTop:0}, 800);
});
	
});





jQuery(window).scroll(function(){ 
	var scroll = jQuery(window).scrollTop();
	if (scroll>=150){ 
		jQuery('header').addClass('fixed-nav');
	}
	else{
		jQuery('header').removeClass('fixed-nav');
	}
});

