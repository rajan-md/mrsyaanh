<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('CI')) {
    function CI()
    {
        $CI =& get_instance();
        return $CI;
    }
}

if (!function_exists('streetTypeOption')) {
    function streetTypeOption()
    {
        $options = array(
            '' => CI()->lang->line('SELECT_DIRECTION'),
            'Street' => 'Street (ST)',
            'Road' => 'Road (RD)',
            'Avenue' => 'Avenue (AVE)',
            'Boulvard' => 'Boulvard (BLVD)'
        );
        return $options;
    }
}

if (!function_exists('paymentMethodOption')) {
    function paymentMethodOption()
    {
        $options = array(
            '' => '-N/A-',
            'cod' => 'Cash On Delivery',
            'paypal' => 'Paypal',
            'card' => 'Debit/Credit Card'
        );
        return $options;
    }
}

if (!function_exists('shippingMethodOption')) {
    function shippingMethodOption()
    {
        $options = array(
            '' => 'Free Delivery',
            'free' => 'Free Delivery',
            'flat' => 'Flat Shipping'
        );
        return $options;
    }
}

if(!function_exists('getDiscountTypes')){
    function getDiscountTypes(){
        return array('flat'=>'Flat','percent'=>'Percent (%)');
    }
}

if (!function_exists('directionOption')) {
    function directionOption()
    {
        $options = array(
            '' => CI()->lang->line('SELECT_DIRECTION'),
            'East' => 'East (E)',
            'West' => 'West (W)',
            'North' => 'North (N)',
            'South' => 'South (S)'
        );
        return $options;
    }
}

if (!function_exists('bussinessTypeOption')) {
    function bussinessTypeOption()
    {
        $options = array(
            '' => CI()->lang->line('BUSINESS_TYPE'),
            'Sole Proprietorship' => 'Sole Proprietorship',
            'Corporation' => 'Corporation',
            'Partnership' => 'Partnership'
        );
        return $options;
    }
}

if (!function_exists('setUpdateData')) {
    function setUpdateData($table, $data, $id)
    {
        $array = array(
            'id' => $id
        );
        CI()->db->where($array);
        CI()->db->update($table, $data);
        return $id;
    }
}

if (!function_exists('setInsertData')) {
    function setInsertData($table, $data)
    {
        CI()->db->insert($table, $data);
        return CI()->db->insert_id();
    }
}

if (!function_exists('setDeleteData')) {
    function setDeleteData($table, $id)
    {
        $array = array(
            'id' => $id
        );
        CI()->db->where($array);
        CI()->db->delete($table);
        return $id;
    }
}

if (!function_exists('getDataRecords')) {
    function getDataRecords($table, $whereArr = array(), $start = 0, $limit = "", $order = "")
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        $query = CI()->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
        } else {
            $result = array();
        }
        return $result;
    }
}

if (!function_exists('getRowCount')) {
    function getRowCount($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        $query = CI()->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            return $count;
        } else {
            return 0;
        }
    }
}

if (!function_exists('getDataRecord')) {
    function getDataRecord($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        $query = CI()->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }
}

if (!function_exists('getStatus')) {
    function getStatus($id)
    {
        CI()->db->select('name');
        CI()->db->where('id', $id);
        $query    = CI()->db->get('tbl_status');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result->name;
        } else {
            return false;
        }
    }
}

if (!function_exists('requestStatus')) {
    function requestStatus($id)
    {
        $user_id = CI()->session->userdata('userID');
        CI()->db->where('service_order_id', $id);
        $query = CI()->db->get('tbl_service_leads');
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            
            if (!empty($result) && $result->accepter_id != $user_id && $result->status == 1) {
                return 6;
            } elseif (!empty($result) && $result->accepter_id == $user_id && $result->status == 2) {
                return 3;
            } else {
                return getServiceBookingStatus($id);
            }
            
        } else {
            return getServiceBookingStatus($id);
        }
    }
}

if(!function_exists('getCurrency')){
    function getCurrency()
    {
      CI()->db->select('value');
      CI()->db->where('name', 'currency');
      $query = CI()->db->get('tbl_settings');   
      $result = $query->row();
      return $result->value;

    }
}

if(!function_exists('getCurrencySymbol')){
    function getCurrencySymbol($currency_id=''){
          if(empty($currency_id))
                $currency_id = getCurrency();

          CI()->db->select('symbol');
          CI()->db->where('id',$currency_id);
          $query = CI()->db->get('tbl_currencies');
          $result = $query->row();
          return $result->symbol;
    }
}

if (!function_exists('generateOperatorNumber')) {
    function generateOperatorNumber($service_request_id, $userId)
    {
        $servicename = getServicesNameByRequestId($service_request_id);
        $servicename = substr($servicename, 0, 2);
        $servicename = strtoupper($servicename);
        $servicename = $servicename . date('Y');
        $servicename = $servicename . $userId;
        return $servicename;
    }
}

if(!function_exists('getSetting')){
    function getSetting($key)
    {
        $settings = get_record('settings',array('name'=>$key));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getSiteTitle')){
    function getSiteTitle()
    {
        $settings = get_record('settings',array('name'=>'sitename'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getSiteAdminEmail')){
    function getSiteAdminEmail()
    {
        $settings = get_record('settings',array('name'=>'admin_email'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getSiteEmail')){
    function getSiteEmail()
    {
        $settings = get_record('settings',array('name'=>'site_email'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getEmailLogo')){
    function getEmailLogo()
    {
        $settings = get_record('settings',array('name'=>'emaillogo'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getEmailSignature')){
    function getEmailSignature()
    {
        $settings = get_record('settings',array('name'=>'email_signature'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getEmailDisclaimer')){
    function getEmailDisclaimer()
    {
        $settings = get_record('settings',array('name'=>'email_disclaimer'));
        return !empty($settings->value)?$settings->value:'';
    }
}

if(!function_exists('getRoleName')){
    function getRoleName($id){
      CI()->db->select('name');
      CI()->db->where('id', $id);
      $query = CI()->db->get('tbl_role'); 
      $rowCount=$query->num_rows();
      if($rowCount>0){
          $result = $query->row();
          return $result->name;
      }
      else{
        return false;
      }
    }
}


if (!function_exists('getServicesNameByRequestId')) {
    function getServicesNameByRequestId($service_request_id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('p.*');
        CI()->db->from('tbl_services_content p');
        CI()->db->join('tbl_language l', 'l.id = p.language_id');
        CI()->db->join('tbl_service_provider_settings s', 's.service_id = p.services_id');
        CI()->db->where('s.services_request_id', $service_request_id);
        CI()->db->where('l.id', $language_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $row = $query->row();
            return $row->name;
        } else {
            return '';
        }
    }
}

if (!function_exists('getSPSettingsByBookingId')) {
    function getSPSettingsByBookingId($booking_id)
    {
        CI()->db->select('S.*');
        CI()->db->from('tbl_service_provider_settings S');
        CI()->db->join('tbl_service_provider_request R', 'R.id = S.services_request_id');
        CI()->db->join('tbl_service_orders O', 'O.vendor_id = R.user_id');
        CI()->db->where('O.id', $booking_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $row = $query->row();
            return $row;
        } else {
            return '';
        }
    }
}

if (!function_exists('getServiceBookingStatus')) {
    function getServiceBookingStatus($id)
    {
        CI()->db->where('id', $id);
        $query = CI()->db->get('tbl_service_orders');
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            //print_r($result); die;
            return $result->status;
        } else {
            return 0;
        }
    }
}

if (!function_exists('getBlogName')) {
    function getBlogName($id)
    {
        CI()->db->select('name');
        CI()->db->where('id', $id);
        $query    = CI()->db->get('blog');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result->name;
        } else {
            return false;
        }
    }
}

if (!function_exists('getServiceBookingStatusHeading')) {
    function getServiceBookingStatusHeading($status)
    {
        $heading = '';
        if (!empty($status) && $status == 6) {
            $heading = '<h2><i class="fa fa-times-circle"></i> ' . CI()->lang->line('BOOKING_ASSIGNED_TO_OTHERS') . '</h2>';
        } elseif (!empty($status) && $status == 5) {
            $heading = '<h2><i class="fa fa-check-circle"></i> ' . CI()->lang->line('BOOKING_CLOSED') . '</h2>';
        } elseif (!empty($status) && $status == 4) {
            $heading = '<h2><i class="fa fa-check-circle"></i> ' . CI()->lang->line('BOOKING_PROCESSED') . '</h2>';
        } elseif (!empty($status) && $status == 3) {
            $heading = '<h2><i class="fa fa-times-circle"></i> ' . CI()->lang->line('BOOKING_REJECTED') . '</h2>';
        } elseif (!empty($status) && $status == 2) {
            $heading = '<h2><i class="fa fa-times-circle"></i> ' . CI()->lang->line('BOOKING_CANCELLED') . '</h2>';
        } elseif (!empty($status) && $status == 2) {
            $heading = '<h2><i class="fa fa-times-circle"></i> ' . CI()->lang->line('BOOKING_CANCELLED') . '</h2>';
        } elseif (!empty($status) && $status == 1) {
            $heading = '<h2><i class="fa fa-check-circle"></i> ' . CI()->lang->line('BOOKING_ACCEPTED') . '</h2>';
        } else {
            $heading = '<h2><i class="fa fa-question-circle"></i> ' . CI()->lang->line('BOOKING_PENDING') . '</h2>';
        }
        
        return $heading;
    }
}

if (!function_exists('getServiceBookingStatusLabel')) {
    function getServiceBookingStatusLabel($status = '')
    {
        $statusArr = array(
            "Pending",
            "Accepted",
            "Cancalled",
            "Rejected",
            "Proceed",
            "Closed",
            "Unavailable"
        );
        if (array_key_exists($status, $statusArr)) {
            return $statusArr[$status];
        } else {
            return 'Pending';
        }
    }
}

if (!function_exists('getServiceBookingStatusOptions')) {
    function getServiceBookingStatusOptions($label = '')
    {
        $label     = !empty($label) ? $label : '---Status---';
        $statusArr = array(
            '' => $label,
            0 => "Pending",
            1 => "Accepted",
            2 => "Cancalled",
            3 => "Rejected",
            4 => "Proceed",
            5 => "Closed",
            6 => "Unavailable"
        );
        return $statusArr;
    }
}

if (!function_exists('getServiceBookingStatusClass')) {
    function getServiceBookingStatusClass($statusLabel = '')
    {
        $statusClassArr = array(
            "Pending" => "badge badge-warning",
            "Accepted" => "badge badge-success",
            "Cancalled" => "badge badge-danger",
            "Rejected" => "badge badge-danger",
            "Proceed" => "badge badge-success",
            "Closed" => "badge badge-secondary",
            "Unavailable" => "badge badge-danger"
        );
        
        if (!empty($statusLabel) && array_key_exists($statusLabel, $statusClassArr)) {
            return $statusClassArr[$statusLabel];
        } else {
            return 'badge badge-warning';
        }
    }
}

if (!function_exists('getServicePrice')) {
    function getServicePrice($id)
    {
        CI()->db->select('price');
        CI()->db->where('service_id', $id);
        $query    = CI()->db->get('tbl_services_price');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result->price;
        } else {
            return false;
        }
    }
}

if (!function_exists('getServiceProviderSettings')) {
    function getServiceProviderSettings($id = 0)
    {
        CI()->db->where('services_request_id', $id);
        $query    = CI()->db->get('tbl_service_provider_settings');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
    }
}

if (!function_exists('getAdvanceOption')) {
    function getAdvanceOption($name = "")
    {
        if (!empty($name)) {
            CI()->db->select('value');
            CI()->db->where('name', $name);
            $query  = CI()->db->get('tbl_settings');
            $result = $query->row();
            return $result->value;
        } else {
            return array();
        }
    }
}

if (!function_exists('getCurrencySign')) {
    function getCurrencySign()
    {
        $currency_id = getCurrency();
        CI()->db->select('code');
        CI()->db->where('id', $currency_id);
        $query  = CI()->db->get('tbl_currencies');
        $result = $query->row();
        return $result->code;
    }
}

if (!function_exists('getCurrencySignById')) {
    function getCurrencySignById($currency_id)
    {
        CI()->db->select('code');
        CI()->db->where('id', $currency_id);
        $query = CI()->db->get('tbl_currencies');
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result->code;
        }
    }
}

if (!function_exists('getlanguageOptions')) {
    function getlanguageOptions()
    {
        $html     = '';
        $sitelang = CI()->session->userdata('site_lang');
        CI()->db->order_by("order_by", "desc");
        $query = CI()->db->get('tbl_language');
        $lang  = $query->result();
        foreach ($lang as $value) {
            $html .= '<option value="' . strtolower($value->name) . '"';
            if ($sitelang == strtolower($value->name)) {
                $html .= 'selected="selected"';
            }
            $html .= '>' . $value->name . '</option>';
        }
        return $html;
    }
}

if (!function_exists('getCustomerDetailsByEmail')) {
    function getCustomerDetailsByEmail($email = "")
    {
        if (!empty($email)) {
            CI()->db->where('email', $email);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('isUserEmailExist')) {
    function isUserEmailExist($email)
    {
        if (!empty($email)) {
            CI()->db->where('email', $email);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}

if (!function_exists('getCountries')) {
    function getCountries()
    {
        CI()->db->select('*');
        CI()->db->from('tbl_country');
        CI()->db->where('status', 1);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
}

if (!function_exists('getCountry')) {
    function getCountry($id)
    {
        CI()->db->select('*');
        CI()->db->from('tbl_country');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
}

if (!function_exists('getCountryName')) {
    function getCountryName($id)
    {
        CI()->db->select('*');
        CI()->db->from('tbl_country');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $row = $query->row();
            return $row->name;
        } else {
            return '';
        }
    }
}

if (!function_exists('getCountriesOptions')) {
    function getCountriesOptions()
    {
        CI()->db->select('*');
        CI()->db->from('tbl_country');
        CI()->db->where('status', 1);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        $options  = array(
            '' => CI()->lang->line('SELECT_COUNTRY')
        );
        if ($rowCount > 0) {
            $records = $query->result();
            foreach ($records as $rec) {
                $options[$rec->id] = $rec->name;
            }
        }
        return $options;
    }
}

if (!function_exists('getStatesOptions')) {
    function getStatesOptions($country_id)
    {
        CI()->db->select('*');
        CI()->db->from('state');
        CI()->db->where('status', 1);
        CI()->db->where('country_id', $country_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        $options  = array(
            0 => CI()->lang->line('SELECT_STATE')
        );
        if ($rowCount > 0) {
            $records = $query->result();
            foreach ($records as $rec) {
                $options[$rec->id] = $rec->name;
            }
        }
        return $options;
    }
}

if (!function_exists('getStateName')) {
    function getStateName($id)
    {
        CI()->db->select('*');
        CI()->db->from('the_state');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $row = $query->row();
            return $row->name;
        } else {
            return '';
        }
    }
}

if (!function_exists('getCustomerDetailsById')) {
    function getCustomerDetailsById($id = "")
    {
        
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('getServiceProviderDetailsById')) {
    function getServiceProviderDetailsById($id = "")
    {
        
        if (!empty($id)) {
            CI()->db->from('tbl_users as U');
            CI()->db->join('tbl_service_provider_request as R', 'R.user_id = U.id');
            CI()->db->where('U.id', $id);
            $query = CI()->db->get();
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('getOrderDetailsById')) {
    function getOrderDetailsById($id = "")
    {
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_products_orders');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('getServicesDetails')) {
    function getServicesDetails($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('p.*');
        CI()->db->from('tbl_services_content p');
        CI()->db->join('tbl_language l', 'l.id = p.language_id');
        CI()->db->where('p.services_id', $id);
        CI()->db->where('l.id', $language_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
}

if (!function_exists('getOtherServicesName')) {
    function getOtherServicesName($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('p.name');
        CI()->db->from('tbl_services_price_content p');
        CI()->db->join('tbl_language l', 'l.id = p.language_id');
        CI()->db->where('p.service_price_id', $id);
        CI()->db->where('l.id', $language_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('name');
        } else {
            return '';
        }
    }
}

if (!function_exists('getServicesName')) {
    function getServicesName($id = 0)
    {
        CI()->db->select('name');
        CI()->db->from('tbl_services');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('name');
        } else {
            return '';
        }
    }
}

if (!function_exists('getServicesEmailContent')) {
    function getServicesEmailContent($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('email_content');
        CI()->db->from('tbl_services');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('email_content');
        } else {
            return '';
        }
    }
}

if (!function_exists('getAddressDetails')) {
    function getAddressDetails($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->from('tbl_users_address');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
}

if (!function_exists('getDistance')) {
    function getDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        if ((!empty($latitudeFrom)) && (!empty($longitudeFrom)) && (!empty($latitudeTo)) && (!empty($longitudeTo))) {
            $theta    = $longitudeFrom - $longitudeTo;
            $dist     = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) + cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
            $dist     = acos($dist);
            $dist     = rad2deg($dist);
            $miles    = $dist * 60 * 1.1515;
            $distance = ($miles * 1.609344); //km
            return $distance;
        } else {
            return $distance = 100; //km
        }
    }
}

if (!function_exists('findNearestProviders')) {
    function findNearestProviders($service_id, $addressData)
    {
        if (!empty($addressData)) {
            $postal_code = $addressData->postal_code;
            $areaCode    = substr($postal_code, 0, 3);
            CI()->db->select('U.id as id');
            CI()->db->from('tbl_users U');
            CI()->db->join('tbl_service_provider_request R', 'R.user_id = U.id');
            CI()->db->join('tbl_service_provider_settings S', 'S.services_request_id = R.id');
            CI()->db->where('U.role', 5);
            CI()->db->where('U.status', 1);
            CI()->db->where('R.service_status', 1);
            CI()->db->where('S.status', 1);
            CI()->db->where('S.service_id', $service_id);
            CI()->db->where('( find_in_set("' . $postal_code . '", S.service_zip_code) <> 0  OR (S.service_zip_code LIKE "' . $areaCode . '%" AND S.is_allowed_all_area=1) )');
            $query    = CI()->db->get();
            $rowCount = $query->num_rows();
            if ($rowCount > 0) {
                return $query->result_array();
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('findNearestUsers')) {
    function findNearestUsers($addressData = array())
    {
        if (!empty($addressData)) {
            $lat          = $addressData->latitude;
            $lon          = $addressData->longitude;
            $radius       = 30;
            $angle_radius = $radius / 111; // Every lat|lon degree째 is ~ 111Km
            $min_lat      = $lat - $angle_radius;
            $min_lon      = $lon - $angle_radius;
            $max_lat      = $lat + $angle_radius;
            $max_lon      = $lon + $angle_radius;
            
            CI()->db->where("latitude BETWEEN $min_lat AND $max_lat AND longitude BETWEEN $min_lon AND $max_lon");
            CI()->db->select('id');
            CI()->db->from('tbl_users');
            CI()->db->where('role', 5);
            
            
            $query    = CI()->db->get();
            $rowCount = $query->num_rows();
            if ($rowCount > 0) {
                return $query->result_array();
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('getServicesAddress')) {
    function getServicesAddress($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('site_location');
        CI()->db->from('tbl_users_address');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('site_location');
        } else {
            return '';
        }
    }
}

if (!function_exists('validateZipcodeForService')) {
    function validateZipcodeForService($zipcode, $service_id)
    {
        
        
        CI()->db->select('*');
        CI()->db->from('tbl_services s');
        CI()->db->join('tbl_service_provider_settings ss', 'ss.service_id = s.id');
        CI()->db->where('s.parent', 0);
        CI()->db->where('ss.service_id', $service_id);
        $areaCode = substr($zipcode, 0, 3);
        CI()->db->where('( find_in_set("' . $zipcode . '", ss.service_zip_code) <> 0  OR (ss.service_zip_code LIKE "' . $areaCode . '%" AND ss.is_allowed_all_area=1) )');
        CI()->db->where('ss.status', 1);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}

if (!function_exists('getAddressByZipcode')) {
    function getAddressByZipcode($zipcode)
    {
        $address = array();
        $key     = 'AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA';
        if (!empty($zipcode)) {
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $zipcode . '&sensor=true&key=' . $key);
            $output1         = json_decode($geocodeFromAddr);
            //echo "<pre>"; print_r($output1); echo "</pre>";
            //Get latitude and longitute from json data
            $latitude        = !empty($output1->results[0]->geometry->location->lat) ? $output1->results[0]->geometry->location->lat : '';
            $longitude       = !empty($output1->results[0]->geometry->location->lng) ? $output1->results[0]->geometry->location->lng : '';
            //Send request and receive json data by latitude longitute
            $addressArr      = array();
            if (!empty($latitude) && !empty($longitude)) {
                $geocodeFromLatlon = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&sensor=true&key=' . $key);
                $output2           = json_decode($geocodeFromLatlon);
                //echo "<pre>"; print_r($output2); echo "</pre>"; die;
                if (!empty($output2)) {
                    $addressComponents = $output2->results[0]->address_components;
                    foreach ($addressComponents as $addrComp) {
                        $addressArr[$addrComp->types[0]] = $addrComp->long_name;
                    }
                    $address['address']   = !empty($output2->results[0]->formatted_address) ? $output2->results[0]->formatted_address : '';
                    $address['zipcode']   = !empty($addressArr['postal_code']) ? $addressArr['postal_code'] : '';
                    $address['city']      = !empty($addressArr['locality']) ? $addressArr['locality'] : '';
                    $address['state']     = !empty($addressArr['administrative_area_level_1']) ? $addressArr['administrative_area_level_1'] : '';
                    $address['country']   = !empty($addressArr['country']) ? $addressArr['country'] : '';
                    $address['latitude']  = !empty($output2->results[0]->geometry->location->lat) ? $output2->results[0]->geometry->location->lat : '';
                    $address['longitude'] = !empty($output2->results[0]->geometry->location->lng) ? $output2->results[0]->geometry->location->lng : '';
                }
            }
            
            return $address;
        } else {
            return array();
        }
    }
}

if (!function_exists('getUserAddressZipCode')) {
    function getUserAddressZipCode($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('postal_code');
        CI()->db->from('tbl_users_address');
        CI()->db->where('id', $id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('postal_code');
        } else {
            return '';
        }
    }
}

if (!function_exists('getServicesOption')) {
    function getServicesOption($type = '', $id = 0)
    {
        $options     = array(
            '' => $type
        );
        $language_id = getActiveLanguage();
        CI()->db->select('p.id,p.services_id, p.name');
        CI()->db->from('tbl_services s');
        CI()->db->join('tbl_services_content p', 's.id = p.services_id');
        CI()->db->join('tbl_language l', 'l.id = p.language_id');
        CI()->db->where('l.id', $language_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $skills = $query->result();
            foreach ($skills as $value) {
                $options[$value->services_id] = $value->name;
            }
        }
        
        return $options;
    }
}

if (!function_exists('getServicesImage')) {
    function getServicesImage($id = 0)
    {
        CI()->db->select('featured_img');
        CI()->db->where('featured_img!=""');
        CI()->db->where('id', $id);
        $query    = CI()->db->get('tbl_services');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $featured_img = $query->row('featured_img');
            return '<img src="' . base_url($featured_img) . '" class="img-responsive" style="height:180px">';
        } else {
            return '<img src="' . base_url('skin/front/images/no_image.jpg') . '" class="img-responsive" style="height:180px">';
        }
    }
}

if (!function_exists('getTestimonialImage')) {
    function getTestimonialImage($id = 0)
    {
        CI()->db->select('featured_img');
        CI()->db->where('featured_img!=""');
        CI()->db->where('id', $id);
        $query    = CI()->db->get('tbl_testimonial');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $featured_img = $query->row('featured_img');
            return '<img src="' . base_url($featured_img) . '" class="img-responsive" style="height:50px">';
        } else {
            return '<img src="' . base_url('skin/front/images/no_image.jpg') . '" class="img-responsive" style="height:180px">';
        }
    }
}

if (!function_exists('getProductName')) {
    function getProductName($id = 0)
    {
        $language_id = getActiveLanguage();
        CI()->db->select('p.name');
        CI()->db->from('tbl_products_content p');
        CI()->db->join('tbl_language l', 'l.id = p.language_id');
        CI()->db->where('p.products_id', $id);
        CI()->db->where('l.id', $language_id);
        $query    = CI()->db->get();
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return $query->row('name');
        } else {
            return '';
        }
    }
}

if (!function_exists('getProductPrice')) {
    function getProductPrice($id = 0)
    {
        CI()->db->select('price');
        CI()->db->where('id', $id);
        $query    = CI()->db->get('tbl_products');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $price = $query->row('price');
            return $price;
        } else {
            return '0';
        }
    }
}

if (!function_exists('getProductStock')) {
    function getProductStock($id = 0)
    {
        CI()->db->select('quantity');
        CI()->db->where('id', $id);
        CI()->db->where('quantity>0');
        $query    = CI()->db->get('tbl_products');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return '<span class="text-success"><strong>In Stock</strong></span>';
        } else {
            return '<span class="text-danger"><strong>Out of Stock</strong></span>';
        }
    }
}


if (!function_exists('getProductImage')) {
    function getProductImage($id = 0)
    {
        CI()->db->select('file_name');
        CI()->db->where('product_id', $id);
        CI()->db->limit(1);
        CI()->db->order_by('id', 'desc');
        $query     = CI()->db->get('tbl_products_images');
        $rowCount  = $query->num_rows();
        $file_name = $query->row('file_name');
        if (!empty($file_name)) {
            return '<img src="' . base_url($file_name) . '" class="img-responsive">';
        } else {
            return '<img src="' . base_url('skin/front/images/no_image.jpg') . '" class="img-responsive">';
        }
    }
}

if (!function_exists('getBlogImage')) {
    function getBlogImage($id = 0)
    {
        CI()->db->select('featured_img');
        CI()->db->where('id', $id);
        $query     = CI()->db->get('tbl_blog');
        $rowCount  = $query->num_rows();
        $file_name = $query->row('featured_img');
        if (!empty($file_name)) {
            return '<img src="' . base_url($file_name) . '" class="img-responsive">';
        } else {
            return '<img src="' . base_url('skin/front/images/no_image.jpg') . '" class="img-responsive">';
        }
    }
}

if (!function_exists('getProductAllImage')) {
    function getProductAllImage($id = 0)
    {
        CI()->db->select('file_name');
        CI()->db->order_by('id', 'desc');
        CI()->db->where('product_id', $id);
        $query    = CI()->db->get('tbl_products_images');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
}

if (!function_exists('in_wishlist')) {
    function in_wishlist($product_id = 0, $user_id = 0)
    {
        
        CI()->db->where('product_id', $product_id);
        CI()->db->where('user_id', $user_id);
        $query    = CI()->db->get('tbl_wishlist');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            return false;
        } else {
            return true;
        }
    }
}


if (!function_exists('FooterServicesMenu')) {
    function FooterServicesMenu($limit = 15)
    {
        
        $language_id = getActiveLanguage();
        $query       = CI()->db->select('p1.id as id,p1.slug as slug, p1.featured_img as featured_img, p1.status as status , p2.name as name, p2.description as description')->from('tbl_services as p1')->join('tbl_services_content as p2', 'p1.id = p2.services_id', 'LEFT')->where('p2.language_id', $language_id)->where('p1.status', 1)->limit($limit, 0)->get();
        $rowCount    = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
}

if (!function_exists('getFooterHomeData')) {
    function getFooterHomeData()
    {
        
        $language_id = getActiveLanguage();
        $query       = CI()->db->get('tbl_home_settings');
        $resulthome  = $query->result();
        foreach ($resulthome as $homeContent) {
            $homeData[$homeContent->name] = $homeContent->value;
        }
        CI()->db->where('language_id', $language_id);
        $query    = CI()->db->get('tbl_home_setting_content');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row_array();
            return array_merge($result, $homeData);
        } else {
            return $homeData;
        }
    }
}


if (!function_exists('getCurrency')) {
    function getCurrency()
    {
        CI()->db->select('value');
        CI()->db->where('name', 'currency');
        $query  = CI()->db->get('tbl_settings');
        $result = $query->row();
        return $result->value;
    }
}

if (!function_exists('getPriceFormate')) {
    function getPriceFormate($price = "", $currency_id = "")
    {
        if (empty($currency_id))
            $currency_id = getCurrency();
        
        CI()->db->select('symbol');
        CI()->db->where('id', $currency_id);
        $query  = CI()->db->get('tbl_currencies');
        $result = $query->row();
        return $result->symbol . '' . number_format($price, 2);
    }
}

if (!function_exists('getCurrencyCode')) {
    function getCurrencyCode($currency_id = "")
    {
        if (empty($currency_id))
            $currency_id = getCurrency();
        
        CI()->db->select('code');
        CI()->db->where('id', $currency_id);
        $query  = CI()->db->get('tbl_currencies');
        $result = $query->row();
        return $result->code;
    }
}

if (!function_exists('getPriceSymbolById')) {
    function getPriceSymbolById($currency_id = 0)
    {
        if (empty($currency_id))
            $currency_id = getCurrency();
        
        CI()->db->select('symbol');
        CI()->db->where('id', $currency_id);
        $query  = CI()->db->get('tbl_currencies');
        $result = $query->row();
        return $result->symbol;
    }
}

if (!function_exists('getPriceSymbolByCode')) {
    function getPriceSymbolByCode($code)
    {
        CI()->db->select('symbol');
        CI()->db->where('code', $code);
        $query  = CI()->db->get('tbl_currencies');
        $result = $query->row();
        return $result->symbol;
    }
}

if (!function_exists('getFormatedPriceByCode')) {
    function getFormatedPriceByCode($code, $amount)
    {
        $symbol = getPriceSymbolByCode($code);
        return $symbol . '' . number_format($amount, 2);
    }
}

if (!function_exists('get_total_records')) {
    function get_total_records($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        return $count = $query->num_rows();
    }
}

if (!function_exists('get_record')) {
    function get_record($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
        
    }
}

if (!function_exists('get_records')) {
    function get_records($table, $whereArr = array(), $orderBy = '', $order = 'DESC')
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        if (!empty($orderBy))
            CI()->db->order_by($orderBy, $order);
        
        CI()->db->from($table);
        $query = CI()->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
}

if (!function_exists('get_current_page_records')) {
    function get_current_page_records($table, $limit, $start, $whereArr = array(), $orderBy = '', $order = 'DESC')
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        if (!empty($orderBy))
            CI()->db->order_by($orderBy, $order);
        
        if (!empty($limit))
            CI()->db->limit($limit, $start);
        
        CI()->db->from($table);
        $query = CI()->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
}

if (!function_exists('Jpagination')) {
    function Jpagination($total_rows, $per_page, $limit)
    {
        $config['use_page_numbers']   = TRUE;
        $config['num_links']          = 3;
        $config['full_tag_open']      = '<ul class="pagination">';
        $config['full_tag_close']     = '</ul>';
        $config['first_link']         = 'First';
        $config['first_tag_open']     = '<li>';
        $config['first_tag_close']    = '</li>';
        $config['last_link']          = 'Last';
        $config['last_tag_open']      = '<li>';
        $config['last_tag_close']     = '</li>';
        $config['num_tag_open']       = '<li>';
        $config['num_tag_close']      = '</li>';
        $config['next_link']          = '<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open']      = '<li>';
        $config['next_tag_close']     = '</li>';
        $config['prev_link']          = '<i class="fa fa-angle-double-left"></i>';
        $config['prev_tag_open']      = '<li>';
        $config['prev_tag_close']     = '</li>';
        $config['cur_tag_open']       = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close']      = '</a></li>';
        $config['reuse_query_string'] = true;
        CI()->pagination->initialize($config);
        $config               = array();
        $config["base_url"]   = base_url() . CI()->uri->segment(1);
        $config["total_rows"] = $total_rows;
        $config["per_page"]   = $per_page;
        CI()->pagination->initialize($config);
        return CI()->pagination->create_links();
    }
}

if (!function_exists('getAdministratorEmail')) {
    function getAdministratorEmail()
    {
        
        CI()->db->where('id', 1);
        $query = CI()->db->get('tbl_users');
        $count = $query->num_rows();
        if ($count > 0) {
            $userData = $query->row();
            return $userData->email;
        } else {
            return '';
        }
        
    }
}

if (!function_exists('getLoginUserRole')) {
    function getLoginUserRole()
    {
        $user_id = CI()->session->userdata('userID');
        CI()->db->where('id', $user_id);
        $query = CI()->db->get('tbl_users');
        $count = $query->num_rows();
        if ($count > 0) {
            $userData = $query->row();
            return $userData->role;
        } else {
            return '';
        }
        
    }
}

if (!function_exists('getUserRole')) {
    function getUserRole($id)
    {
        CI()->db->where('id', $id);
        $query = CI()->db->get('tbl_users');
        $count = $query->num_rows();
        if ($count > 0) {
            $userData = $query->row();
            return $userData->role;
        } else {
            return '';
        }
        
    }
}

if (!function_exists('getRoleLabel')) {
    function getRoleLabel($id)
    {
        CI()->db->where('id', $id);
        $query = CI()->db->get('tbl_role');
        $count = $query->num_rows();
        if ($count > 0) {
            $userData = $query->row();
            return $userData->name;
        } else {
            return '';
        }
        
    }
}

if (!function_exists('getCustomerEmail')) {
    function getCustomerEmail($id = "")
    {
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->email;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('getCustomerContact')) {
    function getCustomerContact($id = "")
    {
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->mobile;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('getLoginUserPassword')) {
    function getLoginUserPassword()
    {
        $id = CI()->session->userdata('userID');
        if (!empty($id)) {
            $password = md5(CI()->input->post('old_password'));
            CI()->db->where('id', $id);
            CI()->db->where('password', $password);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}

if (!function_exists('getRequestStatus')) {
    function getRequestStatus($id = "")
    {
        if ($id == 0) {
            $id = CI()->session->userdata('userID');
        }
        if (!empty($id)) {
            CI()->db->select('service_status');
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_service_provider_request');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->service_status;
            }
        }
    }
}

if (!function_exists('chckValidOtp')) {
    function chckValidOtp($otp = "")
    {
        $id = 0;
        if (!empty($otp)) {
            $mobile = CI()->session->userdata('userphone');
            CI()->db->where('otp', $otp);
            CI()->db->where('mobile', $mobile);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->id;
            } else {
                return false;
            }
        }
    }
}

if (!function_exists('getCustomerIdByMobile')) {
    function getCustomerIdByMobile($mobile = "")
    {
        
        if (!empty($mobile)) {
            CI()->db->where('mobile', $mobile);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->id;
            } else {
                return 0;
            }
        }
    }
}

if (!function_exists('getCustomerName')) {
    function getCustomerName($id = "")
    {
        if ($id == 0) {
            $id = CI()->session->userdata('userID');
        }
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->username;
            } else {
                return 'unknown user';
            }
        }
    }
}

if (!function_exists('getCustomerEmail')) {
    function getCustomerEmail($id = "")
    {
        if ($id == 0) {
            $id = CI()->session->userdata('userID');
        }
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->email;
            } else {
                return '';
            }
        }
    }
}


if (!function_exists('getCustomerPicture')) {
    function getCustomerPicture($id = 0)
    {
        if ($id == 0) {
            $id = CI()->session->userdata('userID');
        }
        CI()->db->select('featured_img');
        CI()->db->where('id', $id);
        CI()->db->where('featured_img!=""');
        $query    = CI()->db->get('tbl_users');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $featured_img = $query->row('featured_img');
            return '<img src="' . base_url($featured_img) . '" class="img-responsive">';
        } else {
            return '<img src="' . base_url('skin/front/images/no_image.jpg') . '" class="img-responsive">';
        }
    }
}

if (!function_exists('getCurrencyOptions')) {
    function getCurrencyOptions($table, $label)
    {
        $options = array(
            '' => $label
        );
        if ($table) {
            $query  = CI()->db->get($table);
            $skills = $query->result();
            foreach ($skills as $value) {
                $options[$value->id] = $value->name . ' ( ' . $value->symbol . ' )';
            }
        }
        return $options;
    }
}


if (!function_exists('calculateTime')) {
    function calculateTime($start, $end)
    {
        $t1    = strtotime($end);
        $t2    = strtotime($start);
        $diff  = $t1 - $t2;
        $hours = $diff / (60 * 60);
        return $hours;
    }
}

if (!function_exists('calculateTimeInMinute')) {
    function calculateTimeInMinute($start, $end)
    {
        $t1     = strtotime($end);
        $t2     = strtotime($start);
        $diff   = $t1 - $t2;
        $minues = $diff / 60;
        return floor($minues);
    }
}

if (!function_exists('shownExpendedTime')) {
    function shownExpendedTime($start, $end)
    {
        $t1 = strtotime($end);
        $t2 = strtotime($start);
        
        $dateDiff = intval(($t1 - $t2) / 60);
        $hours    = intval($dateDiff / 60);
        $minutes  = $dateDiff % 60;
        $showTime = '';
        $showTime .= !empty($hours) ? $hours . ' Hours, ' : '';
        $showTime .= $minutes . ' Minutes';
        
        return $showTime;
    }
}

if (!function_exists('shownAdditionalTime')) {
    function shownAdditionalTime($start, $end, $alloted_time = 60)
    {
        $t1 = strtotime($end);
        $t2 = strtotime($start);
        
        $dateDiff = intval(($t1 - $t2) / 60);
        $dateDiff = $dateDiff - $alloted_time;
        $hours    = intval($dateDiff / 60);
        $minutes  = $dateDiff % 60;
        $showTime = '';
        $showTime .= !empty($hours) ? $hours . ' Hours, ' : '';
        $showTime .= $minutes . ' Minutes';
        
        return $showTime;
    }
}

if(!function_exists('getOptions')){
    function getOptions($table="", $type="", $status=0){
        $options = array(''=>$type);
        if($table){
            if($status==1){ 
              CI()->db->where('status',1); 
            }
            $query = CI()->db->get($table);
            $records =  $query->result();
            foreach ($records as $value) {
                $options[$value->id] = $value->name;
            }
        }
        return $options;
    }
}

if (!function_exists('getOptionsFrom')) {
    function getOptionsFrom($table, $column, $label)
    {
        $options = array(
            '' => $label
        );
        if ($table) {
            $query  = CI()->db->select($column)->get($table);
            $record = $query->result();
            foreach ($record as $rec) {
                $options[$rec->$column] = $rec->$column;
            }
        }
        return $options;
    }
}

if (!function_exists('send_email')) {
    function send_email($to_email, $subject, $message = '', $from_email = '', $from_name = '')
    {
        $from_email = !empty($from_email) ? $from_email : getSetting('smtp_user');
        $from_name  = !empty($from_name) ? $from_name : getSetting('smtp_from');
        CI()->load->library('email');
        $smtp_host              = getSetting('smtp_host');
        $smtp_port              = getSetting('smtp_port');
        $smtp_user              = getSetting('smtp_user');
        $smtp_pass              = getSetting('smtp_pass');
        $config                 = array(
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'priority' => '1',
            'newline' => '\r\n'
        );
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $smtp_host;
        $config['smtp_port']    = $smtp_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $smtp_user;
        $config['smtp_pass']    = $smtp_pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        
        CI()->email->initialize($config);
        CI()->email->from($smtp_user, $from_name);
        CI()->email->to($to_email);
        CI()->email->subject($subject);
        CI()->email->message($message);
        if (!CI()->email->send()) {
            $from    = $from_name . "<" . $from_email . ">";
            $headers = "From: $from\r\n";
            $headers .= "MIME-Version: 1.0\r\n" . "Content-Type: text/html;";
            $success = @mail($to_email, $subject, $message, $headers);
        }
    }
}

if (!function_exists('email_compose')) {
    function email_compose($email_template, $templateTags)
    {
        $templateContents = file_get_contents(APPPATH . '/controllers/email-templates/' . $email_template);
        return $message = strtr($templateContents, $templateTags);
    }
}

if (!function_exists('getNotificationHtml')) {
    function getNotificationHtml()
    {
        $CI =& get_instance();
        if ($CI->session->flashdata('notification'))
        {
            $notificationData = $CI->session->flashdata('notification');
            
            if ($notificationData['error'] == 0)
            {
                ?>
                <div class="alert alert-success"><a href="#" data-dismiss="alert" aria-label="close" title="close" class="close">×</a>
                    <?php echo $notificationData['message']; ?>
                </div>
                <?php
            }
            elseif ($notificationData['error'] == 1)
            {
                ?>
                <div class="alert alert-danger"><a href="#" data-dismiss="alert" aria-label="close" title="close" class="close">×</a>
                    <?php echo $notificationData['message'];?>
                </div>
                <?php
            }
            elseif (validation_errors())
            {
                echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
            }
        }
    }
}

if (!function_exists('validateZipCodeByCountry')) {
    function validateZipCodeByCountry($country_code, $zip_code)
    {
        if (!empty($country_code) && !empty($zip_code)) {
            $ZIPREG = array(
                "GB" => "GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4}",
                "JE" => "JE\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
                "GG" => "GY\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
                "IM" => "IM\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
                "US" => "^\d{5}([\-]?\d{4})?$",
                "CA" => "[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d",
                "DE" => "^\d{5}?$",
                "JP" => "\d{3}-\d{4}",
                "FR" => "\d{2}[ ]?\d{3}",
                "AU" => "^\d{4}?$",
                "IT" => "^\d{5}?$",
                "CH" => "^\d{4}?$",
                "AT" => "^\d{4}?$",
                "ES" => "^\d{5}?$",
                "NL" => "\d{4}[ ]?[A-Z]{2}",
                "BE" => "^\d{4}?$",
                "DK" => "^\d{4}?$",
                "SE" => "\d{3}[ ]?\d{2}",
                "NO" => "^\d{4}?$",
                "BR" => "\d{5}[\-]?\d{3}",
                "PT" => "\d{4}([\-]\d{3})?",
                "FI" => "^\d{5}?$",
                "AX" => "22\d{3}",
                "KR" => "\d{3}[\-]\d{3}",
                "CN" => "^\d{6}?$",
                "TW" => "\d{3}(\d{2})?",
                "SG" => "^\d{6}?$",
                "DZ" => "^\d{5}?$",
                "AD" => "AD\d{3}",
                "AR" => "([A-HJ-NP-Z])?\d{4}([A-Z]{3})?",
                "AM" => "(37)?\d{4}",
                "AZ" => "^\d{4}?$",
                "BH" => "((1[0-2]|[2-9])\d{2})?",
                "BD" => "^\d{4}?$",
                "BB" => "(BB\d{5})?",
                "BY" => "^\d{6}?$",
                "BM" => "[A-Z]{2}[ ]?[A-Z0-9]{2}",
                "BA" => "^\d{5}?$",
                "IO" => "BBND 1ZZ",
                "BN" => "[A-Z]{2}[ ]?\d{4}",
                "BG" => "^\d{4}?$",
                "KH" => "^\d{5}?$",
                "CV" => "^\d{4}?$",
                "CL" => "\d{7}",
                "CR" => "\d{4,5}|\d{3}-\d{4}",
                "HR" => "^\d{5}?$",
                "CY" => "^\d{4}?$",
                "CZ" => "\d{3}[ ]?\d{2}",
                "DO" => "^\d{5}?$",
                "EC" => "([A-Z]\d{4}[A-Z]|(?:[A-Z]{2})?\d{6})?",
                "EG" => "^\d{5}?$",
                "EE" => "^\d{5}?$",
                "FO" => "^\d{3}?$",
                "GE" => "^\d{4}?$",
                "GR" => "\d{3}[ ]?\d{2}",
                "GL" => "39\d{2}",
                "GT" => "^\d{5}?$",
                "HT" => "^\d{4}?$",
                "HN" => "(?:\d{5})?",
                "HU" => "^\d{4}?$",
                "IS" => "^\d{3}?$",
                "IN" => "^\d{6}?$",
                "ID" => "^\d{5}?$",
                "IL" => "^\d{5}?$",
                "JO" => "^\d{5}?$",
                "KZ" => "^\d{6}?$",
                "KE" => "^\d{5}?$",
                "KW" => "^\d{5}?$",
                "LA" => "^\d{5}?$",
                "LV" => "^\d{4}?$",
                "LB" => "(\d{4}([ ]?\d{4})?)?",
                "LI" => "(948[5-9])|(949[0-7])",
                "LT" => "^\d{5}?$",
                "LU" => "^\d{4}?$",
                "MK" => "^\d{4}?$",
                "MY" => "^\d{5}?$",
                "MV" => "^\d{5}?$",
                "MT" => "[A-Z]{3}[ ]?\d{2,4}",
                "MU" => "(\d{3}[A-Z]{2}\d{3})?",
                "MX" => "^\d{5}?$",
                "MD" => "^\d{4}?$",
                "MC" => "980\d{2}",
                "MA" => "^\d{5}?$",
                "NP" => "^\d{5}?$",
                "NZ" => "^\d{4}?$",
                "NI" => "((\d{4}-)?\d{3}-\d{3}(-\d{1})?)?",
                "NG" => "(\d{6})?",
                "OM" => "(PC )?\d{3}",
                "PK" => "^\d{5}?$",
                "PY" => "^\d{4}?$",
                "PH" => "^\d{4}?$",
                "PL" => "\d{2}-\d{3}",
                "PR" => "00[679]\d{2}([ \-]\d{4})?",
                "RO" => "^\d{6}?$",
                "RU" => "^\d{6}?$",
                "SM" => "4789\d",
                "SA" => "^\d{5}?$",
                "SN" => "^\d{5}?$",
                "SK" => "\d{3}[ ]?\d{2}",
                "SI" => "^\d{4}?$",
                "ZA" => "^\d{4}?$",
                "LK" => "^\d{5}?$",
                "TJ" => "^\d{6}?$",
                "TH" => "^\d{5}?$",
                "TN" => "^\d{4}?$",
                "TR" => "^\d{5}?$",
                "TM" => "^\d{6}?$",
                "UA" => "^\d{5}?$",
                "UY" => "^\d{5}?$",
                "UZ" => "^\d{6}?$",
                "VA" => "00120",
                "VE" => "^\d{4}?$",
                "ZM" => "^\d{5}?$",
                "AS" => "96799",
                "CC" => "6799",
                "CK" => "^\d{4}?$",
                "RS" => "^\d{6}?$",
                "ME" => "8\d{4}",
                "CS" => "^\d{5}?$",
                "YU" => "^\d{5}?$",
                "CX" => "6798",
                "ET" => "^\d{4}?$",
                "FK" => "FIQQ 1ZZ",
                "NF" => "2899",
                "FM" => "(9694[1-4])([ \-]\d{4})?",
                "GF" => "9[78]3\d{2}",
                "GN" => "^\d{3}?$",
                "GP" => "9[78][01]\d{2}",
                "GS" => "SIQQ 1ZZ",
                "GU" => "969[123]\d([ \-]\d{4})?",
                "GW" => "^\d{4}?$",
                "HM" => "^\d{4}?$",
                "IQ" => "^\d{5}?$",
                "KG" => "^\d{6}?$",
                "LR" => "^\d{4}?$",
                "LS" => "^\d{3}?$",
                "MG" => "^\d{3}?$",
                "MH" => "969[67]\d([ \-]\d{4})?",
                "MN" => "^\d{6}?$",
                "MP" => "9695[012]([ \-]\d{4})?",
                "MQ" => "9[78]2\d{2}",
                "NC" => "988\d{2}",
                "NE" => "^\d{4}?$",
                "VI" => "008(([0-4]\d)|(5[01]))([ \-]\d{4})?",
                "PF" => "987\d{2}",
                "PG" => "^\d{3}?$",
                "PM" => "9[78]5\d{2}",
                "PN" => "PCRN 1ZZ",
                "PW" => "96940",
                "RE" => "9[78]4\d{2}",
                "SH" => "(ASCN|STHL) 1ZZ",
                "SJ" => "^\d{4}?$",
                "SO" => "^\d{5}?$",
                "SZ" => "[HLMS]\d{3}",
                "TC" => "TKCA 1ZZ",
                "WF" => "986\d{2}",
                "XK" => "^\d{5}?$",
                "YT" => "976\d{2}"
            );
            
            if (!empty($country_code) && !empty($zip_code)) {
                $country_code = strtoupper($country_code);
                if ($ZIPREG[$country_code]) {
                    if (!preg_match("/" . $ZIPREG[$country_code] . "/i", $zip_code)) {
                        return 'fail';
                    } else {
                        return 'success';
                    }
                } else {
                    return 'country_unmatch';
                }
            } else {
                return 'data_missing';
            }
        }
    }
}

if (!function_exists('do_upload')) {
    function do_upload($folder, $filename = 'featured_img')
    {
        
        $config['upload_path']   = './upload/' . $folder . '/'; // Added forward slash 
        $config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?
        
        //$config['max_size']     = '1000';
        //$config['max_width']     = '1024';
        //$config['max_height'] = '768';
        
        $uploaddir = 'upload/' . $folder . '/';
        
        if (!is_dir($uploaddir)) {
            mkdir($uploaddir);
        }
        $config['overwrite'] = TRUE;
        CI()->load->library('upload', $config);
        CI()->upload->initialize($config);
        $input = $filename; // on view the name <input type="file" name="userfile" size="20"/>
        
        if (!CI()->upload->do_upload($filename)) {
            CI()->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
        } else {
            $upload_data = CI()->upload->data();
            $file_name   = $upload_data['file_name'];
            $file_path   = $uploaddir . $file_name;
            return $file_path;
        }
    }
}

if (!function_exists('exportEtpCsv')) {
    function exportEtpCsv($data = array(), $filename = 'leads')
    {
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"" . $filename . ".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $csvfile   = fopen('php://output', 'w');
        $first_row = array_shift($data);
        //prepare the titles
        $title     = array();
        $firstrow  = array();
        foreach ($first_row as $key => $value) {
            $title[]        = $key;
            $firstrow[$key] = $value;
        }
        //Write heading titles to the csv
        fputcsv($csvfile, $title);
        fputcsv($csvfile, $firstrow);
        foreach ($data as $row) {
            fputcsv($csvfile, $row);
        }
        fclose($csvfile);
    }
}

if (!function_exists('getFilterDaysOptions')) {
    function getFilterDaysOptions($label = '')
    {
        $label      = !empty($label) ? $label : 'All Days';
        $optionsArr = array(
            '' => $label,
            'today' => 'Today',
            'tomorrow' => 'Tomorrow'
        );
        return $optionsArr;
    }
}

if (!function_exists('getFilterLeadsOptions')) {
    function getFilterLeadsOptions($label = '')
    {
        $label      = !empty($label) ? $label : 'All Leads';
        $optionsArr = array(
            '' => $label,
            'mine' => 'Mine',
            'other' => 'Others'
        );
        return $optionsArr;
    }
}

if (!function_exists('isWeekend')) {
    function isWeekend($date)
    {
        $dayname = date('l', strtotime($date));
        $dayname = strtolower($dayname);
        
        
        if (($dayname == "saturday") || ($dayname == "sunday")) {
            return 1;
        } else {
            $service_time = date('H:i:s', strtotime($date));
            $start_time   = '10:00';
            $end_time     = '19:00';
            if ((strtotime($service_time) < strtotime($start_time)) || (strtotime($service_time) > strtotime($end_time))) {
                return 1;
            } else {
                return 0;
            }
        }
        
        
    }
}

if (!function_exists('durationOptions')) {
    function durationOptions($type = '')
    {
        if (!empty($type) && $type == 'minute') {
            $optionArr = array(
                '15' => '15 Minutes',
                '30' => '30 Minutes',
                '45' => '45 Minutes',
                '60' => '1 Hour',
                '75' => '1 Hour 15 Minutes',
                '90' => '1 Hour 30 Minutes',
                '105' => '1 Hour 45 Minutes',
                '120' => '2 Hours'
            );
        } else {
            $optionArr = array(
                '1' => '1 Hour',
                '2' => '2 Hours',
                '3' => '3 Hours',
                '4' => '4 Hours',
                '5' => '5 Hours',
                '6' => '6 Hours',
                '7' => '7 Hours',
                '8' => '8 Hours',
                '9' => '9 Hours',
                '10' => '10 Hours',
                '11' => '11 Hours',
                '12' => '12 Hours'
            );
        }
        
        return $optionArr;
        
    }
}

if (!function_exists('getDuration')) {
    function getDuration($time, $type = 'minute')
    {
        if (!empty($time) && !empty($type) && $type == 'minute') {
            $optionArr = array(
                '15' => '15 Minutes',
                '30' => '30 Minutes',
                '45' => '45 Minutes',
                '60' => '1 Hour',
                '75' => '1 Hour 15 Minutes',
                '90' => '1 Hour 30 Minutes',
                '105' => '1 Hour 45 Minutes',
                '120' => '2 Hours'
            );
            return array_key_exists($time, $optionArr) ? $optionArr[$time] : '';
            
        } elseif (!empty($time) && !empty($type) && $type == 'hours') {
            $optionArr = array(
                '1' => '1 Hour',
                '2' => '2 Hours',
                '3' => '3 Hours',
                '4' => '4 Hours',
                '5' => '5 Hours',
                '6' => '6 Hours',
                '7' => '7 Hours',
                '8' => '8 Hours',
                '9' => '9 Hours',
                '10' => '10 Hours',
                '11' => '11 Hours',
                '12' => '12 Hours'
            );
            return array_key_exists($time, $optionArr) ? $optionArr[$time] : '';
        } else {
            return '';
        }
        
    }
}

if (!function_exists('calculate_additional_estimate')) {
    function calculate_additional_estimate($id)
    {
        $responseArr   = array();
        $bookingData   = getServiceBookingDetailById($id);
        $operation_row = getServiceOperationDetailByServiceId($id);
        
        $is_ended   = $operation_row->is_service_ended;
        $start_time = $operation_row->service_start_time;
        $end_time   = $operation_row->service_end_time;
        
        $allocated_time  = getAllocatedTime($id);
        $additional_time = calculateAdditionalTime($id, $start_time, $end_time);
        
        $responseArr['expended_time_readable']   = !empty($is_ended) ? shownExpendedTime($start_time, $end_time) : 0;
        $responseArr['additional_time_readable'] = !empty($is_ended) ? shownAdditionalTime($start_time, $end_time, 1) : 0;
        
        $responseArr['time_spent']      = calculateTimeSpent($start_time, $end_time);
        $responseArr['additional_time'] = $additional_time;
        
        $responseArr['allocated_time'] = $allocated_time;
        
        $additional_operator = !empty($bookingData->additional_operator) ? $bookingData->additional_operator : 0;
        if (!empty($additional_operator)) {
            $additional_amount = calculateAdditionalAmount4AO($id, $start_time, $end_time);
        } else {
            $additional_amount = calculateAdditionalAmount($id, $start_time, $end_time);
        }
        $responseArr['additional_amount']   = $additional_amount;
        $responseArr['additional_tax']      = $additional_tax = calculateTax($id, $additional_amount);
        $responseArr['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        
        if (!empty($is_ended)) {
            return $responseArr;
        } else {
            return array();
        }
        
    }
}

if (!function_exists('getAllocatedTime')) {
    function getAllocatedTime($order_id)
    {
        $bookingData             = getServiceBookingDetailById($order_id);
        $providerDate            = getSPSettingsByBookingId($order_id);
        $sevice_time             = !empty($bookingData->booking_datetime) ? $bookingData->booking_datetime : '';
        $is_nonworking_time      = isWeekend($sevice_time);
        $sevice_duration         = !empty($providerDate->service_duration) ? $providerDate->service_duration : '';
        $sevice_weekend_duration = !empty($providerDate->weekend_duration) ? $providerDate->weekend_duration : '';
        $allocted_time           = !empty($is_nonworking_time) ? $sevice_weekend_duration : $sevice_duration;
        return $allocted_time;
    }
}

if (!function_exists('calculateTimeSpent')) {
    function calculateTimeSpent($start_time, $end_time)
    {
        $spent_time = calculateTimeInMinute($start_time, $end_time);
        return $spent_time;
    }
}

if (!function_exists('calculateAdditionalTime')) {
    function calculateAdditionalTime($order_id, $start_time, $end_time)
    {
        $allocated_time  = getAllocatedTime($order_id);
        $calculated_time = calculateTimeInMinute($start_time, $end_time);
        $additional_time = $calculated_time - $allocated_time;
        return $additional_time;
    }
}

if (!function_exists('calculateAdditionalAmount')) {
    function calculateAdditionalAmount($order_id, $start_time, $end_time)
    {
        $allocated_time = getAllocatedTime($order_id);
        $bookingData    = getServiceBookingDetailById($order_id);
        $providerDate   = getSPSettingsByBookingId($order_id);
        
        $calculated_time = calculateTimeInMinute($start_time, $end_time);
        $additional_time = $calculated_time - $allocated_time;
        $sevice_time     = !empty($bookingData->booking_datetime) ? $bookingData->booking_datetime : '';
        
        if (isWeekend($sevice_time)) {
            $charges_after     = !empty($providerDate->weekend_charges_after) ? $providerDate->weekend_charges_after : '';
            $duration_after    = !empty($providerDate->weekend_duration_after) ? $providerDate->weekend_duration_after : '';
            $additional_charge = !empty($providerDate->weekend_additional_charge) ? $providerDate->weekend_additional_charge : '';
            $additional_hours  = !empty($providerDate->weekend_additional_hours) ? $providerDate->weekend_additional_hours : '';
            $hourly_charge     = !empty($providerDate->weekend_hourly_charge) ? $providerDate->weekend_hourly_charge : '';
        } else {
            $charges_after     = !empty($providerDate->service_charges_after) ? $providerDate->service_charges_after : '';
            $duration_after    = !empty($providerDate->service_duration_after) ? $providerDate->service_duration_after : '';
            $additional_charge = !empty($providerDate->service_additional_charge) ? $providerDate->service_additional_charge : '';
            $additional_hours  = !empty($providerDate->service_additional_hours) ? $providerDate->service_additional_hours : '';
            $hourly_charge     = !empty($providerDate->service_hourly_charge) ? $providerDate->service_hourly_charge : '';
        }
        
        $additional_time_hours = $additional_time / 60;
        $additional_time_hours = number_format($additional_time_hours, 2);
        $additionalHours       = $duration_after + $additional_hours;
        
        $cost = 0;
        if ($additional_time_hours <= $duration_after) {
            $duration_after_minutes       = $duration_after * 60;
            $duration_after_minutes_slots = $duration_after_minutes / 15;
            $duration_after_slot_price    = $charges_after / $duration_after_minutes_slots;
            
            $extra_time_additional         = $additional_time_hours;
            $extra_time_additional_minutes = $extra_time_additional * 60;
            $extra_time_additional_minutes = round($extra_time_additional_minutes);
            $used_slots                    = $extra_time_additional_minutes / 15;
            $used_slots                    = round($used_slots);
            
            $extra_time_cost = $used_slots * $duration_after_slot_price;
            $cost            = $cost + $extra_time_cost;
        } else if ($additional_time_hours > $duration_after && $additional_time_hours <= $additionalHours) {
            
            $additional_minutes       = $additional_hours * 60;
            $additional_minutes_slots = $additional_minutes / 15;
            $slot_price               = $additional_charge / $additional_minutes_slots;
            
            $cost = $charges_after;
            
            $extra_time_additional         = $additional_time_hours - $duration_after;
            $extra_time_additional_minutes = $extra_time_additional * 60;
            $extra_time_additional_minutes = round($extra_time_additional_minutes);
            $used_slots                    = $extra_time_additional_minutes / 15;
            $used_slots                    = round($used_slots);
            
            $extra_time_cost = $used_slots * $slot_price;
            $cost            = $cost + $extra_time_cost;
        } elseif ($additional_time_hours > $additionalHours) {
            $cost            = $charges_after + $additional_charge;
            $extra_time      = $additional_time_hours - $additionalHours;
            $extra_time_cost = $extra_time * $hourly_charge;
            $cost            = $cost + $extra_time_cost;
        }
        
        return round($cost);
    }
}

if (!function_exists('calculateAdditionalAmount4AO')) {
    function calculateAdditionalAmount4AO($order_id, $start_time, $end_time)
    {
        $allocated_time = getAllocatedTime($order_id);
        $bookingData    = getServiceBookingDetailById($order_id);
        $providerDate   = getSPSettingsByBookingId($order_id);
        
        $calculated_time = calculateTimeInMinute($start_time, $end_time);
        $additional_time = $calculated_time - $allocated_time;
        $sevice_time     = !empty($bookingData->booking_datetime) ? $bookingData->booking_datetime : '';
        
        if (isWeekend($sevice_time)) {
            $hourly_charge = !empty($providerDate->weekend_additional_worker) ? $providerDate->weekend_additional_worker : '';
        } else {
            $hourly_charge = !empty($providerDate->service_additional_worker) ? $providerDate->service_additional_worker : '';
        }
        $cost = 0;
        
        $additional_time_hours = $additional_time / 60;
        $additional_time_hours = number_format($additional_time_hours, 2);
        $cost                  = $additional_time_hours * $hourly_charge;
        return round($cost);
    }
}


if (!function_exists('calculateTax')) {
    function calculateTax($order_id, $amount)
    {
        $bookingData      = getServiceBookingDetailById($order_id);
        $tax              = !empty($bookingData->tax) ? $bookingData->tax : 0;
        $other_tax        = !empty($bookingData->other_tax) ? $bookingData->other_tax : 0;
        $base_tax_amount  = !empty($tax) ? ($tax / 100) * $amount : 0;
        $other_tax_amount = !empty($other_tax) ? ($other_tax / 100) * $amount : 0;
        
        $tax_amount = $base_tax_amount + $other_tax_amount;
        
        return $tax_amount;
    }
}

if (!function_exists('shippingAddress')) {
    function shippingAddress($order_id)
    {
        $shippingData = get_record('tbl_shipping_information', array(
            'order_id' => $order_id
        ));
        
        $shipping_address = '';
        $shipping_address .= '<p><strong>' . $shippingData->fname . ' ' . $shippingData->lname . '</strong></p>';
        $shipping_address .= '<p><strong>Email : </strong>' . $shippingData->email . '</p>';
        $shipping_address .= '<p><strong>Contact : </strong>' . $shippingData->mobile . '</p><br>';
        $shipping_address .= '<p><strong>Address</strong></p>';
        //$shipping_address.= '<p>'.$shippingData->site_location.'<br>';
        $shipping_address .= !empty($shippingData->aptno) ? $shippingData->aptno . ',' : '' . ' Street : ' . $shippingData->street_number . ',' . $shippingData->street_name . '<br>';
        
        $st = streetTypeOption();
        $dr = directionOption();
        $shipping_address .= !empty($shippingData->direction) ? ', Direction: ' . $dr[$shippingData->direction] : '';
        $shipping_address .= !empty($shippingData->street_type) ? ' Street Type: ' . $st[$shippingData->street_type] : '';
        $shipping_address .= $shippingData->city . ', ' . getStateName($shippingData->state) . ', ' . getCountryName($shippingData->country) . '<br>';
        $shipping_address .= $shippingData->postal_code . ', Fax : ' . $shippingData->fax . '<br>';
        $shipping_address .= '</p>';
        
        return $shipping_address;
    }
}

if (!function_exists('send_order_notification')) {
    function send_order_notification($oid)
    {
        CI()->load->model('login_model');
        $msg           = '';
        $orderData     = CI()->login_model->getDataCollectionByID('tbl_products_orders', $oid);
        $user_id       = !empty($orderData['user_id']) ? $orderData['user_id'] : 0;
        $customer_data = CI()->login_model->getDataCollectionByID('tbl_users', $user_id);
        
        $customer_data_html = '<p>';
        $customer_data_html .= '<strong>' . getCustomerName($user_id) . '</strong><br>';
        $customer_data_html .= '<strong>Email : </strong>' . getCustomerEmail($user_id) . '<br>';
        $customer_data_html .= '<strong>Contact : </strong>' . getCustomerContact($user_id) . '<br>';
        $customer_data_html .= '</p>';
        
        $shippingData = get_record('tbl_shipping_information', array(
            'order_id' => $oid
        ));
        
        $shipping_address = '';
        /* $shipping_address.= '<p><strong>'.$shippingData->fname.' '.$shippingData->lname.'</strong></p>';
        $shipping_address.= '<p><strong>Email : </strong>'.$shippingData->email.'</p>';
        $shipping_address.= '<p><strong>Contact : </strong>'.$shippingData->mobile.'</p>';
        $shipping_address.= '<p><strong>Address</strong></p>';*/
        $shipping_address .= '<p>' . $shippingData->site_location . '<br>';
        $shipping_address .= !empty($shippingData->aptno) ? $shippingData->aptno . ',' : '' . ' Street : ' . $shippingData->street_number . ',' . $shippingData->street_name . '<br>';
        
        $st        = streetTypeOption();
        $dr        = directionOption();
        $direction = !empty($shippingData->direction) ? ', Direction: ' . $dr[$shippingData->direction] : '';
        
        $shipping_address .= ' Street Type: ' . $st[$shippingData->street_type] . $direction . '<br>';
        $shipping_address .= $shippingData->city . ', ' . getStateName($shippingData->state) . ', ' . getCountryName($shippingData->country) . '<br>';
        $shipping_address .= $shippingData->postal_code . ', Fax : ' . $shippingData->fax . '<br>';
        $shipping_address .= '</p>';
        
        $pm             = paymentMethodOption();
        $payment_method = $orderData['payment_method'];
        $paymentMethod  = $pm[$payment_method];
        
        $sm              = shippingMethodOption();
        $shipping_method = $orderData['shipping_method'];
        $shippingMethod  = $sm[$shipping_method];
        
        
        $orderedItemsArr = get_records('tbl_products_orders_items', array(
            'order_id' => $oid
        ));
        $orderedItem     = '<table width="100%" border="1" style="border-collapse: collapse;" bordercolor="#CCC" cellpadding="5" cellspacing="5">';
        $orderedItem .= '<tr>
        <th valign="top" align="left" bgcolor="#CCCCCC">Name</th>
        <th valign="top" align="left" bgcolor="#CCCCCC">Qty</th>
        <th valign="top" align="left" bgcolor="#CCCCCC">Price</th>
        <th valign="top" align="left" bgcolor="#CCCCCC">Sub Total</th>
        </tr>';
        if (!empty($orderedItemsArr)) {
            foreach ($orderedItemsArr as $item) {
                $orderedItem .= '<tr>';
                $orderedItem .= '<td valign="top" align="left">' . getProductName($item->product_id) . '</td>';
                $orderedItem .= '<td valign="top" align="left">' . $item->qty . '</td>';
                $orderedItem .= '<td valign="top" align="left">' . getPriceFormate($item->unit_price) . '</td>';
                $orderedItem .= '<td valign="top" align="left">' . getPriceFormate($item->prices) . '</td>';
                $orderedItem .= '</tr>';
            }
        }
        $orderedItem .= '</table>';
        
        $templateTags = array(
            '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
            '{{site_name}}' => 'firstchoice.com',
            '{{site_url}}' => base_url(),
            '{{team_name}}' => 'firstchoice',
            '{{user_name}}' => getCustomerName($user_id),
            '{{customer_data}}' => $customer_data_html,
            '{{shipping_address}}' => $shipping_address,
            '{{ordered_items}}' => $orderedItem,
            '{{order_id}}' => $oid,
            '{{ordered_time}}' => $orderData['orderdata'],
            '{{order_subtotal}}' => getPriceFormate($orderData['subtotal']),
            '{{order_shipping}}' => $shippingMethod,
            '{{order_shipping_amt}}' => getPriceFormate($orderData['shipping_amount']),
            '{{order_discount}}' => getPriceFormate($orderData['discount']),
            '{{order_amount}}' => getPriceFormate($orderData['total']),
            '{{payment_method}}' => $paymentMethod,
            '{{year}}' => date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com'
        );
        
        $email_template = 'order-reciept.html';
        $email          = getCustomerEmail($user_id);
        $message        = email_compose($email_template, $templateTags);
        send_email($email, 'Order reciept (#' . $oid . ')', $message, 'support@markupdesigns.info', 'First Choice');
        
        $email_template = 'new-order.html';
        $admin_email    = getAdministratorEmail();
        $message        = email_compose($email_template, $templateTags);
        send_email($admin_email, 'New Order #' . $oid, $message, 'support@markupdesigns.info', 'First Choice');
        
        $mobile = $customer_data['mobile'];
        $smsMsg = 'Dear ' . $customer_data['username'] . ', ';
        $smsMsg .= 'Your Order has placed successfully. which Order ID is #' . $oid;
        send_sms($mobile, $smsMsg);
    }
}

if (!function_exists('getServicesPrices')) {
    function getServicesPrices($service_id, $zipcode)
    {
        
        CI()->db->select('*');
        CI()->db->from('tbl_service_provider_settings');
        CI()->db->where('service_id', $service_id);
        $areaCode = substr($zipcode, 0, 3);
        CI()->db->where('( find_in_set("' . $zipcode . '", service_zip_code) <> 0  OR (service_zip_code LIKE "' . $areaCode . '%" AND is_allowed_all_area=1) )');
        CI()->db->where('status', 1);
        $query = CI()->db->get();
        $count = $query->num_rows();
        //echo CI()->db->last_query(); die;
        if ($count > 0) {
            $row = $query->row();
            return $row;
        } else {
            return array();
        }
    }
}

if (!function_exists('coupon_amount')) {
    function coupon_amount($coupon_code, $amount)
    {
        CI()->db->from('tbl_coupons');
        CI()->db->where('coupon_code', $coupon_code);
        CI()->db->where('status', 1);
        $query = CI()->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $row = $query->row();
            if (!empty($row->type) && $row->type == 'flat') {
                return !empty($row->coupon_discount) ? $row->coupon_discount : 0;
            } elseif (!empty($row->type) && $row->type == 'percent') {
                return !empty($row->coupon_discount) ? $row->coupon_discount / 100 * $amount : 0;
            }
        } else {
            return 0;
        }
    }
}

if (!function_exists('service_price_on_booking')) {
    function service_price_on_booking($service, $searched_zipcode, $datetime, $additional_operator = 0, $coupon_code = '')
    {
        $serviceInfo = getServicesPrices($service, $searched_zipcode);
        $subtotal    = 0;
        
        if (isWeekend($datetime)) {
            if (!empty($additional_operator)) {
                $service_charge   = !empty($serviceInfo->weekend_additional_worker) ? $serviceInfo->weekend_additional_worker : 0;
                $service_duration = getDuration(60);
            } else {
                $service_charge   = !empty($serviceInfo->weekend_charge) ? $serviceInfo->weekend_charge : 0;
                $service_duration = !empty($serviceInfo->weekend_duration) ? getDuration($serviceInfo->weekend_duration) : '';
            }
        } else {
            if (!empty($additional_operator)) {
                $service_charge   = !empty($serviceInfo->service_additional_worker) ? $serviceInfo->service_additional_worker : 0;
                $service_duration = getDuration(60);
            } else {
                $service_charge   = !empty($serviceInfo->service_charge) ? $serviceInfo->service_charge : 0;
                $service_duration = !empty($serviceInfo->service_duration) ? getDuration($serviceInfo->service_duration) : '';
            }
        }
        
        $subtotal = $subtotal + $service_charge;
        
        if (!empty($coupon_code)) {
            $coupon_amount = coupon_amount($coupon_code, $subtotal);
            if (!empty($coupon_amount)) {
                $subtotal = $subtotal - $coupon_amount;
            }
        }
        $tax              = !empty($serviceInfo->tax) ? $serviceInfo->tax : 0;
        $other_tax        = !empty($serviceInfo->tax_other_info) ? $serviceInfo->tax_other_info : 0;
        $base_tax_amount  = !empty($tax) ? ($tax / 100) * $service_charge : 0;
        $other_tax_amount = !empty($other_tax) ? ($other_tax / 100) * $service_charge : 0;
        
        $tax_amount = $base_tax_amount + $other_tax_amount;
        
        $tax_name = !empty($serviceInfo->tax_name) ? $serviceInfo->tax_name : '';
        $subtotal = $subtotal + $tax_amount;
        
        $currency_id = !empty($serviceInfo->currency) ? $serviceInfo->currency : getCurrency();
        
        $output['service_charge']   = $service_charge;
        $output['service_duration'] = $service_duration;
        $output['coupon_code']      = !empty($coupon_code) ? $coupon_code : '';
        $output['coupon_amount']    = !empty($coupon_amount) ? $coupon_amount : 0;
        $output['tax']              = $tax;
        $output['other_tax']        = $other_tax;
        $output['tax_name']         = $tax_name;
        $output['tax_amount']       = $tax_amount;
        $output['subtotal']         = $subtotal;
        
        $output['currency_id'] = $currency_id;
        return $output;
    }
}

if (!function_exists('getTodayBlogPosts')) {
    function getTodayBlogPosts()
    {
        
        CI()->db->select('b.id as id, b.created as created,b.slug as slug, b.featured_img as featured_img, b.status as status , c.name as name, c.description as description');
        CI()->db->from('tbl_blog as b');
        CI()->db->join('tbl_blog_content as c', 'b.id = c.blog_id', 'LEFT');
        CI()->db->where('c.language_id', 1);
        CI()->db->where('b.created LIKE', '%' . date('Y-m-d') . '%');
        CI()->db->order_by('b.id', 'desc');
        
        if (!empty($limit))
            CI()->db->limit($limit, $start);
        
        $query = CI()->db->get();
        
        //echo CI()->db->last_query();
        
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
}

if (!function_exists('video_thumbnail')) {
    function video_thumbnail($url, $source = 'Youtube')
    {
        if ($source == 'Vimeo') {
            $thumbnail_url = vimeo_video_thumbnail($url);
        } else {
            $thumbnail_url = youtube_video_thumbnail($url);
        }
        return $thumbnail_url;
    }
}

if (!function_exists('youtube_video_thumbnail')) {
    function youtube_video_thumbnail($url)
    {
        $video_id = youtube_video_id($url);
        return $thumbnail = "http://img.youtube.com/vi/" . $video_id . "/mqdefault.jpg";
    }
}

if (!function_exists('youtube_video_id')) {
    function youtube_video_id($url)
    {
        // Here is a sample of the URLs this regex matches: (there can be more content after the given URL that will be ignored)
        // http://youtu.be/dQw4w9WgXcQ
        // http://www.youtube.com/embed/dQw4w9WgXcQ
        // http://www.youtube.com/watch?v=dQw4w9WgXcQ
        // http://www.youtube.com/?v=dQw4w9WgXcQ
        // http://www.youtube.com/v/dQw4w9WgXcQ
        // http://www.youtube.com/e/dQw4w9WgXcQ
        // http://www.youtube.com/user/username#p/u/11/dQw4w9WgXcQ
        // http://www.youtube.com/sandalsResorts#p/c/54B8C800269D7C1B/0/dQw4w9WgXcQ
        // http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ
        // http://www.youtube.com/?feature=player_embedded&v=dQw4w9WgXcQ
        // It also works on the youtube-nocookie.com URL with the same above options.
        // It will also pull the ID from the URL in an embed code (both iframe and object tags)
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        $video_id = !empty($match[1]) ? $match[1] : 0;
        return $video_id;
    }
}

if (!function_exists('vimeo_video_thumbnail')) {
    function vimeo_video_thumbnail($url)
    {
        $id    = vimeo_video_id($url);
        $vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        return $medium = $vimeo[0]['thumbnail_medium'];
    }
}

if (!function_exists('vimeo_video_id')) {
    function vimeo_video_id($url)
    {
        //Get Vimeo video id from url
        //Supported url formats -
        //https://vimeo.com/11111111
        //http://vimeo.com/11111111
        //https://www.vimeo.com/11111111
        //http://www.vimeo.com/11111111
        //https://vimeo.com/channels/11111111
        //http://vimeo.com/channels/11111111
        //https://vimeo.com/groups/name/videos/11111111
        //http://vimeo.com/groups/name/videos/11111111
        //https://vimeo.com/album/2222222/video/11111111
        //http://vimeo.com/album/2222222/video/11111111
        //https://vimeo.com/11111111?param=test
        //http://vimeo.com/11111111?param=test
        
        $regs = array();
        $id   = '';
        if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
            $id = !empty($regs[3]) ? $regs[3] : 0;
        }
        return $id;
    }
}

if (!function_exists('getInvoiceFile')) {
    function getInvoiceFile($id)
    {
        CI()->db->select('invoice_filename');
        CI()->db->where('id', $id);
        CI()->db->from('tbl_invoice');
        $query = CI()->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result->invoice_filename;
        } else {
            return '';
        }
    }
}

if (!function_exists('getInvoiceDeatilsByID')) {
    function getInvoiceDeatilsByID($id)
    {
        CI()->db->where('id', $id);
        CI()->db->from('tbl_invoice');
        $query = CI()->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
    }
}

if (!function_exists('getServiesProviderDetails')) {
    function getServiesProviderDetails($id)
    {
        
        CI()->db->select('s.*, u.username, u.fname, u.lname,u.email,u.mobile, u.featured_img, u.site_location, u.country, u.state, u.city, u.postal_code');
        CI()->db->from('tbl_service_provider_request as s');
        CI()->db->join('tbl_users as u', 's.user_id = u.id', 'LEFT');
        CI()->db->join('tbl_service_provider_settings as ss', 's.id = ss.services_request_id', 'LEFT');
        CI()->db->where('s.user_id', $id);
        $query = CI()->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $row = $query->row();
            return $row;
        } else {
            return array();
        }
    }
}

if (!function_exists('getServiesProviderPic')) {
    function getServiesProviderPic($id)
    {
        $details = getServiesProviderDetails($id);
        if (!empty($details->featured_img)) {
            $img = base_url($details->featured_img);
        } else {
            $img = base_url('skin/admin/images/profile.jpg');
        }
        
        return $img;
    }
}


if (!function_exists('getServiesProviderName')) {
    function getServiesProviderName($id)
    {
        $details = getServiesProviderDetails($id);
        if (!empty($details->business) && $details->business == 'yes' && !empty($details->business_name)) {
            $name = $details->business_name;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->username)) {
            $name = $details->username;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->fname)) {
            $name = $details->fname . ' ' . $details->lname;
        } else {
            $name = '';
        }
        
        return $name;
    }
}

if (!function_exists('getServiesProviderEmail')) {
    function getServiesProviderEmail($id)
    {
        $details = getServiesProviderDetails($id);
        $email   = !empty($details->business_email) ? $details->business_email : $details->email;
        return $email;
    }
}

if (!function_exists('getServiesProviderContact')) {
    function getServiesProviderContact($id)
    {
        $details = getServiesProviderDetails($id);
        $contact = !empty($details->business_mobile) ? $details->business_mobile : $details->mobile;
        return $contact;
    }
}

if (!function_exists('getServiesProviderAddress')) {
    function getServiesProviderAddress($id)
    {
        $details = getServiesProviderDetails($id);
        $address = '<p>';
        if (!empty($details->business_aptno)) {
            $address .= $details->business_aptno . ', ';
        }
        if (!empty($details->business_street_no)) {
            $address .= $details->business_street_no . ', ';
        }
        if (!empty($details->business_street_name)) {
            $address .= $details->business_street_name . ', <br>';
        }
        
        if (!empty($details->business_direction)) {
            $address .= '<strong>Direction : </strong>' . $details->business_direction . ', ';
        }
        
        if (!empty($details->business_street_type)) {
            $address .= '<strong>Street Type : </strong>' . $details->business_street_type . ', <br>';
        }
        
        if (!empty($details->business_city)) {
            $address .= $details->business_city . ', ';
        }
        if (!empty($details->business_state)) {
            $address .= getStateName($details->business_state) . ', ';
        }
        if (!empty($details->business_country)) {
            $address .= getCountryName($details->business_country) . ', <br>';
        }
        if (!empty($details->business_zip)) {
            $address .= $details->business_zip . ', ';
        }
        $address .= '</p>';
        return $address;
    }
}

if (!function_exists('getOerderItemList')) {
    function getOrderItemList($id = 0)
    {
        if (!empty($id)) {
            CI()->db->where('order_id', $id);
            $query = CI()->db->get('tbl_products_orders_items');
            $count = $query->num_rows();
            if ($count > 0) {
                $orderData = $query->result();
                $data_text = "";
                foreach ($orderData as $data) {
                    $data_text .= getProductName($data->product_id) . "<br/>";
                }
                return $data_text;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('setPicInsertData')) {
    function setPicInsertData($table, $data)
    {
        CI()->db->insert($table, $data);
        return CI()->db->insert_id();
    }
}

if (!function_exists('getUserNamebyEmail')) {
    function getUserNamebyEmail($email)
    {
        if (!empty($email)) {
            CI()->db->where('email', $email);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                $fname    = $userData->fname;
                if (!empty($fname)) {
                    return $userData->fname . ' ' . $userData->lname;
                } else {
                    return $userData->company_name;
                }
            } else {
                return 'unknown user';
            }
        }
    }
}

if (!function_exists('getUserAddressbyId')) {
    function getUserAddressbyId($id)
    {
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users_address');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row_array();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('isExistRecord')) {
    function isExistRecord($table, $whereArr)
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        $query = CI()->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}

if (!function_exists('getUserName')) {
    function getUserName($id)
    {
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                $fname    = $userData->fname;
                if (!empty($fname)) {
                    return $userData->fname . ' ' . $userData->lname;
                } else {
                    return $userData->company_name;
                }
            } else {
                return 'unknown user';
            }
        }
    }
}

if (!function_exists('getBusinessName')) {
    function getBusinessName($id = "")
    {
        if (!empty($id)) {
            CI()->db->where('user_id', $id);
            $query = CI()->db->get('tbl_service_provider_request');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData->business_name;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('getRoleByEmail')) {
    function getRoleByEmail($id)
    {
        CI()->db->select('role');
        CI()->db->where('username', $id);
        CI()->db->or_where('email', $id);
        $query    = CI()->db->get('tbl_users');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result->role;
        } else {
            return false;
        }
    }
}

if(!function_exists('send_sms')){
	function send_sms($mobile,$message)
	{
	    $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
		    'api_key' =>'8a787c41',
		    'api_secret' =>'8qIqSl7UXVEFg6c1',
		    'to' =>$mobile,
		    'from' =>'MARKD',
		    'text' =>$message
	    ]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return $response = curl_exec($ch);
	}
}

if(!function_exists('pre'))
{
    function pre($arrayData)
    {
        echo "<pre>"; print_r($arrayData); echo "</pre>";
    }
}


if (!function_exists('getServicesOptions')) {
    function getServicesOptions($type, $status = 0)
    {
        $options = array(
            '' => $type
        );
        CI()->db->from('services');
        CI()->db->where('status', 1);
        $query   = CI()->db->get();
        $records = $query->result_array();
        if(!empty($records))
        {
           $records = getOptionsTree($records);
            foreach ($records as $rec) {
                $options[$rec['id']] = $rec['name'];
            }
        }               
        return $options;
    }
}

if(!function_exists('getMultilevelOptions'))
{

    function getMultilevelOptions($table, $whereArr = array(), $blankOption = '------Select Option-----')
    {
        $options = array(
            '' => $blankOption
        );

        if(!empty($whereArr))
            CI()->db->where($whereArr);

        CI()->db->from($table);        
        $query   = CI()->db->get();
        $records = $query->result_array();
        if(!empty($records))
        {
           $records = getOptionsTree($records);
            foreach ($records as $rec) {
                $options[$rec['id']] = $rec['name'];
            }
        }               
        return $options;
    }
}

if(!function_exists('getOptionsTree'))
{
    function getOptionsTree($records, $parent = 0, $indent = "")
    {
        $results = array();
        if ($records)
        {           
            foreach ($records as $key => $val)
            {
                if ($val['parent'] == $parent)
                {
                    $val['name'] = $indent . $val['name'];
                    $results[$val['id']] = $val;
                    $results = $results + (getOptionsTree($records, $val['id'], $indent . "&nbsp;&nbsp;--&nbsp;&nbsp;"));
                }
            }
        }
        return $results;
    }
}