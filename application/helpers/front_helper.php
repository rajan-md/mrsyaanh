<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('CI')) {
    function CI()
    {
        $CI =& get_instance();
        return $CI;
    }
}

if (!function_exists('addJs')) {
    function addJs($jsArray = array())
    {
        $str = '';
        if (is_array($jsArray)):
            foreach ($jsArray as $key => $value) {
                $str .= "<script  src='" . base_url('skin/front/js/' . $value) . "'></script>" . "\n";
            }
        endif;
        echo $str;
    }
}

if (!function_exists('getCustomerSidebar')) {
    function getCustomerSidebar($id = 0)
    {
        if ($id == 0) {
            $id = CI()->session->userdata('userID');
        }
        if (!empty($id)) {
            CI()->db->where('id', $id);
            $query = CI()->db->get('tbl_users');
            $count = $query->num_rows();
            if ($count > 0) {
                $userData = $query->row();
                return $userData;
            } else {
                return array();
            }
        }
    }
}

if (!function_exists('CheckUserLoginSession')) {
    function CheckUserLoginSession()
    {
        
        $userID = CI()->session->userdata('userID');
        if (empty($userID)) {
            redirect('/', 'refresh');
        }
        
    }
}

if (!function_exists('CheckNotLoginSession')) {
    function CheckNotLoginSession()
    {
        
        $userID = CI()->session->userdata('userID');
        if (!empty($userID)) {
            redirect('my-account', 'refresh');
        }
        
    }
}