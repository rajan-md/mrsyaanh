<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('CI')) {
    function CI()
    {
        $CI =& get_instance();
        return $CI;
    }
}

if (!function_exists('CheckAdminLoginSession')) {
    function CheckAdminLoginSession()
    {
        $role     = CI()->session->userdata('role');
        $segment  = CI()->uri->segment(2);
        $menu     = AllowedMenuAccess($role, $segment);
        $admin_id = CI()->session->userdata('admin_id');
        if ($menu == 0) {
            redirect('admin/security', 'refresh');
        } else {
            if (empty($admin_id)) {
                redirect('admin', 'refresh');
            } else {
                return 1;
            }
        }
    }
}


if (!function_exists('AllowedMenuAccess')) {
    function AllowedMenuAccess($id, $attr)
    {
        if ($id == 1) {
            return 1;
        } else {
            CI()->db->where('roles', $id);
            CI()->db->where($attr, 1);
            $query    = CI()->db->get('tbl_privilege');
            $rowCount = $query->num_rows();
            if ($rowCount > 0) {
                
                return 1;
            } else {
                return 0;
            }
        }
    }
}



if (!function_exists('AllowedAction')) {
    function AllowedAction($module, $edit = "", $delete = "", $msg = "", $id = 0, $setting_icon = false)
    {
        $html   = '';
        $action = CI()->uri->segment(2);
        $url    = 'admin/' . $module;
        $role   = CI()->session->userdata('role');
        if ($role == 1) {
            $allowedit   = 1;
            $allowdelete = 1;
        } else {
            $allowedit   = getActionAttribute($action, $edit);
            $allowdelete = getActionAttribute($action, $delete);
        }
        if (($edit == 'edit') && ($allowedit == 1) && $setting_icon == true) {
            $html .= '<a href="' . base_url('' . $url . '/' . $edit . '/' . $id) . '" class="btn btn-xs"> <i class="icon-large icon-cogs"></i> </a>';
        } elseif (($edit == 'edit') && ($allowedit == 1) && $setting_icon == false) {
            $html .= '<a href="' . base_url('' . $url . '/' . $edit . '/' . $id) . '" class="btn btn-xs btn-primary"> <i class="icon-large icon-pencil"></i> </a>';
        } else if (($edit == 'view') && ($allowedit == 1)) {
            $html .= '<a href="' . base_url('' . $url . '/' . $edit . '/' . $id) . '" class="btn btn-xs btn-primary"> <i class="icon-large icon-eye-open"></i> </a>';
        } else {
            $confirm = "confirm('Access denied')";
            $html .= ' <a onclick="return ' . $confirm . '" disabled class="btn btn-xs btn-primary"> <i class="icon-large icon-pencil"></i> </a>';
        }
        
        if ((!empty($delete)) && ($allowdelete == 1)) {
            $confirm = "confirm('" . $msg . "')";
            $html .= ' <a onclick="return ' . $confirm . '" href="' . base_url('' . $url . '/' . $delete . '/' . $id) . '"  class="btn btn-xs btn-danger"> <i class="icon-large icon-remove"></i> </a>';
        } else {
            $confirm = "confirm('Access denied')";
            $html .= ' <a onclick="return ' . $confirm . '" disabled class="btn btn-xs btn-danger"> <i class="icon-large icon-remove"></i> </a>';
        }
        
        return $html;
    }
}

if (!function_exists('AddButton')) {
    function AddButton($add = "")
    {
        $html   = '';
        $action = CI()->uri->segment(2);
        $role   = CI()->session->userdata('role');
        
        if ($role == 1) {
            $allowadd = 1;
        } else {
            $allowadd = getActionAttribute($action, $add);
        }
        if ($allowadd == 1) {
            $html .= '<a  href="' . base_url('admin/' . $action . '/' . $add) . '" class="btn btn-s-md btn-success"> Add New </a>';
        } else {
            $confirm = "confirm('Access denied')";
            $html .= '<a onclick="return ' . $confirm . '"  disabled href="#" class="btn btn-s-md btn-success"> Add New </a>';
        }
        
        return $html;
    }
}

if (!function_exists('statusButton')) {
    function statusButton($module, $action, $status, $id)
    {
        $html    = '';
        $user_id = CI()->session->userdata('admin_id');
        $role    = getUserRole($user_id);
        $url     = 'admin/' . $module . '/' . $action . '/' . $id;
        
        if ($role == 1) {
            $allowedit   = 1;
            $allowdelete = 1;
        } else {
            $allowedit   = 0; //getActionAttribute($action,$edit);
            $allowdelete = 0; //getActionAttribute($action,$delete);
        }
        
        $label = (!empty($status) && $status == 1) ? 'Active' : 'Deactive';
        $class = (!empty($status) && $status == 1) ? 'label label-success' : 'label label-warning';
        $html  = '<a title="Change record status" href="' . base_url($url) . '"><small class="' . $class . '">' . $label . '</small></a>';
        return $html;
    }
}


function getCategories($parent, $categories) 
{
    $html = "";
        $html .= "<ul>\n";
        foreach ($categories as $category)
        {
            if (!isset($category->parent))
            {
              $html .= "<li>".$category->name."</li> \n";
            }
            
            if (isset($category->parent))
            {
              $html .= "<li>". $category->name . " \n";
              $html .= getCategories($category->name, $categories);
              $html .= "</li> \n";
            }
        }
        $html .= "</ul> \n";
    return $html;
}