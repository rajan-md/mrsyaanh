<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller']    				= 'front'; 
$route['404_override']          				= '';
$route['translate_uri_dashes']  				= FALSE;
$route['admin']                 				= 'admin/login';
$route['admin/services/prices/add']         	= 'admin/services/add_price';
$route['admin/services/prices/edit/(:num)'] 	= 'admin/services/edit_price/$1';
$route['admin/services/prices/delete/(:num)']	= 'admin/services/prices_delete/$1';

$route['admin/services/service_provider_records/add/(:num)']         = 'admin/services/add_servicerequest/$1';
$route['admin/services/service_provider_records/edit/(:num)'] 		 = 'admin/services/provider_request_details/$1';
$route['admin/services/service_provider_records/(:num)/edit/(:num)'] = 'admin/services/edit_servicerequest/$1/$2';
$route['admin/services/service_provider_records/delete/(:num)']		 = 'admin/services/delete_servicerequest/$1';


$route['admin/services/servicearea/add']           = 'admin/services/addservicearea';
$route['admin/services/servicearea/edit/(:num)']   = 'admin/services/editservicearea/$1';
$route['admin/services/servicearea/delete/(:num)'] = 'admin/services/deleteservicearea/$1';
$route['admin/reports/(:num)']        		= 'admin/services/index/$1';
$route['admin/ajax-serviceprovider-info']   = 'admin/services/ajax_serviceprovider_info';
$route['admin/ajax-find-statements']   		= 'admin/services/ajax_find_statements';
$route['admin/services/invoices']   		= 'admin/services/invoices';
$route['admin/services/mark-invoice-paid/(:num)']  = 'admin/services/mark_invoice_paid/$1';
$route['admin/services/download-invoice/(:num)']  = 'admin/services/download_invoice/$1';
$route['admin/services/resend-invoice/(:num)']  = 'admin/services/resend_invoice/$1';
$route['admin/services/create-invoice']   	  = 'admin/services/createinvoice';
$route['admin/services/export-invoice']   	  = 'admin/services/export_invoice';
$route['admin/services/send-invoice']   	  = 'admin/services/send_invoice';
$route['admin/services/videos/(:num)']   	  = 'admin/services/videos/$1';
$route['admin/services/videos/add']           = 'admin/services/add_video';
$route['admin/services/videos/edit/(:num)']   = 'admin/services/edit_video/$1';
$route['admin/services/videos/delete/(:num)'] = 'admin/services/delete_video/$1';

$route['admin/vehicle_request'] 			= 'admin/vehicle/request';
$route['admin/profile']         			= 'admin/users/profile';
$route['admin/privilege']       			= 'admin/users/privilege';
$route['admin/security']        			= 'admin/dashboard/security';

$route['services/(:any)']        			= 'services/details/$1';
$route['become-a-service-provider']         = 'front/become_professtional';
$route['ajax-service-provider-request']     = 'front/ajax_become_professtional';
$route['service-provider-signup/(:any)']    = 'front/register_professtional/$1';
$route['service-provider-signup/(:any)/(:any)']    = 'front/register_professtional/$1/$2';
$route['ajax-service-provider-signup']    	= 'front/ajax_register_professtional';
$route['ajax-validate-zipcode']    			= 'front/validateZipCode';
$route['ajax-states']    					= 'front/ajax_states';
$route['service-provider-signup']    		= 'front/register_professtional';
$route['service-provider-thanks']    		= 'front/thanks_professional'; 
$route['addtocart']        			        = 'shop/addtocart';
$route['updatecart']        			    = 'shop/updatecart';
$route['removecart']        			    = 'shop/removeCart';
$route['cart']        			            = 'shop/cart_list';
$route['ajax-find-services']        	    = 'services/findSearchResult';
$route['ajax-subscribe-service']        	= 'services/ajaxSubscribeService';
$route['ajax-validate-zipcode-search']      = 'services/ajaxValidateZipcode';
$route['send_services_not_found_message']   = 'services/send_services_not_found_message';
$route['ajax-login-info']        	        = 'services/findLoginDetail';
$route['ajax-price-listing']        	    = 'services/findPriceingListing';
$route['ajax-additional-operator-charges']  = 'services/findAdditionalOperatorPricing';
$route['ajax-service-charges']        	    = 'services/findServicePriceingListing';
$route['new-customer']        	            = 'services/newCustomerDetail';
$route['repeat-customer']        	        = 'services/repeatCustomerDetail';
$route['phone-login']        	            = 'services/phonelogin';
$route['repeat-login']        	            = 'services/repeatLogin';
$route['validateotp']        	            = 'services/validateotp';
$route['ajax-user-registration']        	= 'services/ajaxUserRegistration';
$route['ajax-forgot-password']        		= 'services/ajaxForgotPassword';
$route['reset-password'] 					= 'services/resetPassword';
$route['ajax-choose-address']        	    = 'services/ajaxChooseAddress';
$route['delete-address']        	        = 'services/deleteUserAddress';
$route['choose-date-time']        	        = 'services/chooseDatetime';
$route['service-order-review']        	    = 'services/serviceOrderReview';
$route['ajax-apply-coupon']        	    	= 'services/ajaxApplyCoupon';
$route['choose-payment-method']        		= 'services/choosePaymentOption';
$route['add-new-address']        	        = 'services/addNewAddress';
$route['request-step-one']        			= 'services/requestStepOne';
$route['request-step-thanks']        		= 'services/requestStepThanks';
$route['order-processing']                  = 'services/order_processing';
$route['pay-outstanding/(:any)']            = 'services/pay_outstanding/$1';
$route['provider-accept-request/(:any)']    = 'services/providerAcceptRequest/$1';
$route['provider-reject-request/(:any)']    = 'services/providerRejectRequest/$1';
$route['close-service-request/(:any)']    	= 'services/closeServiceRequest/$1';
$route['customer-cancel-request/(:any)']    = 'services/customerCancelRequest/$1';
$route['booking-details/(:any)']            = 'services/serviceBookingDetails/$1';
$route['ajax-assign-operator']            	= 'services/ajaxAssignOperator';
$route['reschedule/(:any)']                 = 'services/reschedule/$1';
$route['getlocation']        			    = 'services/getCurrentLocation';
$route['notify']                    		= 'ipn/verifyShopIPN';
$route['service-notify']                    = 'ipn/verifyIPN';
$route['service-os-notify']                 = 'ipn/verifyIPNOS';
$route['checkout']        			        = 'shop/checkout_review';
$route['payment-method']        			= 'shop/payment_method';
$route['shipping-method']        			= 'shop/shipping_method';
$route['order-review']        			    = 'shop/order_review';
$route['payment-processing']        	    = 'shop/payment_processing';
$route['user/wishlist']        			    = 'shop/wishlist';
$route['orders-details']        			= 'shop/orders_details';
$route['thank-you']        					= 'shop/thank_you';
$route['cancel']        					= 'shop/cancel';
$route['dashboard']        			        = 'login/dashboard';
$route['my-bookings']        			    = 'login/mybookings';
$route['my-bookings/(:num)']        		= 'login/mybookings/$1';
$route['my-invoices']        				= 'login/myInvoices';
$route['download-invoice/(:num)']        	= 'login/download_invoice/$1';
$route['customer-request']        			= 'login/customerRequest';
$route['customer-request/(:num)']        	= 'login/customerRequest/$1';
$route['customer-request-details/(:any)']   = 'login/customerRequestDetails/$1';
$route['export-leads']    					= 'login/export_leads';
$route['orders']        			        = 'login/orders';
$route['orders/(:num)']        			    = 'login/orders/$1';
$route['order-details/(:any)']        		= 'login/orders_details/$1';
$route['invoice/(:any)']        			= 'login/customer_invoice/$1';
$route['my-account']        			    = 'login/myaccount';
$route['service-video']        			    = 'login/videos';
$route['service-video/(:num)']        		= 'login/videos/$1';
$route['service-video-play/(:any)']        	= 'login/videos_play/$1';
$route['contract-form-102']        			= 'login/ContractForm102';
$route['contract-form-103']        			= 'login/ContractForm103';
$route['change-password']        			= 'login/change_password';
$route['service-request']        			= 'login/provider_service_request';
$route['new-service-request']        		= 'login/register_new_service';
$route['service-request-details/(:any)']    = 'login/serviceRequestDetails/$1';
$route['wishlist']        			        = 'shop/wishlist';
$route['wishlist/(:num)']        			= 'shop/wishlist/(:num)';
$route['edit-profile']        			    = 'login/profile_update';
$route['operators']        			    	= 'login/operators';
$route['operators/(:num)']        			= 'login/operators/$1';
$route['operations']        			    = 'login/operations';
$route['operations/(:num)']        			= 'login/operations/$1';
$route['operation_details/(:any)']        	= 'login/operation_details/$1';
$route['send-operations-otp/(:any)']        = 'login/send_operations_otp/$1';
$route['ajax-otp-verify']        			= 'login/verify_service_otp';
$route['ajax-start-service']        		= 'login/ajax_start_service';
$route['ajax-end-service']        			= 'login/ajax_end_service';
$route['jobs']        			    		= 'login/jobs';
$route['jobs/(:num)']        				= 'login/jobs/$1';
$route['job_details/(:any)']        		= 'login/job_details/$1';
$route['blog']        						= 'blog/index';
$route['blog/(:num)']        				= 'blog/index/$1';
$route['blog/(:any)']        				= 'blog/details/$1';
$route['support-order-details/(:any)']      = 'supports/order_details/$1';
$route['add-operator']        			    = 'login/add_operator';
$route['edit-operator/(:any)']    			= 'login/edit_operator/$1';
$route['delete-operator/(:any)']    		= 'login/delete_operator/$1';
$route['operator-status/(:any)']    		= 'login/status_operator/$1';
$route['addtowishlist']        		        = 'shop/addtowishlist';
$route['shop']        			            = 'shop/lists';
$route['shop/(:num)']        			    = 'shop/lists/$1';
$route['shop/(:any)']        			    = 'shop/product_details/$1';
$route['download-contract']                 = 'front/download_contract';
$route['supports']                			= 'supports/index';
$route['supports/(:num)']                	= 'supports/index/$1';
$route['support-tickets']                	= 'supports/supporttickets';
$route['request-support']                	= 'supports/requestSupport';
$route['complaint']                			= 'login/complaint';
$route['feedback']                			= 'login/feedback';
$route['feedback/(:any)']                	= 'login/feedback/$1';
$route['service-orders']                	= 'login/service_orders';
$route['service-orders/(:num)']             = 'login/service_orders/$1';
$route['service-order-details/(:any)']      = 'login/service_order_details/$1';
$route['service-provider-details/(:any)']   = 'login/service_provider_details/$1';
$route['ajax-order-options']   				= 'supports/ajaxOrderOptions';
$route['support-request-details/(:any)']    = 'supports/supportRequestDeatil/$1';
$route['delete-support-ticket/(:any)']    	= 'supports/deleteSupportRequest/$1';

$route['cron-blog']                			= 'cron/index';
$route['cron-service']                		= 'cron/service';


$route['(:any)']        			        = 'front/page_details/$1';
