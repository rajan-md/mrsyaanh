<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('front_model');
        $this->load->model('services_model');   
        $this->load->helper('front_helper');
        $this->load->model('smtp_model');        
    }

    public $tbl_name='pages';
    public $users='users';
    public $users_address='users_address';
    public $services='services';
    public $services_area='services_area';
    public $provider_request='service_provider_request';
    public $provider_request_setting='service_provider_settings';

	public function index()
    {
        $data = array();    
        //$data['pageContent']=$this->front_model->getSinglePageDataCollection($slug, $language_id);
        //$data['testimonialContent']=$this->front_model->getTestimonialsDataCollection($language_id);
        //$data['featureServices']=$this->front_model->getFeaturSrvices($language_id);
        //$data['featureproducts']=$this->front_model->getFeaturProducts($language_id);
        
        $this->load->view('front/include/header', $data);
        $this->load->view('front/home', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

    public function page_details($slug)
    {
        $data['pageContent'] = $this->master_model->get_recordArr($this->tbl_name,array('slug'=>$slug));
        if(!empty($data['pageContent'])){
            $data['pageTitle']=$data['pageContent']['name'];
        } else {
            $data['pageTitle']=$this->lang->line('PAGE_NOT_FOUND');
        }
        $this->load->view('front/include/header', $data);
        $this->load->view('front/pages/details', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function ajax_states()
    {
        $country_id=$this->input->post('country_id');
        $options = '';
        if(!empty($country_id))
        {
            $records = getStatesOptions($country_id);
            if(!empty($records))
            {
                foreach($records as $key=>$val)
                {
                   $options.= '<option value="'.$key.'">'.$val.'</option>';
                }
            }
            else
            {
                $options.= '<option value="0">----Choose State/Province----</option>';
            }
        }
        echo $options;
    }

    public function ajax_cities()
    {
        $state_id=$this->input->post('state_id');
        $options = '';
        if(!empty($state_id))
        {
            $records = getCitiesOptions($state_id);
            if(!empty($records))
            {
                foreach($records as $key=>$val)
                {
                   $options.= '<option value="'.$key.'">'.$val.'</option>';
                }
            }
            else
            {
                $options.= '<option value="0">----Choose City----</option>';
            }
        }
        echo $options;
    }

    public function become_professtional()
    {
        CheckNotLoginSession(); 
        $data['serviceOptions']=getServicesOption($this->lang->line('CHOOSE_SERVICE'));
        $js=array('become-a-professtional.js');
        $this->config->set_item('headJs',$js);
        $this->load->view('front/include/header', $data);
        $this->load->view('front/become_professtional', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);      
    }

    public function ajax_become_professtional()
    {
        $html = ''; 
        $post_data=$this->input->post();        
        if(!empty($post_data))
        {
            $data['services_type']=$this->input->post('chooseserices');
            $data['message']=$this->input->post('message');
            $data['bussiness_holder_fname']=$this->input->post('business_holder_fname');
            $data['bussiness_holder_lname']=$this->input->post('business_holder_lname');
            $data['business']=$this->input->post('business');
            $data['business_name']=$this->input->post('business_name');
            $data['business_email']=$this->input->post('business_email');
            $data['business_mobile']=$this->input->post('mobbox');
            $data['business_zip']=$this->input->post('service_zipcode');
            $data['business_type']=$this->input->post('business_type');
            $data['license']=$this->input->post('license');
            $data['license_details']=$this->input->post('license_details');
            $data['request_date']=date("Y-m-d h:i:s");
            $data['service_status']=2;
            $data['is_verified']=0;                    
            $data['user_id']=0;
            $service_request_id = $this->front_model->setInsertData($this->provider_request, $data);
            if($service_request_id>0)
            {                        
                $data2['service_id'] = $this->input->post('chooseserices');                     
                $data2['services_request_id'] = $service_request_id;      
                $data2['service_zip_code']= $this->input->post('service_zipcode');
                $data2['service_category_code']=$this->input->post('chooseserices');
                $settings_id = $this->front_model->setInsertData($this->provider_request_setting, $data2);

                $zipcode = $this->input->post('service_zipcode');
                $address = getAddressByZipcode($zipcode);
                if(!empty($address['zipcode']))
                {
                    $this->insertZipcodeWithRelatedInfo($address);
                }

                $this->send_provider_thank_you();
                $this->send_provider_service_description();
                $this->send_provider_request_to_administrator();
                
                $html='<fieldset>                    
                   <div class="bus-part-success">
                    <i class="fa fa-check"></i>
                    <h4>Congratulations!</h4>
                    <h5>You have successfully submitted a service request for partner on First Choice Everything</h5>
                    <p>Thank you for requesting information. We will send email with sign up link after verification of your submitted request. We will contact tou soon</p>
                    <p>If you need more information you can contact us for any type of query and concern.</p>
                   </div>
                </fieldset>';
            }
        }              
        echo $html;
    }

    public function register_professtional()
    {    
        CheckNotLoginSession();

        $data = array();
        $service_request_id = base64_decode($this->uri->segment(2));
        $token = $this->uri->segment(3);
        $data['request_id'] = $this->uri->segment(2);
        $data['token'] = $token;
        if(empty($service_request_id) || empty($token))
        {
            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Parameter of your signup request are missing. Contact to admin please.'));
            redirect('become-a-service-provider','refresh');
        }
        $is_verified = $this->front_model->isVerifiedServiceRequest($this->provider_request,$service_request_id,$token);
        if (empty($is_verified))
        {
            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Request token is invalid or till not verified.'));
            redirect('become-a-service-provider','refresh');
        }
        $serviceRequest = $this->services_model->getRequestDetails($service_request_id);

        if(!empty($serviceRequest['user_id']))
        {
            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'You have already registered with us. Please <a href="javascript:void(0)" data-dismiss="modal" data-toggle="modal" data-target="#login">click here</a> to login your account.'));
            redirect('become-a-service-provider','refresh');
        }

        $data['serviceRequest'] = $serviceRequest;
        $js=array('become-a-professtional.js');
        $this->config->set_item('headJs',$js);
        $this->load->view('front/include/header', $data);
        $this->load->view('front/register_professtional', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

    public function ajax_register_professtional()
    {  
        $html='';
        $post_data=$this->input->post();
       // echo "<pre>"; print_r($post_data); echo "</pre>"; die;
        if(!empty($post_data))
        {   
            $service_request_id = base64_decode($this->input->post('srid'));
            $token = $this->input->post('token');
            $userdata['username']=$this->input->post('fname').' '.$this->input->post('lname');
            $userdata['fname']=$this->input->post('fname');
            $userdata['lname']=$this->input->post('lname');
            $userdata['email']=$this->input->post('useremail');
            $userdata['gender']=$this->input->post('gender');
            $userdata['dob']=$this->input->post('dob'); 
            $userdata['mobile']=$this->input->post('mobbox');
            $userdata['site_location']=$this->input->post('site_location');
            $userdata['aptno']=$this->input->post('aptno');
            $userdata['street_number']=$this->input->post('street_number');
            $userdata['street_name']=$this->input->post('street_name');
            $userdata['street_type']=$this->input->post('street_type');
            $userdata['direction']=$this->input->post('direction');
            $userdata['country']=$this->input->post('country');
            $userdata['state']=$this->input->post('state');
            $userdata['city']=$this->input->post('city');
            $userdata['postal_code']=$this->input->post('postal_code');
            $userdata['telephone']=$this->input->post('telephonebox');
            $userdata['latitude']=$this->input->post('site_latitude');
            $userdata['longitude']=$this->input->post('site_longitude');
            $userdata['password']=md5($this->input->post('password'));            
            $userdata['role']=5;
            $userdata['status']=1;

            $useremail = $this->input->post('useremail');
            $user_id = $this->front_model->isExistData($this->users,'email',$useremail);

            if(!empty($user_id))
            {
                $html = 'USER_EXIST';
            }
            else
            {
                $insertId=$this->front_model->setInsertData($this->users, $userdata);

                $dataCArea['zipcode']   = $this->input->post('postal_code');
                $dataCArea['latitude']  = $this->input->post('latitude');
                $dataCArea['longitude'] = $this->input->post('longitude');
                $dataCArea['country']   = $this->input->post('country');
                $dataCArea['state']     = $this->input->post('state');
                $dataCArea['city']      = $this->input->post('city');
                $dataCArea['address']   = $this->input->post('site_location');

                if(!empty($dataCArea['zipcode']))
                {
                    $this->insertZipcodeWithRelatedInfo($dataCArea);
                }                
            }

            if(!empty($insertId) && $insertId>0)
            {   
                $request['user_id'] = $insertId;
                $request['business'] = $this->input->post('business');  
                $request['business_name'] = $this->input->post('business_name');
                $request['business_type'] = $this->input->post('business_type');
                $request['license'] = $this->input->post('license');                            
                $request['license_details'] = $this->input->post('license_details');                        
                $request['business_aptno'] = $this->input->post('business_aptno');                          
                $request['business_street_no'] = $this->input->post('business_street_no');                  
                $request['business_street_name'] = $this->input->post('business_street_name');          
                $request['business_street_type'] = $this->input->post('business_street_type');          
                $request['business_country'] = $this->input->post('business_country');          
                $request['business_state'] = $this->input->post('business_state');          
                $request['business_city'] = $this->input->post('business_city');            
                $request['business_type'] = $this->input->post('business_type');            
                $request['business_direction'] = $this->input->post('business_direction');          
                $request['business_zip'] = $this->input->post('business_zip');          
                $request['business_mobile'] = $this->input->post('bussmobbox');            
                $request['business_fax'] = $this->input->post('business_fax');          
                $request['business_email'] = $this->input->post('business_email');          
                /*$request['driver_licence'] = $this->input->post('driver_licence');
                $request['car_made'] = $this->input->post('made');
                $request['car_model'] = $this->input->post('model');
                $request['car_year'] = $this->input->post('year');
                $request['car_color'] = $this->input->post('color');
                $request['car_number'] = $this->input->post('licence_plate_no');*/

                $bussiness_insert = $this->front_model->setUpdateData($this->provider_request, $request,$service_request_id);

                if(!empty($bussiness_insert))
                {
                    $dataBArea['zipcode']   = $this->input->post('business_zip');
                    $dataBArea['country']   = $this->input->post('business_country');
                    $dataBArea['state']     = $this->input->post('business_state');
                    $dataBArea['city']      = $this->input->post('business_city');
                    if(!empty($dataBArea['zipcode']))
                    {
                        $this->insertZipcodeWithRelatedInfo($dataBArea);
                    }                    
                }

                $settings['start_date'] = $this->input->post('start_date');
                $settings['operator_no'] = generateOperatorNumber($service_request_id,$insertId);
                $settings_insert = $this->front_model->setUpdateDataByWhere($this->provider_request_setting, $settings,array('services_request_id'=>$service_request_id));              

                $this->send_signup_notification();
                $this->send_signup_notification_to_administrator();

                $html='<fieldset>                    
                   <div class="bus-part-success">
                    <i class="fa fa-check"></i>
                    <h4>Congratulations!</h4>
                    <h5>You have successfully signed up as a partner on First Choice Everything</h5>
                    <p>Thank you for requesting information. We will email you the infor mation you need.</p>
                    <p>In that information pavkage we will explain how our system work and a form (Form 102)</p>
                    <p>If you need more information you will fill that form and we will send you more deatails.</p>
                   </div>
                </fieldset>';
            }
             
        }
        echo $html;
    }

    public function thanks_professional()
    {
        CheckNotLoginSession();
        $this->load->view('front/include/header');
        $this->load->view('front/thanks_professional');
        $this->load->view('front/include/modals');
        $this->load->view('front/include/footer');
          
    }

    public function validateZipCode()
    {
        $post_data=$this->input->post();
        if(!empty($post_data))
        { 
            $post_data=$this->input->post();
            $country_code = !empty($post_data['country_code'])?$post_data['country_code']:"US";
            $zip_code   = !empty($post_data['zip_code'])?$post_data['zip_code']:"";
            $response = validateZipCodeByCountry($country_code,$zip_code);
            echo $response;
        }
    }

    public function insertZipcodeWithRelatedInfo($dataArr)
    {

        $wherArr = array('zipcode'=> $dataArr['zipcode']);
        $isExist = $this->front_model->isExist($this->services_area,$wherArr);
        if(empty($isExist))
        {
            $data['zipcode']    = $dataArr['zipcode'];
            $data['fas']        = substr($dataArr['zipcode'], 0, 3);
            $data['latitude']   = $dataArr['latitude'];
            $data['longitude']  = $dataArr['longitude'];
            $data['country']    = getCountryName($dataArr['country']);
            $data['place_name'] = getStateName($dataArr['state']);
            $data['area']       = $dataArr['city'];
            $data['address']    = $dataArr['address'];
            $data['status']     = 0;
            $this->front_model->setInsertData($this->services_area,$data);
        }
    }
    
    public function download_contract() {
        $id =  $this->session->userdata('userID');
        if(!empty($id)) {
            $this->load->library('pdf');
            $data['page_title']='Orders';
            $id=$this->session->userdata('userID');
            $data['userDetails']=$this->front_model->getUserDetails($id);
            $data['businessDetails']=$this->front_model->getServiesProviderBusinessDetailsByUserId($id);
            $data['businessSettings']=getServiceProviderSettings($data['businessDetails']->id);
            $this->pdf->load_view('front/pdf/download_contract', $data);
            //$this->load->view('front/pdf/download_contract', $data);
        } else {
            redirect('/','refresh');
        }
    }

    public function send_provider_thank_you() { 
        $email=$this->input->post('business_email');
        $email_template  = 'service_provider_thank_you.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Thank You For Request',$message);

        $mobile_number = $this->input->post('mobbox');
        $smsMsg = 'Dear Candidate, ';
        $smsMsg.= 'We have recieved your service request and our team will contact you soon.';
        send_sms($mobile_number,$smsMsg);


    }


    public function send_provider_service_description() {
        $email=$this->input->post('business_email');
        $chooseserices=$this->input->post('chooseserices');
        $serviceName = getServicesName($chooseserices);
        $serviceEmailContent = getServicesEmailContent($chooseserices);

        $email_template  = 'service_provider_service_description.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{service_name}}'=>$serviceName,
        '{{service_description}}'=>$serviceEmailContent,
        '{{team_name}}'=>'firstchoice',
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'About Your Service',$message);
    }  

    public function send_provider_request_to_administrator()
    {
        $chooseserices=$this->input->post('chooseserices');
        $business_email=$this->input->post('business_email');
        $service_zipcode=$this->input->post('service_zipcode');
        $business_mobile=$this->input->post('business_mobile');
        $email= getAdministratorEmail();
        $email_template  = 'service_provider_request.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{bussiness_email}}'=> $business_email,
        '{{bussiness_contact}}'=>$business_mobile,
        '{{bussiness_service}}'=> getServicesName($chooseserices),
        '{{bussiness_zipcode}}'=>$service_zipcode,
        '{{team_name}}'=>'firstchoice',
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'New Service Request',$message);
    }
	
	public function send_signup_notification()
	{ 
        $email=$this->input->post('useremail');                   
        $email_template  = 'signup_notification.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Welcome to First Choice Everything',$message);
    } 

    public function send_signup_notification_to_administrator()
    { 
        $srid = base64_decode($this->input->post('srid'));
        $request = $this->services_model->getRequestDetails($srid);

        $user_name=$this->input->post('fname').' '.$this->input->post('lname');
        $user_email=$this->input->post('useremail');
        $user_mobile=$this->input->post('mobbox');

        $request_details = '';
        if(!empty($request) && !empty($request['id']))
        {
            $request_details.= '<p><strong>Request Id</strong> : #'.$request['id'].'<p>';
        }
        if(!empty($request) && !empty($request['service_id']))
        {
            $request_details.= '<p><strong>Requested Service</strong> : '.getServicesName($request['service_id']).'<p>';
        }
        if(!empty($request) && !empty($request['service_zip_code']))
        {
            $request_details.= '<p><strong>Requested Service Area zipcode</strong> : '.$request['service_zip_code'].'<p>';
        }

        $email= getAdministratorEmail();
        $email_template  = 'signup_notification_admin.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{request_details}}'=> $request_details,
        '{{user_name}}'=>$user_name,
        '{{user_email}}'=> $user_email,
        '{{user_mobile}}'=>$user_mobile,
        '{{team_name}}'=>'firstchoice',
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'New registration notification',$message);
    }
}