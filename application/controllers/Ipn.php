<?php 
class Ipn extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('services_model');
        $this->load->model('front_model');
        $this->load->model('smtp_model');
        $this->load->helper('front_helper');
        $this->load->model('login_model'); 
    }

    /** @var bool Indicates if the sandbox endpoint is used. */
    private $use_sandbox = true;
    /** @var bool Indicates if the local certificates are used. */
    private $use_local_certs = false;
    /** Production Postback URL */
    const VERIFY_URI = 'https://ipnpb.paypal.com/cgi-bin/webscr';
    /** Sandbox Postback URL */
    const SANDBOX_VERIFY_URI = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
    /** Response from PayPal indicating validation was successful */
    const VALID = 'VERIFIED';
    /** Response from PayPal indicating validation failed */
    const INVALID = 'INVALID';
    /**
     * Sets the IPN verification to sandbox mode (for use when testing,
     * should not be enabled in production).
     * @return void
     */
    public function useSandbox()
    {
        $this->use_sandbox = true;
    }
    /**
     * Sets curl to use php curl's built in certs (may be required in some
     * environments).
     * @return void
     */
    public function usePHPCerts()
    {
        $this->use_local_certs = false;
    }
    /**
     * Determine endpoint to post the verification data to.
     *
     * @return string
     */
    public function getPaypalUri()
    {
        if ($this->use_sandbox) {
            return self::SANDBOX_VERIFY_URI;
        } else {
            return self::VERIFY_URI;
        }
    }
    /**
     * Verification Function
     * Sends the incoming post data back to PayPal using the cURL library.
     *
     * @return bool
     * @throws Exception
     */

    public function verifyShopIPN()
    {
        if ( ! count($_POST)) {
            throw new Exception("Missing POST Data");
        }
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($this->getPaypalUri());
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.
        if ($this->use_local_certs) {
            curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
        }
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));
        $res = curl_exec($ch);
        if ( ! ($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);
            throw new Exception("cURL error: [$errno] $errstr");
        }
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            throw new Exception("PayPal responded with http code $http_code");
        }
        curl_close($ch);
        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::VALID) {
            $data_text = "";
            foreach ($_POST as $key => $value) {
                $data_text .= $key . " = " . $value . "\r\n";
            }
            $oid=$_POST['custom'];
            $data['payment_status'] =$_POST['payment_status'];
            $data['status'] = 2;
            $this->front_model->setUpdateData('tbl_products_orders', $data, $oid);
            send_order_notification($oid);
            //return true;
        }
        else
        {
            //$data['value'] ='fffffffffffff';
            //$this->front_model->setUpdateData('tbl_settings', $data, 36);
            //return false;
        }
    }

    public function verifyIPN()
    {
        if ( ! count($_POST)) {
            throw new Exception("Missing POST Data");
        }
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($this->getPaypalUri());
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.
        if ($this->use_local_certs) {
            curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
        }
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));
        $res = curl_exec($ch);
        if ( ! ($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);
            throw new Exception("cURL error: [$errno] $errstr");
        }
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            throw new Exception("PayPal responded with http code $http_code");
        }
        curl_close($ch);
        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::VALID) {
            $data_text = "";
            foreach ($_POST as $key => $value) {
                $data_text .= $key . " = " . $value . "\r\n";
            }
            $oid=$_POST['custom'];
            $data['payment_status'] =$_POST['payment_status'];
            $data['status'] = 0;
            $this->front_model->setUpdateData('tbl_service_orders', $data, $oid);
            $this->send_booking_customer_notification($oid);
            $this->send_booking_vendor_notification($oid);
            //return true;
        }
        else
        {
            //$data['value'] ='fffffffffffff';
            //$this->front_model->setUpdateData('tbl_settings', $data, 36);
            //return false;
        }
    }

    public function verifyIPNOS()
    {
        if ( ! count($_POST)) {
            throw new Exception("Missing POST Data");
        }
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($this->getPaypalUri());
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.
        if ($this->use_local_certs) {
            curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
        }
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));
        $res = curl_exec($ch);
        if ( ! ($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);
            throw new Exception("cURL error: [$errno] $errstr");
        }
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            throw new Exception("PayPal responded with http code $http_code");
        }
        curl_close($ch);
        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::VALID) {
            $data_text = "";
            foreach ($_POST as $key => $value) {
                $data_text .= $key . " = " . $value . "\r\n";
            }
            $oid=$_POST['custom'];
            $data['payment_status'] = $_POST['payment_status'];
            $data['transaction_id'] = $_POST['txn_id'];
            $data['payer_email']    = $_POST['payer_email'];
            $this->front_model->setUpdateDataByWhere('tbl_service_operations', $data, array('service_order_id'=>$oid));
            $this->send_payment_notification_email($oid);
            $this->send_outsatnding_notification_email($oid);
            //return true;
        }
        else
        {
            //$data['value'] ='fffffffffffff';
            //$this->front_model->setUpdateData('tbl_settings', $data, 36);
            //return false;
        }
    }

    public function send_booking_vendor_notification($oid)
    { 
        $msg='';
        $orderData = $this->login_model->getDataCollectionByID('tbl_service_orders',$oid);
        $services_provider = explode(",", $orderData['services_provider']);
        $customer_data = $this->login_model->getDataCollectionByID('tbl_users',$orderData['user_id']);

        if(!empty($services_provider))
        {
            foreach ($services_provider as $key => $userId)
            {
                $vendor_data = $this->login_model->getDataCollectionByID('tbl_users',$userId);
                if(!empty($vendor_data['email']))
                {

                    $email_template  = 'customer-request.html';
                    $templateTags =  array(
                        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                        '{{site_name}}'=>'firstchoice.com',
                        '{{site_url}}'=> base_url(),
                        '{{team_name}}'=>'firstchoice',
                        '{{vendor_name}}'=>$vendor_data['username'],
                        '{{vendor_email}}'=>$vendor_data['email'],
                        '{{service_name}}'=>getServicesName($orderData['services']),
                        '{{customer_name}}'=>$customer_data['username'],
                        '{{customer_email}}'=>$customer_data['email'],
                        '{{customer_contact}}'=>$customer_data['mobile'],
                        '{{year}}'=>date('Y'),
                        '{{company_name}}' => 'firstchoice.com',
                        '{{company_email}}' => 'info@firstchoice.com',
                        );
                    $message = email_compose($email_template,$templateTags);
                    send_email($vendor_data['email'],'Customer request for service',$message);

                    $mobile = $vendor_data['mobile'];
                    $smsMsg = 'Dear '.$vendor_data['username'].', ';
                    $smsMsg.= 'New service lead has recieved to you for your service area which service booking ID is #'.$oid;
                    send_sms($mobile,$smsMsg);
                }
            }
        }
    }

    public function send_booking_customer_notification($oid)
    { 
        $msg='';
        $orderData = $this->login_model->getDataCollectionByID('tbl_service_orders',$oid);
        $customer_data = $this->login_model->getDataCollectionByID('tbl_users',$orderData['user_id']);
        //$customer_data = $this->login_model->getDataCollectionByID('tbl_users',100);
        
        $discount = !empty($orderData['coupon'])?'<tr><td colspan="2" valign="top" align="right"><strong>Discount ('.$orderData['coupon'].') </strong></td><td colspan="2" valign="top" align="left">'.getFormatedPriceByCode($orderData['code'],$orderData['discount']).'</td></tr>':'';

        $tax = !empty($orderData['tax_name'])?'<tr><td colspan="2" valign="top" align="right"><strong>Tax ('.$orderData['tax_name'].') '.$orderData['tax'].'% </strong></td><td colspan="2" valign="top" align="left">'.getFormatedPriceByCode($orderData['code'],$orderData['tax_amount']).'</td></tr>':'';
        
        $address = getAddressDetails($customer_data['address_id']);
        $userAddress = $address['site_location'];
        $mobile = $customer_data['mobile'];
        $email_template  = 'service-booking-notification.html';
        $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}'=>'firstchoice.com',
            '{{site_url}}'=> base_url(),
            '{{team_name}}'=>'firstchoice',
            '{{oid}}'=>$oid,
            '{{user_name}}'=>$customer_data['username'],
            '{{user_email}}'=>$customer_data['email'],
            '{{service_name}}'=>getServicesName($orderData['services']),
            '{{service_address}}'=>$userAddress,
            '{{service_date}}'=>date("d F, Y h:i A",strtotime($orderData['booking_datetime'])),
            '{{service_charge}}'=>getFormatedPriceByCode($orderData['code'],$orderData['amount']),
            '{{discount}}'=>$discount,
            '{{tax}}'=>$tax,            
            '{{total_amount}}'=>getFormatedPriceByCode($orderData['code'],$orderData['total_amount']),
            '{{year}}'=>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
        $message = email_compose($email_template,$templateTags);
        send_email($customer_data['email'],'Service Request notification',$message);

        $smsMsg = 'Dear '.$customer_data['username'].', ';
        $smsMsg.= 'Your service has booked successfully. which service booking ID is #'.$oid;
        send_sms($mobile,$smsMsg);
    }

    public function send_outsatnding_notification_email($oid)
    { 
        $msg='';
        $bookingData = $this->front_model->getServiceBookingDetails('tbl_service_orders', $oid);
        $operation_row = $this->front_model->getDataCollectionByField('tbl_service_operations','service_order_id',$oid);
        $vendor_id = !empty($bookingData->vendor_id)?$bookingData->vendor_id:0;
        $customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
        $currency_code = !empty($bookingData->code)?$bookingData->code:"";

        $start_time = $operation_row['service_start_time'];
        $end_time = $operation_row['service_end_time'];
        $allocated_time = getAllocatedTime($id);
        $time_additional_display = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

        if(!empty($vendor_id))
        {
                $vendor_email = getCustomerEmail($vendor_id);
                $vendor_name = getCustomerName($vendor_id);
                $vendor_phone = getCustomerContact($vendor_id);

                $customer_email = getCustomerEmail($customer_id);
                $customer_name = getCustomerName($customer_id);
                $customer_phone = getCustomerContact($customer_id);

                $additional_subtotal = getFormatedPriceByCode($currency_code,$operation_row['additional_subtotal']);

                $email_template  = 'outstanding-payment.html';
                $templateTags =  array(
                    '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                    '{{site_name}}'=>'firstchoice.com',
                    '{{site_url}}'=> base_url(),
                    '{{team_name}}'=>'firstchoice',
                    '{{booking_id}}'=>$oid,
                    '{{vendor_name}}'=>$vendor_name,
                    '{{vendor_email}}'=>$vendor_email,
                    '{{service_name}}'=>getServicesName($bookingData->services),
                    '{{customer_name}}'=>$customer_name,
                    '{{customer_email}}'=>$customer_email,
                    '{{customer_contact}}'=>$customer_phone,
                    '{{additional_time}}'=>$time_additional_display, 
                    '{{additional_amount}}'=>$additional_subtotal,                   
                    '{{year}}'=>date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com',
                    );
                $message = email_compose($email_template,$templateTags);
                send_email($vendor_email,'Payment of outstanding',$message);
                
            $smsMsg = 'Dear '.$vendor_name.', ';
            $smsMsg.= 'Service outstanding amount '.$additional_subtotal.' has paid by customer against Service Booking ID #'.$oid;
            send_sms($vendor_phone,$smsMsg);
        }
    }

    public function send_payment_notification_email($oid)
    { 
        $msg='';
        $bookingData = $this->front_model->getServiceBookingDetails('tbl_service_orders', $oid);
        $operation_row = $this->front_model->getDataCollectionByField('tbl_service_operations','service_order_id',$oid);
        $user_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
        $currency_code = !empty($bookingData->code)?$bookingData->code:"";
        $email = getCustomerEmail($user_id);
        $name = getCustomerName($user_id);
        $phone = getCustomerContact($customer_id);

        $additional_subtotal = getFormatedPriceByCode($currency_code,$operation_row['additional_subtotal']);

        $email_template  = 'payment-notifcation.html';
        $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}'=>'firstchoice.com',
            '{{site_url}}'=> base_url(),
            '{{team_name}}'=>'firstchoice',
            '{{booking_id}}'=>$oid,
            '{{user_name}}'=>$name,
            '{{user_email}}'=>$email,
            '{{service_name}}'=>getServicesName($bookingData->services),
            '{{amount}}'=>$additional_subtotal,
            '{{year}}'=>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Outstanding payment notification',$message);

        $smsMsg = 'Dear '.$name.', ';
        $smsMsg.= 'Service outstanding amount '.$additional_subtotal.' has received against Service Booking ID #'.$oid;
        send_sms($phone,$smsMsg);
    }
}
?>