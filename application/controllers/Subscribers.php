<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subscribers extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('subscribers_model');
        $this->load->model('front_model');
        $this->load->model('smtp_model');
        $this->load->helper('front_helper');
    }
    
    public $subscribers='tbl_subscribers';

    public function findSearchResult()
    {
        $servicesarea=$this->input->post('servicesarea');
        $this->session->set_userdata('foo','');
        $serviceList=$this->services_model->findServicesByZipCode($servicesarea);
        $html='';
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a data-dismiss="modal" href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></button>'; 
        if(!empty($serviceList)){        
            $html.='<h4 class="modal-title">'.$this->lang->line('SERVICE_OFFERED_IN_YOUR_AREA').'</h4>';
        } else {
            $html.='<h4 class="modal-title">'.$this->lang->line('SERVCE_UNAVAILABLE').'</h4>';
        }
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';       
        $html.='<div class="service-popup-links-block">';  
        $html.='<div id="chooseserviceMsg">';
        $html.='</div>';     
        $html.='<ul class="service-popup-links">';
        if(!empty($servicesarea)){
            $this->session->set_userdata('zipcode', $servicesarea);
        }
        if(!empty($serviceList)){
            foreach ($serviceList as $data) {
                $html.='<li><a class="services_data" href="'.base_url($data->slug).'" service-data="'.$data->id.'">'.$data->name.'</a></li>';
            }
        } else {
            $html.='<li class="srvice_not_available"><strong><i class="fa fa-exclamation-triangle" style="color:red"></i> </strong> <br/>'.$this->lang->line('SERVICE_NOT_AVAILABLE').' <br/> '.$this->lang->line('ARE_YOU_SERVICE_PROVIDERS').'<a href="javascript:void(0)">Click here</a> </li>';
            $html.='<li class="sendQuery">';
            $html.='<div id="errorQueryMsg"></div>';
            $html.='<form method="post" id="sendMyQuery">';
            $html.='<input type="hidden" name="custome_area" value="'.$servicesarea.'">';
            $html.='<input type="text" class="inputQuery" id="inputNameQuery" name="custome_name" placeholder="Your Name*" required="">';
            $html.='<input type="text" class="inputQuery" id="inputEmailQuery" name="custome_email" placeholder="Your Email*">';
            //$html.='<input type="submit" class="submitQuery" name="custome_email" placeholder="Your Email">';
            $html.='</form>';
            $html.='</li>';

        }
        $html.='<ul>';
        
        $html.='</div></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a href="javascript:void(0)" id="chooseAnyServices" class="next-full-btn">Next <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;
    }

    public function send_services_not_found_message()
    {
        $html='';
        $this->send_services_not_found_email();
        //$html.='<div class="modal-header text-center">';      
        //$html.='<button class="back"><a href="#"><i class="fa fa-arrow-left"></i></a></button>';         
        //$html.='<h4 class="modal-title">Thank You  </h4>';
        //$html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        //$html.='</div>';
        $html.='<div class="modal-body">';
        $html.='<div class="gap20"></div>';
        $html.='<div class="thank_request">';                 
        $html.='<div class="thanks_tick"><i class="fa fa-check-circle" aria-hidden="true"></i></div>';
        $html.='<div class="thanks_heading">'.$this->lang->line('THANK_YOU_FOR_FEEDBACK').'</div>';
        $html.='<div class="thanks_msg">'.$this->lang->line('THANK_YOU_FOR_FEEDBACK_MESSAGE').'</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a href="javascript:void(0)"  data-dismiss="modal" class="next-full-btn">Close '; 
        $html.='</a></div>';        
        echo $html;
    }

    public function findLoginDetail()
    {
        $html='';
        $service=$this->input->post('service');
        $newinstalation=$this->input->post('newinstalation');
        if(!empty($newinstalation)){
        	$impload=implode(',',$newinstalation);
            $this->session->set_userdata('newinstalation', $impload);
        }
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMERS_INFORMATION').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';       
        $html.='<div class="service-popup-links-block">';
        $html.='<div id="chooseserviceMsg">';
        $html.='</div>'; 
        $html.='<ul class="service-popup-links">';       
        $html.='<li><a id="newCustomer" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-user"></i> '.$this->lang->line('NEW_CUSTOMERS').'</a></li>';
        $html.='<li><a id="repeatCustomer" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-user"></i> '.$this->lang->line('REPEAT_CUSTOMER').'</a></li>';
        $html.='</ul>';
        $html.='</div></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a id="isTypeOfUser" href="javascript:void(0)" class="next-full-btn">Next <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;

    }
    
    public function newCustomerDetail(){
        $html='';
        $service=$this->input->post('service');
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoLogin" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMER_REGISTRATION').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body" >';       
        $html.='<div class="otp-modal">';        
        $html.='<div class="row justify-content-center">';
        $html.='<div class="col-md-8" id="rbxc">';
        $html.='<div class="inner-login-form">';
        $html.='<div id="infoExist"></div>';
        $html.='<form method="post" id="jserilize">';
        $html.='<input name="userphone" id="userphone" placeholder="Enter Mobile No." type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" required="" class="form-control">';
        $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
        $html.='</form>';                     
        $html.='</div>';
        $html.='</div>'; 
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap20"></div>';
        echo $html;
    }

    public function repeatCustomerDetail(){
		$html='';
        $service=$this->input->post('service');
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoLogin" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('REPEAT_CUSTOMER').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body" >';       
        $html.='<div class="otp-modal">';        
        $html.='<div class="row justify-content-center">';
        $html.='<div class="col-md-8" id="rbxc">';
        $html.='<div class="inner-login-form">';
        $html.='<div id="infoExist"></div>';
        $html.='<form method="post" id="repeatLogin">';
        $html.='<input name="userphone" id="userphone" placeholder="Enter Mobile No." type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" required="" class="form-control">';
        $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
        $html.='</form>';                     
        $html.='</div>';
        $html.='</div>'; 
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap20"></div>';
        echo $html;
    }

    public function getCurrentLocation(){

        $ip_address=$_SERVER['REMOTE_ADDR'];
        $geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
        $geo = curl_init();
        curl_setopt($geo, CURLOPT_URL, $geopluginURL);
        curl_setopt($geo, CURLOPT_RETURNTRANSFER, true);
        $geooutput = curl_exec($geo);
        curl_close($geo);
        $addrDetailsArr=unserialize($geooutput);
        echo '<pre>';
        print_r($addrDetailsArr);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        print_r($location);

    }
    public function phonelogin(){
        $service=$this->input->post('service');
        $userphone=$this->input->post('userphone');
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('userphone', 'mobile', 'required|is_unique[tbl_users.mobile]'); 
            if($this->form_validation->run() == FALSE) { 
               echo '2'; 
            } else {

                $html='';
                $html.='<div class="modal-header text-center">';            
                $html.='<button class="back"><a id="backtophone" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
                $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMER_REGISTRATION').'</h4>';
                $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
                $html.='</div>';
                $html.='<div class="modal-body">';            
                $html.='<div class="otp-modal">';            
                $html.='<div class="row justify-content-center">';
                $html.='<div class="col-md-8" >';
                $html.='<div class="inner-login-form">';            
                $html.='<div class="form-social-ico">';
                $html.='<div id="emailExist"></div>';
                $html.='<form method="post" id="ajxRegistration">';               
                $html.='<input type="hidden" name="service" id="service" value="'.$service.'">';
                $html.='<input type="hidden" name="userphone" id="userphone" value="'.$userphone.'">';
                $html.='<input type="text" placeholder="User Name" name="username" id="username" required="" class="form-control">';
                $html.='<input type="text" placeholder="Email" name="useremail" id="useremail" required="" class="form-control">';
                $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('REGISTRATION').'</button>';
                $html.='</form>'; 
                $html.='</div>';      
                $html.='</div>';
                $html.='</div>'; 
                $html.='</div>';  
                $html.='</div>';
                $html.='</div>';
                $html.='<div class="gap20"></div>';
                echo $html;
            }
        } else { 
             echo '2';
        }        
    }

    public function repeatLogin(){
        $service=$this->session->userdata('services');
        $userphone=$this->input->post('userphone');        
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules('userphone', 'mobile', 'required|is_unique[tbl_users.mobile]'); 
        if($this->form_validation->run() == FALSE) { 
            $this->send_otp($userphone);
            $this->session->set_userdata('userphone', $userphone);
            $html='';
            $html.='<div class="modal-header text-center">';      
            $html.='<button class="back"><a id="backtorepeatphone" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
            $html.='<h4 class="modal-title">'.$this->lang->line('REPEAT_CUSTOMER').'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body" >';       
            $html.='<div class="otp-modal">';        
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-8" id="rbxc">';
            $html.='<div class="inner-login-form">';
            $html.='<div id="infoExist"></div>';
            $html.='<form method="post" id="validateotp">';
            $html.='<input name="userotp" id="userotp" placeholder="Please enter one time password (otp)" type="tel" required="" class="form-control">';
            $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
            $html.='</form>';                     
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap20"></div>';
            echo $html;

        } else {        
           echo '3'; 
        }        
    }

    public function validateotp() {
         
        $service=$this->session->userdata('service');
        $userphone=$this->session->userdata('userphone');
        $userotp=$this->input->post('userotp');
        $ok=chckValidOtp($userotp);
        if($ok) {        
        $this->session->set_userdata('userID',$ok);
        $user_id = $this->session->userdata('userID');
        if(!empty($user_id)) {                  
            $html='';
            $html.='<div class="modal-header text-center">';            
            $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-arrow-left"></i></a></button>';
            $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';            
            $html.='<div class="otp-modal">';            
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-12" >';
            $html.='<div class="inner-login-form">';            
            $html.='<div class="form-social-ico">';             
            $html.='<div class="service-popup-links">';
            $html.='<div class="text-center">';
            $html.='<h4 class="chooseAddress">Where do you require the service?</h4>';
            $html.='</div>';
            $html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
            $html.='<div class="newu-loc-box">';
            $html.='<input type="hidden" name="street_number" id="street_number">';          
            $html.='<input type="hidden" name="route" id="route">';          
            $html.='<input type="hidden" id="latitude" name="site_latitude">';          
            $html.='<input type="hidden" id="longitude" name="site_longitude">';            
            $html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="Enter Location" class="form-control" required>';
            $html.='<input type="submit" value="Add" name="add-user-loc" class="btn btn-gray" >';
            $html.='</div>';
            $html.='<input type="hidden" id="country" name="country" value="">';
            $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
            $html.='<input type="hidden" id="locality" name="city" value="">';
            $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
            $html.='</form>';            
            $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
            $html.='<ul class="custom-r-btn" id="addresListMsg">';
            $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
            if(!empty($addresList)){
                foreach ($addresList as $addressData)
                {    
                    $id=isset($addressData->id)?$addressData->id:"";
                    $html.='<li class="custom-r-btn-col w-100">';
                    $html.='<div class="row align-items-center">';              
                    $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                    $html.='<label class="radio-btnn">';
                    $html.=isset($addressData->site_location)?$addressData->site_location:"";
                    $html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
                    $html.='<span class="checkmark"></span>';
                    $html.='</label>';       
                    $html.='</div>';
                    $html.='<div class="edit-del-btn col-3">';
                    //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                    $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                    $html.='</div>';
                    $html.='</div>';
                    $html.='</li>';     
                }
            }   
            $html.='</ul>';
            $html.='</form>';
            $html.='</div>';      
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap10"></div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">Next <i class="fa fa-arrow-right"></i></a>'; 
            $html.='</div>';            
            echo $html;

        } else {        
           echo '3'; 
        }    
        } else {        
           echo '3'; 
        }  
    }

    public function ajaxUserRegistration(){
        $service=$this->session->userdata('services');
        $userphone=$this->input->post('userphone');
        $useremail=$this->input->post('useremail');
        $username=$this->input->post('username');        
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('useremail', 'email ID', 'required|valid_email|is_unique[tbl_users.email]',array('is_unique' => 'Applicant email ID already exist.')); 
            if($this->form_validation->run() == FALSE) { 
               echo '2'; 
            } else {
                $newinstalation=$this->session->userdata('newinstalation');
                $userdata   = array(
                'instalation'=>$newinstalation,
                'services'  =>$service
                );          
                $this->session->set_userdata($userdata);
                $user_id = $this->session->userdata('userID');
                if(empty($user_id)){                    
                    $insertData['username'] =$username;
                    $insertData['email']    =$useremail;
                    $insertData['mobile']   =$userphone;
                    $user_id=$this->front_model->setInsertData($this->users, $insertData); 
                    $this->session->set_userdata('userID',$user_id);
                    $this->session->set_userdata('user_email',$useremail);
                    $this->session->set_userdata('user_name',$username);
                }
                $html='';
                $html.='<div class="modal-header text-center">';            
                $html.='<button class="back"><a id="backtoregistration" href="javascript:void(0)" service-data="'.$service.'" phone-data="'.$userphone.'" ><i class="fa fa-arrow-left"></i></a></button>';
                $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
                $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
                $html.='</div>';
                $html.='<div class="modal-body">';            
                $html.='<div class="otp-modal">';            
                $html.='<div class="row justify-content-center">';
                $html.='<div class="col-md-12" >';
                $html.='<div class="inner-login-form">';            
                $html.='<div class="form-social-ico">';             
                $html.='<div class="service-popup-links">';
                $html.='<div class="text-center">';
                $html.='<h4 class="chooseAddress">'.$this->lang->line('WHERE_DO_YOU_REQUIRED_SERVICES').'</h4>';
                $html.='</div>';                
                $html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
                $html.='<div class="newu-loc-box">';
                $html.='<input type="hidden" name="street_number" id="street_number">';          
                $html.='<input type="hidden" name="route" id="route">';          
                $html.='<input type="hidden" id="latitude" name="site_latitude">';          
                $html.='<input type="hidden" id="longitude" name="site_longitude">';            
                $html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="Enter Location" class="form-control" required>';
                $html.='<input type="submit" value="Add" name="add-user-loc" class="btn btn-gray" >';
                $html.='</div>';
                $html.='<input type="hidden" id="country" name="country" value="">';
                $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
                $html.='<input type="hidden" id="locality" name="city" value="">';
                $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
                $html.='</form>';
                $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
                $html.='<ul class="custom-r-btn" id="addresListMsg">';
                $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
                if(!empty($addresList)){
                    foreach ($addresList as $addressData) {    

                        $id=isset($addressData->id)?$addressData->id:"";
                        $html.='<li class="custom-r-btn-col w-100">';
                        $html.='<div class="row align-items-center">';              
                        $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                        $html.='<label class="radio-btnn">';
                        $html.=isset($addressData->site_location)?$addressData->site_location:"";
                        $html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
                        $html.='<span class="checkmark"></span>';
                        $html.='</label>';       
                        $html.='</div>';
                        $html.='<div class="edit-del-btn col-3">';
                        //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                        $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                        $html.='</div>';
                        $html.='</div>';
                        $html.='</li>';     
                    }
                }   
                $html.='</ul>';
                //addhomeaddress
                $html.='</form>';
                
                $html.='</div>'; 
                $html.='</div>';      
                $html.='</div>';
                $html.='</div>'; 
                $html.='</div>';  
                $html.='</div>';
                $html.='</div>';
                $html.='<div class="gap10"></div>';
                $html.='<div class="modal-footer text-center">';
                $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">Next <i class="fa fa-arrow-right"></i></a>';  
                 $html.='</div>';
                //$html.=print_r($this->session->userdata());
                echo $html;
            }
        } else { 
             echo '2';
        }        
    }

    public function ajaxForgotPassword()
    {
        $useremail=$this->input->post('useremail');
        if(!empty($useremail))
        { 
            $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$useremail);
            if(!empty($userdata))
            {
                //print_r($userdata);
                $token = md5(rand(11111,99999));                    
                if($this->front_model->setUpdateData($this->users,array('token'=>$token),$userdata['id']))
                {
                    $resetUrl = base_url('reset-password')."/?email=".$userdata['email']."&token=".$token;
                    $email_template  = 'forgot-pass.html';
                    $templateTags =  array(
                    '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                    '{{site_name}}'=>'firstchoice.com',
                    '{{site_url}}'=> base_url(),
                    '{{team_name}}'=>'firstchoice',
                    '{{name}}'=>$userdata['username'],
                    '{{email}}'=>$userdata['email'],
                    '{{resetUrl}}'=>$resetUrl,
                    '{{year}}'=>date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com',
                    );
                    $message = email_compose($email_template,$templateTags);
                    send_email($userdata['email'],'Forgot Password',$message);
                    echo '<div class="success">Email with reset url has sent to your inbox.</div>';
                }
                else
                {
                    echo '<div class="error">Oops: Something going wrong, please try again.</div>';
                }
            }
            else
            {
                echo '<div class="error">Email address does not exist.</div>';
            }
        }      
    }

    public function resetPassword()
    {
        $get_data=$this->input->get();
        $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$get_data['email']);
        if(!empty($userdata))
        {
            if($userdata['token'] == $get_data['token'])
            {
                $post_data=$this->input->post();
                if(!empty($post_data)) {
            
                    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
                    $this->form_validation->set_rules('password', 'Password', 'trim|required');
                    $this->form_validation->set_rules('repassword', 'Confirm Password ', 'trim|required|matches[password]');        
                    if($this->form_validation->run() != FALSE)
                    {
                        $newPassword = md5($post_data['password']);
                        if($this->front_model->setUpdateData($this->users,array('password'=>$newPassword),$userdata['id']))
                        {
                            $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your password has Successfully changed.'));
                        }
                        else
                        {
                            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Invalid email or password.'));
                        }
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Token has been expired, Please try again.'));
            }
        }
        else
        {
            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'User with this email are not exist.'));
        }

        $this->load->view('front/include/header');
        $this->load->view('front/services/reset_password');
        $this->load->view('front/include/footer');
        $this->load->view('front/include/modals');
    }
    public function ajaxChooseAddress(){
        $service=$this->session->userdata('services');        
        $user_id = $this->session->userdata('userID');
        $newinstalation=$this->input->post('newinstalation');
        if(!empty($newinstalation)){
            $impload=implode(',',$newinstalation);
            $this->session->set_userdata('newinstalation', $impload);
        }
        if(!empty($user_id)){                  
    		$html='';
            $html.='<div class="modal-header text-center">';            
            $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-arrow-left"></i></a></button>';
            $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';            
            $html.='<div class="otp-modal">';            
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-12" >';
            $html.='<div class="inner-login-form">';            
            $html.='<div class="form-social-ico">';	            
            $html.='<div class="service-popup-links">';
    		$html.='<div class="text-center">';
    		$html.='<h4 class="chooseAddress">'.$this->lang->line('WHERE_DO_YOU_REQUIRED_SERVICES').'</h4>';
    		$html.='</div>';
    		$html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
    		$html.='<div class="newu-loc-box">';
            $html.='<input type="hidden" name="street_number" id="street_number">';          
    		$html.='<input type="hidden" name="route" id="route">';          
    		$html.='<input type="hidden" id="latitude" name="site_latitude">';          
            $html.='<input type="hidden" id="longitude" name="site_longitude">';            
        	$html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="Enter Location" class="form-control" required>';
        	$html.='<input type="submit" value="Add" name="add-user-loc" class="btn btn-gray" >';
        	$html.='</div>';
            $html.='<input type="hidden" id="country" name="country" value="">';
            $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
            $html.='<input type="hidden" id="locality" name="city" value="">';
            $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
            $html.='</form>';
            
            
            $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
        	$html.='<ul class="custom-r-btn" id="addresListMsg">';
            $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
            if(!empty($addresList)){
                foreach ($addresList as $addressData) {    

                    $id=isset($addressData->id)?$addressData->id:"";
                    $html.='<li class="custom-r-btn-col w-100">';
                    $html.='<div class="row align-items-center">';              
                    $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                    $html.='<label class="radio-btnn">';
                    $html.=isset($addressData->site_location)?$addressData->site_location:"";
            		$html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
            		$html.='<span class="checkmark"></span>';
            		$html.='</label>';		 
            		$html.='</div>';
            		$html.='<div class="edit-del-btn col-3">';
                    //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                    $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                    $html.='</div>';
            		$html.='</div>';
            		$html.='</li>';		
    			}
            }	
    		$html.='</ul>';
    		$html.='</form>';
            $html.='</div>';      
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap10"></div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">Next <i class="fa fa-arrow-right"></i></a>'; 
            $html.='</div>';
            

            echo $html;	    
    	}        
    }

    public function chooseDatetime(){
        $radio=$this->input->post('radio');  
        if(!empty($radio)){
            $this->session->set_userdata('address_id',$radio);
        }
        $service=$this->session->userdata('services');        
        $html='';
        $html.='<div class="modal-header text-center">';            
        $html.='<button class="back"><a id="backtoaddress" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
        $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';            
        $html.='<div class="otp-modal">';            
        $html.='<div class="row justify-content-center">';
        $html.='<div class="col-md-8" >';
        $html.='<div class="inner-login-form">';            
        $html.='<div class="form-social-ico">';             
        $html.='<div class="service-popup-links">';
        $html.='<div class="text-center">';
        $html.='<h4>Select date of service</h4>';
        $html.='</div>'; 
        $html.='<form method="post" id="setDatetime">';       
        $html.='<div style="width: 250px; margin: 30px auto;">';
        $html.='<div id="datpicker" class="dtp_main"><span>'.date("Y-m-d").'</span><i class="fa fa-calendar ico-size"></i><span>'.date("H:i").'</span><i class="fa fa-clock-o ico-size"></i></div>';
        $html.='<div id="picker"></div>';
        $html.='<input type="hidden" id="result" value="'.date("Y-m-d H:i").'" />';
        $html.='</form>'; 
        $html.='</div>'; 
        $html.='</div>'; 
        $html.='</div>';      
        $html.='</div>';
        $html.='</div>'; 
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap10"></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a id="addServiceOrder" href="javascript:void(0)" class="next-full-btn">Next <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;             
    }

    public function serviceOrderReview(){
        $service=$this->session->userdata('services');
        $datetime=$this->input->post('datetime');  
        if(!empty($datetime)){
            $this->session->set_userdata('datetime',$datetime);  
        }     
        $html='';
        $html.='<div class="modal-header text-center">';            
        $html.='<button class="back"><a id="backtodatetime" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
        $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';                   
        $html.='<div class="form-social-ico">';             
        $html.='<div class="service-popup-links">';
        $html.='<div class="text-center">';
        $html.='<h4>Pricing Details</h4>';
        $html.='</div>';
        $html.='<form method="post" id="gotoPayment">'; 
        $html.='<div class="price_detail">'; 
        $language_id=$this->front_model->getLanguage();
        $servicList=$this->services_model->getServicesOptionList($language_id,$service,1);
        $subtotal=0;
        if(!empty($servicList)){ 
            foreach ($servicList as $servicedata) {
                $serviceName=isset($servicedata->name)?$servicedata->name:"";
                $serviceCode=isset($servicedata->id)?$servicedata->id:"";
                $servicPrice=$servicedata->price;
                $html.='<div class="row">'; 
                $html.='<div class="col-sm-9 text-left">';
                $html.='<p><strong>'.$serviceName.'</strong></p>';
                $html.='<p>'.$servicedata->availability.'</p>';  
                $html.='</div>';   
                $html.='<div class="col-sm-3 text-right">'; 
                $html.='<p><strong>'.getPriceFormate($servicPrice).'</strong></p>';    
                $html.='</div>';   
                $html.='</div>'; 
                $subtotal=$subtotal+$servicPrice;
            }
        }       

        $html.='<div class="row total">';
        $html.='<div class="col-sm-9 text-left">'; 
        $html.='<p><strong>SubTotal</strong></p>';   
        $html.='</div>';    
        $html.='<div class="col-sm-3 text-right">'; 
        $html.='<p><strong>'.getPriceFormate($subtotal).'</strong></p>';    
        $html.='</div>';   
        $html.='</div>';  

        $html.='</div>';
        $html.='</form>'; 
        //$html.=$this->session->userdata('address_id'); 
        //$html.=$this->session->userdata('datetime'); 
        $html.='<div class="privacy-pol">By submitting request, you accept our <a target="_blanck" href="'.base_url('terms-conditions').'">terms of use</a> and <a target="_blanck" href="'.base_url('privacy-policies').'">privacy policy</a></div>'; 
        $html.='</div>';  
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap10"></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a id="choosertopayment" href="javascript:void(0)" class="next-full-btn">Next <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;             
    }

    public function choosePaymentOption(){
        $service=$this->session->userdata('services');               
		$html='';
        $html.='<div class="modal-header text-center">';            
        $html.='<button class="back"><a id="backtodatetime" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
        $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';                   
        $html.='<div class="form-social-ico">';	            
        $html.='<div class="service-popup-links">';
		$html.='<div class="text-center">';
		$html.='<h4>Payment Method</h4>';
        $html.='</div>';
		$html.='<form method="post" id="gotoPayment" action="'.base_url('order-processing').'">';

        $html.='<div class="user-addres-pop">';		
        $html.='<ul class="custom-r-btn">		
		<li class="pay-r-btn-col w-100">
		<div class="row align-top">';								
		$html.='<div class="col-sm-6">';
		$html.='<ul class="paymentMemo pay-deatil">';
		$html.='<li class="col-sm-12 user-serv-pop pay-rb">';
		$html.='<label class="radio-btnn"><img src="'.base_url('skin/front/images/paypal-logo.jpg').'">';
		  $html.='<input type="radio" name="method" checked="checked" value="paypal">';
		  $html.='<span class="checkmark"></span>';
		$html.='</label>';
		$html.='</li>';
		$html.='<li class="col-sm-12 user-serv-pop pay-rb">';
		$html.='<label class="radio-btnn"><img src="'.base_url('skin/front/images/cod-logo.jpg').'">';
		  $html.='<input type="radio" name="method" value="cod">';
		  $html.='<span class="checkmark"></span>';
		$html.='</label>';
		$html.='</li>';
		$html.='<ul>';
		$html.='</div>';			 
			 			 
	   $html.='<div class="col-sm-6">';
       $html.='<ul class="priceMemo pay-deatil">';
        $language_id=$this->front_model->getLanguage();
        $servicList=$this->services_model->getServicesOptionList($language_id,$service,1);
        $subtotal=0;
        if(!empty($servicList)){ 
            foreach ($servicList as $servicedata) {
                $serviceName=isset($servicedata->name)?$servicedata->name:"";
                $serviceCode=isset($servicedata->id)?$servicedata->id:"";
                $servicPrice=$servicedata->price;    
                $html.='<li>'.$serviceName.' <span>'.getPriceFormate($servicPrice).'</span></li>';            
                $subtotal=$subtotal+$servicPrice;

            }
        } 

	    $html.='<li><strong>Service Amount Payable</strong> <span><strong>'.getPriceFormate($subtotal).'</strong></span></li>'; 
	    $html.='<li><strong>Amount Payable</strong> <span><strong>'.getPriceFormate($subtotal).'</strong></span></li>';        	
        $html.='</ul>';
		$html.='</div>
		</div>
		</li>
        </ul>';
        $html.='<input type="hidden" name="amount" value="'.$subtotal.'">';
        $html.='</div>';  
        $html.='</div>';  
        $html.='</form>'; 
        
		$html.='</div>';  
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap10"></div>';
        $html.='<div class="modal-footer text-center">';
		$html.='<a id="confirmpayment" href="javascript:void(0)" class="next-full-btn">Confirm Payment <i class="fa fa-arrow-right"></i>'; 
		$html.='</a></div>';
        echo $html;    	        
    } 

    public function findPriceingListing(){
        $user_id = $this->session->userdata('userID');
        $service=$this->input->post('service');
        $isFoo=$this->input->post('foo');
        if(!empty($service)){
            $this->session->set_userdata('services',$service);
        }
        if(!empty($isFoo)){
            $this->session->set_userdata('foo',$isFoo);
        }
        $foo=$this->session->userdata('foo');
        $language_id=$this->front_model->getLanguage();
        $servicList=$this->services_model->getServicesOptionList($language_id,$service);
        $servicesData=$this->services_model->getSingleServicesDataCollection($language_id,$service);

        if(!empty($servicesData)){        
            $html='';
            $html.='<div class="modal-header text-center">';   
            if(empty($foo)){   
            $html.='<button class="back"><a id="backtoServices" href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></button>'; 
            }        
            $html.='<h4 class="modal-title">'.$servicesData->name.'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';   
            $html.='<div class="service-popup-links">';
            $html.=$servicesData->description;          
            if(!empty($servicList)){
            $html.='<p>(All applicable taxes in your area are extra.)</p>';
            $html.='<form method="post" id="installatioServices">';
            $html.='<input type="hidden" value="'.$service.'" name="service">';
            $html.='<div class="table checkinstalation">';
                foreach ($servicList as $servicesinfo) {
                    $html.='<p>';                    
                    
                    if($servicesinfo->availability==''){
	                    $html.='<label class="l-one">';
						$html.='<input type="checkbox" value="'.$servicesinfo->id.'"  name="newinstalation[]">';
						$html.='<span class="checkmark"></span>'.$servicesinfo->name;
						$html.='</label>'; 
				    }
                    else
                    {
				    $html.='<span class="l-one">';
                    $html.=$servicesinfo->name.'</span>'; 
                    }
                    $html.='<span class="l-two">'.!empty($servicesinfo->availability)?$servicesinfo->availability:'1 Hours'.'</span>';  
                    $html.='<span class="l-three">'.getPriceFormate($servicesinfo->price).'</span>';
                    $html.='</p>';
                }
            $html.='</div>';
            $html.='</form>';
            } else {
                $html.='<p class="srvice_not_available"><strong><i class="fa fa-exclamation-triangle" style="color:red"></i> </strong> <br>'.$this->lang->line('SERVICE_NOT_AVAILABLE').'</p>';
            }
            $html.='<p>Time starts when the Service Provider arrives to your location and ends when job is completed.</p>
            <p class="m-0">Service Provider has some common parts with him, if he has to go and get the parts and come back we charge only 30 minutes for picking up the part not for the full time that Service Provider is away</p>
          </div>'; 

            $html.='</div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="loginMe" userId="'.$user_id.'" service-data="'.$service.'" class="next-full-btn">Next <i class="fa fa-arrow-right"></i>'; 
            $html.='</a></div>';
            //$ip = $_SERVER['REMOTE_ADDR'];
            //$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            //echo $details->city;
        }
        echo $html;
    }

    public function addNewAddress(){
        $user_id = $this->session->userdata('userID');
        $site_location=$this->input->post('site_location');
        $data['user_id']=$user_id;
        $data['site_location']=$this->input->post('site_location');
        $data['street_number']=$this->input->post('street_number');
        $data['route']=$this->input->post('route');
        $data['latitude']=$this->input->post('site_latitude');
        $data['longitude']=$this->input->post('site_longitude');
        $data['country']=$this->input->post('country');
        $data['state']=$this->input->post('state');
        $data['city']=$this->input->post('city');
        $data['postal_code']=$this->input->post('postal_code');
        $insertId=$this->front_model->setInsertData($this->users_address, $data);  
        $html='';
        if($insertId>0){
            $html.='<li class="custom-r-btn-col w-100">';
            $html.='<div class="row align-items-center">';              
            $html.='<div class="user-addres-pop col-sm-8 cus-rb">';  
            $html.='<label class="radio-btnn">'.$site_location;
            $html.='<input type="radio" name="radio" checked="checked" value="'.$insertId.'" required>';
            $html.='<span class="checkmark"></span>';
            $html.='</label>';       
            $html.='</div>';
            $html.='<div class="edit-del-btn col-3">';
            //$html.='<a href="javascript:void(0)" editId="'.$insertId.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
            $html.='<a href="javascript:void(0)" delId="'.$insertId.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
            $html.='</div>';
            $html.='</div>';
            $html.='</li>';
        }
        echo $html;
    }

    public function deleteUserAddress(){
       $id=$this->input->post('id');
       $this->front_model->setStatusData($this->users_address, $id);
       echo 'Your address has been delete successfully';
    }

    public function details(){
        $data[]='';
        $this->load->view('front/include/header', $data);
        $this->load->view('front/services/details', $data);
        $this->load->view('front/include/footer', $data);
        $this->load->view('front/include/modals', $data);
    }

    public function email_compose($email_template,$templateTags){
        $templateContents = file_get_contents( dirname(__FILE__) . '/email-templates/'.$email_template);
        return $message =  strtr($templateContents, $templateTags);
    }

    public function send_services_not_found_email() { 
        $msg='';
        $email = getAdministratorEmail();
        $custome_area=$this->input->post('custome_area');
        $custome_name=$this->input->post('custome_name');
        $custome_email=$this->input->post('custome_email');      

        $smtp_host=$this->smtp_model->getSmtp('smtp_host');
        $smtp_port=$this->smtp_model->getSmtp('smtp_port');
        $smtp_user=$this->smtp_model->getSmtp('smtp_user');
        $smtp_pass=$this->smtp_model->getSmtp('smtp_pass');
        $config                 = array(
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'priority' => '1',
        'newline' => '\r\n'
        );
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $smtp_host;
        $config['smtp_port']    = $smtp_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $smtp_user;
        $config['smtp_pass']    = $smtp_pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $this->email->initialize($config);
        $this->email->from($smtp_user, 'firstchoice');
        $this->email->to($email);
        $this->email->subject('Services Request');             
        $email_template  = 'services_request.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$custome_name,
        '{{custome_area}}'=>$custome_area,
        '{{custome_email}}'=>$custome_email,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = $this->email_compose($email_template,$templateTags);
        $this->email->message($message); 
        if(!$this->email->send()) {

            $subject = "Services Request";
            $from = "firstchoice<support@markupdesigns.info>";
            $headers = "From: $from\r\n";
            $headers .= "MIME-Version: 1.0\r\n"
            ."Content-Type: multipart/mixed; boundary=\"1a2a3a\""; 
            $msg .= "If you can see this MIME than your client doesn't accept MIME types!\r\n" ."--1a2a3a\r\n"; 
            $msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
              ."Content-Transfer-Encoding: 7bit\r\n\r\n";
            $msg .= $message."\r\n";
            $msg .= "--1a2a3a\r\n"; 
            $success = mail($email,$subject,$msg,$headers);
        }
    }

    public function requestStepOne(){ 
    	$html='';
        $post=$this->input->post();
        if(!empty($post)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('phonecode', 'mobile', 'required|is_unique[tbl_users.mobile]',array('is_unique' => 'Applicant mobile number already exist.')); 
            if($this->form_validation->run() == FALSE) { 
               echo '2'; 
            } else {
                $chooseserices=$this->input->post('chooseserices');
                $phonecode=$this->input->post('mobbox');
                $message=$this->input->post('message');
                $business=$this->input->post('business');
                $business_name=$this->input->post('business_name');
                $designation=$this->input->post('designation');
                $license=$this->input->post('license');
                $license_details=$this->input->post('license_details');
                
            	$html.='<form role="form" id="requestStepOne" action="" class="personalDetails" method="post" >';
                $html.='<input type="hidden" value="'.(isset($business)?$business:"").'" name="business" id="business">';                
                $html.='<input type="hidden" value="'.(isset($business_name)?$business_name:"").'" name="business_name" id="business_name">';                
                $html.='<input type="hidden" value="'.(isset($designation)?$designation:"").'" name="designation" id="designation">';                
                $html.='<input type="hidden" value="'.(isset($license)?$license:"").'" name="license" id="license">';                
                $html.='<input type="hidden" value="'.(isset($license_details)?$license_details:"").'" name="license_details" id="license_details">';                
                $html.='<input type="hidden" value="'.(isset($chooseserices)?$chooseserices:"").'" name="chooseserices" id="chooseserices">';                
                $html.='<input type="hidden" value="'.(isset($phonecode)?$phonecode:"").'" name="phonecode" id="phonecode">';
                $html.='<input type="hidden" value="'.(isset($message)?$message:"").'" name="message" id="message">';                       
                $html.='<input type="hidden" value="'.(isset($site_latitude)?$site_latitude:"").'" id="latitude" name="site_latitude"/>';          
                $html.='<input type="hidden" value="'.(isset($site_longitude)?$site_longitude:"").'" id="longitude" name="site_longitude"/>';
            	$html.='<fieldset>
        		<div class="form-group jformgroup">
        		<input type="text" name="fname" placeholder="First Name" class="f1-email form-control" id="f-name" required="">
        		</div>
                <div class="form-group jformgroup">
                <input type="text" name="lname" placeholder="Last Name" class="f1-email form-control" id="l-name" required="">
                </div>
        		<div class="form-group jmargin">
        		<input type="email" name="useremail" placeholder="Email..." pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" class="f1-email form-control" id="f1-email" required="">
        		</div>
                <div class="form-group jmargin">
                <input type="text" onclick="initialize();" placeholder="Address" name="site_location"  class="f1-last-name form-control" id="site_location" required="">
                </div>
                <div class="form-group jformgroup">               
                <input type="text" name="aptno" placeholder="Apt/Unit No" class="f1-email form-control" id="aptno-name" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_number" placeholder="Street No" class="f1-email form-control" id="street_number" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_name" placeholder="Street Name" class="f1-email form-control" id="street_name-name" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_type" placeholder="Street Type" class="f1-email form-control" id="street_type-name" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="direction" placeholder="Direction" class="f1-email form-control" id="route" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="country" placeholder="Country" class="f1-email form-control" id="country" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="state" placeholder="State/Province" class="f1-email form-control" id="administrative_area_level_1" required="">
                </div> 
                <div class="form-group jformgroup">
                <input type="text" name="city" placeholder="City" class="f1-email form-control" id="locality" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="postal_code" placeholder="Postl/Zip code" class="f1-email form-control" id="postal_code" required="">
                </div>
                
        		<div class="form-group jformgroup">
        		<input type="password" name="password" placeholder="Password..." class="f1-password form-control" id="password">
        		</div>
        		<div class="form-group jformgroup">
                <input type="password" name="confirm_password" placeholder="Repeat password..." 
                class="f1-repeat-password form-control" id="confirm_password" required>
                </div>
        		<div class="f1-buttons">
        		<button type="submit" class="btn btn-next choosePersonalNext">Next</button>
        		</div>
        		</fieldset>';
            	$html.='</form>';
    	        echo $html;
            }
        }
    }

    public function requestStepThanks(){ 
        $html='';
        $post_data=$this->input->post();
        if(!empty($post_data))
        { 
            $userdata['username']=$this->input->post('fname').' '.$this->input->post('lname');
            $userdata['fname']=$this->input->post('fname');
            $userdata['lname']=$this->input->post('lname');
            $userdata['email']=$this->input->post('useremail');
            $userdata['site_location']=$this->input->post('site_location');
            $userdata['aptno']=$this->input->post('aptno');
            $userdata['street_number']=$this->input->post('street_number');
            $userdata['street_name']=$this->input->post('street_name');
            $userdata['street_type']=$this->input->post('street_type');
            $userdata['direction']=$this->input->post('direction');
            $userdata['country']=$this->input->post('country');
            $userdata['state']=$this->input->post('state');
            $userdata['city']=$this->input->post('city');
            $userdata['postal_code']=$this->input->post('postal_code');
            $userdata['latitude']=$this->input->post('site_latitude');
            $userdata['longitude']=$this->input->post('site_longitude');                
            $userdata['password']=md5($this->input->post('password'));
            $userdata['mobile']=$this->input->post('phonecode');
            $userdata['role']=5;

            //print_r($userdata); die;
            $insertId=$this->front_model->setInsertData($this->users, $userdata);
            
            if($insertId>0)
            {                     
                $data['services_type']=$this->input->post('chooseserices');
                $data['message']=$this->input->post('message');
                $data['business']=$this->input->post('business');
                $data['business_name']=$this->input->post('business_name');
                $data['designation']=$this->input->post('designation');
                $data['license']=$this->input->post('license');
                $data['license_details']=$this->input->post('license_details');
                $data['request_date']=date("Y-m-d h:i:s");
                $data['service_status']=2;
                $data['user_id']=$insertId;

                $service_request_id = $this->front_model->setInsertData($this->provider_request, $data);
                if($service_request_id>0)
                {                        
                    $data2['service_id']=$this->input->post('chooseserices');                     
                    $data2['services_request_id']=$service_request_id;      
                    $data2['service_zip_code']=$this->input->post('postal_code');
                    $data2['service_category_code']=$this->input->post('chooseserices');
                    $this->front_model->setInsertData($this->provider_request_setting, $data2);

                    $addr['zipcode']=$this->input->post('postal_code');
                    $addr['latitude']=$this->input->post('site_latitude');
                    $addr['longitude']=$this->input->post('site_longitude');
                    $addr['country']=$this->input->post('country');
                    $addr['place_name']=$this->input->post('state');
                    $addr['area']=$this->input->post('city');
                    $addr['address']=$this->input->post('site_location');

                    $this->front_model->checkNinsertData($this->services_area,'zipcode', $addr);
                }
                $this->send_provider_thank_you();
                $this->send_provider_request_to_administrator();
                $html.='<fieldset>                    
                   <div class="bus-part-success">
                    <i class="fa fa-check"></i>
                    <h4>Congratulations!</h4>
                    <h5>You have successfully signed up as a partner on First Choice Everything</h5>
                    <p>Thank you for requesting information. We will email you the infor mation you need.</p>
                    <p>In that information pavkage we will explain how our system work and a form (Form 102)</p>
                    <p>If you need more information you will fill that form and we will send you more deatails.</p>
                   </div>
                </fieldset>';
            }
            echo $html;
        }
        
    }

    public function providerAcceptRequest() { 

        $id=base64_decode($this->uri->segment(2));
        $user_id=$this->session->userdata('userID');
        $dataupdate['vendor_id']=$user_id;
        $dataupdate['status']   =1;
        $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
        $this->session->set_flashdata('message','You have accept request successfully');
        redirect('customer-request', 'refresh');
    }

    public function customerCancelRequest() { 

        $id=base64_decode($this->uri->segment(2));
        $dataupdate['status']   =2;
        $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
        $this->session->set_flashdata('message','You have cancel request successfully');
        redirect('my-bookings', 'refresh');
    }

    public function reschedule() { 
        $segment=$this->uri->segment(2);
        $id=base64_decode($segment);
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('reschedule_date', 'reschedule date', 'required'); 
            $this->form_validation->set_rules('resion', 'resion', 'required'); 
            if($this->form_validation->run() == FALSE) { 
               $this->session->set_flashdata('error','Please choose reschedule date and resion');
            } else {
            $dataupdate['booking_datetime']   =$this->input->post('reschedule_date');
            $dataupdate['resion']   =$this->input->post('resion');
            $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
            $this->session->set_flashdata('message','You have reschedule request successfully');
            }
        }
        redirect('booking-details/'.$segment, 'refresh');
    }

    public function providerCancelRequest() { 

    }
    public function order_processing() { 

        $post=$this->input->post();
        if(!empty($post)){
            $paymentMethod=$this->input->post('method');
            $data['amount']=$this->input->post('amount');
            $user_id = $this->session->userdata('userID');
            $address_id = $this->session->userdata('address_id');
            $services = $this->session->userdata('services');
            $datetime = $this->session->userdata('datetime');
            $newinstalation = $this->session->userdata('newinstalation');
            $addressDetails = getAddressDetails($address_id);
            $nearestUser = findNearestUsers($addressDetails);
            $nearestUser=array_column($nearestUser,'id');
            $imploadUser = implode (',' , $nearestUser);
            if(!empty($newinstalation)){

                $ordesinfo['other_services']=$newinstalation;

            } else {

                $ordesinfo['other_services']=0;
            }
            $data['code']=getCurrencySign();
            $ordesinfo['code']=getCurrencySign();
            $ordesinfo['booking_datetime']=$datetime;
            $ordesinfo['user_id']=$user_id;
            $ordesinfo['services_provider']=$imploadUser;
            $ordesinfo['address_id']=$address_id;
            $ordesinfo['services']=$services;
            $ordesinfo['amount']=$this->input->post('amount');
            $ordesinfo['method']=$paymentMethod;
            $insertId=$this->front_model->setInsertData($this->service_orders, $ordesinfo);
            //$orderId=$this->session->userdata('orderId');
            $data['order_id']=$insertId;
            //$data['ordersDetails']=getOrderDetailsById($orderId);
            $data['userData']=getCustomerDetailsById($user_id);
            if($paymentMethod=='cod'){

            } else {            
                $this->load->view('front/payment/paypal-services', $data);            
            } 
        } else {
            redirect('/','refresh');
        }        
    }

    public function serviceBookingDetails() {
        CheckUserLoginSession();
        $id=base64_decode($this->uri->segment(2));
        $data['bookingData']=$this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $data['page_title']='Booking Details';
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        //$this->load->view('front/users/sidebar', $data);
        $this->load->view('front/orders/booking-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function send_reschedule_email() { 
        $msg='';        
        
        $email='m.jabir@markupdesigns.com';      
        $smtp_host=$this->smtp_model->getSmtp('smtp_host');
        $smtp_port=$this->smtp_model->getSmtp('smtp_port');
        $smtp_user=$this->smtp_model->getSmtp('smtp_user');
        $smtp_pass=$this->smtp_model->getSmtp('smtp_pass');
        $config                 = array(
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'priority' => '1',
        'newline' => '\r\n'
        );
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $smtp_host;
        $config['smtp_port']    = $smtp_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $smtp_user;
        $config['smtp_pass']    = $smtp_pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $this->email->initialize($config);
        $this->email->from($smtp_user, 'Firstchoice');
        $this->email->to($email);
        $this->email->subject('Reschedule');             
        $email_template  = 'reschedule.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{otp}}'=>$otp,
        '{{mobile}}'=>$mobile,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = $this->email_compose($email_template,$templateTags);
        $this->email->message($message); 
        if(!$this->email->send()) {

            $subject = "Reschedule";
            $from = "Firstchoice<support@markupdesigns.info>";
            $headers = "From: $from\r\n";
            $headers .= "MIME-Version: 1.0\r\n"
            ."Content-Type: multipart/mixed; boundary=\"1a2a3a\""; 
            $msg .= "If you can see this MIME than your client doesn't accept MIME types!\r\n" ."--1a2a3a\r\n"; 
            $msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
              ."Content-Transfer-Encoding: 7bit\r\n\r\n";
            $msg .= $message."\r\n";
            $msg .= "--1a2a3a\r\n"; 
            $success = mail($email,$subject,$msg,$headers);
        }
    }

    public function send_otp($mobile="") { 
        $msg='';        
        $otp=rand(111111,999999);
        $updatedata['otp']=$otp;
        $id=getCustomerIdByMobile($mobile);
        if($id>0){
            $this->front_model->setUpdateData($this->users, $updatedata, $id);
        }
        $email='m.jabir@markupdesigns.com';      
        $smtp_host=$this->smtp_model->getSmtp('smtp_host');
        $smtp_port=$this->smtp_model->getSmtp('smtp_port');
        $smtp_user=$this->smtp_model->getSmtp('smtp_user');
        $smtp_pass=$this->smtp_model->getSmtp('smtp_pass');
        $config                 = array(
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'priority' => '1',
        'newline' => '\r\n'
        );
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $smtp_host;
        $config['smtp_port']    = $smtp_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $smtp_user;
        $config['smtp_pass']    = $smtp_pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $this->email->initialize($config);
        $this->email->from($smtp_user, 'Firstchoice');
        $this->email->to($email);
        $this->email->subject('One Time Password (otp)');             
        $email_template  = 'otp.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{otp}}'=>$otp,
        '{{mobile}}'=>$mobile,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = $this->email_compose($email_template,$templateTags);
        $this->email->message($message); 
        if(!$this->email->send()) {

            $subject = "One Time Password (otp)";
            $from = "Firstchoice<support@markupdesigns.info>";
            $headers = "From: $from\r\n";
            $headers .= "MIME-Version: 1.0\r\n"
            ."Content-Type: multipart/mixed; boundary=\"1a2a3a\""; 
            $msg .= "If you can see this MIME than your client doesn't accept MIME types!\r\n" ."--1a2a3a\r\n"; 
            $msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
              ."Content-Transfer-Encoding: 7bit\r\n\r\n";
            $msg .= $message."\r\n";
            $msg .= "--1a2a3a\r\n"; 
            $success = mail($email,$subject,$msg,$headers);
        }
    }
}

