<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

  public function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('front_model');    
        $this->load->model('payment_model');    
        $this->load->helper('front_helper');
        $this->load->model('smtp_model');
    }

    public function calculate_assurance(){
      $days=$this->input->post('days');
      $request_id=$this->input->post('request_id');
      $administrator_fee=getAdvanceOption('admin_charge');
      $vehicle_id=checkMaintinaceRequest($request_id);
      $tax_rate=getAdvanceOption('admin_tax');
      $html='<table class="table">';
     
      if($vehicle_id>0){
       $price=getAssurancebyDay($days);
       $maintenance_type=getVehicleDetailsByField('maintenance_cost',$vehicle_id);
       $registration_date=getVehicleDetailsByField('registration_date',$vehicle_id);
       $age=date('Y', strtotime($registration_date));
       $discount=getAssuranceDiscountbyDay($days);
       $maintenance_cost=getMaintenanceCharge($age,$maintenance_type);
       $kms=getMaintinaceKmRequestByUser($request_id);
       $discount_amount=getTotalAssurancePrice($maintenance_cost*$kms+$price, $discount);
       $total=($maintenance_cost*$kms+$price+$administrator_fee)-$discount_amount;
       $tax=calculateTax($total,$tax_rate);
       $html.='<tr><td>'.$this->lang->line('REQUEST_KMS').'</td><td align="right">'.$kms.'</td></tr>';
       $html.='<tr><td>'.$this->lang->line('MAINTENANCE_CHARGE').' </td><td align="right">'.getPriceFormate($maintenance_cost).' / kms</td></tr>';
       $html.='<tr><th>'.$this->lang->line('EMAIL_DESIGNATION').'</th><th style="text-align:right">'.$this->lang->line('PRICE').'</th></tr>';

       $html.='<tr style="border-top:1px solid #ccc"><td>'.$this->lang->line('TOTAL_MAINTENANCE_COST').'</td><td align="right">'.getPriceFormate($maintenance_cost*$kms).'</td></tr>';
       $html.='<tr style="border-top:1px solid #ccc"><td>'.$this->lang->line('ASSURANCE_COST').'</td><td align="right">'.getPriceFormate($price).'</td></tr>';
       $html.='<tr><td>'.$this->lang->line('ADMINISTRATOR_FEE').'</td><td align="right">'.getPriceFormate($administrator_fee).'</td></tr>';
       $html.='<tr><td>'.$this->lang->line('DISCOUNTS').' ('.$discount.'%)</td><td align="right">- '.getPriceFormate($discount_amount).'</td></tr>';
       $html.='<tr><td>TOTAL CHF TVA ( '.$tax_rate.'% )</td><td align="right">- '.getPriceFormate($tax).'</td></tr>';
       $html.='<tr style="border-top:1px solid #ccc"><td><strong>'.$this->lang->line('EMAIL_TOTALS').'</strong></td><td align="right"><strong>'.getPriceFormate($total+$tax).'</strong></td></tr>';

      } else {

       $price=getAssurancebyDay($days);
       $discount=getAssuranceDiscountbyDay($days);
       $discount_amount=getTotalAssurancePrice($price+$administrator_fee, $discount);
       $total=($price+$administrator_fee)-$discount_amount;
       $tax=calculateTax($total,$tax_rate);
       $html.='<tr><td>'.$this->lang->line('TOTALS_DAYS').'</td><td align="right">'.$days.'</td></tr>';
       $html.='<tr><th>'.$this->lang->line('EMAIL_DESIGNATION').'</th><th style="text-align:right">'.$this->lang->line('PRICE').'</th></tr>';      
       $html.='<tr style="border-top:1px solid #ccc"><td>'.$this->lang->line('ASSURANCE_COST').'</td><td align="right">'.getPriceFormate($price).'</td></tr>';
       $html.='<tr><td>'.$this->lang->line('ADMINISTRATOR_FEE').'</td><td align="right">'.getPriceFormate($administrator_fee).'</td></tr>';
       $html.='<tr><td>'.$this->lang->line('DISCOUNTS').' ('.$discount.'%)</td><td align="right">- '.getPriceFormate($discount_amount).'</td></tr>';
       $html.='<tr><td>TOTAL CHF TVA ( '.$tax_rate.'% )</td><td align="right">- '.getPriceFormate($tax).'</td></tr>';
       $html.='<tr style="border-top:1px solid #ccc"><td><strong>'.$this->lang->line('EMAIL_TOTALS').'</strong></td><td align="right"><strong>'.getPriceFormate($total+$tax).'</strong></td></tr>';

      }
      
      $html.='</table>';
      $html.='<div class="gap10"></div>';
      echo $html;
      
    }


    public function getInvoceFormate($days=1,$request_id=0) {
      
        $administrator_fee=getAdvanceOption('admin_charge');
        $tax_rate=getAdvanceOption('admin_tax');
        $vehicle_id=checkMaintinaceRequest($request_id);
        $kms=getMaintinaceKmRequestByUser($request_id);
        $currentdate=date('M d, Y');
        $newdate = strtotime ( '+'.$days.' day' , strtotime ( $currentdate ) ) ;
        $newdate = date ( 'M d, Y' , $newdate );
        $price=getAssurancebyDay($days);
        $perday=$price/$days;
        $html='';
        $html.='<tr>';
        $html.='<td valign="top" style="padding:30px 30px 0 30px;">';
        $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:14px; color:#333;" >';
        $html.='<tr>';

        $html.='<tr><td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('REQUEST_KMS').'</td>
           
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;"></td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$kms.'</td></tr>';
        $html.='<tr><td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('EMAIL_PERIOD').'</td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;"></td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$currentdate.' - '. $newdate.' ( '.$days.' Days )</td></tr>';

        $html.='<tr><th align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('EMAIL_DESIGNATION').'</th>
           
           <th align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('PRICE').'</th>
           <th align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('EMAIL_TOTALS').'</th></tr>';

        if($vehicle_id>0){
           
           $maintenance_type=getVehicleDetailsByField('maintenance_cost',$vehicle_id);
           $registration_date=getVehicleDetailsByField('registration_date',$vehicle_id);
           $age=date('Y', strtotime($registration_date));
           $discount=getAssuranceDiscountbyDay($days);
           $maintenance_cost=getMaintenanceCharge($age,$maintenance_type);           
           $discount_amount=getTotalAssurancePrice($maintenance_cost*$kms+$price, $discount);
           $subtotal=$maintenance_cost*$kms+$price+$administrator_fee;
           $total=($maintenance_cost*$kms+$price+$administrator_fee)-$discount_amount;
           $tax=calculateTax($total,$tax_rate);
           
           
           
           
           $html.='<tr><td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('MAINTENANCE_CHARGE').'</td>
           <td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($maintenance_cost).' / kms</td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($maintenance_cost*$kms).'</td></tr>';
            $html.='<tr><td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('ASSURANCE_COST').'</td>
        
           <td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($perday).' / Day</td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($price).'</td></tr>';
           
            
        } else {

           
           $discount=getAssuranceDiscountbyDay($days);
           $discount_amount=getTotalAssurancePrice($price+$administrator_fee, $discount);
           $subtotal=$price+$administrator_fee;
           $total=($price+$administrator_fee)-$discount_amount;
           $tax=calculateTax($total,$tax_rate);
           
          
            $html.='<tr><td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.$this->lang->line('ASSURANCE_COST').'</td>
          
           <td align="left" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($perday).' / Day</td>
           <td align="right" valign="top" style="border-bottom:1px solid #ccc; padding:15px;">'.getPriceFormate($price).'</td></tr>';     
           
        }

        
        $html.='<tr><td align="left" valign="top" style="padding:15px;">'.$this->lang->line('ADMINISTRATOR_FEE').'</td><td align="left" valign="top" style="padding:15px;">'.getPriceFormate($administrator_fee).'</td><td align="right" valign="top" style="padding:15px;">'.getPriceFormate($administrator_fee).'</td></tr>';

        $html.='</table>';
        $html.='</td>';
        $html.='</tr>';
        $html.='<tr>';
        $html.='<td align="left" valign="middle" style="background:#e8e8e8; border-bottom:1px solid #929292; border-top:1px solid #929292;">';
        $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        $html.='<tr>';
        $html.='<td align="right" colspan="2" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;">TOTAL CHF HT : </td>';
        $html.='<td align="right" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;">'.getPriceFormate($subtotal).'</td>
        <tr/>';
        $html.='<tr>';
        $html.='<td align="right" colspan="2" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;">'.$this->lang->line('DISCOUNTS').' ('.$discount.'%) : </td>';
        $html.='<td align="right" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;"> - '.getPriceFormate($discount_amount).'</td>';
        $html.='<tr/>';
        $html.='<tr>';
        $html.='<td align="right" colspan="2" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;">TOTAL CHF TVA ( '.$tax_rate.'% ) : </td>';
        $html.='<td align="right" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;"> + '.getPriceFormate($tax).'</td>';
        $html.='<tr/>';
        $html.='<tr>';
        $html.='<td align="right" colspan="2" valign="top" style="border-bottom:1px solid #c7c7c7;padding:15px; "><strong>TOTAL CHF TTC : </strong></td>';
        $html.='<td align="right" valign="top" style="border-bottom:1px solid #c7c7c7; padding:15px;"><strong style="color:#1fc32e; font-size:16px; font-weight:700;">'.getPriceFormate($total+$tax).'</strong></td>';
        $html.='</tr>';
        $html.='<tr>';
        $html.='</tr>';
        $html.='</table>';
        $html.='</td>';
        $html.='</tr>';
        $html.='</tr>';
        return $html;
    }

    public function process()  {
    $administrator_fee=getAdvanceOption('admin_charge');
    $tax_rate=getAdvanceOption('admin_tax');
    $postData=$this->input->post();
    $days=$this->input->post('days');
    $request_id=$this->input->post('request_id');
    $user_id=$this->session->userdata('userID');
    $requestData=getVehicleRequestDetailsByID($request_id);
    $vehicle_id=checkMaintinaceRequest($request_id);
    if($vehicle_id){

       $price=getAssurancebyDay($days);
       $maintenance_type=getVehicleDetailsByField('maintenance_cost',$vehicle_id);
       $registration_date=getVehicleDetailsByField('registration_date',$vehicle_id);
       $age=date('Y', strtotime($registration_date));
       $discount=getAssuranceDiscountbyDay($days);
       $maintenance_cost=getMaintenanceCharge($age,$maintenance_type);
       $kms=getMaintinaceKmRequestByUser($request_id);
       $discount_amount=getTotalAssurancePrice($maintenance_cost*$kms+$price, $discount);
       $total=($maintenance_cost*$kms+$price+$administrator_fee)-$discount_amount;
       $tax=calculateTax($total,$tax_rate);
    } else {
       $price=getAssurancebyDay($days);
       $discount=getAssuranceDiscountbyDay($days);
       $discount_amount=getTotalAssurancePrice($price+$administrator_fee, $discount);
       $total=($price+$administrator_fee)-$discount_amount;
       $tax=calculateTax($total,$tax_rate);
    }
    
    $data['days']=$days;
    $data['request_id']=$request_id;
    $data['user_id']=$user_id;
    $data['requestData']=$requestData;
    $data['userData']=getUserProfileDataByID($user_id);
    $data['amount']=$total+$tax;
    $data['code']=getCurrencySign();
    $this->load->view('front/payment/paypal', $data);
    }

    public function membership_payment_proccess()  {
        $user_id=$this->session->userdata('userID');
        $month=$this->input->post('subscribe_month');
        $data['user_id']=$user_id;
        $data['month']=$month;
        $data['userData']=getUserProfileDataByID($user_id);
        $fee=getUserMembershipFee();
        if($fee>0) {
          $data['amount']=$fee*$month;
        } else {
          $data['amount']=0;
        }
        $data['code']=getCurrencySign();
        $this->load->view('front/payment/membership', $data);
    }

    public function cancel()  {
    
    }

    public function thankyou()  {
       
    }
    public function notify()  {
    
        $request = "cmd=_notify-validate";
        foreach ($_POST as $varname => $varvalue){
        $email .= "$varname: $varvalue\n";  
        if(function_exists('get_magic_quotes_gpc') and get_magic_quotes_gpc()){  
            $varvalue = urlencode(stripslashes($varvalue)); 
        } else { 
            $value = urlencode($value); 
        } 
            $request .= "&$varname=$varvalue"; 
        } 

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,"https://www.sandbox.paypal.com/cgi-bin/webscr");
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$request);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,false);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $result = curl_exec($ch);
        curl_close($ch);
        $tokens = explode("\r\n\r\n", trim($result));
        $result = trim(end($tokens));
        switch($result) {
            
            case "VERIFIED":
            $data['item_name']        = $_POST['item_name'];
            $data['item_number']      = $_POST['item_number'];
            $data['payment_status']   = $_POST['payment_status'];
            $data['address_name']     = $_POST['address_name'];
            $data['custom']           = $_POST['custom'];
            $data['payment_amount']   = $_POST['mc_gross'];
            $data['payment_currency'] = $_POST['mc_currency'];
            $data['txn_id']           = $_POST['txn_id'];
            $data['receiver_email']   = $_POST['receiver_email'];
            $data['payer_email']      = $_POST['payer_email'];
            $data['transaction']      =$_POST['txn_type'];
            $transaction=$_POST['txn_type'];
            $subscriberid=$_POST['subscr_id'];
            $businessemail=$_POST['receiver_email'];
            $details=serialize($_POST);
            $txnid = $data['txn_id'];
            $item_name=$data['item_name']; 
            $invoiceid_new=$this->payment_model->InvoiceData($data);
            if($item_name=='membership'){
                $this->send_membership_notify($data);
            } else {
                $this->send_assurance_notify($data,$invoiceid_new);
            }
            break;
            case "INVALID":
            $data['item_name']        = $_POST['item_name'];
            $data['item_number']      = $_POST['item_number'];
            $data['payment_status']   = $_POST['payment_status'];
            $data['address_name']     = $_POST['address_name'];
            $data['custom']           = $_POST['custom'];
            $data['payment_amount']   = $_POST['mc_gross'];
            $data['payment_currency'] = $_POST['mc_currency'];
            $data['txn_id']           = $_POST['txn_id'];
            $data['receiver_email']   = $_POST['receiver_email'];
            $data['payer_email']      = $_POST['payer_email'];
            $data['transaction']      =$_POST['txn_type'];
            $transaction=$_POST['txn_type'];
            $subscriberid=$_POST['subscr_id'];
            $businessemail=$_POST['receiver_email'];
            $details=serialize($_POST);
            $txnid = $data['txn_id'];
            $item_name=$data['item_name']; 
            $invoiceid_new=$this->payment_model->InvoiceData($data);
            if($item_name=='membership'){
                $this->send_membership_notify($data);
            } else {
                $this->send_assurance_notify($data,$invoiceid_new);
            }
            break;
            default:
            $data['item_name']        = $_POST['item_name'];
            $data['item_number']      = $_POST['item_number'];
            $data['payment_status']   = $_POST['payment_status'];
            $data['address_name']     = $_POST['address_name'];
            $data['custom']           = $_POST['custom'];
            $data['payment_amount']   = $_POST['mc_gross'];
            $data['payment_currency'] = $_POST['mc_currency'];
            $data['txn_id']           = $_POST['txn_id'];
            $data['receiver_email']   = $_POST['receiver_email'];
            $data['payer_email']      = $_POST['payer_email'];
            $data['transaction']      =$_POST['txn_type'];
            $transaction=$_POST['txn_type'];
            $businessemail=$_POST['receiver_email'];
            $details=serialize($_POST);
            $txnid = $data['txn_id'];
            $item_name=$data['item_name']; 
            $invoiceid_new=$this->payment_model->InvoiceData($data);
            if($item_name=='membership'){
                $this->send_membership_notify($data);
            } else {
                $this->send_assurance_notify($data,$invoiceid_new);
            }
        }
    }

    public function send_membership_notify($invoiceData)
    {
        $exdata=$invoiceData['custom'];
        $expInvoce =explode(":",$exdata);
        $user_id   = $expInvoce[0];
        $offset    = $expInvoce[1];
        $userData=getUserProfileDataByID($user_id);
        $email=$userData['email'];
        $currentdate=date('M d, Y');
        $newdate = strtotime ( '+'.$offset.' month' , strtotime ( $currentdate ) ) ;
        $newdate = date ( 'M d, Y' , $newdate );            
        $email_template  = 'membership_invoice.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{item_name}}'=>$invoiceData['item_name'],
        '{{invoice_date}}'=> date('M d, Y'),
        '{{receive_date}}'=> date('M d, Y').'at'. date('H:i:s'),
        '{{exp_date}}'=> $newdate,
        '{{membership}}'=> 'Membership',
        '{{amount}}'=>getPriceFormate($invoiceData['payment_amount']),
        '{{client_number}}'=>100000+$user_id,
        '{{username}}'=>$userData['username'],
        '{{site_location}}'=>$userData['site_location'],
        '{{receiver_email}}'=>$invoiceData['receiver_email'],
        '{{payment_status}}'=>$invoiceData['payment_status'],
        '{{txn_id}}' => $invoiceData['txn_id'],
        '{{period}}' => $offset.' Month',
        '{{email_invoice}}' => $this->lang->line('EMAIL_INVOICE'),
        '{{invoice_bill_number}}' => $this->lang->line('EMAIL_BILL_NUMBER'),
        '{{invoice_client_number}}' => $this->lang->line('EMAIL_CLIENT_NUMBER'),
        '{{invoice_designation}}' => $this->lang->line('EMAIL_DESIGNATION'),
        '{{invoice_designation}}' => $this->lang->line('EMAIL_DESIGNATION'),
        '{{invoice_period}}' => $this->lang->line('EMAIL_PERIOD'),
        '{{invoice_price}}' => $this->lang->line('EMAIL_PRICE'),
        '{{invoice_total}}' => $this->lang->line('EMAIL_TOTALS'),
        '{{invoice_payment}}' => $this->lang->line('EMAIL_PAYMENT'),
        '{{invoice_by_paypal}}' => $this->lang->line('EMAIL_BY_PAYPAL'),
        '{{transaction}}' => $invoiceData['transaction'],
        '{{term_and_conditions}}' => $this->lang->line('TERMS_CONDITIONS'),
        '{{conditions}}' => getAdvanceOption('conditions')
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Membership Invoice',$message);
    }

    public function send_assurance_notify($invoiceData , $inviceid) {
        $new_id=100000000+$inviceid;
        $download_link=base_url('download-contract').'/'.base64_encode($new_id);
        $exdata=$invoiceData['custom'];
        $expInvoce =explode(":",$exdata);
        $user_id   = $expInvoce[0];
        $offset    = $expInvoce[1];
        $request_id= $expInvoce[3]; 
        $userData=getUserProfileDataByID($user_id);
        $email=$userData['email'];
        $currentdate=date('M d, Y');
        $newdate = strtotime ( '+'.$offset.' day' , strtotime ( $currentdate ) ) ;
        $newdate = date ( 'M d, Y' , $newdate );
        $invoiceFormate=$this->getInvoceFormate($offset,$request_id);            
        $email_template  = 'request_invoice.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{item_name}}'=>$invoiceData['item_name'],
        '{{invoice_date}}'=> date('M d, Y'),
        '{{receive_date}}'=> date('M d, Y').'at'. date('H:i:s'),
        '{{exp_date}}'=> $newdate,
        '{{membership}}'=> 'Assurance Cost',
        '{{amount}}'=>getPriceFormate($invoiceData['payment_amount']),
        '{{client_number}}'=>100000+$user_id,
        '{{receiver_email}}'=>$invoiceData['receiver_email'],
        '{{username}}'=>$userData['username'],
        '{{site_location}}'=>$userData['site_location'],
        '{{payment_status}}'=>$invoiceData['payment_status'],
        '{{txn_id}}' => $invoiceData['txn_id'],
        '{{period}}' => $offset.' day',
        '{{invoice_content}}' => $invoiceFormate,
        '{{email_invoice}}' => $this->lang->line('EMAIL_INVOICE'),
        '{{invoice_bill_number}}' => $this->lang->line('EMAIL_BILL_NUMBER'),
        '{{invoice_client_number}}' => $this->lang->line('EMAIL_CLIENT_NUMBER'),
        '{{invoice_designation}}' => $this->lang->line('EMAIL_DESIGNATION'),
        '{{invoice_designation}}' => $this->lang->line('EMAIL_DESIGNATION'),
        '{{invoice_period}}' => $this->lang->line('EMAIL_PERIOD'),
        '{{invoice_price}}' => $this->lang->line('EMAIL_PRICE'),
        '{{invoice_total}}' => $this->lang->line('EMAIL_TOTALS'),
        '{{invoice_payment}}' => $this->lang->line('EMAIL_PAYMENT'),
        '{{invoice_by_paypal}}' => $this->lang->line('EMAIL_BY_PAYPAL'),
        '{{term_and_conditions}}' => $this->lang->line('TERMS_CONDITIONS'),
        '{{transaction}}' => $invoiceData['transaction'],
        '{{download_link}}' => $download_link,
        '{{conditions}}' => getAdvanceOption('conditions')
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Assurance Invoice',$message);
    }
}