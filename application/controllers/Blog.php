<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	
	function __construct()
	{
    	parent::__construct();
    	$this->lang->load('front','english');
    	$this->load->model('front_model');
    	$this->load->helper('front_helper');
    	$this->load->model('blog_model');
	}
	
	public $tbl_name = 'blog';

	public function index()
	{
        $data['page_title'] = $this->lang->line('BLOG');

       	$limit_per_page = 5;//$this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;

        $total_records = $this->master_model->get_total_records($this->tbl_name);

        if ($total_records > 0) 
        {
        	$data["records"] = $this->master_model->get_current_page_records($this->tbl_name,$limit_per_page,$start,array('status'=>1));
        	$data["pagination"] = Jpagination($total_records,$limit_per_page,$start); 
        }

        $this->load->view('front/include/header', $data);
        $this->load->view('front/blog/index', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data); 
	}

	public function details()
    {
        $slug = $this->uri->segment(2);
        $data['record'] = $this->master_model->get_record($this->tbl_name,array('slug'=>$slug));
        $data['page_title'] = $this->lang->line('BLOG');

        $this->load->view('front/include/header', $data);
        $this->load->view('front/blog/details', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data); 
    }
}

  
