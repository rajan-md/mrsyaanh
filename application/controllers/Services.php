<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Services extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('services_model');
        $this->load->model('front_model');
        $this->load->model('smtp_model');
        $this->load->helper('front_helper');
    }
    
    public $users='tbl_users';
    public $service_leads = 'tbl_service_leads';
    public $users_address='tbl_users_address';
    public $services='tbl_services';
    public $provider_request='tbl_service_provider_request';
    public $provider_request_setting='tbl_service_provider_settings';
    public $service_orders='tbl_service_orders';
    public $service_operations='tbl_service_operations';
    public $services_area='tbl_services_area';
    public $subscribers='tbl_subscribers';

    public function findSearchResult()
    {
        $servicesarea=$this->input->post('servicesarea');
        $this->session->set_userdata('searched_zipcode',$servicesarea);
        $this->session->set_userdata('foo','');
        $serviceList=$this->services_model->findServicesByZipCode($servicesarea);
        $html='';
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a data-dismiss="modal" href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></button>'; 
        if(!empty($serviceList)){        
            $html.='<h4 class="modal-title">'.$this->lang->line('SERVICE_OFFERED_IN_YOUR_AREA').'</h4>';
        } else {
            $html.='<h4 class="modal-title">'.$this->lang->line('SERVCE_UNAVAILABLE').'</h4>';
        }
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';       
        $html.='<div class="service-popup-links-block">';  
        $html.='<div id="chooseserviceMsg">';
        $html.='</div>';     
        $html.='<ul class="service-popup-links">';
        if(!empty($servicesarea)){
            $this->session->set_userdata('zipcode', $servicesarea);
        }
        if(!empty($serviceList)){
            foreach ($serviceList as $data) {
                $html.='<li><a class="services_data" href="'.base_url($data->slug).'" service-data="'.$data->id.'">'.$data->name.'</a></li>';
            }
        } else {
            $html.='<li class="srvice_not_available"><strong><i class="fa fa-exclamation-triangle" style="color:red"></i> </strong> <br/>'.$this->lang->line('SERVICE_NOT_AVAILABLE').' <br/> '.$this->lang->line('ARE_YOU_SERVICE_PROVIDERS').'<a href="'.base_url('become-a-service-provider').'">'.$this->lang->line('CLICK_HERE').'</a> </li>';
            $html.='<li class="sendQuery">';
            $html.='<div id="errorQueryMsg"></div>';
            $html.='<form method="post" id="sendMyQuery">';
            $html.='<input type="hidden" name="custome_area" value="'.$servicesarea.'">';
            $html.='<input type="text" class="inputQuery" id="inputNameQuery" name="custome_name" placeholder="'.$this->lang->line('NAME').'*" required="">';
            $html.='<input type="email" class="inputQuery" id="inputEmailQuery" name="custome_email" placeholder="'.$this->lang->line('EMAIL').'*" required="">';
            //$html.='<input type="submit" class="submitQuery" name="custome_email" placeholder="Your Email">';
            $html.='</form>';
            $html.='</li>';

        }
        $html.='<ul>';
        
        $html.='</div></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a href="javascript:void(0)" id="chooseAnyServices" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;
    }

    public function ajaxSubscribeService()
    {
        $postData = $this->input->post();
        if(!empty($postData))
        {
            $group = !empty($postData['group'])?$postData['group']:'';
            $name = !empty($postData['name'])?$postData['name']:'';
            $email = !empty($postData['email'])?$postData['email']:'';
            $zipcode = !empty($postData['zipcode'])?$postData['zipcode']:'';

            $whereArr = array('subscription_group'=>$group,'subscriber_email'=>$email);
            $rec = get_record($this->subscribers,$whereArr);

            /*-----------data----------*/
            $data['subscriber_name'] = $name;
            $data['subscriber_email'] = $email;
            $data['subscription_group'] = $group;
            $data['status'] = 1;
            /*-----------data-----------*/             
            if(!empty($rec) && $group==1)
            {
                $zipcodesArr = !empty($rec->zipcode)?explode(",", $rec->zipcode):array();
                if(!empty($zipcodesArr))
                {
                    if(in_array($zipcode, $zipcodesArr))
                    {
                        echo 'exist';
                    }
                    else
                    {
                        array_push($zipcodesArr, $zipcode);
                        $zipcode_str = implode(",", $zipcodesArr);
                        $data['zipcode'] = $zipcode_str;
                        $this->services_model->setUpdateData($this->subscribers,$data,$rec->id);
                        $this->send_service_subscription_notification($name,$email,$zipcode);
                        echo 'success';
                    }
                }
                
            }
            elseif(!empty($rec) && $group>1)
            {
                echo 'exist';                
            }
            else
            {
                $data['zipcode'] = $zipcode;
                $this->services_model->setInsertData($this->subscribers,$data);
                if(!empty($name) && !empty($zipcode))
                {
                    $this->send_service_subscription_notification($name,$email,$zipcode);
                }
                else
                {
                    $this->send_blog_subscription_notification($email);
                }
                echo 'success';
            }
        }
    }

    public function send_service_subscription_notification($name,$email,$zipcode)
    {
        $email_template  = 'service-subscriptions.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'assets/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$name,
        '{{user_email}}'=>$email,
        '{{zipcode}}'=>$zipcode,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com'
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Service subscription',$message);
    }

    public function send_blog_subscription_notification($email)
    {
        $email_template  = 'blog-subscriptions.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'assets/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_email}}'=>$email,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com'
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Newsletter subscription',$message);
    }

    public function send_services_not_found_message()
    {
        $html='';
        $this->send_services_not_found_email();
        //$html.='<div class="modal-header text-center">';      
        //$html.='<button class="back"><a href="#"><i class="fa fa-arrow-left"></i></a></button>';         
        //$html.='<h4 class="modal-title">Thank You  </h4>';
        //$html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        //$html.='</div>';
        $html.='<div class="modal-body">';
        $html.='<div class="gap20"></div>';
        $html.='<div class="thank_request">';                 
        $html.='<div class="thanks_tick"><i class="fa fa-check-circle" aria-hidden="true"></i></div>';
        $html.='<div class="thanks_heading">'.$this->lang->line('THANK_YOU_FOR_FEEDBACK').'</div>';
        $html.='<div class="thanks_msg">'.$this->lang->line('THANK_YOU_FOR_FEEDBACK_MESSAGE').'</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a href="javascript:void(0)"  data-dismiss="modal" class="next-full-btn">Close '; 
        $html.='</a></div>';        
        echo $html;
    }

    public function findLoginDetail()
    {
        $html='';
        $service=$this->input->post('service');
        $newinstalation=$this->input->post('newinstalation');
        if(!empty($newinstalation)){
        	$impload=implode(',',$newinstalation);
            $this->session->set_userdata('newinstalation', $impload);
        }
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMERS_INFORMATION').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';       
        $html.='<div class="service-popup-links-block">';
        $html.='<div id="chooseserviceMsg">';
        $html.='</div>'; 
        $html.='<ul class="service-popup-links">';       
        $html.='<li><a id="newCustomer" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-user"></i> '.$this->lang->line('NEW_CUSTOMERS').'</a></li>';
        $html.='<li><a id="repeatCustomer" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-user"></i> '.$this->lang->line('REPEAT_CUSTOMER').'</a></li>';
        $html.='</ul>';
        $html.='</div></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a id="isTypeOfUser" href="javascript:void(0)" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;
    }
    
    public function newCustomerDetail(){
        $html='';
        $service=$this->input->post('service');
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoLogin" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMER_REGISTRATION').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body" >';       
        $html.='<div class="otp-modal">';        
        $html.='<div class="row justify-content-center">';
        $html.='<div class="col-md-8" id="rbxc">';
        $html.='<div class="inner-login-form">';
        $html.='<div id="infoExist"></div>';
        $html.='<form method="post" id="jserilize"  class="repeat_loginbox">';
        $html.='<input type="hidden" name="userphone" id="userphone" value="">';
        $html.='<input name="repeat_phonecode" id="repeat_phonecode" placeholder="'.$this->lang->line('ENTER_PHONE_NO').'" type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" required="" class="form-control">';
        $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
        $html.='</form>';                     
        $html.='</div>';
        $html.='</div>'; 
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap20"></div>';
        $html.='<script type="text/javascript">
        var repeat_phonecode = $("#repeat_phonecode");
        var rep_phone_input = document.querySelector("#repeat_phonecode");
        var rep_phone_iti = window.intlTelInput(rep_phone_input, {
          formatOnDisplay: false,
          nationalMode: true,
          onlyCountries: ["us","in","ca"],
          separateDialCode: true,
          utilsScript: "js/build/js/utils.js",
        });

        $(function() {
            var repeat_phonecode_txt = $(".repeat_loginbox .selected-dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt + repeat_phonecode_number);
        });

        $(".repeat_loginbox #country-listbox li").on("click", function () {
            var repeat_phonecode_txt = $(this).find(".dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt + repeat_phonecode_number);
        });

        repeat_phonecode.on("keyup", function () {
            var repeat_phonecode_txt=$(".repeat_loginbox .selected-dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt+repeat_phonecode_number);
        });

        </script>';
        echo $html;
    }

    public function repeatCustomerDetail(){
		$html='';
        $service=$this->input->post('service');
        $html.='<div class="modal-header text-center">';      
        $html.='<button class="back"><a id="backtoLogin" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
        $html.='<h4 class="modal-title">'.$this->lang->line('REPEAT_CUSTOMER').'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body" >';       
        $html.='<div class="otp-modal">';        
        $html.='<div class="row justify-content-center">';
        $html.='<div class="col-md-8" id="rbxc">';
        $html.='<div class="inner-login-form">';
        $html.='<div id="infoExist"></div>';
        $html.='<form method="post" id="repeatLogin" class="repeat_loginbox">';
        $html.='<input type="hidden" name="userphone" id="userphone" value="">';
        $html.='<input name="repeat_phonecode" id="repeat_phonecode" placeholder="'.$this->lang->line('ENTER_PHONE_NO').'" type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" required="" class="form-control">';
        $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
        $html.='</form>';                     
        $html.='</div>';
        $html.='</div>'; 
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap20"></div>';
        $html.='<script type="text/javascript">
        var repeat_phonecode = $("#repeat_phonecode");
        var rep_phone_input = document.querySelector("#repeat_phonecode");
        var rep_phone_iti = window.intlTelInput(rep_phone_input, {
          formatOnDisplay: false,
          nationalMode: true,
          onlyCountries: ["us","in","ca"],
          separateDialCode: true,
          utilsScript: "js/build/js/utils.js",
        });

        $(function() {
            var repeat_phonecode_txt = $(".repeat_loginbox .selected-dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt + repeat_phonecode_number);
        });

        $(".repeat_loginbox #country-listbox li").on("click", function () {
            var repeat_phonecode_txt = $(this).find(".dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt + repeat_phonecode_number);
        });

        repeat_phonecode.on("keyup", function () {
            var repeat_phonecode_txt=$(".repeat_loginbox .selected-dial-code").text();
            var repeat_phonecode_number = repeat_phonecode.val();
            $("input[name=userphone]").val(repeat_phonecode_txt+repeat_phonecode_number);
        });

        </script>';
        echo $html;
    }

    public function getCurrentLocation(){

        $ip_address=$_SERVER['REMOTE_ADDR'];
        $geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
        $geo = curl_init();
        curl_setopt($geo, CURLOPT_URL, $geopluginURL);
        curl_setopt($geo, CURLOPT_RETURNTRANSFER, true);
        $geooutput = curl_exec($geo);
        curl_close($geo);
        $addrDetailsArr=unserialize($geooutput);
        echo '<pre>';
        print_r($addrDetailsArr);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        print_r($location);
    }

    public function ajaxValidateZipcode()
    {
        $zipcode=$this->input->post('zipcode');
        if(!empty($zipcode))
        {
            $checkZipCode=$this->services_model->verifyServiceZipcode($zipcode);
            if(!empty($checkZipCode))
            {
                echo 'valid';
            }
            else
            {
                echo 'invalid';
            }
        }
    }

    public function phonelogin(){
        $service=$this->input->post('service');
        $userphone=$this->input->post('userphone');
        $post_data=$this->input->post();
        if(!empty($post_data))
        { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('userphone', 'mobile', 'required|is_unique[tbl_users.mobile]'); 
            if($this->form_validation->run() == FALSE)
            { 
               echo '2'; 
            }
            else
            {
                $html='';
                $html.='<div class="modal-header text-center">';            
                $html.='<button class="back"><a id="backtophone" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
                $html.='<h4 class="modal-title">'.$this->lang->line('CUSTOMER_REGISTRATION').'</h4>';
                $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
                $html.='</div>';
                $html.='<div class="modal-body">';            
                $html.='<div class="otp-modal">';            
                $html.='<div class="row justify-content-center">';
                $html.='<div class="col-md-8" >';
                $html.='<div class="inner-login-form">';            
                $html.='<div class="form-social-ico">';
                $html.='<div id="emailExist"></div>';
                $html.='<form method="post" id="ajxRegistration">';               
                $html.='<input type="hidden" name="service" id="service" value="'.$service.'">';
                $html.='<input type="hidden" name="userphone" id="userphone" value="'.$userphone.'">';
                $html.='<div class="row">';
                $html.='<div class="col-md-6"><input type="text" placeholder="'.$this->lang->line('FIRST_NAME').'" name="fname" id="fname" required="" class="form-control"></div>';
                $html.='<div class="col-md-6"><input type="text" placeholder="'.$this->lang->line('LAST_NAME').'" name="lname" id="lname" required="" class="form-control"></div>';
                $html.='</div>';
                $html.='<div class="row">';
                $html.='<div class="col-md-12"><input type="text" placeholder="'.$this->lang->line('EMAIL').'" name="useremail" id="useremail" required="" class="form-control"></div>';
                $html.='</div>';
                $html.='<div class="row">';
                $html.='<div class="col-md-12"><button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('REGISTRATION').'</button></div>';
                $html.='</div>';
                $html.='</form>'; 
                $html.='</div>';      
                $html.='</div>';
                $html.='</div>'; 
                $html.='</div>';  
                $html.='</div>';
                $html.='</div>';
                $html.='<div class="gap20"></div>';
                echo $html;
            }
        }
        else
        { 
             echo '2';
        }        
    }

    public function repeatLogin(){
        $service=$this->session->userdata('services');
        $userphone=$this->input->post('userphone');        
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules('userphone', 'mobile', 'required|is_unique[tbl_users.mobile]'); 
        if($this->form_validation->run() == FALSE)
        { 
            $this->send_otp($userphone);
            $this->session->set_userdata('userphone', $userphone);
            $html='';
            $html.='<div class="modal-header text-center">';      
            $html.='<button class="back"><a id="backtorepeatphone" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';         
            $html.='<h4 class="modal-title">'.$this->lang->line('REPEAT_CUSTOMER').'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body" >';       
            $html.='<div class="otp-modal">';        
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-8" id="rbxc">';
            $html.='<div class="inner-login-form">';
            $html.='<div id="infoExist"></div>';
            $html.='<form method="post" id="validateotp">';
            $html.='<input name="userotp" id="userotp" placeholder="'.$this->lang->line('PLZ_ENTER_OTP').'" type="tel" required="" class="form-control">';
            $html.='<span class="lost-password d-inline-block"><a id="forgotpass" data-dismiss="model" href="javascript:void(0)" data-toggle="modal" data-target="#forgotpass">Lost your password?</a></span>';
            $html.='<button type="submit" id="pLogincheck" class="shop-btn" service-data="'.$service.'">'.$this->lang->line('NEXT').'</button>';
            $html.='</form>';                     
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap20"></div>';
            echo $html;

        }
        else
        {        
           echo '3'; 
        }        
    }

    public function validateotp() {
         
        $service=$this->session->userdata('service');
        $userphone=$this->session->userdata('userphone');
        $userotp=$this->input->post('userotp');
        $ok=chckValidOtp($userotp);
        if($ok) {        
        $this->session->set_userdata('userID',$ok);
        $user_id = $this->session->userdata('userID');
        if(!empty($user_id)) {                  
            $html='';
            $html.='<div class="modal-header text-center">';            
            $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-arrow-left"></i></a></button>';
            $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';            
            $html.='<div class="otp-modal">';            
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-12" >';
            $html.='<div class="inner-login-form">';            
            $html.='<div class="form-social-ico">';             
            $html.='<div class="service-popup-links">';
            $html.='<div class="text-center">';
            $html.='<h4 class="chooseAddress">'.$this->lang->line('WHERE_SERVICE_REQUIRED').'</h4>';
            $html.='</div>';
            $html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
            $html.='<div class="newu-loc-box">';
            $html.='<input type="hidden" name="street_number" id="street_number">';          
            $html.='<input type="hidden" name="route" id="route">';          
            $html.='<input type="hidden" id="latitude" name="site_latitude">';          
            $html.='<input type="hidden" id="longitude" name="site_longitude">';            
            $html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="'.$this->lang->line('ENTER_LOCATION').'" class="form-control" required>';
            $html.='<input type="submit" value="'.$this->lang->line('ADD').'" name="add-user-loc" class="btn btn-gray" >';
            $html.='</div>';
            $html.='<input type="hidden" id="country" name="country" value="">';
            $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
            $html.='<input type="hidden" id="locality" name="city" value="">';
            $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
            $html.='</form>';            
            $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
            $html.='<ul class="custom-r-btn" id="addresListMsg">';
            $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
            if(!empty($addresList)){
                foreach ($addresList as $addressData)
                {    
                    $id=isset($addressData->id)?$addressData->id:"";
                    $html.='<li class="custom-r-btn-col w-100">';
                    $html.='<div class="row align-items-center">';              
                    $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                    $html.='<label class="radio-btnn">';
                    $html.=isset($addressData->site_location)?$addressData->site_location:"";
                    $html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
                    $html.='<span class="checkmark"></span>';
                    $html.='</label>';       
                    $html.='</div>';
                    $html.='<div class="edit-del-btn col-3">';
                    //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                    $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                    $html.='</div>';
                    $html.='</div>';
                    $html.='</li>';     
                }
            }   
            $html.='</ul>';
            $html.='</form>';
            $html.='</div>';      
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap10"></div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i></a>'; 
            $html.='</div>';            
            echo $html;

        } else {        
           echo '3'; 
        }    
        } else {        
           echo '3'; 
        }  
    }

    public function ajaxUserRegistration(){
        $service=$this->session->userdata('services');
        $userphone=$this->input->post('userphone');
        $useremail=$this->input->post('useremail');
        $fname=$this->input->post('fname');
        $lname=$this->input->post('lname');         
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('useremail', 'email ID', 'required|valid_email|is_unique[tbl_users.email]',array('is_unique' => 'Applicant email ID already exist.')); 
            if($this->form_validation->run() == FALSE)
            { 
               echo '2'; 
            }
            else
            {
                $newinstalation=$this->session->userdata('newinstalation');
                $userdata   = array(
                'instalation'=>$newinstalation,
                'services'  =>$service
                );          
                $this->session->set_userdata($userdata);
                $user_id = $this->session->userdata('userID');
                if(empty($user_id))
                {                    
                    $username = $fname.' '.$lname;            
                    $insertData['username'] = $username;
                    $insertData['fname']    = $fname;
                    $insertData['lname']    = $lname;
                    $insertData['role']     = 2;
                    $insertData['email']    = $useremail;
                    $insertData['mobile']   = $userphone;
                    $user_id = $this->front_model->setInsertData($this->users, $insertData);

                    $this->set_password_email($useremail);
                    $this->send_verification_email($user_id);

                    $this->session->set_userdata('userID',$user_id);
                    $this->session->set_userdata('user_email',$useremail);
                    $this->session->set_userdata('user_name',$username);
                }

                $html='';
                $html.='<div class="modal-header text-center">';            
                $html.='<button class="back"><a id="backtoregistration" href="javascript:void(0)" service-data="'.$service.'" phone-data="'.$userphone.'" ><i class="fa fa-arrow-left"></i></a></button>';
                $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
                $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
                $html.='</div>';
                $html.='<div class="modal-body">';            
                $html.='<div class="otp-modal">';            
                $html.='<div class="row justify-content-center">';
                $html.='<div class="col-md-12" >';
                $html.='<div class="inner-login-form">';            
                $html.='<div class="form-social-ico">';             
                $html.='<div class="service-popup-links">';
                $html.='<div class="text-center">';
                $html.='<h4 class="chooseAddress">'.$this->lang->line('WHERE_DO_YOU_REQUIRED_SERVICES').'</h4>';
                $html.='</div>';                
                $html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
                $html.='<div class="newu-loc-box">';
                $html.='<input type="hidden" name="street_number" id="street_number">';          
                $html.='<input type="hidden" name="route" id="route">';          
                $html.='<input type="hidden" id="latitude" name="site_latitude">';          
                $html.='<input type="hidden" id="longitude" name="site_longitude">';            
                $html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="'.$this->lang->line('ENTER_LOCATION').'" class="form-control" required>';
                $html.='<input type="submit" value="Add" name="add-user-loc" class="btn btn-gray" >';
                $html.='</div>';
                $html.='<input type="hidden" id="country" name="country" value="">';
                $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
                $html.='<input type="hidden" id="locality" name="city" value="">';
                $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
                $html.='</form>';
                $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
                $html.='<ul class="custom-r-btn" id="addresListMsg">';
                $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
                if(!empty($addresList)){
                    foreach ($addresList as $addressData) {    

                        $id=isset($addressData->id)?$addressData->id:"";
                        $html.='<li class="custom-r-btn-col w-100">';
                        $html.='<div class="row align-items-center">';              
                        $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                        $html.='<label class="radio-btnn">';
                        $html.=isset($addressData->site_location)?$addressData->site_location:"";
                        $html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
                        $html.='<span class="checkmark"></span>';
                        $html.='</label>';       
                        $html.='</div>';
                        $html.='<div class="edit-del-btn col-3">';
                        //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                        $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                        $html.='</div>';
                        $html.='</div>';
                        $html.='</li>';     
                    }
                }   
                $html.='</ul>';
                //addhomeaddress
                $html.='</form>';
                
                $html.='</div>'; 
                $html.='</div>';      
                $html.='</div>';
                $html.='</div>'; 
                $html.='</div>';  
                $html.='</div>';
                $html.='</div>';
                $html.='<div class="gap10"></div>';
                $html.='<div class="modal-footer text-center">';
                $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i></a>';  
                 $html.='</div>';
                //$html.=print_r($this->session->userdata());
                echo $html;
            }
        } else { 
             echo '2';
        }        
    }

    public function set_password_email($useremail)
    {
        if(!empty($useremail))
        { 
            $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$useremail);
            if(!empty($userdata))
            {
                //print_r($userdata);
                $token = md5(rand(11111,99999));                    
                if($this->front_model->setUpdateData($this->users,array('token'=>$token),$userdata['id']))
                {
                    $resetUrl = base_url('reset-password')."/?email=".$userdata['email']."&token=".$token;
                    $email_template  = 'setting-password.html';
                    $templateTags =  array(
                    '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                    '{{site_name}}'=>'firstchoice.com',
                    '{{site_url}}'=> base_url(),
                    '{{team_name}}'=>'firstchoice',
                    '{{name}}'=>$userdata['username'],
                    '{{email}}'=>$userdata['email'],
                    '{{resetUrl}}'=>$resetUrl,
                    '{{year}}'=>date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com',
                    );
                    $message = email_compose($email_template,$templateTags);
                    send_email($useremail,'Set password link',$message);
                }
            }
        }      
    }

    public function send_verification_email($userID)
    { 
        $userdata = $this->front_model->getDataCollectionByID($this->users,$userID);
        $verification_link=base_url("login/verify_email/".rtrim(base64_encode($userID),"="));
        $email=$userdata['email'];
        $email_template  = 'verify.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'assets/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$userdata['username'],
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        '{{verification_link}}'=>$verification_link,
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Verification Email',$message);
    }

    public function ajaxForgotPassword()
    {
        $useremail=$this->input->post('useremail');
        if(!empty($useremail))
        { 
            $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$useremail);
            if(!empty($userdata))
            {
                //print_r($userdata);
                $token = md5(rand(11111,99999));                    
                if($this->front_model->setUpdateData($this->users,array('token'=>$token),$userdata['id']))
                {
                    $resetUrl = base_url('reset-password')."/?email=".$userdata['email']."&token=".$token;
                    $email_template  = 'forgot-pass.html';
                    $templateTags =  array(
                    '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                    '{{site_name}}'=>'firstchoice.com',
                    '{{site_url}}'=> base_url(),
                    '{{team_name}}'=>'firstchoice',
                    '{{name}}'=>$userdata['username'],
                    '{{email}}'=>$userdata['email'],
                    '{{resetUrl}}'=>$resetUrl,
                    '{{year}}'=>date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com',
                    );
                    $message = email_compose($email_template,$templateTags);
                    send_email($userdata['email'],'Forgot Password',$message);
                    echo '<div class="success">Email with reset url has sent to your inbox.</div>';
                }
                else
                {
                    echo '<div class="error">Oops: Something going wrong, please try again.</div>';
                }
            }
            else
            {
                echo '<div class="error">Email address does not exist.</div>';
            }
        }      
    }

    public function resetPassword()
    {
        $get_data=$this->input->get();
        $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$get_data['email']);
        if(!empty($userdata))
        {
            if($userdata['token'] == $get_data['token'])
            {
                $post_data=$this->input->post();
                if(!empty($post_data)) {
            
                    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
                    $this->form_validation->set_rules('password', 'Password', 'trim|required');
                    $this->form_validation->set_rules('repassword', 'Confirm Password ', 'trim|required|matches[password]');        
                    if($this->form_validation->run() != FALSE)
                    {
                        $newPassword = md5($post_data['password']);
                        if($this->front_model->setUpdateData($this->users,array('password'=>$newPassword),$userdata['id']))
                        {
                            $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your password has Successfully changed.'));
                        }
                        else
                        {
                            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Invalid email or password.'));
                        }
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Token has been expired, Please try again.'));
            }
        }
        else
        {
            $this->session->set_flashdata('notification',array('error'=>1,'message'=>'User with this email are not exist.'));
        }

        $this->load->view('front/include/header');
        $this->load->view('front/services/reset_password');
        $this->load->view('front/include/modals');
        $this->load->view('front/include/footer');        
    }

    public function ajaxChooseAddress(){
        $service=$this->session->userdata('services');        
        $user_id = $this->session->userdata('userID');

        $newinstalation=$this->input->post('newinstalation');
        $additionalOperator=$this->input->post('additional_operator');
        if(!empty($newinstalation)){
            $impload=implode(',',$newinstalation);
            $this->session->set_userdata('newinstalation', $impload);
        }
        if(!empty($additionalOperator))
        {
            $this->session->set_userdata('additional_operator', $additionalOperator);
        }
        else
        {
            $this->session->set_userdata('additional_operator', 0);
        }

        if(!empty($user_id)){                  
    		$html='';
            $html.='<div class="modal-header text-center">';            
            $html.='<button class="back"><a id="backtoprices" href="javascript:void(0)" service-data="'.$service.'"><i class="fa fa-arrow-left"></i></a></button>';
            $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';            
            $html.='<div class="otp-modal">';            
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-12" >';
            $html.='<div class="inner-login-form">';            
            $html.='<div class="form-social-ico">';	            
            $html.='<div class="service-popup-links">';
    		$html.='<div class="text-center">';
    		$html.='<h4 class="chooseAddress">'.$this->lang->line('WHERE_DO_YOU_REQUIRED_SERVICES').'</h4>';
    		$html.='</div>';
    		$html.='<form method="post" id="getAddress" style="padding-bottom:0px;">';
    		$html.='<div class="newu-loc-box">';
            $html.='<input type="hidden" name="street_number" id="street_number">';          
    		$html.='<input type="hidden" name="route" id="route">';          
    		$html.='<input type="hidden" id="latitude" name="site_latitude">';          
            $html.='<input type="hidden" id="longitude" name="site_longitude">';            
        	$html.='<input type="text" name="site_location" id="site_location" onclick="initialize()" placeholder="'.$this->lang->line('ENTER_LOCATION').'" class="form-control" required>';
        	$html.='<input type="submit" value="Add" name="add-user-loc" class="btn btn-gray" >';
        	$html.='</div>';
            $html.='<input type="hidden" id="country" name="country" value="">';
            $html.='<input type="hidden" id="administrative_area_level_1" name="state" value="">';
            $html.='<input type="hidden" id="locality" name="city" value="">';
            $html.='<input type="hidden" id="postal_code" name="postal_code" value="">';
            $html.='</form>';
            
            
            $html.='<form method="post" id="addDatatime" style="margin:0;padding-top: 0;">';
        	$html.='<ul class="custom-r-btn" id="addresListMsg">';
            $addresList=$this->front_model->getUserAddressData($this->users_address, $user_id);
            if(!empty($addresList)){
                foreach ($addresList as $addressData) {    

                    $id=isset($addressData->id)?$addressData->id:"";
                    $html.='<li class="custom-r-btn-col w-100">';
                    $html.='<div class="row align-items-center">';              
                    $html.='<div class="user-addres-pop col-sm-8 cus-rb"> ';     
                    $html.='<label class="radio-btnn">';
                    $html.=isset($addressData->site_location)?$addressData->site_location:"";
            		$html.='<input type="radio" name="radio" checked="checked" value="'.$id.'" required>';
            		$html.='<span class="checkmark"></span>';
            		$html.='</label>';		 
            		$html.='</div>';
            		$html.='<div class="edit-del-btn col-3">';
                    //$html.='<a href="javascript:void(0)" editId="'.$id.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
                    $html.='<a href="javascript:void(0)" delId="'.$id.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
                    $html.='</div>';
            		$html.='</div>';
            		$html.='</li>';		
    			}
            }	
    		$html.='</ul>';
    		$html.='</form>';
            $html.='</div>';      
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap10"></div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="gotoaddrespic" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i></a>'; 
            $html.='</div>';
            

            echo $html;	    
    	}        
    }

    public function chooseDatetime()
    {
        $address_id=$this->session->userdata('address_id');
        $radio=$this->input->post('radio');  
        if(!empty($radio))
        {
            $address_id = $radio;
            $this->session->set_userdata('address_id',$address_id);
        }

        $service=$this->session->userdata('services');
        $searched_zipcode=$this->session->userdata('searched_zipcode');
        $zipcode = getUserAddressZipCode($address_id);

        $is_service_available = validateZipcodeForService($zipcode,$service);

        $html='';
        if(!empty($searched_zipcode) && !empty($zipcode) && !empty($is_service_available))
        {
            $html.='<div class="modal-header text-center">';            
            $html.='<button class="back"><a id="backtoaddress" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
            $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';            
            $html.='<div class="otp-modal">';            
            $html.='<div class="row justify-content-center">';
            $html.='<div class="col-md-8" >';
            $html.='<div class="inner-login-form">';            
            $html.='<div class="form-social-ico">';             
            $html.='<div class="service-popup-links">';
            $html.='<div class="text-center">';
            $html.='<h4>Select date of service</h4>';
            $html.='</div>'; 
            $html.='<form method="post" id="setDatetime">';       
            $html.='<div style="width: 250px; margin: 30px auto;">';
            $html.='<div id="datpicker" class="dtp_main"><span>'.date("Y-m-d").'</span><i class="fa fa-calendar ico-size"></i><span>'.date("H:i").'</span><i class="fa fa-clock-o ico-size"></i></div>';
            $html.='<div id="picker"></div>';
            $html.='<input type="hidden" id="result" value="'.date("Y-m-d H:i").'" />';
            $html.='</form>'; 
            $html.='</div>'; 
            $html.='</div>'; 
            $html.='</div>';      
            $html.='</div>';
            $html.='</div>'; 
            $html.='</div>';  
            $html.='</div>';
            $html.='</div>';
            $html.='<div class="gap10"></div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a id="addServiceOrder" href="javascript:void(0)" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i>'; 
            $html.='</a></div>';
        }
        else{

            $html.='unmatched';
        }       
        
        echo $html;             
    }

    public function serviceOrderReview()
    {
        $service=$this->session->userdata('services');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $additional_operator = $this->session->userdata('additional_operator');
        $coupon_code = $this->session->userdata('coupon_code');

        $datetime=$this->input->post('datetime');  
        if(!empty($datetime))
        {
            $this->session->set_userdata('datetime',$datetime);
        }

        $sPrice = service_price_on_booking($service,$searched_zipcode,$datetime,$additional_operator,$coupon_code);
        if(!empty($sPrice))
        {
            extract($sPrice);
        }

        if(!empty($coupon_code))
        {
            $response = $this->validate_coupon($coupon_code,$service_charge,$service);
            if(empty($response) || $response!='success')
            {
                $this->session->set_userdata('coupon_code','');
            }
        }

        $html='';
        $html.='<div class="modal-header text-center">';            
        $html.='<button class="back"><a id="backtodatetime" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
        $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';                   
        $html.='<div class="form-social-ico">';             
        $html.='<div class="service-popup-links">';
        $html.='<div class="text-center">';
        $html.='<h4>'.$this->lang->line('PRICING_DEATILS').'</h4>';
        $html.='</div>';
        $html.='<form method="post" id="gotoPayment">'; 
        $html.='<div class="price_detail">'; 
        $language_id=$this->front_model->getLanguage();
        if(!empty($service_charge)){

                $html.='<div class="row">'; 

                $html.='<div class="col-sm-9 text-left">';
                $html.='<p><strong>'.$this->lang->line('MINIMUM_CHARGES').'</strong></p>';
                $html.='<p>'.$service_duration.' '.$this->lang->line('MINUTES').'</p>';
                $html.='</div>';

                $html.='<div class="col-sm-3 text-right">'; 
                $html.='<p><strong>'.getPriceFormate($service_charge,$currency_id).'</strong></p>';    
                $html.='</div>';  

                $html.='</div>';                
        }

        if(!empty($coupon_code) && !empty($coupon_amount)){

                $html.='<div class="row">'; 

                $html.='<div class="col-sm-9 text-left">';
                $html.='<p><strong>'.$this->lang->line('COUPON').' ('.$coupon_code.')</strong></p>';  
                $html.='</div>';

                $html.='<div class="col-sm-3 text-right">'; 
                $html.='<p><strong>'.getPriceFormate($coupon_amount,$currency_id).'</strong></p>';    
                $html.='</div>';  

                $html.='</div>';                
        }

        if(!empty($tax) && !empty($tax_amount))
        {
                $tax_name = !empty($tax_name)?$tax_name:'Tax';

                $html.='<div class="row">'; 

                $html.='<div class="col-sm-9 text-left">';
                $oTax = !empty($other_tax)?' + '.$other_tax.'%':'';
                $html.='<p><strong>'.$tax_name.' ('.$tax.'%'.$oTax.') </strong></p>';  
                $html.='</div>';

                $html.='<div class="col-sm-3 text-right">'; 
                $html.='<p><strong>'.getPriceFormate($tax_amount,$currency_id).'</strong></p>';    
                $html.='</div>';  

                $html.='</div>';                
        }        

        $html.='<div class="row total">';
        $html.='<div class="col-sm-9 text-left">'; 
        $html.='<p><strong>SubTotal</strong></p>';   
        $html.='</div>';    
        $html.='<div class="col-sm-3 text-right">'; 
        $html.='<p><strong>'.getPriceFormate($subtotal,$currency_id).'</strong></p>';    
        $html.='</div>';   
        $html.='</div>';  

        $html.='</div>';
        $html.='</form>';

        $html.='<div class="couponbox">';
        $html.='<div class="price_detail">';

        $html.='<div class="row">';
        $html.='<div class="col-sm-12 text-center"><p><strong>'.$this->lang->line('COUPON').'</strong></p></div>';
        $html.='</div>';

        $html.='<div class="row">';

        $html.='<div class="col-md-3">&nbsp;</div>';
        $html.='<div class="col-md-4">';
            $html.='<input type="text" id="coupon_code" name="coupon_code" placeholder="'.$this->lang->line('COUPON_CODE').'...." class="form-control">';
        $html.='</div>';

        $html.='<div class="col-md-5 text-left">';
            $html.='<button id="applyCoupon" name="applyCoupon" class="btn btn-success">'.$this->lang->line('APPLY').'</button>';
        $html.='</div>';

        $html.='</div>';
        $html.='</div>';
        $html.='</div>';  
        //$html.=$this->session->userdata('address_id'); 
        //$html.=$this->session->userdata('datetime'); 
        $html.='<div class="privacy-pol">By submitting request, you accept our <a target="_blanck" href="'.base_url('terms-conditions').'">terms of use</a> and <a target="_blanck" href="'.base_url('privacy-policies').'">privacy policy</a></div>'; 
        $html.='</div>';  
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap10"></div>';
        $html.='<div class="modal-footer text-center">';
        $html.='<a id="choosertopayment" href="javascript:void(0)" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i>'; 
        $html.='</a></div>';
        echo $html;             
    }

    public function ajaxApplyCoupon()
    {
        $service = $this->session->userdata('services');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $additional_operator = $this->session->userdata('additional_operator');
        $datetime = $this->session->userdata('datetime');

        $sPrice = service_price_on_booking($service,$searched_zipcode,$datetime,$additional_operator);
        if(!empty($sPrice))
        {
            extract($sPrice);
        }

        $coupon_code=$this->input->post('coupon_code');
        if(!empty($coupon_code))
        {
            $response = $this->validate_coupon($coupon_code,$service_charge,$service);
        }

        echo !empty($response)?$response:'';
    }

    public function validate_coupon($coupon_code,$amount,$service=0)
    {
        $statusArr = array('invalid','success','expired','unreached_limit','invalid_cat');
        if(!empty($coupon_code))
        {
            $couponRow = get_record('tbl_coupons',array('coupon_code'=>$coupon_code,'status'=>1));
            $flag = 0;
            $today = date('Y-m-d');

            if(!empty($couponRow))
            {
                if(empty($couponRow->always) && (!empty($couponRow->valid_from) && !empty($couponRow->valid_to) && ($couponRow->valid_from > $today || $couponRow->valid_to < $today)))
                {
                    $flag = 2;
                }
                elseif(!empty($couponRow->restrict_cat) && $couponRow->restrict_cat != $service)
                {
                    $flag = 4;
                }
                elseif(!empty($couponRow->minimum_target) && $amount < $couponRow->minimum_target)
                {
                    $flag = 3;
                }
                else
                {
                    $flag = 1;
                }
            }
            else
            {
               $flag = 0;
            }

            if(!empty($flag) && $flag == 1)
            {
                $this->session->set_userdata('coupon_code',$coupon_code);
            }

            if(array_key_exists($flag, $statusArr))
            {
                $response = $statusArr[$flag];
            }
            else
            {
               $response = 'invalid';
            }
        }
        return $response;
    }

    public function choosePaymentOption()
    {
        $service = $this->session->userdata('services');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $additional_operator = $this->session->userdata('additional_operator');
        $datetime = $this->session->userdata('datetime');
        $coupon_code = $this->session->userdata('coupon_code');
        $sPrice = service_price_on_booking($service,$searched_zipcode,$datetime,$additional_operator,$coupon_code);

        //print_r($sPrice);
        if(!empty($sPrice))
        {
            extract($sPrice);
        }

		$html='';
        $html.='<div class="modal-header text-center">';            
        $html.='<button class="back"><a id="backtodatetime" href="javascript:void(0)" service-data="'.$service.'" ><i class="fa fa-arrow-left"></i></a></button>';
        $html.='<h4 class="modal-title">'.getServicesName($service).'</h4>';
        $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
        $html.='</div>';
        $html.='<div class="modal-body">';                   
        $html.='<div class="form-social-ico">';	            
        $html.='<div class="service-popup-links">';
		$html.='<div class="text-center">';
		$html.='<h4>Payment Method</h4>';
        $html.='</div>';
		$html.='<form method="post" id="gotoPayment" action="'.base_url('order-processing').'">';

        $html.='<div class="user-addres-pop">';		
        $html.='<ul class="custom-r-btn">		
		<li class="pay-r-btn-col w-100">
		<div class="row align-top">';								
		$html.='<div class="col-sm-6">';
		$html.='<ul class="paymentMemo pay-deatil">';
		$html.='<li class="col-sm-12 user-serv-pop pay-rb">';
		$html.='<label class="radio-btnn"><img src="'.base_url('skin/front/images/paypal-logo.jpg').'">';
		  $html.='<input type="radio" name="method" checked="checked" value="paypal">';
		  $html.='<span class="checkmark"></span>';
		$html.='</label>';
		$html.='</li>';
		/*$html.='<li class="col-sm-12 user-serv-pop pay-rb">';
		$html.='<label class="radio-btnn"><img src="'.base_url('skin/front/images/cod-logo.jpg').'">';
		  $html.='<input type="radio" name="method" value="cod">';
		  $html.='<span class="checkmark"></span>';
		$html.='</label>';
		$html.='</li>';*/
		$html.='<ul>';
		$html.='</div>';			 
			 			 
	   $html.='<div class="col-sm-6">';
       $html.='<ul class="priceMemo pay-deatil">';
        $language_id=$this->front_model->getLanguage();
        $servicList=$this->services_model->getServicesOptionList($language_id,$service,1);
        
        if(!empty($service_charge))
        {
            $html.='<li>Minimum Charges:'.$service_duration.'<span>'.getPriceFormate($service_charge,$currency_id).'</span></li>';
        }

        if(!empty($coupon_code) && !empty($coupon_amount)){
            $html.='<li> Coupon ('.$coupon_code.')<span>'.getPriceFormate($coupon_amount,$currency_id).'</span></li>'; 
        }

        if(!empty($tax) && !empty($tax_amount))
        {
            $tax_name = !empty($tax_name)?$tax_name:'Tax';
            $oTax = !empty($other_tax)?' + '.$other_tax.'%':'';
            $html.='<li>'.$tax_name.' ('.$tax.'%'.$oTax.') <span>'.getPriceFormate($tax_amount,$currency_id).'</span></li>';
        }  


	    $html.='<li><strong>Amount Payable</strong> <span><strong>'.getPriceFormate($subtotal,$currency_id).'</strong></span></li>';        	
        $html.='</ul>';
		$html.='</div>
		</div>
		</li>
        </ul>';
        $currency_code = getCurrencyCode($currency_id);
        $html.='<input type="hidden" name="amount" value="'.$service_charge.'">';
        $html.='<input type="hidden" name="currency" value="'.$currency_code.'">';
        $html.='<input type="hidden" name="coupon" value="'.$coupon_code.'">';
        $html.='<input type="hidden" name="discount" value="'.$coupon_amount.'">';
        $html.='<input type="hidden" name="tax" value="'.$tax.'">';
        $html.='<input type="hidden" name="other_tax" value="'.$other_tax.'">';
        $html.='<input type="hidden" name="tax_name" value="'.$tax_name.'">';
        $html.='<input type="hidden" name="tax_amount" value="'.$tax_amount.'">';
        $html.='<input type="hidden" name="total_amount" value="'.$subtotal.'">';
        $html.='</div>';  
        $html.='</div>';  
        $html.='</form>'; 
        
		$html.='</div>';  
        $html.='</div>';  
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="gap10"></div>';
        $html.='<div class="modal-footer text-center">';
		$html.='<a id="confirmpayment" href="javascript:void(0)" class="next-full-btn">Confirm Payment <i class="fa fa-arrow-right"></i>'; 
		$html.='</a></div>';
        echo $html;    	        
    } 

    public function findPriceingListing(){
        $user_id = $this->session->userdata('userID');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $service=$this->input->post('service');
        $back_allowed=$this->input->post('back_allowed');
        $this->session->set_userdata('coupon_code','');
        if(!empty($service)){
            $this->session->set_userdata('services',$service);
        }
        if(!empty($back_allowed))
        {
            $this->session->set_userdata('back_allowed',$back_allowed);
        }
        $back_allowed=$this->session->userdata('back_allowed');
        $language_id=$this->front_model->getLanguage();
        $serviceInfo=$this->services_model->getServicesPrices($service,$searched_zipcode);
        $servicesData=$this->services_model->getSingleServicesDataCollection($language_id,$service);

        $currency_id = !empty($serviceInfo->currency)?$serviceInfo->currency:getCurrency();

        if(!empty($servicesData)){        
            $html='';
            $html.='<div class="modal-header text-center">';   
            if(empty($back_allowed)){   
            $html.='<button class="back"><a id="backtoServices" href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></button>'; 
            }        
            $html.='<h4 class="modal-title">'.$servicesData->name.'</h4>';
            $html.='<button type="button" class="close" data-dismiss="modal">×</button>';
            $html.='</div>';
            $html.='<div class="modal-body">';   
            $html.='<div class="service-popup-links">';
            $html.=$servicesData->description; 


            if(!empty($serviceInfo))
            {
                $html.='<p>('.$this->lang->line('SERVICE_AREA_TAX_INFO').')</p>';
                $html.='<form method="post" id="installatioServices">';
                $html.='<input type="hidden" value="'.$service.'" id="service" name="service">';
                $html.='<div id="price_container" class="table checkinstalation">';

                if(!empty($serviceInfo->service_charge))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('SERVICE_CHARGE_LABEL').'</span>'; 
                    $html.='<span class="l-two">'.!empty($serviceInfo->service_duration)?getDuration($serviceInfo->service_duration,'minute'):'30 '.$this->lang->line('MINUTES').'</span>'; 
                    $price = !empty($serviceInfo->service_charge)?$serviceInfo->service_charge:0;
                    $html.='<span class="l-three">'.getPriceFormate($price,$currency_id).'</span>';
                    $html.='</p>';                    
                }

                if(!empty($serviceInfo->weekend_charge))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('WEEKEND_SERVICE_CHARGE_LABEL').'</span>';
                    $html.='<span class="l-two">'.!empty($serviceInfo->weekend_duration)?getDuration($serviceInfo->weekend_duration,'minute'):'30 '.$this->lang->line('MINUTES').'</span>'; 
                    $price2 = !empty($serviceInfo->weekend_charge)?$serviceInfo->weekend_charge:0;
                    $html.='<span class="l-three">'.getPriceFormate($price2,$currency_id).'</span>';
                    $html.='</p>';                    
                }
                $html.='</div>';

                $html.='<div class="row"><div class="col-md-12">';
                $html.='<label><input type="checkbox" id="additional_operator" name="additional_operator" value="1"> '.$this->lang->line('ARE_YOU_WANT_ADDITIONAL_OPERATOR').'</label>';
                $html.='</div></div>';

                $html.='</form>';
            }
            else 
            {
                $html.='<p class="srvice_not_available"><strong><i class="fa fa-exclamation-triangle" style="color:red"></i> </strong> <br>'.$this->lang->line('SERVICE_NOT_AVAILABLE').'</p>';
            }
            $html.='<p>'.$this->lang->line('TIME_CAL_INSTRUCTION').'</p>';
            $html.='<p class="m-0">'.$this->lang->line('SERVICE_GAP_INSTRUCTION').'</p>';

            $html.='</div>'; 

            $html.='</div>';
            $html.='<div class="modal-footer text-center">';
            $html.='<a href="javascript:void(0)" id="loginMe" userId="'.$user_id.'" service-data="'.$service.'" class="next-full-btn">'.$this->lang->line('NEXT').' <i class="fa fa-arrow-right"></i>'; 
            $html.='</a></div>';
            //$ip = $_SERVER['REMOTE_ADDR'];
            //$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            //echo $details->city;
        }
        echo $html;
    }

    public function findAdditionalOperatorPricing()
    {
        $user_id = $this->session->userdata('userID');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $service=$this->input->post('service');

        $language_id=$this->front_model->getLanguage();
        $serviceInfo=$this->services_model->getServicesPrices($service,$searched_zipcode);
        $servicesData=$this->services_model->getSingleServicesDataCollection($language_id,$service);

        $currency_id = !empty($serviceInfo->currency)?$serviceInfo->currency:getCurrency();
        $html='';
        if(!empty($servicesData))
        {        
            if(!empty($serviceInfo))
            {
                $html.='<p class="text-center"><strong>'.$this->lang->line('ADDITIONAL_WORKER_CHARGES').'</strong></p>'; 
                if(!empty($serviceInfo->service_additional_worker))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('SERVICE_CHARGE_LABEL').'</span>'; 
                    $html.='<span class="l-two">1 '.$this->lang->line('MINUTES').'</span>';
                    $price = !empty($serviceInfo->service_additional_worker)?$serviceInfo->service_additional_worker:0;
                    $html.='<span class="l-three">'.getPriceFormate($price,$currency_id).'</span>';
                    $html.='</p>';                    
                }
                if(!empty($serviceInfo->weekend_additional_worker))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('WEEKEND_SERVICE_CHARGE_LABEL').'</span>';
                    $html.='<span class="l-two">1 '.$this->lang->line('MINUTES').'</span>'; 
                    $price2 = !empty($serviceInfo->weekend_additional_worker)?$serviceInfo->weekend_additional_worker:0;
                    $html.='<span class="l-three">'.getPriceFormate($price2,$currency_id).'</span>';
                    $html.='</p>';                    
                }
            }
        }
        echo $html;
    }

    public function findServicePriceingListing()
    {
        $user_id = $this->session->userdata('userID');
        $searched_zipcode = $this->session->userdata('searched_zipcode');
        $service=$this->input->post('service');

        $language_id=$this->front_model->getLanguage();
        $serviceInfo=$this->services_model->getServicesPrices($service,$searched_zipcode);
        $servicesData=$this->services_model->getSingleServicesDataCollection($language_id,$service);

        $currency_id = !empty($serviceInfo->currency)?$serviceInfo->currency:getCurrency();
        $html='';
        if(!empty($servicesData))
        {        
            if(!empty($serviceInfo))
            {
                if(!empty($serviceInfo->service_charge))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('SERVICE_CHARGE_LABEL').'</span>';
                    $html.='<span class="l-two">'.!empty($serviceInfo->service_duration)?getDuration($serviceInfo->service_duration,'minute'):'30 '.$this->lang->line('MINUTES').'</span>';
                    $price = !empty($serviceInfo->service_charge)?$serviceInfo->service_charge:0;
                    $html.='<span class="l-three">'.getPriceFormate($price,$currency_id).'</span>';
                    $html.='</p>';
                }
                if(!empty($serviceInfo->weekend_charge))
                {
                    $html.='<p>';
                    $html.='<span class="l-one">'.$this->lang->line('WEEKEND_SERVICE_CHARGE_LABEL').'</span>';
                    $html.='<span class="l-two">'.!empty($serviceInfo->weekend_duration)?getDuration($serviceInfo->weekend_duration,'minute'):'30 '.$this->lang->line('MINUTES').'</span>'; 
                    $price2 = !empty($serviceInfo->weekend_charge)?$serviceInfo->weekend_charge:0;
                    $html.='<span class="l-three">'.getPriceFormate($price2,$currency_id).'</span>';
                    $html.='</p>';                    
                }
            }
        }
        echo $html;
    }


    public function addNewAddress(){
        $user_id = $this->session->userdata('userID');
        $site_location=$this->input->post('site_location');
        $data['user_id']=$user_id;
        $data['site_location']=$this->input->post('site_location');
        $data['street_number']=$this->input->post('street_number');
        $data['route']=$this->input->post('route');
        $data['latitude']=$this->input->post('site_latitude');
        $data['longitude']=$this->input->post('site_longitude');
        $data['country']=$this->input->post('country');
        $data['state']=$this->input->post('state');
        $data['city']=$this->input->post('city');
        $data['postal_code']=$this->input->post('postal_code');
        $insertId=$this->front_model->setInsertData($this->users_address, $data);  
        $html='';
        if($insertId>0){
            $html.='<li class="custom-r-btn-col w-100">';
            $html.='<div class="row align-items-center">';              
            $html.='<div class="user-addres-pop col-sm-8 cus-rb">';  
            $html.='<label class="radio-btnn">'.$site_location;
            $html.='<input type="radio" name="radio" checked="checked" value="'.$insertId.'" required>';
            $html.='<span class="checkmark"></span>';
            $html.='</label>';       
            $html.='</div>';
            $html.='<div class="edit-del-btn col-3">';
            //$html.='<a href="javascript:void(0)" editId="'.$insertId.'" class="editMe btn btn-primary"><i class="fas fa-edit"></i></a>'; 
            $html.='<a href="javascript:void(0)" delId="'.$insertId.'" class="deleteMe btn btn-danger"><i class="fas fa-times"></i></a>'; 
            $html.='</div>';
            $html.='</div>';
            $html.='</li>';
        }
        echo $html;
    }

    public function deleteUserAddress(){
       $id=$this->input->post('id');
       $this->front_model->setStatusData($this->users_address, $id);
       echo 'Your address has been delete successfully';
    }

    public function details(){
        $data[]='';
        $this->load->view('front/include/header', $data);
        $this->load->view('front/services/details', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

    public function send_services_not_found_email() { 
        $email = getAdministratorEmail();
        $custome_area=$this->input->post('custome_area');
        $custome_name=$this->input->post('custome_name');
        $custome_email=$this->input->post('custome_email');      
        $email_template  = 'services_request.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$custome_name,
        '{{custome_area}}'=>$custome_area,
        '{{custome_email}}'=>$custome_email,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Services Request',$message);
    }

    public function requestStepOne(){ 
    	$html='';
        $post=$this->input->post();
        if(!empty($post)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('phonecode', 'mobile', 'required|is_unique[tbl_users.mobile]',array('is_unique' => 'Applicant mobile number already exist.')); 
            if($this->form_validation->run() == FALSE) { 
               echo '2'; 
            } else {
                $chooseserices=$this->input->post('chooseserices');
                $phonecode=$this->input->post('mobbox');
                $message=$this->input->post('message');
                $business=$this->input->post('business');
                $business_name=$this->input->post('business_name');
                $designation=$this->input->post('designation');
                $license=$this->input->post('license');
                $license_details=$this->input->post('license_details');
                
            	$html.='<form role="form" id="requestStepOne" action="" class="personalDetails" method="post" >';
                $html.='<input type="hidden" value="'.(isset($business)?$business:"").'" name="business" id="business">';                
                $html.='<input type="hidden" value="'.(isset($business_name)?$business_name:"").'" name="business_name" id="business_name">';                
                $html.='<input type="hidden" value="'.(isset($designation)?$designation:"").'" name="designation" id="designation">';                
                $html.='<input type="hidden" value="'.(isset($license)?$license:"").'" name="license" id="license">';                
                $html.='<input type="hidden" value="'.(isset($license_details)?$license_details:"").'" name="license_details" id="license_details">';                
                $html.='<input type="hidden" value="'.(isset($chooseserices)?$chooseserices:"").'" name="chooseserices" id="chooseserices">';                
                $html.='<input type="hidden" value="'.(isset($phonecode)?$phonecode:"").'" name="phonecode" id="phonecode">';
                $html.='<input type="hidden" value="'.(isset($message)?$message:"").'" name="message" id="message">';                       
                $html.='<input type="hidden" value="'.(isset($site_latitude)?$site_latitude:"").'" id="latitude" name="site_latitude"/>';          
                $html.='<input type="hidden" value="'.(isset($site_longitude)?$site_longitude:"").'" id="longitude" name="site_longitude"/>';
            	$html.='<fieldset>
        		<div class="form-group jformgroup">
        		<input type="text" name="fname" placeholder="'.$this->lang->line('FIRST_NAME').'" class="f1-email form-control" id="f-name" required="">
        		</div>
                <div class="form-group jformgroup">
                <input type="text" name="lname" placeholder="'.$this->lang->line('LAST_NAME').'" class="f1-email form-control" id="l-name" required="">
                </div>
        		<div class="form-group jmargin">
        		<input type="email" name="useremail" placeholder="'.$this->lang->line('EMAIL').'..." pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="'.$this->lang->line('INVALID_EMAIL').'" class="f1-email form-control" id="f1-email" required="">
        		</div>
                <div class="form-group jmargin">
                <input type="text" onclick="initialize();" placeholder="'.$this->lang->line('ADDRESS').'" name="site_location"  class="f1-last-name form-control" id="site_location" required="">
                </div>
                <div class="form-group jformgroup">               
                <input type="text" name="aptno" placeholder="'.$this->lang->line('APT_UNIT_NO').'" class="f1-email form-control" id="aptno-name">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_number" placeholder="'.$this->lang->line('STREET_NO').'" class="f1-email form-control" id="street_number" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_name" placeholder="'.$this->lang->line('STREET_NAME').'" class="f1-email form-control" id="street_name-name" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="street_type" placeholder="'.$this->lang->line('STREET_TYPE').'" class="f1-email form-control" id="street_type-name" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="direction" placeholder="'.$this->lang->line('DIRECTION').'" class="f1-email form-control" id="route">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="country" placeholder="'.$this->lang->line('COUNTRY').'" class="f1-email form-control" id="country" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="state" placeholder="'.$this->lang->line('STATE_PROVINCE').'" class="f1-email form-control" id="administrative_area_level_1" required="">
                </div> 
                <div class="form-group jformgroup">
                <input type="text" name="city" placeholder="'.$this->lang->line('CITY').'" class="f1-email form-control" id="locality" required="">
                </div>
                <div class="form-group jformgroup">
                <input type="text" name="postal_code" placeholder="'.$this->lang->line('ZIP_CODE').'" class="f1-email form-control" id="postal_code" required="">
                </div>
                
        		<div class="form-group jformgroup">
        		<input type="password" name="password" placeholder="'.$this->lang->line('PASSWORD').'..." class="f1-password form-control" id="password">
        		</div>
        		<div class="form-group jformgroup">
                <input type="password" name="confirm_password" placeholder="'.$this->lang->line('CONFIRM_PASSWORD').'..." 
                class="f1-repeat-password form-control" id="confirm_password" required>
                </div>
        		<div class="f1-buttons">
        		<button type="submit" class="btn btn-next choosePersonalNext">'.$this->lang->line('NEXT').'</button>
        		</div>
        		</fieldset>';
            	$html.='</form>';
    	        echo $html;
            }
        }
    }

    public function requestStepThanks(){ 
        $html='';
        $post_data=$this->input->post();
        if(!empty($post_data))
        { 
            $userdata['username']=$this->input->post('fname').' '.$this->input->post('lname');
            $userdata['fname']=$this->input->post('fname');
            $userdata['lname']=$this->input->post('lname');
            $userdata['email']=$this->input->post('useremail');
            $userdata['site_location']=$this->input->post('site_location');
            $userdata['aptno']=$this->input->post('aptno');
            $userdata['street_number']=$this->input->post('street_number');
            $userdata['street_name']=$this->input->post('street_name');
            $userdata['street_type']=$this->input->post('street_type');
            $userdata['direction']=$this->input->post('direction');
            $userdata['country']=$this->input->post('country');
            $userdata['state']=$this->input->post('state');
            $userdata['city']=$this->input->post('city');
            $userdata['postal_code']=$this->input->post('postal_code');
            $userdata['latitude']=$this->input->post('site_latitude');
            $userdata['longitude']=$this->input->post('site_longitude');                
            $userdata['password']=md5($this->input->post('password'));
            $userdata['mobile']=$this->input->post('phonecode');
            $userdata['role']=5;

            //print_r($userdata); die;
            $insertId=$this->front_model->setInsertData($this->users, $userdata);
            
            if($insertId>0)
            {                     
                $data['services_type']=$this->input->post('chooseserices');
                $data['message']=$this->input->post('message');
                $data['business']=$this->input->post('business');
                $data['business_name']=$this->input->post('business_name');
                $data['designation']=$this->input->post('designation');
                $data['license']=$this->input->post('license');
                $data['license_details']=$this->input->post('license_details');
                $data['request_date']=date("Y-m-d h:i:s");
                $data['service_status']=2;
                $data['user_id']=$insertId;

                $service_request_id = $this->front_model->setInsertData($this->provider_request, $data);
                if($service_request_id>0)
                {                        
                    $data2['service_id']=$this->input->post('chooseserices');                     
                    $data2['services_request_id']=$service_request_id;      
                    $data2['service_zip_code']=$this->input->post('postal_code');
                    $data2['service_category_code']=$this->input->post('chooseserices');
                    $this->front_model->setInsertData($this->provider_request_setting, $data2);

                    $addr['zipcode']=$this->input->post('postal_code');
                    $addr['latitude']=$this->input->post('site_latitude');
                    $addr['longitude']=$this->input->post('site_longitude');
                    $addr['country']=$this->input->post('country');
                    $addr['place_name']=$this->input->post('state');
                    $addr['area']=$this->input->post('city');
                    $addr['address']=$this->input->post('site_location');

                    $this->front_model->checkNinsertData($this->services_area,'zipcode', $addr);
                }
                $this->send_provider_thank_you();
                $this->send_provider_request_to_administrator();
                $html.='<fieldset>                    
                   <div class="bus-part-success">
                    <i class="fa fa-check"></i>
                    <h4>Congratulations!</h4>
                    <h5>You have successfully signed up as a partner on First Choice Everything</h5>
                    <p>Thank you for requesting information. We will email you the infor mation you need.</p>
                    <p>In that information pavkage we will explain how our system work and a form (Form 102)</p>
                    <p>If you need more information you will fill that form and we will send you more deatails.</p>
                   </div>
                </fieldset>';
            }
            echo $html;
        }
        
    }

    public function providerAcceptRequest()
    { 
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');

        $dataupdate['vendor_id']=$user_id;
        $dataupdate['status']   =1;

        $leaddata['service_order_id']=$id;
        $leaddata['status']=1;
        $leaddata['accepter_id']=$user_id;      
        

        $is_other_accepted = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $request_desicion = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);

        if(empty($is_other_accepted) && empty($request_desicion))
        {
           $this->front_model->setInsertData($this->service_leads, $leaddata);
           $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);            
           $this->sendAcceptNotificationToCustomer($id);
           $this->session->set_flashdata('message','You have accept request successfully');
        }
        else
        {
            $this->session->set_flashdata('message','This request is not available to accept.'); 
        }

        redirect('customer-request-details/'.$rawId, 'refresh');
    }

    public function sendAcceptNotificationToCustomer($order_id)
    {
        $bookingData = getServiceBookingDetailById($order_id);
        $customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
        $service_id = !empty($bookingData->services)?$bookingData->services:0;

        $email_template  = 'accept-notification.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>getCustomerName($customer_id),
        '{{service_name}}'=> getServicesName($service_id),
        '{{booking_id}}' => $order_id,
        '{{detail_link}}' => base_url('booking-details').'/'.base64_encode($order_id),
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        $email = getCustomerEmail($customer_id);
        send_email($email,'Service booking accepted',$message);
    }
    
    public function providerRejectRequest()
    { 
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');

        $leaddata['service_order_id']=$id;
        $leaddata['status']=2;
        $leaddata['accepter_id']=$user_id;       

        $is_other_accepted = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $request_desicion = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);

        $order_data = $this->front_model->getDataCollectionByField($this->service_orders,'id',$id);
        $spArr = explode(",", $order_data['services_provider']);

        //print_r($spArr); die;

        if(empty($is_other_accepted) && empty($request_desicion))
        {
           $this->front_model->setInsertData($this->service_leads, $leaddata);           
           $this->session->set_flashdata('message','You have rejected request successfully'); 
        }
        else
        {
            $this->session->set_flashdata('message','This request is not available to reject.'); 
        }

        $flag = array();
       foreach ($spArr as $sp_id)
       {
            $whereArr = array('service_order_id'=>$id,'accepter_id'=>$sp_id,'status'=>2);
            $checker = get_total_records($this->service_leads,$whereArr);
            $cnt = (!empty($checker) && $checker>0)?1:0;
            array_push($flag, $cnt);
       }
       if(!empty($flag) && !in_array(0, $flag))
       {
            $dataupdate['status']   = 3;
            $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
       }

        redirect('customer-request-details/'.$rawId, 'refresh');
    }


    public function customerCancelRequest()
    { 
        $id=base64_decode($this->uri->segment(2));
        $dataupdate['status']   =2;
        $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
        $this->session->set_flashdata('message','You have cancel request successfully');
        redirect('my-bookings', 'refresh');
    }

    public function closeServiceRequest()
    { 
        $id=base64_decode($this->uri->segment(2));
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
        $service_id = !empty($bookingData->services)?$bookingData->services:0;

        $customer_email = getCustomerEmail($customer_id);
        $customer_name = getCustomerName($customer_id);
        $customer_phone = getCustomerContact($customer_id);

        //print_r($bookingData); die;
        $dataupdate['status']   =5;
        $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);

        $email_templateN  = 'booking-closed-notification.html';
        $templateTagsN =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$customer_name,
        '{{service_name}}'=> getServicesName($service_id),
        '{{booking_id}}' => $id,
        '{{feedback_form_url}}' => base_url('feedback').'/'.base64_encode($id),
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message_notify = email_compose($email_templateN,$templateTagsN);
        send_email($customer_email,'Booking Closed Confirmation & Feedback Request',$message_notify);

        $smsMsg = 'Dear '.$customer_name.', ';
        $smsMsg.= 'Service Booking ID #'.$id.' has successfully completed and closed.';
        send_sms($customer_phone,$smsMsg);

        $this->session->set_flashdata('message','Service request closed successfully');
        redirect('customer-request-details/'.$this->uri->segment(2), 'refresh');
    }

    public function ajaxAssignOperator()
    {
        $post_data=$this->input->post();
        if(!empty($post_data))
        {
            $id = base64_decode($post_data['id']);
            $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
            $operator_id = $post_data['operator'];
            $email = getCustomerEmail($operator_id);
            $dataupdate['operator_id']=$operator_id;
            $dataupdate['status']=4;
            $response = $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
            if(!empty($response))
            {
                $customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
                $service_id = !empty($bookingData->services)?$bookingData->services:0;


                $customer_name = getCustomerName($customer_id);
                $customer_email = getCustomerEmail($customer_id);
                $customer_phone = getCustomerContact($customer_id);

                $email_templateN  = 'assign-operator-notification.html';
                $templateTagsN =  array(
                '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                '{{site_name}}'=>'firstchoice.com',
                '{{site_url}}'=> base_url(),
                '{{team_name}}'=>'firstchoice',
                '{{user_name}}'=>$customer_name,
                '{{service_name}}'=> getServicesName($service_id),
                '{{booking_id}}' => $id,
                '{{detail_link}}' => base_url('booking-details').'/'.base64_encode($id),
                '{{year}}'=>date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com',
                );
                $message_notify = email_compose($email_templateN,$templateTagsN);                
                send_email($customer_email,'Operator Assigned to service booking',$message_notify);

                $operator_name  = getCustomerName(!empty($operator_id)?$operator_id:0);
                $operator_email = getCustomerEmail(!empty($operator_id)?$operator_id:0);
                $operator_phone = getCustomerContact(!empty($operator_id)?$operator_id:0);

                $email_template  = 'assign-operator.html';
                $templateTags =  array(
                '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                '{{site_name}}'=>'firstchoice.com',
                '{{site_url}}'=> base_url(),
                '{{team_name}}'=>'firstchoice',
                '{{user_name}}'=>$operator_name,
                '{{service}}'=> getServicesName(!empty($bookingData->services)?$bookingData->services:0),
                '{{service_address}}' => getServicesAddress(!empty($bookingData->address_id)?$bookingData->address_id:0),
                '{{service_schedule}}' => !empty($bookingData->booking_datetime)?$bookingData->booking_datetime:'',
                '{{customer_name}}' => $customer_name,
                '{{customer_email}}' => $customer_email,
                '{{customer_contact}}' => $customer_phone,
                '{{detail_link}}' => base_url('job_details').'/'.base64_encode($id),
                '{{year}}'=>date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com',
                );
                $message = email_compose($email_template,$templateTags);
                send_email($email,'Job Assigned',$message);

                $smsMsg = 'Dear '.$customer_name.', ';
                $smsMsg.= 'Your service request has processed and Operator has assigned to you against Service Booking ID #'.$id;
                send_sms($customer_phone,$smsMsg);

                $smsMsg2 = 'Dear '.$operator_name.', ';
                $smsMsg2.= 'A new service has assigned to you by Service Provider which Service Booking ID is #'.$id;
                send_sms($operator_phone,$smsMsg2);
                die;

               echo 'success';                
            }
            else
            {
               echo 'failed';
            }
        }
        else
        {
            echo 'invalid';
        }
    }

    public function notifyPaypal()
    { 

        $data['value'] ='ddddd';
        $this->front_model->setUpdateData('tbl_settings', $data, 36);
        /* STEP 1: Read POST data
        reading posted data from directly from $_POST causes serialization 
        issues with array data in POST
        reading raw POST data from input stream instead. */
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
          $keyval = explode ('=', $keyval);
          if (count($keyval) == 2)
             $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
           $get_magic_quotes_exists = true;
        } 
        foreach ($myPost as $key => $value) {        
           if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
                $value = urlencode(stripslashes($value)); 
           } else {
                $value = urlencode($value);
           }
           $req .= "&$key=$value";
        }
        // STEP 2: Post IPN data back to paypal to validate
        $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
        // of the certificate as shown below.
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);
        // STEP 3: Inspect IPN validation result and act accordingly
        if (strcmp ($res, "VERIFIED") == 0) {
            // check whether the payment_status is Completed
            // check that txn_id has not been previously processed
            // check that receiver_email is your Primary PayPal email
            // check that payment_amount/payment_currency are correct
            // process payment
            // assign posted variables to local variables
            $data['value'] = $_POST;
            


            $this->front_model->setUpdateData('tbl_settings', $data, 36);

        } else if (strcmp ($res, "INVALID") == 0) {
            $data['value'] = $_POST;
            $this->front_model->setUpdateData('tbl_settings', $data, 36);
        }
    }
    
    public function reschedule() { 
        $segment=$this->uri->segment(2);
        $id=base64_decode($segment);
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('reschedule_date', 'reschedule date', 'required'); 
            $this->form_validation->set_rules('resion', 'resion', 'required'); 
            if($this->form_validation->run() == FALSE) { 
               $this->session->set_flashdata('error','Please choose reschedule date and resion');
            } else {
            $dataupdate['booking_datetime']   =$this->input->post('reschedule_date');
            $dataupdate['resion']   =$this->input->post('resion');
            $this->front_model->setUpdateData($this->service_orders, $dataupdate, $id);
            $this->session->set_flashdata('message','You have reschedule request successfully');
            }
        }
        redirect('booking-details/'.$segment, 'refresh');
    }

    public function providerCancelRequest() { 
    }

    public function order_processing() { 

        $post=$this->input->post();
        if(!empty($post)){
            $paymentMethod=$this->input->post('method');
            $amount = $this->input->post('amount');
            $coupon = $this->input->post('coupon'); 
            $discount = $this->input->post('discount'); 
            $tax = $this->input->post('tax'); 
            $other_tax = $this->input->post('other_tax'); 
            $tax_name = $this->input->post('tax_name'); 
            $tax_amount = $this->input->post('tax_amount'); 
            $total_amount = $this->input->post('total_amount');                  
            $user_id = $this->session->userdata('userID');
            $address_id = $this->session->userdata('address_id');
            $services = $this->session->userdata('services');
            $datetime = $this->session->userdata('datetime');
            $newinstalation = $this->session->userdata('newinstalation');
            $additional_operator = $this->session->userdata('additional_operator');

            //echo $total_amount; 
            $addressDetails = getAddressDetails($address_id);
            //print_r($addressDetails);
            //$nearestUser = findNearestUsers($addressDetails);
            $nearestUser = findNearestProviders($services,$addressDetails);
            //print_r($nearestUser);die;
            $nearestUser=array_column($nearestUser,'id');
            $imploadUser = implode (',' , $nearestUser);
            if(!empty($newinstalation)){

                $ordesinfo['other_services']=$newinstalation;

            } else {

                $ordesinfo['other_services']=0;
            }
            $currency=$this->input->post('currency');
            $code = !empty($currency)?$currency:getCurrencySign();
            
            $ordesinfo['code']=$code;
            $ordesinfo['booking_datetime']=$datetime;
            $ordesinfo['user_id']=$user_id;
            $ordesinfo['services_provider']=$imploadUser;
            $ordesinfo['address_id']=$address_id;
            $ordesinfo['services']=$services;
            $ordesinfo['additional_operator']= !empty($additional_operator)?1:0;
            $ordesinfo['code']=$code;
            $ordesinfo['amount']=$amount;
            $ordesinfo['coupon']=$coupon;
            $ordesinfo['discount']=$discount;
            $ordesinfo['tax']=$tax;
            $ordesinfo['other_tax']=$other_tax;
            $ordesinfo['tax_name']=$tax_name;
            $ordesinfo['tax_amount']=$tax_amount;
            $ordesinfo['total_amount']=$total_amount;
            $ordesinfo['method']=$paymentMethod;
            $ordesinfo['status']=0;

            //echo "<pre>"; print_r($ordesinfo); echo "</pre>"; die;
            $insertId=$this->front_model->setInsertData($this->service_orders, $ordesinfo);
            //$orderId=$this->session->userdata('orderId');
            $data['order_id']=$insertId;
            
            $data['code']=$code;
            $data['amount']=$total_amount;
            //$data['ordersDetails']=getOrderDetailsById($orderId);
            $data['userData']=getCustomerDetailsById($user_id);

            //echo "<pre>"; print_r($data); echo "</pre>"; die;
            if($paymentMethod=='cod'){

            } else {            
                $this->load->view('front/payment/paypal-services', $data);            
            } 
        } else {
            redirect('/','refresh');
        }        
    }

    public function pay_outstanding()
    { 
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);

        $outstanding = calculate_additional_estimate($id);


        //echo "<pre>"; print_r($outstanding); echo "</pre>"; die;
        $dataArr['additional_hours'] = $outstanding['additional_time'] / 60;
        $dataArr['additional_hrs_charges'] = $outstanding['additional_amount'];
        $dataArr['additional_amt_tax'] = $outstanding['additional_tax'];
        $dataArr['additional_subtotal'] = $outstanding['additional_subtotal'];
        $dataArr['additional_payment_method'] = 'paypal';
        $this->front_model->setUpdateDataByWhere('tbl_service_operations', $dataArr, array('service_order_id'=>$id));

        $user_id = $this->session->userdata('userID');
        $data['amount'] = $outstanding['additional_subtotal'];
        $data['item_name'] = getServicesName(!empty($bookingData->services)?$bookingData->services:0);
        $data['code'] = !empty($bookingData->code)?$bookingData->code:getCurrencySign();
        $data['order_id'] = $id;
        $data['userData'] = getCustomerDetailsById($user_id);

        //print_r($data); die;
        $this->load->view('front/payment/paypal-service-additional', $data);
    }

    public function serviceBookingDetails()
    {
        CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
        $data['bookingData'] = $bookingData;
        $data['operation_row'] = $operation_row;
        $data['is_valid_user'] = (!empty($bookingData->user_id) && $bookingData->user_id==$user_id)?1:0;
        $data['rawId'] = $rawId;
        $data['is_other_accepted'] = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $data['request_desicion'] = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);
        $data['is_service_started'] = $this->front_model->isServiceStarted($this->service_operations, $id);
        $data['is_service_ended'] = $this->front_model->isServiceEnded($this->service_operations, $id);
        $data['service_start_time'] = $start_time = $operation_row['service_start_time'];
        $data['service_end_time'] = $end_time = $operation_row['service_end_time'];

        if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
        {
            $allocated_time = getAllocatedTime($id);
            $data['service_allocated'] = getDuration($allocated_time,'minute');
            $data['time_spent'] = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
            $data['additional_time'] = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

            $additional_time = calculateAdditionalTime($id,$start_time,$end_time);
            $additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;           
            if(!empty($additional_operator))
            {
                $additional_amount = calculateAdditionalAmount4AO($id,$start_time,$end_time);
            }
            else
            {
                $additional_amount = calculateAdditionalAmount($id,$start_time,$end_time);
            }
            $data['additional_amount'] = $additional_amount;
            $data['additional_tax'] = $additional_tax = calculateTax($id,$additional_amount);
            $data['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        }
                
        $data['payment_status']    = $operation_row['payment_status'];

        $data['code']=$this->uri->segment(2);
        $data['page_title']='Booking Details';
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        //$this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/booking-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }


    public function send_reschedule_email() { 
        $email=getAdministratorEmail();
        $email_template  = 'reschedule.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{otp}}'=>$otp,
        '{{mobile}}'=>$mobile,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Reschedule',$message);
    }

    public function send_otp($mobile)
    { 
        $otp=rand(111111,999999);
        $updatedata['otp']=$otp;
        $user_id=getCustomerIdByMobile($mobile);
        if(!empty($user_id))
        {
            $this->front_model->setUpdateData($this->users, $updatedata, $user_id);        
            $email=getCustomerEmail($user_id);
            $email_template  = 'otp-pass.html';
            $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}'=>'firstchoice.com',
            '{{site_url}}'=> base_url(),
            '{{team_name}}'=>'firstchoice',
            '{{otp}}'=>$otp,
            '{{mobile}}'=>$mobile,
            '{{year}}'=>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
            $message = email_compose($email_template,$templateTags);
            send_email($email,'One Time Password (otp)',$message);
        }

        $smsMsg = 'Dear '.getCustomerName($user_id).', ';
        $smsMsg.= 'Login OTP from FirstChoice is '.$otp;

        send_sms($mobile,$smsMsg);
    }

    public function send_provider_thank_you() { 
        $username=$this->input->post('username');
        $email=$this->input->post('useremail');           
        $email_template  = 'service_provider_thank_you.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$username,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Thank You For Request',$message);
    } 

    public function send_provider_request_to_administrator() { 
        $username=$this->input->post('username');
        $useremail=$this->input->post('useremail');
        $chooseserices=$this->input->post('chooseserices');
        $site_location=$this->input->post('site_location');
        $email= getAdministratorEmail();  
        $email_template  = 'service_provider_request.html';
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$username,
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Thank You For Request',$message);
    }
}