<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Supports extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('supports_model');
        $this->load->model('front_model');
        $this->load->model('smtp_model');
        $this->load->helper('front_helper');
    }
    
    public $users='tbl_users';
    public $services='tbl_services';
    public $tickets='tbl_support_tickets';
    public $support_responses='tbl_support_responses';
    public $products_orders='tbl_products_orders';
    public $service_orders='tbl_service_orders';
    public $service_operations='tbl_service_operations';    
    public $service_leads = 'tbl_service_leads';

    public function index()
    {
        CheckUserLoginSession();
        $data['page_title']='Support';
        $user_id = $this->session->userdata('userID');


        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('user_id'=>$user_id);
        $total_records = get_total_records($this->tickets,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->tickets,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

        //$data['records']=$this->supports_model->getDataCollectionByUserId($this->tickets,$user_id);
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/supports/supports', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function supportRequestDeatil($id)
    {
        CheckUserLoginSession();
        $rawid = $id;
        $id = !empty($id)?base64_decode($id):'';
        $data['page_title']='Support Ticket #'.$id;
        $post_data=$this->input->post();
        $user_id = $this->session->userdata('userID');
        //print_r($post_data); die();
        if(!empty($post_data))
        {        
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('response', 'comment', 'required');                    
            if($this->form_validation->run() != FALSE) 
            {                
                $dataArr['ticket_id']       = $id;
                $dataArr['responder_id']    = $user_id;
                $dataArr['response']        = $this->input->post('response');
                $dataArr['status']          = 1;

                $insert_id= $this->supports_model->setInsertData($this->support_responses, $dataArr);
                if($insert_id>0)
                {
                    //$this->send_ticket_submission_notification_email($insert_id);
                    $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your Response/Comment has submitted successfully.'));
                    redirect('support-request-details/'.$rawid,'refresh');
                }
                else
                {
                    $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Oops: your support concern has failed to submit. Please try again later.'));
                }
            }
        }



        $data['record']=$this->supports_model->getDataRowCollectionById($this->tickets,$id);
        $data['responses']=$this->supports_model->getDataRecords($this->support_responses,array('ticket_id'=>$id));
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/supports/support-request-detail', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function order_details()
    {
        CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
        $data['is_sent_otp'] = !empty($bookingData->otp)?1:0;
        $data['bookingData']= $bookingData;
        $data['operation_row']= $operation_row;
        $data['rawId'] = $rawId;
        $data['is_other_accepted'] = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $data['request_desicion'] = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);
        $data['is_otp_verified'] = $this->front_model->isOTPVerified($this->service_operations, $id);
        $data['is_service_started'] = $this->front_model->isServiceStarted($this->service_operations, $id);
        $data['is_service_ended'] = $this->front_model->isServiceEnded($this->service_operations, $id);        
        $data['service_start_time'] = $start_time = $operation_row['service_start_time'];
        $data['service_end_time'] = $end_time = $operation_row['service_end_time'];
       
       if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
        {
            $allocated_time = getAllocatedTime($id);
            $data['service_allocated'] = getDuration($allocated_time,'minute');
            $data['time_spent'] = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
            $data['additional_time'] = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

            
            $additional_time = calculateAdditionalTime($id,$start_time,$end_time);

            $additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;           
            if(!empty($additional_operator))
            {
                $additional_amount = calculateAdditionalAmount4AO($id,$start_time,$end_time);
            }
            else
            {
                $additional_amount = calculateAdditionalAmount($id,$start_time,$end_time);
            }
            $data['additional_amount'] = $additional_amount;

            $data['additional_tax'] = $additional_tax = calculateTax($id,$additional_amount);
            $data['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        } 

        $data['code']=$this->uri->segment(2);
        $data['page_title']='Job Details';
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/supports/order-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function supporttickets()
    {
        CheckUserLoginSession();
        $data['page_title']='Support Tickets';
        $user_id = $this->session->userdata('userID');
        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array(); //array('user_id'=>$user_id);
        $total_records = get_total_records($this->tickets,$whereArr);
        if ($total_records > 0) 
        {
            $data["records"] = get_current_page_records($this->tickets,$limit_per_page,$start,$whereArr,'id','DESC');
            $data["pagination"] = Jpagination($total_records, $limit_per_page, $limit_per_page);
        }

        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/supports/support-tickets', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function requestSupport()
    {
        CheckUserLoginSession();

        $post_data=$this->input->post();
        $user_id = $this->session->userdata('userID');
        //print_r($post_data); die();
        if(!empty($post_data))
        {        
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('support_type', 'type of support', 'required');
            $this->form_validation->set_rules('orders', 'order refrence', 'trim|required');
            $this->form_validation->set_rules('support_title', 'support title', 'trim|required');
            $this->form_validation->set_rules('support_concern', 'support title', 'trim|required');
                    
            if($this->form_validation->run() != FALSE) 
            {                
                $dataArr['support_for'] = $this->input->post('support_type');
                $dataArr['order_id']    = $this->input->post('orders');
                $dataArr['user_id']     = $user_id;
                $dataArr['title']       = $this->input->post('support_title');
                $dataArr['description'] = $this->input->post('support_concern');
                $dataArr['status']      = 1;

                $insert_id= $this->supports_model->setInsertData($this->tickets, $dataArr);
                if($insert_id>0)
                {
                    $this->send_ticket_submission_notification_email($insert_id);
                    $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your Support ticket has submitted successfully.'));
                    redirect('supports','refresh');
                }
                else
                {
                    $this->session->set_flashdata('notification',array('error'=>1,'message'=>'Oops: your support concern has failed to submit. Please try again later.'));
                }
            }
        }

        $data['page_title']='Support Tickets';
        $data['currencyOption'] = getCurrencyOptions('tbl_currencies', 'Select Currency');        

        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/supports/request-support', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function ajaxOrderOptions()
    {
        CheckUserLoginSession();
        $post_data=$this->input->post();
        $user_id = $this->session->userdata('userID');

        if($post_data['support_type']=='services')
        {
            $whereArr = array('user_id'=>$user_id,'status'=>5);
            $orders=$this->supports_model->getDataRecords($this->service_orders,$whereArr);
        }
        elseif($post_data['support_type']=='products')
        {
            $whereArr = array('user_id'=>$user_id,'orderstatus'=>5);
            $orders=$this->supports_model->getDataRecords($this->products_orders,$whereArr);
        }
        else
        {
            $orders=array();
        }

        $optionHTML = '<option value="">Choose your order</option>';
        if(!empty($orders))
        {
            foreach ($orders as $ord)
            {
                $optionHTML.= '<option value="'.$ord->id.'">#'.$ord->id.'</option>';
            }            
        }

        echo $optionHTML;
        
    }

    public function deleteSupportRequest($id)
    {
        CheckUserLoginSession();
        $id = !empty($id)?base64_decode($id):'';
        $deleted_id=$this->supports_model->setDeleteData($this->tickets,$id);
        $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your Support ticket has been deleted successfully.'));
        redirect('supports','refresh');
    }

    public function send_ticket_submission_notification_email($id)
    {
        $record=$this->supports_model->getDataRowCollectionById($this->tickets,$id);
        $email_template  = 'support-request.html';
        $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}'=>'firstchoice.com',
            '{{site_url}}'=> base_url(),
            '{{team_name}}'=>'firstchoice',
            '{{user_name}}'=>getCustomerName($record->user_id),
            '{{ticket_number}}'=>$record->id,
            '{{ticket_url}}'=>base_url('support-request-details').'/'.base64_encode($record->id),
            '{{year}}'=>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
        $message = email_compose($email_template,$templateTags);
        $to_email = getCustomerEmail($record->user_id);
        if(!empty($to_email))
        {
            send_email($to_email,"Support request notification",$message);
        }        
    }
}