<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
	{
    	parent::__construct();
    	$this->lang->load('front','english');
    	$this->load->model('login_model'); 
    	$this->load->model('smtp_model');
    	$this->load->model('front_model');
    	$this->load->helper('front_helper');
    	$this->load->model('services_model');
    	$this->load->helper('download');
	}
	
	public $users='users';
	public $service_orders='service_orders';
	public $service_operations='service_operations';	
	public $service_leads = 'service_leads';
	public $services_area='services_area';
	public $service_provider_request="service_provider_request";
	public $service_provider_settings = "service_provider_settings";
	public $products_orders='products_orders';
	public $videos='videos';
	public $invoice='invoice';

	public function frontlogin()
	{
		$user_id = $this->session->userdata('userID');
		if(!empty($user_id)){
			redirect('my-account','refresh');
		}
		else
		{   
			$login_arr=$this->input->post();
			if (!empty($login_arr['rememberme']))
			{
				$useremail = $login_arr['useremail'];
				$password = $login_arr['password'];
				// Outputs: This is a plain-text message!
				//echo $this->encryption->decrypt($ciphertext);
				$this->input->set_cookie('rememberme', 1, 86500); /* Create cookie for store flag */
			    $this->input->set_cookie('uemail', $useremail, 86500); /* Create cookie for store emailid */
			    $this->input->set_cookie('upassword', $password, 86500); /* Create cookie for password */
			}
			else
			{
				//delete_cookie('rememberme'); /* Delete flag cookie */
			    //delete_cookie('uemail'); /* Delete email cookie */
			    //delete_cookie('upassword'); /* Delete password cookie */
			}

			if(!empty($login_arr))
			{

				$userdata= $this->login_model->UserLogin();				
				if($userdata==1)
				{
					echo $userdata;
				}
				elseif($userdata==2)
				{
					echo $userdata;
				}
				else
				{
					echo $userdata;
				}					
				
			}
		}		
	}

	public function forgotlogin(){    
		   
		$login_arr=$this->input->post('useremail');			
		if(!empty($login_arr)){
			$userdata= $this->login_model->UserForgot();	
			$status=$userdata->status;
			$id=$userdata->id;
			if($status==1){
				echo $status;
                $this->send_forgot_email($id);
			}elseif($status==2){
				echo $status;
			}else{
				echo $status;
			}					
		}
	}

	public function logout(){
		$this->session->set_flashdata('notification', array('error'=>1,'message'=>'You are successfully logout!!'));
		$user_id = $this->session->userdata('userID');
		if(!empty($user_id)){  
		    $userdata = array(
			'userID'    =>'',
			'user_name'    =>'',
			'user_email' =>'',
			'support_id'=>''
			);			
			$this->session->set_userdata($userdata);
		}
		redirect('/','refresh');	
	}

	public function valid_old_password() { 
		$passOk=getLoginUserPassword();	
        if(!$passOk){
	        $this->form_validation->set_message('valid_old_password', 'Invalid old password');
	        return FALSE;
	    } else {
	    	return TRUE;
	    }
	}

	public function password_matches() {
		$userpass=$this->input->post('userpass');
		$userconfpass=$this->input->post('userconfpass');
        if ($userpass!=$userconfpass){
	        $this->form_validation->set_message('password_matches', 'The confirmation password does not match with password.');
	        return FALSE;
	    } else {
	    	return TRUE;
	    }
	}

	
	public function register(){
						
		$post_data=$this->input->post();
		//print_r($post_data);
		//die();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');
			$this->form_validation->set_rules('phonecode', 'phone number', 'trim|required|is_unique[users.mobile]',array('is_unique' => 'Applicant mobile number already exist.'));
			$this->form_validation->set_rules('useremail', 'email ID', 'required|valid_email|is_unique[users.email]',array('is_unique' => 'Applicant email ID already exist.'));
			$this->form_validation->set_rules('userpass', 'password', 'required');
			$this->form_validation->set_rules('userconfpass', 'confirm password', "trim|required|matches[userpass]");
					
			if($this->form_validation->run() != FALSE) {
				echo validation_errors();
			} else {				
				$dataArr['role'] = $role = $this->input->post('role');
				$dataArr['username'] = $this->input->post('name');
				if(!empty($role) && $role==5)
				{
					$dataArr['companyname'] = $this->input->post('companyname');
				}				
				$dataArr['name'] = $this->input->post('name');
				$dataArr['email'] = $this->input->post('useremail');
				$dataArr['mobile'] = $this->input->post('mobbox');
				$dataArr['created'] = date('Y-m-d h:i:sa');
				$dataArr['password'] = md5($this->input->post('userpass'));
				$dataArr['status'] = '2';
				
				$insert_id= $this->master_model->setInsertData($this->users, $dataArr);
				if($insert_id>0) {
					$this->send_verification_email($insert_id);
					echo $msg = '<div class="alert alert-success">You have registered successfully , please check your registered email id to verify account</div>';
			    } else  {
			    	echo $msg = '<div class="alert alert-danger">Invalid user details</div>';
			    }
			}
		}
	}

    public function send_verification_email($userID) { 
		$userdata = $this->master_model->get_recordArr($this->users,array('id'=>$userID));

		$mobile_number = !empty($userdata['mobile'])?$userdata['mobile']:'';
		if(!empty($mobile_number))
		{
			$smsMsg = 'Dear '.$userdata['username'].', ';
        	$smsMsg.= 'Your registration has successfully completed. you can verify your email and start booking services and products after login.';
        	send_sms($mobile_number,$smsMsg);
		}

	    $verification_link=base_url("login/verify_email/".rtrim(base64_encode($userID),"="));
	    $email=$userdata['email'];                   
		$email_template  = 'verify.html';
		$templateTags =  array(
			'{{site_logo}}' => getEmailLogo(),
			'{{site_name}}' => getSiteTitle(),			
			'{{site_url}}'=> base_url(),
			'{{user_name}}'=> $userdata['username'],
			'{{verification_link}}'=>$verification_link,
			'{{signature}}' => getEmailSignature(),
			'{{disclaimer}}' => getEmailDisclaimer()
		);
	    $message = email_compose($email_template,$templateTags);
        send_email($email,'Verification Email',$message);
	}

	public function send_forgot_email($userID) {
		$userdata = $this->login_model->getDataCollectionByID($this->users,$userID);
	    $email=$userdata['email'];
	    $forgot_link=base_url("login/reset_password/".rtrim(base64_encode($userID),"="));
        $email_template  = 'forgot.html';
		$templateTags =  array(
		'{{site_logo}}' => base_url().'skin/front/images/logo.png',
		'{{site_name}}'=>'firstchoice.com',
		'{{site_url}}'=> base_url(),
		'{{team_name}}'=>'firstchoice',
		'{{user_name}}'=>$userdata['username'],
		'{{year}}'=>date('Y'),
		'{{company_name}}' => 'firstchoice.com',
		'{{company_email}}' => 'info@firstchoice.com',
		'{{forgot_link}}'=>$forgot_link,
		);
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Forgot Password',$message);
	}

	public function reset_password() {

		$id=$this->uri->segment(3);
		$post_data 	= $this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
	        $this->form_validation->set_rules('confirm_password', 'Confirm Password ', 'trim|required|matches[password]');
			if($this->form_validation->run() == FALSE) {
			   
			}
			else
			{
			   $data = array('password' => md5($this->input->post('password')));
			   $insert_id=$this->login_model->setUpdateData($this->users,$data,base64_decode($id)); 
			   if($insert_id>0)
			   {
                   $this->session->set_flashdata('message','Your password has been reset successfully.');
                   //redirect('login/reset_password','refresh');
               }
            }   
	    }
			
		$data['title'] = $this->lang->line('RESET_PASSWORD'); // Capitalize the first letter
		$this->load->view('front/include/header-inner', $data);
	   	$this->load->view('front/include/inner-banner', $data);
	   	$this->load->view('front/include/container_start', $data);
		//$this->load->view('front/include/my_account_sidebar', $data);
		$this->load->view('front/users/reset_password', $data);
		$this->load->view('front/include/container_end', $data);
		$this->load->view('front/include/footer', $data);
		
	}

	function verify_email(){
		
		$id=$this->uri->segment(3);
		$userID=base64_decode($id);
		if(!empty($userID)){
	        $result = $this->login_model->getDataCollectionByID($this->users,$userID);
			$data['status'] = 1;
			$this->login_model->setUpdateData($this->users,$data,$userID);
			$this->session->set_flashdata('message','Your account has been verified please login to continue');
	    }

	    $data['title'] = $this->lang->line('RESET_PASSWORD'); 
		$this->load->view('front/include/header', $data);
		$this->load->view('front/home', $data);
		$this->load->view('front/include/modals', $data);
		$this->load->view('front/include/footer', $data);
	}

	public function myaccount() {
		CheckUserLoginSession();
        $data['page_title']= $this->lang->line('MY_ACCOUNT');
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/myaccount', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
	}

	public function complaint()
    {
        CheckUserLoginSession();

        $post_data=$this->input->post();
        $user_id = $this->session->userdata('userID');
        //print_r($post_data); die();
        if(!empty($post_data))
        {        
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('service_order', 'service order refrence', 'trim|required');
            $this->form_validation->set_rules('complaint_subject', 'complaint subject', 'trim|required');
            $this->form_validation->set_rules('complaint_concern', 'concern/comment', 'trim|required');
                    
            if($this->form_validation->run() != FALSE) 
            {

            	$userdata = $this->login_model->getDataCollectionByID('users',$user_id);
            	$username = !empty($userdata['username'])?$userdata['username']:$userdata['fname'].' '.$userdata['lname'];
            	$email=$userdata['email'];

            	/*================Start : Complaint notification to customer==============*/
				$email_template_customer  = 'complaint-notification.html';
				$templateTags_customer =  array(
				'{{site_logo}}' => base_url().'assets/front/images/logo.png',
				'{{site_name}}'=>'firstchoice.com',
				'{{site_url}}'=> base_url(),
				'{{team_name}}'=>'firstchoice',
				'{{user_name}}'=>$username,
				'{{year}}'=>date('Y'),
				'{{company_name}}' => 'firstchoice.com',
				'{{company_email}}' => 'info@firstchoice.com',
				);
			    $message = email_compose($email_template_customer,$templateTags_customer);
		        send_email($email,'Complaint Notification',$message);
		        /*================End : Complaint notification to customer==============*/

		        /*=====Start : Complaint notification to customer service executive & admin=====*/
		        $email_template  = 'new-complaint.html';
				$templateTags =  array(
				'{{site_logo}}' => base_url().'assets/front/images/logo.png',
				'{{site_name}}'=>'firstchoice.com',
				'{{site_url}}'=> base_url(),
				'{{team_name}}'=>'firstchoice',
				'{{user_name}}'=>$username,
				'{{user_email}}'=>$email,
				'{{user_contact}}'=>!empty($userdata['mobile'])?$userdata['mobile']:'',
				'{{order_id}}'=>!empty($post_data['service_order'])?$post_data['service_order']:'--N/A--',
				'{{complain_subject}}'=>!empty($post_data['complaint_subject'])?$post_data['complaint_subject']:'',
				'{{complain_concern}}'=>!empty($post_data['complaint_concern'])?$post_data['complaint_concern']:'',
				'{{year}}'=>date('Y'),
				'{{company_name}}' => 'firstchoice.com',
				'{{company_email}}' => 'info@firstchoice.com',
				);
			    $message2 = email_compose($email_template,$templateTags);
			    $support_email = 'support@markupdesigns.info';
			    $admin_email = getAdministratorEmail();
		        send_email($support_email,'New Complain Request',$message2,$email,$username);
		        send_email($admin_email,'New Complain Request',$message2,$email,$username);
		        /*=====End : Complaint notification to customer service executive & admin=====*/

               $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your complaint has submitted successfully.'));
                redirect('complaint','refresh');
            }
        }

        $data['page_title'] = $this->lang->line('COMPLAINT');
        $whereArr = array('user_id'=>$user_id,'status'=>5);
        $orders=$this->front_model->getDataRecords($this->service_orders,$whereArr);

        $service_orders = array(''=>$this->lang->line('CHOOSE_SERVICE_ORDER_ID'));
        if(!empty($orders))
        {
            foreach ($orders as $ord)
            {
                $service_orders[$ord->id] = $ord->id; //array_push($service_orders, $ord->id);
            }            
        }  
        $data['service_orders'] = $service_orders;
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/complaint', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

    public function feedback()
    {
        CheckUserLoginSession();
        $rawId=$this->uri->segment(2);
		$id=base64_decode($rawId);

		if(empty($id))
		{
			redirect('/','refresh');
		}
        $post_data=$this->input->post();
        $user_id = $this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);

        $vendor_id = !empty($bookingData->vendor_id)?$bookingData->vendor_id:0;
        $service_id = !empty($bookingData->services)?$bookingData->services:0;
        
        if(!empty($post_data))
        {
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('customer_feedback', 'feedback/comment', 'trim|required');
                    
            if($this->form_validation->run() != FALSE) 
            {
            	$username = getCustomerName($user_id);
            	$email = getCustomerEmail($user_id);
            	$contact = getCustomerContact($user_id);

            	$vendor_email = getCustomerEmail($vendor_id);

            	$templateTags =  array(
				'{{site_logo}}' => base_url().'assets/front/images/logo.png',
				'{{site_name}}'=>'firstchoice.com',
				'{{site_url}}'=> base_url(),
				'{{team_name}}'=>'firstchoice',
				'{{user_name}}'=>$username,
				'{{booking_id}}'=>$id,
				'{{service_name}}'=>getServicesName($service_id),
				'{{feedback}}'=>!empty($post_data['customer_feedback'])?$post_data['customer_feedback']:'',
				'{{year}}'=>date('Y'),
				'{{company_name}}' => 'firstchoice.com',
				'{{company_email}}' => 'info@firstchoice.com',
				);

            	/*================Start : Feedback notification to customer==============*/			
				$email_template_customer  = 'feedback-notification.html';
			    $message_customer = email_compose($email_template_customer,$templateTags);
		        send_email($email,'Feedback Notification',$message_customer);
		        /*================End : Feedback notification to customer==============*/

		        /*=====Start : Feedback notification to service provider & admin=====*/
		        $email_template  = 'customer-feedback.html';
			    $message = email_compose($email_template,$templateTags);
			    $admin_email = getAdministratorEmail();
		        send_email($vendor_email,'Customer Feedback On #'.$id,$message,$email,$username);
		        send_email($admin_email,'Customer Feedback On #'.$id,$message,$email,$username);
		        /*=====End : Feedback notification to service provider & admin=====*/

               $this->session->set_flashdata('notification',array('error'=>0,'message'=>'Your feedback has submitted successfully.'));
                redirect('my-bookings','refresh');
            }
        }

        $data['page_title'] = $this->lang->line('CUSTOMER_FEEDBACK').' (Service Booking Id: #'.$id.')';
        $whereArr = array('user_id'=>$user_id,'status'=>5);
        $orders=$this->front_model->getDataRecords($this->service_orders,$whereArr);

        $service_orders = array(''=>$this->lang->line('CHOOSE_SERVICE_ORDER_ID'));
        if(!empty($orders))
        {
            foreach ($orders as $ord)
            {
                $service_orders[$ord->id] = $ord->id; //array_push($service_orders, $ord->id);
            }            
        }  
        $data['service_orders'] = $service_orders;
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/feedback', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

	public function change_password() {
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('CHANGE_PASSWORD');
        $id=$this->session->userdata('userID');
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('old_password', 'old password', 'trim|required|callback_valid_old_password');
			$this->form_validation->set_rules('userpass', 'new password', 'required');	
			$this->form_validation->set_rules('userconfpass', 'confirm password', "trim|required|callback_password_matches");							
			if($this->form_validation->run() == FALSE) {
				//echo validation_errors();
			} else {				
				$dataArr['password']        = md5($this->input->post('userpass'));							
				$insert_id= $this->login_model->setUpdateData($this->users, $dataArr,$id);
				if($insert_id>0) {					
					$this->session->set_flashdata('message','Your password has been update successfully');
		        	redirect('my-account','refresh');
			    } 
			}
		}
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/change-password', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
	}

	public function orders() {
        CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('ORDERS');
        $user_id = $this->session->userdata('userID');

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('user_id'=>$user_id);
        $total_records = get_total_records($this->products_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->products_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/orders', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function orders_details()
	{
        CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('ORDERS');
        $user_id = $this->session->userdata('userID');

        $rawId = $this->uri->segment(2);
        $oid = base64_decode($rawId);
        $data['orderDetails'] = $orderDetails = get_record($this->products_orders,array('id'=>$oid));
        $data['orderItems'] = $orderItems = get_records('products_orders_items',array('order_id'=>$oid));

        //print_r($orderDetails);
        //print_r($orderItems);

		$this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/orders-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}  

	public function provider_service_request2() {
        CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('SP_REQUEST');
        $user_id = $this->session->userdata('userID');
        $data['servicesData']=$this->front_model->getServiesProviderRequestByUserId($user_id);
		$this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/service-request', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function provider_service_request()
	{
        CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('SP_REQUEST');
        $user_id = $this->session->userdata('userID');
        $data['servicesData']=$this->front_model->getServiesProviderRequestByUserId($user_id);
		$this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/service-request', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	} 

	public function serviceRequestDetails()
	{
        CheckUserLoginSession();
        echo $id=base64_decode($this->uri->segment(2));
        $data['page_title'] = $this->lang->line('SERVICE_REQUEST_DETAILS');
        $data['servicesData']=$this->front_model->getServiesProviderServiceDetailsById($id);       

        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/service-request-detail', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function register_new_service() {
		CheckUserLoginSession();
        $id=$this->session->userdata('userID');
        $post_data=$this->input->post();		
        if(!empty($post_data))
        { 
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');            
            $this->form_validation->set_rules('base_service_id', 'Service provider informtion', 'required');
            $this->form_validation->set_rules('services_type', 'service category', 'required');
            $this->form_validation->set_rules('service_zip_code', 'service zipcode', 'required');
            if($this->form_validation->run() != FALSE)
            {   
            	//echo "<pre>"; print_r($post_data); echo "</pre>"; die;
                $data['services_request_id'] = $this->input->post('base_service_id');
                $data['service_id'] 	=$this->input->post('services_type');
                $data['service_category_code'] 	=$this->input->post('base_service_id');
                $data['service_zip_code'] 	=$this->input->post('service_zip_code');
               	$insertId=$this->services_model->setInsertData($this->service_provider_settings, $data);
                if($insertId>0)
                {
                	$this->send_notificationemail_on_service_request($insertId);
                	$this->send_adminemail_on_service_request($insertId);
		        	$this->session->set_flashdata('message','Your service request has been submitted successfully');
		        	redirect('service-request','refresh');
		        }              
            }
        }
        $data['page_title'] = 'Request new service';
        $data['servicestypeOptions']=getServicesOptions('Select Sevices');
        $data['currencyOption'] = getCurrencyOptions('currencies', 'Select Currency');
        $data['servicesData']=$this->services_model->getServiesProviderRequestDetailsByUserId($id);

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/new_service_request', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function send_notificationemail_on_service_request($service_req_id)
	{
		$service_req_detail = $this->front_model->getVendorServiceDetailsById($service_req_id);
	    $email=$service_req_detail->user_email;
		$email_template  = 'new_service_request.html';
		$templateTags =  array(
		'{{site_logo}}' => base_url().'assets/front/images/logo.png',
		'{{site_name}}'=>'firstchoice.com',
		'{{site_url}}'=> base_url(),
		'{{team_name}}'=>'firstchoice',
		'{{user_name}}'=>$service_req_detail->username,
		'{{year}}'=>date('Y'),
		'{{company_name}}' => 'firstchoice.com',
		'{{company_email}}' => 'info@firstchoice.com'
		);
	    $message = email_compose($email_template,$templateTags);
        send_email($email,'Service request notification',$message);
	}

	public function send_adminemail_on_service_request($service_req_id)
	{
		$service_req_detail = $this->front_model->getVendorServiceDetailsById($service_req_id);
	    $email = getAdministratorEmail();         
		$email_template  = 'new_service_request_admin.html';
		$templateTags =  array(
		'{{site_logo}}' => base_url().'assets/front/images/logo.png',
		'{{site_name}}'=>'firstchoice.com',
		'{{site_url}}'=> base_url(),
		'{{team_name}}'=>'firstchoice',
		'{{user_name}}'=>$service_req_detail->username,
		'{{service_name}}'=>getServicesName($service_req_detail->service_id),
		'{{zipcode}}'=>$service_req_detail->service_zip_code,
		'{{bussiness}}'=>$service_req_detail->business_name,
		'{{bussiness_contact}}'=>$service_req_detail->business_mobile.' / '.$service_req_detail->business_email,
		'{{year}}'=>date('Y'),
		'{{company_name}}' => 'firstchoice.com',
		'{{company_email}}' => 'info@firstchoice.com'
		);
	    $message = email_compose($email_template,$templateTags);
        send_email($email,'New service request notification',$message);
	}

	public function ContractForm102() {
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('SP_INFO');
        $id=$this->session->userdata('userID');
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('fname', 'first name', 'trim|required');
			$this->form_validation->set_rules('lname', 'last name', 'required');	
			$this->form_validation->set_rules('gender', 'gender', "trim|required");							
			if($this->form_validation->run() != FALSE)
			{	
				
				$userdata['username']=$this->input->post('fname').' '.$this->input->post('lname');
	            $userdata['fname']=$this->input->post('fname');
	            $userdata['lname']=$this->input->post('lname');
	            $userdata['gender']=$this->input->post('gender');
	            $userdata['dob']=$this->input->post('dob');
	            $userdata['mobile']=$this->input->post('mobbox');

	            $userdata['site_location']=$this->input->post('site_location');
	            $userdata['aptno']=$this->input->post('aptno');
	            $userdata['street_number']=$this->input->post('street_number');
	            $userdata['street_name']=$this->input->post('street_name');
	            $userdata['street_type']=$this->input->post('street_type');
	            $userdata['direction']=$this->input->post('direction');
	            $userdata['country']=$this->input->post('country');
	            $userdata['state']=$this->input->post('state');
	            $userdata['city']=$this->input->post('city');
	            $userdata['postal_code']=$this->input->post('postal_code');
	            $userdata['telephone']=$this->input->post('telephonebox');
	            $userdata['latitude']=$this->input->post('latitude');
	            $userdata['longitude']=$this->input->post('longitude');
	            $update_id = $this->front_model->setUpdateData($this->users, $userdata, $id);

	            $dataCArea['zipcode']   = $this->input->post('postal_code');
                $dataCArea['latitude']  = $this->input->post('latitude');
                $dataCArea['longitude'] = $this->input->post('longitude');
                $dataCArea['country']   = $this->input->post('country');
                $dataCArea['state']     = $this->input->post('state');
                $dataCArea['city']      = $this->input->post('city');
                $dataCArea['address']   = $this->input->post('site_location');
                if(!empty($dataCArea['zipcode']))
                {
                	$this->insertZipcodeWithRelatedInfo($dataCArea);
                }

				$dataArr['business'] = $this->input->post('business');
				$dataArr['business_name'] = $this->input->post('business_name');							
				$dataArr['business_type'] = $this->input->post('business_type');								
				$dataArr['license'] = $this->input->post('license');							
				$dataArr['license_details'] = $this->input->post('license_details');						
				$dataArr['business_aptno'] = $this->input->post('business_aptno');							
				$dataArr['business_street_no'] = $this->input->post('business_street_no');					
				$dataArr['business_street_name'] = $this->input->post('business_street_name');			
				$dataArr['business_street_type'] = $this->input->post('business_street_type');			
				$dataArr['business_country'] = $this->input->post('business_country');			
				$dataArr['business_state'] = $this->input->post('business_state');			
				$dataArr['business_city'] = $this->input->post('business_city');			
				$dataArr['business_type'] = $this->input->post('business_type');			
				$dataArr['business_direction'] = $this->input->post('business_direction');			
				$dataArr['business_zip'] = $this->input->post('business_zip');			
				$dataArr['business_mobile'] = $this->input->post('bussmobbox');			
				$dataArr['business_fax'] = $this->input->post('business_fax');			
				$dataArr['business_email'] = $this->input->post('business_email');			
				$dataArr['driver_licence'] = $this->input->post('driver_licence');
                $dataArr['car_made'] = $this->input->post('made');
                $dataArr['car_model'] = $this->input->post('model');
                $dataArr['car_year'] = $this->input->post('year');
                $dataArr['car_color'] = $this->input->post('color');
                $dataArr['car_number'] = $this->input->post('licence_plate_no');

				$service_id = $this->input->post('service_id');

				$bussiness_update_id= $this->login_model->setUpdateData($this->service_provider_request, $dataArr,$service_id);

				$settings['start_date'] = $this->input->post('start_date');
				$settings['bank_name'] = $this->input->post('bank_name');
				$settings['bank_address'] = $this->input->post('bank_address');
				$settings['bank_code'] = $this->input->post('bank_code');
				$settings['account_no'] = $this->input->post('account_no');
				$settings['swift_no'] = $this->input->post('swift_no');
				$settings['transition_no'] = $this->input->post('transition_no');
				$settings['account_type'] = $this->input->post('account_type');
                $settings_insert = $this->front_model->setUpdateDataByWhere($this->service_provider_settings, $settings,array('services_request_id'=>$service_id)); 

				if(!empty($_FILES["featured_img"]["name"]))
                {
                     $featured_img = do_upload('users','featured_img');
                     $data_featured_img = array('featured_img' => $featured_img );
                     $this->login_model->setUpdateData($this->users,$data_featured_img,$id);
                }

				if(!empty($_FILES["picture"]["name"]))
                {
                     $picture_img = do_upload('cars','picture');
                     $data_picture_img = array('car_picture' => $picture_img);
                     $this->login_model->setUpdateData($this->service_provider_request,$data_picture_img,$service_id);
                }

				$zipcode = $this->input->post('business_zip');
                $address = getAddressByZipcode($zipcode);
                if(!empty($address['zipcode']))
                {
                    $this->insertZipcodeWithRelatedInfo($address);
                }

				$this->session->set_flashdata('message','Your details has been update successfully');
			    redirect('contract-form-102','refresh');
			}
		}
        $data['userDetails']=$this->front_model->getUserDetails($id);
        $data['businessDetails']=$this->front_model->getServiesProviderBusinessDetailsByUserId($id);
        //print_r($data['businessDetails']);
        //$js=array('become-a-professtional.js');
        //$this->config->set_item('headJs',$js);
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/contract-form-102', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	} 

	public function ContractForm103() {
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('SP_INFO');
        $id=$this->session->userdata('userID');
        $data['userDetails']=$this->front_model->getUserDetails($id);
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/contract-form-103', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	} 

	public function mybookings() {
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('MY_BOOKING');
        $id=$this->session->userdata('userID');

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('user_id'=>$id);
        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["bookingList"] = get_current_page_records($this->service_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/mybookings', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function myInvoices()
	{
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('MY_INVOICE');
        $user_id = $this->session->userdata('userID');


        $whereArr = array();

        if(!empty($user_id))
        {
            $whereArr['user_id'] = $user_id;
        }

        /*if(!empty($getdata['invoiceId']))
        {
            $whereArr['id'] = $data['invoiceId'] = $getdata['invoiceId'];
        }*/

        $data['records'] = $records = get_records($this->invoice,$whereArr);




        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $total_records = get_total_records($this->invoice,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->invoice,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/invoices', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

    public function download_invoice()
    {
        CheckUserLoginSession();
        $id=$this->uri->segment(2);
        if(!empty($id))
        {
            $filename = getInvoiceFile($id);
            $base_path = $this->config->item('base_path');
            $filePath = $base_path.'upload/invoices/'.$filename;
            force_download($filePath,NULL);
        }
    }

    public function customer_invoice()
    {
    	$this->load->library('pdf');
    	CheckUserLoginSession();
    	$user_id=$this->session->userdata('userID');

        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        
        
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);

        //echo "<pre>"; print_r($bookingData); echo "</pre>"; 
        //echo "<pre>"; print_r($operation_row); echo "</pre>"; 

        $data['oid'] = $id;
        $data['service_datetime'] = $service_datetime = !empty($bookingData->booking_datetime)?$bookingData->booking_datetime:'';
        $default_curr_id = getCurrency();
        $data['currency_code'] = $currency_code = !empty($bookingData->code)?$bookingData->code:getCurrencyCode($default_curr_id);

        $customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
        $address_id = !empty($bookingData->address_id)?$bookingData->address_id:0;
        $vendor_id = !empty($bookingData->vendor_id)?$bookingData->vendor_id:0;

        $address = getAddressDetails($address_id);

        $data['name'] = $name = getCustomerName($customer_id);
        $data['email'] = $email = getCustomerEmail($customer_id);
        $data['contact'] = $contact = getCustomerContact($customer_id);
        $data['address'] = $site_location = !empty($address->site_location)?$address->site_location:'';

        $data['service_prepaid_charges'] = $service_prepaid_charges = !empty($bookingData->amount)?$bookingData->amount:0;
        $data['service_prepaid_coupon'] = $service_prepaid_coupon = !empty($bookingData->coupon)?$bookingData->coupon:'';
        $data['service_prepaid_discount'] = $service_prepaid_discount = !empty($bookingData->discount)?$bookingData->discount:0;
        $data['service_prepaid_tax'] = $service_prepaid_tax = !empty($bookingData->tax_name)?$bookingData->tax_name:'';
        $data['service_prepaid_tax_amount'] = $service_prepaid_tax_amount = !empty($bookingData->tax_amount)?$bookingData->tax_amount:'';
        $data['service_prepaid_sub_amount'] = $service_prepaid_sub_amount = !empty($bookingData->total_amount)?$bookingData->total_amount:'';


        $data['service_postpaid_charges'] = $service_postpaid_charges = !empty($operation_row['additional_hrs_charges'])?$operation_row['additional_hrs_charges']:0;
        $data['service_postpaid_discount'] = $service_postpaid_discount = 0;
        $data['service_postpaid_tax_amount'] = $service_postpaid_tax_amount = !empty($operation_row['additional_amt_tax'])?$operation_row['additional_amt_tax']:0;
        $data['service_postpaid_sub_amount'] = $service_postpaid_sub_amount = !empty($operation_row['additional_subtotal'])?$operation_row['additional_subtotal']:0;


        $data['grand_total'] = $grand_total = $service_prepaid_sub_amount + $service_postpaid_sub_amount;

        $filename = 'fce-customer-invoice-'.$user_id.'-'.time().'.pdf';

        $this->pdf->download_pdf('front/users/customer-invoice', $data,$filename);
        //$this->load->view('front/users/customer-invoice', $data);
    }

	public function service_orders()
	{
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('ORDERS');
        $id=$this->session->userdata('userID');
        $oid = $this->input->get('oid');
        $email = $this->input->get('email');
        $phone = $this->input->get('phone');
        $data['oid'] = $oid;
        $data['email'] = $email;
        $data['phone'] = $phone;

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        
        $whereSArr = array(); //array('status'=>5);
        $user_id = 0;
        if(!empty($email) && !empty($phone))
        {
        	$whereSArr['email'] = $email;
        	$whereSArr['mobile'] = $phone;
        	$row = get_record($this->users,$whereSArr);
        	$user_id = !empty($row->id)?$row->id:0;
        }
        elseif(!empty($email))
        {
        	$whereSArr['email'] = $email;
        	$row = get_record($this->users,$whereSArr);
        	$user_id = !empty($row->id)?$row->id:0;
        }
        elseif(!empty($phone))
        {
        	$whereSArr['mobile'] = $phone;
        	$row = get_record($this->users,$whereSArr);
        	$user_id = !empty($row->id)?$row->id:0;
        }

        $whereArr = array(); //array('status'=>5);
        if(!empty($user_id))
        {
        	$whereArr['user_id'] = $user_id;
        }
        if(!empty($oid))
        {
        	$whereArr['id'] = $oid;
        }
        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["bookingList"] = get_current_page_records($this->service_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/service-orders', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function service_order_details()
    {
        CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
        $data['is_sent_otp'] = !empty($bookingData->otp)?1:0;
        $data['bookingData']= $bookingData;
        $data['operation_row']= $operation_row;
        $data['rawId'] = $rawId;
        $data['is_other_accepted'] = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $data['request_desicion'] = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);
        $data['is_otp_verified'] = $this->front_model->isOTPVerified($this->service_operations, $id);
        $data['is_service_started'] = $this->front_model->isServiceStarted($this->service_operations, $id);
        $data['is_service_ended'] = $this->front_model->isServiceEnded($this->service_operations, $id);        
        $data['service_start_time'] = $start_time = $operation_row['service_start_time'];
        $data['service_end_time'] = $end_time = $operation_row['service_end_time'];

        if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
        {
        	$allocated_time = getAllocatedTime($id);
	        $data['service_allocated'] = getDuration($allocated_time,'minute');
	        $data['time_spent'] = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
	        $data['additional_time'] = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

	        $additional_time = calculateAdditionalTime($id,$start_time,$end_time);
	        $additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;        	
        	if(!empty($additional_operator))
        	{
        		$additional_amount = calculateAdditionalAmount4AO($id,$start_time,$end_time);
        	}
        	else
        	{
        		$additional_amount = calculateAdditionalAmount($id,$start_time,$end_time);
        	}
        	$data['additional_amount'] = $additional_amount;
	        $data['additional_tax'] = $additional_tax = calculateTax($id,$additional_amount);
	        $data['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        }

        $data['code']=$this->uri->segment(2);
        $data['page_title'] = $this->lang->line('SERVICE_BOOKING_DETAILS');
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/service-order-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function service_provider_details()
    {    	
    	CheckUserLoginSession();
    	$rawId=$this->uri->segment(2);
    	$id = base64_decode($rawId);
    	$data['code'] = $rawId;
    	$data['backcode'] = $rawId;
		$data['userDetails']=$this->front_model->getUserDetails($id);
		$data['businessDetails']=$this->front_model->getServiesProviderBusinessDetailsByUserId($id);
		$data['businessSettings']=getServiceProviderSettings($data['businessDetails']->id);    	
        $data['page_title'] = $this->lang->line('SERVICE_PROVIDER_DETAILS');
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/service-provider-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

	public function customerRequest() {
		CheckUserLoginSession();
		$id=$this->session->userdata('userID');
        $data['page_title'] = $this->lang->line('CUSTOMER_REQUEST');

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('FIND_IN_SET('.$id.', services_provider) !='=>0);
        $whereArr['payment_status !='] = 'NULL';
        $post_data=$this->input->post();
		if(!empty($post_data))
		{
			if(!empty($post_data['service']))
			{
				$whereArr['services'] = $post_data['service'];
			}
			if(!empty($post_data['status']) && $post_data['status']==1)
			{
				$whereArr['status'] = 1;
				$whereArr['vendor_id'] = $id;
			}
			elseif(!empty($post_data['status']) && $post_data['status']==6)
			{
				$whereArr['status'] = 1;
				$whereArr['vendor_id !='] = $id;
			}
			elseif(!empty($post_data['status']))
			{
				$whereArr['status'] = $post_data['status'];
			}
			elseif($post_data['status']==0 && $post_data['status']!='')
			{
				$whereArr['status'] = 0;
			}

			if(!empty($post_data['leads']))
			{
				if(!empty($post_data['leads']) && $post_data['leads']=='mine')
				{
					$whereArr['vendor_id'] = $id;
				}
				elseif(!empty($post_data['leads']) && $post_data['leads']=='other')
				{
					$whereArr['vendor_id !='] = $id;
				}				
			}

			if(!empty($post_data['days']))
			{
				if(!empty($post_data['days']) && $post_data['days']=='today')
				{
					$whereArr['booking_datetime BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 1 day) !='] = 0;
				}
				elseif(!empty($post_data['days']) && $post_data['days']=='tomorrow')
				{
					$whereArr['booking_datetime BETWEEN DATE_ADD(CURDATE(), INTERVAL 1 day) AND DATE_ADD(CURDATE(), INTERVAL 2 day) !='] = 0;
				}				
			}						
		}
        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["bookingList"] = get_current_page_records($this->service_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/customer-request', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function videos() {
		CheckUserLoginSession();
		$user_id=$this->session->userdata('userID');
        $data['page_title'] = $this->lang->line('VIDEOS');

        $userdata = getServiceProviderDetailsById($user_id);
       	$service_id = !empty($userdata->services_type)?$userdata->services_type:0;

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;        
        $whereArr['service_id'] = $service_id;
        $whereArr['status'] = 1;
        $total_records = get_total_records($this->videos,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->videos,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/videos', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function videos_play() {
		CheckUserLoginSession();
		$user_id=$this->session->userdata('userID');
		$video_id = base64_decode($this->uri->segment(2));

        $data['page_title'] = $this->lang->line('VIDEOS');

        $userdata = getServiceProviderDetailsById($user_id);
       	$service_id = !empty($userdata->services_type)?$userdata->services_type:0;

       	$data['record'] = get_record($this->videos, array('id'=>$video_id));

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/video-play', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	

	public function export_leads()
    {
    	CheckUserLoginSession();
		$id=$this->session->userdata('userID');		
        $whereArr = array('vendor_id'=>$id);
        $records = get_records($this->service_orders,$whereArr,'id','ASC');
        //echo "<pre>"; print_r($records); echo "</pre>";
        if(!empty($records))
        {
        	$data = array();
        	foreach ($records as $row)
        	{
        		$rowdata = array();
        		$rowdata['Booing ID'] = $row->id;
        		$rowdata['Service Name'] = getServicesName($row->services);
        		$rowdata['Service Date'] = $row->booking_datetime;
        		$rowdata['Paid Amount'] = $row->amount.' '.$row->code;
        		$rowdata['Customer Name'] = getCustomerName($row->user_id);
        		$rowdata['Customer Email'] = getCustomerEmail($row->user_id);
        		$rowdata['Customer Contact'] = getCustomerContact($row->user_id);
        		$address = getAddressDetails($row->address_id);
        		$rowdata['Service Address'] = $address->site_location;
        		$rowdata['Operator Name'] = getCustomerName($row->operator_id);
        		$rowdata['Operator Email'] = getCustomerEmail($row->operator_id);
        		$rowdata['Operator Contact'] = getCustomerContact($row->operator_id);
        		$rowdata['Status'] = getServiceBookingStatusLabel($row->status);
        		$rowdata['Payment Method'] = $row->method;
        		$rowdata['Payment Status'] = $row->payment_status;
        		array_push($data, $rowdata);
        	}
        }
        else
        {
        	redirect('customer-request','refresh');
        }
        //echo "<pre>"; print_r($data); echo "</pre>";
        $filename = 'SP_'.$id.'_'.time();
        exportEtpCsv($data,$filename);
    }

	public function operations() {
		CheckUserLoginSession();
		$id=$this->session->userdata('userID');
        $data['page_title']='Operations';

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('FIND_IN_SET('.$id.', services_provider) !='=>0,'vendor_id'=>$id);
        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->service_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/operations', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function customerRequestDetails()
    {
        CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
        $data['bookingData'] = $bookingData;
        $data['operation_row']= $operation_row;
        $data['rawId'] = $rawId;
        $data['is_valid_vendor'] = (!empty($bookingData->vendor_id) && $bookingData->vendor_id==$user_id)?1:0;
        $data['is_other_accepted'] = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $data['request_desicion'] = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);
        $data['is_otp_verified'] = $this->front_model->isOTPVerified($this->service_operations, $id);
        $data['is_service_started'] = $this->front_model->isServiceStarted($this->service_operations, $id);
        $data['is_service_ended'] = $this->front_model->isServiceEnded($this->service_operations, $id); 
        $data['service_start_time'] = $start_time = $operation_row['service_start_time'];
        $data['service_end_time'] = $end_time = $operation_row['service_end_time'];

        if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
        {
        	$allocated_time = getAllocatedTime($id);
	        $data['service_allocated'] = getDuration($allocated_time,'minute');
	        $data['time_spent'] = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
	        $data['additional_time'] = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

	        $additional_time = calculateAdditionalTime($id,$start_time,$end_time);
	        $additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;        	
        	if(!empty($additional_operator))
        	{
        		$additional_amount = calculateAdditionalAmount4AO($id,$start_time,$end_time);
        	}
        	else
        	{
        		$additional_amount = calculateAdditionalAmount($id,$start_time,$end_time);
        	}
        	$data['additional_amount'] = $additional_amount;
	        $data['additional_tax'] = $additional_tax = calculateTax($id,$additional_amount);
	        $data['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        }

        $userData = getCustomerDetailsById($user_id);
        if(!empty($userData->role) && $userData->role==5)
        {
            $data['operators']=$this->front_model->getDataCollectionsByColumn($this->users,'parent_id',$user_id);
        }
        $data['code']=$this->uri->segment(2);
        $data['page_title'] = $this->lang->line('BOOKING_DETAILS');
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/customer-request-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }    

    public function send_operations_otp()
    {
    	CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $order_data = $this->front_model->getServiceBookingDetails($this->service_orders, $id);

        $customer_detail = getCustomerDetailsById($order_data->user_id);
        $useremail = !empty($customer_detail->email)?$customer_detail->email:'';

        if(!empty($useremail))
        { 
        	$otp = rand(111111,999999);        
            if($this->front_model->setUpdateData($this->service_orders,array('otp'=>$otp),$id))
            {
            	$customer_name = getCustomerName(!empty($order_data->user_id)?$order_data->user_id:0);
            	$customer_email = getCustomerEmail(!empty($order_data->user_id)?$order_data->user_id:0);
            	$customer_phone = getCustomerContact(!empty($order_data->user_id)?$order_data->user_id:0);

            	$operator_name = getCustomerName(!empty($order_data->operator_id)?$order_data->operator_id:0);
            	$operator_phone = getCustomerContact(!empty($order_data->operator_id)?$order_data->operator_id:0);

                $email_template  = 'operation-otp.html';
                $templateTags =  array(
                '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                '{{site_name}}'=>'firstchoice.com',
                '{{site_url}}'=> base_url(),
                '{{team_name}}'=>'firstchoice',
                '{{name}}'=>$customer_name,
                '{{otp}}'=> $otp,
                '{{operator_name}}' => $operator_name,
                '{{operator_contact}}' => $operator_phone,
                '{{year}}'=>date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com',
                );
                $message = email_compose($email_template,$templateTags);
                send_email($customer_email,'Service OTP',$message);

                $smsMsg = 'Dear '.$customer_name.', ';
                $smsMsg.= 'Your Customer verification OTP is '.$otp.'. Please share that OTP to operator.';
                send_sms($customer_phone,$smsMsg);

                $this->session->set_flashdata('message','Service OTP has sent to customer inbox');
		        redirect('job_details/'.$rawId,'refresh');
            }
        }      
    }

	public function jobs()
	{
		CheckUserLoginSession();
		$id=$this->session->userdata('userID');
        $data['page_title'] = $this->lang->line('MY_JOBS');
        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('operator_id'=>$id);
        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
         $data["records"] = get_current_page_records($this->service_orders,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/jobs', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function job_details()
    {
        CheckUserLoginSession();
        $rawId = $this->uri->segment(2);
        $id=base64_decode($rawId);
        $user_id=$this->session->userdata('userID');
        $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
        $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
        
        $data['is_sent_otp'] = !empty($bookingData->otp)?1:0;
        $data['bookingData']= $bookingData;
        $data['operation_row']= $operation_row;
        $data['rawId'] = $rawId;
        $data['is_other_accepted'] = $this->front_model->isAccepetedByAnyone($this->service_leads, $id, $user_id);
        $data['request_desicion'] = $this->front_model->getMydecisionOnRequest($this->service_leads, $id, $user_id);
        $data['is_otp_verified'] = $this->front_model->isOTPVerified($this->service_operations, $id);
        $data['is_service_started'] = $this->front_model->isServiceStarted($this->service_operations, $id);
        $data['is_service_ended'] = $this->front_model->isServiceEnded($this->service_operations, $id);        
        $data['service_start_time'] = $start_time = $operation_row['service_start_time'];
        $data['service_end_time'] = $end_time = $operation_row['service_end_time']; 
        
        if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
        {
        	$allocated_time = getAllocatedTime($id);
	        $data['service_allocated'] = getDuration($allocated_time,'minute');
	        $data['time_spent'] = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
	        $data['additional_time'] = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

	        
        	$additional_time = calculateAdditionalTime($id,$start_time,$end_time);

        	$additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;        	
        	if(!empty($additional_operator))
        	{
        		$additional_amount = calculateAdditionalAmount4AO($id,$start_time,$end_time);
        	}
        	else
        	{
        		$additional_amount = calculateAdditionalAmount($id,$start_time,$end_time);
        	}
        	$data['additional_amount'] = $additional_amount;

        	$data['additional_tax'] = $additional_tax = calculateTax($id,$additional_amount);
        	$data['additional_subtotal'] = $additional_subtotal = $additional_amount + $additional_tax;
        }       

        $data['code']=$this->uri->segment(2);
        $data['page_title'] = $this->lang->line('JOB_DETAILS');
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/job-details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
    }

    public function verify_service_otp()
    {
    	$otp = $this->input->post('otp');
    	$ordid = $this->input->post('ordid');
    	$oid = base64_decode($ordid);
    	$cnt = get_total_records($this->service_orders, array('id'=>$oid,'otp'=>$otp));

    	$bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $oid);

    	if($cnt>0 && !empty($bookingData))
    	{
    		$started_time = date('Y-m-d H:i:s');
			$cnt_verified = get_total_records($this->service_operations, array('service_order_id'=>$oid,'otp_verified'=>1));
			$operationsData = array('otp_verified'=>1,'is_service_started'=>1,'service_start_time'=>$started_time,'service_order_id'=>$oid,'customer_id'=>$bookingData->user_id,'vendor_id'=>$bookingData->vendor_id,'operator_id'=>$bookingData->operator_id,'pre_paid_hours'=>1,'pre_paid_amount'=>$bookingData->amount);

			//print_r($operationsData);
			//echo $cnt_verified; die;

			$customer_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
			$service_id = !empty($bookingData->services)?$bookingData->services:0;
			$customer_name = getCustomerName($customer_id);
            $customer_email = getCustomerEmail($customer_id);
            $customer_phone = getCustomerContact($customer_id);

			if($cnt_verified>0)
			{
				$operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$oid);
				$response_id = $this->front_model->setUpdateData($this->service_operations,$operationsData,$operation_row['id']);
			}
			else
			{
				$response_id = $this->front_model->setInsertData($this->service_operations,$operationsData);
			}

			if(!empty($response_id))
			{
				$started_time_display = date('Y-m-d h:i A',strtotime($started_time));

				$email_template  = 'service-started.html';
                $templateTags =  array(
                '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                '{{site_name}}'=>'firstchoice.com',
                '{{site_url}}'=> base_url(),
                '{{team_name}}'=>'firstchoice',
                '{{user_name}}'=>$customer_name,
                '{{service_name}}'=> getServicesName($service_id),
                '{{booking_id}}' => $oid,
                '{{started_time}}' => $started_time_display,
                '{{year}}'=>date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com',
                );
                $message = email_compose($email_template,$templateTags);                
                send_email($customer_email,'Service Started Notification',$message);

				$smsMsg = 'Dear '.$customer_name.', ';
                $smsMsg.= 'Your service start started on '.$started_time_display.' which service booking ID is #'.$oid.'.';

                send_sms($customer_phone,$smsMsg);

				echo 'success';
			}
    	}
    	else
    	{
    		echo 'invalid';
    	}    	
    }
	
	public function ajax_start_service()
    {
    	$ordid = $this->input->post('ordid');
    	$service_order_id = base64_decode($ordid);
    	$cnt = get_total_records($this->service_operations, array('service_order_id'=>$service_order_id,'is_service_started'=>0));
    	$order_data = $this->front_model->getDataCollectionByField($this->service_orders,'id',$service_order_id);

    	if(!empty($order_data) && $cnt>0)
    	{
    		$operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$service_order_id);
			$operationsData = array('is_service_started'=>1,'service_start_time'=>date('Y-m-d H:i:s'));
			$res_id = $this->front_model->setUpdateData($this->service_operations,$operationsData,$operation_row['id']);
			echo 'success';
		}
		else
		{
			echo 'invalid';
		}		
    }

    public function ajax_end_service()
    {
    	$ordid = $this->input->post('ordid');
    	$service_order_id = base64_decode($ordid);
    	$cnt = get_total_records($this->service_operations, array('service_order_id'=>$service_order_id,'is_service_started'=>1));
    	$order_data = $this->front_model->getDataCollectionByField($this->service_orders,'id',$service_order_id);

    	if(!empty($order_data) && $cnt>0)
    	{
    		$operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$service_order_id);
			$operationsData = array('is_service_ended'=>1,'service_end_time'=>date('Y-m-d H:i:s'));
			$res_id = $this->front_model->setUpdateData($this->service_operations,$operationsData,$operation_row['id']);
			$this->send_job_invoice_to_customer(base64_decode($ordid));
			echo 'success';
		}
		else
		{
			echo 'invalid';
		}		
    }

    public function send_job_invoice_to_customer($id)
	{    
	    $bookingData = $this->front_model->getServiceBookingDetails($this->service_orders, $id);
	    $operation_row = $this->front_model->getDataCollectionByField($this->service_operations,'service_order_id',$id);
	    $currency_code = !empty($bookingData->code)?$bookingData->code:"";
	    $tax = !empty($bookingData->tax)?$bookingData->tax:"";
        $tax_name = !empty($bookingData->tax_name)?$bookingData->tax_name:"";
     
	    $start_time = $operation_row['service_start_time'];
	    $end_time = $operation_row['service_end_time'];
    	$allocated_time = getAllocatedTime($id);
        $time_spent_display = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
        $time_allocated_display = getDuration($allocated_time,'minute');
        $time_additional_display = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;

        $additional_time = calculateAdditionalTime($id,$start_time,$end_time);
        $additional_operator = !empty($bookingData->additional_operator)?$bookingData->additional_operator:0;        	
    	$additional_amount = !empty($additional_operator)?calculateAdditionalAmount4AO($id,$start_time,$end_time):calculateAdditionalAmount($id,$start_time,$end_time);
        $additional_tax = calculateTax($id,$additional_amount);
        $additional_subtotal = $additional_amount + $additional_tax;

	    $user_id = !empty($bookingData->user_id)?$bookingData->user_id:0;
	    $email = getCustomerEmail($user_id);
	    $name = getCustomerName($user_id);
	    $mobile = getCustomerContact($user_id);
	    
	    $addition_charge_info = (!empty($additional_time) && $additional_time>0)?'<p>As above declaration, we have estimation of that additional time in a serprate email as invoice.</p><p>We wish best and feel free for any type of concern and assistance.</p> ':'<p>We wish best and feel free for any type of concern and assistance.</p>';

	    $details_url = base_url('booking-details').'/'.base64_encode($id);
        $templateTags =  array(
        '{{site_logo}}' => base_url().'skin/front/images/logo.png',
        '{{site_name}}'=>'firstchoice.com',
        '{{site_url}}'=> base_url(),
        '{{team_name}}'=>'firstchoice',
        '{{user_name}}'=>$name,
        '{{booking_id}}'=>$id,
        '{{start_time}}'=>date('d M, Y h:i A',strtotime($start_time)),
        '{{end_time}}'=>date('d M, Y h:i A',strtotime($end_time)),
        '{{time_spent}}'=>$time_spent_display,
        '{{time_allocated}}'=>$time_allocated_display,
        '{{additional_estimated}}'=>$addition_charge_info,
        '{{time_additional}}'=>$time_additional_display,        
        '{{additional_amount}}'=>getFormatedPriceByCode($currency_code,$additional_amount),
        '{{tax}}'=>$tax,
        '{{tax_name}}'=>$tax_name,
        '{{additional_tax}}'=>getFormatedPriceByCode($currency_code,$additional_tax),
        '{{additional_subtotal}}'=>getFormatedPriceByCode($currency_code,$additional_subtotal),
        '{{details_url}}'=>$details_url,        
        '{{year}}'=>date('Y'),
        '{{company_name}}' => 'firstchoice.com',
        '{{company_email}}' => 'info@firstchoice.com',
        );
        $email_template  = 'service-completion-notification.html';
        $message = email_compose($email_template,$templateTags);
        send_email($email,'Service Completion notification',$message);

	    if(!empty($additional_time) && $additional_time>0)
	    {
	    	$email_template  = 'service-invoice.html';
	        $message = email_compose($email_template,$templateTags);
	        send_email($email,'Service Invoice',$message);

	        $smsMsg = 'Dear '.$name.', ';
            $smsMsg.= 'Service Booking ID #'.$id.': Service has completed and Invoice has been sent to your registered email.';
            send_sms($mobile,$smsMsg);
	    }
	    else
	    {
            $smsMsg = 'Dear '.$name.', ';
            $smsMsg.= 'Service Booking ID #'.$id.': Service has completed successfully in allotted time in your minimum payment.';
            send_sms($mobile,$smsMsg);
	    }
	    
	}

	public function operators() {
		CheckUserLoginSession();
		$id=$this->session->userdata('userID');
        $data['page_title'] = $this->lang->line('MY_OPERATORS');

        $limit_per_page = $this->config->item('limits_per_page');
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = $page*$limit_per_page;
        $whereArr = array('parent_id'=>$id);
        $total_records = get_total_records($this->users,$whereArr);
        if ($total_records > 0) 
        {
         $data["operators"] = get_current_page_records($this->users,$limit_per_page,$start,$whereArr,'id','DESC');
         $data["pagination"] = Jpagination($total_records, $limit_per_page,$limit_per_page);
        }

		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/operators', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function add_operator() {
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('ADD_OPERATOR');
        $id=$this->session->userdata('userID');
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]',array('is_unique' => 'Applicant email ID already exist.'));
			$this->form_validation->set_rules('mobile', 'mobile', 'trim|required|is_unique[users.mobile]',array('is_unique' => 'Applicant mobile number already exist.'));			
			$this->input->post('full_number');
			if($this->form_validation->run() == FALSE)
			{
				echo validation_errors();
			}
			else
			{
				$useremail = $this->input->post('email');
				$dataArr['parent_id']= $id;
				$dataArr['role']     = 9;
				$dataArr['username'] = $this->input->post('fname').' '.$this->input->post('lname');
				$dataArr['fname']    = $this->input->post('fname');
				$dataArr['lname']    = $this->input->post('lname');
				$dataArr['email']    = $useremail;
				$dataArr['mobile']   = $this->input->post('mobbox');
				$dataArr['gender']   = $this->input->post('gender');
				$dataArr['status']   = 1;
				$insert_id= $this->login_model->setInsertData($this->users, $dataArr);
				if($insert_id>0)
				{
					$this->set_password_email($useremail);
					$this->session->set_flashdata('message','A new operator has been registered successfully');
		        	redirect('operators','refresh');
			    } 
			}
		}

        $data['userDetails']=$this->front_model->getUserDetails($id);
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/add_operator', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function set_password_email($useremail)
    {
        if(!empty($useremail))
        { 
            $userdata = $this->front_model->getDataCollectionByField($this->users,'email',$useremail);
            if(!empty($userdata))
            {
                //print_r($userdata);
                $token = md5(rand(11111,99999));                    
                if($this->front_model->setUpdateData($this->users,array('token'=>$token),$userdata['id']))
                {
                    $resetUrl = base_url('reset-password')."/?email=".$userdata['email']."&token=".$token;
                    $email_template  = 'set-pass.html';
                    $templateTags =  array(
                    '{{site_logo}}' => base_url().'skin/front/images/logo.png',
                    '{{site_name}}'=>'firstchoice.com',
                    '{{site_url}}'=> base_url(),
                    '{{team_name}}'=>'firstchoice',
                    '{{name}}'=>$userdata['username'],
                    '{{email}}'=>$userdata['email'],
                    '{{resetUrl}}'=>$resetUrl,
                    '{{year}}'=>date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com',
                    );
                    $message = email_compose($email_template,$templateTags);
                    send_email($userdata['email'],'Set password link',$message);
                }
            }
        }      
    }

	public function edit_operator()
	{
		CheckUserLoginSession();
        $data['page_title'] = $this->lang->line('EDIT_OPERATOR');
        $encrypted_id=$this->uri->segment(2);
        $id = base64_decode($encrypted_id);
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');
			$this->input->post('full_number');
			if($this->form_validation->run() == FALSE) {
				echo validation_errors();
			} else {				
				$dataArr['username']        = $this->input->post('fname').' '.$this->input->post('lname');
				$dataArr['fname']        = $this->input->post('fname');
				$dataArr['lname']        = $this->input->post('lname');
				$dataArr['email']           = $this->input->post('email');
				$dataArr['mobile']          = $this->input->post('mobbox');				
				$dataArr['site_location']   = $this->input->post('site_location');				
				$dataArr['aptno']           = $this->input->post('aptno');				
				$dataArr['street_number']   = $this->input->post('street_number');				
				$dataArr['street_name']     = $this->input->post('street_name');				
				$dataArr['street_type']     = $this->input->post('street_type');				
				$dataArr['direction']     	= $this->input->post('direction');				
				$dataArr['country']         = $this->input->post('country');				
				$dataArr['state']           = $this->input->post('state');				
				$dataArr['city']            = $this->input->post('city');				
				$dataArr['postal_code']     = $this->input->post('postal_code');				
				$dataArr['dob']             = $this->input->post('dob');
				$dataArr['gender']          = $this->input->post('gender');
				$insert_id= $this->login_model->setUpdateData($this->users, $dataArr,$id);

				if($_FILES["featured_img"]["name"] != "") {
					 $featured_img=$this->do_upload('users','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->login_model->setUpdateData($this->users,$data_featured_img,$id);
				}
				if($insert_id>0) {					
					$this->session->set_flashdata('message','Operator profile has been update successfully');
		        	//redirect('operators','refresh');
			    } 
			}
		}

        $data['userDetails']=$this->front_model->getUserDetails($id);
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/edit_operator', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function delete_operator()
	{
		$encrypted_id=$this->uri->segment(2);
        $id = base64_decode($encrypted_id);
        $this->front_model->setDeleteData($this->users,$id);
        $this->session->set_flashdata('message','Operator profile has been deleted successfully');
		redirect('operators','refresh');
	}

	public function status_operator()
	{
		$encrypted_id=$this->uri->segment(2);
        $id = base64_decode($encrypted_id);
        $userDetail = $this->front_model->getUserDetails($id);

        $dataArr['status'] = empty($userDetail->status)?1:0;				
		$update_id = $this->login_model->setUpdateData($this->users, $dataArr,$id);

        $this->session->set_flashdata('message','Operator status has been changed successfully');
		redirect('operators','refresh');
	}
	
	public function dashboard() {
		CheckUserLoginSession();
        $data[]='';
		$this->load->view('front/include/header', $data);
        $this->load->view('front/users/dashboard', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}  

	public function profile_update() {
		CheckUserLoginSession();

		$userrole = getLoginUserRole();
		if($userrole==5)
		{
			redirect('contract-form-102');
		}
		
        $data['page_title'] = $this->lang->line('PROFILE_UPDATE');
        $id=$this->session->userdata('userID');
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');
			//$this->form_validation->set_rules('userphone', 'phone number', 'trim|required|is_unique[users.mobile]',array('is_unique' => 'Applicant mobile number already exist.'));
			//$this->form_validation->set_rules('useremail', 'email ID', 'required|valid_email|is_unique[users.email]',array('is_unique' => 'Applicant email ID already exist.'));
			echo $this->input->post('full_number');
			//die();
			if($this->form_validation->run() == FALSE) {
				echo validation_errors();
			} else {				
				$dataArr['username']        = $this->input->post('fname').' '.$this->input->post('lname');
				$dataArr['fname']        	= $this->input->post('fname');
				$dataArr['lname']        	= $this->input->post('lname');
				$dataArr['email']           = $this->input->post('email');
				$dataArr['mobile']          = $this->input->post('mobbox');				
				$dataArr['site_location']   = $this->input->post('site_location');				
				$dataArr['aptno']           = $this->input->post('aptno');				
				$dataArr['street_number']   = $this->input->post('street_number');				
				$dataArr['street_name']     = $this->input->post('street_name');				
				$dataArr['street_type']     = $this->input->post('street_type');				
				$dataArr['direction']     	= $this->input->post('direction');				
				$dataArr['country']         = $this->input->post('country');				
				$dataArr['state']           = $this->input->post('state');				
				$dataArr['city']            = $this->input->post('city');				
				$dataArr['postal_code']     = $this->input->post('postal_code');				
				$dataArr['dob']             = $this->input->post('dob');				
				$dataArr['gender']          = $this->input->post('gender');
				$dataArr['latitude']        = $this->input->post('latitude');				
				$dataArr['longitude']       = $this->input->post('longitude');
							
				$upd_id= $this->login_model->setUpdateData($this->users, $dataArr,$id);
				
				if(!empty($_FILES["featured_img"]["name"]))
				{
					 $featured_img=$this->do_upload('users','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 //print_r($data_featured_img); die;
					 $this->login_model->setUpdateData($this->users,$data_featured_img,$id);
				}

				if($upd_id>0)
				{
					$dataCArea['zipcode']   = $this->input->post('postal_code');
	                $dataCArea['latitude']  = $this->input->post('latitude');
	                $dataCArea['longitude'] = $this->input->post('longitude');
	                $dataCArea['country']   = $this->input->post('country');
	                $dataCArea['state']     = $this->input->post('state');
	                $dataCArea['city']      = $this->input->post('city');
	                $dataCArea['address']   = $this->input->post('site_location');
	                if(!empty($dataCArea['zipcode']))
	                {
	                	$this->insertZipcodeWithRelatedInfo($dataCArea);
	                }                	

					$this->session->set_flashdata('message','Your profile has been update successfully');
		        	redirect('edit-profile','refresh');
			    } 
			}
		}

        $data['userDetails']=$this->front_model->getUserDetails($id);
		$this->load->view('front/include/header', $data);
		$this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/users/profile_update', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
	}

	public function insertZipcodeWithRelatedInfo($dataArr)
    {

        $wherArr = array('zipcode'=> $dataArr['zipcode']);
        $isExist = $this->front_model->isExist($this->services_area,$wherArr);
        if(empty($isExist))
        {
            $data['zipcode']    = $dataArr['zipcode'];
            $data['fas']        = '';
            $data['latitude']   = $dataArr['latitude'];
            $data['longitude']  = $dataArr['longitude'];
            $data['country']    = $dataArr['country'];
            $data['place_name'] = $dataArr['state'];
            $data['area']       = $dataArr['city'];
            $data['address']    = $dataArr['address'];
            $isExist = $this->front_model->setInsertData($this->services_area,$data);
        }
    }

	public function do_upload($folder, $filename) {
		
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?

		//$config['max_size'] 	= '1000';
		//$config['max_width'] 	= '1024';
		//$config['max_height'] = '768';

		$uploaddir = 'upload/'.$folder.'/';

		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$input = $filename; // on view the name <input type="file" name="userfile" size="20"/>

		if (!$this->upload->do_upload($filename))
		{
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
		}
		else
		{
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];
			$file_path=$uploaddir.$file_name;
			return $file_path;
		}
	}

}