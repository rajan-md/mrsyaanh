<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('front_model');  
        $this->load->model('products_model');  
        $this->load->helper('front_helper');
        $this->load->model('smtp_model');
        $this->load->library('cart');        
    }

    public $users='tbl_users';
    public $wishlist="tbl_wishlist";
    public $shipping_information="tbl_shipping_information";
    public $products_orders="tbl_products_orders"; 
    public $products_orders_items="tbl_products_orders_items";
    

	public function lists()    { 

        $perpage=$this->input->get('perpage');  
        if($perpage!=""){
            $per_page=$perpage;           
        } else {
            $per_page=15;        
        }
        if($this->uri->segment(2)){
            $page = ($this->uri->segment(2)) ;
        }
        else{
            $page = 1;
        }      
        $start=($page-1)*$per_page;
        $limit=$per_page;
        $language_id=$this->front_model->getLanguage();
        $totalResult=$this->products_model->getTotalProducts($language_id);
        $data['startview']=$start;
        $data['totalResult']=$totalResult;
        if($totalResult < $per_page){
            $data['limitview']=$totalResult;
        } else {
            $data['limitview']=$limit;
        }

        $data['totalResult']=$totalResult;
        $data['shopData']=$this->products_model->getProductsCollection($language_id,$limit,$start);
        $data['categoryList']=$this->products_model->getProductsCategoryCollection($language_id);
        $data['footerContent']=$this->front_model->getFooterHomeData($language_id);
        $data['appContent']=$this->front_model->getFooterHomeData($language_id);
        $data["pagination"] = Jpagination($totalResult,$limit,$start); 
        $this->load->view('front/include/header', $data);
        $this->load->view('front/shop/list', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
        
    }

    public function addtocart() {   
        
        $product_id=$this->input->post('product_id');
        $qty=$this->input->post('qty');
        if($qty<=0){
            $qty=1;
        }
        $productName=getProductName($product_id);
        $price=getProductPrice($product_id);
        $data = array(
        'id'      => $product_id,
        'qty'     => $qty,
        'price'   => $price,
        'name'    => $productName
        );

        if ($this->cart->total_items() >=0) {         
            $cartData=$this->cart->insert($data);
            echo $productName.' successfully added to your shopping cart';
        }
    }

    public function updatecart() {   
        $row_id=$this->input->post('row_id');
        $qty=$this->input->post('qty');
        $data = array(
        'rowid' => $row_id,
        'qty'   => $qty
		);
		$this->cart->update($data);
		echo 'Quantity has been update successfully';
    } 

    public function removeCart() {   
        $row_id=$this->input->post('row_id');
        $qty=$this->input->post('qty');
        $data = array(
        'rowid' => $row_id,
        'qty'   => $qty
        );
        $this->cart->remove($row_id);
        echo 'Your item has been remove from  shopping cart successfully';
    }

    public function removeWishlist() { 
        $id=$this->input->post('id');
        $array = array('id'=> $id);
        $this->db->where($array);
        $this->db->delete($this->wishlist);
		echo 'Your item has been remove from wishlist';
    }

     public function addtowishlist() {       
        $product_id=$this->input->post('product_id');
        $user_id = $this->session->userdata('userID');
        $productName=getProductName($product_id);
        if(!empty($user_id)){
            $is_wishlist=in_wishlist($product_id,$user_id);
            if($is_wishlist){
                $data['product_id']=$product_id;
                $data['user_id']=$user_id;
                $this->front_model->setInsertData($this->wishlist,$data);
                echo $productName.' successfully added to your wishlist cart';
            } else {
                echo $productName.' already in your wishlist cart';                
            }
        }   else {
            echo '2';
        }
    }

    public function product_details($slug)    {            
        $language_id=$this->front_model->getLanguage();
        $data['shopData']=$this->products_model->getSingleProductsDataCollection($language_id,$slug); 
        $data['footerContent']=$this->front_model->getFooterHomeData($language_id); 
        $data['featureproducts']=$this->front_model->getFeaturProducts($language_id);  
        $data['appContent']=$this->front_model->getFooterHomeData($language_id);
        $js=array('jquery.exzoom.js','exzoom.js');
        $this->config->set_item('headJs',$js);  
        $this->load->view('front/include/header', $data);
        $this->load->view('front/shop/details', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }

    public function cart_list() {            
        $language_id=$this->front_model->getLanguage();
        $data['footerContent']=$this->front_model->getFooterHomeData($language_id);
        $data[]=array();
        $this->load->view('front/include/header', $data);
        $this->load->view('front/shop/cart', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);       
    }

    public function checkout_review()
    {           
        $data[]=array();
        $post_data=$this->input->post();
        if(!empty($post_data)) { 
            $shippingData = array(
            'fname'             =>$this->input->post('firstname'),
            'lname'             =>$this->input->post('lastname'),
            'email'             =>$this->input->post('emailaddress'),
            'mobile'            =>$this->input->post('mobbox'),
            'site_location'     =>$this->input->post('site_location'),
            'aptno'             =>$this->input->post('aptno'),            
            'street_number'     =>$this->input->post('street_number'),
            'street_name'       =>$this->input->post('street_name'),
            'street_type'       =>$this->input->post('street_type'),
            'direction'         =>$this->input->post('direction'),
            'city'              =>$this->input->post('city'),
            'country'           =>$this->input->post('country'),
            'state'             =>$this->input->post('state'),            
            'postal_code'       =>$this->input->post('postal_code'),
            'fax'               =>$this->input->post('fax')
            );  
            $this->session->set_userdata('shippingData',$shippingData);
            redirect('shipping-method','refresh');
        } else {
            //$this->session->set_userdata('shippingData','');
            $this->session->set_userdata('shippingMethod','');
            $this->session->set_userdata('paymentMethod','');
        }
        $user_id = $this->session->userdata('userID');
        if(!empty($user_id))
        {
            $userdata = $this->front_model->getDataCollectionByID($this->users,$user_id);
        }
        $data['userdata'] = !empty($userdata)?$userdata:array();
        $this->load->view('front/include/header', $data);
        $this->load->view('front/shop/checkout', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);       
    }

    public function shipping_method() {           
        $data[]=array();
        $shippingData=$this->session->userdata('shippingData');
        if(!empty($shippingData)) {
            $post_data=$this->input->post();
            if(!empty($post_data)) { 
                $shippingMethod=$this->input->post('shipping_method');
                $this->session->set_userdata('shippingMethod',$shippingMethod);
                redirect('payment-method','refresh');
            }
            $this->load->view('front/include/header', $data);
            $this->load->view('front/shop/shipping-method', $data);
            $this->load->view('front/include/modals', $data);
            $this->load->view('front/include/footer', $data);
        } else {
            redirect('checkout','refresh');
        }       
    }

    public function payment_method() {           
        $data[]=array();
        $shippingData=$this->session->userdata('shippingData');
        $shippingMethod=$this->session->userdata('shippingMethod');
        if((!empty($shippingData)) && (!empty($shippingMethod))) {
            $post_data=$this->input->post();
            if(!empty($post_data)) { 
                $paymentMethod=$this->input->post('payment_method');
                $this->session->set_userdata('paymentMethod',$paymentMethod);
                redirect('order-review','refresh');
            }
            $this->load->view('front/include/header', $data);
            $this->load->view('front/shop/payment-method', $data);
            $this->load->view('front/include/modals', $data);
            $this->load->view('front/include/footer', $data);
        } else {
            redirect('checkout','refresh');
        }       
    }    

    public function order_review() {           
        $data[]=array();
        $shippingData=$this->session->userdata('shippingData');
        //print_r($shippingData); die;
        $shippingMethod=$this->session->userdata('shippingMethod');
        $paymentMethod=$this->session->userdata('paymentMethod');
        $user_id = $this->session->userdata('userID');
        if((!empty($shippingData)) && (!empty($shippingMethod)) && (!empty($paymentMethod))) {
            $data['payment_method']=$paymentMethod;
            $post_data=$this->input->post();
            if(!empty($post_data)) { 
                $shipping=0;
                $orderDetails['user_id']        =$user_id;
                $orderDetails['shipping_method']=$shippingMethod;
                $orderDetails['shipping_amount']=$shipping;
                $orderDetails['payment_method'] =$paymentMethod;
                $orderDetails['subtotal']       =$this->input->post('payamount');
                $orderDetails['discount']       =0;
                $orderDetails['total']          =$this->input->post('payamount');
                $orderDetails['orderdata']      =date('Y-m-d H:i:s');
                $orderDetails['orderstatus']    =2;

                $orderId=$this->front_model->setInsertData($this->products_orders,$orderDetails);
                $shippingData["order_id"]=$orderId;
                $this->session->set_userdata('orderId',$orderId);
                if ($this->cart->total_items() > 0) {
                    foreach ($this->cart->contents() as $cartItem) { 
                        $orderItem['order_id']   =$orderId;
                        $orderItem['product_id'] =$cartItem['id'];
                        $orderItem['qty']        =$cartItem['qty'];
                        $orderItem['unit_price'] =$cartItem['price'];
                        $orderItem['prices']     =$cartItem['price'];
                        $this->front_model->setInsertData($this->products_orders_items,$orderItem);
                    }
                }
                $this->front_model->setInsertData($this->shipping_information,$shippingData);
                if($paymentMethod!='cod'){
                    redirect('payment-processing','refresh');
                }
                else
                {  
                    send_order_notification($orderId);                 
                    redirect('thank-you','refresh');                   
                }
            }
            $this->load->view('front/include/header', $data);
            $this->load->view('front/shop/order-review', $data);
            $this->load->view('front/include/modals', $data);
            $this->load->view('front/include/footer', $data); 
        } else {
            redirect('checkout','refresh');
        }        
    }

    public function payment_processing()
    {  
        $data[]=array();
        $user_id = $this->session->userdata('userID');
        $data['code']=getCurrencySign();
        $orderId=$this->session->userdata('orderId');
        $data['order_id']=$orderId;
        $data['ordersDetails']=getOrderDetailsById($orderId);
        $data['userData']=getCustomerDetailsById($user_id);
        $shippingData=$this->session->userdata('shippingData');
        $shippingMethod=$this->session->userdata('shippingMethod');
        $paymentMethod=$this->session->userdata('paymentMethod');
        $this->load->view('front/payment/'.$paymentMethod, $data);         
    }

    public function wishlist() { 
        $data['page_title'] = $this->lang->line('WISHLIST');           
        $language_id=$this->front_model->getLanguage();
        $data['footerContent']=$this->front_model->getFooterHomeData($language_id);
        $data['appContent']=$this->front_model->getFooterHomeData($language_id);
        $data['appContent']=$this->front_model->getFooterHomeData($language_id);
        $user_id = $this->session->userdata('userID');
        $data['wishlistContent']=$this->products_model->getWishlistDataCollection($language_id,$user_id);
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/shop/wishlist', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data); 
        
    }

    public function orders_details() { 
        $data['page_title']='Order Details';           
        
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        // $this->load->view('front/users/sidebar', $data);
        $this->load->view('front/orders/details', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data); 
        
    }

    public function thank_you() { 
        $data['page_title']='';       
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/orders/thank-you', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);
        
    }

    public function cancel() { 
        $data['page_title']='Canceled';       
        $this->load->view('front/include/header', $data);
        $this->load->view('front/users/container_start', $data);
        $this->load->view('front/orders/cancel', $data);
        $this->load->view('front/users/container_end', $data);
        $this->load->view('front/include/modals', $data);
        $this->load->view('front/include/footer', $data);        
    }
}