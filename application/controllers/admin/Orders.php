<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Orders extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $table = "tbl_products";
	public $products_content = "tbl_products_content";
	public $category = "tbl_category";	
	public $orders = "tbl_products_orders";
	public $orders_items = "tbl_products_orders_items";	
	function __construct() {
		parent::__construct();	
		$this->load->model('products_model');
		$this->load->helper('admin_helper');

	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['ordersData']=$this->products_model->getProductsOrderCollection();
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/orders/orders',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function view()
	{		
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$data['ordersData'] = $orderDetails = get_record($this->orders,array('id'=>$id));
		$data['orderItems'] = $orderItems = get_records($this->orders_items,array('order_id'=>$id));
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/orders/view',$data);
		$this->load->view('admin/include/footer',$data);
	}
	
    
}