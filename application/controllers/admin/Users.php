<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    
    public $tbl_name = "users";
    public $tbl_roles = "role";
    
	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['records'] = $this->master_model->get_records($this->tbl_name);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/users/lists',$data);
		$this->load->view('admin/include/footer',$data);
	}

	
	public function add()
	{
		CheckAdminLoginSession();	
		$post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');					
			$this->form_validation->set_rules('email', 'email ID', 'trim|required|valid_email|is_unique['.$this->tbl_name.'.email]',array('is_unique' => 'Applicant email ID already exist.'));		
			$this->form_validation->set_rules('mobile', 'mobile', 'required');
			$this->form_validation->set_rules('role', 'role', 'required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password ', 'trim|required|matches[password]');						
			if($this->form_validation->run() != FALSE)
			{

				$dataArr['role'] = $role = $this->input->post('role');
				$dataArr['username'] = $this->input->post('name');
				if(!empty($role) && $role==5)
				{
					$dataArr['companyname'] = $this->input->post('companyname');
				}				
				$dataArr['name'] = $this->input->post('name');
				$dataArr['email'] = $this->input->post('email');
				$dataArr['mobile'] = $this->input->post('mobbox');
				$dataArr['created'] = date('Y-m-d h:i:sa');
				$dataArr['password'] = md5($this->input->post('password'));
				$dataArr['status'] = 1;

				$this->master_model->setInsertData($this->tbl_name,$dataArr);
				$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'User has been added successfully !!'));
		        redirect('admin/users','refresh');
		    }
        }
        $data['roleOptions'] = getOptions($this->tbl_roles, 'Select role');
        
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/users/add', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function edit()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		$on=$this->input->post('pass');
		$data['on']=$on;
		$resetpass=$this->input->post('password');
		$profile_type=$this->input->post('profile_type');
		$confirm_password=$this->input->post('confirm_password');
		if(!empty($post_data)) { 			
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('role', 'role', 'required');
			if(!empty($this->input->post('role')) && $this->input->post('role')==5)
			{ 
				$this->form_validation->set_rules('companyname', 'company name', 'required');
			}
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('mobbox', 'mobile', 'required');
			$this->form_validation->set_rules('email', 'email ID', 'trim|required|valid_email');
			$this->form_validation->set_rules('country', 'country', 'required');
			$this->form_validation->set_rules('state', 'state', 'required');
			$this->form_validation->set_rules('site_location', 'address', 'required');
			$this->form_validation->set_rules('city', 'city', 'required');
			$this->form_validation->set_rules('postal_code', 'zipcode', 'required');							

            if(!empty($on)) {   
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
            	$this->form_validation->set_rules('confirm_password', 'Confirm Password ', 'trim|required|matches[password]');
            }
									
			if($this->form_validation->run() != FALSE)
			{

				$dataArr['role'] = $role = $this->input->post('role');
				$dataArr['username'] = $this->input->post('name');
				if(!empty($role) && $role==5)
				{
					$dataArr['companyname'] = $this->input->post('companyname');
				}				
				$dataArr['name'] = $this->input->post('name');
				$dataArr['email'] = $this->input->post('email');
				$dataArr['mobile'] = $this->input->post('mobbox');
				$dataArr['created'] = date('Y-m-d h:i:sa');
				$dataArr['password'] = md5($this->input->post('password'));
				$dataArr['site_location']   = $this->input->post('site_location');
                $dataArr['country']         = $this->input->post('country');
				$dataArr['state']           = $this->input->post('state');
				$dataArr['city']            = $this->input->post('city');				
				$dataArr['postal_code']     = $this->input->post('postal_code');

				$update_id=$this->master_model->setUpdateData($this->tbl_name,$dataArr,$id);
                

				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img = do_upload('users','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->master_model->setUpdateData($this->tbl_name,$data_featured_img,$id);
				}

				$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'User has been updated successfully !!'));
		        redirect('admin/users','refresh');
		    }
        }

        $data['userData'] = $this->master_model->get_recordArr($this->tbl_name, array('id' => $id));
        $data['roleOptions']=getOptions($this->tbl_roles, 'Select role');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/users/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}

	

	public function status()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
		$userData = $this->master_model->get_recordArr($this->tbl_name, array('id' => $id));
		$data['status'] = $status = (empty($statusId) || $statusId == 0) ? 1 : 0;
		if(!empty($status) && $status==1)
		{
			$status_label = 'Approved';
			$subject = 'Account approved';
			$message_line1 = 'Congratulation, Your account has been approved.';
		}
		elseif(!empty($status) && $status==6)
		{
			$status_label = 'Rejected';
			$subject = 'Account rejected';
			$message_line1 = 'Sorry to inform, due to any reason your account has been rejected. please contact to admin and resolved concern.';
		}
		else
		{			
			$status_label = 'Pending';
			$subject = 'Account on Pending state';
			$message_line1 = 'Sorry to inform, due to any reason your account has been hold on pending stat. please contact to admin and resolved concern.';
		}
				
		$this->master_model->setUpdateData($this->tbl_name,$data,$id);
		$email_template  = 'user-status.html';
		$templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}'=>'firstchoice.com',
            '{{site_url}}'=> base_url(),
            '{{team_name}}'=>'firstchoice',
            '{{user_name}}'=>$userData['username'],
            '{{user_email}}'=>$userData['email'],
            '{{status_label}}'=>$status_label,
            '{{status_message}}'=>$message_line1,
            '{{year}}'=>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
        $message = email_compose($email_template,$templateTags);
        send_email($userData['email'],$subject,$message);

		$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'User status has been updated successfully !!'));
        redirect('admin/users','refresh');
	}

	public function delete()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$this->master_model->DeleteByID($this->tbl_name,$id);
		$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'User has been removed successfully !!'));
        redirect('admin/users','refresh');
	}


	public function profile()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->session->userdata('admin_id');
		$on=$this->input->post('pass');
		$data['on']=$on;
		$resetpass=$this->input->post('password');
		$confirm_password=$this->input->post('confirm_password');
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('fname', 'first name', 'required');
			$this->form_validation->set_rules('lname', 'last name', 'required');				
			$this->form_validation->set_rules('mobile', 'mobile', 'required');
			$this->form_validation->set_rules('email', 'email ID', 'trim|required|valid_email');
			$this->form_validation->set_rules('role', 'role', 'required');
			$this->form_validation->set_rules('site_location', 'site_location', 'required');
			$this->form_validation->set_rules('country', 'country', 'required');
			$this->form_validation->set_rules('state', 'state', 'required');
			$this->form_validation->set_rules('city', 'city', 'required');
			$this->form_validation->set_rules('postal_code', 'postal_code', 'required');
            if(!empty($on)) {   
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password ', 'trim|required|matches[password]');
            }
									
			if($this->form_validation->run() == FALSE) {   } else {
								
				$dataArr['fname']= $this->input->post('fname');
				$dataArr['lname']= $this->input->post('lname');
				$dataArr['site_location']= $this->input->post('site_location');				
				$dataArr['mobile']= $this->input->post('mobile');
                $dataArr['email']= $this->input->post('email');

                $dataArr['country']= $this->input->post('country');
				$dataArr['state']= $this->input->post('state');
				$dataArr['city']= $this->input->post('city');				
				$dataArr['postal_code']= $this->input->post('postal_code');
				$dataArr['fax']= $this->input->post('fax');

				$dataArr['role']= $this->input->post('role');
				if(!empty($on)) { 
				$dataArr['password']= md5($this->input->post('password'));	
				}				        	             
				

				$update_id=$this->admin_model->setUpdateData($this->tbl_name,$dataArr,$id);
				if($_FILES["featured_img"]["name"]!=" ")
				{
					 $featured_img=$this->do_upload('users','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->admin_model->setUpdateData($this->tbl_name,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your user has been update successfully');
		        //redirect('admin/users','refresh');
		    }
        }
		
        $data['userData']=$this->admin_model->getDataCollectionByID($this->tbl_name,$id);
        $data['roleOptions']=getOptions($this->tbl_roles, 'Select role');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/users/profile',$data);
		$this->load->view('admin/include/footer',$data);
	}

	

	public function do_upload($folder, $filename) {
		// Codeigniter upload for single files only.
		// Uploads folder must be in main directory.
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?

		//$config['max_size'] = '1000';
		// $config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$uploaddir = 'upload/'.$folder.'/';
		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$input = $filename; // on view the name <input type="file" name="userfile" size="20"/>

			if (!$this->upload->do_upload($filename)) {
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
				$file_path=$uploaddir.$file_name;
				return $file_path;
			}

	}
}