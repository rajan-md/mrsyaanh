<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
		
	}

	public function index() {
		$data['active']='login';
		$admin_id = $this->session->userdata('admin_id');
		if(!empty($admin_id)) {
			redirect('admin/dashboard','refresh');
		} else {   
			$login_arr=$this->input->post();		
			if(!empty($login_arr)) {				
                $isactive= $this->admin_model->IsUserActive();
				if($isactive==1) {			
					$admindata= $this->admin_model->AdminLogin();
					if($admindata==1) {
						redirect('admin/dashboard','refresh');
					} else {
						$this->session->set_flashdata('error','Please enter the correct username or password');
						redirect('admin/login','refresh');
					}

				} else {

					$this->session->set_flashdata('error','Your account has been disabled , please contact to administrator');
					redirect('admin/login','refresh');
				}
			}

		}
		$this->load->view('admin/login');
	}

	public function logout()
	{   
		//$this->session->sess_destroy();
		$userdata = array(
		'admin_id'    =>'',
		'role'    =>'',
		'admin_name'    =>'',
		'admin_email' =>''
		);
		$this->session->set_userdata($userdata);
		redirect('admin/login','refresh');
	}

}
