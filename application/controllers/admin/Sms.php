<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller
{
	public $table = "sms";
	 
	function __construct() {
		parent::__construct();
	    
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
	}

	public function index(){
		CheckAdminLoginSession();
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('sms_mobile', 'sms user', 'required');
			$this->form_validation->set_rules('sms_title', 'sms title', 'required');				
			$this->form_validation->set_rules('sms_message', 'sms message', 'required');				
			if($this->form_validation->run() != FALSE)
			{
			  $sms_mobile  =$this->input->post('sms_mobile');
			  $sms_title   =$this->input->post('sms_title');
			  $sms_message =$this->input->post('sms_message');
			  $message_body=$sms_title.' - '.$sms_message;
			  if($sms_mobile=='smsall'){
                send_sms_to_all_users($message_body);
			  } else {
			  	$result=send_sms($sms_mobile,$message_body);
			  	$this->session->set_flashdata('message','Your home settings has been update successfully'.$result);
                redirect('admin/sms','refresh');
			  }
				
		    }
        }
        $data[]='';
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/sms/sms', $data);
		$this->load->view('admin/include/footer', $data);
	}

    public function ajax_find_user(){
         
        $keyword  =$this->input->post('keyword');
        $this->db->where('status', 1);
        $this->db->like('username', $keyword);
		$query = $this->db->get('tbl_users');
		$count = $query->num_rows();
	    if($count>0){
			$result = $query->result();
			foreach ($result as $data) {
             echo '<li onclick="SetSearchData(this)" mobile="'.$data->mobile.'" title="'.$data->username.' - '.$data->mobile.'">'.$data->username.' - '.$data->mobile.'</li>';
			}
		}
		else{
            echo '<li>Result not found</li>';
	    }
    }
}