<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $table = "tbl_products";
	public $products_content = "tbl_products_content";
	public $category = "tbl_category";	
	function __construct() {
		parent::__construct();	
		$this->load->model('products_model');
		$this->load->helper('admin_helper');

	}
	public function index()
	{		
		CheckAdminLoginSession();
		$data['productsData']=$this->products_model->getProductsCollection();
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/products/products',$data);
		$this->load->view('admin/include/footer',$data);
	}
	public function add()
	{
		CheckAdminLoginSession();		
		$post_data=$this->input->post();
		//print_r($post_data);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');						
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug'       => $slug,
				'price'	     =>$this->input->post('price'),
				'sku'	     =>$this->input->post('sku'),
				'quantity'   =>$this->input->post('quantity'),
				'height'     =>$this->input->post('height'),
				'weight'     =>$this->input->post('weight'),
				'category'	 =>$this->input->post('category'),
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key'   => $this->input->post('meta_key'),			
				'created'    => date('Y-m-d H:i:s'),
				'status'     => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 
				$id=$this->products_model->setInsertProductsData($this->table,$data);
				$j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {                    
					$data_content['products_id'] = $id;
					$data_content['language_id'] = $langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$this->products_model->setInsertProductsData($this->products_content,$data_content);
					$j++;
				}
				$this->doProductsPic($id);
				$this->session->set_flashdata('message','Your new products has been added successfully');
		        redirect('admin/products','refresh');
		    }
        }
        $data['language']=$this->products_model->getProductsLanguageCollection('tbl_language');
        $data['categoryOptions']= $this->getProductCategoryOptions($this->category, 'Select category');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/products/add',$data);
		$this->load->view('admin/include/footer',$data);
	}


	public function getProductCategoryOptions($table="", $type="", $status=0){
		$options = array(''=>$type);
		if($table){
			 $query= $this->db->select('cat.id as id, cat.created as created, cat.featured_img as featured_img, cat.status as status , c.name as name, c.description as description')
		        ->from('tbl_category as cat')
		        ->join('tbl_category_content as c', 'cat.id = c.category_id', 'LEFT')
		        ->where('c.language_id', 1)
		        ->get();
			$resultData =  $query->result();
			foreach ($resultData as $value) {
				$options[$value->id] = $value->name;
			}
		}
		return $options;
	}
	public function edit()	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');
			$this->form_validation->set_rules('price', 'price', 'required');
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug'             => $slug,
				'price'	           =>$this->input->post('price'),		
				'sku'	           =>$this->input->post('sku'),	
				'quantity'         =>$this->input->post('quantity'),
				'height'           =>$this->input->post('height'),
				'weight'           =>$this->input->post('weight'),	
				'category'	       =>$this->input->post('category'),		
				'meta_title'       => $this->input->post('meta_title'),				
				'meta_key'         => $this->input->post('meta_key'),
				'meta_description' => $this->input->post('meta_description')					        	             
				); 
				$this->products_model->setUpdateProductsData($this->table,$data,$id);

                $j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {
                    $lang_id=$langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$data_content['products_id'] = $id;
					$data_content['language_id'] = $lang_id;
					$check_products_content=$this->products_model->CheckProductsContent($this->products_content,$id,$lang_id);
					if($check_products_content==1){
					$this->products_model->setUpdateproductsContentData($this->products_content,$data_content,$id,$lang_id);
				    }
				    else
				    {
				    	$this->products_model->setInsertProductsData($this->products_content,$data_content);
				    }
					$j++;
				}
				$this->doProductsPic($id);
				$this->session->set_flashdata('message','Your products has been update successfully');
		        redirect('admin/products','refresh');
		    }
        }
        $data['productsData']=$this->products_model->getProductsDataCollectionByID($this->table,$id);

        $data['language']=$this->products_model->getProductsLanguageCollection('tbl_language');
         $data['categoryOptions']= $this->getProductCategoryOptions($this->category, 'Select category');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/products/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}
        public function status()
        {
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$statusId=getStatusById($this->table,$id);
		if($statusId==2) {
		   $data['status']=1;           
		}
		else {
           $data['status']=2;
		}
		$this->products_model->setUpdateProductsData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your products status has been change successfully');
        redirect('admin/products','refresh');
	}

    public function feature()
    {
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$featureId=getFeatureById($this->table,$id);
		if($featureId==2) {
		   $data['feature']=1;           
		}
		else {
           $data['feature']=2;
		}
		$this->products_model->setUpdateProductsData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your feature status has been change successfully');
        redirect('admin/products','refresh');
	}
	public function delete()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$this->products_model->DeleteproductsByID($this->table,$id);
		$this->session->set_flashdata('message','Your products has been deleted successfully');
        redirect('admin/products','refresh');
	}
	public function do_upload($folder, $filename) {
		// Codeigniter upload for single files only.
		// Uploads folder must be in main directory.
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?
		//$config['max_size'] = '1000';
		// $config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$uploaddir = 'upload/'.$folder.'/';
		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$input = 'featured_img'; // on view the name <input type="file" name="userfile" size="20"/>
			if (!$this->upload->do_upload('featured_img')) {
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
				$file_path=$uploaddir.$file_name;
				return $file_path;
			}
	}


	public function doProductsPic($id){
		$table='tbl_products_images';
		$data = array();
		if(!empty($_FILES['productimg']['name'])){
			$filesCount = count($_FILES['productimg']['name']);
		
			for($i = 0; $i < $filesCount; $i++){
				$_FILES['productname']['name'] = $_FILES['productimg']['name'][$i];
				$_FILES['productname']['type'] = $_FILES['productimg']['type'][$i];
				$_FILES['productname']['tmp_name'] = $_FILES['productimg']['tmp_name'][$i];
				$_FILES['productname']['error'] = $_FILES['productimg']['error'][$i];
				$_FILES['productname']['size'] = $_FILES['productimg']['size'][$i];
				$uploadPath = 'upload/products/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('productname')){
					$fileData = $this->upload->data();
					$uploadData['file_name'] = $uploadPath.$fileData['file_name'];
					$uploadData['created'] = date("Y-m-d H:i:s");
					$uploadData['product_id'] = $id;
					setPicInsertData($table, $uploadData);
				}
				
			}
			

		}
	}
}