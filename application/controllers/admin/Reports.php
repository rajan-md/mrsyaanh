<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $service_orders = "tbl_service_orders";

	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$limit_per_page = $this->config->item('adm_limits_per_page');
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $start_index = $page*$limit_per_page;

        $whereArr = array('status'=>5);

        $total_records = get_total_records($this->service_orders,$whereArr);
        if ($total_records > 0) 
        {
            $data["records"] = get_current_page_records($this->service_orders,$limit_per_page, $start_index,$whereArr);
            $data["paginations"] = Jpagination($total_records, $limit_per_page,base_url('admin/reports'));
        }

        $serviceProvider = serviceProvidersList();
        $serviceProviderOptions = array('---Choose Service Provider---');
        if(!empty($serviceProvider))
        {
        	foreach ($serviceProvider as $row)
        	{
        		$sp_details = '';
        		if(!empty($row->username) && !empty($row->business_name))
        		{
        			$sp_details = $row->username.' ('.$row->business_name.')';
        		}
        		elseif(!empty($row->username) && empty($row->business_name))
        		{
        			$sp_details = $row->username;
        		}
        		
        		$serviceProviderOptions[$row->id] = $sp_details;
        	}
        }
        //echo "<pre>"; print_r($serviceProvider); echo "</pre>";
        $data["serviceProviderOptions"] = $serviceProviderOptions;
        $data["durationOptions"] = array("weekly"=>"Weekly","bi-monthly"=>"Bi-Monthly","monthly"=>"Monthly","quaterly"=>"Quaterly");
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/reports/services-report',$data);
		$this->load->view('admin/include/footer',$data);
	}
}