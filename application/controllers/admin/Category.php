<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $table = "tbl_category";
	public $category_content = "tbl_category_content";


	function __construct() {
		parent::__construct();
		$this->load->model('category_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['categoryData']=$this->category_model->getCategoryCollection();
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/category/category',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function add()
	{
		CheckAdminLoginSession();		
		$post_data=$this->input->post();
		//print_r($post_data);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');		
								
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug' => $slug,				
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'created' => date('Y-m-d H:i:s'),
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 

				$id=$this->category_model->setInsertCategoryData($this->table,$data);
				$j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {                    
					$data_content['category_id'] = $id;
					$data_content['language_id'] = $langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$this->category_model->setInsertCategoryData($this->category_content,$data_content);
					$j++;
				}
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('category','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->category_model->setUpdateCategoryData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your new category has been added successfully');
		        redirect('admin/category','refresh');
		    }
        }

        $data['language']=$this->category_model->getCategoryLanguageCollection('tbl_language');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/category/add',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function edit()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
				$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');
									
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug' => $slug,				
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 
				$this->category_model->setUpdateCategoryData($this->table,$data,$id);
                
                $j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {
                    $lang_id=$langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$data_content['category_id'] = $id;
					$data_content['language_id'] = $lang_id;
					$check_category_content=$this->category_model->CheckCategoryContent($this->category_content,$id,$lang_id);
					if($check_category_content==1){
					$this->category_model->setUpdateCategoryContentData($this->category_content,$data_content,$id,$lang_id);
				    }
				    else
				    {
				    	$this->category_model->setInsertCategoryData($this->category_content,$data_content);
				    }
					$j++;
				}
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('category','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->category_model->setUpdateCategoryData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your category has been update successfully');
		        redirect('admin/category','refresh');
		    }
        }
        $data['categoryData']=$this->category_model->getCategoryDataCollectionByID($this->table,$id);
        
        $data['language']=$this->category_model->getCategoryLanguageCollection('tbl_language');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/category/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function status()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$statusId=getStatusById($this->table,$id);
		if($statusId==2) {
		   $data['status']=1;           
		}
		else {
           $data['status']=2;
		}
		$this->category_model->setUpdateCategoryData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your category status has been change successfully');
        redirect('admin/category','refresh');
	}

	public function delete()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$this->category_model->DeleteCategoryByID($this->table,$id);
		$this->session->set_flashdata('message','Your category has been deleted successfully');
        redirect('admin/category','refresh');
	}

	public function do_upload($folder, $filename) {
		// Codeigniter upload for single files only.
		// Uploads folder must be in main directory.
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?

		//$config['max_size'] = '1000';
		// $config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$uploaddir = 'upload/'.$folder.'/';
		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$input = 'featured_img'; // on view the name <input type="file" name="userfile" size="20"/>

			if (!$this->upload->do_upload('featured_img')) {
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
				$file_path=$uploaddir.$file_name;
				return $file_path;
			}

	}
}