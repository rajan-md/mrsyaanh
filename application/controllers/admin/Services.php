<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller
{
    
    public $tbl_name = "services";
    public $users = "users";
    public $services_price = "services_price";
    public $service_provider_request = "service_provider_request";
    public $service_provider_settings = "service_provider_settings";
    public $currencies = "currencies";
    public $services_area = "services_area";
    public $service_orders = "service_orders";
    public $service_operations = "service_operations";
    public $videos = "videos";
    public $invoice = "invoice";
    
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('services_model');
        $this->load->helper('admin_helper');
        $this->load->helper('download');
    }
    
    public function index()
    {
        CheckAdminLoginSession();
        $records = $this->master_model->get_recordsArr($this->tbl_name);
        $data['records'] = getOptionsTree($records); 
        //$data['records'] = getOptionsTree($this->tbl_name); 
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/lists', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function add()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name', ' name', 'required');
            $this->form_validation->set_rules('description', 'description', 'required');
            
            if ($this->form_validation->run() != FALSE) {
                $slug_str = $this->input->post('name[0]');
                $slug     = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data     = array(
                    'parent' => $this->input->post('parent'),
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'description' => $this->input->post('description'),
                    'meta_title' => $this->input->post('meta_title'),
                    'meta_key' => $this->input->post('meta_key'),
                    'meta_description' => $this->input->post('meta_description'),
                    'status' => 1
                );
                $record_id = $this->master_model->setInsertData($this->tbl_name, $data);
                if(!empty($record_id))
                {
                    if ($_FILES["featured_img"]["name"] != "")
                    {
                        $featured_img      = do_upload('services');
                        $data_featured_img = array('featured_img' => $featured_img);
                        $this->master_model->setUpdateData($this->tbl_name, $data_featured_img, $record_id);
                    }
                    $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been added successfully !!'));
                    redirect('admin/services', 'refresh');
                }
            }
        }
        $data['parentOptions'] = getMultilevelOptions($this->tbl_name, array('status'=>1), '---Self---');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/add', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        $rec_id        = $this->uri->segment(4);
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name', ' name', 'required');
            $this->form_validation->set_rules('description', 'description', 'required');
            if ($this->form_validation->run() != FALSE) {
                $slug_str = $this->input->post('name[0]');
                $slug     = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data     = array(
                    'parent' => $this->input->post('parent'),
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'description' => $this->input->post('description'),
                    'meta_title' => $this->input->post('meta_title'),
                    'meta_key' => $this->input->post('meta_key'),
                    'meta_description' => $this->input->post('meta_description')
                );
                $record_id = $this->master_model->setUpdateData($this->tbl_name, $data, $rec_id);
                
                if ($record_id > 0)
                {
                    if ($_FILES["featured_img"]["name"] != "")
                    {
                        $featured_img      = do_upload('services');
                        $data_featured_img = array('featured_img' => $featured_img);
                        $this->master_model->setUpdateData($this->tbl_name, $data_featured_img, $record_id);
                    } 

                    $this->session->set_flashdata('notification', array('error' => 0,'message' => 'Record has been modified successfully !!'));
                    redirect('admin/services', 'refresh');
                }
            }
        }
        $data['parentOptions'] = getMultilevelOptions($this->tbl_name, array('status'=>1), '---Self---');
        $data['record'] = $this->master_model->get_recordArr($this->tbl_name, array('id' => $rec_id));
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/edit', $data);
        $this->load->view('admin/include/footer', $data);
    }

    public function delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        $this->master_model->DeleteByID($this->tbl_name, $id);
        $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been delete successfully !!'));
        redirect('admin/services', 'refresh');
    }
    
    public function status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
        $data['status'] = (empty($statusId) || $statusId == 0) ? 1 : 0;
        $rec_id         = $this->master_model->setUpdateData($this->tbl_name, $data, $id);
        if ($rec_id > 0)
        {
            $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Status has been updated successfully !!'));
            redirect('admin/services', 'refresh');
        }
    }
    
    
    public function prices()
    {
        CheckAdminLoginSession();
        $data['servicesData'] = $this->services_model->getServiesPriceDataCollection();
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/services_prices', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function service_provider_request()
    {
        CheckAdminLoginSession();
        $data           = array();
        $limit_per_page = $this->config->item('adm_limits_per_page');
        $page           = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        $start_index    = $page * $limit_per_page;
        $total_records  = $this->services_model->getServiesProviderRequestCounts();
        if ($total_records > 0) {
            $data["servicesData"] = $this->services_model->getServiesProviderRequestDataCollection($limit_per_page, $start_index);
            $data["paginations"]  = Jpagination($total_records, $limit_per_page, base_url('admin/services/service_provider_request'));
        }
        //$data['servicesData']=$this->services_model->getServiesProviderRequestDataCollection();
        
        /*
        $serviceProvider = serviceProvidersList();
        $serviceProviderOptions = array('---Choose Service Provider---');
        if(!empty($serviceProvider))
        {
        foreach ($serviceProvider as $row)
        {
        $sp_details = '';
        if(!empty($row->username) && !empty($row->business_name))
        {
        $sp_details = $row->username.' ('.$row->business_name.')';
        }
        elseif(!empty($row->username) && empty($row->business_name))
        {
        $sp_details = $row->username;
        }
        
        $serviceProviderOptions[$row->id] = $sp_details;
        }
        }
        
        $data["serviceProviderOptions"] = $serviceProviderOptions;*/
        //$data["durationOptions"] = array(0=>"----Statement Duration----","weekly"=>"Weekly","bi-monthly"=>"Bi-Monthly","monthly"=>"Monthly","quaterly"=>"Quaterly","other"=>"Others");
        $data["durationOptions"] = array(
            0 => "----Statement Duration----",
            "weekly" => "Weekly",
            "monthly" => "Monthly"
        );
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/service_provider_request', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function ajax_currency_symbol()
    {
        $currency_id    = $this->input->post('currency_id');
        $currencySymbol = !empty($currency_id) ? getCurrencySymbol($currency_id) : getCurrencySymbol();
        echo $currencySymbol;
    }
    
    public function ajax_serviceprovider_info()
    {
        $serviceProviderId = $this->input->post('serviceProviderId');
        $details           = $this->services_model->getServiesProviderDetails($serviceProviderId);
        $message           = '';
        $message .= '<td align="center" colspan="2"><table><tr><td>';
        
        if (!empty($details->featured_img)) {
            $img = base_url($details->featured_img);
        } else {
            $img = base_url('skin/admin/images/profile.jpg');
        }
        
        $message .= '<img style="height:55px;width:55px;border: 1px solid #664f5c;" alt="64x64" src="' . $img . '" class="img-circle">';
        $message .= '</td><td><address>';
        
        $name = '';
        if (!empty($details->business) && $details->business == 'yes' && !empty($details->business_name)) {
            $name = $details->business_name;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->username)) {
            $name = $details->username;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->fname)) {
            $name = $details->fname . ' ' . $details->lname;
        } else {
            $name = '';
        }
        $message .= '<strong>' . $name . '</strong><br>';
        $email = !empty($details->business_email) ? $details->business_email : $details->email;
        $message .= '<abbr title="Work email">E-mail:</abbr> ' . $email . '<br>';
        $mobile = !empty($details->business_mobile) ? $details->business_mobile : $details->mobile;
        $message .= '<abbr title="Work Phone">Phone:</abbr> ' . $mobile . '<br>';
        $message .= '</address></td></tr></table></td></tr>';
        echo $message;
    }
    
    public function ajax_find_statements()
    {
        //$durationArr = array("weekly"=>"Weekly","bi-monthly"=>"Bi-Monthly","monthly"=>"Monthly","quaterly"=>"Quaterly");
        $durationArr        = array(
            "weekly" => "Weekly",
            "monthly" => "Monthly"
        );
        $postdata           = $this->input->post();
        $whereArr['status'] = 5;
        
        if (!empty($postdata['service_provider_id'])) {
            $whereArr['vendor_id'] = $postdata['service_provider_id'];
            $providerSettings      = getServiceProviderSettingsByProviderId($postdata['service_provider_id']);
        }
        
        $code = !empty($providerSettings->code) ? $providerSettings->code : getCurrencySign();
        
        if (!empty($postdata['duration']) && $postdata['duration'] == 'weekly') {
            $start_date = strtotime("-7 day", time());
            $end_date   = time();
            
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'bi-monthly') {
            $start_date = strtotime("-15 day", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'monthly') {
            $start_date = strtotime("-1 month", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'quaterly') {
            $start_date = strtotime("-3 month", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'other') {
            $start_date = !empty($postdata['from']) ? strtotime($postdata['from']) : '';
            $end_date   = !empty($postdata['to']) ? strtotime($postdata['to']) : '';
        }
        
        $start = date("Y-m-d", $start_date);
        $end   = date("Y-m-d", $end_date);
        
        $records = get_records($this->service_orders, $whereArr);
        $output  = '<hr>';
        
        if (!empty($start) && !empty($end)) {
            $d             = $postdata['duration'];
            $durationLabel = (!empty($d) && array_key_exists($d, $durationArr)) ? '(' . $durationArr[$d] . ')' : '';
            $output .= '<table class="table table-popuped">';
            $output .= '<tr>';
            $output .= '<th colspan="2" align="center"><strong> Service Provider Financial Statement ' . $durationLabel . ' </strong></th>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<td width="50%"><strong> From :  </strong>' . $start . '</td>';
            $output .= '<td><strong> To :  </strong>' . $end . '</td>';
            $output .= '</tr>';
            $output .= '</table>';
        }
        
        $output .= '<table class="table table-popuped">
              <thead>
                <tr>
                  <th>Booking Id</th>
                  <th>Service Date</th>
                  <th>Min. Charges</th>
                  <th>Add. Charges</th>
                  <th>Sub Total</th> 
                  <th>H.O. Charges</th>
                  <th>Total</th>                  
                </tr>
              </thead>
              <tbody>';
        
        $sub_total   = 0;
        $ho_total    = 0;
        $grand_total = 0;
        if (!empty($records)) {
            foreach ($records as $rec) {
                $operation         = get_record($this->service_operations, array(
                    'service_order_id' => $rec->id
                ));
                //print_r($operation);
                $amount            = (float) $rec->total_amount;
                $additional_amount = (float) $operation->additional_subtotal;
                $total             = $amount + $additional_amount;
                
                
                $ho_fees             = $providerSettings->ho_fees;
                $ho_per_order_charge = $providerSettings->ho_per_order_charge;
                $ho_fee_charge       = $ho_fees / 100 * $total;
                $ho_fee_charge2      = (!empty($ho_fee_charge) && $ho_fee_charge < $ho_per_order_charge) ? $ho_per_order_charge : $ho_fee_charge;
                
                $gtotal = $total - $ho_fee_charge2;
                
                $output .= '<tr>
                        <td>' . $rec->id . '</td>
                        <td>' . $rec->booking_datetime . '</td>
                        <td>' . getFormatedPriceByCode($code, $amount) . '</td>
                        <td>' . getFormatedPriceByCode($code, $additional_amount) . '</td>                        
                        <td>' . getFormatedPriceByCode($code, $total) . '</td>
                        <td>' . getFormatedPriceByCode($code, $ho_fee_charge2) . '</td>
                        <td>' . getFormatedPriceByCode($code, $gtotal) . '</td>
                      </tr>';
                $sub_total   = $sub_total + $total;
                $ho_total    = $ho_total + $ho_fee_charge2;
                $grand_total = $grand_total + $gtotal;
            }
        } else {
            $output .= '<tr><td colspan="6">No Service records found in this duration for this service provider.</td></tr>';
        }
        
        $output .= '</tbody>
            </table>';
        
        if (!empty($records) && !empty($total)) {
            $ho_fix_weekly_charges        = $providerSettings->ho_fix_weekly_charges;
            $ho_other_charge              = $providerSettings->ho_other_charge;
            $ho_credit_debit_card_charges = $providerSettings->ho_credit_debit_card_charges;
            
            $output .= '<table class="table table-popuped">';
            $output .= '<tr>';
            $output .= '<th width="50%"><strong> Sub Total : </strong></th>';
            $output .= '<td align="left"><strong>' . getFormatedPriceByCode($code, $sub_total) . '</strong></td>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<th width="50%"><strong> Total H.O Charges (-) : </strong></th>';
            $output .= '<td align="left"><strong>' . getFormatedPriceByCode($code, $ho_total) . '</strong></td>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<th width="50%"><strong> Grand Total : </strong></th>';
            $output .= '<td align="left"><strong>' . getFormatedPriceByCode($code, $grand_total) . '</strong></td>';
            $output .= '</tr>';
            $output .= '</table>';
        }
        echo $output;
    }
    
    public function invoices()
    {
        CheckAdminLoginSession();
        $getdata = $this->input->get();
        
        $whereArr = array();
        
        if (!empty($getdata['service_provider_id'])) {
            $whereArr['user_id'] = $data['service_provider_id'] = $getdata['service_provider_id'];
        }
        
        if (!empty($getdata['invoiceId'])) {
            $whereArr['id'] = $data['invoiceId'] = $getdata['invoiceId'];
        }
        
        $data['records'] = $records = get_records($this->invoice, $whereArr);
        
        $data["serviceProviderOptions"] = serviceProviderOptions();
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/invoices-list', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function mark_invoice_paid()
    {
        CheckAdminLoginSession();
        $id       = $this->uri->segment(4);
        $pay_time = date('d-m-Y H:i:s');
        $this->services_model->setUpdateData($this->invoice, array(
            'is_paid' => 1,
            'paid_date' => $pay_time
        ), $id);
        $this->session->set_flashdata('message', 'Invoiced #' . $id . ' has marked as PAID successfully');
        redirect('admin/services/invoices', 'refresh');
    }
    
    public function download_invoice()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        if (!empty($id)) {
            $filename  = getInvoiceFile($id);
            $base_path = $this->config->item('base_path');
            $filePath  = $base_path . 'upload/invoices/' . $filename;
            force_download($filePath, NULL);
        }
    }
    
    
    public function resend_invoice()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        if (!empty($id)) {
            $invoiceData = getInvoiceDeatilsByID($id);
            
            $invoiceId           = !empty($invoiceData->id) ? $invoiceData->id : 0;
            $service_provider_id = !empty($invoiceData->user_id) ? $invoiceData->user_id : 0;
            $durationLabel       = !empty($invoiceData->duration) ? getInvoiceDuration($invoiceData->duration) : '';
            $start               = !empty($invoiceData->start) ? $invoiceData->start : '';
            $end                 = !empty($invoiceData->end) ? $invoiceData->end : '';
            $invoice_filename    = !empty($invoiceData->invoice_filename) ? $invoiceData->invoice_filename : '';
            
            //send_email_with_attachment
            $email_template = 'resend_sp_invoice.html';
            $templateTags   = array(
                '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
                '{{site_name}}' => 'firstchoice.com',
                '{{site_url}}' => base_url(),
                '{{user_name}}' => getServiesProviderName($service_provider_id),
                '{{invoice_id}}' => $invoiceId,
                '{{duration}}' => $durationLabel,
                '{{start}}' => $start,
                '{{end}}' => $end,
                '{{team_name}}' => 'firstchoice',
                '{{year}}' => date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com'
            );
            $message        = email_compose($email_template, $templateTags);
            $email          = getServiesProviderEmail($service_provider_id);
            
            $filePath = '';
            if (!empty($invoice_filename)) {
                $base_path = $this->config->item('base_path');
                $filePath  = $base_path . 'upload/invoices/' . $invoice_filename;
            }
            
            send_email_with_attachment($email, 'Invoiced for Service Provider', $message, array(
                $filePath
            ));
            $this->session->set_flashdata('message', 'Invoice has send Successfully');
            redirect('admin/services/invoices', 'refresh');
        }
    }
    
    
    public function createinvoice()
    {
        CheckAdminLoginSession();
        $getdata = $this->input->get();
        
        $data['service_provider_id'] = $service_provider_id = !empty($getdata['service_provider_id']) ? $getdata['service_provider_id'] : 0;
        $data['duration']            = $duration = !empty($getdata['duration']) ? $getdata['duration'] : '';
        $data['img']                 = getServiesProviderPic($service_provider_id);
        $data['name']                = getServiesProviderName($service_provider_id);
        $data['email']               = getServiesProviderEmail($service_provider_id);
        $data['mobile']              = getServiesProviderContact($service_provider_id);
        $providerSettings            = !empty($service_provider_id) ? getServiceProviderSettingsByProviderId($service_provider_id) : '';
        
        $currency_id = !empty($providerSettings->currency) ? $providerSettings->currency : getCurrency();
        $code        = getCurrencySign($currency_id);
        
        $dates                 = getDatesFromDuration($duration);
        $data['start']         = $start = $dates['start'];
        $data['end']           = $end = $dates['end'];
        $data['durationLabel'] = $durationLabel = getInvoiceDuration($duration);
        
        $ho_fees                      = !empty($providerSettings->ho_fees) ? $providerSettings->ho_fees : 0;
        $ho_per_order_charge          = !empty($providerSettings->ho_per_order_charge) ? $providerSettings->ho_per_order_charge : 0;
        $ho_advertising_charges       = !empty($providerSettings->ho_advertising) ? $providerSettings->ho_advertising : 0;
        $ho_fix_weekly_charges        = !empty($providerSettings->ho_fix_weekly_charges) ? $providerSettings->ho_fix_weekly_charges : 0;
        $ho_other_charge              = !empty($providerSettings->ho_other_charge) ? $providerSettings->ho_other_charge : 0;
        $ho_credit_debit_card_charges = !empty($providerSettings->ho_credit_debit_card_charges) ? $providerSettings->ho_credit_debit_card_charges : 0;
        
        $dataArr     = array();
        $sub_total   = 0;
        $ho_total    = 0;
        $grand_total = 0;
        
        //$recordsOrd = getServiceOrdersForInvoice($service_provider_id,$start,$end);
        //echo "<pre>"; print_r($recordsOrd); echo "</pre>";
        //die;
        
        $whereArr['status']      = 5;
        $whereArr['is_invoiced'] = 0;
        if (!empty($service_provider_id)) {
            $whereArr['vendor_id'] = $service_provider_id;
        }
        
        $orders = get_records($this->service_orders, $whereArr);
        if (!empty($orders)) {
            foreach ($orders as $rec) {
                $invoiceItem       = array();
                $operation         = get_record($this->service_operations, array(
                    'service_order_id' => $rec->id
                ));
                $amount            = (float) $rec->total_amount;
                $additional_amount = (float) $operation->additional_subtotal;
                $total             = $amount + $additional_amount;
                
                $ho_per_order_charge              = !empty($ho_per_order_charge) ? $ho_per_order_charge : $ho_per_order_charge;
                $invoiceItem['id']                = $rec->id;
                $invoiceItem['booking_datetime']  = $rec->booking_datetime;
                $invoiceItem['amount']            = getFormatedPriceByCode($code, $amount);
                $invoiceItem['additional_amount'] = getFormatedPriceByCode($code, $additional_amount);
                $invoiceItem['total']             = getFormatedPriceByCode($code, $total);
                $sub_total                        = $sub_total + $total;
                $ho_total                         = $ho_total + $ho_per_order_charge;
                array_push($dataArr, $invoiceItem);
            }
        }
        
        $data['records'] = $dataArr;
        
        $ho_fee_charge  = !empty($ho_fees) ? $ho_fees / 100 * $sub_total : 0;
        $ho_adv_charge  = !empty($ho_advertising_charges) ? $ho_advertising_charges / 100 * $sub_total : 0;
        $ho_card_charge = !empty($ho_credit_debit_card_charges) ? $ho_credit_debit_card_charges / 100 * $sub_total : 0;
        
        $ho_weekly_charges = 0;
        if (!empty($duration) && $duration == 'bi-monthly') {
            $ho_weekly_charges = 2 * $ho_fix_weekly_charges;
        } elseif (!empty($duration) && $duration == 'monthly') {
            $ho_weekly_charges = 4 * $ho_fix_weekly_charges;
        } else {
            $ho_weekly_charges = $ho_fix_weekly_charges;
        }
        
        $ho_total    = $ho_total + $ho_fee_charge + $ho_weekly_charges + $ho_adv_charge + $ho_other_charge + $ho_card_charge;
        $grand_total = $sub_total - $ho_total;
        if (!empty($orders) && !empty($total)) {
            $data['sub_total']   = getFormatedPriceByCode($code, $sub_total);
            $data['ho_total']    = getFormatedPriceByCode($code, $ho_total);
            $data['grand_total'] = getFormatedPriceByCode($code, $grand_total);
        }
        
        $data["serviceProviderOptions"] = serviceProviderOptions();
        $data["durationOptions"]        = invoiceDurationOptions();
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/create-invoice', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function invoice22()
    {
        //$durationArr = array("weekly"=>"Weekly","bi-monthly"=>"Bi-Monthly","monthly"=>"Monthly","quaterly"=>"Quaterly");
        $durationArr = array(
            "weekly" => "Weekly",
            "monthly" => "Monthly"
        );
        $postdata    = $this->input->get();
        
        $data['service_provider_id'] = $service_provider_id = !empty($postdata['service_provider_id']) ? $postdata['service_provider_id'] : 0;
        $details                     = $this->services_model->getServiesProviderDetails($service_provider_id);
        
        if (!empty($details->featured_img)) {
            $img = base_url($details->featured_img);
        } else {
            $img = base_url('skin/admin/images/profile.jpg');
        }
        $data['img'] = $img;
        
        //print_r($details);
        
        if (!empty($details->business) && $details->business == 'yes' && !empty($details->business_name)) {
            $name = $details->business_name;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->username)) {
            $name = $details->username;
        } elseif ((empty($details->business) || $details->business == 'no') && !empty($details->fname)) {
            $name = $details->fname . ' ' . $details->lname;
        } else {
            $name = '';
        }
        
        $data['name']   = !empty($name) ? $name : '';
        $data['email']  = !empty($details->business_email) ? $details->business_email : $details->email;
        $data['mobile'] = !empty($details->business_mobile) ? $details->business_mobile : $details->mobile;
        
        $whereArr['status'] = 5;
        
        if (!empty($postdata['service_provider_id'])) {
            $whereArr['vendor_id'] = $postdata['service_provider_id'];
            $providerSettings      = getServiceProviderSettingsByProviderId($postdata['service_provider_id']);
        }
        
        $code             = !empty($providerSettings->code) ? $providerSettings->code : getCurrencySign();
        $data['duration'] = !empty($postdata['duration']) ? $postdata['duration'] : '';
        
        if (!empty($postdata['duration']) && $postdata['duration'] == 'weekly') {
            $start_date = strtotime("-7 day", time());
            $end_date   = time();
            
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'bi-monthly') {
            $start_date = strtotime("-15 day", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'monthly') {
            $start_date = strtotime("-1 month", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'quaterly') {
            $start_date = strtotime("-3 month", time());
            $end_date   = time();
        } elseif (!empty($postdata['duration']) && $postdata['duration'] == 'other') {
            $start_date = !empty($postdata['from']) ? strtotime($postdata['from']) : '';
            $end_date   = !empty($postdata['to']) ? strtotime($postdata['to']) : '';
        }
        
        $data['start'] = $start = !empty($start_date) ? date("Y-m-d", $start_date) : date("Y-m-d");
        $data['end']   = $end = !empty($end_date) ? date("Y-m-d", $end_date) : date("Y-m-d");
        
        $orders = get_records($this->service_orders, $whereArr);
        
        if (!empty($start) && !empty($end)) {
            $d                     = !empty($postdata['duration']) ? $postdata['duration'] : '';
            $data['durationLabel'] = $durationLabel = (!empty($d) && array_key_exists($d, $durationArr)) ? $durationArr[$d] : '';
        }
        
        
        //echo "<pre>"; print_r($data); echo "</pre>";
        
        $sub_total   = 0;
        $ho_total    = 0;
        $grand_total = 0;
        
        $dataArr = array();
        if (!empty($orders)) {
            foreach ($orders as $rec) {
                $invoiceItem       = array();
                $operation         = get_record($this->service_operations, array(
                    'service_order_id' => $rec->id
                ));
                $amount            = (float) $rec->total_amount;
                $additional_amount = (float) $operation->additional_subtotal;
                $total             = $amount + $additional_amount;
                
                $ho_fees             = !empty($providerSettings->ho_fees) ? $providerSettings->ho_fees : 0;
                $ho_per_order_charge = !empty($providerSettings->ho_per_order_charge) ? $providerSettings->ho_per_order_charge : 0;
                $ho_fee_charge       = !empty($ho_fees) ? $ho_fees / 100 * $total : 0;
                $ho_fee_charge2      = (!empty($ho_fee_charge) && $ho_fee_charge < $ho_per_order_charge) ? $ho_per_order_charge : $ho_fee_charge;
                $gtotal              = $total - $ho_fee_charge2;
                
                $invoiceItem['id']                = $rec->id;
                $invoiceItem['booking_datetime']  = $rec->booking_datetime;
                $invoiceItem['amount']            = getFormatedPriceByCode($code, $amount);
                $invoiceItem['additional_amount'] = getFormatedPriceByCode($code, $additional_amount);
                $invoiceItem['total']             = getFormatedPriceByCode($code, $total);
                $invoiceItem['ho_charge']         = getFormatedPriceByCode($code, $ho_fee_charge2);
                $invoiceItem['gtotal']            = getFormatedPriceByCode($code, $gtotal);
                
                $sub_total   = $sub_total + $total;
                $ho_total    = $ho_total + $ho_fee_charge2;
                $grand_total = $grand_total + $gtotal;
                
                array_push($dataArr, $invoiceItem);
            }
        }
        
        $data['records'] = $dataArr;
        
        if (!empty($orders) && !empty($total)) {
            $ho_fix_weekly_charges        = !empty($providerSettings->ho_fix_weekly_charges) ? $providerSettings->ho_fix_weekly_charges : 0;
            $ho_other_charge              = !empty($providerSettings->ho_other_charge) ? $providerSettings->ho_other_charge : 0;
            $ho_credit_debit_card_charges = !empty($providerSettings->ho_credit_debit_card_charges) ? $providerSettings->ho_credit_debit_card_charges : 0;
            
            $data['sub_total']   = getFormatedPriceByCode($code, $sub_total);
            $data['ho_total']    = getFormatedPriceByCode($code, $ho_total);
            $data['grand_total'] = getFormatedPriceByCode($code, $grand_total);
        }
        
        $data["serviceProviderOptions"] = serviceProviderOptions();
        $data["durationOptions"]        = $durationArr; //array("weekly"=>"Weekly","bi-monthly"=>"Bi-Monthly","monthly"=>"Monthly","quaterly"=>"Quaterly");
        
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/invoice-report', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function export_invoice()
    {
        CheckAdminLoginSession();
        $this->load->library('pdf');
        $getdata                     = $this->input->get();
        $data['generated_date']      = $generated_date = date('d-m-Y h:i A');
        $data['service_provider_id'] = $service_provider_id = !empty($getdata['service_provider_id']) ? $getdata['service_provider_id'] : 0;
        $data['duration']            = $duration = !empty($getdata['duration']) ? $getdata['duration'] : '';
        $data['img']                 = getServiesProviderPic($service_provider_id);
        $data['name']                = getServiesProviderName($service_provider_id);
        $data['email']               = getServiesProviderEmail($service_provider_id);
        $data['mobile']              = getServiesProviderContact($service_provider_id);
        $data['address']             = getServiesProviderAddress($service_provider_id);
        $providerSettings            = !empty($service_provider_id) ? getServiceProviderSettingsByProviderId($service_provider_id) : '';
        
        $currency_id = !empty($providerSettings->currency) ? $providerSettings->currency : getCurrency();
        $code        = getCurrencySign($currency_id);
        
        $dates                 = getDatesFromDuration($duration);
        $data['start']         = $start = $dates['start'];
        $data['end']           = $end = $dates['end'];
        $data['durationLabel'] = $durationLabel = getInvoiceDuration($duration);
        
        $ho_fees                      = !empty($providerSettings->ho_fees) ? $providerSettings->ho_fees : 0;
        $ho_per_order_charge          = !empty($providerSettings->ho_per_order_charge) ? $providerSettings->ho_per_order_charge : 0;
        $ho_advertising_charges       = !empty($providerSettings->ho_advertising) ? $providerSettings->ho_advertising : 0;
        $ho_fix_weekly_charges        = !empty($providerSettings->ho_fix_weekly_charges) ? $providerSettings->ho_fix_weekly_charges : 0;
        $ho_other_charge              = !empty($providerSettings->ho_other_charge) ? $providerSettings->ho_other_charge : 0;
        $ho_credit_debit_card_charges = !empty($providerSettings->ho_credit_debit_card_charges) ? $providerSettings->ho_credit_debit_card_charges : 0;
        
        $dataArr     = array();
        $sub_total   = 0;
        $ho_total    = 0;
        $grand_total = 0;
        
        $whereArr['status']      = 5;
        $whereArr['is_invoiced'] = 0;
        if (!empty($service_provider_id)) {
            $whereArr['vendor_id'] = $service_provider_id;
        }
        
        $orders = get_records($this->service_orders, $whereArr);
        if (!empty($orders)) {
            foreach ($orders as $rec) {
                $invoiceItem       = array();
                $operation         = get_record($this->service_operations, array(
                    'service_order_id' => $rec->id
                ));
                $amount            = (float) $rec->total_amount;
                $additional_amount = (float) $operation->additional_subtotal;
                $total             = $amount + $additional_amount;
                
                $ho_per_order_charge              = !empty($ho_per_order_charge) ? $ho_per_order_charge : $ho_per_order_charge;
                $invoiceItem['id']                = $rec->id;
                $invoiceItem['booking_datetime']  = $rec->booking_datetime;
                $invoiceItem['amount']            = getFormatedPriceByCode($code, $amount);
                $invoiceItem['additional_amount'] = getFormatedPriceByCode($code, $additional_amount);
                $invoiceItem['total']             = getFormatedPriceByCode($code, $total);
                $sub_total                        = $sub_total + $total;
                $ho_total                         = $ho_total + $ho_per_order_charge;
                array_push($dataArr, $invoiceItem);
            }
        }
        
        $data['records'] = $dataArr;
        
        $ho_fee_charge  = !empty($ho_fees) ? $ho_fees / 100 * $sub_total : 0;
        $ho_adv_charge  = !empty($ho_advertising_charges) ? $ho_advertising_charges / 100 * $sub_total : 0;
        $ho_card_charge = !empty($ho_credit_debit_card_charges) ? $ho_credit_debit_card_charges / 100 * $sub_total : 0;
        
        $ho_weekly_charges = 0;
        if (!empty($duration) && $duration == 'bi-monthly') {
            $ho_weekly_charges = 2 * $ho_fix_weekly_charges;
        } elseif (!empty($duration) && $duration == 'monthly') {
            $ho_weekly_charges = 4 * $ho_fix_weekly_charges;
        } else {
            $ho_weekly_charges = $ho_fix_weekly_charges;
        }
        
        $ho_total    = $ho_total + $ho_fee_charge + $ho_weekly_charges + $ho_adv_charge + $ho_other_charge + $ho_card_charge;
        $grand_total = $sub_total - $ho_total;
        
        if (!empty($orders) && !empty($total)) {
            $data['sub_total']   = getFormatedPriceByCode($code, $sub_total);
            $data['ho_total']    = getFormatedPriceByCode($code, $ho_total);
            $data['grand_total'] = getFormatedPriceByCode($code, $grand_total);
        }
        
        $data['notes'] = $notes = !empty($getdata['notes']) ? $getdata['notes'] : '';
        
        $html = $this->pdf->load_view('admin/services/download_invoice', $data);
        
        //this the the PDF filename that user will get to download
        $filename = "fce-" . $service_provider_id . '-invoice-' . time() . ".pdf";
        //$this->pdf->download_pdf('admin/services/download_invoice', $data, $filename);
    }
    
    public function send_invoice()
    {
        $this->load->library('pdf');
        CheckAdminLoginSession();
        $getdata                     = $this->input->get();
        $data['generated_date']      = $generated_date = date('d-m-Y h:i A');
        $data['service_provider_id'] = $service_provider_id = !empty($getdata['service_provider_id']) ? $getdata['service_provider_id'] : 0;
        $data['duration']            = $duration = !empty($getdata['duration']) ? $getdata['duration'] : '';
        $data['img']                 = getServiesProviderPic($service_provider_id);
        $data['name']                = getServiesProviderName($service_provider_id);
        $data['email']               = getServiesProviderEmail($service_provider_id);
        $data['mobile']              = getServiesProviderContact($service_provider_id);
        $data['address']             = getServiesProviderAddress($service_provider_id);
        $providerSettings            = !empty($service_provider_id) ? getServiceProviderSettingsByProviderId($service_provider_id) : '';
        
        $currency_id = !empty($providerSettings->currency) ? $providerSettings->currency : getCurrency();
        $code        = getCurrencySign($currency_id);
        
        $dates                 = getDatesFromDuration($duration);
        $data['start']         = $start = $dates['start'];
        $data['end']           = $end = $dates['end'];
        $data['durationLabel'] = $durationLabel = getInvoiceDuration($duration);
        
        $ho_fees                      = !empty($providerSettings->ho_fees) ? $providerSettings->ho_fees : 0;
        $ho_per_order_charge          = !empty($providerSettings->ho_per_order_charge) ? $providerSettings->ho_per_order_charge : 0;
        $ho_advertising_charges       = !empty($providerSettings->ho_advertising) ? $providerSettings->ho_advertising : 0;
        $ho_fix_weekly_charges        = !empty($providerSettings->ho_fix_weekly_charges) ? $providerSettings->ho_fix_weekly_charges : 0;
        $ho_other_charge              = !empty($providerSettings->ho_other_charge) ? $providerSettings->ho_other_charge : 0;
        $ho_credit_debit_card_charges = !empty($providerSettings->ho_credit_debit_card_charges) ? $providerSettings->ho_credit_debit_card_charges : 0;
        
        $dataArr     = array();
        $sub_total   = 0;
        $ho_total    = 0;
        $grand_total = 0;
        
        $whereArr['status']      = 5;
        $whereArr['is_invoiced'] = 0;
        if (!empty($service_provider_id)) {
            $whereArr['vendor_id'] = $service_provider_id;
        }
        
        $orders = get_records($this->service_orders, $whereArr);
        
        $bookingIds = array();
        if (!empty($orders)) {
            foreach ($orders as $rec) {
                $invoiceItem       = array();
                $operation         = get_record($this->service_operations, array(
                    'service_order_id' => $rec->id
                ));
                $amount            = (float) $rec->total_amount;
                $additional_amount = (float) $operation->additional_subtotal;
                $total             = $amount + $additional_amount;
                
                $ho_per_order_charge              = !empty($ho_per_order_charge) ? $ho_per_order_charge : $ho_per_order_charge;
                $invoiceItem['id']                = $rec->id;
                $invoiceItem['booking_datetime']  = $rec->booking_datetime;
                $invoiceItem['amount']            = getFormatedPriceByCode($code, $amount);
                $invoiceItem['additional_amount'] = getFormatedPriceByCode($code, $additional_amount);
                $invoiceItem['total']             = getFormatedPriceByCode($code, $total);
                $sub_total                        = $sub_total + $total;
                $ho_total                         = $ho_total + $ho_per_order_charge;
                array_push($dataArr, $invoiceItem);
                array_push($bookingIds, $rec->id);
            }
        }
        
        $data['records'] = $dataArr;
        
        $ho_fee_charge  = !empty($ho_fees) ? $ho_fees / 100 * $sub_total : 0;
        $ho_adv_charge  = !empty($ho_advertising_charges) ? $ho_advertising_charges / 100 * $sub_total : 0;
        $ho_card_charge = !empty($ho_credit_debit_card_charges) ? $ho_credit_debit_card_charges / 100 * $sub_total : 0;
        
        $ho_weekly_charges = 0;
        if (!empty($duration) && $duration == 'bi-monthly') {
            $ho_weekly_charges = 2 * $ho_fix_weekly_charges;
        } elseif (!empty($duration) && $duration == 'monthly') {
            $ho_weekly_charges = 4 * $ho_fix_weekly_charges;
        } else {
            $ho_weekly_charges = $ho_fix_weekly_charges;
        }
        
        $ho_total    = $ho_total + $ho_fee_charge + $ho_weekly_charges + $ho_adv_charge + $ho_other_charge + $ho_card_charge;
        $grand_total = $sub_total - $ho_total;
        
        if (!empty($orders) && !empty($total)) {
            $data['sub_total']   = getFormatedPriceByCode($code, $sub_total);
            $data['ho_total']    = getFormatedPriceByCode($code, $ho_total);
            $data['grand_total'] = getFormatedPriceByCode($code, $grand_total);
        }
        
        
        $data['notes'] = $notes = !empty($getdata['notes']) ? $getdata['notes'] : '';
        
        //$html = $this->pdf->load_view('admin/services/download_invoice', $data);
        
        //this the the PDF filename that user will get to download
        $filename = "fce-" . $service_provider_id . '-invoice' . time() . ".pdf";
        
        $this->pdf->save_pdf('admin/services/download_invoice', $data, $filename, 'invoices');
        
        $bookingIdsStr = implode(",", $bookingIds);
        
        $invoiceArr['user_id']             = $service_provider_id;
        $invoiceArr['user_role']           = 5;
        $invoiceArr['currency_code']       = $code;
        $invoiceArr['duration']            = $duration;
        $invoiceArr['start']               = $start;
        $invoiceArr['end']                 = $end;
        $invoiceArr['service_booking_ids'] = $bookingIdsStr;
        $invoiceArr['total_amount']        = $sub_total;
        $invoiceArr['ho_charge']           = $ho_total;
        ;
        $invoiceArr['payable_amount']   = $grand_total;
        $invoiceArr['notes']            = $notes;
        $invoiceArr['invoice_filename'] = $filename;
        
        
        //echo "<pre>"; print_r($invoiceArr); echo "</pre>"; die;
        $base_path = $this->config->item('base_path');
        
        $filePath = $base_path . 'upload/invoices/' . $filename;
        
        $invoiceId = $this->services_model->setInsertData($this->invoice, $invoiceArr);
        if ($invoiceId > 0) {
            if (!empty($bookingIds)) {
                foreach ($bookingIds as $oid) {
                    $this->services_model->setUpdateData($this->service_orders, array(
                        'is_invoiced' => 1,
                        'invoice_id' => $invoiceId
                    ), $oid);
                }
            }
            
            //send_email_with_attachment
            $email_template = 'send_sp_invoice.html';
            $templateTags   = array(
                '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
                '{{site_name}}' => 'firstchoice.com',
                '{{site_url}}' => base_url(),
                '{{user_name}}' => getServiesProviderName($service_provider_id),
                '{{invoice_id}}' => $invoiceId,
                '{{duration}}' => $durationLabel,
                '{{start}}' => $start,
                '{{end}}' => $end,
                '{{team_name}}' => 'firstchoice',
                '{{year}}' => date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com'
            );
            $message        = email_compose($email_template, $templateTags);
            $email          = getServiesProviderEmail($service_provider_id);
            
            send_email_with_attachment($email, 'Invoiced for Service Provider', $message, array(
                $filePath
            ));
            $this->session->set_flashdata('message', 'Invoice has generated Successfully');
        }
        
        //echo "<pre>"; print_r($invoiceArr); echo "</pre>";
        $return_url = base_url('admin/services/create-invoice') . '?service_provider_id=' . $service_provider_id . '&duration=' . $duration;
        redirect($return_url);
    }
    
    public function add_servicerequest()
    {
        CheckAdminLoginSession();
        $id        = $this->uri->segment(5);
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('start_date', 'start date', 'required');
            $this->form_validation->set_rules('operator_no', 'operator no', 'required');
            $this->form_validation->set_rules('services_type', 'services category', 'required');
            
            if ($this->form_validation->run() != FALSE) {
                //echo "<pre>"; print_r($post_data); echo "</pre>"; die;             
                $settings_id                       = $this->input->post('settings_id');
                $data['start_date']                = $this->input->post('start_date');
                $data['services_request_id']       = $id;
                $data['service_id']                = $this->input->post('services_type');
                $data['service_category_code']     = $this->input->post('service_category_code');
                $data['service_zip_code']          = $this->input->post('service_zip_code');
                $data['operator_no']               = $this->input->post('operator_no');
                $data['coupons']                   = $this->input->post('coupons');
                $data['discount']                  = $this->input->post('discount');
                $data['bonus_point']               = $this->input->post('bonus_point');
                $data['min_use_points']            = $this->input->post('min_use_points');
                $data['additional_charge']         = $this->input->post('additional_charge');
                $data['emergency_service_charge']  = $this->input->post('emergency_service_charge');
                $data['additional_worker']         = $this->input->post('additional_worker');
                $data['service_call']              = $this->input->post('service_call');
                $data['hr_charge']                 = $this->input->post('hr_charge');
                $data['horly_charge']              = $this->input->post('horly_charge');
                $data['advertising']               = $this->input->post('advertising');
                $data['fix_weekly_charges']        = $this->input->post('fix_weekly_charges');
                $data['other_charge']              = $this->input->post('other_charge');
                $data['credit_debit_card_charges'] = $this->input->post('credit_debit_card_charges');
                $data['area_service_pricing']      = $this->input->post('area_service_pricing');
                $data['taxes']                     = $this->input->post('taxes');
                $data['tax_name']                  = $this->input->post('tax_name');
                $data['other_info']                = $this->input->post('other_info');
                $data['currency']                  = $this->input->post('currency');
                $data['expiry_year']               = $this->input->post('expiry_year');
                $data['ho_fees']                   = $this->input->post('ho_fees');
                $data['per_order_charge']          = $this->input->post('per_order_charge');
                $data['after_call_work']           = $this->input->post('after_call_work');
                $data['after_hr_charge']           = $this->input->post('after_hr_charge');
                $data['after_horly_charge']        = $this->input->post('after_horly_charge');
                $data['after_additional_worker']   = $this->input->post('after_additional_worker');
                $data['after_other_charge']        = $this->input->post('after_other_charge');
                $catdata['services_type']          = $this->input->post('services_type');
                $insertId                          = $this->services_model->setInsertData($this->service_provider_settings, $data);
                if ($insertId > 0) {
                    $this->session->set_flashdata('message', 'Your services provider information has been added successfully');
                    redirect('admin/services/service_provider_records/' . $id, 'refresh');
                }
            }
        }
        $data['currencyOption']           = $this->admin_model->getCurrencyOptions($this->currencies, 'Select Currency');
        $data['servicestypeOptions']      = getServicesOptions('Select Sevices');
        $data['servicesprovidersOptions'] = getServiceRequestOptions('Select Sevice Provider');
        $data['servicesData']             = $this->services_model->getServiesProviderRequestDetails($id);
        
        
        //$data['servicesproviderData']=$this->services_model->getServiesProviderDetails($id);
        //$data['servicesSettings']=getServiceProviderSettings($id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/add_servicerequest', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function service_request_details()
    {
        CheckAdminLoginSession();
        $id                          = $this->uri->segment(4);
        $data                        = array();
        $data['currencyOption']      = $this->admin_model->getCurrencyOptions($this->currencies, 'Select Currency');
        $data['servicestypeOptions'] = getServicesOptions('Select Sevices');
        $data['servicesData']        = $this->services_model->getRequestDetails($id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/servicerequest_details', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit_servicerequest()
    {
        
        CheckAdminLoginSession();
        
        $spr_id    = $this->uri->segment(4);
        $id        = $this->uri->segment(6);
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('start_date', 'start date', 'required');
            $this->form_validation->set_rules('operator_no', 'operator no', 'required');
            $this->form_validation->set_rules('services_type', 'services category', 'required');
            
            if ($this->form_validation->run() == FALSE) {
            } else {
                
                $settings_id                       = $this->input->post('settings_id');
                $data['start_date']                = $this->input->post('start_date');
                $data['service_id']                = $this->input->post('services_type');
                $data['service_category_code']     = $this->input->post('service_category_code');
                $data['service_zip_code']          = $this->input->post('service_zip_code');
                $data['operator_no']               = $this->input->post('operator_no');
                $data['coupons']                   = $this->input->post('coupons');
                $data['discount']                  = $this->input->post('discount');
                $data['bonus_point']               = $this->input->post('bonus_point');
                $data['min_use_points']            = $this->input->post('min_use_points');
                $data['additional_charge']         = $this->input->post('additional_charge');
                $data['emergency_service_charge']  = $this->input->post('emergency_service_charge');
                $data['additional_worker']         = $this->input->post('additional_worker');
                $data['service_call']              = $this->input->post('service_call');
                $data['hr_charge']                 = $this->input->post('hr_charge');
                $data['horly_charge']              = $this->input->post('horly_charge');
                $data['advertising']               = $this->input->post('advertising');
                $data['fix_weekly_charges']        = $this->input->post('fix_weekly_charges');
                $data['other_charge']              = $this->input->post('other_charge');
                $data['credit_debit_card_charges'] = $this->input->post('credit_debit_card_charges');
                $data['area_service_pricing']      = $this->input->post('area_service_pricing');
                $data['taxes']                     = $this->input->post('taxes');
                $data['tax_name']                  = $this->input->post('tax_name');
                $data['other_info']                = $this->input->post('other_info');
                $data['currency']                  = $this->input->post('currency');
                $data['expiry_year']               = $this->input->post('expiry_year');
                $data['ho_fees']                   = $this->input->post('ho_fees');
                $data['per_order_charge']          = $this->input->post('per_order_charge');
                $data['after_call_work']           = $this->input->post('after_call_work');
                $data['after_hr_charge']           = $this->input->post('after_hr_charge');
                $data['after_horly_charge']        = $this->input->post('after_horly_charge');
                $data['after_additional_worker']   = $this->input->post('after_additional_worker');
                $data['after_other_charge']        = $this->input->post('after_other_charge');
                $catdata['services_type']          = $this->input->post('services_type');
                
                $updId = $this->services_model->setUpdateData($this->service_provider_settings, $data, $id);
                
                if ($updId > 0) {
                    $this->session->set_flashdata('message', 'Your services provider information has been update successfully');
                    redirect('admin/services/service_provider_records/' . $spr_id, 'refresh');
                }
            }
        }
        $data['currencyOption']      = $this->admin_model->getCurrencyOptions($this->currencies, 'Select Currency');
        $data['servicestypeOptions'] = getServicesOptions('Select Sevices');
        $data['servicesData']        = $this->services_model->getServiesProviderRequestDetails($spr_id);
        $data['servicesSettings']    = getServiceProviderSettingsById($id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/edit_servicerequest', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function service_provider_records($id)
    {
        CheckAdminLoginSession();
        $data['servicesData'] = $this->services_model->getServiesProviderServicesDataCollection($id);
        $data['id']           = $id;
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/service_provider_records', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function insertZipcodeWithRelatedInfo($dataArr)
    {
        
        $wherArr = array(
            'zipcode' => $dataArr['zipcode']
        );
        $isExist = $this->services_model->isExist($this->services_area, $wherArr);
        if (empty($isExist)) {
            $data['zipcode']    = $dataArr['zipcode'];
            $data['fas']        = substr($dataArr['zipcode'], 0, 3);
            $data['latitude']   = $dataArr['latitude'];
            $data['longitude']  = $dataArr['longitude'];
            $data['country']    = $dataArr['country'];
            $data['place_name'] = $dataArr['state'];
            $data['area']       = $dataArr['city'];
            $data['address']    = $dataArr['address'];
            $this->services_model->setInsertData($this->services_area, $data);
        }
    }
    
    public function addprovider()
    {
        
        CheckAdminLoginSession();
        
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $data['services_type']          = $this->input->post('chooseserices');
            $data['message']                = $this->input->post('message');
            $data['bussiness_holder_fname'] = $this->input->post('business_holder_fname');
            $data['bussiness_holder_lname'] = $this->input->post('business_holder_lname');
            $data['business']               = $this->input->post('business');
            $data['business_name']          = $this->input->post('business_name');
            $data['business_email']         = $this->input->post('business_email');
            $data['business_mobile']        = $this->input->post('mobbox');
            $data['business_zip']           = $this->input->post('service_zipcode');
            $data['business_type']          = $this->input->post('business_type');
            $data['license']                = $this->input->post('license');
            $data['license_details']        = $this->input->post('license_details');
            $data['request_date']           = date("Y-m-d h:i:s");
            $data['service_status']         = 2;
            $data['is_verified']            = 0;
            $data['user_id']                = 0;
            
            //echo "<pre>"; print_r($data); echo "</pre>"; die;
            $service_request_id = $this->services_model->setInsertData($this->service_provider_request, $data);
            if ($service_request_id > 0) {
                $data2['service_id']            = $this->input->post('chooseserices');
                $data2['services_request_id']   = $service_request_id;
                $data2['service_zip_code']      = $this->input->post('service_zipcode');
                $data2['service_category_code'] = $this->input->post('chooseserices');
                $settings_id                    = $this->services_model->setInsertData($this->service_provider_settings, $data2);
                
                $zipcode = $this->input->post('service_zipcode');
                $address = getAddressByZipcode($zipcode);
                if (!empty($address['zipcode'])) {
                    $this->insertZipcodeWithRelatedInfo($address);
                }
                
                $this->session->set_flashdata('notification', array(
                    'error' => 0,
                    'message' => 'Service provider has added successfully and welcome email has sent successfully.'
                ));
                
                $this->send_provider_thank_you();
                redirect('admin/services/service_provider_request', 'refresh');
            }
        }
        
        $data['serviceOptions'] = getServicesOptions('Select services');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/add_serviceprovider', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function send_provider_thank_you()
    {
        $email          = $this->input->post('business_email');
        $email_template = 'service_provider_thank_you.html';
        $templateTags   = array(
            '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
            '{{site_name}}' => 'firstchoice.com',
            '{{site_url}}' => base_url(),
            '{{team_name}}' => 'firstchoice',
            '{{year}}' => date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com'
        );
        $message        = email_compose($email_template, $templateTags);
        send_email($email, 'Thank You For Request', $message);
    }
    
    public function service_provider_settings()
    {
        
        CheckAdminLoginSession();
        
        $id        = $this->uri->segment(4);
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('start_date', 'start date', 'required');
            $this->form_validation->set_rules('services_type', 'services category', 'required');
            
            if ($this->form_validation->run() != FALSE) {
                $service_zip_code = $this->input->post('service_zip_code');
                $zipcodes         = explode(",", $service_zip_code);
                if (!empty($zipcodes)) {
                    foreach ($zipcodes as $zipcode) {
                        $address = getAddressByZipcode($zipcode);
                        if (!empty($address['zipcode'])) {
                            $this->insertZipcodeWithRelatedInfo($address);
                        }
                    }
                }
                
                $settings_id                   = $this->input->post('settings_id');
                $data['start_date']            = $this->input->post('start_date');
                $data['service_category_code'] = $this->input->post('service_category_code');
                $data['service_zip_code']      = $this->input->post('service_zip_code');
                $data['is_allowed_all_area']   = $this->input->post('is_allowed_all_area');
                //$data['operator_no']    =$this->input->post('operator_no');
                
                $data['ho_fees']                      = $this->input->post('ho_fees');
                $data['ho_per_order_charge']          = $this->input->post('ho_per_order_charge');
                $data['ho_advertising']               = $this->input->post('ho_advertising');
                $data['ho_fix_weekly_charges']        = $this->input->post('ho_fix_weekly_charges');
                $data['ho_other_charge']              = $this->input->post('ho_other_charge');
                $data['ho_credit_debit_card_charges'] = $this->input->post('ho_credit_debit_card_charges');
                
                $data['tax']            = $this->input->post('tax');
                $data['tax_name']       = $this->input->post('tax_name');
                $data['tax_other_info'] = $this->input->post('tax_other_info');
                
                $data['currency']                  = $this->input->post('currency');
                $data['service_charge']            = $this->input->post('service_charge');
                $data['service_duration']          = $this->input->post('service_duration');
                $data['service_charges_after']     = $this->input->post('service_charges_after');
                $data['service_duration_after']    = $this->input->post('service_duration_after');
                $data['service_additional_charge'] = $this->input->post('service_additional_charge');
                $data['service_additional_hours']  = $this->input->post('service_additional_hours');
                $data['service_hourly_charge']     = $this->input->post('service_hourly_charge');
                $data['service_additional_worker'] = $this->input->post('service_additional_worker');
                
                $data['weekend_charge']            = $this->input->post('weekend_charge');
                $data['weekend_duration']          = $this->input->post('weekend_duration');
                $data['weekend_charges_after']     = $this->input->post('weekend_charges_after');
                $data['weekend_duration_after']    = $this->input->post('weekend_duration_after');
                $data['weekend_additional_charge'] = $this->input->post('weekend_additional_charge');
                $data['weekend_additional_hours']  = $this->input->post('weekend_additional_hours');
                $data['weekend_hourly_charge']     = $this->input->post('weekend_hourly_charge');
                $data['weekend_additional_worker'] = $this->input->post('weekend_additional_worker');
                
                $data['bank_name']     = $this->input->post('bank_name');
                $data['bank_address']  = $this->input->post('bank_address');
                $data['bank_code']     = $this->input->post('bank_code');
                $data['account_no']    = $this->input->post('account_no');
                $data['swift_no']      = $this->input->post('swift_no');
                $data['transition_no'] = $this->input->post('transition_no');
                $data['account_type']  = $this->input->post('account_type');
                
                $catdata['services_type']  = $this->input->post('services_type');
                $catdata['driver_licence'] = $this->input->post('driver_licence');
                $catdata['car_made']       = $this->input->post('car_made');
                $catdata['car_model']      = $this->input->post('car_model');
                $catdata['car_year']       = $this->input->post('car_year');
                $catdata['car_color']      = $this->input->post('car_color');
                $catdata['car_number']     = $this->input->post('car_number');
                $this->services_model->setUpdateData($this->service_provider_request, $catdata, $id);
                if ($settings_id > 0) {
                    //echo "<pre>"; print_r($data); echo "</pre>";                                               
                    $insertId = $this->services_model->setUpdateData($this->service_provider_settings, $data, $settings_id);
                } else {
                    $data['services_request_id'] = $id;
                    $insertId                    = $this->services_model->setInsertData($this->service_provider_settings, $data);
                }
                
                if (!empty($_FILES["car_picture"]["name"])) {
                    $picture_img      = do_upload('cars', 'car_picture');
                    $data_picture_img = array(
                        'car_picture' => $picture_img
                    );
                    $this->services_model->setUpdateData($this->service_provider_request, $data_picture_img, $id);
                }
                
                if ($insertId > 0) {
                    $this->session->set_flashdata('message', 'Your services provider information has been update successfully');
                    redirect('admin/services/service_provider_settings/' . $id, 'refresh');
                }
            }
        }
        $data['currencyOption']      = $this->admin_model->getCurrencyOptions($this->currencies, 'Select Currency');
        $data['servicestypeOptions'] = getServicesOptions('Select Sevices');
        $data['servicesData']        = $this->services_model->getServiesProviderRequestDetails($id);
        $data['servicesSettings']    = getServiceProviderSettings($id);
        $data['servicesRequest']     = $this->services_model->getServiceRequestDetails($id);
        
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/service_provider_settings', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function provider_sales_details()
    {
        CheckAdminLoginSession();
        $id                          = $this->uri->segment(4);
        $data['servicestypeOptions'] = getServicesOptions('Select Sevices');
        $data['servicesData']        = $this->services_model->getServiesProviderRequestDetails($id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/provider_sales_details', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function provider_request_details()
    {
        
        CheckAdminLoginSession();
        $id        = $this->uri->segment(5);
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            //echo "<pre>"; print_r($post_data); echo "</pre>"; die;
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('fname', 'first name', 'required');
            $this->form_validation->set_rules('lname', 'last name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $user_id = $this->input->post('user_id');
                if (!empty($user_id)) {
                    $userdata['username']      = $this->input->post('fname') . ' ' . $this->input->post('lname');
                    $userdata['fname']         = $this->input->post('fname');
                    $userdata['lname']         = $this->input->post('lname');
                    $userdata['email']         = $this->input->post('email');
                    $userdata['site_location'] = $this->input->post('site_location');
                    $userdata['aptno']         = $this->input->post('aptno');
                    $userdata['street_number'] = $this->input->post('street_number');
                    $userdata['street_name']   = $this->input->post('street_name');
                    $userdata['street_type']   = $this->input->post('street_type');
                    $userdata['direction']     = $this->input->post('direction');
                    $userdata['country']       = $this->input->post('country');
                    $userdata['state']         = $this->input->post('state');
                    $userdata['city']          = $this->input->post('city');
                    $userdata['postal_code']   = $this->input->post('postal_code');
                    $userdata['mobile']        = $this->input->post('mobbox');
                    $insertId                  = $this->services_model->setUpdateData($this->users, $userdata, $user_id);
                    
                    $data['services_type']   = $this->input->post('services_type');
                    $data['business']        = $this->input->post('business');
                    $data['business_name']   = $this->input->post('business_name');
                    $data['designation']     = $this->input->post('designation');
                    $data['license']         = $this->input->post('license');
                    $data['license_details'] = $this->input->post('license_details');
                    $this->services_model->setUpdateData($this->service_provider_request, $data, $id);
                    
                } else {
                    $data['bussiness_holder_fname'] = $this->input->post('fname');
                    $data['bussiness_holder_lname'] = $this->input->post('lname');
                    $data['business_email']         = $this->input->post('email');
                    $data['business_address']       = $this->input->post('site_location');
                    $data['business_aptno']         = $this->input->post('aptno');
                    $data['business_street_no']     = $this->input->post('street_number');
                    $data['business_street_name']   = $this->input->post('street_name');
                    $data['business_street_type']   = $this->input->post('street_type');
                    $data['business_direction']     = $this->input->post('direction');
                    $data['business_country']       = $this->input->post('country');
                    $data['business_state']         = $this->input->post('state');
                    $data['business_city']          = $this->input->post('city');
                    $data['business_zip']           = $this->input->post('postal_code');
                    $data['business_mobile']        = $this->input->post('mobbox');
                    $data['services_type']          = $this->input->post('services_type');
                    $data['business']               = $this->input->post('business');
                    $data['business_name']          = $this->input->post('business_name');
                    $data['designation']            = $this->input->post('designation');
                    $data['license']                = $this->input->post('license');
                    $data['license_details']        = $this->input->post('license_details');
                    $this->services_model->setUpdateData($this->service_provider_request, $data, $id);
                }
                $this->session->set_flashdata('message', 'Your services provider information has been update successfully');
                redirect('admin/services/service_provider_request', 'refresh');
            }
        }
        
        $data['servicesData']        = $this->services_model->getServiesProviderRequestDetails($id);
        $data['servicestypeOptions'] = getServicesOptions('Select Sevices');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/service_provider_details', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function view_request()
    {
        
        CheckAdminLoginSession();
        $id                    = $this->uri->segment(5);
        $data['servicesData']  = $this->services_model->getServiceReqestDetailById($id);
        $data['operationData'] = $this->services_model->getDataCollectionByField($this->service_operations, 'service_order_id', $id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/view_request', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    
    public function findzipcode()
    {
        
        $html = '';
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $text = $this->input->post('id');
            $this->db->like('zipcode', $text);
            $query = $this->db->get($this->services_area);
            $count = $query->num_rows();
            if ($count > 0) {
                $resultData = $query->result();
                if (!empty($resultData)) {
                    foreach ($resultData as $data) {
                        $checked = (!empty($this->session->userdata('zipcode_item')) && @in_array($data->zipcode, $this->session->userdata('zipcode_item'))) ? 'checked="checked"' : '';
                        $html .= '<li ><input class="zchoose" type="checkbox" value="' . $data->zipcode . '" ' . $checked . '>' . $data->zipcode . '</li>';
                    }
                } else {
                    $html .= '<li>result not found</li>';
                }
            } else {
                $html .= '<li>result not found</li>';
            }
        }
        
        echo $html;
    }
    
    public function getzipcodebysid()
    {
        $id = $this->input->post('id');
        $this->session->unset_userdata('zipcode_item');
        $row = $this->services_model->getDataRowByID($this->table, $id);
        if (!empty($row['servicearea'])) {
            $servicearea = explode(",", $row['servicearea']);
            $this->session->set_userdata('zipcode_item', $servicearea);
        }
    }
    
    public function seleczipcode()
    {
        $html = '';
        CheckAdminLoginSession();
        $data      = array();
        $post_data = $this->input->post('id');
        if (!empty($post_data)) {
            $items_session = $this->session->userdata('zipcode_item');
            if (!empty($items_session)) {
                foreach ($items_session as $key => $value) {
                    $data[] = $value;
                }
                //print_r($data);
                array_push($data, $post_data);
                $newdata = array_unique($data);
                $this->session->set_userdata('zipcode_item', $newdata);
            } else {
                
                $arrayInit = array(
                    '0' => $post_data
                );
                $this->session->set_userdata('zipcode_item', $arrayInit);
            }
            $items_session = $this->session->userdata('zipcode_item');
            if (!empty($items_session)) {
                foreach ($items_session as $key => $value) {
                    $html .= '<span><i class="icon-large icon-check"></i> ' . $value . ' </span>';
                }
            }
        } else {
            $items_session = $this->session->userdata('zipcode_item');
            if (!empty($items_session)) {
                foreach ($items_session as $key => $value) {
                    $html .= '<span><i class="icon-large icon-check"></i> ' . $value . ' </span>';
                }
            }
        }
        
        echo $html;
    }
    
    public function unsetzipcode()
    {
        
        $html = '';
        
        CheckAdminLoginSession();
        $post_data = $this->input->post('id');
        $data      = array();
        $post_data = $this->input->post('id');
        if (!empty($post_data)) {
            $items_session = $this->session->userdata('zipcode_item');
            foreach ($items_session as $key => $value) {
                $data[] = $value;
            }
            foreach ($data as $key => $value) {
                if ($value == $post_data) {
                    unset($data[$key]);
                }
            }
            $this->session->set_userdata('zipcode_item', $data);
            $items_session = $this->session->userdata('zipcode_item');
            if (!empty($items_session)) {
                foreach ($items_session as $key => $value) {
                    $html .= '<span><i class="icon-large icon-check"></i> ' . $value . ' </span>';
                }
            }
        }
        
        echo $html;
    }
    
    public function allowed_zip_code()
    {
        
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="alertdanger">', '</div>');
            $this->form_validation->set_rules('services', 'services', 'required');
            //$this->form_validation->set_rules('servicearea', 'service area', 'required'); 
            if ($this->form_validation->run() != FALSE) {
                
                if (!empty($this->session->userdata('zipcode_item'))) {
                    $id                      = $this->input->post('services');
                    $areaArray               = $this->session->userdata('zipcode_item');
                    $arrayList               = implode(',', $areaArray);
                    $userdata['servicearea'] = $arrayList;
                    $this->services_model->setUpdateData($this->table, $userdata, $id);
                    $this->session->unset_userdata('zipcode_item');
                    $this->session->set_flashdata('message', 'Your update serice area code successfully');
                    redirect('admin/services/allowed_zip_code', 'refresh');
                } else {
                    $this->session->set_flashdata('message', 'You need to choose at least 1 service area zipcode.');
                }
            }
        }
        $data['servicesOptions'] = getServicesOptions('Select Services');
        //$data['areaOptions']=getareaOptions('Select Services Area');    
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/allowed_zip_code', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    
    public function servicearea()
    {
        $data           = array();
        $limit_per_page = $this->config->item('adm_limits_per_page');
        $page           = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        $start_index    = $page * $limit_per_page;
        $total_records  = get_total_records($this->services_area);
        if ($total_records > 0) {
            $data["results"]     = get_current_page_records($this->services_area, $limit_per_page, $start_index);
            $data["paginations"] = Jpagination($total_records, $limit_per_page, base_url('admin/services/servicearea'));
        }
        
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/servicearea', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function addservicearea()
    {
        CheckAdminLoginSession();
        $data      = array();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="alertdanger">', '</div>');
            $this->form_validation->set_rules('postal_code', 'zip code', 'required');
            $this->form_validation->set_rules('servicearea', 'service area', 'required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'zipcode' => !empty($post_data['postal_code']) ? $post_data['postal_code'] : '',
                    'latitude' => !empty($post_data['latitude']) ? $post_data['latitude'] : '',
                    'longitude' => !empty($post_data['longitude']) ? $post_data['longitude'] : '',
                    'country' => !empty($post_data['country']) ? $post_data['country'] : '',
                    'place_name' => !empty($post_data['state']) ? $post_data['state'] : '',
                    'area' => !empty($post_data['city']) ? $post_data['city'] : '',
                    'address' => !empty($post_data['servicearea']) ? $post_data['servicearea'] : ''
                );
                
                //print_r($data); die;
                $insert_id = $this->services_model->setInsertData($this->services_area, $data);
                if ($insert_id > 0) {
                    $this->session->set_flashdata('message', 'Service area zip/postal code has added successfully');
                    redirect('admin/services/servicearea', 'refresh');
                } else {
                    $this->session->set_flashdata('message', 'Oops: Something going wrong, try again !!');
                    redirect('admin/services/addservicearea', 'refresh');
                }
                
            }
        }
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/addservicearea', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function editservicearea()
    {
        CheckAdminLoginSession();
        $id        = $this->uri->segment(5);
        $data      = array();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<div class="alertdanger">', '</div>');
            $this->form_validation->set_rules('postal_code', 'zip code', 'required');
            $this->form_validation->set_rules('servicearea', 'service area', 'required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'zipcode' => !empty($post_data['postal_code']) ? $post_data['postal_code'] : '',
                    'latitude' => !empty($post_data['latitude']) ? $post_data['latitude'] : '',
                    'longitude' => !empty($post_data['longitude']) ? $post_data['longitude'] : '',
                    'country' => !empty($post_data['country']) ? $post_data['country'] : '',
                    'place_name' => !empty($post_data['state']) ? $post_data['state'] : '',
                    'area' => !empty($post_data['city']) ? $post_data['city'] : '',
                    'address' => !empty($post_data['servicearea']) ? $post_data['servicearea'] : ''
                );
                
                //print_r($data); die;
                $update_id = $this->services_model->setUpdateData($this->services_area, $data, $id);
                if ($update_id > 0) {
                    $this->session->set_flashdata('message', 'Service area zip/postal code has updated successfully');
                    redirect('admin/services/servicearea', 'refresh');
                } else {
                    $this->session->set_flashdata('message', 'Oops: Something going wrong, try again !!');
                    redirect('admin/services/addservicearea', 'refresh');
                }
                
            }
        }
        $data['record'] = $this->services_model->getZipcodeRecordByID($this->services_area, $id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/editservicearea', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function deleteservicearea()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(5);
        $this->services_model->DeleteByID($this->services_area, $id);
        $this->session->set_flashdata('message', 'Your service area has been deleted successfully');
        redirect('admin/services/servicearea', 'refresh');
    }
    
    public function service_customer_request()
    {
        CheckAdminLoginSession();
        $data           = array();
        $limit_per_page = $this->config->item('adm_limits_per_page');
        $page           = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        $start_index    = $page * $limit_per_page;
        $total_records  = get_total_records($this->service_orders);
        if ($total_records > 0) {
            $data["results"]     = get_current_page_records($this->service_orders, $limit_per_page, $start_index);
            $data["paginations"] = Jpagination($total_records, $limit_per_page, base_url('admin/services/service_customer_request'));
        }
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/service_customer_request', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function import_zipcode()
    {
        
        CheckAdminLoginSession();
        //$filename = $_FILES["csvfile"]["tmp_name"];
        if (@$_FILES['csvfile']['size'] > 0) {
            $count = 0;
            $fp = fopen($_FILES['csvfile']['tmp_name'], 'r') or die("can't open file");
            while ($csv_line = fgetcsv($fp, 1000000)) {
                $count++;
                if ($count == 1) {
                    continue;
                }
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    if (!empty($csv_line[0])) {
                        $insert_csv               = array();
                        $insert_csv['zipcode']    = $csv_line[0];
                        $insert_csv['fas']        = $csv_line[1];
                        $insert_csv['latitude']   = $csv_line[2];
                        $insert_csv['longitude']  = $csv_line[3];
                        $insert_csv['place_name'] = $csv_line[4];
                        $insert_csv['area']       = $csv_line[5];
                    }
                }
                $i++;
                $data = array(
                    'zipcode' => $insert_csv['zipcode'],
                    'fas' => $insert_csv['fas'],
                    'latitude' => $insert_csv['latitude'],
                    'longitude' => $insert_csv['longitude'],
                    'place_name' => $insert_csv['place_name'],
                    'area' => $insert_csv['area']
                );
                //echo "<pre>";
                //print_r($data); //die;
                $this->services_model->setInsertData($this->services_area, $data);
            }
            
            fclose($fp) or die("can't close file");
            
            $this->session->set_flashdata('message', 'Your zipcode imported successfully');
            redirect('admin/services/import_zipcode', 'refresh');
        }
        $data['servicesData'] = $this->services_model->getServiceReqestDataCollection();
        //print_r($data['servicesData']);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/import_zip', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    
    public function add_price()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        //print_r($post_data);
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name[0]', ' name', 'required');
            $this->form_validation->set_rules('price', 'price', 'required');
            $this->form_validation->set_rules('parent', 'services', 'required');
            $this->form_validation->set_rules('availability', 'services availability', 'required');
            if ($this->form_validation->run() == FALSE) {
            } else {
                $data = array(
                    'service_id' => $this->input->post('parent'),
                    'price' => $this->input->post('price'),
                    'availability' => $this->input->post('availability'),
                    'created' => date('Y-m-d H:i:s'),
                    'status' => 1
                );
                
                $id       = $this->services_model->setInsertData($this->services_price, $data);
                $j        = 0;
                $namePost = $this->input->post('name');
                $langPost = $this->input->post('lang');
                foreach ($namePost as $pagaTitle) {
                    $data_content['service_price_id'] = $id;
                    $data_content['language_id']      = $langPost[$j];
                    $data_content['name']             = $pagaTitle;
                    $this->services_model->setInsertData($this->services_price_content, $data_content);
                    $j++;
                }
                
                $this->session->set_flashdata('message', 'Your new services prices has been added successfully');
                redirect('admin/services/prices', 'refresh');
            }
        }
        
        $data['language']        = $this->services_model->getLanguageCollection('tbl_language');
        $data['servicesOptions'] = getServicesOptions('Select Parent Services');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/add_price', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit_price()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        $id        = $this->uri->segment(5);
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name[0]', ' name', 'required');
            $this->form_validation->set_rules('price', 'price', 'required');
            $this->form_validation->set_rules('parent', 'services', 'required');
            $this->form_validation->set_rules('availability', 'services availability', 'required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'service_id' => $this->input->post('parent'),
                    'price' => $this->input->post('price'),
                    'availability' => $this->input->post('availability'),
                    'created' => date('Y-m-d H:i:s'),
                    'status' => 1
                );
                $this->services_model->setUpdateData($this->services_price, $data, $id);
                
                $j        = 0;
                $namePost = $this->input->post('name');
                $langPost = $this->input->post('lang');
                foreach ($namePost as $pagaTitle) {
                    
                    $lang_id                          = $langPost[$j];
                    $data_content['name']             = $pagaTitle;
                    $data_content['service_price_id'] = $id;
                    $data_content['language_id']      = $lang_id;
                    $check_content                    = $this->services_model->CheckPriceContent($this->services_price_content, $id, $lang_id);
                    if ($check_content == 1) {
                        $this->services_model->setUpdatePriceContentData($this->services_price_content, $data_content, $id, $lang_id);
                    } else {
                        $this->services_model->setInsertData($this->services_price_content, $data_content);
                    }
                    $j++;
                }
                $this->session->set_flashdata('message', 'Your services price has been update successfully');
                redirect('admin/services/prices', 'refresh');
            }
        }
        $data['servicesOptions'] = getServicesOptions('Select Parent Services');
        $data['servicesData']    = $this->services_model->getServicesPricesDataCollectionByID($this->services_price, $id);
        $data['language']        = $this->services_model->getLanguageCollection('tbl_language');
        $price_contents          = $this->services_model->getServicePriceContent($this->services_price_content, $id);
        $services_content_array  = array();
        if (!empty($price_contents)) {
            foreach ($price_contents as $rec) {
                $services_content_array[$rec->language_id] = $rec->name;
            }
        }
        $data['services_content'] = $services_content_array;
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/edit_price', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    
    public function service_status()
    {
        CheckAdminLoginSession();
        $id         = $this->uri->segment(4);
        $statusId   = getProvicerStatusById($this->service_provider_request, $id);
        $vendordata = $this->services_model->getUserDataByServiceRequestId($id);
        $service    = $this->services_model->getRequestDetails($id);
        
        $servicesSettings   = getServiceProviderSettingsArr($id);
        $mandatoryFieldsArr = array(
            'service_id' => 'Service',
            'start_date' => 'Start Date',
            'service_category_code' => 'Service Code',
            'operator_no' => 'Operator No',
            'service_zip_code' => 'Service ZipCode',
            'currency' => 'Currency',
            'ho_fees' => 'H.O Fees',
            'ho_per_order_charge' => 'H.O Per order charges',
            'ho_fix_weekly_charges' => 'H.O Fix weekly charges',
            'tax' => 'Tax',
            'tax_name' => 'Tax Name',
            'service_charge' => 'Service Charge',
            'service_duration' => 'Service Duration',
            'service_charges_after' => 'Charges after that',
            'service_duration_after' => 'How long',
            'service_additional_charge' => 'Charges after XX hours',
            'service_additional_hours' => 'How long',
            'service_hourly_charge' => 'Hourly charges after that',
            'service_additional_worker' => 'Additional worker charges',
            'weekend_charge' => 'Weekend Service Charges',
            'weekend_duration' => 'Weekend Service Duraion',
            'weekend_charges_after' => 'Weekend service charge after',
            'weekend_duration_after' => 'For how long',
            'weekend_additional_charge' => 'Charges after X hours',
            'weekend_additional_charge' => 'For how long',
            'weekend_hourly_charge' => 'Hourly charges after that',
            'weekend_additional_worker' => 'Additional worker charges',
            'weekend_hourly_charge' => 'Hourly charges after that'
        );
        
        //echo "<pre>"; print_r($service); echo "</pre>"; die;
        
        if ($statusId == 2) {
            $data['service_status'] = 1;
            $data2['status']        = 1;
            $subject                = 'Service request approval';
            $email_template         = 'service-request-activated.html';
        } else {
            $data['service_status'] = 2;
            $data2['status']        = 0;
            $subject                = 'Service request changed';
            $email_template         = 'service-request-deactivated.html';
        }
        
        
        $flag       = 0;
        $needtofill = array();
        
        foreach ($mandatoryFieldsArr as $fieldname => $fieldlabel) {
            if (array_key_exists($fieldname, $servicesSettings) && empty($servicesSettings[$fieldname])) {
                $flag                   = $flag + 1;
                $needtofill[$fieldname] = $fieldlabel;
            }
        }
        
        if (!empty($needtofill)) {
            $fields_labels = implode(", ", $needtofill);
            $fields_labels = !empty($fields_labels) ? '(' . $fields_labels . ')' : '';
        }
        
        if ($statusId == 2 && empty($service['user_id'])) {
            $this->session->set_flashdata('notification', array(
                'error' => 1,
                'message' => 'Service provider has not registed till, So approval will done after their registration.'
            ));
        } elseif ($statusId == 2 && !empty($flag) && !empty($needtofill) && !empty($fields_labels)) {
            $msg = 'All mandatory fields of setting need be configured before proceed to approve the service provider.';
            $this->session->set_flashdata('notification', array(
                'error' => 1,
                'message' => $msg
            ));
        } else {
            $this->services_model->setUpdateData($this->service_provider_request, $data, $id);
            $this->services_model->setUpdateSettingStatusByRequestId($this->service_provider_settings, $data2, $id);
            
            if ($statusId == 2 && !empty($servicesSettings['service_zip_code'])) {
                $zipcodeArr = explode(",", $servicesSettings['service_zip_code']);
                if (!empty($zipcodeArr)) {
                    foreach ($zipcodeArr as $zipcode) {
                        $is_zipcode_exist = $this->services_model->isExist($this->services_area, array(
                            'zipcode' => $zipcode
                        ));
                        if (!empty($is_zipcode_exist)) {
                            $this->services_model->setUpdateRecord($this->services_area, array(
                                'status' => 1
                            ), array(
                                'zipcode' => $zipcode
                            ));
                        }
                    }
                }
            }
            
            $templateTags = array(
                '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
                '{{site_name}}' => 'firstchoice.com',
                '{{site_url}}' => base_url(),
                '{{team_name}}' => 'firstchoice',
                '{{user_name}}' => $vendordata['username'],
                '{{user_email}}' => $vendordata['email'],
                '{{service_name}}' => getServicesName($service['service_id']),
                '{{zipcode}}' => $service['service_zip_code'],
                '{{year}}' => date('Y'),
                '{{company_name}}' => 'firstchoice.com',
                '{{company_email}}' => 'info@firstchoice.com'
            );
            $message      = email_compose($email_template, $templateTags);
            send_email($vendordata['email'], $subject, $message);
            $this->session->set_flashdata('notification', array(
                'error' => 0,
                'message' => 'Service provider status has changed successfully.'
            ));
        }
        redirect('admin/services/service_provider_request', 'refresh');
    }
    
    public function servicearea_status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = getServiceAreaStatusById($this->services_area, $id);
        $data['status'] = (!empty($statusId) && $statusId == 1) ? 0 : 1;
        $this->services_model->setUpdateData($this->services_area, $data, $id);
        $this->session->set_flashdata('notification', array(
            'error' => 0,
            'message' => 'Service area status has changed successfully.'
        ));
        redirect('admin/services/servicearea', 'refresh');
        
    }
    
    public function service_request_status()
    {
        CheckAdminLoginSession();
        $id       = $this->uri->segment(4);
        $statusId = getProvicerStatusById($this->service_provider_request, $id);
        if ($statusId == 2) {
            $data['service_status'] = 1;
        } else {
            $data['service_status'] = 2;
        }
        
        //print_r($data);die;
        $this->services_model->setUpdateData($this->service_provider_request, $data, $id);
        $this->session->set_flashdata('message', 'Your services provider request status has been change successfully');
        redirect('admin/services/service_provider_request', 'refresh');
    }
    
    public function request_verify()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        
        $vendordata = $this->services_model->getDataByServiceRequestId($id);
        /* email to service provider */
        
        $token = md5(rand(11111, 99999));
        $this->services_model->setUpdateData($this->service_provider_request, array(
            'token' => $token
        ), $id);
        $email_template = 'service-request-verified.html';
        $signup_link    = base_url('service-provider-signup') . '/' . base64_encode($id) . '/' . $token;
        $templateTags   = array(
            '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
            '{{site_name}}' => 'firstchoice.com',
            '{{site_url}}' => base_url(),
            '{{team_name}}' => 'firstchoice',
            '{{user_email}}' => $vendordata['business_email'],
            '{{service_name}}' => getServicesName($vendordata['service_id']),
            '{{zipcode}}' => $vendordata['service_zip_code'],
            '{{signup_link}}' => $signup_link,
            '{{year}}' => date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com'
        );
        $message        = email_compose($email_template, $templateTags);
        send_email($vendordata['business_email'], 'Service provider registration link', $message);
        
        
        
        $business_mobile = !empty($vendordata['business_mobile']) ? $vendordata['business_mobile'] : '';
        if (!empty($business_mobile)) {
            $smsMsg = 'Dear Candidate,';
            $smsMsg .= 'Your service request has verified and accepted. Please check your email inbox for registration link and process for registration.';
            send_sms($business_mobile, $smsMsg);
        }
        
        
        $this->services_model->setUpdateData($this->service_provider_request, array(
            'is_verified' => 1
        ), $id);
        $this->session->set_flashdata('notification', array(
            'error' => 0,
            'message' => 'Registration link for services provider has been sent to their inbox.'
        ));
        redirect('admin/services/service_provider_request/', 'refresh');
    }
    
    public function service_records_status()
    {
        CheckAdminLoginSession();
        $id               = $this->uri->segment(4);
        $statusId         = getStatusById($this->service_provider_settings, $id);
        $serviceRequestId = getSPRIdById($this->service_provider_settings, $id);
        $vendordata       = $this->services_model->getUserDataByServiceSettingId($id);
        $service          = $this->services_model->getServiesProviderRequestServiceDetailsById($id);
        if (!empty($vendordata['status']) && $vendordata['status'] == 1) {
            if (empty($statusId) && $statusId == 0) {
                $data['status'] = 1;
                $this->services_model->setUpdateData($this->service_provider_settings, $data, $id);
                /* email to service provider */
                $email_template = 'service-request-activated.html';
                $templateTags   = array(
                    '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
                    '{{site_name}}' => 'firstchoice.com',
                    '{{site_url}}' => base_url(),
                    '{{team_name}}' => 'firstchoice',
                    '{{user_name}}' => $vendordata['username'],
                    '{{user_email}}' => $vendordata['email'],
                    '{{service_name}}' => getServicesName($service['service_id']),
                    '{{zipcode}}' => $service['service_zip_code'],
                    '{{year}}' => date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com'
                );
                $message        = email_compose($email_template, $templateTags);
                send_email($vendordata['email'], 'Service request activated', $message);
                /*///////////////////////////////////*/
            } else {
                $data['status'] = 0;
                $this->services_model->setUpdateData($this->service_provider_settings, $data, $id);
                /* email to service provider */
                $email_template = 'service-request-deactivated.html';
                $templateTags   = array(
                    '{{site_logo}}' => base_url() . 'skin/front/images/logo.png',
                    '{{site_name}}' => 'firstchoice.com',
                    '{{site_url}}' => base_url(),
                    '{{team_name}}' => 'firstchoice',
                    '{{user_name}}' => $vendordata['username'],
                    '{{user_email}}' => $vendordata['email'],
                    '{{service_name}}' => getServicesName($service['service_id']),
                    '{{zipcode}}' => $service['service_zip_code'],
                    '{{year}}' => date('Y'),
                    '{{company_name}}' => 'firstchoice.com',
                    '{{company_email}}' => 'info@firstchoice.com'
                );
                $message        = email_compose($email_template, $templateTags);
                send_email($vendordata['email'], 'Service request deactivated', $message);
            }
            $this->session->set_flashdata('notification', array(
                'error' => 0,
                'message' => 'Your services provider request status has been change successfully'
            ));
        } else {
            $this->session->set_flashdata('notification', array(
                'error' => 1,
                'message' => 'Associated service provider is under pending. So, their service cannot be approved whenever service provider will be under pending status.'
            ));
        }
        redirect('admin/services/service_provider_records/' . $serviceRequestId, 'refresh');
    }
    
    public function prices_status()
    {
        CheckAdminLoginSession();
        $id       = $this->uri->segment(4);
        $statusId = getStatusById($this->services_price, $id);
        if ($statusId == 2) {
            $data['status'] = 1;
        } else {
            $data['status'] = 2;
        }
        $this->services_model->setUpdateData($this->services_price, $data, $id);
        $this->session->set_flashdata('message', 'Your services price status has been change successfully');
        redirect('admin/services/prices', 'refresh');
    }
    
    public function feature()
    {
        CheckAdminLoginSession();
        $id        = $this->uri->segment(4);
        $featureId = getFeatureById($this->table, $id);
        if ($featureId == 2) {
            $data['feature'] = 1;
        } else {
            $data['feature'] = 2;
        }
        $this->services_model->setUpdateData($this->table, $data, $id);
        $this->session->set_flashdata('message', 'Your services feature has been change successfully');
        redirect('admin/services', 'refresh');
    }
    
   
    public function prices_delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(5);
        $this->services_model->DeleteByID($this->services_price, $id);
        $this->session->set_flashdata('message', 'Your services price has been deleted successfully');
        redirect('admin/services/prices', 'refresh');
    }
    
    public function delete_servicerequest()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(5);
        
        $spData = $this->services_model->getServiesProviderRequestDetails($id);
        //echo "<pre>"; print_r($spData); echo "</pre>"; die;
        $this->services_model->deleteDataByField($this->service_provider_request, 'id', $id);
        $this->services_model->deleteDataByField($this->service_provider_settings, 'services_request_id', $id);
        if (!empty($spData['user_id'])) {
            $this->services_model->deleteDataByField($this->users, 'id', $spData['user_id']);
        }
        $this->session->set_flashdata('notification', array(
            'error' => 0,
            'message' => 'Service request has been deleted successfully'
        ));
        redirect('admin/services/service_provider_request', 'refresh');
    }
    
    public function videos()
    {
        $data           = array();
        $limit_per_page = $this->config->item('adm_limits_per_page');
        $page           = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        $start_index    = $page * $limit_per_page;
        $total_records  = get_total_records($this->videos);
        if ($total_records > 0) {
            $data["results"]     = get_current_page_records($this->videos, $limit_per_page, $start_index);
            $data["paginations"] = Jpagination($total_records, $limit_per_page, base_url('admin/services/videos'));
        }
        
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/videos', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function add_video()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            //print_r($post_data); die;
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('service_id', ' service', 'required');
            $this->form_validation->set_rules('video', 'video embedded url', 'required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'service_id' => $this->input->post('service_id'),
                    'video_source' => $this->input->post('video_source'),
                    'video' => $this->input->post('video'),
                    'status' => 1
                );
                
                $this->services_model->setInsertData($this->videos, $data);
                $this->session->set_flashdata('notification', array(
                    'error' => 0,
                    'message' => 'Service video has added successfully.'
                ));
                redirect('admin/services/videos', 'refresh');
            }
        }
        $data['servicesOptions'] = getServicesOptions('Select Service');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/add_video', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit_video()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        $id        = $this->uri->segment(5);
        if (!empty($post_data)) {
            //print_r($post_data); die;
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('service_id', ' service', 'required');
            $this->form_validation->set_rules('video', 'video embedded url', 'required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'service_id' => $this->input->post('service_id'),
                    'video_source' => $this->input->post('video_source'),
                    'video' => $this->input->post('video'),
                    'status' => 1
                );
                
                $this->services_model->setUpdateData($this->videos, $data, $id);
                $this->session->set_flashdata('notification', array(
                    'error' => 0,
                    'message' => 'Service video has updated successfully.'
                ));
                redirect('admin/services/videos', 'refresh');
            }
        }
        $data['servicesOptions'] = getServicesOptions('Select Services');
        $data['videoData']       = $this->services_model->getRowDataByID($this->videos, $id);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/services/edit_video', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function video_status()
    {
        CheckAdminLoginSession();
        $id       = $this->uri->segment(4);
        $statusId = getStatusById($this->videos, $id);
        if ($statusId == 0) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
        $this->services_model->setUpdateData($this->videos, $data, $id);
        $this->session->set_flashdata('message', 'Service video status has been change successfully');
        redirect('admin/services/videos', 'refresh');
    }
    
    public function delete_video()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(5);
        $this->services_model->DeleteByID($this->videos, $id);
        $this->session->set_flashdata('message', 'Service video has been deleted successfully');
        redirect('admin/services/videos', 'refresh');
    }
    
    
    public function do_upload($folder, $filename)
    {
        // Codeigniter upload for single files only.
        // Uploads folder must be in main directory.
        $config['upload_path']   = './upload/' . $folder . '/'; // Added forward slash 
        $config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?
        
        //$config['max_size'] = '1000';
        // $config['max_width'] = '1024';
        //$config['max_height'] = '768';
        $uploaddir = 'upload/' . $folder . '/';
        if (!is_dir($uploaddir)) {
            mkdir($uploaddir);
        }
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $input = 'featured_img'; // on view the name <input type="file" name="userfile" size="20"/>
        
        if (!$this->upload->do_upload('featured_img')) {
            $this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
        } else {
            $upload_data = $this->upload->data();
            $file_name   = $upload_data['file_name'];
            $file_path   = $uploaddir . $file_name;
            return $file_path;
        }
        
    }
}