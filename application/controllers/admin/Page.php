<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public $tbl_name = "pages";

	function __construct() {
		parent::__construct();
		$this->load->model('page_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['active']    = $this->uri->segment(2);
        $data['records'] = $this->master_model->get_records($this->tbl_name);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/page/lists',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function add()
	{
		CheckAdminLoginSession();
		$data = array();
		$post_data      = $this->input->post();
        if (!empty($post_data))
        {
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            $this->form_validation->set_rules('name', 'title', 'required');
            $this->form_validation->set_rules('description', 'content', 'required');
            if ($this->form_validation->run() != FALSE)
            {
                $slug_str  = $this->input->post('name');
                $slug      = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data      = array(
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'description' => $this->input->post('description'),
                    'meta_title' => $this->input->post('meta_title'),
                    'meta_key' => $this->input->post('meta_key'),
                    'meta_description' => $this->input->post('meta_description'),
                    'status' => 1
                );
                $record_id = $this->master_model->setInsertData($this->tbl_name, $data);
                if(!empty($record_id))
                {
                    if ($_FILES["featured_img"]["name"] != "")
                    {
                        $featured_img      = do_upload('pages');
                        $data_featured_img = array('featured_img' => $featured_img);
                        $this->master_model->setUpdateData($this->tbl_name, $data_featured_img, $record_id);
                    }
                    $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been added successfully !!'));
                    redirect('admin/page', 'refresh');
                }
                
            }
        }
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/page/add',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function edit()
    {
        CheckAdminLoginSession();
        $post_data      = $this->input->post();
        $rec_id        = $this->uri->segment(4);

        if (!empty($post_data))
        {
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            $this->form_validation->set_rules('name', 'title', 'required');
            $this->form_validation->set_rules('description', 'content', 'required');
            
            if ($this->form_validation->run() != FALSE)
            {
                $slug_str = $this->input->post('name');
                $slug     = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data     = array(
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'description' => $this->input->post('description'),
                    'meta_title' => $this->input->post('meta_title'),
                    'meta_key' => $this->input->post('meta_key'),
                    'meta_description' => $this->input->post('meta_description')
                );
                
                $record_id = $this->master_model->setUpdateData($this->tbl_name, $data, $rec_id);
                if ($record_id > 0)
                {
                    if ($_FILES["featured_img"]["name"] != "")
                    {
                        $featured_img      = do_upload('pages');
                        $data_featured_img = array('featured_img' => $featured_img);
                        $this->master_model->setUpdateData($this->tbl_name, $data_featured_img, $record_id);
                    } 

                    $this->session->set_flashdata('notification', array('error' => 0,'message' => 'Record has been modified successfully !!'));
                    redirect('admin/page', 'refresh');
                }
            }
        }

        $data['record'] = $this->master_model->get_recordArr($this->tbl_name, array('id' => $rec_id));
        $this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/page/edit',$data);
		$this->load->view('admin/include/footer',$data);
    }
    
    public function delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        $this->master_model->DeleteByID($this->tbl_name, $id);
        $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been delete successfully !!'));
        redirect('admin/page', 'refresh');
    }
    
    public function status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
        $data['status'] = (empty($statusId) || $statusId == 0) ? 1 : 0;
        $rec_id         = $this->master_model->setUpdateData($this->tbl_name, $data, $id);
        if ($rec_id > 0)
        {
            $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Status has been updated successfully !!'));
            redirect('admin/page', 'refresh');
        }
    }
}