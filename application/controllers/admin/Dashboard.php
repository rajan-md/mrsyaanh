<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        //$this->load->model('master_model');
        $this->load->model('admin_model');
        $this->load->helper('admin_helper');
        
    }
    
    public $blog = "blog";
    public $users = "users";
    public $pages = "pages";
    public $services = "services";
    
    public function index()
    {
        CheckAdminLoginSession();
        
        $data['blogCount']     = getRowCount($this->blog);
        $data['userCount']     = getRowCount($this->users);
        $data['pageCount']     = getRowCount($this->pages);
        $data['servicesCount'] = getRowCount($this->services);
        
        $data['blogData']     = getDataRecords($this->blog, array(), 0, 8);
        $data['userData']     = getDataRecords($this->users, array(), 0, 8);
        $data['servicesData'] = getDataRecords($this->services, array(), 0, 8);
        
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function security()
    {
        $this->load->view('admin/security');
    }
    
    
}