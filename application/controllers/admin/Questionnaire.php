<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire extends CI_Controller
{
    
    public $tbl_name     = "service_questionnaire";
    public $tbl_services = "services";
    
    
    function __construct()
    {
        parent::__construct();
        $this->load->helper('admin_helper');
    }
    
    public function index()
    {
        CheckAdminLoginSession();
        $records = $this->master_model->get_recordsArr($this->tbl_name);
        $data['records'] = getOptionsTree($records); 
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/questionnaire/lists', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function add()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name', ' name', 'required');            
            if ($this->form_validation->run() != FALSE) {
                $slug_str = $this->input->post('name[0]');
                $slug     = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data     = array(                    
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'parent' => $this->input->post('parent'),
                    'service_id' => $this->input->post('service_id'),                    
                    'status' => 1
                );
                $record_id = $this->master_model->setInsertData($this->tbl_name, $data);
                if(!empty($record_id))
                {
                    $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been added successfully !!'));
                    redirect('admin/questionnaire', 'refresh');
                }
            }
        }
        $data['serviceOptions'] = getMultilevelOptions($this->tbl_services, array('status'=>1), '---Self---');
        $data['parentOptions'] = getMultilevelOptions($this->tbl_name, array('status'=>1), '---Self---');
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/questionnaire/add', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        $rec_id        = $this->uri->segment(4);
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('name', ' name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $slug_str = $this->input->post('name[0]');
                $slug     = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
                $data     = array(                    
                    'name' => $this->input->post('name'),
                    'slug' => $slug,
                    'parent' => $this->input->post('parent'),
                    'service_id' => $this->input->post('service_id'),
                );
                $record_id = $this->master_model->setUpdateData($this->tbl_name, $data, $rec_id);
                
                if ($record_id > 0)
                {
                    $this->session->set_flashdata('notification', array('error' => 0,'message' => 'Record has been modified successfully !!'));
                    redirect('admin/questionnaire', 'refresh');
                }
            }
        }
        $data['serviceOptions'] = getMultilevelOptions($this->tbl_services, array('status'=>1), '---Self---');
        $data['parentOptions'] = getMultilevelOptions($this->tbl_name, array('status'=>1), '---Self---');
        $data['record'] = $this->master_model->get_recordArr($this->tbl_name, array('id' => $rec_id));
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/questionnaire/edit', $data);
        $this->load->view('admin/include/footer', $data);
    }

    public function delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        $this->master_model->DeleteByID($this->tbl_name, $id);
        $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been delete successfully !!'));
        redirect('admin/questionnaire', 'refresh');
    }
    
    public function status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
        $data['status'] = (empty($statusId) || $statusId == 0) ? 1 : 0;
        $rec_id         = $this->master_model->setUpdateData($this->tbl_name, $data, $id);
        if ($rec_id > 0)
        {
            $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Status has been updated successfully !!'));
            redirect('admin/questionnaire', 'refresh');
        }
    }
}