<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public $tbl_name = "role";


	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['records'] = $this->master_model->get_records($this->tbl_name);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/role/lists',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function add()
	{
				
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name', ' name', 'required');	
								
			if($this->form_validation->run() != FALSE)
			{
				$slug_str=$this->input->post('name');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(				
				'name' => $this->input->post('name'),
				'slug' => $slug,
				'description' => $this->input->post('description'),
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'created' => date('Y-m-d H:i:s'),
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 

				$this->master_model->setInsertData($this->tbl_name,$data);
				$this->session->set_flashdata('message','Your new role has been added successfully');
		        redirect('admin/role','refresh');
		    }
        }
		$this->load->view('admin/include/header');
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/role/add');
		$this->load->view('admin/include/footer');
	}

	public function edit()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name', ' name', 'required');
									
			if($this->form_validation->run() != FALSE)
			{
				$slug_str=$this->input->post('name');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(				
				'name' => $this->input->post('name'),
				'slug' => $slug,
				'description' => $this->input->post('description'),
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'created' => date('Y-m-d H:i:s'),
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 

				$this->master_model->setUpdateData($this->tbl_name,$data,$id);
				$this->session->set_flashdata('message','Your role has been update successfully');
		        redirect('admin/role','refresh');
		    }
        }
        $data['record']=$this->master_model->get_recordArr($this->tbl_name,array('id'=>$id));
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/role/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}
    
    public function status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
        $data['status'] = (empty($statusId) || $statusId == 0) ? 1 : 0;
        $rec_id         = $this->master_model->setUpdateData($this->tbl_name, $data, $id);
        if ($rec_id > 0)
        {
            $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Status has been updated successfully !!'));
            redirect('admin/role', 'refresh');
        }
    }

    public function delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        $this->master_model->DeleteByID($this->tbl_name, $id);
        $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been delete successfully !!'));
        redirect('admin/role', 'refresh');
    }
}