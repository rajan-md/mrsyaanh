<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons extends CI_Controller
{
    
    public $tbl_name = "coupons";
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('coupons_model');
        $this->load->helper('admin_helper');
    }
    
    public function index()
    {
        CheckAdminLoginSession();
        $data['records'] = $this->master_model->get_records($this->tbl_name);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/coupons/lists', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function add()
    {
        CheckAdminLoginSession();
        $data      = array();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
            $this->form_validation->set_rules('coupon_code', 'coupon code', 'required');
            $this->form_validation->set_rules('coupon_discount', 'discount', 'required');
            //echo "<pre>"; print_r($post_data); echo "</pre>"; die;
            if ($this->form_validation->run() != FALSE) {
                $coupon_code             = $this->input->post('coupon_code');
                $valid_from              = $this->input->post('valid_from');
                $valid_to                = $this->input->post('valid_to');
                $data['coupon_code']     = $coupon_code;
                $data['coupon_discount'] = $this->input->post('coupon_discount');
                $data['type']            = $this->input->post('type');
                $data['valid_from']      = $valid_from;
                $data['valid_to']        = $valid_to;
                if ($valid_from == '0000-00-00' || $valid_to == '0000-00-00') {
                    $data['always'] = 1;
                } else {
                    $data['always'] = 0;
                }
                $data['minimum_target'] = $this->input->post('minimum_target');
                $data['restrict_cat']   = $this->input->post('restrict_cat');
                $data['status']         = 1;
                
                $isExistsCode = isExistRecord($this->tbl_name, array(
                    'coupon_code' => $coupon_code
                ));
                if (!empty($isExistsCode) && $isExistsCode > 0) {
                    $this->session->set_flashdata('notification', array(
                        'error' => 1,
                        'message' => 'Entered Coupon Code has already exist.'
                    ));
                } else {
                    $id = $this->master_model->setInsertData($this->tbl_name, $data);
                    $this->session->set_flashdata('notification', array(
                        'error' => 0,
                        'message' => 'Record has added successfully.'
                    ));
                    redirect('admin/coupons', 'refresh');
                }
                
                
            }
        }
        $data['servicesOptions'] = getServicesOptions('---All Category---', 1);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/coupons/add', $data);
        $this->load->view('admin/include/footer', $data);
    }
    
    public function edit()
    {
        CheckAdminLoginSession();
        $post_data = $this->input->post();
        $id        = $this->uri->segment(4);
        $record    = $this->master_model->get_recordArr($this->tbl_name, array('id' => $id));
        if (!empty($post_data)) {
            $this->form_validation->set_rules('coupon_code', 'coupon code', 'required');
            $this->form_validation->set_rules('coupon_discount', 'discount', 'required');
            //echo "<pre>"; print_r($post_data); echo "</pre>"; die;
            if ($this->form_validation->run() != FALSE) {
                $coupon_code             = $this->input->post('coupon_code');
                $valid_from              = $this->input->post('valid_from');
                $valid_to                = $this->input->post('valid_to');
                $data['coupon_code']     = $coupon_code;
                $data['coupon_discount'] = $this->input->post('coupon_discount');
                $data['type']            = $this->input->post('type');
                $data['valid_from']      = $valid_from;
                $data['valid_to']        = $valid_to;
                if ($valid_from == '0000-00-00' || $valid_to == '0000-00-00') {
                    $data['always'] = 1;
                } else {
                    $data['always'] = 0;
                }
                $data['minimum_target'] = $this->input->post('minimum_target');
                $data['restrict_cat']   = $this->input->post('restrict_cat');
                
                $isExistsCode = isExistRecord($this->tbl_name, array(
                    'coupon_code' => $coupon_code
                ));
                
                if (!empty($record['coupon_code']) && $record['coupon_code'] != $coupon_code && !empty($isExistsCode) && $isExistsCode > 0) {
                    $this->session->set_flashdata('notification', array(
                        'error' => 1,
                        'message' => 'Entered Coupon Code has already exist.'
                    ));
                } else {
                    $this->master_model->setUpdateData($this->tbl_name, $data, $id);
                    $this->session->set_flashdata('notification', array(
                        'error' => 0,
                        'message' => 'Record has updated successfully.'
                    ));
                }
                redirect('admin/coupons/edit/' . $id, 'refresh');
            }
        }
        $data['record']          = $record;
        $data['servicesOptions'] = getServicesOptions('---All Category---', 1);
        $this->load->view('admin/include/header', $data);
        $this->load->view('admin/include/sidebar', $data);
        $this->load->view('admin/coupons/edit', $data);
        $this->load->view('admin/include/footer', $data);
    }

    public function status()
    {
        CheckAdminLoginSession();
        $id             = $this->uri->segment(4);
        $statusId       = $this->master_model->getStatusById($this->tbl_name, $id);
        $data['status'] = (empty($statusId) || $statusId == 0) ? 1 : 0;
        $rec_id         = $this->master_model->setUpdateData($this->tbl_name, $data, $id);
        if ($rec_id > 0)
        {
            $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Status has been updated successfully !!'));
            redirect('admin/coupons', 'refresh');
        }
    }

    public function delete()
    {
        CheckAdminLoginSession();
        $id = $this->uri->segment(4);
        $this->master_model->DeleteByID($this->tbl_name, $id);
        $this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Record has been delete successfully !!'));
        redirect('admin/coupons', 'refresh');
    }
}