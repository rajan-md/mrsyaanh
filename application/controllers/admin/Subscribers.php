<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribers extends CI_Controller {

	public $tbl_name = "subscribers";

	function __construct() {
		parent::__construct();
		$this->load->model('subscribers_model');
		$this->load->helper('admin_helper');		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['records']=$this->master_model->get_records($this->tbl_name);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/subscribers/lists',$data);
		$this->load->view('admin/include/footer',$data);
	}
}