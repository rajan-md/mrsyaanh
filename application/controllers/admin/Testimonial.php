<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $table = "tbl_testimonial";
	public $testimonial_content = "tbl_testimonial_content";


	function __construct() {
		parent::__construct();
		$this->load->model('testimonial_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['testimonialData']=$this->testimonial_model->getCollection();
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/testimonial/testimonial',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function add()
	{
		CheckAdminLoginSession();		
		$post_data=$this->input->post();
		//print_r($post_data);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');		
								
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug' => $slug,		
				'created' => date('Y-m-d H:i:s'),
				'status' => 1		        	             
				); 

				$id=$this->testimonial_model->setInsertData($this->table,$data);
				$j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {                    
					$data_content['testimonial_id'] = $id;
					$data_content['language_id'] = $langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$this->testimonial_model->setInsertData($this->testimonial_content,$data_content);
					$j++;
				}
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('testimonial','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->testimonial_model->setUpdateData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your new testimonial has been added successfully');
		        redirect('admin/testimonial','refresh');
		    }
        }

        $data['language']=$this->testimonial_model->getLanguageCollection('tbl_language');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/testimonial/add',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function edit()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
				$this->form_validation->set_rules('name[0]', ' name', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');
									
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name[0]');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug' => $slug,			
				'status' => 1			        	             
				); 
				$this->testimonial_model->setUpdateData($this->table,$data,$id);
                
                $j=0;
                $namePost=$this->input->post('name');
				$langPost=$this->input->post('lang');
				$descriptionPost=$this->input->post('description');
				foreach($namePost as $pagaTitle) {
                    $lang_id=$langPost[$j];
					$data_content['name'] = $pagaTitle;
					$data_content['description'] = $descriptionPost[$j];
					$data_content['testimonial_id'] = $id;
					$data_content['language_id'] = $lang_id;
					$check_testimonial_content=$this->testimonial_model->CheckContent($this->testimonial_content,$id,$lang_id);
					if($check_testimonial_content==1){
					$this->testimonial_model->setUpdateContentData($this->testimonial_content,$data_content,$id,$lang_id);
				    }
				    else
				    {
				    	$this->testimonial_model->setInsertData($this->testimonial_content,$data_content);
				    }
					$j++;
				}
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('testimonial','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->testimonial_model->setUpdateData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your testimonial has been update successfully');
		        redirect('admin/testimonial','refresh');
		    }
        }
        $data['testimonialData']=$this->testimonial_model->getDataCollectionByID($this->table,$id);
       /* echo '<pre>';
        print_r($data['testimonialData']);
        echo '</pre>';*/
        $data['language']=$this->testimonial_model->getLanguageCollection('tbl_language');
        
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/testimonial/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function status()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$statusId=getStatusById($this->table,$id);
		if($statusId==2) {
		   $data['status']=1;           
		}
		else {
           $data['status']=2;
		}
		$this->testimonial_model->setUpdateData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your testimonial status has been change successfully');
        redirect('admin/testimonial','refresh');
	}

	public function delete()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$this->testimonial_model->DeleteByID($this->table,$id);
		$this->session->set_flashdata('message','Your testimonial has been deleted successfully');
        redirect('admin/testimonial','refresh');
	}

	public function do_upload($folder, $filename) {
		// Codeigniter upload for single files only.
		// Uploads folder must be in main directory.
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|jpeg|png'; // Not sure docx will work?

		//$config['max_size'] = '1000';
		// $config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$uploaddir = 'upload/'.$folder.'/';
		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$input = 'featured_img'; // on view the name <input type="file" name="userfile" size="20"/>

			if (!$this->upload->do_upload('featured_img')) {
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
				$file_path=$uploaddir.$file_name;
				return $file_path;
			}

	}
}