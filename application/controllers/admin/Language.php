<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller {

	/**
	 * Index language for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $table = "tbl_language";


	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
		
	}

	public function index()
	{		
		CheckAdminLoginSession();
		$data['language']=$this->admin_model->getDataCollection($this->table);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/language/language',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function add()
	{
		CheckAdminLoginSession();		
		$post_data=$this->input->post();
		//print_r($post_data);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('name', ' name', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');		
								
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(			
				'slug' => $slug,				
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'created' => date('Y-m-d H:i:s'),
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 

				$id=$this->admin_model->setInsertData($this->table,$data);
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('language','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->admin_model->setUpdateData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your new language has been added successfully');
		        redirect('admin/language','refresh');
		    }
        }

        
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/language/add',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function edit()
	{
		CheckAdminLoginSession();
		$post_data=$this->input->post();
		$id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
				$this->form_validation->set_rules('name', ' name', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
									
			if($this->form_validation->run() == FALSE) {   } else {
				$slug_str=$this->input->post('name');
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_str)));
				$data = array(				
				'name' => $this->input->post('name'),
				'slug' => $slug,
				'description' => $this->input->post('description'),
				'meta_title' => $this->input->post('meta_title'),				
				'meta_key' => $this->input->post('meta_key'),			
				'created' => date('Y-m-d H:i:s'),
				'status' => 1,
				'meta_description' => $this->input->post('meta_description')					        	             
				); 

				$this->admin_model->setUpdateData($this->table,$data,$id);
				if($_FILES["featured_img"]["name"] != "")
				{
					 $featured_img=$this->do_upload('language','featured_img');
					 $data_featured_img = array('featured_img' => $featured_img );
					 $this->admin_model->setUpdateData($this->table,$data_featured_img,$id);
				}
				$this->session->set_flashdata('message','Your language has been update successfully');
		        redirect('admin/language','refresh');
		    }
        }
        $data['languageData']=$this->admin_model->getDataCollectionByID($this->table,$id);
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar',$data);
		$this->load->view('admin/language/edit',$data);
		$this->load->view('admin/include/footer',$data);
	}

	public function status()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$statusId=getStatusById($this->table,$id);
		if($statusId==3) {
		   $data['status']=4;           
		}
		else {
           $data['status']=3;
		}
		$this->admin_model->setUpdateData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your language status has been change successfully');
        redirect('admin/language','refresh');
	}

	public function default_lang()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		
        $datainfo=$this->admin_model->getLanguageCollection($this->table);
        foreach ($datainfo as $nid) {
	        $data1['default']='';
			$this->admin_model->setUpdateData($this->table,$data1,$nid->id);
        }

		$data['default']='active';
		$this->admin_model->setUpdateData($this->table,$data,$id);
		$this->session->set_flashdata('message','Your language status has been change successfully');
        redirect('admin/language','refresh');
	}

	public function delete()
	{
		CheckAdminLoginSession();
		$id=$this->uri->segment(4);
		$this->admin_model->setDeleteData($this->table,$id);
		$this->session->set_flashdata('message','Your language has been deleted successfully');
        redirect('admin/language','refresh');
	}

	public function do_upload($folder, $filename) {
		// Codeigniter upload for single files only.
		// Uploads folder must be in main directory.
		$config['upload_path'] = './upload/'.$folder.'/'; // Added forward slash 
		$config['allowed_types'] = 'gif|jpg|png|docx'; // Not sure docx will work?

		//$config['max_size'] = '1000';
		// $config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$uploaddir = 'upload/'.$folder.'/';
		if(!is_dir($uploaddir))
		{
			mkdir($uploaddir);
		}
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$input = 'featured_img'; // on view the name <input type="file" name="userfile" size="20"/>

			if (!$this->upload->do_upload('featured_img')) {
			$this->form_validation->set_message('do_upload', 'Post update should have something written or a photo or attach a file.');
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
				$file_path=$uploaddir.$file_name;
				return $file_path;
			}

	}
}