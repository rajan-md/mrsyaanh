<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	 public $tbl_name = "settings";
	 public $smtp = "smtp_settings";
	 public $home = "home_settings";
	 public $home_setting_content = "home_setting_content";
	 public $currencies = "currencies";
	 public $static_content_management = "static_content_management";


	function __construct()
	{
		parent::__construct();	    
		$this->load->model('admin_model');
		$this->load->helper('admin_helper');
	}

	public function save_language_content_by_key($lang_id="",$lang_key="",$lang_desc="") {
		ob_start();
		if(($lang_id!="") && ($lang_key!="") && ($lang_desc!="")) {
			$langData=$this->lang; 
	        $language=strtolower(getLanguageName($lang_id));
	        $this->lang->load('front',$language);
	        $this->load->helper('file');
	        $array = json_decode(json_encode($langData),true);
	    	$lang=array();
	         $langstr="<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                /**
                *
                * Created:  2017-11-27 by Jabir
                *
                * Description:  ".$language." language file for general views
                *
                */"."\n\n\n";
	        foreach ($array['language'] as $key => $value) {
	           if($key==$lang_key){
	               $langstr.= "\$lang['".$key."'] = \"$lang_desc\";"."\n";
	           } else {
	            $langstr.= "\$lang['".$key."'] = \"$value\";"."\n";
	           }
	        } 
	        if (!array_key_exists($lang_key,$array['language'])) {
	            $langstr.= "\$lang['".$lang_key."'] = \"$lang_desc\";"."\n";
	        }
	        write_file('./application/language/'.$language.'/front_lang.php', $langstr);
	        ob_flush();
        }
	}

	public function advance()
	{
		CheckAdminLoginSession();
        $post_data=$this->input->post();
		if(!empty($post_data)) {
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('sitename', 'site name', 'required');
			$this->form_validation->set_rules('admin_email', 'admin email', 'required');	
			$this->form_validation->set_rules('currency', 'currency', 'required');					
			if($this->form_validation->run() != FALSE)
			{				
				foreach ($post_data as $key => $value)
				{
					$keyExist = $this->master_model->isExist($this->tbl_name,array('name'=>$key));
					if(!empty($keyExist))
					{
						$this->master_model->setUpdateRecord($this->tbl_name, array('value' => $value), array('name' => $key));
					}
					else
					{
						$this->master_model->setInsertData($this->tbl_name, array('name'=>$key,'value' => $value));
					}
				}

				$files = $_FILES;
				if(!empty($files))
				{
					foreach ($files as $key=>$file)
					{
						if(!empty($file['name']))
						{

							$keyExist = $this->master_model->isExist($this->tbl_name,array('name'=>$key));
							$filePath = do_upload('logo',$key);
							if(!empty($keyExist))
							{
								$this->master_model->setUpdateRecord($this->tbl_name, array('value' => $filePath), array('name' => $key));
							}
							else
							{
								$this->master_model->setInsertData($this->tbl_name, array('name'=>$key,'value' => $filePath));
							}
						}
					}
				}

				$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'Settings has been added successfully !!'));
		        redirect('admin/settings/advance','refresh');
		    }
        }
        $data['settingsData'] = $this->admin_model->getSettingsDataCollection($this->tbl_name);
        $data['currencyOption'] = $this->admin_model->getCurrencyOptions($this->currencies, 'Select Currency');
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/advance', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function home()
	{
		CheckAdminLoginSession();
        $post_data=$this->input->post();
		if(!empty($post_data)) {  
		$dataArr['featurevideo']     = $this->input->post('featurevideo');
		$dataArr['featurelinks'] = $this->input->post('featurelinks');	
		$dataArr['appstore'] = $this->input->post('appstore');	
		$dataArr['googleplay'] = $this->input->post('googleplay');	
		foreach ($dataArr as $key => $value) {
			$data = array('value' => $value);
			$this->admin_model->setUpdateSetting($this->home, $data, $key);
		} 
        $j=0;
        $featurestitle=$this->input->post('featurestitle');
        $bottomtile=$this->input->post('bottomtile');
        $bottomdescription=$this->input->post('bottomdescription');
        $blogdescription=$this->input->post('blogdescription');
        $footerdescription=$this->input->post('footerdescription');
        $sitetitle=$this->input->post('sitetitle');
        $copyright=$this->input->post('copyright');
		$langPost=$this->input->post('lang');
		$homeid=$this->input->post('homeid');
		$featuredescription=$this->input->post('featuredescription');
		foreach($featurestitle as $settingsTitle) {
            $lang_id=$langPost[$j];
            $id=$homeid[$j];
			$data_content['featurestitle'] = $settingsTitle;
			$data_content['featuredescription'] = $featuredescription[$j];
			$data_content['bottomtile'] = $bottomtile[$j];
			$data_content['bottomdescription'] = $bottomdescription[$j];
			$data_content['blogdescription'] = $blogdescription[$j];
			$data_content['copyright'] = $copyright[$j];
			$data_content['sitetitle'] = $sitetitle[$j];
			$data_content['footerdescription'] = $footerdescription[$j];
			$data_content['language_id'] = $lang_id;
			$check_content=$this->admin_model->CheckContent($this->home_setting_content,$id,$lang_id);
			if($check_content==1){
				$this->admin_model->setUpdateSettingData($this->home_setting_content,$data_content,$id,$lang_id);
		    } else {
		    	$this->admin_model->setInsertData($this->home_setting_content,$data_content);
		    }
			$j++;
		}


		if($_FILES["featureicons"]["name"] != "")
		{
			  $logo=$this->do_icons('home','featureicons');				 
			  $datalogo = array('value' => $logo );
			  $this->admin_model->setUpdateData($this->home,$datalogo,3);
		}

		if($_FILES["bottomicons"]["name"] != "")
		{
			  $logo=$this->do_icons('home','bottomicons');				 
			  $datalogo = array('value' => $logo );
			  $this->admin_model->setUpdateData($this->home,$datalogo,4);
		}
				

		$this->session->set_flashdata('message','Your home settings has been update successfully');
        redirect('admin/settings/home','refresh');
		    
        }
        $data['settingsData'] = $this->master_model->get_records($this->home_setting_content);
        $data['homeData'] = $this->master_model->get_records($this->home);
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/home', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function change_content() {

        $data['contentData'] = $this->admin_model->getDataCollection($this->static_content_management);
    	$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/change_content', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function add()
	{
		CheckAdminLoginSession();
        $post_data=$this->input->post();
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('variable[0]', 'variable', 'required');
			$this->form_validation->set_rules('description[0]', 'description', 'required');				
			if($this->form_validation->run() == FALSE) {   } else {

                for ($i=0; $i < 5 ; $i++) {                 
					$data['variable']=$this->input->post('variable[0]');
					$data['description']=$this->input->post('description['.$i.']');
					$data['language_id']=$this->input->post('lang['.$i.']');
					$this->admin_model->setInsertData($this->static_content_management, $data);
					$this->save_language_content_by_key($data['language_id'],$data['variable'],$data['description']);
				}
                //print_r($post_data);
				$this->session->set_flashdata('message','Your content has been add successfully');
	        	redirect('admin/settings/change_content','refresh');
			}   
        }

        $data['language']=$this->admin_model->getLanguageCollection('tbl_language');
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/add', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function edit()
	{
		CheckAdminLoginSession();
        $post_data=$this->input->post();
        $id=$this->uri->segment(4);
		if(!empty($post_data)) {        
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('description', 'description', 'required');				
			if($this->form_validation->run() == FALSE) {   } else {                
				$lang_id=$this->input->post('language_id');
				$lang_key=$this->input->post('variable');
				$lang_desc=$this->input->post('description');
				$data['description']=$this->input->post('description');
				$this->save_language_content_by_key($lang_id,$lang_key,$lang_desc);
				$this->admin_model->setUpdateData($this->static_content_management, $data, $id);
				$this->session->set_flashdata('message','Your content has been update successfully');
	        	redirect('admin/settings/change_content','refresh');
			}   
        }

        $data['editData']=$this->admin_model->getDataCollectionByID($this->static_content_management,$id);
        $data['language']=$this->admin_model->getLanguageCollection('tbl_language');
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/edit', $data);
		$this->load->view('admin/include/footer', $data);
	}

	public function smtp()
	{
        CheckAdminLoginSession();
        $post_data=$this->input->post();
		if(!empty($post_data)) {
			$this->form_validation->set_error_delimiters('<span class="textdanger">', '</span>');
			$this->form_validation->set_rules('smtp_user', ' smtp user', 'required');
			$this->form_validation->set_rules('smtp_pass', 'smtp password', 'required');		
			$this->form_validation->set_rules('smtp_host', ' smtp host', 'required');
			$this->form_validation->set_rules('smtp_port', 'smtp port', 'required');				
			if($this->form_validation->run() != FALSE)
			{
				foreach ($post_data as $key => $value)
				{
					$keyExist = $this->master_model->isExist($this->tbl_name,array('name'=>$key));
					if(!empty($keyExist))
					{
						$this->master_model->setUpdateRecord($this->tbl_name, array('value' => $value), array('name' => $key));
					}
					else
					{
						$this->master_model->setInsertData($this->tbl_name, array('name'=>$key,'value' => $value));
					}
				}

				$this->session->set_flashdata('notification', array('error' => 0, 'message' => 'SMTP Settings has been added successfully !!'));
		        redirect('admin/settings/smtp','refresh');
		    }
        }
        $data['smtpData'] = $this->admin_model->getSettingsDataCollection($this->tbl_name);
		$this->load->view('admin/include/header', $data);
		$this->load->view('admin/include/sidebar', $data);
		$this->load->view('admin/settings/smtp', $data);
		$this->load->view('admin/include/footer', $data);
	}
}