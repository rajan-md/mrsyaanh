<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->lang->load('front','english');
        $this->load->model('front_model');
        $this->load->model('services_model');   
        $this->load->helper('front_helper');
        $this->load->model('smtp_model');        
    }

    public $users='tbl_users';
    public $subscribers='tbl_subscribers';    
    public $users_address='tbl_users_address';
    public $services='tbl_services';
    public $services_area='tbl_services_area';
    public $provider_request='tbl_service_provider_request';
    public $provider_request_setting='tbl_service_provider_settings';

	public function index()
    {
        $subscribers = get_records($this->subscribers,array('subscription_group'=>2,'status'=>1));
        $posts = getTodayBlogPosts();

        if(!empty($posts) && !empty($subscribers))
        {
            $articles = '<table border="1" style="border-collapse: collapse;" bordercolor="#CCC" width="100%" cellpadding="5" cellspacing="5">';
            foreach ($posts as $blog)
            {
               $articles.= '<tr>';
               $articles.= '<td valign="top" align="left"><h3><a href="'.base_url('blog/'.$blog->slug).'">'.$blog->name.'</a></h3><hr><i><strong>Posted On</strong> : '.date('d M, Y h:i A',strtotime($blog->created)).'</i><hr>'.substr($blog->description,0,300).' <a href="'.base_url('blog/'.$blog->slug).'">Read more...</a></td>';
               $articles.= '</tr>';
            }
            $articles.= '</table>';

            $email_template  = 'crone_blog.html';
            $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}' => 'firstchoice.com',
            '{{site_url}}'  => base_url(),
            '{{today_date}}'  => date('d F, Y'),
            '{{articles}}'  => $articles,
            '{{team_name}}' =>'firstchoice',
            '{{year}}'      =>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
            $message = email_compose($email_template,$templateTags);
            foreach ($subscribers as $rec)
            {
                if(!empty($rec->subscriber_email))
                {
                    send_email($rec->subscriber_email,'Blog Aricles Notification',$message);
                }
            }
        }
    }

    public function service()
    {
        $subscribers = get_records($this->subscribers,array('subscription_group'=>1,'status'=>1));

        echo "<pre>"; print_r($subscribers); echo "</pre>"; die;


        $posts = getTodayBlogPosts();

        if(!empty($posts) && !empty($subscribers))
        {
            $articles = '<table border="1" style="border-collapse: collapse;" bordercolor="#CCC" width="100%" cellpadding="5" cellspacing="5">';
            foreach ($posts as $blog)
            {
               $articles.= '<tr>';
               $articles.= '<td valign="top" align="left"><h3><a href="'.base_url('blog/'.$blog->slug).'">'.$blog->name.'</a></h3><hr><i><strong>Posted On</strong> : '.date('d M, Y h:i A',strtotime($blog->created)).'</i><hr>'.substr($blog->description,0,300).' <a href="'.base_url('blog/'.$blog->slug).'">Read more...</a></td>';
               $articles.= '</tr>';
            }
            $articles.= '</table>';

            $email_template  = 'crone_blog.html';
            $templateTags =  array(
            '{{site_logo}}' => base_url().'skin/front/images/logo.png',
            '{{site_name}}' => 'firstchoice.com',
            '{{site_url}}'  => base_url(),
            '{{today_date}}'  => date('d F, Y'),
            '{{articles}}'  => $articles,
            '{{team_name}}' =>'firstchoice',
            '{{year}}'      =>date('Y'),
            '{{company_name}}' => 'firstchoice.com',
            '{{company_email}}' => 'info@firstchoice.com',
            );
            $message = email_compose($email_template,$templateTags);
            foreach ($subscribers as $rec)
            {
                if(!empty($rec->subscriber_email))
                {
                    send_email($rec->subscriber_email,'Blog Aricles Notification',$message);
                }
            }
        }
    }

}