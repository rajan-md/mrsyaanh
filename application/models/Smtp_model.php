<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smtp_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getSmtp($name)
	{
      $this->db->select('value');
	  $this->db->where('name', $name);
	  $query = $this->db->get('tbl_smtp_settings');	
	  $result = $query->row();
	  return $result->value;

	}
}
?>