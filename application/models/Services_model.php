<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function setUpdateSettingStatusByRequestId($table, $data, $id)
    {
        $array = array(
            'services_request_id' => $id
        );
        $this->db->where($array);
        $this->db->update($table, $data);
        return $id;
    }
    
    public function verifyServiceZipcode($zipcode)
    {
        $this->db->where('zipcode', $zipcode);
        $query = $this->db->get('tbl_services_area');
        $count = $query->num_rows();
        if ($count > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function setUpdatePriceContentData($table, $data, $id, $language_id)
    {
        
        $this->db->where('service_price_id', $id);
        $this->db->where('language_id', $language_id);
        $this->db->update($table, $data);
        return $id;
    }
    
    public function CheckPriceContent($table, $id, $language_id)
    {
        
        $this->db->where('service_price_id', $id);
        $this->db->where('language_id', $language_id);
        $query = $this->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function CheckContent($table, $id, $language_id)
    {
        
        $this->db->where('services_id', $id);
        $this->db->where('language_id', $language_id);
        $query = $this->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function getCollection()
    {
        
        $query = $this->db->select('p1.id as id,p1.parent as parent, p1.created as created,p1.feature as feature, p1.status as status , p2.name as name, p2.description as description')->from('tbl_services as p1')->join('tbl_services_content as p2', 'p1.id = p2.services_id', 'LEFT')->where('p2.language_id', 1)->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    
    public function getServiceReqestDataCollection()
    {
        
        $query = $this->db->select('*')->from('tbl_service_orders')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiceReqestDetailById($id = 0)
    {
        
        $query = $this->db->select('p1.*')->from('tbl_service_orders as p1')->where('p1.id', $id)->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesPriceDataCollection()
    {
        
        /*$query= $this->db->select('p1.id as id,p1.service_id as service_id, p1.created as created, p1.status as status , p2.name as name, p1.service_id as service_id, p1.price as price, p1.availability as availability')
        ->from('tbl_services_price as p1')
        ->join('tbl_services_price_content as p2', 'p1.id = p2.service_price_id', 'LEFT')
        ->where('p2.language_id', 1)
        ->get();*/
        $query = $this->db->select('*')->from('tbl_services_price')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderRequestCounts()
    {
        $query = $this->db->select('s.*, ss.service_zip_code, u.site_location, u.country, u.state, u.city, u.postal_code, u.created, u.status')->from('tbl_service_provider_request as s')->join('tbl_users as u', 's.user_id = u.id', 'LEFT')->join('tbl_service_provider_settings as ss', 's.id = ss.services_request_id', 'LEFT')->group_by('s.id')->get();
        return $count = $query->num_rows();
    }
    
    public function getServiesProviderRequestDataCollection($limit = 0, $start = 0)
    {
        if (!empty($limit))
            $this->db->limit($limit, $start);
        
        $this->db->select('s.*, ss.service_zip_code, u.site_location, u.country, u.state, u.city, u.postal_code, u.created, u.status');
        $this->db->from('tbl_service_provider_request as s');
        $this->db->join('tbl_users as u', 's.user_id = u.id', 'LEFT');
        $this->db->join('tbl_service_provider_settings as ss', 's.id = ss.services_request_id', 'LEFT');
        $this->db->group_by('s.id');
        $this->db->order_by('s.id', 'desc');
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    
    public function getServiesProviderDetails($id)
    {
        
        $this->db->select('s.*, u.username, u.fname, u.lname,u.email,u.mobile, u.featured_img, u.site_location, u.country, u.state, u.city, u.postal_code');
        $this->db->from('tbl_service_provider_request as s');
        $this->db->join('tbl_users as u', 's.user_id = u.id', 'LEFT');
        $this->db->join('tbl_service_provider_settings as ss', 's.id = ss.services_request_id', 'LEFT');
        $this->db->where('s.user_id', $id);
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $row = $query->row();
            return $row;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderServicesDataCollection($request_id)
    {
        $query = $this->db->select('s.*, r.business, r.business_name, u.site_location, u.country, u.state, u.city, u.postal_code')->from('tbl_service_provider_settings as s')->join('tbl_service_provider_request as r', 's.services_request_id = r.id', 'LEFT')->join('tbl_users as u', 'r.user_id = u.id', 'LEFT')->where('s.services_request_id', $request_id)->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderRequestDetails($id = 0)
    {
        $query = $this->db->select('s.id,s.user_id,s.services_type,s.bussiness_holder_fname,s.bussiness_holder_lname,s.license_details,s.license,s.business,s.designation,s.business_email,s.business_mobile,s.business_fax,s.business_address,s.business_aptno,s.business_street_no,s.business_street_name,s.business_street_type,s.business_direction,s.business_country,s.business_state,s.business_city,s.business_zip,s.business_name,s.business_type,s.driver_licence,s.car_made,s.car_model,s.car_year,s.car_color,s.car_number,s.car_picture, u.*')->from('tbl_service_provider_request as s')->join('tbl_users as u', 's.user_id = u.id', 'LEFT')->where('s.id', $id)->group_by('s.id')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getRequestDetails($id = 0)
    {
        $query = $this->db->select('r.*,s.service_id,s.service_zip_code')->from('tbl_service_provider_request as r')->join('tbl_service_provider_settings as s', 's.services_request_id = r.id', 'LEFT')->where('r.id', $id)->group_by('r.id')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiceRequestDetails($id = 0)
    {
        $query = $this->db->select('r.*,s.service_id,s.service_zip_code')->from('tbl_service_provider_request as r')->join('tbl_service_provider_settings as s', 's.services_request_id = r.id', 'LEFT')->where('r.id', $id)->group_by('r.id')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderRequestDetailsByUserId($id = 0)
    {
        $query = $this->db->select('s.id,s.user_id,s.services_type,s.license_details,s.license,s.business,s.designation, s.business_name,u.fname,u.lname')->from('tbl_service_provider_request as s')->join('tbl_users as u', 's.user_id = u.id', 'LEFT')->where('s.user_id', $id)->group_by('s.id')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderRequestDetails2($id = 0)
    {
        $query = $this->db->select('s.id,s.user_id,s.services_type,s.license_details,s.license,s.business,s.designation, s.business_name, u.*')->from('tbl_service_provider_request as s')->join('tbl_users as u', 's.user_id = u.id', 'LEFT')->where('s.id', $id)->group_by('s.id')->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServiesProviderRequestServiceDetailsById($id = 0)
    {
        $query = $this->db->select('*')->from('tbl_service_provider_settings')->where('id', $id)->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return array();
        }
    }
    
    
    public function getUserDataByServiceSettingId($id)
    {
        $this->db->select('U.*');
        $this->db->from('tbl_users U');
        $this->db->join('tbl_service_provider_request R', 'U.id = R.user_id');
        $this->db->join('tbl_service_provider_settings S', 'R.id = S.services_request_id');
        $this->db->where('S.id', $id);
        $query = $this->db->get();
        $row   = $query->row_array();
        return $row;
    }
    
    public function getUserDataByServiceRequestId($id)
    {
        $this->db->select('U.*');
        $this->db->from('tbl_users U');
        $this->db->join('tbl_service_provider_request R', 'U.id = R.user_id');
        $this->db->where('R.id', $id);
        $query = $this->db->get();
        $row   = $query->row_array();
        return $row;
    }
    
    public function getZipcodeRecordByID($table, $id)
    {
        
        $this->db->where('id', $id);
        $query  = $this->db->get($table);
        $result = $query->row_array();
        return $result;
    }
    
    public function getDataByServiceRequestId($id)
    {
        $this->db->select('R.*,S.service_id,S.service_zip_code');
        $this->db->from('tbl_service_provider_request R');
        $this->db->join('tbl_service_provider_settings S', 'R.id = S.services_request_id');
        $this->db->where('R.id', $id);
        $query = $this->db->get();
        $row   = $query->row_array();
        return $row;
    }
    
    public function getServicesPricesDataCollectionByID($table, $id)
    {
        
        $this->db->where('id', $id);
        $query  = $this->db->get($table);
        $result = $query->row_array();
        return $result;
    }
    
    public function getServicePriceContent($table, $id)
    {
        $this->db->select('*');
        $this->db->where('service_price_id', $id);
        $query = $this->db->get($table);
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getSingleServicesDataCollection($language_id = 0, $id = 0)
    {
        
        $this->db->select('p.*, c.description as description, c.name as name');
        $this->db->from('tbl_services as p');
        $this->db->join('tbl_services_content as c', 'p.id = c.services_id', 'LEFT');
        $this->db->where('p.id', $id);
        $this->db->where('c.language_id', $language_id);
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServicesOptionList($language_id = 0, $services_id = 0, $limit = 0)
    {
        
        $this->db->select('p.*, c.name as name');
        $this->db->from('tbl_services_price as p');
        $this->db->join('tbl_services_price_content as c', 'p.id = c.service_price_id', 'LEFT');
        $this->db->where('p.service_id', $services_id);
        $this->db->where('p.status', 1);
        $this->db->where('c.language_id', $language_id);
        //$this->db->group_by('c.service_id');
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServicePricesList($services_id = 0, $limit = 0)
    {
        
        $this->db->select('*');
        $this->db->from('tbl_services_price');
        $this->db->where('service_id', $services_id);
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    
    public function getServicesPrices($service_id, $zipcode)
    {
        
        $this->db->select('*');
        $this->db->from('tbl_service_provider_settings');
        $this->db->where('service_id', $service_id);
        $areaCode = substr($zipcode, 0, 3);
        $this->db->where('( find_in_set("' . $zipcode . '", service_zip_code) <> 0  OR (service_zip_code LIKE "' . $areaCode . '%" AND is_allowed_all_area=1) )');
        $this->db->where('status', 1);
        $query = $this->db->get();
        $count = $query->num_rows();
        //echo $this->db->last_query(); die;
        if ($count > 0) {
            $row = $query->row();
            return $row;
        } else {
            return array();
        }
    }
    
    public function findServicesByZipCode($servicesarea = "")
    {
        $result  = array();
        $lang_id = getActiveLanguage();
        $this->db->select('c.name,s.id,s.slug');
        $this->db->from('tbl_services s');
        $this->db->join('tbl_services_content c', 'c.services_id = s.id');
        $this->db->join('tbl_language l', 'l.id = c.language_id');
        $this->db->join('tbl_service_provider_settings ss', 'ss.service_id = s.id');
        $this->db->where('l.id', $lang_id);
        $this->db->where('s.parent', 0);
        $areaCode = substr($servicesarea, 0, 3);
        $this->db->where('( find_in_set("' . $servicesarea . '", ss.service_zip_code) <> 0  OR (ss.service_zip_code LIKE "' . $areaCode . '%" AND ss.is_allowed_all_area=1) )');
        $this->db->where('ss.status', 1);
        $this->db->group_by('s.id');
        $query = $this->db->get();
        $count = $query->num_rows();
        
        //echo $this->db->last_query();
        
        if ($count > 0) {
            $result = $query->result();
        }
        return $result;
    }
}
?>