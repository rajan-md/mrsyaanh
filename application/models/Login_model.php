<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public $users = 'tbl_users';
    
    public function check_email()
    {
        $user_email = $this->input->post('useremail');
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where('email', $user_email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        } else {
            return array();
        }
    }
    
    public function UserLogin()
    {
        $login    = $this->input->post();
        $active   = 1;
        $uemail   = $login['useremail'];
        $password = md5($login['password']);
        $this->db->where('email', $uemail);
        $this->db->where('password', $password);
        $query    = $this->db->get($this->users);
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result   = $query->row();
            $id       = $result->id;
            $userdata = array(
                'userID' => $result->id,
                'user_name' => $result->username,
                'user_email' => $result->email
            );
            
            if ($result->status == 1) {
                $this->session->set_userdata($userdata);
                return 1;
            }
            if ($result->status == 2) {
                return $result->status;
            } else {
                return $result->status;
            }
        } else {
            return 0;
        }
    }
    
    public function UserForgot()
    {
        
        $login  = $this->input->post();
        $active = 1;
        $uemail = $login['useremail'];
        $this->db->where('email', $uemail);
        $query    = $this->db->get('users');
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result;
        } else {
            return 0;
        }
    }
}