<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function isExist($table, $whereArr = array())
    {
        if (!empty($whereArr))
            $this->db->where($whereArr);
        
        $this->db->from($table);
        $query    = $this->db->get();
        $rowCount = $query->num_rows();
        return (!empty($rowCount) && $rowCount > 0) ? 1 : 0;
    }
    
    public function setInsertData($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    
    public function setUpdateData($table, $data, $id)
    {
        $array = array(
            'id' => $id
        );
        $this->db->where($array);
        $this->db->update($table, $data);
        return $id;
    }
    
    public function setUpdateRecord($table, $data, $whereArr)
    {
        $this->db->where($whereArr);
        $this->db->update($table, $data);
    }
    
    public function DeleteByID($table, $id)
    {
        $array = array(
            'id' => $id
        );
        $this->db->where($array);
        $this->db->delete($table);
        return $id;
    }
    
    public function getStatusById($table, $id)
    {
        CI()->db->select('status');
        CI()->db->where('id', $id);
        $query    = CI()->db->get($table);
        $rowCount = $query->num_rows();
        if ($rowCount > 0) {
            $result = $query->row();
            return $result->status;
        } else {
            return false;
        }
    }
    
    public function checkNinsertData($table, $column, $data)
    {
        $query    = $this->db->select('*')->where($column, $data[$column])->from($table)->get();
        $rowCount = $query->num_rows();
        if ($rowCount < 1) {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }
    
    public function getDataCollectionByField($table = '', $field = '', $value = '')
    {
        $this->db->where($field, $value);
        $query  = $this->db->get($table);
        $result = $query->row_array();
        return $result;
    }
    
    public function deleteDataByField($table, $column, $val)
    {
        $this->db->where($column, $val);
        $this->db->delete($table);
    }
    
    public function get_total_records($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        return $count = $query->num_rows();
    }
    
    public function get_record($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        //echo CI()->db->last_query();
        $count = $query->num_rows();
        if ($count > 0) {
            return $query->row();
        } else {
            return array();
        }
        
    }
    
    public function get_recordArr($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        //echo CI()->db->last_query();
        $count = $query->num_rows();
        if ($count > 0) {
            return $query->row_array();
        } else {
            return array();
        }
        
    }
    
    public function get_records($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        //echo CI()->db->last_query();
        $count = $query->num_rows();
        if ($count > 0) {
            return $query->result();
        } else {
            return array();
        }
        
    }
    
    public function get_current_page_records($table, $limit, $start, $whereArr = array(), $orderBy = '', $order = 'DESC')
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        if (!empty($orderBy))
            CI()->db->order_by($orderBy, $order);
        
        if (!empty($limit))
            CI()->db->limit($limit, $start);
        
        CI()->db->from($table);
        $query = CI()->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function get_recordsArr($table, $whereArr = array())
    {
        if (!empty($whereArr))
            CI()->db->where($whereArr);
        
        CI()->db->select('*');
        CI()->db->from($table);
        $query = CI()->db->get();
        //echo CI()->db->last_query();
        $count = $query->num_rows();
        if ($count > 0) {
            return $query->result_array();
        } else {
            return array();
        }
        
    }
    
}
?>