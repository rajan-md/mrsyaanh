<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function AdminLogin()
    {
        $login = $this->input->post();
        $name  = $login['useremail'];
        $roles = getRoleByEmail($name);
        if (($roles == 3) || ($roles == 1)) {
            $password = md5($login['userpass']);
            $this->db->where('( username="' . $name . '" OR email="' . $name . '")');
            $this->db->where('password', $password);
            $this->db->where('role', $roles);
            $query    = $this->db->get('tbl_users');
            $rowCount = $query->num_rows();
            if ($rowCount > 0) {
                $result   = $query->row();
                $id       = $result->id;
                $userdata = array(
                    'admin_id' => $result->id,
                    'role' => $result->role,
                    'admin_name' => $result->name,
                    'admin_email' => $result->email
                );
                $this->session->set_userdata($userdata);
                return 1;
            } else {
                return 0;
            }
        } else {
            
            $this->session->set_flashdata('error', 'Access denied - You are not authorized to access');
            redirect('admin/login', 'refresh');
        }
    }
    
    public function getSettingsDataCollection($table) {
        $query = $this->db->get($table);
        $count = $query->num_rows();
        if($count>0){
            $result = $query->result();
            
            foreach ($result as $datalist) {
                $smtp[trim($datalist->name)]=$datalist->value;
            }
            return $smtp;
        }
        else{
            return array();
        }
    }

    public function getCurrencyOptions($table, $type)   {
        $options = array(''=>$type);
        if($table){
            $query = $this->db->get($table);
            $skills =  $query->result();
            foreach ($skills as $value) {
                $options[$value->id] = $value->name.' ( '.$value->symbol.' )';
            }
        }
        return $options;
    }

    public function IsUserActive()
    {
        $email = $this->input->post('useremail');
        $this->db->where('username', $email);
        $this->db->or_where('email', $email);
        $query = $this->db->get('tbl_users');
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->row();
            return $result->status;
        } else {
            return $count = 0;
        }
    }
}
?>