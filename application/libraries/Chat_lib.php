<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Library Chat_lib
 *
 * This class handles chatting related functionality
 *
 * @package     addChat
 * @author      classiebit
**/

class Chat_lib {

	var $CI_LIB;
	var $login_user_id 		= '';
	
    function __construct()
    {
        $this->CI_LIB =& get_instance();
        $this->CI_LIB->load->library('chat_data');
        $this->login_user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : NULL;
    }

    public function get_users()
    {
    	$id                 	= $this->login_user_id;

    	if($id)
    	{
    		$data['cur_user']   = $this->CI_LIB->chat_data->get_user($id);
	        $contacts           = $this->CI_LIB->chat_data->get_all_users();

	        foreach ($contacts as $key=>$contact) 
	        {
	            // get unread messages from this user
	            $unread 				= $this->CI_LIB->chat_data->unread_per_user($id, $contact->id); 
	            $contacts[$key]->unread = $unread > 0 ? $unread : null ; 
	        }

	        $data['users'] 		= $contacts;

	        $this->CI_LIB->load->view('chat/postlogin', $data);  	
    	}
    	else
    	{
    		$this->CI_LIB->load->view('chat/prelogin', TRUE);	
    	}
        
    }

    public function change_status()
    {
		$id 	= $this->login_user_id;
		$user 	= $this->CI_LIB->chat_data->get_user($id);
		$status = $user->online == '0' ? '1' : '0';
		$this->CI_LIB->chat_data->user_update($id, array('online'=>$status));	

		$response = array('success' => true, 'status'=> $status);
		// add the header here
		header('Content-Type: application/json');
		echo json_encode( $response );
	}
	
	public function get_messages()
	{
		//get paginated messages 
		$per_page 			= 5;
		$user 				= $this->login_user_id;
		$buddy 				= $this->CI_LIB->input->post('user');
		$limit 				= isset($_POST['limit']) ? $this->CI_LIB->input->post('limit') : $per_page ;
		$messages 			= array_reverse($this->CI_LIB->chat_data->conversation($user, $buddy, $limit));
		$total 				= $this->CI_LIB->chat_data->thread_len($user, $buddy);

		$thread 			= array();
		foreach ($messages as $message) 
		{
			$owner 			= $this->CI_LIB->chat_data->get_user($message->from);
			$chat = array(
				'msg' 		=> $message->id,
				'sender' 	=> $message->from, 
				'recipient' => $message->to,
				'avatar' 	=> $owner->avatar != '' ? $owner->avatar : '',
				'body' 		=> $message->message,
				'time' 		=> date("M j, Y, g:i a", strtotime($message->time)),
				'type'		=> $message->from == $user ? 'out' : 'in',
				'name'		=> $message->from == $user ? 'You' : ucwords($owner->firstname)
				);
			array_push($thread, $chat);
		}

		$chatbuddy = $this->CI_LIB->chat_data->get_user($buddy);

		$contact = array(
			'name'=>ucwords($chatbuddy->firstname.' '.$chatbuddy->lastname),
			'status'=>$chatbuddy->online,
			'id'=>$chatbuddy->id,
			'limit'=>$limit + $per_page,
			'more' => $total  <= $limit ? false : true, 
			'scroll'=> $limit > $per_page  ?  false : true,
			'remaining'=> $total - $limit
			);


		$response = array(
					'success' => true,
					'errors'  => '',
					'message' => '',
					'buddy'	  => $contact,
					'thread'  => $thread
					);
		//add the header here
		header('Content-Type: application/json');
		echo json_encode( $response );
	}
	
	public function send_message()
	{
		$logged_user = $this->login_user_id;
		$buddy 		= $this->CI_LIB->input->post('user');
		$message 	= nl2br($this->CI_LIB->input->post('message'));

		if($message != '' && $buddy != '')
		{
			$msg    = array(
						'from' 		=> $logged_user,
						'to' 		=> $buddy,
						'message' 	=> $message,
						'time' 		=> date('Y-m-d H:i:s'),
					);
			
			$msg_id = $this->CI_LIB->chat_data->insert_message($msg);
			
			$owner = $this->CI_LIB->chat_data->get_user($msg['from']);
			$chat = array(
				'msg' 		=> $msg_id,
				'sender' 	=> $msg['from'], 
				'recipient' => $msg['to'],
				'avatar' 	=> $owner->avatar != '' ? $owner->avatar : '',
				'body' 		=> $msg['message'],
				'time' 		=> date("M j, Y, g:i a", strtotime($msg['time'])),
				'type'		=> $msg['from'] == $logged_user ? 'out' : 'in',
				'name'		=> $msg['from'] == $logged_user ? 'You' : ucwords($owner->firstname)
				);

			$response = array(
				'success' => true,
				'message' => $chat 	  
				);
		}
		else
		{
			$response = array(
				'success' => false,
				'message' => 'Empty fields exists'
			);
		}
		//add the header here
		header('Content-Type: application/json');
		echo json_encode( $response );
	}

	public function get_updates()
	{
	    $new_exists = false;
		$user_id 	= $this->login_user_id;
		$last_seen  = $this->CI_LIB->chat_data->get_last_seen($user_id);
		$last_seen  = empty($last_seen) ? 0 : $last_seen->message_id;
		$exists 	= $this->CI_LIB->chat_data->latest_message($user_id, $last_seen);
		
		if($exists){
			$new_exists = true;
		}
		// THIS WHOLE SECTION NEED A GOOD OVERHAUL TO CHANGE THE FUNCTIONALITY
	    if ($new_exists) 
	    {
	        $new_messages = $this->CI_LIB->chat_data->unread($user_id);
	        $thread = array();
			$senders = array();
			foreach ($new_messages as $message) {
				if(!isset($senders[$message->from])){
					$senders[$message->from]['count'] = 1; 
				}
				else{
					$senders[$message->from]['count'] += 1; 
				}
				$owner = $this->CI_LIB->chat_data->get_user($message->from);
				$chat = array(
					'msg' 		=> $message->id,
					'sender' 	=> $message->from, 
					'recipient' => $message->to,
					'avatar' 	=> $owner->avatar != '' ? $owner->avatar : '',
					'body' 		=> $message->message,
					'time' 		=> date("M j, Y, g:i a", strtotime($message->time)),
					'type'		=> $message->from == $user_id ? 'out' : 'in',
					'name'		=> $message->from == $user_id ? 'You' : ucwords($owner->firstname)
					);
				array_push($thread, $chat);
			}

			$groups = array();
			foreach ($senders as $key=>$sender) {
				$sender = array('user'=> $key, 'count'=>$sender['count']);
				array_push($groups, $sender);
			}
			// END OF THE SECTION THAT NEEDS OVERHAUL DESIGN
			$this->CI_LIB->chat_data->update_lastSeen($user_id);

			if(!empty($thread) && !empty($groups))
			{
				$response = array(
					'success' => true,
					'messages' => $thread,
					'senders' =>$groups
				);
				//add the header here
				header('Content-Type: application/json');
				echo json_encode( $response );exit;
			}
	    } 
	}
	
	public function seen_message()
	{
		$this->CI_LIB->chat_data->seen_message();
	}
}

/*End Chat_lib Class*/