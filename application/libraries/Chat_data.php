<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Library Chat_data
 *
 * This class handles chatting data processing functionality
 *
 * @package     addChat
 * @author      classiebit
**/

class Chat_data
{
    public $_database;

    /*
    * DB Configuration
    */
    // users table
    var $vehicle_request  = 'vehicle_request';
    var $user_table       = 'users';
    var $user_id_field    = 'id';
    var $username_field   = 'username';
    var $firstname_field  = 'fname';
    var $lastname_field   = 'lname';
    var $avatar_field     = 'featured_img';
    var $email_field      = 'email';
    var $online_field      = 'online';
    
    // addchat_messages table
    var $addchat_messages   = 'addchat_messages';

    // addchat_users table
    var $addchat_users      = 'addchat_users';
    
    public function __construct()
    {
        $this->CI_LIB =& get_instance();

        $this->CI_LIB->load->helper('inflector');

        $this->_database = $this->CI_LIB->db;
    }

    public function get_user($user_id = 0)
    {
        return  $this->_database->select(array(
                            "$this->user_table.$this->user_id_field id",
                            "$this->user_table.$this->username_field username",
                            "$this->user_table.$this->firstname_field firstname",
                            "$this->user_table.$this->lastname_field lastname",
                            "$this->user_table.$this->avatar_field avatar",
                            "$this->user_table.$this->email_field email",
                            "$this->user_table.online",
                        ))
                        ->where("$this->user_table.$this->user_id_field", $user_id)
                        //->where("$this->user_table.$this->user_id_field!=1")
                        ->get($this->user_table)
                        ->row();
    }

   

    public function get_user_in_connection($user_id)  {
        $support_id=isset($_SESSION['support_id']) ? $_SESSION['support_id'] : NULL;    
        $user_id=isset($_SESSION['userID']) ? $_SESSION['userID'] : NULL;    
        $this->_database->select("r.request_by as request_by, r.accept_by as accept_by");
        $this->_database->from('users as u');
        $this->_database->join('vehicle_request as r', 'u.id = r.request_by', 'right');
        $this->_database->where("r.accept",1);
        $this->_database->where("r.request_by",$user_id);
        $this->_database->or_where("r.accept_by",$user_id);
        $query= $this->_database->get();
        $results= $query->result(); 
    
        $arrayUser=array();
        foreach ($results as $data) {            
            $arrayUser[$data->request_by]=$data->request_by;
            $arrayUser[$data->accept_by]=$data->accept_by;
        }

        $arrayUser[1]=$support_id;

        //return implode(',', $arrayUser); 
        return $arrayUser; 
    } 

    public function get_all_users()
    {
        $user_id=isset($_SESSION['userID']) ? $_SESSION['userID'] : NULL; 
        $ids=$this->get_user_in_connection($user_id);
        //print_r($ids);        
        $this->_database->select(array(
            "$this->user_table.$this->user_id_field id",
            "$this->user_table.$this->username_field username",
            "$this->user_table.$this->firstname_field firstname",
            "$this->user_table.$this->lastname_field lastname",
            "$this->user_table.$this->avatar_field avatar",
            "$this->user_table.$this->email_field email",
            "$this->user_table.online",
        ));
        //->where("online",1)
        //->where("online",1)
       $support_id=isset($_SESSION['support_id']) ? $_SESSION['support_id'] : NULL;
       if($user_id!=$support_id){
        $this->_database->where_in("$this->user_table.$this->user_id_field", $ids);
       }
       $query=$this->_database->get($this->user_table);
       $result=$query->result();
       return $result;
    }

    public function user_update($user_id = 0, $data = array())
    {
        $this->_database->where("$this->user_table.$this->user_id_field", $user_id)->update('users', $data);
    }

    public function insert_message($data = array()) 
    {
        $this->_database->insert($this->addchat_messages, $data);
        return $this->_database->insert_id();
    }

    public function get_last_seen($user_id)
    {
        return $this->_database->where('user_id', $user_id)
                        ->get($this->addchat_users)
                        ->row();
    }

    public function conversation($user, $chatbuddy, $limit = 5)
    {
        $this->_database->where('from', $user);
        $this->_database->where('to', $chatbuddy);
        $this->_database->or_where('from', $chatbuddy);
        $this->_database->where('to', $user);
        $this->_database->order_by('id', 'desc');
        $messages = $this->_database->get($this->addchat_messages, $limit);

        $this->_database->where('to', $user)->where('from',$chatbuddy)->update($this->addchat_messages, array('is_read'=>'1'));
        return $messages->result();
    }
    
    public function thread_len($user, $chatbuddy)
    {
        $this->_database->where('from', $user);
        $this->_database->where('to', $chatbuddy);
        $this->_database->or_where('from', $chatbuddy);
        $this->_database->where('to', $user);
        $this->_database->order_by('id', 'desc');
        $messages = $this->_database->count_all_results($this->addchat_messages);
        return $messages;
    }

    public function latest_message($user, $last_seen)
    {
        $message  =  $this->_database->where('to', $user)
                              ->where('id  > ', $last_seen)
                              ->order_by('time', 'desc')
                              ->get($this->addchat_messages, 1);

        if($message->num_rows() > 0)
            return true;
        else
            return false;
    }

    public function new_messages($user, $last_seen)
    {
        $messages  =  $this->_database->where('to', $user)
                              ->where('id  > ', $last_seen)
                              ->order_by('time', 'asc')
                              ->get($this->addchat_messages);

        return $messages->result();
    }

    public function unread($user)
    {
        $messages  =  $this->_database->where('to', $user)
                              ->where('is_read', '0')
                              ->order_by('time', 'asc')
                              ->get($this->addchat_messages);

        return $messages->result();
    }

    public function seen_message()
    {
        $id = $this->CI_LIB->input->post('id');
        $this->_database->where('id', $id)->update($this->addchat_messages, array('is_read'=>'1'));
    }

    public function unread_per_user($id, $from)
    {
        $count  =  $this->_database->where('to', $id)
                            ->where('from', $from)
                            ->where('is_read', '0')
                            ->count_all_results($this->addchat_messages);
        return $count;
    }
    
    public function update_lastSeen($user=0)
    {
        $last_msg = $this->_database->where('to', $user)->order_by('id', 'DESC')->get($this->addchat_messages, 1)->row();
        $msg = !empty($last_msg) ? $last_msg->id : 0;

        $record = $this->get_last_seen($user);
        $details = array('user_id' => $user,'message_id' => $msg);

        if(empty($record))
            $this->_database->insert($this->addchat_users, $details);
        else
            $this->_database->where('id', $record->id)->update($this->addchat_users, $details);
    }
    
}

/*End Chat_data class*/