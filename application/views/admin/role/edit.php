<div class="page-content pad-top-zero">
    <div class="content container">
      
      <?php 
      if(!empty($record))
      {
        extract($record);
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Edit Role </h3>
            </div>
            <div class="widget-content">
              <form method="post" class="form-horizontal">
                <fieldset>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Name</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" value="<?=set_value('name', isset($name)?$name:""); ?>" id="normal-field">
                       <?php echo form_error('name'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                        <textarea class="form-control ckeditor"  id="text" name="description" rows="6"><?=set_value('description', isset($description)?$description:""); ?></textarea>
                         <?php echo form_error('description'); ?>
                    </div>
                    </div>
                  </div>
                </fieldset>

                <div class="form-actions">
                  <div>
                    <button class="btn btn-success" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
