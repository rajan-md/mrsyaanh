<?php $controller = $this->uri->segment(2); ?>
<div class="page-content">
    <div class="content container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-title">Coupons <?=AddButton('add')?></h2>
                <?php getNotificationHtml(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-table"></i>
                        <h3>Coupons Management</h3>
                    </div>
                    <div class="widget-content">
                        <div class="body">
                            <table class="table table-striped table-images" id="example">
                                <thead>
                                    <tr>
                                        <th width="50">#</th>
                                        <th>Coupon</th>
                                        <th width="100">Discount</th>
                                        <th>Duration</th>
                                        <th width="100">Min Target</th>
                                        <th width="80">Status</th>
                                        <th width="200">Created</th>
                                        <th>Action</th>
                                      </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($records))
                                    {
                                        foreach ($records as $rec)
                                        {
                                          ?>
                                          <tr>
                                            <td><?=$rec->id?></td>
                                            <td><?=$rec->coupon_code?></td>
                                            <?php
                                            $typeArr = getDiscountTypes();
                                            $type = (!empty($rec->type) && array_key_exists($rec->type,$typeArr))?' ('.$typeArr[$rec->type].')':'';
                                            ?>
                                            <td><?=(!empty($rec->type) && $rec->type=='percent')?$rec->coupon_discount.'%':getCurrencySymbol().$rec->coupon_discount;?></td>
                                            <td>
                                              <?php if(!empty($rec->always) && $rec->always==1){ echo 'Always'; } else { echo date('d M, Y',strtotime($rec->valid_from)).' - '.date('d M, Y',strtotime($rec->valid_to)); } ?>
                                            </td>
                                            <td><?=getCurrencySymbol().number_format($rec->minimum_target,2);?></td>                                            
                                            <td><?=statusButton($controller,'status',$rec->status,$rec->id); ?></td>
                                            <td class="hidden-xs"><?=date('d M, Y h:i A',strtotime($rec->added_date)); ?></td>
                                            <td><?=AllowedAction($controller, 'edit', 'delete', 'Sure you want to delete this record ?', $rec->id) ?></td>
                                          </tr>
                                          <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>