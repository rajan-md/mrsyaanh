<div class="page-content pad-top-zero">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add New Coupon </h3>
            </div>
            <div class="widget-content">
              <?php getNotificationHtml(); ?>
              <form method="post" class="form-horizontal">
                <fieldset>                  
                  <div class="control-group">
                    <div class="col-md-2">
                      <label for="normal-field" class="control-label">Coupon Code</label>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" name="coupon_code" class="form-control" value="<?=set_value('coupon_code', isset($coupon_code)?$coupon_code:""); ?>" id="coupon_code">
                        <?php echo form_error('coupon_code'); ?>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Discount</label>
                    </div>
                    <div class="col-md-1" style="padding-right:0px;">
                      <div class="form-group">
                        <input type="text" name="coupon_discount" class="form-control" value="<?=set_value('coupon_discount', isset($coupon_discount)?$coupon_discount:""); ?>" id="coupon_discount">
                        <?php echo form_error('coupon_discount'); ?>
                      </div>
                    </div>
                    <div class="col-md-2" style="padding-left:5px;">
                      <div class="form-group">
                        <?php echo form_dropdown('type', getDiscountTypes(),set_value('type',isset($type)?$type:""),'id="type" class="form-control"'); ?>
                      <?php echo form_error('type'); ?>
                      </div>
                    </div>

                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Validity From</label>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" name="valid_from" class="datepicker form-control" value="<?=set_value('valid_from', isset($valid_from)?$valid_from:""); ?>" id="valid_from" placeholder="yyyy-mm-dd">
                      <?php echo form_error('valid_from'); ?>
                    </div>
                    </div>
                    <div class="col-md-2"><label>Validity To</label></div>
                    <div class="col-md-3">
                    <div class="form-group">
                      <input type="text" name="valid_to" class="datepicker form-control" value="<?=set_value('valid_to', isset($valid_to)?$valid_to:""); ?>" id="valid_to" placeholder="yyyy-mm-dd">
                      <?php echo form_error('valid_to'); ?>
                    </div>
                    </div>

                  </div>

                  <div class="control-group">
                    <div class="col-md-2">
                      <label for="normal-field" class="control-label">Minimum Target</label>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" name="minimum_target" class="form-control" value="<?=set_value('minimum_target', isset($minimum_target)?$minimum_target:""); ?>" id="minimum_target">
                        <?php echo form_error('minimum_target'); ?>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Restrict Category</label>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <?php echo form_dropdown('restrict_cat', $servicesOptions,set_value('restrict_cat',isset($restrict_cat)?$restrict_cat:""),'id="restrict_cat" class="form-control"'); ?>
                        <?php echo form_error('restrict_cat'); ?>
                      </div>
                    </div>
                  </div>
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-success" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
