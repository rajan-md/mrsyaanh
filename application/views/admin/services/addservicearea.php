<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
<div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Service Area</h2> 
          
        </div>
      </div>
       <div class="row">
        <div class="col-md-12">
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div> 
      <?php 
      
      if(!empty($servicesData))
      {
        extract($servicesData);
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add Zip/Postal Code </h3>
            </div>
            <div class="widget-content">

               <form method="post" class="form-horizontal" enctype="multipart/form-data">

                <fieldset>
                  <div class="control-group">
                    <div class="col-md-4"></div>
                  <div id="seleData" class="col-md-8"></div>
                  </div>
                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> Service Area (Zip/Postal Code) </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="servicearea" name="servicearea" class="form-control"  placeholder="Search service area">
                            <?php echo form_error('servicearea'); ?>
                        </div>
                      </div>
                  </div>
                  
                   <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> Latitude </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="latitude" name="latitude" class="form-control"  placeholder="Latitude">
                            <?php echo form_error('latitude'); ?>
                        </div>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> Longitude </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="longitude" name="longitude" class="form-control"  placeholder="Longitude">
                            <?php echo form_error('longitude'); ?>
                        </div>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> Country </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="country" name="country" class="form-control"  placeholder="Country">
                            <?php echo form_error('country'); ?>
                        </div>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> State </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="administrative_area_level_1" name="state" class="form-control"  placeholder="State">
                            <?php echo form_error('state'); ?>
                        </div>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> City </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="locality" name="city" class="form-control"  placeholder="City">
                            <?php echo form_error('city'); ?>
                        </div>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                      <div class="col-md-4">
                        <label for="hint-field" class="control-label"> Zip/Postal Code </label>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" id="postal_code" name="postal_code" class="form-control"  placeholder="Zipcode">
                            <?php echo form_error('postal_code'); ?>
                        </div>
                      </div>
                  </div>
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>

<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

$("#servicearea").on('focus', function () {
    geolocate();
});

var placeSearch, autocomplete;
var componentForm = {
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initialize() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('servicearea')), {
        types: ['geocode']
    });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
    });
}

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
  //console.log(place);

    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = new google.maps.LatLng(
            position.coords.latitude, position.coords.longitude);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
        });
    }

}

initialize();
// [END region_geolocation]
  </script>