<div class="page-content pad-top-zero">
    <div class="content container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
      <?php 
      
      if(!empty($servicesData))
      {
        extract($servicesData);
      }

      //echo "<pre>"; print_r($services_content); echo "</pre>";
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Edit Service Price </h3>
            </div>
            <div class="widget-content">
               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  

                  <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language))
                          {
                           foreach ($language as $lang)
                            {
                            ?>                         
                            <li class="<?=$lang->default?>">
                              <a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a>
                            </li>
                           <?php
                            }
                          }
                          ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <div class="control-group">
                  <div class="col-md-2">
                    <label for="disabled-input" class="control-label ">Services</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <?php echo form_dropdown('parent', $servicesOptions,set_value('parent',isset($service_id)?$service_id:""),'id="parent" class="form-control"'); ?>
                      <?php echo form_error('parent'); ?>
                    </div>
                    </div>
                  </div>
                    <?php 
                    $i=0;
                    if(!empty($language))
                    {
                     foreach ($language as $lang)
                     {
                     $lid=$lang->id;                                
                     ?> 
                        <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                          <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                            <div class="control-group">
                             <div class="col-md-2">
                            <label for="normal-field" class="control-label">Name</label>
                            </div>
                            <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" name="name[]" class="form-control" value="<?=!empty($services_content[$lang->id])?$services_content[$lang->id]:""; ?>" >
                               <?php echo form_error('name'); ?>
                            </div>
                            </div>
                          </div>
                        </div>
                       <?php
                     }
                   }
                   ?>
                    
                    <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Price</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="price" class="form-control" value="<?=set_value('price',isset($price)?$price:""); ?>" id="normal-field">
                       <?php echo form_error('price'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Availability</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="availability" class="form-control" value="<?=set_value('availability',isset($availability)?$availability:""); ?>" id="normal-field">
                       <?php echo form_error('availability'); ?>
                    </div>
                    </div>
                  </div>
                    
                  </div>
                </div>
              </div>

                  
                  
                  
                  
                   
                  
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
