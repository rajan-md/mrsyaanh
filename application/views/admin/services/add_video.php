<div class="page-content pad-top-zero">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add Service Video </h3>
            </div>

            <div class="widget-content">
               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>

                <div id="wizard">
                <div id="rootwizard">
                  
                  <div class="tab-content">

                    <div class="control-group">
                  <div class="col-md-2">
                    <label for="disabled-input" class="control-label ">Services</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <?php echo form_dropdown('service_id', $servicesOptions,set_value('service_id',isset($service_id)?$service_id:""),'id="service_id" class="form-control"'); ?>
                      <?php echo form_error('service_id'); ?>
                    </div>
                    </div>
                  </div>
                  
                    <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Youtube Video Url</label>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <?php echo form_dropdown('video_source', array('Youtube'=>'Youtube','Vimeo'=>'Vimeo'),set_value('video_source',isset($video_source)?$video_source:""),'id="video_source" class="form-control"'); ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <input type="text" name="video" class="form-control" value="<?=set_value('video'); ?>" id="video" placeholder="Video Embedded Url">
                         <?php echo form_error('video'); ?>
                      </div>
                    </div>
                  </div>
                    
                  </div>
                </div>
              </div>
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
