<div class="page-content pad-top-zero">
    <div class="content container">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script src="//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
     <?php 
      //print_r($servicesData);
      if(!empty($servicesData))
      {
        extract($servicesData);
      }
      $currencySymbol = !empty($servicesSettings->currency)?getCurrencySymbol($servicesSettings->currency):getCurrencySymbol();
      ?>
      <div class="row">
        <div class="col-lg-12">
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>
        </div>
      </div>
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Service Information & Charges </h3>
              
            </div>
            <div class="widget-content">
              <form method="post" class="form-horizontal" enctype="multipart/form-data">
                
                <input type="hidden" name="settings_id" value="<?=isset($servicesSettings->id)?$servicesSettings->id:"0"; ?>">
                <div class="row">
                    
                    <div class="col-md-2">
                      <div class="text-align-center"> 
                      <div style="height:10px"></div>
                      <?php if(!empty($featured_img)) { ?>
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url($featured_img)?>" class="img-circle">
                      <?php } else { ?> 
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url('skin/admin/')?>images/profile.jpg" class="img-circle"> 
                      <?php } ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <h3 class="no-margin"><?=isset($fname)?$fname:$bussiness_holder_fname; ?> <?=isset($lname)?$lname:$bussiness_holder_lname; ?></h3>
                      <address>
                      <?php 
                      if(!empty($business) && $business=='yes') { ?> 
                      <strong><?=isset($designation)?$designation:""; ?></strong> at <strong><?=isset($business_name)?$business_name:""; ?></strong><br>
                      <?php }  ?>
                      <abbr title="Work email">Request Service:</abbr> <?=getServicesName($services_type) ?><br>
                      <abbr title="Work email">E-mail:</abbr> <?=isset($email)?$email:$business_email; ?><br>
                      <abbr title="Work Phone">Phone:</abbr> <?=isset($mobile)?$mobile:$business_mobile; ?><br>
                      <div style="height:5px"></div>
                      
                      </address>
                    </div>
                  </div>


                   	<div style="height:40px;"></div>                    
                <fieldset>              
                   	<div class="clearfix"></div>                    
                 	<div class="s-p-contract-form">
                 
                   	<div class="row">
					    
						<div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Start Date<span class="redstar">*</span></label>

								<input type="text" name="start_date" class="form-control" id="services_start_date" value="<?=set_value('start_date',(!empty($servicesSettings->start_date) && $servicesSettings->start_date!='0000-00-00')?$servicesSettings->start_date:"")?>" required=""><!--hem--date-->

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
						<label class="profile_field">Service<span class="redstar">*</span></label>
							<?php                       
                      		echo form_dropdown('services_type', $servicestypeOptions,set_value('services_type',!empty($servicesData['services_type'])?$servicesData['services_type']:""),'id="services_type" class="form-control" required=""'); ?>
                      		<?php echo form_error('services_type'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
						<label class="profile_field">Service Code<span class="redstar">*</span></label>
						<input readonly type="text" name="service_category_code" class="form-control" value="<?=set_value('service_category_code', isset($servicesSettings->service_category_code)?$servicesSettings->service_category_code:""); ?>" id="service_category_code" required="">
                      <?php echo form_error('service_category_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Operator No.</label>

								<input type="text" name="operator_no" class="form-control" value="<?=set_value('operator_no', isset($servicesSettings->operator_no)?$servicesSettings->operator_no:""); ?>" id="operator_no" readonly="readonly" disabled="disabled">
                      			<?php echo form_error('operator_no'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">Service Zip code<span class="redstar">*</span></label>
								<input type="text" name="service_zip_code" class="form-control" value="<?=set_value('service_zip_code', isset($servicesSettings->service_zip_code)?$servicesSettings->service_zip_code:""); ?>" id="service_zip_code" required="">
                      		<?php echo form_error('service_zip_code'); ?>

                      		<input type="checkbox" name="is_allowed_all_area" value="1" <?=!empty($servicesSettings->is_allowed_all_area)?'checked="checked"':''; ?>" id="service_zip_code"> Is allowed entire area?
							</div>
						 </div>

						<div class="clearfix"></div>
						
						<div class="col-md-12"><h4>Currency</h4></div>
						 
						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Currency<span class="redstar">*</span></label>
								<?php echo form_dropdown('currency', $currencyOption,set_value('currency',isset($servicesSettings->currency)?$servicesSettings->currency:""),'id="currency" class="form-control" required=""'); ?>
                      			<?php echo form_error('currency'); ?>
							</div>
						 </div>
						 <div class="col-md-8"></div>

						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Head Office Charges</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">H.O. Fees (%)<span class="redstar">*</span></label>
							<div class="input-group">
							    <input type="text" name="ho_fees" class="form-control" value="<?=set_value('ho_fees', isset($servicesSettings->ho_fees)?$servicesSettings->ho_fees:""); ?>" id="ho_fees" required="">
							    <span class="input-group-addon">%</span>
							 </div>							
                      		<?php echo form_error('ho_fees'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						<div class="col-md-4">					
							<div class="form-group">						 
								<label class="profile_field">Per order Charges<span class="redstar">*</span></label>
								<div class="input-group">
									<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="text" name="ho_per_order_charge" class="form-control" value="<?=set_value('ho_per_order_charge', isset($servicesSettings->ho_per_order_charge)?$servicesSettings->ho_per_order_charge:""); ?>" id="ho_per_order_charge" required="">
							 	</div>								
	                      		<?php echo form_error('ho_per_order_charge'); ?>
							</div>
						</div>
						 
						<div class="col-md-4">					
							<div class="form-group">						 
								<label class="profile_field">Advertising %</label>								
								<div class="input-group">
								    <input type="text" name="ho_advertising" class="form-control" value="<?=set_value('ho_advertising', isset($servicesSettings->ho_advertising)?$servicesSettings->ho_advertising:""); ?>" id="ho_advertising">
								    <span class="input-group-addon">%</span>
								</div>
	                      		<?php echo form_error('ho_advertising'); ?>
							</div>
						 </div>
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Fix weekly charges<span class="redstar">*</span></label>
							<div class="input-group">
								<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
						    	<input type="text" name="ho_fix_weekly_charges" class="form-control" value="<?=set_value('ho_fix_weekly_charges', isset($servicesSettings->ho_fix_weekly_charges)?$servicesSettings->ho_fix_weekly_charges:""); ?>" id="ho_fix_weekly_charges" required="">
						 	</div>
							<?php echo form_error('ho_fix_weekly_charges'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						<div class="col-md-4">					
							<div class="form-group">						 
								<label class="profile_field">Other Charges</label>
								<div class="input-group">
									<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="text" name="ho_other_charge" class="form-control" value="<?=set_value('ho_other_charge', isset($servicesSettings->ho_other_charge)?$servicesSettings->ho_other_charge:""); ?>" id="ho_other_charge">
							 	</div>
								<?php echo form_error('ho_other_charge'); ?>
							</div>
						 </div>
						 
						<div class="col-md-4">					
							<div class="form-group">						 
								<label class="profile_field">Credit/Debit Card charges %</label>
								<div class="input-group">
								    <input type="text" name="ho_credit_debit_card_charges" class="form-control" value="<?=set_value('ho_credit_debit_card_charges', isset($servicesSettings->ho_credit_debit_card_charges)?$servicesSettings->ho_credit_debit_card_charges:""); ?>" id="ho_credit_debit_card_charges">
								    <span class="input-group-addon">%</span>
								</div>
								<?php echo form_error('ho_credit_debit_card_charges'); ?>
							</div>
						 </div>
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Taxes</h4></div>			 
						
						<div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax name<span class="redstar">*</span></label>

								<input type="text" name="tax_name" class="form-control" value="<?=set_value('tax_name', isset($servicesSettings->tax_name)?$servicesSettings->tax_name:""); ?>" id="tax_name" required="">
                      			<?php echo form_error('tax_name'); ?>

							</div>
						 </div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax (%)<span class="redstar">*</span></label>
							<div class="input-group">
							    <input type="text" name="tax" class="form-control" value="<?=set_value('tax', isset($servicesSettings->tax)?$servicesSettings->tax:""); ?>" id="tax" required="">
							    <span class="input-group-addon">%</span>
							</div>
							<?php echo form_error('tax'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information<span class="redstar">*</span></label>
								<div class="input-group">
									<input type="text" name="tax_other_info" class="form-control" value="<?=set_value('tax_other_info', isset($servicesSettings->tax_other_info)?$servicesSettings->tax_other_info:""); ?>" id="tax_other_info" required="">
									<span class="input-group-addon">%</span>
								</div>
                      			<?php echo form_error('tax_other_info'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						<div class="col-md-12"><h4>Service Charges (Within Working Hours)</h4></div>
						 
						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Service Charge<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="service_charge" class="form-control" value="<?=set_value('service_charge', isset($servicesSettings->service_charge)?$servicesSettings->service_charge:""); ?>" id="service_charge" required="">
							  	</div>								
                      			<?php echo form_error('service_charge'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Service Duration<span class="redstar">*</span></label>
								<?php echo form_dropdown('service_duration', durationOptions('minute'),set_value('service_duration',isset($servicesSettings->service_duration)?$servicesSettings->service_duration:""),'id="service_duration" class="form-control" required=""'); ?>
								<?php echo form_error('service_duration'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Charges after that<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="service_charges_after" class="form-control" value="<?=set_value('service_charges_after', isset($servicesSettings->service_charges_after)?$servicesSettings->service_charges_after:""); ?>" id="service_charges_after" required="">
							  	</div>								
                      			<?php echo form_error('service_charges_after'); ?>
							</div>
						</div>

						<div class="col-md-3">
							<div class="form-group">						 
								<label class="profile_field">For how long<span class="redstar">*</span></label>
								<?php echo form_dropdown('service_duration_after', durationOptions(),set_value('service_duration_after',isset($servicesSettings->service_duration_after)?$servicesSettings->service_duration_after:""),'id="service_duration_after" class="form-control" required=""'); ?>
                      			<?php echo form_error('service_duration_after'); ?>
							</div>
						 </div>
						 
					  	<div class="col-md-3">
							<div class="form-group">
								<?php $howlong = !empty($servicesSettings->service_duration_after)?' after '.$servicesSettings->service_duration_after.' hours':''; ?>
								<label id="service_additional_charge_label" class="profile_field">Charges <?=$howlong?><span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="service_additional_charge" class="form-control" value="<?=set_value('service_additional_charge', isset($servicesSettings->service_additional_charge)?$servicesSettings->service_additional_charge:""); ?>" id="service_additional_charge" required="">
							  	</div>								
                      			<?php echo form_error('service_additional_charge'); ?>
							</div>
						</div>

						<div class="col-md-3">
							<div class="form-group">
								<label id="service_additional_hours_label" class="profile_field">For how long<span class="redstar">*</span></label>
								<?php echo form_dropdown('service_additional_hours', durationOptions(),set_value('service_additional_hours',isset($servicesSettings->service_additional_hours)?$servicesSettings->service_additional_hours:""),'id="service_additional_hours" class="form-control" required=""'); ?>								
                      			<?php echo form_error('service_additional_hours'); ?>
							</div>
						 </div>
						 
						 <div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Hourly charges after that<span class="redstar">*</span></label>
								<div class="input-group">
									<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>	<input type="number" name="service_hourly_charge" class="form-control" value="<?=set_value('service_hourly_charge', isset($servicesSettings->service_hourly_charge)?$servicesSettings->service_hourly_charge:""); ?>" id="service_hourly_charge" required="">
							  	</div>
                      			<?php echo form_error('service_hourly_charge'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">						 
								<label class="profile_field">Additional worker charges<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="service_additional_worker" class="form-control" value="<?=set_value('service_additional_worker', isset($servicesSettings->service_additional_worker)?$servicesSettings->service_additional_worker:""); ?>" id="service_additional_worker" required="">
							  	</div>								
                      			<?php echo form_error('service_additional_worker'); ?>
							</div>
						 </div>
						 
						 
						<div class="clearfix"></div>
						 
						<div class="col-md-12"><h4>Weekend & Non Working Hours Service Charges</h4></div>			 
						 
						<div class="col-md-3">
							<div class="form-group">								
								<label class="profile_field">Service Charge<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="weekend_charge" class="form-control" value="<?=set_value('weekend_charge', isset($servicesSettings->weekend_charge)?$servicesSettings->weekend_charge:""); ?>" id="weekend_charge" required="">
							  	</div>								
                      			<?php echo form_error('weekend_charge'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Service Duration<span class="redstar">*</span></label>
								<?php echo form_dropdown('weekend_duration', durationOptions('minute'),set_value('weekend_duration',isset($servicesSettings->weekend_duration)?$servicesSettings->weekend_duration:""),'id="weekend_duration" class="form-control" required=""'); ?>
								<?php echo form_error('weekend_duration'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Charges after that<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="weekend_charges_after" class="form-control" value="<?=set_value('weekend_charges_after', isset($servicesSettings->weekend_charges_after)?$servicesSettings->weekend_charges_after:""); ?>" id="weekend_charges_after" required="">
							  	</div>								
                      			<?php echo form_error('weekend_charges_after'); ?>
							</div>
						</div>

						<div class="col-md-3">
							<div class="form-group">						 
								<label class="profile_field">For how long<span class="redstar">*</span></label>
								<?php echo form_dropdown('weekend_duration_after', durationOptions(),set_value('weekend_duration_after',isset($servicesSettings->weekend_duration_after)?$servicesSettings->weekend_duration_after:""),'id="weekend_duration_after" class="form-control" required=""'); ?>
                      			<?php echo form_error('weekend_duration_after'); ?>
							</div>
						 </div>
						<?php $howlong_weekend = !empty($servicesSettings->weekend_duration_after)?' after '.$servicesSettings->weekend_duration_after.' hours':''; ?>
					  	<div class="col-md-3">
							<div class="form-group">
								<label id="weekend_additional_charge_label" class="profile_field">Charges <?=$howlong_weekend?><span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="weekend_additional_charge" class="form-control" value="<?=set_value('weekend_additional_charge', isset($servicesSettings->weekend_additional_charge)?$servicesSettings->weekend_additional_charge:""); ?>" id="weekend_additional_charge" required="">
							  	</div>								
                      			<?php echo form_error('weekend_additional_charge'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label id="weekend_additional_hours_label" class="profile_field">For how long<span class="redstar">*</span></label>
								<?php echo form_dropdown('weekend_additional_hours', durationOptions(),set_value('weekend_additional_hours',isset($servicesSettings->weekend_additional_hours)?$servicesSettings->weekend_additional_hours:""),'id="weekend_additional_hours" class="form-control" required=""'); ?>
                      			<?php echo form_error('weekend_additional_hours'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">
								<label class="profile_field">Hourly charges after that<span class="redstar">*</span></label>
								<div class="input-group">	
									<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
									<input type="number" name="weekend_hourly_charge" class="form-control" value="<?=set_value('weekend_hourly_charge', isset($servicesSettings->weekend_hourly_charge)?$servicesSettings->weekend_hourly_charge:""); ?>" id="weekend_hourly_charge" required="">
							  	</div>
                      			<?php echo form_error('weekend_hourly_charge'); ?>
							</div>
						 </div>

						<div class="col-md-3">
							<div class="form-group">						 
								<label class="profile_field">Additional worker charges<span class="redstar">*</span></label>
								<div class="input-group">
							    	<span class="input-group-addon curr_symbol"><?=!empty($currencySymbol)?$currencySymbol:'$';?></span>
							    	<input type="number" name="weekend_additional_worker" class="form-control" value="<?=set_value('weekend_additional_worker', isset($servicesSettings->weekend_additional_worker)?$servicesSettings->weekend_additional_worker:""); ?>" id="weekend_additional_worker" required="">
							  	</div>								
                      			<?php echo form_error('weekend_additional_worker'); ?>
							</div>
						 </div>
						 
						 
						<div class="clearfix"></div>
						 
						<div class="col-md-12"><h4>Bank Information</h4></div>			 
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Bank Name</label>
								<input type="text" name="bank_name" class="form-control" value="<?=set_value('bank_name', isset($servicesSettings->bank_name)?$servicesSettings->bank_name:""); ?>" id="bank_name">
								<?php echo form_error('bank_name'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Bank Address</label>
								<input type="text" name="bank_address" class="form-control" value="<?=set_value('bank_address', isset($servicesSettings->bank_address)?$servicesSettings->bank_address:""); ?>" id="bank_address">
								<?php echo form_error('bank_address'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Bank Code</label>
								<input type="text" name="bank_code" class="form-control" value="<?=set_value('bank_code', isset($servicesSettings->bank_code)?$servicesSettings->bank_code:""); ?>" id="bank_code">
								<?php echo form_error('bank_code'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Account No</label>
								<input type="text" name="account_no" class="form-control" value="<?=set_value('account_no', isset($servicesSettings->account_no)?$servicesSettings->account_no:""); ?>" id="account_no">
								<?php echo form_error('account_no'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">SWIFT No.</label>
								<input type="text" name="swift_no" class="form-control" value="<?=set_value('swift_no', isset($servicesSettings->swift_no)?$servicesSettings->swift_no:""); ?>" id="swift_no">
                      			<?php echo form_error('swift_no'); ?>
							</div>
						</div><!--col-sm-4-->

						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Transition No</label>
								<input type="text" name="transition_no" class="form-control" value="<?=set_value('transition_no', isset($servicesSettings->transition_no)?$servicesSettings->transition_no:""); ?>" id="transition_no">
                      			<?php echo form_error('transition_no'); ?>
							</div>
						</div><!--col-sm-4-->

						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Account Type</label>
								<?php echo form_dropdown('account_type', array("Chequing"=>"Chequing","Saving"=>"Saving"),set_value('account_type',isset($servicesSettings->account_type)?$servicesSettings->account_type:""),'id="account_type" class="form-control"'); ?>
                      			<?php echo form_error('account_type'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						
						<div class="clearfix"></div>						 
						<div class="col-md-12"><h4>Car Specification</h4></div>			 
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Driver Licence</label>
								<input type="text" name="driver_licence" class="form-control" value="<?=set_value('driver_licence', isset($servicesRequest->driver_licence)?$servicesRequest->driver_licence:""); ?>" id="driver_licence">
								<?php echo form_error('driver_licence'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Made</label>
								<input type="text" name="car_made" class="form-control" value="<?=set_value('car_made', isset($servicesRequest->car_made)?$servicesRequest->car_made:""); ?>" id="car_made">
								<?php echo form_error('car_made'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Model</label>
								<input type="text" name="car_model" class="form-control" value="<?=set_value('car_model', isset($servicesRequest->car_model)?$servicesRequest->car_model:""); ?>" id="car_model">
								<?php echo form_error('car_model'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">
								<label class="profile_field">Year</label>
								<input type="text" name="car_year" class="form-control" value="<?=set_value('car_year', isset($servicesRequest->car_year)?$servicesRequest->car_year:""); ?>" id="car_year">
								<?php echo form_error('car_year'); ?>
							</div>
						</div><!--col-sm-4-->
						 
						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Color</label>
								<input type="text" name="car_color" class="form-control" value="<?=set_value('car_color', isset($servicesRequest->car_color)?$servicesRequest->car_color:""); ?>" id="car_color">
                      			<?php echo form_error('car_color'); ?>
							</div>
						</div><!--col-sm-4-->

						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Licence Plate No</label>
								<input type="text" name="car_number" class="form-control" value="<?=set_value('car_number', isset($servicesRequest->car_number)?$servicesRequest->car_number:""); ?>" id="car_number">
                      			<?php echo form_error('car_number'); ?>
							</div>
						</div><!--col-sm-4-->

						<div class="col-md-4">
							<div class="form-group">						 
								<label class="profile_field">Car Picture</label>
								<input type="file" name="car_picture" class="form-control" id="car_picture">
	                              <?php
	                              if(!empty($servicesRequest->car_picture))
	                              {
	                              	echo '<img src="'.base_url($servicesRequest->car_picture).'" width="80">';
	                              }
	                              ?>
							</div>
						</div><!--col-sm-4-->

						 
					 </div><!--row-->
                 
                 
                 
			     </div><!--s-p-contract-form-->
                  
  

                  

                 <div class="clearfix"></div>

                   
                  </fieldset>


                  
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
    <style type="text/css">
    	h4 {
    border-bottom: 1px solid #937c67;
    padding: 10px 0;
    margin-bottom: 30px;
}
    </style>

    

    </div>
  </div>
