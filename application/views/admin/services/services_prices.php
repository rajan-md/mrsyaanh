
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Services Price <?=AddButton('prices/add')?></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Services Price Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>Services</th>                     
                      <th>Price Label</th>
                      <th>Minimum Price</th>                   
                      <th width="150">Created</th>
                      <th width="80">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($servicesData)) {
                      foreach ($servicesData as $data) { ?>
                    <tr>
                      <td><?=getServicesName($data->service_id)?></td>
                      <td><?=$data->availability?></td>
                      <td><?=getPriceFormate($data->price)?></td>
                      <td class="hidden-xs"><?=$data->created?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                       <a href="<?=base_url('admin/services/prices_status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                      <td class="hidden-xs">
                      <?php echo AllowedAction('admin/services/prices', 'edit', 'delete', 'Sure you want to delete this services', $data->id) ?>
                      </td>
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
