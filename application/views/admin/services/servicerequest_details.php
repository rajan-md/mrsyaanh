<div class="page-content pad-top-zero">
    <div class="content container">      
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Service Provider Request Details </h3>
            </div>

            <div class="widget-content">
             <table class="table table-striped table-images" id="example">
              <tr>
                <th width="50">Service Provider Name : </th>
                <td><?=getProviderName($servicesData['id']); ?></td>
              </tr>

              
              <tr>
                <th width="50">Service Type : </th>
                <td><?=getServicesName(isset($servicesData['services_type'])?$servicesData['services_type']:""); ?></td>
              </tr>

              <tr>
                <th>Service ZipCode : </th>
                <td><?=isset($servicesData['service_zip_code'])?$servicesData['service_zip_code']:""; ?></td>
              </tr>

              <tr>
                <th>Request Date : </th>
                <td><?=isset($servicesData['request_date'])?$servicesData['request_date']:""; ?></td>
              </tr>
              <?php
              if(!empty($servicesData['business']) && $servicesData['business']=='yes')
              {
                ?>
              <tr>
                <th>Bussiness Details : </th>
                <td>
                  
                  <table>
                  <?php if(!empty($servicesData['business_name'])){ ?><tr><th>Bussiness Name: </th><td><?=$servicesData['business_name']; ?></td></tr><?php } ?>
                  <?php if(!empty($servicesData['business_type'])){ ?><tr><th>Bussiness Type: </th><td><?=$servicesData['business_type']; ?></td></tr><?php } ?>
                  <?php if(!empty($servicesData['license_details'])){ ?><tr><th>License Details: </th><td><?=$servicesData['license_details']; ?></td></tr><?php } ?>
                  <?php if(!empty($servicesData['business_email'])){ ?><tr><th>Bussiness Email: </th><td><?=$servicesData['business_email']; ?></td></tr><?php } ?>
                  <?php if(!empty($servicesData['business_mobile'])){ ?><tr><th>Bussiness Contact: </th><td><?=$servicesData['business_mobile']; ?></td></tr><?php } ?>
                  </table>

                </td>
              </tr>
              <?php
            }
            ?>

              <tr>
                <th>Request Status : </th>
                <td><?php $status=isset($servicesData['status'])?$servicesData['status']:2 ; ?>
                      <span class="label label-<?php if($status==1) { echo 'success'; } else { $status=2; echo 'info'; } ?>"><?=getStatus($status)?></span></td>
              </tr>

              <?php
              if(empty($servicesData['user_id']))
              {
              ?>
              <tr>
                <th>Email ( With registration link ) : </th>
                <td>
                  <?php                 
                    $sid = !empty($servicesData['id'])?$servicesData['id']:'';
                    if(empty($servicesData['is_verified']))
                    {                    
                       echo '<a href="'.base_url('admin/services/request_verify/'.$sid).'" title="Send email with registration link."><span class="btn btn-l btn-primary">Send email with registration link.</span></a>';
                    }
                    else
                    {
                       echo '<a href="'.base_url('admin/services/request_verify/'.$sid).'" title="Resend email with registration link."><span class="btn btn-l btn-primary">Re-send email with registration link.</span></a>';
                    }               
                  ?>               
                </td>
              </tr>
              <?php
              }
              ?>                       
            </table>
            </div>
          </div>
        </div>
      </div>
     


    </div>
  </div>
  <style type="text/css">
  .table-striped > tbody > tr> td{
    color: #FFFFFF;
  }
  .table-striped > tbody > tr> td:last-child {
      width: 50%; 
  }
  </style>