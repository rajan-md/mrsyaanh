
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Invoice Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <form action="<?=base_url('admin/services/create-invoice');?>" method="get">
                  <table class="table table-striped table-images">
                      <tr>
                        <td><?php echo form_dropdown('service_provider_id', $serviceProviderOptions,set_value('service_provider_id',isset($service_provider_id)?$service_provider_id:""),'id="service_provider_id" class="form-control"'); ?></td>
                        <td><?php echo form_dropdown('duration', $durationOptions,set_value('duration',isset($duration)?$duration:""),'id="duration" class="form-control"'); ?></td>
                        <td><button type="submit" class="btn btn-success">Filter</button></td>
                        <td>&nbsp;</td>
                      </tr>
                  </table>
                </form>
                <?php
                if(!empty($service_provider_id) && !empty($duration))
                {
                  ?>
                  <table class="table table-striped">
                    <tr>
                      <td width="50%">
                        <table class="table table-striped table-summary">
                          <tr>
                            <th colspan="2" align="center"><strong> Service Provider</strong></th>
                          </tr>
                          <tr>
                            <td width="20%">
                              <img style="height:55px;width:55px;border: 1px solid #664f5c;" alt="64x64" src="<?=$img;?>" class="img-circle">
                            </td>
                            <td>
                              <address>
                                  <?php if(!empty($name)){ ?><strong><?=$name;?></strong><br><?php } ?>
                                  <?php if(!empty($email)){ ?><abbr title="Work email">E-mail:</abbr> <?=$email;?><br><?php } ?>
                                  <?php if(!empty($mobile)){ ?><abbr title="Work Phone">Phone:</abbr> <?=$mobile;?><br><?php } ?>
                              </address>
                            </td>
                          </tr>
                        </table>
                      </td>

                      <td width="50%">
                        
                        <table class="table table-striped table-summary">

                          <tr>
                            <th colspan="2" align="center"><strong>Service Duration </strong></th>
                          </tr>

                          <tr>
                            <td width="50%"><strong> Duration :  </strong></td>
                            <td><?=!empty($durationLabel)?$durationLabel:'';?></td>
                          </tr>

                          <tr>
                            <td width="50%"><strong> From :  </strong><?=!empty($start)?$start:'';?></td>
                            <td><strong> To :  </strong><?=!empty($end)?$end:'';?></td>
                          </tr>

                        </table>
                      </td>
                    </tr>
                  </table>
                  <?php
                }
                ?>

               


              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
      if(!empty($service_provider_id) && !empty($duration))
      {
        ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="widget">
              <div class="widget-content">
                <div class="body">

                  <table class="table table-striped table-images" id="example2" cellpadding="5">
                    <thead>
                      <tr>
                        <th>Booking Id</th>
                        <th>Service Date</th>
                        <th style="text-align: right;" width="20%">Initial Charges</th>
                        <th style="text-align: right;" width="20%">Additional Charges</th>
                        <th style="text-align: right;" width="20%">Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($records))
                        {
                          foreach ($records as $data)
                          {
                            ?>
                              <tr>
                                <td><?=$data['id']; ?></td>
                                <td><?=$data['booking_datetime']; ?></td>
                                <td style="text-align: right;"><?=$data['amount']; ?></td>
                                <td style="text-align: right;"><?=$data['additional_amount']; ?></td>
                                <td style="text-align: right;"><?=$data['total']; ?></td>                                                        
                              </tr>
                              <?php
                            }
                          }
                          else
                          {
                            ?>
                            <tr><td colspan="5">No service bookings found for invoiced OR already invoiced.</td></tr>
                            <?php
                          }
                          ?>                                        
                    </tbody>
                    <tfoot>
                        <tr>
                          <th colspan="5"> </th>
                        </tr>
                    </tfoot>
                  </table>

                  <?php
                  if(!empty($records))
                  {
                    $service_provider_id = $this->input->get('service_provider_id');
                    $duration = $this->input->get('duration');
                    $querystring = '?service_provider_id='.$service_provider_id.'&duration='.$duration;
                  ?>
                  <table class="table table-summary">
                    <tr>
                      <th width="50%">&nbsp;</th>
                      <td align="right"><strong> Sub Total : </strong></td>
                      <td align="right"><strong><?=$sub_total; ?></strong></td>
                    </tr>
                    <tr>
                      <th width="50%">&nbsp;</th>
                      <td align="right"><strong> Total H.O Charges (-) : </strong></td>
                      <td align="right"><strong><?=$ho_total; ?></strong></td>
                    </tr>
                    <tr>
                      <th width="50%">&nbsp;</th>
                      <td align="right"><strong> Grand Total : </strong></td>
                      <td align="right"><strong><?=$grand_total; ?></strong></td>
                    </tr>
                  </table>

                  <form action="<?=base_url('admin/services/send-invoice'); ?>" method="get">
                      <input type="hidden" name="service_provider_id" value="<?=$service_provider_id;?>">
                      <input type="hidden" name="duration" value="<?=$duration;?>">
                     <table class="table table-striped" cellpadding="5">
                      <tr>
                        <td colspan="2" align="right"></td>
                        <td colspan="2" align="right" style="width:50%;"><textarea name="notes" id="notes" placeholder="Type message for service provider...." class="form-control" cols="50" rows="4"></textarea></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right">
                          <!--a href="<?=base_url('admin/services/export-invoice'.$querystring); ?>" class="btn btn-primary">Generate & Export Invoice</a>
                          <a href="<?=base_url('admin/services/send-invoice'.$querystring); ?>" class="btn btn-primary">Generate & Send Invoice</a-->
                          <button type="submit" class="btn btn-primary">Generate & Send Invoice</button></td>
                      </tr>
                    </table> 
                  </form>                  
                  <?php
                  }
                  ?>

                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
