<div class="page-content pad-top-zero">
    <div class="content container">
        <?php 
        //print_r($record);
        if(!empty($record))
        {
        extract($record);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-align-left"></i>
                        <h3>Edit Service </h3>
                    </div>
                    <div class="widget-content">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <fieldset>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" value="<?=set_value('name', isset($name)?$name:" "); ?>">
                                            <?php echo form_error('name'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Summary</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control ckeditor" id="text" name="summary" rows="4">
                                                <?=set_value('summary'); ?>
                                            </textarea>
                                            <?php echo form_error('summary'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Description</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control ckeditor" id="text" name="description" rows="6">
                                                <?=set_value('description', isset($description)?$description:""); ?>
                                            </textarea>
                                            <?php echo form_error('description'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Allowed Activity</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <?php echo form_checkbox('allowed_activity[]', 'quote', TRUE); ?> Get Quote
                                                <?php echo form_checkbox('allowed_activity[]', 'booking', TRUE); ?> Book Now
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Parent</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <?php echo form_dropdown('parent', $parentOptions,set_value('parent',isset($parent)?$parent:""),'id="parent" class="form-control"'); ?>
                                                <?php echo form_error('parent'); ?>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Highlights</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="field_wrapper">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <div class="row highlightsBox">
                                                        <div class="col-md-3">
                                                            <div id="blah3_container">
                                                                <img class="imgbox2" id="blah4" src="<?=base_url(!empty($service_icon)?$service_icon:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <label for="service_icon"> Icon: </label>
                                                                <input name="service_icon" type="file" class="form-control pull-right" id="service_icon" onchange="checkPhoto(this,'blah4')">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="service_icon"> Title: </label>
                                                                <?php echo form_input(array('id' => 'choices', 'name' => 'choices[]','class'=>'form-control input-md','placeholder'=>'Options/Choices','value'=>(!empty($val)?$val:''))); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:void(0);" class="add_button" title="Add field">
                                                        <img src="<?=base_url('skin/admin/images/add.png');?>"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="hint-field" class="control-label"> Icons & Images </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group text-center">
                                            <label for="service_icon"> Service Icon: </label>
                                            <div id="blah_container" class="imgbox_container">
                                                <img class="imgbox" id="blah" src="<?=base_url(!empty($service_icon)?$service_icon:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                                            </div>
                                            <input name="service_icon" type="file" class="form-control pull-right" id="service_icon" onchange="checkPhoto(this)">

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group text-center">
                                            <label for="featured_img"> Feature Images: </label>
                                            <div id="blah2_container" class="imgbox_container">
                                                <img class="imgbox" id="blah2" src="<?=base_url(!empty($featured_img)?$featured_img:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                                            </div>
                                            <input name="featured_img" type="file" class="form-control pull-right" id="featured_img" onchange="checkPhoto(this,'blah2')">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group text-center">
                                            <label for="banner_img"> Banner Images: </label>
                                            <div id="blah3_container" class="imgbox_container">
                                                <img class="imgbox" id="blah3" src="<?=base_url(!empty($banner_img)?$banner_img:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                                            </div>
                                            <input name="banner_img" type="file" class="form-control pull-right" id="banner_img" onchange="checkPhoto(this,'blah3')">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="hint-field" class="control-label"> Meta Title </label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="meta_title" class="form-control" value="<?=set_value('meta_title', isset($meta_title)?$meta_title:" "); ?>" id="meta_title">
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="tooltip-enabled" class="control-label ">Meta Key</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_key" rows="3">
                                                <?=set_value('meta_key', isset($meta_key)?$meta_key:""); ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Meta Description</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_description" rows="3">
                                                <?=set_value('meta_description', isset($meta_description)?$meta_description:""); ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-success" type="submit">Save </button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div class="row"> <div class="col-md-11"> <div class="row highlightsBox"> <div class="col-md-3"> <div id="blah3_container"> <img class="imgbox2" id="blah4" src="skin/front/images/no_image.jpg" alt="your image"/> </div></div><div class="col-md-9"> <div class="form-group"> <label for="service_icon"> Service Icon: </label> <input name="service_icon" type="file" class="form-control pull-right" id="service_icon" onchange="checkPhoto(this,"blah4")"> </div><div class="form-group"> <label for="service_icon"> Service Icon: </label> <input name="choices[]" type="test" class="form-control pull-right" id="choices"></div></div></div></div><div class="col-md-1"> <a href="javascript:void(0);" class="remove_button" title="Add field"> <img src="skin/admin/images/delete.png"/> </a> </div></div>'; //New input field html 
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function()
        {
            //Check maximum number of input fields
            if (x < maxField)
            {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
            else
            {
                alert('Exceed maximum limit of records, Only '+maxField+' records are allowed');
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e) {
            e.preventDefault();
            //alert( $(this).closest(".col-md-12").html());
            $(this).closest(".row").remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>