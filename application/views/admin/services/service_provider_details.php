<div class="page-content pad-top-zero">
    <div class="content container">
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
      
      <?php 
      //print_r($servicesData);
      if(!empty($servicesData))
      {
        extract($servicesData);
      }
      ?>
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Services Provider Request Details </h3>
            </div>
            <div class="widget-content">
              <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" id="mobbox" name="mobbox">
                <input type="hidden" name="username" class="form-control" value="<?=set_value('username', isset($username)?$username:""); ?>" id="username">
                <input type="hidden" name="user_id" class="form-control" value="<?=set_value('user_id', isset($user_id)?$user_id:""); ?>" id="user_id">
                <div class="row">
                    <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2">
                      <div class="text-align-center"> 
                      <div style="height:10px"></div>
                      <?php if(!empty($featured_img)) { ?>
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url($featured_img)?>" class="img-circle">
                      <?php } else { ?> 
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url('skin/admin/')?>images/profile.jpg" class="img-circle"> 
                      <?php } ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <h3 class="no-margin">
                      
                      <?=isset($fname)?$fname:$bussiness_holder_fname; ?> <?=isset($lname)?$lname:$bussiness_holder_lname; ?>
                      
                      </h3>
                      <address>
                      <?php 
                      if($business=='yes') { ?> 
                      <strong><?=isset($designation)?$designation:""; ?></strong> at <strong><?=isset($business_name)?$business_name:""; ?></strong><br>
                      <?php } ?>
                      <abbr title="Work email">Request Service:</abbr> <?=getServicesName($services_type) ?><br>
                      <abbr title="Work email">E-mail:</abbr> <?=isset($email)?$email:$business_email; ?><br>
                      <abbr title="Work Phone">Phone:</abbr> <?=isset($mobile)?$mobile:$business_mobile; ?><br>
                      <div style="height:5px"></div>
                      
                      </address>
                    </div>
                  </div>


                <fieldset>
                <legend class="section">Personal Info</legend>
                   <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">First Name</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="fname" class="form-control" value="<?=set_value('fname', isset($fname)?$fname:$bussiness_holder_fname); ?>" id="fname">
                      <?php echo form_error('fname'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Last Name</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="lname" class="form-control" value="<?=set_value('lname', isset($lname)?$lname:$bussiness_holder_lname); ?>" id="lname">
                      <?php echo form_error('lname'); ?>
                    </div>
                    </div>
                  </div>
                  
                 
                    <div class="clearfix"></div>
                   <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Email</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input  type="text" name="email" class="form-control" value="<?=set_value('email', isset($email)?$email:$business_email); ?>" id="email">
                      <?php echo form_error('email'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Mobile</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group personal_phonebox">
                      <input type="text" name="mobile" class="form-control" value="<?=set_value('mobile', isset($mobile)?$mobile:$business_mobile); ?>" id="phonecode">
                      <?php echo form_error('mobile'); ?>
                    </div>
                    </div>
                  </div>
                  
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Location</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="site_location" id="site_location2" class="form-control" value="<?=set_value('site_location', isset($site_location)?$site_location:$business_address); ?>" >
                      <?php echo form_error('site_location'); ?>
                    </div>
                    </div>
                  </div>  
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Apt/Unit No</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="aptno" id="aptno" class="form-control" value="<?=set_value('aptno', isset($aptno)?$aptno:$business_aptno); ?>" >
                      <?php echo form_error('aptno'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Street No</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="street_number" id="street_number" class="form-control" value="<?=set_value('street_number', isset($street_number)?$street_number:$business_street_no); ?>" >
                      <?php echo form_error('street_number'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Street Name</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="street_name" id="street_name" class="form-control" value="<?=set_value('street_name', isset($street_name)?$street_name:$business_street_name); ?>" >
                      <?php echo form_error('street_name'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Street Type</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php echo form_dropdown('street_type', streetTypeOption(),set_value('street_type',isset($street_type)?$street_type:$business_street_type),'id="street_type" class="form-control"'); ?>
                      <?php echo form_error('street_type'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Direction</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php echo form_dropdown('direction', directionOption(),set_value('direction',isset($direction)?$direction:$business_direction),'id="direction" class="form-control"'); ?>
                      <?php echo form_error('direction'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Country</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($country)?$country:$business_country),'id="country" class="form-control"'); ?>
                      <?php echo form_error('country'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">State</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php 
                      if(!empty($country))
                      {
                          $stateOptions = getStatesOptions($country);
                      }
                      elseif(empty($country) && !empty($business_country))
                      {
                          $stateOptions = getStatesOptions($business_country);
                      }
                      else
                      {
                        $stateOptions = array(0=>'---Choose State/Province---');
                      }
                      echo form_dropdown('state',$stateOptions,set_value('state',isset($state)?$state:$business_state),'id="administrative_area_level_1" class="form-control"');

                      echo form_error('state');
                      ?>
                    </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">City</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="city" class="form-control" value="<?=set_value('city', isset($city)?$city:$business_city); ?>" id="locality">
                      <?php echo form_error('city'); ?>
                    </div>
                    </div>
                  </div>
                    <div class="clearfix"></div>
                   <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Zip Code</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:$business_zip); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>
                    </div>
                    </div>
                  </div>
                  
                  <div class="clearfix"></div>
                   
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="disabled-input" class="control-label ">Service</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php                       
                      echo form_dropdown('services_type', $servicestypeOptions,set_value('services_type',isset($services_type)?$services_type:""),'id="services_type" class="form-control"'); ?>
                      <?php echo form_error('services_type'); ?>
                    </div>
                    </div>
                  </div> 

                  <div class="clearfix"></div>
                  <?php $business=set_value('business', isset($business)?$business:""); ?>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Business</label>
                    </div>
                    <div class="col-md-8">
                    <div class="form-group">
                      <label class="cus_radio">
                        <input type="radio" name="business" <?php if($business=='no'){ ?> checked="checked" <?php } ?> value="no">
                        No </label>
                      <label class="cus_radio">
                        <input type="radio" name="business" <?php if($business=='yes'){ ?> checked="checked" <?php } ?>  value="yes">
                        Yes</label>
                      
                      
                        <div class="clearfix"></div>
                        <?php echo form_error('business'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                   <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Business Name</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="business_name" class="form-control" value="<?=set_value('business_name', isset($business_name)?$business_name:""); ?>" id="business_name">
                      <?php echo form_error('business_name'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                   <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">Bussiness Type</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <?php echo form_dropdown('business_type', bussinessTypeOption(),set_value('business_type',isset($business_type)?$business_type:""),'id="business_type" class="form-control" required=""'); ?>
                      <?php echo form_error('business_type'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <?php $license=set_value('license', isset($license)?$license:""); ?>
                  <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">License</label>
                    </div>
                    <div class="col-md-8">
                    <div class="form-group">
                      <label class="cus_radio">
                        <input type="radio" name="license" <?php if($license=='no'){ ?> checked="checked" <?php } ?> value="no">
                        No </label>
                      <label class="cus_radio">
                        <input type="radio" name="license" <?php if($license=='yes'){ ?> checked="checked" <?php } ?>  value="yes">
                        Yes</label>
                      
                      
                        <div class="clearfix"></div>
                        <?php echo form_error('license'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                   <div class="control-group">
                  <div class="col-md-3">
                    <label for="normal-field" class="control-label">License Details</label>
                    </div>
                    <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="license_details" class="form-control" value="<?=set_value('license_details', isset($license_details)?$license_details:""); ?>" id="license_details">
                      <?php echo form_error('license_details'); ?>
                    </div>
                    </div>
                  </div>

                  <div id="address">
                  <input type='hidden' class="field" id="street_number" disabled="true"/>
                  <input type='hidden' class="field" id="route"  disabled="true"/>                
                  <input type='hidden' class="field" id="latitude" name="site_latitude" value="<?=isset($latitude)?$latitude:""; ?>"  />               
                  <input type='hidden' class="field" id="longitude" name="site_longitude" value="<?=isset($longitude)?$longitude:""; ?>"/>             

                  </div> 

                 <div class="clearfix"></div>
                   
                                
                   
                  

                 
                   
                  </fieldset>


                  
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

$("#site_location").on('focus', function () {
    geolocate();
});

jQuery("input[name=driving_license]").click(function(){
    var license=jQuery(this).val();
    if(license=='yes')
    {
      jQuery("#licenseinfo").css('display','block');
    } else {
       jQuery("#licenseinfo").css('display','none');
    }
  });

 jQuery("input[name=company_advertise]").click(function(){
    var license=jQuery(this).val();
    if(license=='yes')
    {
      jQuery("#advertiseinfo").css('display','block');
    } else {
       jQuery("#advertiseinfo").css('display','none');
    }
  });

  $(document).on('change','#driving_license_types',function (e) {
    obj=this;
    var id=$(obj).val();
    $.ajax({
      type: 'post',
      url:'<?=base_url('login/ajax_driving_licence')?>',
      data: {id:id},
      success: function (data) { 
        $('#license_type').html(data);
      }
    });
    
  });



var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initialize() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('site_location')), {
        types: ['geocode']
    });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
    });
}

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
  //console.log(place);

    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = new google.maps.LatLng(
            position.coords.latitude, position.coords.longitude);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
        });
    }

}

initialize();
// [END region_geolocation]
  </script>

    </div>
  </div>
