<div class="page-content pad-top-zero">
    <div class="content container">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
      
  
      
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Edit Service Information & Charges </h3>
            </div>
            <div class="widget-content">
              <form method="post" class="form-horizontal" enctype="multipart/form-data">
                
                <input type="hidden" name="settings_id" value="<?=isset($servicesSettings->id)?$servicesSettings->id:"0"; ?>">
                <div class="row">
                    
                    <div class="col-md-2">
                      <div class="text-align-center"> 
                      <div style="height:10px"></div>
                      <?php if(!empty($featured_img)) { ?>
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url($featured_img)?>" class="img-circle">
                      <?php } else { ?> 
                      <img style="height:150px;width:150px;border: 1px solid #664f5c;" alt="64x64" src="<?=base_url('skin/admin/')?>images/profile.jpg" class="img-circle"> 
                      <?php } ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <h3 class="no-margin">
                      
                      <?=isset($fname)?$fname:""; ?> <?=isset($lname)?$lname:""; ?>
                      
                      </h3>
                      <address>
                      <?php 
					   $business= isset($servicesData['business'])?$servicesData['business']:""; 
                      if($business=='yes') { ?> 
                      <strong><?=isset($servicesData['designation'])?$servicesData['designation']:""; ?></strong> at <strong><?=isset($servicesData['business_name'])?$servicesData['business_name']:""; ?></strong><br>
                      <?php } $services_type= isset($servicesData['business'])?$servicesData['business']:"";  ?>
                      <abbr title="Work email">E-mail:</abbr> <?=isset($servicesData['email'])?$servicesData['email']:""; ?><br>
                      <abbr title="Work Phone">Phone:</abbr> <?=isset($servicesData['mobile'])?$servicesData['mobile']:""; ?><br>
                      <div style="height:5px"></div>
                      
                      </address>
                    </div>
                  </div>


                   	<div style="height:40px;"></div>                    
                <fieldset>              
                   	<div class="clearfix"></div>                    
                 	<div class="s-p-contract-form">
                 
                   	<div class="row">
					    
						<div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">Start Date</label>
								<input type="text" name="start_date" class="form-control" id="services_start_date" value="<?=set_value('start_date',isset($servicesSettings->start_date)?$servicesSettings->start_date:"")?>" required=""><!--hem--date-->
							</div>
						 </div><!--col-sm-4-->
						 <?php //echo "<pre>"; print_r($servicesSettings); echo "</pre>"; ?>
						 <div class="col-md-4">					
						<div class="form-group">						 
						<label class="profile_field">Service Category</label>
							<?php                       
	                      	echo form_dropdown('services_type', $servicestypeOptions,set_value('services_type',isset($servicesSettings->service_id)?$servicesSettings->service_id:''),'id="services_type" class="form-control"');
	                      	echo form_error('services_type');
	                      	?>
							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
						<label class="profile_field">Service Code</label>
						<input readonly type="text" name="service_category_code" class="form-control" value="<?=set_value('service_category_code', isset($servicesSettings->service_category_code)?$servicesSettings->service_category_code:""); ?>" id="service_category_code">
                      <?php echo form_error('service_category_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Operator No.</label>

								<input type="text" name="operator_no" class="form-control" value="<?=set_value('operator_no', isset($servicesSettings->operator_no)?$servicesSettings->operator_no:""); ?>" id="operator_no">
                      			<?php echo form_error('operator_no'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">Service Zip code</label>
								<input type="text" name="service_zip_code" class="form-control" value="<?=set_value('service_zip_code', isset($servicesSettings->service_zip_code)?$servicesSettings->service_zip_code:""); ?>" id="service_zip_code">
                      		<?php echo form_error('service_zip_code'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Promtions</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Coupons</label>

								<input type="text" name="coupons" class="form-control" value="<?=set_value('coupons', isset($servicesSettings->coupons)?$servicesSettings->coupons:""); ?>" id="coupons">
                      <?php echo form_error('coupons'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Discount Promotions</label>

								<input type="text" name="discount" class="form-control" value="<?=set_value('discount', isset($servicesSettings->discount)?$servicesSettings->discount:""); ?>" id="discount">
                      <?php echo form_error('discount'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">FC. Bonus Point %</label>

								<input type="text" name="bonus_point" class="form-control" value="<?=set_value('bonus_point', isset($servicesSettings->bonus_point)?$servicesSettings->bonus_point:""); ?>" id="bonus_point">
                      <?php echo form_error('bonus_point'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Minimum $ to use points</label>

								<input type="text" name="min_use_points" class="form-control" value="<?=set_value('min_use_points', isset($servicesSettings->min_use_points)?$servicesSettings->min_use_points:""); ?>" id="min_use_points">
                      			<?php echo form_error('min_use_points'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">Point expiry in years</label>
								<input type="text" name="expiry_year" class="form-control" value="<?=set_value('expiry_year', isset($servicesSettings->expiry_year)?$servicesSettings->expiry_year:""); ?>" id="expiry_year">
                      		<?php echo form_error('expiry_year'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>H.O. Charges</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">H.O. Fees (%)</label>

								<input type="text" name="ho_fees" class="form-control" value="<?=set_value('ho_fees', isset($servicesSettings->ho_fees)?$servicesSettings->ho_fees:""); ?>" id="ho_fees">
                      <?php echo form_error('ho_fees'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Per order Charges</label>

								<input type="text" name="per_order_charge" class="form-control" value="<?=set_value('per_order_charge', isset($servicesSettings->per_order_charge)?$servicesSettings->per_order_charge:""); ?>" id="per_order_charge">
                      <?php echo form_error('per_order_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Advertising %</label>

								<input type="text" name="advertising" class="form-control" value="<?=set_value('advertising', isset($servicesSettings->advertising)?$servicesSettings->advertising:""); ?>" id="advertising">
                      <?php echo form_error('advertising'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Fix weekly charges</label>

								<input type="text" name="fix_weekly_charges" class="form-control" value="<?=set_value('fix_weekly_charges', isset($servicesSettings->fix_weekly_charges)?$servicesSettings->fix_weekly_charges:""); ?>" id="fix_weekly_charges">
                      <?php echo form_error('fix_weekly_charges'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other Charges</label>

								<input type="text" name="other_charge" class="form-control" value="<?=set_value('other_charge', isset($servicesSettings->other_charge)?$servicesSettings->other_charge:""); ?>" id="other_charge">
                      <?php echo form_error('other_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Credit/Debit Card charges %</label>

								<input type="text" name="credit_debit_card_charges" class="form-control" value="<?=set_value('credit_debit_card_charges', isset($servicesSettings->credit_debit_card_charges)?$servicesSettings->credit_debit_card_charges:""); ?>" id="credit_debit_card_charges">
                      <?php echo form_error('credit_debit_card_charges'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Customers Charges & fees</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Area Service Pricing</label>

								<input type="text" name="area_service_pricing" class="form-control" value="<?=set_value('area_service_pricing', isset($servicesSettings->area_service_pricing)?$servicesSettings->area_service_pricing:""); ?>" id="area_service_pricing">
                      			<?php echo form_error('area_service_pricing'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Taxes</label>

								<input type="text" name="taxes" class="form-control" value="<?=set_value('taxes', isset($servicesSettings->taxes)?$servicesSettings->taxes:""); ?>" id="taxes">
                      <?php echo form_error('taxes'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax name</label>

								<input type="text" name="tax_name" class="form-control" value="<?=set_value('tax_name', isset($servicesSettings->tax_name)?$servicesSettings->tax_name:""); ?>" id="tax_name">
                      <?php echo form_error('tax_name'); ?>

							</div>
						 </div>
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information</label>

								<input type="text" name="other_info" class="form-control" value="<?=set_value('other_info', isset($servicesSettings->other_info)?$servicesSettings->other_info:""); ?>" id="other_info">
                      <?php echo form_error('other_info'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Service Charge</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Currency </label>

								<?php                       
                      echo form_dropdown('currency', $currencyOption,set_value('currency',isset($servicesSettings->currency)?$servicesSettings->currency:""),'id="currency" class="form-control"'); ?>
                      <?php echo form_error('currency'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Service call + 1/2 Hour work</label>

								<input type="text" name="service_call" class="form-control" value="<?=set_value('service_call', isset($servicesSettings->service_call)?$servicesSettings->service_call:""); ?>" id="service_call">
                      <?php echo form_error('service_call'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">1 Hr Chage</label>

								<input type="text" name="hr_charge" class="form-control" value="<?=set_value('hr_charge', isset($servicesSettings->hr_charge)?$servicesSettings->hr_charge:""); ?>" id="hr_charge">
                      <?php echo form_error('hr_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Horly charges after that</label>

								<input type="text" name="horly_charge" class="form-control" value="<?=set_value('horly_charge', isset($servicesSettings->horly_charge)?$servicesSettings->horly_charge:""); ?>" id="horly_charge">
                      <?php echo form_error('horly_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional Charges</label>

								<input type="text" name="additional_charge" class="form-control" value="<?=set_value('additional_charge', isset($servicesSettings->additional_charge)?$servicesSettings->additional_charge:""); ?>" id="additional_charge">
                      <?php echo form_error('additional_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Emergency Service Charge</label>

								<input type="text" name="emergency_service_charge" class="form-control" value="<?=set_value('emergency_service_charge', isset($servicesSettings->emergency_service_charge)?$servicesSettings->emergency_service_charge:""); ?>" id="emergency_service_charge">
                      <?php echo form_error('emergency_service_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional worker</label>

								<input type="text" name="additional_worker" class="form-control" value="<?=set_value('additional_worker', isset($servicesSettings->additional_worker)?$servicesSettings->additional_worker:""); ?>" id="additional_worker">
                      <?php echo form_error('additional_worker'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>After hours and week ends</h4></div>
						 
						 
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Service call + 1/2 Hour work</label>

								<input type="text" name="after_call_work" class="form-control" value="<?=set_value('after_call_work', isset($servicesSettings->after_call_work)?$servicesSettings->after_call_work:""); ?>" id="after_call_work">
                      <?php echo form_error('after_call_work'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">1 Hr Charge</label>

								<input type="text" name="after_hr_charge" class="form-control" value="<?=set_value('after_hr_charge', isset($servicesSettings->after_hr_charge)?$servicesSettings->after_hr_charge:""); ?>" id="after_hr_charge">
                      <?php echo form_error('after_hr_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Horly charges after that</label>

								<input type="text" name="after_horly_charge" class="form-control" value="<?=set_value('after_horly_charge', isset($servicesSettings->after_horly_charge)?$servicesSettings->after_horly_charge:""); ?>" id="after_horly_charge">
                      <?php echo form_error('after_horly_charge'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional workers</label>

								<input type="text" name="after_additional_worker" class="form-control" value="<?=set_value('after_additional_worker', isset($servicesSettings->after_additional_worker)?$servicesSettings->after_additional_worker:""); ?>" id="after_additional_worker">
                      <?php echo form_error('after_additional_worker'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other Charges</label>

								<input type="text" name="after_other_charge" class="form-control" value="<?=set_value('after_other_charge', isset($servicesSettings->after_other_charge)?$servicesSettings->after_other_charge:""); ?>" id="after_other_charge">
                      <?php echo form_error('after_other_charge'); ?>

							</div>
						 </div><!--col-sm-4-->				 
						 
						 
						 
					 </div><!--row-->
                 
                 
                 
			     </div><!--s-p-contract-form-->
                  
  

                  

                 <div class="clearfix"></div>

                   
                  </fieldset>


                  
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
    <style type="text/css">
    	h4 {
    border-bottom: 1px solid #937c67;
    padding: 10px 0;
    margin-bottom: 30px;
}
    </style>

    

    </div>
  </div>
