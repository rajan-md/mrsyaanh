<div class="page-content pad-top-zero">
    <div class="content container">
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
  
      <?php 
      //print_r($servicesData);
      if(!empty($servicesData))
      {
        extract($servicesData);
      }
      ?>
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Service Provider Sales information </h3>
            </div>
            <div class="widget-content">
              <form method="post" class="form-horizontal" enctype="multipart/form-data">
                
                
                <div class="row">
                    
                    <div class="col-md-2">
                      <div class="text-align-center"> 
                      <div style="height:10px"></div>
                      <?php if(!empty($featured_img)) { ?>
                      <img style="height:150px;width:150px; border: 1px solid #664f5c;" alt="64x64" src="<?=base_url($featured_img)?>" class="img-circle">
                      <?php } else { ?> 
                      <img style="height:150px;width:150px; border: 1px solid #664f5c;" alt="64x64" src="<?=base_url('skin/admin/')?>images/profile.jpg" class="img-circle"> 
                      <?php } ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <h3 class="no-margin">
                      
                      <?=isset($fname)?$fname:""; ?> <?=isset($lname)?$lname:""; ?>
                      
                      </h3>
                      <address>
                      <?php 
					   $business= isset($servicesData['business'])?$servicesData['business']:""; 
                      if($business=='yes') { ?> 
                      <strong><?=isset($servicesData['designation'])?$servicesData['designation']:""; ?></strong> at <strong><?=isset($servicesData['business_name'])?$servicesData['business_name']:""; ?></strong><br>
                      <?php } $services_type= isset($servicesData['business'])?$servicesData['business']:"";  ?>
                      <abbr title="Work email">Request Service:</abbr> <?=getServicesName($servicesData['services_type']) ?><br>
                      <abbr title="Work email">E-mail:</abbr> <?=isset($servicesData['email'])?$servicesData['email']:""; ?><br>
                      <abbr title="Work Phone">Phone:</abbr> <?=isset($servicesData['mobile'])?$servicesData['mobile']:""; ?><br>
                      <div style="height:5px"></div>
                      
                      </address>
                    </div>
                  </div>


                   	<div style="height:40px;"></div>                    
                <fieldset>              
                   	<div class="clearfix"></div>                    
                 	<div class="s-p-contract-form">
                 
                   	<div class="row">
					    
						<div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Start Date</label>

								<input type="text" name="start_date" class="form-control" id="services_start_date" value="<?=set_value('start_date',isset($userDetails->start_date)?$userDetails->start_date:" ")?>" required=""><!--hem--date-->

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
						<label class="profile_field">Service Category</label>

								<?php                       
                      echo form_dropdown('services_type', $servicestypeOptions,set_value('services_type',isset($servicesData['services_type'])?$servicesData['services_type']:""),'id="services_type" class="form-control"'); ?>
                      <?php echo form_error('services_type'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
						<label class="profile_field">Service Code</label>
						<input type="text" name="services_code" class="form-control" value="<?=set_value('services_code', isset($services_code)?$services_code:""); ?>" id="services_code">
                      <?php echo form_error('services_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Operator No.</label>

								<input type="text" name="operator_no" class="form-control" value="<?=set_value('operator_no', isset($operator_no)?$operator_no:""); ?>" id="operator_no">
                      			<?php echo form_error('operator_no'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">						 
							<label class="profile_field">Service Zip code</label>
								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      		<?php echo form_error('postal_code'); ?>
							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Debits/credits</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Coupons</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Discount Promotions</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">FC. Bonus Point %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Minimum $ to use points</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Point expiry in years</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>H.O. Charges</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">H.O. Fees (%)</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Per order Charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Advertising %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Fix weekly charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other Charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Credit/Debit Card charges %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Customers Charges & fees</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Area Service Pricing</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Taxes</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax name %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax Name %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Tax Name %</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other information</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>Service Charge</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Currency </label>

								<?php                       
                      echo form_dropdown('services_type', $servicestypeOptions,set_value('services_type',isset($services_type)?$services_type:""),'id="services_type" class="form-control"'); ?>
                      <?php echo form_error('services_type'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Service call + 1/2 Hour work</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">1 Hr Chage</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Horly charges after that</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional Charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Emergency Service Charge</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other Charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional worker</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 
						 <div class="clearfix"></div>
						 
						 <div class="col-md-12"><h4>After hours and week ends</h4></div>
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Currency </label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Service call + 1/2 Hour work</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">1 Hr Charge</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Horly charges after that</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						  <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Hourly charges after that</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Additional workers</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->
						 
						 <div class="col-md-4">					
						<div class="form-group">
						 
							<label class="profile_field">Other Charges</label>

								<input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:""); ?>" id="postal_code">
                      <?php echo form_error('postal_code'); ?>

							</div>
						 </div><!--col-sm-4-->				 
						 
						 
						 
					 </div><!--row-->
                 
                 
                 
			     </div><!--s-p-contract-form-->
                  
  

                  <div id="address">
                  <input type='hidden' class="field" id="street_number" disabled="true"/>
                  <input type='hidden' class="field" id="route"  disabled="true"/>                
                  <input type='hidden' class="field" id="latitude" name="site_latitude" value="<?=isset($latitude)?$latitude:""; ?>"  />               
                  <input type='hidden' class="field" id="longitude" name="site_longitude" value="<?=isset($longitude)?$longitude:""; ?>"/>             

                  </div> 

                 <div class="clearfix"></div>

                   
                  </fieldset>


                  
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
    <style type="text/css">
    	h4 {
    border-bottom: 1px solid #937c67;
    padding: 10px 0;
    margin-bottom: 30px;
}
    </style>

    </div>
  </div>
