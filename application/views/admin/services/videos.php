
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Service Videos <?=AddButton('videos/add')?></h2> 
          <?php getNotificationHtml(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Service Videos Management</h3>

            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example2">
                  <thead>
                    <tr>
                      <th>Id</th>                     
                      <th>Service</th>
                      <th>Source</th>
                      <th>Video</th>
                      <th>Video Url</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                <?php
                if(!empty($results))
                {
                  foreach ($results as $data)
                  {
                  ?>
                    <tr>
                      <td><?=$data->id?></td>
                      <td><?=getServicesName($data->service_id)?></td> 
                      <td><?=$data->video_source?></td>
                      <?php $video_thumbnail = video_thumbnail($data->video,$data->video_source); ?>
                      <td><a href="<?=$data->video?>" target="_blank"><img src="<?=!empty($video_thumbnail)?$video_thumbnail:'';?>" style="height:40px;"></a></td>
                      <td><a href="<?=$data->video?>" target="_blank"><?=$data->video?></a></td>
                      <td> 
                      <?php $status=$data->status; ?>
                      <a href="<?=base_url('admin/services/video_status/'.$data->id) ?>"><span class="label label-<?=($status==1)?'success':'info'; ?>"><?=($status==1)?'Active':'Deactive'; ?></span></a> </td>

                      <td class="hidden-xs">
                        <?php echo AllowedAction('admin/services/videos', 'edit', 'delete', 'Sure you want to delete this record?', $data->id) ?>
                      </td>
                    </tr>
                  <?php
                  }
                }
                else
                {
                  ?>
                  <tr>
                    <td colspan="6">No any videos found !!</td>                    
                  </tr>
                  <?php
                }
                ?>
                                        
                  </tbody>
                </table>
                <?php
                if(!empty($results))
                {
                  ?>
                  <div class="paginations"><?php echo $paginations; ?></div>
                  <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>