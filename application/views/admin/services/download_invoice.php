<html>
<body>

  <div class="main_container" style="margin: auto; width: 100%; border: 1px solid #e7e7e7; margin-bottom: 50px;">
    <div class="top_bg" style="background: #fff; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); display: inline-block; width: 100%; padding: 8px 0; margin-bottom: 30px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td align="left" style="float: left; margin: 0 0 0 14px;">
            &nbsp;&nbsp;<img src="<?=base_url('skin/front/images/logo.png')?>">
            
            </td>
            <td align="right" style="float: right;font-family: arial;margin: 16px 13px;font-size: 20px;">
            
            Service Provider Invoice&nbsp;&nbsp;&nbsp;
            
            </td>
          </tr>
        </tbody>
      </table>
    </div>	   
      <table style="font-family: arial; " width="100%" cellspacing="0" cellpadding="7" border="0">
      <tbody>
        <tr><td colspan="2" align="right" valign="top"><p><?=$generated_date; ?></p></td></tr>
        <tr>
          <td width="50%" align="left" valign="top">
            <?php if(!empty($name)){ ?><h3><?=$name;?></h3><?php } ?>
            <p>
              <?php if(!empty($email)){ ?><abbr title="Work email">E-mail:</abbr> <?=$email;?><br><?php } ?>
              <?php if(!empty($mobile)){ ?><abbr title="Work Phone">Phone:</abbr> <?=$mobile;?><br><?php } ?>                              
            </p>
          </td>
          <td width="50%" align="right" valign="top">
            <h3>Address</h3>
            <?=$address; ?>            
          </td>
        </tr>
      </tbody>
    </table>
	
  	<div style="font-family: arial;margin: auto; width: 60%; background: #212121;text-align: center;color: #fff;padding: 5px 0;border-radius: 30px;font-size: 14px; margin-top: 15px; margin-bottom: 15px;">Services : <?=$durationLabel; ?> ( <?=$start;?> - <?=$end;?>)</div>

    <table style="font-family: arial;border: 1px solid #e4e4e4;" width="100%" cellspacing="0" cellpadding="7" border="0">
      <tbody>

        <tr>
          <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="left">Booking Id</th>
          <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="left">Service Date</th>
          <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Initial Charges</th>
          <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Additional Charges</th>
          <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Sub Total</th>
        </tr>

        <?php
        if(!empty($records))
        {
          foreach ($records as $data)
          {
            ?>
              <tr>
                <td style="border: 1px solid #e4e4e4;" align="left"><?=$data['id']; ?></td>
                <td style="border: 1px solid #e4e4e4;" align="left"><?=$data['booking_datetime']; ?></td>
                <td style="border: 1px solid #e4e4e4;" align="right"><?=$data['amount']; ?></td>
                <td style="border: 1px solid #e4e4e4;" align="right"><?=$data['additional_amount']; ?></td>
                <td style="border: 1px solid #e4e4e4;" align="right"><?=$data['total']; ?></td>                                                     
              </tr>
              <?php
            }
          }
          else
          {
            ?>
            <tr><td colspan="5" style="border: 1px solid #e4e4e4;" align="left">No Service records found in this duration for this service provider.</td></tr>
            <?php
          }
          ?>

        <?php
        if(!empty($records))
        {
          ?>
         <tr>
          <th style="background: #e4e4e4;" colspan="2">&nbsp;</td>        
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right" colspan="2"><strong>Sub Total</strong></td>
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right"><strong><?=$sub_total; ?></strong></td>
        </tr>

        <tr>
          <th style="background: #e4e4e4;" colspan="2">&nbsp;</td>        
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right" colspan="2"><strong>H.O Charge ( - )</strong></td>
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right"><strong><?=$ho_total; ?></strong></td>
        </tr>

        <tr>
          <th style="background: #e4e4e4;" colspan="2">&nbsp;</td>        
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right" colspan="2"><strong>Grand Total</strong></td>
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right"><strong><?=$grand_total; ?></strong></td>
        </tr>
        <?php
      }
      ?>
      </tbody>
    </table>

    <?php
    if(!empty($notes))
    {
      ?>
      <br><br>
      <h4>Message For you :-</h4>
      <p><?=$notes;?></p>
    <?php
    }
    ?>




    <br><br>
    <h4>Note :-</h4>
    <p>1. Above declared amount against respective booking Id has marked as invoiced and that amount will paid to you within 5 working days.</p>
    <p>2. Grand Total is calculated after deducation of H.O Charges as defined criteria at the time of agreement.</p>
  </div>
</body>
</html>
