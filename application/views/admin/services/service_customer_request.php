
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Services Customer Request</h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Services Customer Request</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example2">
                  <thead>
                    <tr>
                      <th width="50">Id</th>
                      <th width="150">Service date</th>
                      <th>Name</th>
                      <th>Services</th>
                      <th>Amount</th>
                      <th width="120">Payment Status</th>
                      <th width="120">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(!empty($results))
                    {
                      foreach ($results as $data)
                      {
                        $address = getUserAddressbyId($data->address_id);
                        ?>
                          <tr>
                            <td><?=$data->id?></td> 
                            <td class="hidden-xs"><?=$data->booking_datetime?></td>                    
                            <td><?=getCustomerName($data->user_id) ?></td>
                            <td><?=getServicesName($data->services) ?></td>
                            <td class="hidden-xs"><?=getPriceFormate($data->amount)?></td>
                            <td> 
                            <?php $payment_status=$data->payment_status; ?>
                            <span class="label label-<?=!empty($payment_status)?'success':'warning';?>"><?=!empty($payment_status)?'Paid':'Unpaid';?></span>
                            </td>
                             <td> 
                            <?php
                            $status=$data->status;
                            $statusLabel = getServiceBookingStatusLabel($status);
                            $statusClass = getServiceBookingStatusClass($statusLabel);
                            ?>
                            <span class="<?=$statusClass; ?>"><?=$statusLabel; ?></span> </td>
                            <td class="hidden-xs">
                            <?php echo AllowedAction('admin/services/view_request', 'view', '', 'Sure you want to delete this services', $data->id) ?>
                            </td>
                          </tr>
                    <?php 
                        }
                      }
                    else
                    {
                      ?>
                      <tr>
                        <td colspan="9"> No any service booking available yet !!</td>                 
                      </tr>
                      <?php
                    }
                    ?>

                
                                        
                  </tbody>
                  <tfoot>
                    <tr><th colspan="8"> </th></tr>
                  </tfoot>
                </table>
                <?php if(!empty( $paginations)){ ?>
                  <div class="paginations"><?php echo $paginations; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
