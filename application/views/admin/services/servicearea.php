
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Service Area <?=AddButton('servicearea/add')?>  <a href="<?=base_url('admin/services/import_zipcode');?>" class="btn btn-success">Import Zipcodes</a></h2> 
          <?php getNotificationHtml(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Service Area / Zipcode Management</h3>

            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example2">
                  <thead>
                    <tr>
                      <th>Id</th>                     
                      <th>Country</th>
                      <th>State</th>
                      <th>City</th>
                      <th>Zipcode</th>
                      <th>Co-ordinates</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                <?php if(!empty($results))
                {
                      // echo "<pre>"; print_r($records); echo "</pre>"; die;
                foreach ($results as $data)
                {
                 ?>
                    <tr>
                      <td><?=$data->id?></td>
                      <td><?=$data->country?></td> 
                      <td><?=$data->place_name?></td>                      
                      <td><?=$data->area?></td>
                      <td><?=$data->zipcode?></td>
                      <td><?=$data->latitude.','.$data->longitude?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                      <a href="<?=base_url('admin/services/servicearea_status/'.$data->id) ?>"><span class="label label-<?=($status==1)?'success':'info'; ?>"><?=($status==1)?'Active':'Deactive'; ?></span></a> </td>

                      <td class="hidden-xs">
                        <?php echo AllowedAction('admin/services/servicearea', 'edit', 'delete', 'Sure you want to delete this record?', $data->id) ?>
                      </td>
                    </tr>
                  <?php
                  }
                }
                else
                {
                  ?>
                  <tr>
                      <td colspan="8">No any service areas found !!</td>
                    </tr>
                  <?php
                }
                ?>
                                        
                  </tbody>
                </table>
                <?php if(!empty($paginations)){ ?><div class="paginations"><?php echo $paginations; ?></div> <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
