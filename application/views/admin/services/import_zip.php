<div class="page-content pad-top-zero">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Csv Imports </h3>

            </div>
            <div class="widget-content">
             <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  <div class="control-group">
                  <div class="col-md-2">Choose Csv File</div>
                    <div class="col-md-9">
                    <div class="form-group">
                     <span class="btn btn-info fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Zipcode...</span>
                                        <input type="file" name="csvfile" id="csvfile">
                                    </span>
                      
                    </div>
                    </div>
                  </div>
                  </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     


    </div>
  </div>
