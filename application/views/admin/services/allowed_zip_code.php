<div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Service Area</h2> 
          
        </div>
      </div>
       <div class="row">
        <div class="col-md-12">
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div> 
      <?php 
      
      if(!empty($servicesData))
      {
        extract($servicesData);
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Allowed Zip/Postal Code </h3>
            </div>
            <div class="widget-content">

               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  <div class="control-group">
                    <div class="col-md-4"></div>
                  <div id="seleData" class="col-md-8"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="control-group">
                  <div class="col-md-4">
                    <label for="disabled-input" class="control-label ">Services</label>
                    </div>
                    <div class="col-md-8">
                    <div class="form-group">
                      <?php 
                      $services=$this->input->get('id');
                      if(!empty($services)){
                         $servicesZip=getCodeList($services);
                        $servicearea=explode(",", $servicesZip);
                        
                      }
                      echo form_dropdown('services', $servicesOptions,set_value('services',isset($services)?$services:""),'id="servicesa" class="form-control"'); ?>
                      <?php echo form_error('services'); ?>
                    </div>
                    </div>
                  </div>
                   <div class="control-group">
                  <div class="col-md-4">
                    <label for="hint-field" class="control-label"> Services Area (Zip/Postal Code) </label>
                    </div>
                    <div class="col-md-8">
                    <div class="form-group">
                      
                      <input type="hidden" name="servicearea" class="form-control" value="<?=set_value('servicearea'); ?>" placeholder="Search Zipcode">
                      <input type="text"  id="serviceareabox" class="form-control"  placeholder="Search Zipcode">

                      <?php echo form_error('servicearea'); ?>
                       
                    </div>
                     </div>
                  </div>
                  

                 
                  
                  
                  

                  
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
