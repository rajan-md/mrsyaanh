
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Services Providers <?=AddButton('addprovider') ?></h2> 
          <?php getNotificationHtml(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header">
              <i class="icon-table"></i>
              <h3>Services Providers</h3>
              <span class="buttoninheading"><a href="<?=base_url('admin/services/invoices'); ?>" class="btn btn-warning">Invoices</a></span>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example2">
                  <thead>
                    <tr>
                      <th width="50">Id</th>
                      <th>Name</th>
                      <th>Service Provider</th>
                      <th>Service</th>
                      <th width="80">Zipcode</th>
                      <th width="120">Created</th>
                      <th width="70">Settings</th>
                      <th width="70">Status</th>
                      <th width="130">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    //echo "<pre>"; print_r($servicesData); echo "</pre>";
                    if(!empty($servicesData))
                    {
                      foreach ($servicesData as $data)
                      {
                      ?>
                      <tr>
                        <td><?=$data->id ?></td>
                        <td><?=getProviderName($data->id); ?></td> 
                        <td><?=!empty($data->business_name)?$data->business_name:''; ?></td>                  
                        <td><?=getServicesName($data->services_type) ?></td>
                        <td><?=$data->service_zip_code?></td>                      
                        <td class="hidden-xs"><?=!empty($data->request_date)?date('d-m-Y',strtotime($data->request_date)):'';?></td>

                        <td class="hidden-xs">
                          <a href="<?=base_url('admin/services/service_provider_settings/'.$data->id) ?>" title="Service settings"><i class="icon-large icon-cogs"></i></a>
                        </td>
                        
                        <td> 
                        <?php $status=$data->service_status; ?><a href="<?=base_url('admin/services/service_status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else {
                          $status=2;
                          echo 'info'; 

                         } ?>"><?=getStatus($status)?></span></a> </td>


                        <td class="hidden-xs">                        
                          <a href="<?=base_url('admin/services/service_request_details/'.$data->id) ?>" title="View request details" class="btn btn-xs btn-info"> <i class="icon-large icon-eye-open"></i> </a>
                          <a href="#myModal" data-toggle="modal" data-id="<?=$data->user_id ?>" title="Financial Statement" class="open-financial-popup btn btn-xs btn-warning" <?=(empty($status) ||  $status!=1)?'disabled="disabled"':'';?>><i class="icon-large icon-book"></i> </a>
                          <?php echo AllowedAction('admin/services/service_provider_records', 'edit', 'delete', 'Sure you want to delete this services', $data->id) ?>
                            
                        </td>
                      </tr>
                      <?php
                      }
                    }
                    else
                    {
                      ?>
                      <tr>
                        <td colspan="9"> No any service providers or request available !!</td>                 
                      </tr>
                      <?php
                    }
                    ?>

                
                                        
                  </tbody>
                  <tfoot>
                      <tr>
                        <th colspan="9"> </th>                 
                      </tr>
                  </tfoot>
                </table>
                <?php if(!empty( $paginations)){ ?>
                  <div class="paginations"><?php echo $paginations; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>

                  
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
        <h4 class="modal-title">Service Provider Financial Statement</h4>
      </div>
      <form id="statement_form" method="get" action="<?=base_url('admin/services/create-invoice'); ?>">
        <div class="modal-body">
            <div id="popup_message_box"></div>
            <input type="hidden" name="service_provider_id" id="service_provider_id">
            <table id="statement_table" class="table">                
                <tr>
                  <td colspan="2">
                    <?php echo form_dropdown('duration', $durationOptions,set_value('duration',isset($duration)?$duration:""),'id="statement_duration" class="form-control"'); ?>
                  </td>
                </tr>
                <!--tr id="date_selector" style="display: none;">              
                  <td width="50%"><input type="text" id="from_date" name="from" class="datepicker form-control" autocomplete="off" readonly="readonly" placeholder="From date"></td>
                  <td><input type="text" id="to_date" name="to" class="datepicker form-control" readonly="readonly" placeholder="To date"></td>
                </tr-->
            </table>
            <div id="statement_output"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="generate_statement" type="submit" class="btn btn-primary">Generate Statement</button>
        </div>
      </form>
    </div>
  </div>
</div>