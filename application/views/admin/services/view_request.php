<div class="page-content pad-top-zero">
    <div class="content container">
    
      
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Customer Request Details </h3>
            </div>
            
            <div class="widget-content">
             <table class="table table-striped table-images noborder" id="example">

              <tr>
                <td>Booking Id : <?=isset($servicesData->id)?'#'.$servicesData->id:""; ?></td>
                <td>Booking Date : <?=isset($servicesData->booking_datetime)?$servicesData->booking_datetime:""; ?></td>
              </tr>

              <tr>
                <td width="50%">
                  <table width="100%">
                    <?php
                      $status = isset($servicesData->status)?$servicesData->status:"";
                      $statusLabel = getServiceBookingStatusLabel($status);
                      $statusClass = getServiceBookingStatusClass($statusLabel);
                    ?>
                    <tr>
                      <td>Service Type : <?=getServicesName(isset($servicesData->services)?$servicesData->services:""); ?></td>
                    </tr>
                    <tr>
                      <td>Request Status : <label class="<?=$statusClass;?>"><?=$statusLabel;?></label></td>
                    </tr>
                    <tr>
                      <td>Payment Status : <?php $payment_status=isset($servicesData->payment_status)?$servicesData->payment_status:"" ; ?>
                            <span class="label label-<?=!empty($payment_status)?'success':'warning';?>"><?=!empty($payment_status)?'Paid':'Unpaid';?></span>
                      </td>
                    </tr>
                               
                  </table>
                </td>

                <?php
                $customer_id = !empty($servicesData->user_id)?$servicesData->user_id:"";
                $address_id = !empty($servicesData->address_id)?$servicesData->address_id:"";
                ?>
                <td>
                  <table width="100%">
                    <tr>
                      <td width="50%">
                          <strong>Customer Details</strong><br>
                          <?=getCustomerName($customer_id) ?><br>
                          <strong>Email : </strong><?=getCustomerEmail($customer_id); ?><br>
                          <strong>Contact : </strong><?=getCustomerContact($customer_id); ?><br>
                      </td>
                       <td>
                          <strong>Service Address</strong><br>
                          <?php
                          $addr = getAddressDetails($address_id); 
                          echo !empty($addr->site_location)?$addr->site_location:'';
                          ?>
                      </td>
                    </tr>
                    
                  </table>
                </td>
              </tr>

              <?php
              //print_r($servicesData);
              $currencyCode = !empty($servicesData->code)?$servicesData->code:getCurrencyCode();
              $amount = !empty($servicesData->amount)?$servicesData->amount:0;
              $discount = !empty($servicesData->discount)?$servicesData->discount:0;
              $tax = !empty($servicesData->tax_amount)?$servicesData->tax_amount:0;
              $total_amount = !empty($servicesData->total_amount)?$servicesData->total_amount:0;

              $grand_total = $total_amount;
              ?>

              <tr>
                <td colspan="2"><h3>Payment Summary</h3></td>
              </tr>

              <tr>
                <td colspan="2">
                  <table width="100%" class="withborder">
                    <tr>
                      <th>Payment Description</th>
                      <th>Amount</th>
                      <th>Discount</th>
                      <th>Tax</th>                      
                      <th>Sub Total</th>
                    </tr>
                    <tr>
                      <td>Minimum Service Charges</td>
                      <td><?=getFormatedPriceByCode($currencyCode,$amount); ?></td>
                      <td><?=getFormatedPriceByCode($currencyCode,$discount); ?></td>
                      <td><?=getFormatedPriceByCode($currencyCode,$tax); ?></td>
                      <td><?=getFormatedPriceByCode($currencyCode,$total_amount); ?></td>
                    </tr>

                    <?php
                    if(!empty($operationData))
                    {
                      $add_amount = !empty($operationData['additional_hrs_charges'])?$operationData['additional_hrs_charges']:0;
                      $add_discount = 0;
                      $add_tax = !empty($operationData['additional_amt_tax'])?$operationData['additional_amt_tax']:0;
                      $add_total_amount = !empty($operationData['additional_subtotal'])?$operationData['additional_subtotal']:0;
                      $grand_total = $grand_total + $add_total_amount;
                      ?>

                      <tr>
                        <td>Post Service Charges</td>
                        <td><?=getFormatedPriceByCode($currencyCode,$add_amount); ?></td>
                        <td><?=getFormatedPriceByCode($currencyCode,$add_discount); ?></td>
                        <td><?=getFormatedPriceByCode($currencyCode,$add_tax); ?></td>
                        <td><?=getFormatedPriceByCode($currencyCode,$add_total_amount); ?></td>
                      </tr>

                    <?php
                    }
                    ?>                   
                    <tr>
                      <td colspan="4" align="right"><strong>Grand Total</strong></td>
                      <td align="left"><strong><?=getFormatedPriceByCode($currencyCode,$grand_total); ?></strong></td>
                    </tr>
                  </table>
                </td>
              </tr>

              <?php
              if(!empty($operationData) && !empty($operationData['is_service_ended']))
              {
                $id = !empty($servicesData->id)?$servicesData->id:"";
                $start_time = !empty($operationData['service_start_time'])?$operationData['service_start_time']:'';
                $end_time = !empty($operationData['service_end_time'])?$operationData['service_end_time']:'';
                $additional_amount = !empty($operationData['additional_subtotal'])?$operationData['additional_subtotal']:0;

                if(!empty($end_time) && $end_time!='0000-00-00 00:00:00')
                {
                    $allocated_time = getAllocatedTime($id);
                    $service_allocated = getDuration($allocated_time,'minute');
                    $time_spent = (!empty($start_time) && !empty($end_time))?shownExpendedTime($start_time,$end_time):0;
                    $additional_time_display = (!empty($start_time) && !empty($end_time))?shownAdditionalTime($start_time,$end_time,$allocated_time):0;
                }

              ?>
                <tr>
                  <td colspan="2"><h3>Job Summary</h3></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <table width="100%" class="withborder">
                      <tr>
                        <td>Job Started</td>
                        <td><?=!empty($operationData['service_start_time'])?date('d M, Y h:i A',strtotime($operationData['service_start_time'])):0; ?></td>
                        <td>Job Ended</td>
                        <td><?=!empty($operationData['service_end_time'])?date('d M, Y h:i A',strtotime($operationData['service_end_time'])):0; ?></td>
                      </tr>

                      <tr>
                        <td>Time Allocated</td>
                        <td><?=$service_allocated; ?></td>
                        <td>Time Spent</td>
                        <td><?=$time_spent; ?></td>
                      </tr>

                      <tr>
                        <td>Additional Time</td>
                        <td><?=$additional_time_display; ?></td>
                        <td>Additional Amount</td>
                        <td><?=getFormatedPriceByCode($currencyCode,$additional_amount); ?></td>
                      </tr>

                      <tr>
                        <td>Additional Operators? </td>
                        <td><?=!empty($additional_operator)?'Yes':'No';?></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>
                  </td>
                </tr>              
              <?php
              }

              $vendor_id = !empty($servicesData->vendor_id)?$servicesData->vendor_id:"";
              $operator_id = !empty($servicesData->operator_id)?$servicesData->operator_id:"";
              if(!empty($vendor_id))
              {
                ?>
                <tr>
                  <td colspan="2"><h3>Provider Details</h3></td>
                </tr>
                <tr>
                  <td width="50%">
                    <table width="100%">
                      <tr>
                        <td>
                            <strong>Service Provider Details</strong><br>
                            <?=getCustomerName($vendor_id) ?><br>
                            <strong>Email : </strong><?=getCustomerEmail($vendor_id); ?><br>
                            <strong>Contact : </strong><?=getCustomerContact($vendor_id); ?><br>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <?php
                    if(!empty($operator_id))
                    {
                      ?>
                      <table width="100%">
                        <tr>
                          <td>
                              <strong>Operator Details</strong><br>
                              <?=getCustomerName($operator_id) ?><br>
                              <strong>Email : </strong><?=getCustomerEmail($operator_id); ?><br>
                              <strong>Contact : </strong><?=getCustomerContact($operator_id); ?><br>
                          </td>
                        </tr>
                      </table>
                      <?php
                    }
                    ?>
                    
                  </td>
                </tr>
                <?php
              }
              ?>
            </table>
            </div>
          </div>
        </div>
      </div>
     


    </div>
  </div>
