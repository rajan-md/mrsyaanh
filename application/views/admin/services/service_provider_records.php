
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Service Information & Charges <?=AddButton('service_provider_records/add/'.$id)?></h2> 
          <?php getNotificationHtml();?>
          <?php
          if(!empty($this->session->flashdata('message')))
          {
          ?>
          <div class="label-msg  btn-success"><?php echo $this->session->flashdata('message'); ?></div>
          <?php
          }
          if(!empty($this->session->flashdata('message')))
          {
          ?>
          <div class="label-msg  btn-success"><?php echo $this->session->flashdata('message'); ?></div>
          <?php
          }
          ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Services Provider Request</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>Business name</th>                
                      <th>Services Category</th>
                      <th>ZipCode</th>
                      <th>Charges</th>
                      <th width="80">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                       <?php
                       //echo "<pre>"; print_r($servicesData); echo "</pre>";
                       if(!empty($servicesData)) {
                      foreach ($servicesData as $data) { ?>
                    <tr>
                      <td><?=$data->business_name ?></td>  
                      <td><?=getServicesName($data->service_id) ?></td>                      
                      <td><?=$data->service_zip_code ?></td>
                      <td><?=getCurrencySymbol($data->currency) ?><?=$data->hr_charge ?></td>
                      <td> 
                      <?php $status=$data->status; ?><a href="<?=base_url('admin/services/service_records_status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else {
                        $status=2;
                        echo 'info'; 

                       } ?>"><?=getStatus($status)?></span></a> </td>
                      <td class="hidden-xs">
                      <?php echo AllowedAction('admin/services/service_provider_records/'.$data->services_request_id, 'edit', '', 'Sure you want to delete this services', $data->id , true) ?>                      </td>
                    </tr>

                    <?php } } ?>
                                        
                    
                      </td>
                    </tr>

                
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th> 
          <th></th>
          <th></th>
          
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
