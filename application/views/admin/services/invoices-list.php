
  <div class="page-content">
    <div class="content container">

      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Invoices <a href="<?=base_url('admin/services/create-invoice')?>" class="btn btn-s-md btn-info"> Generate/Create Invoice</a></h2> 
          </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Invoices</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <form action="" method="get">
                  <table class="table table-striped table-images">
                      <tr>
                        <td><?php echo form_dropdown('service_provider_id', $serviceProviderOptions,set_value('service_provider_id',isset($service_provider_id)?$service_provider_id:""),'id="service_provider_id" class="form-control"'); ?></td>
                        <td><input type="text" name="invoiceId" id="invoiceId" class="form-control" placeholder="Invoice Id" value="<?=set_value('invoiceId',isset($invoiceId)?$invoiceId:"");?>"></td>
                        <td><button type="submit" class="btn btn-success">Filter</button></td>
                        <td>&nbsp;</td>
                      </tr>
                  </table>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
      
        <div class="row">
          <div class="col-lg-12">
            <div class="widget">
              <div class="widget-content">
                <div class="body">

                  <table class="table table-striped table-images" id="example1" cellpadding="5">
                    <thead>
                      <tr>
                        <th>Invoice Id</th>                        
                        <th>Service Provider</th>
                        <th style="text-align:right;">Invoiced Amount</th>                        
                        <th>Invoiced Date</th>
                        <th>Bookings IDs Included </th>
                        <th>Payment</th>
                        <th>Action</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(!empty($records))
                      {
                        foreach ($records as $rec)
                        {
                          ?>
                            <tr>
                              <td>#<?=$rec->id; ?></td>                              
                              <td><?=getServiesProviderName($rec->user_id); ?></td>                              
                              <td align="right"><?=getFormatedPriceByCode($rec->currency_code,$rec->payable_amount); ?></td>                              
                              <td><?=date('d M, Y h:i A', strtotime($rec->created)); ?></td>
                              <td><?=$rec->service_booking_ids; ?></td>
                              <td>
                                <?php
                                if(empty($rec->is_paid))
                                {
                                  ?>
                                  <a href="<?=base_url('admin/services/mark-invoice-paid/'.$rec->id); ?>" title="I fyou have made payment of invoice, then mark as paid !!" class="label label-warning" onclick="return confirm('Are you sure that you have made the payment against this Invoice? Once it will be mark PAID then it will not be reverted back. So, Please confirm and continue with OK button.');">
                                    Mark as PAID
                                  </a>
                                  <?php
                                }
                                else
                                {
                                  ?>
                                    <label class="label label-success">Paid</label>
                                  <?php
                                }
                                ?>
                              </td>
                              <td>
                                <a href="<?=base_url('admin/services/download-invoice/'.$rec->id);?>" title="Download Invoice" class="btn btn-xs btn-success"> <i class="icon-large icon-download-alt"></i> </a>
                                <a href="<?=base_url('admin/services/resend-invoice/'.$rec->id);?>" title="Send Invoice To Service Provider" class="btn btn-xs btn-primary"> <i class="icon-large icon-envelope"></i> </a>
                              </td>                              
                            </tr>
                          <?php
                        }
                      }
                      else
                      {
                        ?>
                          <tr><td colspan="7">Nothing found.</td></tr>
                        <?php
                      }
                      ?>
                    </tbody>
                    <tfoot>
                        <tr>
                          <th colspan="7"> </th>
                        </tr>
                    </tfoot>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>