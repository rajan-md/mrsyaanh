<div class="page-content pad-top-zero">
    <div class="content container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add New Service Provider</h3>
            </div>
            <div class="widget-content">




            	<form role="form" id="service_request" action="" method="post" >
			    <input type="hidden" id="mobbox" name="mobbox" value="+1">
				<fieldset> 
                    <div class="row">

	                	<div class="col-sm-12"> 
							<div class="form-group">
								<label><?=$this->lang->line('SELECT_SERVICE');?><span class="redstar">*</span></label>
								 <div class="form-group">
								 <?php echo form_dropdown('chooseserices', $serviceOptions,set_value('chooseserices'),'id="chooseserices" class="form-control" required=""'); ?>
								 <?php echo form_error('chooseserices'); ?>
				                </div>
							</div>
						</div>	

						<div class="col-sm-12"> 
							<div class="form-group">
								<label><?=$this->lang->line('DO_YOU_HAVE_OWN_A_BUSINESS_NOW');?><span class="redstar">*</span></label>
								<div>									
					                <label class="cus_radio">
					                  <input type="radio" name="business" value="no" required="" checked="checked">
					                  <?=$this->lang->line('NO');?></label>
					                  <label class="cus_radio">
					                  <input type="radio" name="business" value="yes" required="">
					                  <?=$this->lang->line('YES');?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div id="businessDetails" style="display: none;">
									<div class="form-group">
										<input type="text" id="business_name" name="business_name" placeholder="<?=$this->lang->line('BUSINESS_NAME');?>" class="business_name form-control" >
									</div>
									<div class="form-group">
										<?php echo form_dropdown('business_type', bussinessTypeOption(),set_value('business_type',isset($business_type)?$business_type:""),'id="business_type" class="form-control"'); ?>
			                      		<?php echo form_error('business_type'); ?>
									</div>
									<div class="form-group">
										<label><?=$this->lang->line('DO_YOU_HAVE_A_LICENSE');?><span class="redstar">*</span></label>
										<div>
										 	<label class="cus_radio"><input type="radio" name="license" value="no" required="" checked="checked"><?=$this->lang->line('NO');?></label>
											<label class="cus_radio"><input type="radio" name="license" value="yes" required=""><?=$this->lang->line('YES');?></label>
										</div>
									</div>

								<div id="licenseDetails" style="display:none">
									<div class="form-group">
										<input type="text" id="license_details" name="license_details" placeholder="<?=$this->lang->line('LICENSE_DETAILS');?>" class="f1-last-name form-control" >
									</div>
								</div>					
		                    </div>
		                </div>

		                <div class="col-sm-6"> 
							<div class="form-group">
		                    	<label><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>
								<input type="text" name="business_holder_fname" placeholder="<?=$this->lang->line('FIRST_NAME');?>" class="f1-fname form-control" id="business_holder_fname" required="">
								<?php echo form_error('business_holder_fname'); ?>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
		                    	<label><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>
								<input type="text" name="business_holder_lname" placeholder="<?=$this->lang->line('LAST_NAME');?>" class="f1-lname form-control" id="business_holder_lname" required="">
								<?php echo form_error('business_holder_lname'); ?>
							</div>
						</div>

	                    <div class="col-sm-12 clear">
		                    <div class="form-group">
		                    	<label><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>
								<input type="email" name="business_email" placeholder="<?=$this->lang->line('EMAIL');?>" class="f1-email form-control" id="business_email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" required="">
								<?php echo form_error('business_email'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group personal_phonebox">
								<label><?=$this->lang->line('PHONE_NO');?><span class="redstar">*</span></label>
								<input name="phonecode" placeholder="<?=$this->lang->line('ENTER_PHONE_NO');?>." class="f1-last-name form-control" id="phonecode" type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}" title="Invalid Mobile No" required="">
								<?php echo form_error('phonecode'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>
								<input type="text" name="service_zipcode" placeholder="<?=$this->lang->line('ZIP_CODE');?>" class="f1-last-name form-control" id="service_zipcode" required="">
								<?php echo form_error('service_zipcode'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label><?=$this->lang->line('MESSAGE');?></label>
								<textarea name="message" class="f1-last-name form-control"></textarea>
								<?php echo form_error('message'); ?>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-actions">
                  <div>
                    <button class="btn btn-success" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>

			  </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
