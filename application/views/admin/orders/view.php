<div class="page-content pad-top-zero">
    <div class="content container">
    
      
       <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Customer Request Details </h3>
            </div>
            <div class="widget-content">
              <?php
                $paymentMethod = paymentMethodOption();
                $shippingMethod = shippingMethodOption();
              ?>
             <table class="table table-striped table-images noborder" id="example">

              <tr>
                <td width="50%">
                  <p>
                    <strong>Order Id : <?=!empty($ordersData->id)?'#'.$ordersData->id:""; ?></strong><br>
                    <strong>Ordered Date : </strong> <?=!empty($ordersData->orderdata)?$ordersData->orderdata:""; ?><br>
                    <strong>Shipping Method : </strong> <?=!empty($ordersData->shipping_method)?$shippingMethod[$ordersData->shipping_method]:""; ?><br>
                    <strong>Payment Method : </strong> <?=isset($ordersData->payment_method)?$paymentMethod[$ordersData->payment_method]:""; ?><br>
                  </p>
                  <p><strong>Customer Information</strong></p>
                  <p>
                    <strong><?=getCustomerName(isset($ordersData->user_id)?$ordersData->user_id:"") ?></strong><br>
                    <strong>Email : </strong> <?=getCustomerEmail(isset($ordersData->user_id)?$ordersData->user_id:"") ?><br>
                    <strong>Contact : </strong> <?=getCustomerContact(isset($ordersData->user_id)?$ordersData->user_id:"") ?><br>                    
                  </p>

                </td>
                <td>                  
                  <p><strong>Shipping Address</strong></p>
                  <p><?=shippingAddress($ordersData->id); ?></p>
                </td>
              </tr>

              <tr>
                <td colspan="2"><h4>Ordered Items</h4></td>
              </tr>

              <tr>
                <td colspan="2">
                  <table cellpadding="5" cellspacing="5" width="100%" class="withborder">
                    <thead>
                      <tr>
                        <th colspan="2">Product</th>
                        <th>Qty</th>
                        <th>unit Price</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <?php
                    if(!empty($orderItems))
                    {
                      ?>
                          <tbody>
                            <?php
                            foreach ($orderItems as $item)
                            {
                              ?>
                              <tr>
                                <td width="80"><?=getProductImage($item->product_id); ?></td>
                                <td><?=getProductName($item->product_id); ?></td>
                                <td><?=$item->qty; ?></td>
                                <td><?=getPriceFormate($item->unit_price); ?></td>
                                <td><?=getPriceFormate($item->prices); ?></td>
                              </tr>
                              <?php
                            }
                            ?>

                            <tr>
                              <td colspan="2" rowspan="4">&nbsp;</td>
                              <td colspan="2" align="right"><strong>Sub Total</strong></td>
                              <td><?=isset($ordersData->subtotal)?getPriceFormate($ordersData->subtotal):""; ?></td>
                            </tr>

                            <tr>
                              <td colspan="2" align="right"><strong>Discount</strong></td>
                              <td><?=isset($ordersData->discount)?getPriceFormate($ordersData->discount):""; ?></td>
                            </tr>

                            <tr>
                              <td colspan="2" align="right"><strong>Shipping Charge</strong></td>
                              <td><?=isset($ordersData->shipping_amount)?getPriceFormate($ordersData->shipping_amount):""; ?></td>
                            </tr>

                            <tr>
                              <td colspan="2" align="right"><strong>Grand Total</strong></td>
                              <td><?=isset($ordersData->total)?getPriceFormate($ordersData->total):""; ?></td>
                            </tr>
                          </tbody>
                      <?php
                    }
                    ?>
                  </table>  
                </td>
              </tr>

              
            </table>
            </div>
          </div>
        </div>
      </div>
     


    </div>
  </div>
