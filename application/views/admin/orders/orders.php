
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Orders Management </h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php }  ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Orders Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>#ID</th>
                      <th>Username</th>
                      <th>Amount</th>
                      <th>Payment Method</th>
                      <th width="150">Created</th>
                      <th width="120">Order Status</th>
                      <th width="120">Payment Status</th>
                      <th width="80">Action</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if(!empty($ordersData))
                  {
                    foreach ($ordersData as $data)
                    {
                      ?>
                      <tr>
                        <td>
                        <?=isset($data->id)?$data->id:""?>
                        </td>  
                        <td><?=getCustomerName(isset($data->user_id)?$data->user_id:"0")?></td>                     
                        <td><?=getPriceFormate(isset($data->total)?$data->total:"0")?></td>                      
                        <td class="hidden-xs"><?=isset($data->payment_method)?$data->payment_method:""?></td>
                        <td class="hidden-xs"><?=isset($data->orderdata)?$data->orderdata:""?></td>
                        <td> 
                        <?php $status=isset($data->orderstatus)?$data->orderstatus:""; ?>
                         <a href="<?=base_url('admin/page/status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                          <td> 
                        <?php $payment_status=isset($data->payment_status)?$data->payment_status:"" ; ?>
                        <span class="label label-<?=!empty($payment_status)?'success':'warning'; ?>"><?=!empty($payment_status)?'Paid':'Unpaid';?></span> </td>
                        <td class="hidden-xs"><a href="<?=base_url('admin/orders/view/'.$data->id) ?>" class="btn btn-xs btn-primary"><i class="icon-large icon-eye-open"></i></a></td>
                      </tr>
                      <?php
                    }
                  }
                  ?>                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
