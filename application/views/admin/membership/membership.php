
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Membership Plans <?=AddButton('add')?></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Membership Plans Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th width="50">#</th>
                      <th>Name</th>
                      <th>Membership Fee</th>
                      <th>Description</th>
                      <th width="150">Created</th>
                      <th width="80">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($membershipData)) {
                      foreach ($membershipData as $data) { ?>
                    <tr>
                      <td>
                      <?php if(!empty($data->featured_img)) { ?>
                      <img style="height:30px;width:30px"  src="<?=base_url($data->featured_img)?>" >
                      <?php } else { ?> 
                      <img style="height:30px;width:30px"  src="<?=base_url('skin/admin/')?>images/no_image.jpg" > 
                      <?php } ?>
                      </td>                      
                      <td><?=$data->name?></td>                      
                      <td><?=getPriceFormate($data->fee)?></td>                      
                      <td class="hidden-xs"><?=strip_tags(substr($data->description,0,50)).'...'?></td>
                      <td class="hidden-xs"><?=$data->created?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                       <a href="<?=base_url('admin/membership/status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                      <td class="hidden-xs">
                      <?php echo AllowedAction('admin/membership', 'edit', 'delete', 'Sure you want to delete this membership', $data->id) ?>
                      
                      </td>
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
