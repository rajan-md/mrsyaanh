<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<title>Lapsmile</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="<?=base_url('skin/admin/css/bootstrap.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/thin-admin.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/font-awesome.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/style/style.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/style/dashboard.css')?>" rel="stylesheet">

<link href="<?=base_url('skin/admin/css/demo_page.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/demo_table.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/style/nestable.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/jquery.fileupload-ui.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/wizard_progress.css')?>" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
 
</head>
<body>

<div class="container text-center" style="margin-top: 150px">
	<div class="row">
	<div class="col-md-12">
		<div class="header-logo">
			<h1 id="logo1"><img src="<?=base_url('skin/front/images/logo-footer.png')?>" /></h1>
			
		</div>
		
		<div class="content">
				<h1><p style="font-size: 40px;text-transform: uppercase;color:#fff">Access denied <br/> You have insufficient permissions</p></h1>
		</div>
	
	</div>
	</div>
</div>

	
	

</body>
</html>
