
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Products <?=AddButton('add')?></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Products Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th width="50">#</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Price</th>
                      <th>Feature</th>
                      <th width="150">Created</th>
                      <th width="80">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($productsData)) {
                      foreach ($productsData as $data) { ?>
                    <tr>
                      <td>
                      <?=getProductImage($data->id)?>
                      </td>                      
                      <td><?=$data->name?></td> 
                      <td class="hidden-xs"><?=getCategoryName($data->category)?></td>
                      <td><?=getPriceFormate($data->price)?></td>
                      <td> 
                      <?php  $feature=$data->feature; ?>
                       <a href="<?=base_url('admin/products/feature/'.$data->id) ?>">
                        <?php if($feature==1) { echo '<i class="icon-large icon-star"></i>'; } else { echo '<i class="icon-large icon-star-empty"></i>'; } ?></a> </td>    
                  
                      <td class="hidden-xs"><?=$data->created?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                       <a href="<?=base_url('admin/products/status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                      <td class="hidden-xs">
                      <?php echo AllowedAction('admin/products', 'edit', 'delete', 'Sure you want to delete this products', $data->id) ?>
                     
                      </td>
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
