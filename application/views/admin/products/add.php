<div class="page-content pad-top-zero">
    <div class="content container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add New Products </h3>
            </div>

            <div class="widget-content">
               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  
                   <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">
                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                    if(!empty($language)) {
                     foreach ($language as $lang) { ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                    <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                    <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Name</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="name[]" class="form-control" value="<?=set_value('name[0]'); ?>" id="normal-field">
                       <?php echo form_error('name[0]'); ?>
                    </div>
                    </div>
                  </div> 
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      
                        <textarea class="form-control ckeditor"  id="text" name="description[]" rows="6"><?=set_value('description[0]'); ?></textarea>
                         <?php echo form_error('description[0]'); ?>
                    </div>
                    </div>
                  </div>
                  

                    </div>
                     <?php  } } ?>

                        <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">SKU</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="sku" class="form-control" value="<?=set_value('sku'); ?>" id="normal-field">
                       <?php echo form_error('sku'); ?>
                    </div>
                    </div>
                  </div>  


                  <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Price</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="price" class="form-control" value="<?=set_value('price'); ?>" id="normal-field">
                       <?php echo form_error('price'); ?>
                    </div>
                    </div>
                  </div> 
                  <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Quantity</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="quantity" class="form-control" value="<?=set_value('quantity'); ?>" id="normal-field">
                       <?php echo form_error('quantity'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Height</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="height" class="form-control" value="<?=set_value('height'); ?>" id="normal-field">
                       <?php echo form_error('height'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Weight</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="weight" class="form-control" value="<?=set_value('weight'); ?>" id="normal-field">
                       <?php echo form_error('weight'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="disabled-input" class="control-label ">Products Category</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <?php echo form_dropdown('category', $categoryOptions,set_value('category',isset($category)?$category:""),'id="category" class="form-control"'); ?>
                      <?php echo form_error('category'); ?>
                    </div>
                    </div>
                  </div>
                    
                  </div>
                </div>
              </div>

                  

                  <div class="control-group">
                  <div class="col-md-2">
                    
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                     <span class="btn btn-info fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Pic...</span>
                                        <input type="file" name="productimg[]" multiple="multiple" id="featured_img">
                                    </span>
                      
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="hint-field" class="control-label"> Meta Title </label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_title" rows="3"><?=set_value('meta_title', isset($meta_title)?$meta_title:""); ?></textarea>
                       
                    </div>
                     </div>
                  </div>
                  
                  
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Meta Key</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_key" rows="3"><?=set_value('meta_key', isset($meta_key)?$meta_key:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="disabled-input" class="control-label ">Meta Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_description" rows="3"><?=set_value('meta_description', isset($meta_description)?$meta_description:""); ?></textarea>
                       
                    </div>
                    </div>
                  </div>
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
