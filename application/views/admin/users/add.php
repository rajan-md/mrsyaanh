<div class="page-content pad-top-zero">
    <div class="content container">
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-align-left"></i>
                        <h3>Add New Users </h3>
                    </div>
                    <div class="widget-content">
                        <form method="post" class="form-horizontal">
                            <input type="hidden" id="mobbox" name="mobbox" value="+1">
                            <fieldset>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Role</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <?php echo form_dropdown('role', $roleOptions,set_value('role',isset($role)?$role:""),'id="role" class="form-control"'); ?>
                                                <?php echo form_error('role'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div id="companybox" class="control-group" style="display: none;">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Company Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="companyname" class="form-control" value="<?=set_value('companyname', isset($companyname)?$companyname:" "); ?>" id="companyname">
                                            <?php echo form_error('companyname'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label" id="namelabel">Full Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" value="<?=set_value('name', isset($name)?$name:" "); ?>" id="name">
                                            <?php echo form_error('name'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Email</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control" value="<?=set_value('email', isset($email)?$email:" "); ?>" id="email">
                                            <?php echo form_error('email'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Mobile</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group personal_phonebox">
                                            <input type="tel" name="phonecode" class="form-control" value="<?=set_value('mobile', isset($mobile)?$mobile:""); ?>" id="phonecode" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" placeholder="Phone number" required>
                                            <?php echo form_error('phonecode'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Password</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control" value="<?=set_value('password', isset($password)?$password:""); ?>" id="password">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Confirm Password</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="password" name="confirm_password" class="form-control" value="<?=set_value('confirm_password', isset($confirm_password)?$confirm_password:""); ?>" id="confirm_password">
                                            <?php echo form_error('confirm_password'); ?>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-success" type="submit">Save </button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>