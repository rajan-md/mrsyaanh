<?php $controller = $this->uri->segment(2); ?>
<div class="page-content">
    <div class="content container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-title">Privilege </h2>
                <?php getNotificationHtml(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form method="post">
                    <div class="widget">
                        <div class="widget-header"> <i class="icon-table"></i>
                            <h3>Privilege Management</h3>
                            <input style="margin:14px 20px;" class="btn btn-s-md btn-success pull-right" type="submit" value="Save Privilege">
                        </div>
                        <div class="widget-content">
                            <div class="body">
                                <table class="privilege table table-striped table-images" id="example">
                                    <tbody>
                                        <tr>
                                            <th>Role</th>
                                            <th>Page</th>
                                            <th>Blog</th>
                                            <th>Users</th>
                                            <th>Vehicle</th>
                                            <th>Vehicle Request</th>
                                            <th>Feedback</th>
                                            <th>Testimonial</th>
                                        </tr>
                                        <?php
                                        $i=1;
                                        if(!empty($roleeData))
                                        {
                                          foreach ($roleeData as $data)
                                          {
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="todo-check" style="display: inline">
                                                        <input type="checkbox" checked name="role[]" value="<?=$data->id?>" id="todo-check10 <?=$i?>">
                                                        <label for="todo-check10<?=$i?>"></label>
                                                    </div>
                                                    <?=$data->name?>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="page[]" value="<?=$data->page?>">
                                                        <input type="checkbox" <?php if($data->page==1) { ?> checked <?php } ?> value="1" id="todo-check0 <?=$i?>">
                                                        <label for="todo-check0<?=$i?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="blog[]" value="<?=$data->blog?>">
                                                        <input type="checkbox" <?php if($data->blog==1) { ?> checked <?php } ?> value="1" id="todo-check1 <?=$i?>">
                                                        <label for="todo-check1<?=$i?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="user[]" value="<?=$data->users?>">
                                                        <input type="checkbox" <?php if($data->users==1) { ?> checked <?php } ?> value="1" id="todo-check2 <?=$i?>">
                                                        <label for="todo-check2<?=$i?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="vehicle[]" value="<?=$data->vehicle?>">
                                                        <input type="checkbox" <?php if($data->vehicle==1) { ?> checked <?php } ?> value="1" id="todo-check3 <?=$i?>">
                                                        <label for="todo-check3<?=$i?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="vehicle_request[]" value="<?=$data->vehicle_request?>">
                                                        <input type="checkbox" <?php if($data->vehicle_request==1) { ?> checked <?php } ?> value="1" id="todo-check4 <?=$i?>">
                                                        <label for="todo-check4<?=$i?>"></label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="feedback[]" value="<?=$data->feedback?>">
                                                        <input type="checkbox" <?php if($data->feedback==1) { ?> checked <?php } ?> value="1" id="todo-check5 <?=$i?>" onclick="this.value">
                                                        <label for="todo-check5<?=$i?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="todo-check">
                                                        <input type="hidden" name="testimonial[]" value="<?=$data->testimonial?>">
                                                        <input type="checkbox" <?php if($data->testimonial==1) { ?> checked <?php } ?> value="1" id="todo-check6 <?=$i?>" onclick="this.value">
                                                        <label for="todo-check6<?=$i?>"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                          }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <form method="post" action="<?=base_url('admin/users/attribute')?>">
                    <div class="widget">
                        <div class="widget-header"> <i class="icon-table"></i>
                            <h3>Privilege Attribute Management</h3>
                            <input style="margin:14px 20px;" class="btn btn-s-md btn-success pull-right" type="submit" value="Save Attribute">
                        </div>
                        <div class="widget-content">
                            <div class="body">
                              <table class="privilege table table-striped table-images" id="example">
                                <tbody>
                                  <tr>
                                      <th>Attribute</th>
                                      <th>Add</th>
                                      <th>Edit</th>
                                      <th>Delete</th>
                                  </tr>
                                  <tr>
                                      <td>
                                        Page <input type="hidden" name="action[]" value="page">
                                      </td>
                                      <td>
                                          <?php $pageadd=getActionAttribute('page','add'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="add[]" value="<?=$pageadd?>">
                                              <input type="checkbox" <?php if($pageadd==1) { ?> checked <?php } ?> value="1" id="todo-addpage">
                                              <label for="todo-addpage"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $pageedit=getActionAttribute('page','edit'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="edit[]" value="<?=$pageedit?>">
                                              <input type="checkbox" <?php if($pageedit==1) { ?> checked <?php } ?> value="1" id="todo-editpage">
                                              <label for="todo-editpage"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $pagedelete=getActionAttribute('page','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$pagedelete?>">
                                              <input type="checkbox" <?php if($pagedelete==1) { ?> checked <?php } ?> value="1" id="todo-deletepage">
                                              <label for="todo-deletepage"></label>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Blog <input type="hidden" name="action[]" value="blog">
                                    </td>
                                    <td>
                                        <?php $blogadd=getActionAttribute('blog','add'); ?>
                                        <div class="todo-check">
                                            <input type="hidden" name="add[]" value="<?=$blogadd?>">
                                            <input type="checkbox" <?php if($blogadd==1) { ?> checked <?php } ?> value="1" id="todo-addblog">
                                            <label for="todo-addblog"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <?php $blogedit=getActionAttribute('blog','edit'); ?>
                                        <div class="todo-check">
                                            <input type="hidden" name="edit[]" value="<?=$blogedit?>">
                                            <input type="checkbox" <?php if($blogedit==1) { ?> checked <?php } ?> value="1" id="todo-editblog">
                                            <label for="todo-editblog"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <?php $blogdelete=getActionAttribute('blog','delete'); ?>
                                        <div class="todo-check">
                                            <input type="hidden" name="delete[]" value="<?=$blogdelete?>">
                                            <input type="checkbox" <?php if($blogdelete==1) { ?> checked <?php } ?> value="1" id="todo-deleteblog">
                                            <label for="todo-deleteblog"></label>
                                        </div>
                                    </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Users <input type="hidden" name="action[]" value="users">
                                      </td>
                                      <td>
                                          <?php $usersadd=getActionAttribute('users','add'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="add[]" value="<?=$usersadd?>">
                                              <input type="checkbox" <?php if($usersadd==1) { ?> checked <?php } ?> value="1" id="todo-addusers">
                                              <label for="todo-addusers"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $usersedit=getActionAttribute('users','edit'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="edit[]" value="<?=$usersedit?>">
                                              <input type="checkbox" <?php if($usersedit==1) { ?> checked <?php } ?> value="1" id="todo-editusers">
                                              <label for="todo-editusers"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $usersdelete=getActionAttribute('users','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$usersdelete?>">
                                              <input type="checkbox" <?php if($usersdelete==1) { ?> checked <?php } ?> value="1" id="todo-deleteusers">
                                              <label for="todo-deleteusers"></label>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Vehicle <input type="hidden" name="action[]" value="vehicle">
                                      </td>
                                      <td>
                                        <?php $vehicleadd=getActionAttribute('vehicle','add'); ?>
                                        <div class="todo-check">
                                          <input type="hidden" name="add[]" value="<?=$vehicleadd?>">
                                          <input type="checkbox" <?php if($vehicleadd==1) { ?> checked <?php } ?> value="1" id="todo-addvehicle">
                                          <label for="todo-addvehicle"></label>
                                        </div>
                                      </td>
                                      <td>
                                          <?php $vehicleedit=getActionAttribute('vehicle','edit'); ?>
                                          <div class="todo-check">
                                            <input type="hidden" name="edit[]" value="<?=$vehicleedit?>">
                                            <input type="checkbox" <?php if($vehicleedit==1) { ?> checked <?php } ?> value="1" id="todo-editvehicle">
                                            <label for="todo-editvehicle"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $vehicledelete=getActionAttribute('vehicle','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$vehicledelete?>">
                                              <input type="checkbox" <?php if($vehicledelete==1) { ?> checked <?php } ?> value="1" id="todo-deletevehicle">
                                              <label for="todo-deletevehicle"></label>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Vehicle Request <input type="hidden" name="action[]" value="vehicle_request">
                                      </td>
                                      <td>
                                          <?php $vehicle_requestadd=getActionAttribute('vehicle_request','add'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="add[]" value="<?=$vehicle_requestadd?>">
                                              <input type="checkbox" <?php if($vehicle_requestadd==1) { ?> checked <?php } ?> value="1" id="todo-addvehicle_request">
                                              <label for="todo-addvehicle_request"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $vehicle_requestedit=getActionAttribute('vehicle_request','edit'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="edit[]" value="<?=$vehicle_requestedit?>">
                                              <input type="checkbox" <?php if($vehicle_requestedit==1) { ?> checked <?php } ?> value="1" id="todo-editvehicle_request">
                                              <label for="todo-editvehicle_request"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $vehicle_requestdelete=getActionAttribute('vehicle_request','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$vehicle_requestdelete?>">
                                              <input type="checkbox" <?php if($vehicle_requestdelete==1) { ?> checked <?php } ?> value="1" id="todo-deletevehicle_request">
                                              <label for="todo-deletevehicle_request"></label>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Feedback <input type="hidden" name="action[]" value="feedback">
                                      </td>
                                      <td>
                                          <?php $feedbackadd=getActionAttribute('feedback','add'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="add[]" value="<?=$feedbackadd?>">
                                              <input type="checkbox" <?php if($feedbackadd==1) { ?> checked <?php } ?> value="1" id="todo-addfeedback">
                                              <label for="todo-addfeedback"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $feedbackedit=getActionAttribute('feedback','edit'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="edit[]" value="<?=$feedbackedit?>">
                                              <input type="checkbox" <?php if($feedbackedit==1) { ?> checked <?php } ?> value="1" id="todo-editfeedback">
                                              <label for="todo-editfeedback"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $feedbackdelete=getActionAttribute('feedback','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$feedbackdelete?>">
                                              <input type="checkbox" <?php if($feedbackdelete==1) { ?> checked <?php } ?> value="1" id="todo-deletefeedback">
                                              <label for="todo-deletefeedback"></label>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Testimonial <input type="hidden" name="action[]" value="testimonial">
                                      </td>
                                      <td>
                                          <?php $testimonialadd=getActionAttribute('testimonial','add'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="add[]" value="<?=$testimonialadd?>">
                                              <input type="checkbox" <?php if($testimonialadd==1) { ?> checked <?php } ?> value="1" id="todo-addtestimonial">
                                              <label for="todo-addtestimonial"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $testimonialedit=getActionAttribute('testimonial','edit'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="edit[]" value="<?=$testimonialedit?>">
                                              <input type="checkbox" <?php if($testimonialedit==1) { ?> checked <?php } ?> value="1" id="todo-edittestimonial">
                                              <label for="todo-edittestimonial"></label>
                                          </div>
                                      </td>
                                      <td>
                                          <?php $testimonialdelete=getActionAttribute('testimonial','delete'); ?>
                                          <div class="todo-check">
                                              <input type="hidden" name="delete[]" value="<?=$testimonialdelete?>">
                                              <input type="checkbox" <?php if($testimonialdelete==1) { ?> checked <?php } ?> value="1" id="todo-deletetestimonial">
                                              <label for="todo-deletetestimonial"></label>
                                          </div>
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>