<?php $controller = $this->uri->segment(2); ?>
<div class="page-content">
  <div class="content container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="page-title">Users <?=AddButton('add')?></h2> 
        <?php getNotificationHtml(); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="widget">
          <div class="widget-header"> <i class="icon-table"></i>
            <h3>Users Management</h3>
          </div>
          <div class="widget-content">
            <div class="body">
              <table class="table table-striped table-images" id="example">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Role</th>
                    <th width="140">Account Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($records))
                    {
                      foreach ($records as $rec)
                      {
                        ?>
                        <tr>
                          <td><?=getCustomerName($rec->id)?></td>
                          <td class="hidden-xs"><?=$rec->email?></td>
                          <td class="hidden-xs"><?=$rec->mobile?></td>
                          <td class="hidden-xs"><?=getRoleName($rec->role)?></td>
                          <td><?=statusButton($controller,'status',$rec->status,$rec->id); ?></td>
                          <td><?=AllowedAction($controller, 'edit', 'delete', 'Sure you want to delete this record ?', $rec->id) ?></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="6"> </th>
                  </tr>
                </tfoot>
              </table>                
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
