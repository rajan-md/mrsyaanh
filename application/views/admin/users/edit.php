<div class="page-content pad-top-zero">
  <div class="content container">
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCI9SJJzJtkW9SaSV86YJUePC-e49HiNMA"></script>
    <?php 
    //pre($userData);
    if(!empty($userData))
    {
      extract($userData);
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-align-left"></i>
                    <h3>Edit Users </h3>
                </div>
                <div class="widget-content">
                    <form method="post" class="form-horizontal" enctype="multipart/form-data">
                        <input type="hidden" id="mobbox" name="mobbox">
                        <input type="hidden" name="username" class="form-control" value="<?=set_value('username', isset($username)?$username:" "); ?>" id="username">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-2">
                                <div class="text-align-center">
                                    <div style="height:10px"></div>
                                    <?php if(!empty($featured_img)) { ?>
                                        <img style="height:135px;width:135px" alt="64x64" src="<?=base_url($featured_img)?>" class="img-circle">
                                    <?php } else { ?>
                                        <img style="height:135px;width:135px" alt="64x64" src="<?=base_url('skin/admin/')?>images/profile.jpg" class="img-circle">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-md-8">
                              <h3 class="no-margin"><?=isset($name)?$name:""; ?></h3>
                              <address>
                                <abbr title="Work email">e-mail:</abbr> <a href="mailto:#"><?=isset($email)?$email:""; ?></a><br>
                                <abbr title="Work Phone">phone:</abbr> <?=isset($mobile)?$mobile:""; ?><br>
                                <div style="height:5px"></div>
                                <abbr title="Profile Picture">
                                  <span class="btn btn-success fileinput-button">
                                    <i class="icon-plus"></i>
                                    <span>Add Profile Picture...</span>
                                    <input type="file" name="featured_img" id="featured_img">
                                  </span>
                                </abbr>
                              </address>
                            </div>
                        </div>
                        <fieldset>
                            <legend class="section">Personal Info</legend>
                            <div class="clearfix"></div>
                             <div class="control-group">
                                <div class="col-md-3">
                                    <label for="disabled-input" class="control-label ">Role</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?php echo form_dropdown('role', $roleOptions,set_value('role',isset($role)?$role:""),'id="role" class="form-control"'); ?>
                                        <?php echo form_error('role'); ?>
                                    </div>
                                </div>
                            </div>

                            
                            <?php
                            if(!empty($role) && $role==5)
                            {
                                ?>
                                <div class="clearfix"></div>
                                <div class="control-group">
                                    <div class="col-md-3">
                                        <label for="normal-field" class="control-label">Company Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" name="companyname" class="form-control" value="<?=set_value('companyname', isset($companyname)?$companyname:" "); ?>" id="companyname">
                                            <?php echo form_error('companyname'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="normal-field" class="control-label"><?=(!empty($role) && $role==5)?'Contact Name':'Full Name'; ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" value="<?=set_value('name', isset($name)?$name:" "); ?>" id="name">
                                        <?php echo form_error('name'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="normal-field" class="control-label">Email</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" value="<?=set_value('email', isset($email)?$email:" "); ?>" id="email">
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="normal-field" class="control-label">Mobile</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group personal_phonebox">
                                        <input type="text" name="mobile" class="form-control" value="<?=set_value('mobile', isset($mobile)?$mobile:" "); ?>" id="phonecode">
                                        <?php echo form_error('mobbox'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="disabled-input" class="control-label ">Country</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($country)?$country:""),'id="country" class="form-control"'); ?>
                                            <?php echo form_error('country'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="disabled-input" class="control-label ">State</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?php
                                        $stateOptions = !empty($country)?getStatesOptions($country):array(0=>'---Choose State/Province---');
                                        echo form_dropdown('state',$stateOptions,set_value('state',isset($state)?$state:""),'id="administrative_area_level_1" class="form-control" required=""');
                                        ?>
                                        <?php echo form_error('state'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="disabled-input" class="control-label ">Address</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="site_location" id="site_location" class="form-control" value="<?=set_value('site_location', isset($site_location)?$site_location:" "); ?>">
                                        <?php echo form_error('site_location'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="normal-field" class="control-label">City</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="city" class="form-control" value="<?=set_value('city', isset($city)?$city:" "); ?>" id="locality">
                                        <?php echo form_error('city'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="control-group">
                                <div class="col-md-3">
                                    <label for="normal-field" class="control-label">Zip Code</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="postal_code" class="form-control" value="<?=set_value('postal_code', isset($postal_code)?$postal_code:" "); ?>" id="postal_code">
                                        <?php echo form_error('postal_code'); ?>
                                    </div>
                                </div>
                            </div>

                            <!--div id="address">
                                <input type='hidden' class="field" id="street_number" disabled="true" />
                                <input type='hidden' class="field" id="route" disabled="true" />
                                <input type='hidden' class="field" id="latitude" name="site_latitude" value="<?=isset($latitude)?$latitude:" "; ?>" />
                                <input type='hidden' class="field" id="longitude" name="site_longitude" value="<?=isset($longitude)?$longitude:" "; ?>"/>
                            </div-->

                            <div class="form-group lable-padd">
                                <label class="col-sm-3"></label>
                                <div class="col-sm-6 left-align">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="pass" <?php if(!empty($on)) { echo 'checked'; } ?>
                                            <?php if(!empty($on)) {  echo 'value="on"'; } ?> id="setpassword"> Update Password </label>
                                    </div>
                                </div>
                            </div>

                            <div id="resetpassword" style="display:<?php if(empty($on)) {  echo 'none'; } ?>">

                                <div class="control-group">
                                    <div class="col-md-3">
                                        <label for="normal-field" class="control-label">Password</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control" id="password">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-3">
                                        <label for="normal-field" class="control-label">Confirm Password</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="password" name="confirm_password" class="form-control" id="confirm_password">
                                            <?php echo form_error('confirm_password'); ?>
                                        </div>
                                    </div>
                                </div>

                                </div>
                        
                        </fieldset>

                        <div class="form-actions">
                            <div>
                                <button class="btn btn-success" type="submit">Save </button>
                                <button class="btn btn-default" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        $("#site_location").on('focus', function() {
            geolocate();
        });

        jQuery("input[name=driving_license]").click(function() {
            var license = jQuery(this).val();
            if (license == 'yes') {
                jQuery("#licenseinfo").css('display', 'block');
            } else {
                jQuery("#licenseinfo").css('display', 'none');
            }
        });

        jQuery("input[name=company_advertise]").click(function() {
            var license = jQuery(this).val();
            if (license == 'yes') {
                jQuery("#advertiseinfo").css('display', 'block');
            } else {
                jQuery("#advertiseinfo").css('display', 'none');
            }
        });

        $(document).on('change', '#driving_license_types', function(e) {
            obj = this;
            var id = $(obj).val();
            $.ajax({
                type: 'post',
                url: '<?=base_url('
                login / ajax_driving_licence ')?>',
                data: {
                    id: id
                },
                success: function(data) {
                    $('#license_type').html(data);
                }
            });

        });

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initialize() {
            // Create the autocomplete object, restricting the search
            // to geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */
                (document.getElementById('site_location')), {
                    types: ['geocode']
                });
            // When the user selects an address from the dropdown,
            // populate the address fields in the form.
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                fillInAddress();
            });
        }

        // [START region_fillform]
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            //console.log(place);

            document.getElementById("latitude").value = place.geometry.location.lat();
            document.getElementById("longitude").value = place.geometry.location.lng();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        // [END region_fillform]

        // [START region_geolocation]
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);

                    var latitude = position.coords.latitude;
                    var longitude = position.coords.longitude;
                    document.getElementById("latitude").value = latitude;
                    document.getElementById("longitude").value = longitude;

                    autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
                });
            }

        }

        initialize();
        // [END region_geolocation]
    </script>
  </div>
</div>