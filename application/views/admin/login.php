
<!DOCTYPE html>
<html>
<head>
<title>Admin Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="<?=base_url('skin/admin/images/favicon.png')?>">
<link href="<?=base_url('skin/admin/css/bootstrap.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/thin-admin.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/font-awesome.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/style/style.css')?>" rel="stylesheet">



</head>
<body>
<div style="height:80px;"></div>
<div class="login-logo">
	<img src="<?=base_url('skin/admin/images/logo.png')?>" > 
</div>

<div class="widget">
<div class="login-content">
  <?php $error= $this->session->flashdata('error');
  if(!empty($error)) { ?>
  <div style="text-align:center;color: red"><?=$error?></div>
  <?php } ?>
  <div class="widget-content" style="padding-bottom:0;">
  <form method="post" class="no-margin">
                <!-- <h3 class="form-title">Login to your account</h3> -->
                
                <fieldset>
                    <div class="form-group no-margin">
                        <label for="email">Username</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="icon-user"></i>
                                </span>
                            <input type="text" name="useremail" class="form-control input-lg" id="email">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="icon-lock"></i>
                                </span>
                            <input type="password" name="userpass" class="form-control input-lg" id="password">
                        </div>

                    </div>
                </fieldset>
               <div class="form-actions">
				<label class="checkbox">
				<!-- <div class="checker"><span><input type="checkbox" value="1" name="remember"></span></div> Remember me -->
				</label>
				<button class="btn btn-success pull-right" type="submit">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button> 
                <!-- <div class="forgot"><a href="#" class="forgot">Forgot Username or Password?</a></div>   -->         
			</div>
            
            
            </form>
  
  
  </div>
   </div>
</div>

</body>
</html>
