<div class="page-content pad-top-zero">
    <div class="content container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
      <?php 
      
      if(!empty($categoryData))
      {
        extract($categoryData);
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Edit Category </h3>
            </div>
            <div class="widget-content">
               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  

                  <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                    $i=0;
                    if(!empty($language)) {
                     foreach ($language as $lang) {
                     $lid=$lang->id;
                     if($lid==@$category_content[$i]['language_id']) {
                     $name=$category_content[$i]['name'];
                     $description=$category_content[$i]['description']; 
                     $i++;

                     }
                     else {
                     $name='';
                     $description='';                   
                     }                 
                     ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                    <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                    <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Name</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="name[]" class="form-control" value="<?=set_value('name', isset($name)?$name:""); ?>" >
                       <?php echo form_error('name'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      
                        <textarea class="form-control ckeditor"  id="text" name="description[]" rows="6"><?=set_value('description', isset($description)?$description:""); ?></textarea>
                         <?php echo form_error('description'); ?>
                    </div>
                    </div>
                  </div>

                    </div>
                     <?php  } } ?>
                    
                    
                    
                  </div>
                </div>
              </div>

                  
                  
                  

                  <div class="control-group">
                  <div class="col-md-2">
                    
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                     <span class="btn btn-info fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Pic...</span>
                                        <input type="file" name="featured_img" id="featured_img">
                                    </span>
                      
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="hint-field" class="control-label"> Meta Title </label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_title" rows="3"><?=set_value('meta_title', isset($meta_title)?$meta_title:""); ?></textarea>
                       
                    </div>
                     </div>
                  </div>
                  
                  
                  

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Meta Key</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_key" rows="3"><?=set_value('meta_key', isset($meta_key)?$meta_key:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="disabled-input" class="control-label ">Meta Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control" name="meta_description" rows="3"><?=set_value('meta_description', isset($meta_description)?$meta_description:""); ?></textarea>
                       
                    </div>
                    </div>
                  </div>
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
