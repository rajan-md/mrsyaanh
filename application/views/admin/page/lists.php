<?php $controller = $this->uri->segment(2); ?>
<div class="page-content">
    <div class="content container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-title">Page <?=AddButton('add')?></h2>
                <?php getNotificationHtml(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-table"></i>
                        <h3>Page Management</h3>
                    </div>
                    <div class="widget-content">
                        <div class="body">
                            <table class="table table-striped table-images" id="example">
                                <thead>
                                    <tr>
                                        <th width="50">#</th>
                                        <th>Name</th>
                                        <th width="200">Created</th>
                                        <th width="80">Status</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($records))
                                    {
                                        foreach ($records as $rec)
                                        {
                                          ?>
                                          <tr>
                                            <td><?=$rec->id;?></td>
                                            <td><a href="<?=base_url($rec->slug)?>" target="_blank"><?=$rec->name?></a></td>
                                            <td class="hidden-xs"><?=date('d M, Y h:i A',strtotime($rec->created)); ?></td>
                                           <td><?=statusButton($controller,'status',$rec->status,$rec->id); ?></td>
                                           <td><?=AllowedAction($controller, 'edit', 'delete', 'Sure you want to delete this record ?', $rec->id) ?></td>
                                          </tr>
                                          <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>