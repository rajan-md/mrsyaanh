<div class="page-content pad-top-zero">
    <div class="content container">      
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-align-left"></i>
                        <h3>Add New Page </h3>
                    </div>

                    <div class="widget-content">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <fieldset>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" value="<?=set_value('name'); ?>" id="normal-field">
                                            <?php echo form_error('name'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Description</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control ckeditor" id="text" name="description" rows="6"><?=set_value('description'); ?></textarea>
                                            <?php echo form_error('description'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-9" id="blah_container">
                                      <img class="imgbox" id="blah" src="<?=base_url(!empty($featured_img)?$featured_img:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="hint-field" class="control-label"> Feature Images: </label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input name="featured_img" type="file" class="form-control pull-right" id="featured_img" onchange="checkPhoto(this)">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="hint-field" class="control-label"> Meta Title </label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="meta_title" class="form-control" value="<?=set_value('meta_title', isset($meta_title)?$meta_title:""); ?>" id="meta_title">
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="tooltip-enabled" class="control-label ">Meta Key</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_key" rows="3"><?=set_value('meta_key', isset($meta_key)?$meta_key:""); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label">Meta Description</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_description" rows="3"><?=set_value('meta_description', isset($meta_description)?$meta_description:""); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-success" type="submit">Save </button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>