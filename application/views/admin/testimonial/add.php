<div class="page-content pad-top-zero">
    <div class="content container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
      </div> -->
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add New Testimonial </h3>
            </div>

            <div class="widget-content">
               <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                  
                   <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">
                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                    if(!empty($language)) {
                     foreach ($language as $lang) { ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                    <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                    <div class="control-group">
                     <div class="col-md-2">
                    <label for="normal-field" class="control-label">Name</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" name="name[]" class="form-control" value="<?=set_value('name[0]'); ?>" id="normal-field">
                       <?php echo form_error('name[0]'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Description</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      
                        <textarea class="form-control ckeditor"  id="text" name="description[]" rows="6"><?=set_value('description[0]'); ?></textarea>
                         <?php echo form_error('description[0]'); ?>
                    </div>
                    </div>
                  </div>

                    </div>
                     <?php  } } ?>
                    
                    
                    
                  </div>
                </div>
              </div>

                  

                  <div class="control-group">
                  <div class="col-md-2">
                    
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                     <span class="btn btn-info fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Pic...</span>
                                        <input type="file" name="featured_img" id="featured_img">
                                    </span>
                      
                    </div>
                    </div>
                  </div>
                  
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
