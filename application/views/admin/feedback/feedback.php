
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Comment and Ratings <!-- <a class="btn btn-s-md btn-info" href="<?=base_url('admin/blog/add')?>">Add New</a> --></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Comment and Ratings Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      
                      <th>Vehicle Name</th>
                      <th>Comment</th>
                      <!-- <th width="240">Rating (avg)</th> -->
                      <th>Username</th>
                      <th width="100">Created</th>
                      <th width="40">Status</th>
                      <th width="40">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($feedbackData)) {
                      foreach ($feedbackData as $data) { ?>
                    <tr>
                      <td><?=getVehicleName($data->vehicle_id)?></td>                     
                      <td class="hidden-xs"><?=strip_tags(substr($data->comment,0,40)).'...'?></td>
                      <td><?=getUserName($data->user_id)?></td>                                    
                      <!-- <td style="font-size: 5px;">
                        <input id="input-21b" value="<?=getRating($data->vehicle_id)?>" type="text" class="rating" data-min=0 data-max=6 data-step=0.2 required title="">
                      </td> --> 
                      <td class="hidden-xs"><?=$data->created?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                       <a href="<?=base_url('admin/feedback/status/'.$data->id) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                      <td class="hidden-xs">
                      <?php echo AllowedAction('admin/feedback', 'view', 'delete', 'Sure you want to delete this feedback', $data->id) ?>
                      
                      </td>
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
