
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Feedback and Ratings <!-- <a class="btn btn-s-md btn-info" href="<?=base_url('admin/blog/add')?>">Add New</a> --></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } 
          //print_r($editfeedbackData);

          ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Feedback and Ratings Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    
                  </thead>
                  <tbody>

                    <?php if(!empty($editfeedbackData)) { ?>
                    <tr>
                    <td>Vehicle Name</td>
                     </tr> 
                    <tr>
                      <td><?=getVehicleName($editfeedbackData['vehicle_id'])?></td>  
                    </tr> 
                    <tr>
                    <td>Comment By</td> 
                     </tr> 
                    <tr>                 
                      <td><?=getUserName($editfeedbackData['user_id'])?></td>
                       </tr> 
                    <tr> 
                    <td>Comment</td>   
                     </tr> 
                    <tr>                                
                      <td class="hidden-xs"><?=$editfeedbackData['comment']?></td>
                       </tr> 
                    <tr>
                    <td>Average Rating</td>
                     </tr> 
                    <tr>
                      <td style="font-size: 5px;">
                        <input id="input-21b" value="<?=getRating($editfeedbackData['vehicle_id'])?>" type="text" class="rating" data-min=0 data-max=5 data-step=0.2         required title="">
                      </td> 
                       </tr> 
                    <tr>
                    <td>Date</td>
                     </tr> 
                    <tr>
                      <td class="hidden-xs"><?=$editfeedbackData['created']?></td>
                       </tr> 
                    <tr>
                    <td>Status</td>
                     </tr> 
                    <tr>
                      <td> 
                      <?php $status=$editfeedbackData['status']; ?>
                       <a href="<?=base_url('admin/feedback/status/'.$editfeedbackData['id']) ?>"><span class="label label-<?php if($status==1) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                        </tr> 
                    

                    <?php  } ?>
                                        
                  </tbody>
                  <tfoot>
        <tr>
          <th> </th>
        
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      <style type="text/css">
        .rating-container .caption { display: inline!important; }
        .rating-md {  font-size: 25px!important;}
      </style>


    </div>
  </div>
