<div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Dashboard </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?=base_url('admin/vehicle_request')?>" class="stats-container">
              <div class="stats-heading">Services</div>
              <div class="stats-body-alt"> 
               
                <div class="text-center"><?=isset($servicesCount)?$servicesCount:""?></div>
                <small>Total Services</small> </div>
              <div class="stats-footer">More Details</div>
              </a> </div>
            <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?=base_url('admin/blog')?>" class="stats-container">
              <div class="stats-heading">Blog Management</div>
              <div class="stats-body-alt"> 
               
                <div class="text-center"><?=isset($blogCount)?$blogCount:""?></div>
                <small>Total Blog</small> </div>
              <div class="stats-footer">More Details</div>
              </a> </div>
            <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?=base_url('admin/users')?>" class="stats-container">
              <div class="stats-heading">User Management</div>
              <div class="stats-body-alt"> 
               
                <div class="text-center"><span class="text-top"></span><?=isset($userCount)?$userCount:""?></div>
                <small>Total User Registered</small> </div>
              <div class="stats-footer">More Details</div>
              </a> </div>
            <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?=base_url('admin/page')?>" class="stats-container">
              <div class="stats-heading">Page Management</div>
              <div class="stats-body-alt"> 
             
                <div class="text-center"><span class="text-top"></span><?=isset($pageCount)?$pageCount:""?> </div>
                <small>Total Page</small> </div>
              <div class="stats-footer">More Details</div>
              </a> </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Important Link</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
              <a class="shortcut" href="<?=base_url()?>"><i class="shortcut-icon icon-home"></i><span class="shortcut-label">Website</span> </a>
              <a class="shortcut" href="<?=base_url('admin/settings/advance')?>"><i class="shortcut-icon icon-cogs"></i><span class="shortcut-label">Settings</span> </a>
              <a class="shortcut" href="<?=base_url('admin/settings/smtp')?>"><i class="shortcut-icon icon-wrench"></i> <span class="shortcut-label">SMTP</span> </a>
              <a class="shortcut" href="<?=base_url('admin/services')?>"> <i class="shortcut-icon icon-truck"></i><span class="shortcut-label">Services</span> </a>
              <a class="shortcut" href="<?=base_url('admin/users')?>"><i class="shortcut-icon icon-user"></i><span class="shortcut-label">Users</span> </a>
              <a class="shortcut" href="<?=base_url('admin/page')?>"><i class="shortcut-icon icon-list"></i><span class="shortcut-label">Page</span> </a>
              <a class="shortcut" href="<?=base_url('admin/blog')?>"><i class="shortcut-icon icon-tasks"></i> <span class="shortcut-label">Blog</span> </a>
              <a class="shortcut" href="<?=base_url('admin/language')?>"> <i class="shortcut-icon  icon-align-left"></i><span class="shortcut-label">Language</span> </a> </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
       <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>User Account</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="table-responsive" style="overflow-x:auto">
                <table class="table">
                  <thead>
                    <tr>
                
                      <th width="22%">Name</th>
                      <th width="50%">Email Address</th>
                      <th width="22%">Status</th>
                    </tr>
                  </thead>
              
                    <tbody class="selects">
                  <?php if(!empty($userData)){  
                  foreach ($userData as $userInfo) { ?>  
                  <tr>
                   
                        <td><?=$userInfo->username?></td>
                         <td><?=$userInfo->email?></td>
                        <td> <?php $status=$userInfo->status; ?>
                      <span class="label label-<?php if($status==1) { echo 'success'; } else if($status==6) { echo 'info'; } else { echo 'warning'; } ?>"><?=getStatus($status)?></span>
                      </td>
                      </tr>            
                                  
                               
                 <?php } } ?>
                      
                      
                    </tbody>
                  
                 
                </table>
              </div>
              
              
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
      </div>
      
      
    <div class="row">
        <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-check"></i>
              <h3>Recent Blog </h3>
            </div>
            <div class="widget-content">
               <?php if(!empty($blogData)){ ?>
               <ul class="to-do-list ui-sortable" id="sortable-todo">
                <?php foreach ($blogData as $blogInfo) { ?>              
                 <li class="clearfix">                  
                  <p><?=getBlogName($blogInfo->id)?></p>                  
                </li>                
                <?php } ?>
              </ul>
             <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3> Services Request</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="table-responsive" style="overflow-x:auto">
                <table class="table">
                  <thead>
                    <tr>
                      
                      <th>Request Id</th>
                      <th>Request By</th>
                      <th>Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                 
                    <tbody class="selects">                   
                      
                      <?php if(!empty($vehicleData)){  
                  foreach ($vehicleData as $vehicleInfo) { ?>  
                  <tr>
                   
                        <td><?=$vehicleInfo->request_id?> </td>
                         <td><?=getUserName($vehicleInfo->request_by)?></td>
                         <td><?=$vehicleInfo->created?></td>
                        <td> <?php $status=$vehicleInfo->status; ?>
                      <span class="label label-<?php if($status==5) { echo 'success'; } else if($status==7)  { echo 'warning'; } else if($status==8)  { echo 'info'; } else { echo 'danger'; } ?>"><?=getStatus($status)?></span>
                      </td>
                      </tr>            
                                  
                               
                 <?php } } ?>
                    
                      
                    </tbody>
                  
                 
                </table>
              </div>
              
              
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
      </div>  
 
      
     
      
   </div>
</div>


