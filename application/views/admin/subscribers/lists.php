
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Subscribers</h2> 
          <?php getNotificationHtml(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Subscribers Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>Id</th>                      
                      <th>Email</th>
                      <th>Name</th>
                      <th>Group</th>
                      <th>ZipCode</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($records))
                    {
                      foreach ($records as $data)
                      {
                        ?>
                          <tr>
                            <td><?=$data->id; ?></td>
                            <td><?=$data->subscriber_email; ?></td>
                            <td><?=$data->subscriber_name; ?></td>
                            <td><?=(!empty($data->subscription_group) && $data->subscription_group==1)?'Services':'Blog'; ?></td>
                            <td><?=$data->zipcode; ?></td>
                            <td><label class="label <?=(!empty($data->status) && $data->status==1)?'label-success':'label-warning'; ?>"><?=(!empty($data->status) && $data->status==1)?'Subscribed':'Unsubscribed'; ?></label></td>                            
                          </tr>
                          <?php
                        }
                      }
                      ?>                                        
                  </tbody>
                  <tfoot>
            <tr>
              <th colspan="6"> </th>
            </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
