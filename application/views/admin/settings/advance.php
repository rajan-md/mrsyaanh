<div class="page-content pad-top-zero">
    <div class="content container">
      <?php
      getNotificationHtml();
      if(!empty($settingsData))
      {
        extract($settingsData);
      }
      ?>
      <div class="row">
        <form method="post" class="form-horizontal" enctype="multipart/form-data">
          <div class="col-lg-12">
            <div class="widget">

              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>General Settings</h3>
              </div>

              <div class="widget-content">
                  <fieldset>
                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Site Name</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?=set_value('sitename', isset($sitename)?$sitename:""); ?>" name="sitename" id="sitename">
                              <?php echo form_error('sitename'); ?>
                            </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Admin Email</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?=set_value('admin_email', isset($admin_email)?$admin_email:""); ?>" name="admin_email" id="admin_email">
                              <?php echo form_error('admin_email'); ?>
                            </div>
                          </div>
                      </div>

                  </fieldset>           
              </div>

              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>Logo</h3>
              </div>
              
              <div class="widget-content">
                  <fieldset>
                      <div class="control-group">
                          <div class="col-md-2">
                             <label for="hint-field" class="control-label"> Logo: </label>
                          </div>
                          <div class="col-md-9" id="blah_container">
                            <img class="imgbox" id="blah" src="<?=base_url(!empty($logo)?$logo:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                          </div>
                      </div>
                      <div class="control-group">
                          <div class="col-md-2">
                             
                          </div>
                          <div class="col-md-9">
                              <div class="form-group">
                                  <input name="logo" type="file" class="form-control pull-right" id="logo" onchange="checkPhoto(this)">
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="control-group">
                          <div class="col-md-2">
                            <label for="hint-field" class="control-label"> Footer Logo: </label>
                          </div>
                          <div class="col-md-9" id="blah2_container">
                            <img class="imgbox" id="blah2" src="<?=base_url(!empty($logofooter)?$logofooter:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                          </div>
                      </div>
                      <div class="control-group">
                          <div class="col-md-2"></div>
                          <div class="col-md-9">
                              <div class="form-group">
                                  <input name="logofooter" type="file" class="form-control pull-right" id="logofooter" onchange="checkPhoto(this,'blah2')">
                              </div>
                          </div>
                      </div>
                  </fieldset>
              </div>

              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>Currency Settings </h3>
              </div>

              <div class="widget-content">
                  <fieldset>                
                    <div class="control-group">
                    <div class="col-md-2">
                      <label for="tooltip-enabled" class="control-label ">Currency</label>
                      </div>
                      <div class="col-md-9">
                      <div class="form-group">
                        <?php 
                        echo form_dropdown('', $currencyOption,set_value('user_id',isset($currency)?$currency:""),'name="currency" id="currencytype" class="form-control"'); ?>
                      </div>
                      </div>
                    </div>
                    
                  </fieldset>           
              </div>

              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>Paypal Settings</h3>
              </div>

              <div class="widget-content">
                  <fieldset>
                      <div class="control-group">
                        <div class="col-md-2">
                            <label for="normal-field" class="control-label">Mode</label>
                        </div>
                        <div class="col-md-9">
                          <div class="form-group">
                            <input type="radio" value="1" <?php if($live_paypal==1) { echo 'checked'; } ?> name="live_paypal" id="live_paypal"> Live &nbsp;&nbsp;&nbsp;
                            <input type="radio" value="0" <?php if($live_paypal==0) { echo 'checked'; } ?> name="live_paypal" id="live_paypal"> Test 
                          </div>
                        </div>
                      </div>
                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Paypal ID</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?=set_value('paypal', isset($paypal)?$paypal:""); ?>" name="paypal" id="paypal">
                              <?php echo form_error('paypal'); ?>
                            </div>
                          </div>
                      </div>
                  </fieldset>
              </div>
              
              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>Social Settings </h3>
              </div>

              <div class="widget-content">
                <fieldset>
                    <div class="control-group">
                      <div class="col-md-2">
                          <label for="normal-field" class="control-label">Facebook</label>
                      </div>
                      <div class="col-md-9">
                          <div class="form-group">
                            <input type="text" class="form-control" value="<?=set_value('facebook', isset($facebook)?$facebook:""); ?>" name="facebook" id="facebook">
                            <?php echo form_error('facebook'); ?>
                          </div>
                      </div>
                    </div>                   
                    <div class="control-group">
                      <div class="col-md-2">
                          <label for="normal-field" class="control-label">Twitter</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" value="<?=set_value('twitter', isset($twitter)?$twitter:""); ?>" name="twitter" id="twitter">
                          <?php echo form_error('twitter'); ?>
                        </div>
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="col-md-2">
                        <label for="normal-field" class="control-label">Linkedin</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" value="<?=set_value('linkedin', isset($linkedin)?$linkedin:""); ?>" name="linkedin" id="linkedin">
                          <?php echo form_error('linkedin'); ?>
                        </div>
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="col-md-2">
                        <label for="normal-field" class="control-label">Rss</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" value="<?=set_value('rss', isset($rss)?$rss:""); ?>" name="rss" id="rss">
                          <?php echo form_error('rss'); ?>
                        </div>
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="col-md-2">
                        <label for="normal-field" class="control-label">Instagram</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" value="<?=set_value('instagram', isset($instagram)?$instagram:""); ?>" name="instagram" id="instagram">
                          <?php echo form_error('instagram'); ?>
                        </div>
                      </div>
                    </div>
                </fieldset>
              </div>


              <div class="widget-header">
                <i class="icon-align-left"></i>
                <h3>Email Settings</h3>
              </div>

              <div class="widget-content">
                  <fieldset>
                      <div class="control-group">
                          <div class="col-md-2">
                            <label for="hint-field" class="control-label"> Site Logo: </label>
                          </div>
                          <div class="col-md-9" id="blah3_container">
                            <img class="imgbox" id="blah3" src="<?=base_url(!empty($emaillogo)?$emaillogo:'skin/front/images/no_image.jpg'); ?>" alt="your image" />
                          </div>
                      </div>                      
                      <div class="control-group">
                          <div class="col-md-2"></div>
                          <div class="col-md-9">
                              <div class="form-group">
                                  <input name="emaillogo" type="file" class="form-control pull-right" id="emaillogo" onchange="checkPhoto(this,'blah3')">
                              </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Site Email</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?=set_value('site_email', isset($site_email)?$site_email:""); ?>" name="site_email" id="site_email">
                              <?php echo form_error('site_email'); ?>
                            </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Email Signature</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <textarea class="ckeditor form-control" name="email_signature" id="email_signature"><?=set_value('email_signature', isset($email_signature)?$email_signature:""); ?></textarea>
                              <?php echo form_error('email_signature'); ?>
                            </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="control-group">
                          <div class="col-md-2">
                              <label for="normal-field" class="control-label">Disclaimer</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <textarea class="ckeditor form-control" name="email_disclaimer" id="email_disclaimer"><?=set_value('email_disclaimer', isset($email_disclaimer)?$email_disclaimer:""); ?></textarea>
                              <?php echo form_error('email_disclaimer'); ?>
                            </div>
                          </div>
                      </div>

                  </fieldset>           
              </div>


              <div class="form-actions">
                <div>
                  <button class="btn btn-success" type="submit">Save </button>
                  <button class="btn btn-default" type="button">Cancel</button>
                </div>
              </div> 

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>