<div class="page-content pad-top-zero">
    <div class="content container">
      <?php if(!empty($settingsData)) {  sort($settingsData);  }
      if(!empty($homeData))  {  extract($homeData);  }  ?>
      <?php $success= $this->session->flashdata('message'); 
      if(!empty($success)) { ?>
      <div class="label-msg  btn-success">
        <?php echo $this->session->flashdata('message'); ?>
      </div>
      <?php } ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
              <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add New Content </h3>
              </div>
              <div class="widget-content">              
                <fieldset>                  
                   <div id="wizard">
                      <div id="rootwizard">
                        <div class="navbar">
                          <div class="navbar-inner">                      
                            <ul class="nav">
                            <?php 
                            if(!empty($language)) {
                              foreach ($language as $lang) { ?>                         
                              <li class="<?=$lang->default?>"><a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                            <?php  } } ?>                          
                            </ul>                      
                          </div>
                        </div>                  
                      <div class="tab-content">
                      <?php 
                       $i=0;
                       if(!empty($language)) {
                       foreach ($language as $lang) { ?> 
                       <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                        <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                        <div class="control-group">
                          <div class="col-md-2">
                            <label for="normal-field" class="control-label">Variable</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?=set_value('variable[0]'); ?>" name="variable[]" id="variable">
                              <?php echo form_error('variable[0]'); ?>
                            </div>
                          </div>
                        </div>
                        
                        <div class="control-group">
                          <div class="col-md-2">
                            <label for="normal-field" class="control-label">Description</label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                            <textarea class="form-control" name="description[]" id="description"><?=set_value('description['.$i.']'); ?></textarea>
                              
                              <?php echo form_error('description['.$i.']'); ?>
                            </div>
                          </div>
                        </div>                        
                        </div>
                      <?php $i++;  } } ?>
                  </div>
                </div>
              </div> 
                </fieldset>
                </div>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>         
             </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
