<div class="page-content pad-top-zero">
    <div class="content container">
      <?php if(!empty($settingsData)) {  sort($settingsData);  }
      if(!empty($editData))  {  extract($editData);  }  ?>
      <?php $success= $this->session->flashdata('message'); 
      if(!empty($success)) { ?>
      <div class="label-msg  btn-success">
        <?php echo $this->session->flashdata('message'); ?>
      </div>
      <?php }   ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="language_id" value="<?=$editData['language_id']?>">
            <input type="hidden" name="variable" value="<?=$editData['variable']?>">
              <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Content </h3>
              </div>
              <div class="widget-content">              
                <fieldset>                  
                        <div class="control-group">
                          <div class="col-md-2">
                            <label for="normal-field" class="control-label">Language :  </label>
                          </div>
                          <div class="col-md-7"><?=getLanguageName($editData['language_id'])?></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="control-group">
                          <div class="col-md-2">
                            <label for="normal-field" class="control-label">Variable :  </label>
                          </div>
                          <div class="col-md-7"><?=$editData['variable']?></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="control-group">
                          <div class="col-md-2">
                            <label for="normal-field" class="control-label">Description : </label>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                            <textarea class="form-control" name="description" id="description"><?=set_value('description', $editData['description']); ?></textarea>
                              
                              <?php echo form_error('description'); ?>
                            </div>
                          </div>
                        </div>                        
                        
                </fieldset>
                
                </div>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-primary" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>


      
            </div>         
             </div>
        </div>
      </div>
    
      


    </div>
  </div>
