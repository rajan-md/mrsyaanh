<div class="page-content pad-top-zero">
    <div class="content container">
      
       <?php 
      if(!empty($settingsData))
      {
        sort($settingsData);

      }

      if(!empty($homeData))
      {
        extract($homeData);
      }
   
      ?>
      <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            
            
              <form method="post" class="form-horizontal" enctype="multipart/form-data">
                
               
          

            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Features Settings </h3>
            </div>

             <div class="widget-content">
              
                <fieldset>
                  
                   <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">
                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tab<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                     $i=0;
                     if(!empty($language)) {
                     foreach ($language as $lang) {
                         $lid=$lang->id;
                         $setting=$settingsData[$i];
                         $setting->language_id;
                         if($lid==@$setting->language_id) {
                           $id=$setting->id;
                           $featurestitle=$setting->featurestitle;
                           $featuredescription=$setting->featuredescription; 
                           $i++;
                         }
                         else {
                           $featurestitle='';
                           $featuredescription='';                   
                         }                 
                     ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tab<?=$lang->id?>">
                    <input type="hidden" name="lang[]" value="<?=$lang->id?>">
                    <input type="hidden" name="homeid[]" value="<?=$id?>">
                    <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Title</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('featurestitle[]', isset($featurestitle)?$featurestitle:""); ?>" name="featurestitle[]" id="featurestitle">
                      
                    </div>
                    </div>
                  </div>

                   <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Descriptions</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control ckeditor"  name="featuredescription[]" rows="3"><?=set_value('featuredescription[]', isset($featuredescription)?$featuredescription:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>

                    </div>
                     <?php  } } ?>
                    
                    
                    
                  </div>
                </div>
              </div>
                  
                  

                   <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Video</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('featurevideo', isset($featurevideo)?$featurevideo:""); ?>" name="featurevideo" id="featurevideo">
                 
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Links</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('featurelinks', isset($featurelinks)?$featurelinks:""); ?>" name="featurelinks" id="featurelinks">
                      
                    </div>
                    </div>
                  </div>

                 
                  <div class="control-group">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                    <div class="form-group">
                     
                      <abbr title="Profile Picture"><span class="btn btn-success fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Logo...</span>
                                        <input  type="file" name="featureicons" id="featureicons">
                                    </span></abbr>
                    </div>
                    </div>
                     <div class="col-md-8">
                     <?php 
                      if(!empty($featureicons)) { ?>
                      <img  width="100" src="<?=base_url($featureicons)?>" >
                      <?php }
                       ?>
                     </div>
                  </div>
                  <div style="height: 20px;clear: both;" class="clearfix"></div>
                 
                 

                  
                  
                </fieldset>
                </div>
                 <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Footer Settings</h3>
                </div>
                <div class="widget-content">
                <fieldset>
                  

                   <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">
                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tabs<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                    $i=0;
                    if(!empty($language)) {
                     foreach ($language as $lang) { 
                         $lid=$lang->id;
                         $setting=$settingsData[$i];
                         $setting->language_id;
                         if($lid==@$setting->language_id) {
                           $bottomtile=$setting->bottomtile;
                           $bottomdescription=$setting->bottomdescription; 
                           $blogdescription=$setting->blogdescription; 
                           $i++;
                         }
                         else {
                           $bottomtile='';
                           $bottomdescription='';                   
                           $blogdescription='';                   
                         }  

                      ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tabs<?=$lang->id?>">
                    
                    <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Blog Title</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('bottomtile[]', isset($bottomtile)?$bottomtile:""); ?>" name="bottomtile[]" id="bottomtile">
                      
                    </div>
                    </div>
                  </div>

                   <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Footer Descriptions</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control ckeditor"  name="bottomdescription[]" rows="3"><?=set_value('bottomdescription[]', isset($bottomdescription)?$bottomdescription:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Blog Descriptions</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control ckeditor"  name="blogdescription[]" rows="3"><?=set_value('blogdescription[]', isset($blogdescription)?$blogdescription:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>
                </div>
                <?php  } } ?>
                 </div>
                </div>
              </div>

                  

                  <div class="control-group">
                    <div class="col-md-2">
                   
                    </div>
                    <div class="col-md-2">
                    <div class="form-group">
                     
                      <abbr title="Profile Picture"><span class="btn btn-success fileinput-button">
                                        <i class="icon-plus"></i>
                                        <span>Upload Logo...</span>
                                        <input  type="file" name="bottomicons" id="bottomicons">
                                    </span></abbr>
                    </div>
                    </div>
                     <div class="col-md-8">
                     <?php 
                      if(!empty($bottomicons)) { ?>
                      <img  width="100" src="<?=base_url($bottomicons)?>" >
                      <?php }
                       ?>
                     </div>
                  </div>
                  <div class="clearfix"></div>


                  </fieldset>
                  </div>

                    <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>App Settings </h3>
                </div>
                <div class="widget-content">
                <fieldset>


                <div id="wizard">
                <div id="rootwizard">
                  <div class="navbar">
                    <div class="navbar-inner">
                      
                        <ul class="nav">
                         <?php 
                          if(!empty($language)) {
                           foreach ($language as $lang) { ?>                         
                          <li class="<?=$lang->default?>"><a href="#tabfoo<?=$lang->id?>" data-toggle="tab"><?=$lang->name?></a></li>
                          <?php  } } ?>                          
                        </ul>
                      
                    </div>
                  </div>
                  
                  <div class="tab-content">
                    <?php 
                    $i=0;
                    if(!empty($language)) {
                     foreach ($language as $lang) { 
                         $lid=$lang->id;
                         $setting=$settingsData[$i];
                         $setting->language_id;
                         if($lid==@$setting->language_id) {
                           $sitetitle=$setting->sitetitle;
                           $footerdescription=$setting->footerdescription; 
                           $copyright=$setting->copyright; 
                           $i++;
                         }
                         else {
                           $sitetitle='';
                           $footerdescription='';                   
                           $copyright='';                   
                         }  

                      ?> 
                    <div class="tab-pane <?=$lang->default?>" id="tabfoo<?=$lang->id?>">
                    
                    <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Footer Title</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('sitetitle[]', isset($sitetitle)?$sitetitle:""); ?>" name="sitetitle[]" id="sitetitle">
                      
                    </div>
                    </div>
                  </div>

                   <div class="control-group">
                  <div class="col-md-2">
                    <label for="tooltip-enabled" class="control-label ">Footer Descriptions</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <textarea class="form-control ckeditor"  name="footerdescription[]" rows="3"><?=set_value('footerdescription[]', isset($footerdescription)?$footerdescription:""); ?></textarea>
                      
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Copyright</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('copyright[]', isset($copyright)?$copyright:""); ?>" name="copyright[]" id="copyright">
                      
                    </div>
                    </div>
                  </div>

                   

                    </div>
                     <?php  } } ?>
                    
                    
                    
                  </div>
                </div>
              </div>

                 <div class="clearfix"></div>
                  

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">Google Play</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('googleplay', isset($googleplay)?$googleplay:""); ?>" name="googleplay" id="googleplay">
                      <?php echo form_error('googleplay'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">App Store</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('appstore', isset($appstore)?$appstore:""); ?>" name="appstore" id="appstore">
                      <?php echo form_error('appstore'); ?>
                    </div>
                    </div>
                  </div>
                  
                  </fieldset>
                  </div>
                

                <div class="form-actions">
                  <div>
                    <button class="btn btn-success" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
            

          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
