<div class="page-content pad-top-zero">
    <div class="content container">
      <?php
      getNotificationHtml();
      if(!empty($smtpData))
      {
        extract($smtpData);
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>SMTP Settings </h3>
            </div> 
             <div class="widget-content">
              <form method="post" class="form-horizontal">
                <fieldset>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">From Name</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('smtp_from', isset($smtp_from)?$smtp_from:""); ?>" name="smtp_from" id="smtp_from">
                      <?php echo form_error('smtp_from'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">SMTP Username</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('smtp_user', isset($smtp_user)?$smtp_user:""); ?>" name="smtp_user" id="smtp_user">
                      <?php echo form_error('smtp_user'); ?>
                    </div>
                    </div>
                  </div>
                 
                   <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">SMTP Host</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('smtp_host', isset($smtp_host)?$smtp_host:""); ?>" name="smtp_host" id="smtp_host">
                      <?php echo form_error('smtp_host'); ?>
                    </div>
                    </div>
                  </div>                 
                  
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">SMTP Port</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('smtp_port', isset($smtp_port)?$smtp_port:""); ?>" name="smtp_port" id="smtp_port">
                      <?php echo form_error('smtp_port'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="control-group">
                  <div class="col-md-2">
                    <label for="normal-field" class="control-label">SMTP Password</label>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <input type="text" class="form-control" value="<?=set_value('smtp_pass', isset($smtp_pass)?$smtp_pass:""); ?>" name="smtp_pass" id="smtp_pass">
                      <?php echo form_error('smtp_pass'); ?>
                    </div>
                    </div>
                  </div>
                
                 
                  
                  
                  
                </fieldset>
                <div class="form-actions">
                  <div>
                    <button class="btn btn-success" type="submit">Save </button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
               </form>
            </div>

          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
