
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Content <?php //AddButton('add')?></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Content Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>Variable</th>
                      <th>Description</th>
                      <th width="100">Language</th>
                      <th width="30">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($contentData)) {
                      foreach ($contentData as $data) { ?>
                    <tr>                     
                      <td><?=$data->variable?></td>                      
                      <td><?=$data->description?></td>
                      <td><?=getLanguageName($data->language_id)?></td>                      
                      <td class="hidden-xs">
                      
                      <a href="<?=base_url('admin/settings/edit/'.$data->id) ?>" class="btn btn-sm btn-primary"> <i class="icon-large icon-pencil"></i> </a>
                      </td>
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>
          <tr>
          <th> </th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
        </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
