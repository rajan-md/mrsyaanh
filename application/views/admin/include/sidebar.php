<?php
$segment=$this->uri->segment(2);
$segment2=$this->uri->segment(3);
$role=$this->session->userdata('role');
?>
<div class="left-nav">
    <div id="side-nav">
            <ul id="nav">
                <li class="<?php if($segment=='dashboard') { echo 'current'; } ?>">
                    <a href="<?=base_url('admin/dashboard')?>"> <i class="icon-dashboard"></i> Dashboard </a>
                </li>

                <li class="<?php if($segment=='page') { echo 'current'; } ?>">
                    <a href="<?=base_url('admin/page')?>"> <i class="icon-th-list"></i> Page Management </a>
                </li>

                <li class="<?php if($segment=='blog' || $segment=='subscribers') { echo 'current'; } ?>">
                    <a href="#"> <i class="icon-th-list"></i> Blog Management </a>
                    <ul class="sub-menu">

                        <li class="<?php if($segment=='blog') { echo 'current'; } ?>">
                            <a href="<?=base_url('admin/blog')?>"> <i class="icon-angle-right"></i> Articles </i>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if($segment=='services' || $segment=='questionnaire') { echo 'current'; } ?>">
                    <a href="#"> <i class="icon-th-list"></i> Services Management <i class="arrow icon-angle-left"></i></a>
                    <ul class="sub-menu">
                        <li class="<?php if(($segment=='services') && ($segment2!='prices') && ($segment2!='service_provider_request') && ($segment2!='provider_request_details')&& ($segment2!='view_request') && ($segment2!='service_provider_settings') && ($segment2!='service_customer_request') && ($segment2!='allowed_zip_code') && ($segment2!='service_provider_records') && ($segment2!='addservicearea') && ($segment2!='servicearea') && ($segment2!='service_request_details') && ($segment2!='addprovider') && ($segment2!='videos') && ($segment2!='invoices') && ($segment2!='create-invoice')){ echo 'current'; } ?>">
                            <a href="<?=base_url('admin/services')?>"> <i class="icon-angle-right"></i> Services Category </a>
                        </li>

                        <li class="<?php if($segment=='questionnaire') { echo 'current'; } ?>">
                            <a href="<?=base_url('admin/questionnaire')?>"> <i class="icon-angle-right"></i> Service Questionnaire </a>
                        </li>

                       <!--  <li class="<?php if(($segment2=='provider_request_details') || ($segment2=='addprovider') || ($segment2=='service_provider_request') || ($segment2=='service_provider_records') || ($segment2=='service_request_details') || ($segment2=='service_provider_settings') || ($segment2=='invoices') || ($segment2=='create-invoice')) { echo 'current'; } ?>">
                            <a href="<?=base_url('admin/services/service_provider_request')?>"> <i class="icon-angle-right"></i> Service Providers </a>
                        </li>
                        <li class="<?php if(($segment=='services') && ($segment2=='prices')) { echo 'current'; } ?>"> <a href="<?=base_url('admin/services/prices')?>"> <i class="icon-angle-right"></i> Service Price  </a> </li> 
                        <li class="<?php if(($segment2=='view_request')||($segment2=='service_customer_request')) { echo 'current'; } ?>">
                            <a href="<?=base_url('admin/services/service_customer_request')?>"> <i class="icon-angle-right"></i> Service Bookings</a>
                        </li>
                        <li class="<?php if(($segment=='services' && $segment2=='allowed_zip_code')) { echo 'current'; } ?>">
                            <a href="<?=base_url('admin/services/allowed_zip_code')?>"> <i class="icon-angle-right"></i> Allow Zipcode </a>
                        </li>

                      <li class="<?php if(($segment=='services' && $segment2=='servicearea') || ($segment=='services' && $segment2=='addservicearea') || ($segment=='services' && $segment2=='editservicearea') ) { echo 'current'; } ?>">
                          <a href="<?=base_url('admin/services/servicearea')?>"> <i class="icon-angle-right"></i> Service Area </a>
                      </li>
                      <li class="<?php if(($segment=='services') && ($segment2=='import_zipcode')) { echo 'current'; } ?>">
                          <a href="<?=base_url('admin/services/import_zipcode')?>"> <i class="icon-angle-right"></i> Import Zipcode </a>
                      </li>

                    <li class="<?php if(($segment=='services' && $segment2=='videos') || ($segment=='services' && $segment2=='addvideo') || ($segment=='services' && $segment2=='editservice') ) { echo 'current'; } ?>">
                        <a href="<?=base_url('admin/services/videos')?>"> <i class="icon-angle-right"></i> Service Videos </a>
                    </li>--> 

                    </ul>
                    </li>

                    <li class="<?php if($segment=='coupons') { echo 'current'; } ?>">
                        <a href="<?=base_url('admin/coupons')?>"> <i class="icon-tags"></i> Coupons Management </a>
                    </li>

                    <li class="<?php if(($segment=='role') || ($segment=='users')) { echo 'current'; } ?>">
                        <a href="#"> <i class="icon-user"></i> Users Management <i class="arrow icon-angle-left"></i></a>
                        <ul class="sub-menu">

                            <li class="<?php if($segment=='users') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/users')?>"> <i class="icon-angle-right"></i> Users </i>
                                </a>
                            </li>

                            <li class="<?php if($segment=='role') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/role')?>"> <i class="icon-angle-right"></i> User Roles </a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?php if(($segment=='subscription-lists') || ($segment=='subscribers')) { echo 'current'; } ?>">
                        <a href="#"> <i class="icon-user"></i> Subscriptions <i class="arrow icon-angle-left"></i></a>
                        <ul class="sub-menu">

                            <li class="<?php if($segment=='subscribers') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/subscribers')?>"> <i class="icon-angle-right"></i> Subscribers </i>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?php if(($segment2=='home') ||($segment=='privilege') ||($segment=='settings') || ($segment=='language')) { echo 'current'; } ?>">
                        <a href="#"> <i class="icon-large icon-cogs"></i> Settings <i class="arrow icon-angle-left"></i></a>
                        <ul class="sub-menu">                            
                            <li class="<?php if($segment2=='advance') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/settings/advance')?>"> <i class="icon-angle-right"></i> General Settings </a>
                            </li>
                            <li class="<?php if($segment2=='smtp') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/settings/smtp')?>"> <i class="icon-angle-right"></i> SMTP Settings </a>
                            </li>
                            <!--li class="<?php if($segment2=='home') { echo 'current'; } ?>">
                                <a href="<?=base_url('admin/settings/home')?>"> <i class="icon-angle-right"></i> Home Management </a>
                            </li-->

                        </ul>
                    </li>

            </ul>
    </div>
</div>