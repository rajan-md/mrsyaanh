</div>
<div class="bottom-nav footer"> @2019 Markup Designs</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?=base_url('skin/admin/js/jquery.js')?>"></script> 

<script src="<?=base_url('skin/admin/js/bootstrap.min.js')?>"></script> 
<script type="text/javascript" src="<?=base_url('skin/admin/js/smooth-sliding-menu.js')?>"></script> 
<script class="include" type="text/javascript" src="<?=base_url('skin/admin/javascript/jquery191.min.js')?>"></script> 
<script class="include" type="text/javascript" src="<?=base_url('skin/admin/javascript/jquery.jqplot.min.js')?>"></script> 
<script src="<?=base_url('skin/admin/assets/sparkline/jquery.sparkline.js')?>" type="text/javascript"></script>
<script src="<?=base_url('skin/admin/assets/sparkline/jquery.customSelect.min.js')?>" ></script>
<script src="<?=base_url('skin/admin/assets/sparkline/sparkline-chart.js')?>"></script>
<!-- <script src="<?=base_url('skin/admin/assets/sparkline/easy-pie-chart.js')?>"></script> -->
<script src="<?=base_url('skin/admin/js/select-checkbox.js')?>"></script> 
<!-- <script src="<?=base_url('skin/admin/js/to-do-admin.js')?>"></script> --> 
<script type="text/javascript" src="<?=base_url('skin/admin/assets/plugins/ckeditor/ckeditor.js')?>"></script> 
<!--switcher html end--> 
<script src="<?=base_url('skin/admin/assets/switcher/switcher.js')?>"></script> 
<script src="<?=base_url('skin/admin/assets/switcher/moderziner.custom.js')?>"></script>
<link href="<?=base_url('skin/admin/assets/switcher/switcher.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/assets/switcher/switcher-defult.css')?>" rel="stylesheet">

<script src="<?=base_url('skin/admin/javascript/jquery.nestable.js')?>"></script> 
<script src="<?=base_url('skin/admin/build/js/intlTelInput.js')?>"></script>


<script type="text/javascript">
      $(document).on('change','#services_type',function (e) {
        obj=this;
        var id=$(obj).val();
        $('#service_category_code').val(id); 
    });
    </script>

<script>


$('input[type=checkbox]').click(function(){
    var id=$(this).val();    
    if($(this).is(':checked'))
    $(this).siblings('input[type=hidden]').val(id);
    else
    $(this).siblings('input[type=hidden]').val(0);
});

$(document).ready(function(){
    $("toggleButton").click(function(){
        $(".left-nav").toggle();
    });
});
</script>

<script>

/*$(document).ready(function()
{

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);
    
    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $('#nestable3').nestable();

});*/


$(document).on('change','#servicesa',function (e) {
    obj=this
    var id=$(obj).val();
    $.ajax({
        type: 'post',
        url:'<?=base_url('admin/services/getzipcodebysid'); ?>',
        data: {id:id},
        success: function (data) { 
          //alert(data);
           console.log('existing zipcode has retrieve successfully');
        }
      });
});

$(document).on('click','#serviceareabox',function (e) {
  
  if( $('#seleData').is(':empty') )
  {
    $.ajax({
        type: 'post',
        url:'<?=base_url('admin/services/seleczipcode'); ?>',
        success: function (data) { 
           $('#seleData').html(data);
        }
      });
  }
});

$(document).on('keyup','#serviceareabox',function (e) {
	
    obj=this;
    var id=$(obj).val();
    $('#ajaxbox').remove();
    if(id!=""){
	    $(this).after('<ul id="ajaxbox"></ul>');
	    img= '<?=base_url('skin/front/images/ajax-loading.gif'); ?>';
	    $('#ajaxbox').html('<li class="loadboxbx"><img style="width:100%; background: transparent; padding: 0;" src="'+img+'"><li>');	    
	    $.ajax({
	      type: 'post',
	      url: '<?=base_url('admin/services/findzipcode'); ?>',
	      data: {id:id},
	      success: function (data) { 
	        $('#ajaxbox').html(data);
	      }
	    });
	} else {
		$('#ajaxbox').remove();
	}
  });


$(document).click(function(){
  $("#ajaxbox").hide();
});


$(document).on('change','.zchoose', function() {
    var id=$(this).val();
    if ($(this).is(':checked')) {
	    obj=this;
	    $.ajax({
		    type: 'post',
		    url:'<?=base_url('admin/services/seleczipcode'); ?>',
		    data: {id:id},
		    success: function (data) { 
		       $('#seleData').html(data);
		    }
	    });
    } else {
    	obj=this;
	    $.ajax({
		    type: 'post',
		    url:'<?=base_url('admin/services/unsetzipcode'); ?>',
		    data: {id:id},
		    success: function (data) { 
		       $('#seleData').html(data);
		    }
	    });
    }
});

$(document).on('click','.delvihiclepic', function() {
    var id=$(this).attr('id');
    obj=this;
    $.ajax({
    type: 'post',
    url:'<?=base_url('admin/vehicle/ajax_vehicle_pic_delete'); ?>',
    data: {id:id},
    success: function (data) { 
       $(obj).closest('.picbox').remove();
    }
  });
});

$(document).on('change','#vehicle_category_id',function (e) {
    obj=this;
    var id=$(obj).val();
    $.ajax({
      type: 'post',
      url:'<?=base_url('admin/vehicle/ajax_vehicle_type')?>',
      data: {id:id},
      success: function (data) { 
        $('#vehicle_type').html(data);
        $('#vehicle_models_id').html('<option value="">Select vehicle models</option>');
      }
    });    
});

$(document).on('change','#vehicle_type',function (e) {
    obj=this;
    var id=$(obj).val();
    $.ajax({
      type: 'post',
      url:'<?=base_url('admin/vehicle/ajax_vehicle_models')?>',
      data: {id:id},
      success: function (data) { 
        $('#vehicle_models_id').html(data);
      }
    });
});



</script>

<script>
jQuery('#region').on('change', function() { 
jQuery('#sms_search').val('');  
jQuery( "#sss_autosuggestion" ).html('');
});

function SetSearchData(obj) {
var getdata=jQuery(obj);
var mobile=getdata.attr("mobile");
var title=getdata.attr("title");
jQuery("#sms_search").val(title);
jQuery("#sms_mobile").val(mobile);
jQuery( "#sss_autosuggestion" ).html('');
}

jQuery( "#sms_search" ).keyup(function() { 
id='sss_autosuggestion';
var getdata=jQuery(this);           
var setdata=jQuery('#'+id)
setdata.html('<li>Please wait...</li>');
post_txt=getdata.val();
var sval=jQuery( "#sms_search" ).val();
var slen=sval.length;
var check=setdata.length;
if((slen>2) && (slen!=0)) {
if (check==0) { getdata.after('<ul class="'+id+'" id="'+id+'"><ul>');   } 
  
jQuery.ajax({
url: '<?=base_url('admin/sms/ajax_find_user')?>',
type: 'POST',
data: {keyword:post_txt},
success: function (data) {
  setdata.html(data);
},
error: function (data) {
setdata.html('<li>Result not found</li>');
}
}) ;

}
else
{
  setdata.html('');
}
});

</script>


<script>

function SetSearchEmail(obj) {
var getdata=jQuery(obj);
var email=getdata.attr("email");
var title=getdata.attr("title");
jQuery("#subscribe_search").val(title);
jQuery("#subscribe_email").val(email);
jQuery( "#sss_autosuggestion" ).html('');
}

jQuery( "#subscribe_search" ).keyup(function() { 
id='sss_autosuggestion';
var getdata=jQuery(this);           
var setdata=jQuery('#'+id)
setdata.html('<li>Please wait...</li>');
post_txt=getdata.val();
var sval=jQuery( "#subscribe_search" ).val();
var slen=sval.length;
var check=setdata.length;
if((slen>2) && (slen!=0)) {
if (check==0) { getdata.after('<ul class="'+id+'" id="'+id+'"><ul>');   } 
  
jQuery.ajax({
url: '<?=base_url('admin/subscribe/ajax_find_subscribe')?>',
type: 'POST',
data: {keyword:post_txt},
success: function (data) {
  setdata.html(data);
},
error: function (data) {
setdata.html('<li>Result not found</li>');
}
}) ;

}
else
{
  setdata.html('');
}
});



</script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
<script type="text/javascript" language="javascript" src="<?=base_url('skin/admin/js/jquery.dataTables.js')?>"></script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable( {
      "sPaginationType": "full_numbers"
    } );

  } );
</script>

<script>
$(".datepicker").datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat: "yy-mm-dd"
});

$("#dob").datepicker({
  defaultDate: "+1w",
  yearRange: '1948:2018',
  changeMonth: true,
  changeYear: true,
  numberOfMonths: 1,
  dateFormat: "yy-mm-dd"
});

$("#expirydata" ).datepicker({
  defaultDate: "+1w",
  yearRange: '2008:2038',
  changeMonth: true,
  changeYear: true,
  numberOfMonths: 1,
  dateFormat: "yy-mm-dd"
}); 

$("#services_start_date" ).datepicker({
  defaultDate: "+1w",
  yearRange: '2008:2038',
  changeMonth: true,
  changeYear: true,
  numberOfMonths: 1,
  dateFormat: "yy-mm-dd"
}); 

$( function() {
var dateFormat = "yy-mm-dd",
from = $( "#start_date" )
.datepicker({
defaultDate: "+1w",
changeMonth: false, 
dateFormat:"yy-mm-dd",
numberOfMonths: 1
})
.on( "change", function() {
to.datepicker( "option", "minDate", getDate( this ) );
}),
to = $( "#end_date" ).datepicker({
defaultDate: "+1w",
changeMonth: false,
dateFormat:"yy-mm-dd",
numberOfMonths: 1
})
.on( "change", function() {
from.datepicker( "option", "maxDate", getDate( this ) );
});

function getDate( element ) {
var date;
try {
date = $.datepicker.parseDate( dateFormat, element.value );
} catch( error ) {
date = null;
}

return date;
}
} );
</script>


<script>
$( function() {
var dateFormat = "yy-mm-dd",
from = $( "#insurance_start_date" )
.datepicker({
defaultDate: "+1w",
changeMonth: false, 
dateFormat:"yy-mm-dd",
numberOfMonths: 1
})
.on( "change", function() {
to.datepicker( "option", "minDate", getDate( this ) );
}),
to = $( "#insurance_end_date" ).datepicker({
defaultDate: "+1w",
changeMonth: false,
dateFormat:"yy-mm-dd",
numberOfMonths: 1
})
.on( "change", function() {
from.datepicker( "option", "maxDate", getDate( this ) );
});

function getDate( element ) {
var date;
try {
date = $.datepicker.parseDate( dateFormat, element.value );
} catch( error ) {
date = null;
}

return date;
}
});
</script>

<script type="text/javascript">
   $( function() {
    var dateFormat = "yy-mm-dd",
      from = $("#datepicker")
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          changeYear: true,
          numberOfMonths: 1,
          minDate: 0,
          dateFormat: dateFormat
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $("#datepicker1").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: dateFormat
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
      return date;
    }
  });

  $("#registration_date").datepicker({
        defaultDate: "+1w",
        yearRange: '<?=date('Y')-58?>:<?=date('Y')?>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd"
   });

   $("#technical_date").datepicker({
        defaultDate: "+1w",
        yearRange: '<?=date('Y')?>:<?=date('Y')+32?>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd"
   });
</script>
<link rel="stylesheet" href="<?=base_url('skin/admin/css/fselect.css')?>" type="text/css"/>
<script src="<?=base_url('skin/admin/js/fselect.js')?>"></script>
<script >
  (function($) {
    $(function() {
        $('#multipleSelecct').fSelect();
    });
})(jQuery);

$(document).on('change','#servicesa',function (e) {
    var id=$(this).val();
    if(id!=''){
      window.location.href = "?id="+id;
    } else {
      window.location.href = "<?=base_url('admin/services/allowed_zip_code')?>";
    }
});

</script>

<link href="<?=base_url('skin/admin/css/star-rating.css')?>" media="all" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('skin/admin/js/star-rating.js')?>" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<?=base_url('skin/admin/js/custom.js')?>"></script>
</body>
</html>