<!DOCTYPE html>
<html>
<head>
<title>Administrator Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link rel="shortcut icon" href="<?=base_url('skin/admin/images/favicon.png')?>">
<link href="<?=base_url('skin/admin/css/bootstrap.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/thin-admin.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/css/font-awesome.css')?>" rel="stylesheet" media="screen">
<link href="<?=base_url('skin/admin/style/style.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/style/dashboard.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/demo_page.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/demo_table.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/style/nestable.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/jquery.fileupload-ui.css')?>" rel="stylesheet">
<link href="<?=base_url('skin/admin/css/wizard_progress.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url('skin/admin/build/css/intlTelInput.css')?>">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
<base id="baseurl" href="<?=base_url();?>">
</head>
<body>
<div class="container">
  <div class="top-navbar header b-b"> <a data-original-title="Toggle navigation" class="toggle-side-nav pull-left" href="#"><i class="icon-reorder"></i> </a>
    <div class="brand pull-left"> <a class="logoadmin" href="<?=base_url('admin/dashboard')?>"><img src="<?=base_url('skin/admin/images/logo.png')?>"></a>  <a class="pull-right" id="toggleButton" href="#"><i class="icon-large icon-reorder"></i></a></div>
    <ul class="nav navbar-nav navbar-right  hidden-xs">     
      <li class="dropdown user  hidden-xs"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-male"></i> <span class="username"><?=$this->session->userdata('admin_name');?></span> <i class="icon-caret-down small"></i> </a>
        <ul class="dropdown-menu">
          <li><a href="<?=base_url('admin/profile')?>"><i class="icon-user"></i> My Profile</a></li>
       
          <li class="divider"></li>
          <li><a href="<?=base_url('admin/login/logout')?>"><i class="icon-off"></i> Log Out</a></li>
        </ul>
      </li>
    </ul>
    
  </div>
</div>
<div class="wrapper">