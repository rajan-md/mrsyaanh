<div class="page-content pad-top-zero">
    <div class="content container">
        <?php 
       //print_r($settingsData);
        if(!empty($settingsData))
        {
          extract($settingsData);
        }
        ?>
        <?php getNotificationHtml(); ?>
        <form method="post">
            <input type="hidden" value="<?=set_value('sms_mobile', isset($sms_mobile)?$sms_mobile:" "); ?>" name="sms_mobile" id="sms_mobile">
            <div class="row">
                <div class="col-lg-12">
                    <div class="widget">
                        <div class="widget-header"> <i class="icon-align-left"></i>
                            <h3>Send SMS</h3>
                        </div>
                        <div class="widget-content">
                            <fieldset>

                                <div class="control-group">
                                    <div class="col-md-12">
                                        <label for="normal-field" class="control-label">Users</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" autocomplete="off" class="form-control" value="<?=set_value('sms_search', isset($sms_search)?$sms_search:" "); ?>" name="sms_search" id="sms_search">
                                            <?php echo form_error('sms_search'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="control-group">
                                    <div class="col-md-12">
                                        <label for="normal-field" class="control-label">SMS Title</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="<?=set_value('sms_title', isset($sms_title)?$sms_title:" "); ?>" name="sms_title" id="sms_title">
                                            <?php echo form_error('sms_title'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="control-group">
                                    <div class="col-md-12">
                                        <label for="normal-field" class="control-label">Message</label>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea rows="5" class="form-control" name="sms_message" id="sms_message">
                                            <?=set_value('sms_message', isset($sms_message)?$sms_message:""); ?>
                                        </textarea>
                                        <?php echo form_error('sms_message'); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="form-actions">
                            <div>
                                <button class="btn btn-primary" type="submit">Save </button>
                                <button class="btn btn-default" type="reset">Reset</button>
                            </div>
                        </div>
        </form>
    </div>
</div>
<style type="text/css">
    .sss_autosuggestion {
        position: absolute;
        top: 34px;
        background: rgb(0, 0, 0);
        width: 95%;
        margin: 0;
        left: 15px;
        z-index: 99999999;
        padding: 0;
    }
    
    .sss_autosuggestion li {
        cursor: pointer;
        text-align: left;
        list-style: none;
        padding: 5px 10px;
        border-bottom: 1px solid #ccc;
    }
</style>