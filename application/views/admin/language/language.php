
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Language <!-- <a class="btn btn-s-md btn-info" href="<?=base_url('admin/language/add')?>">Add New</a> --></h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Language Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images" id="example">
                  <thead>
                    <tr>
                      <th>Flag</th>
                      <th>Name</th>
                      <td>Language Code</td>
                      <th width="150">Default Language</th>
                      <th width="150">Created</th>
                      <th width="80">Status</th>
                      
                    </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($language)) {
                      foreach ($language as $data) { ?>
                    <tr>
                      <td>
                      <?php if(!empty($data->featured_img)) { ?>
                      <img style="height:20px;width:20px"  src="<?=base_url($data->featured_img)?>" >
                      <?php } else { ?> 
                      <img style="height:30px;width:30px"  src="<?=base_url('skin/admin/')?>images/no_image.jpg" > 
                      <?php } ?>
                      </td> 
                      <td><?=$data->name?></td>    
                      <td style="text-transform: capitalize;"><?=$data->code?></td>                  
                      <td class="hidden-xs">
                      <a href="<?=base_url('admin/language/default_lang/'.$data->id) ?>">
                      <?php if(!empty($data->default)) { echo '<i class="icon-large icon-star"></i>'; } else { echo '<i class="icon-large icon-star-empty"></i>'; }?>
                      </a>
                      </td>
                      <td class="hidden-xs"><?=$data->created?></td>
                      <td> 
                      <?php $status=$data->status; ?>
                       <a href="<?=base_url('admin/language/status/'.$data->id) ?>"><span class="label label-<?php if($status==3) { echo 'success'; } else { echo 'info'; } ?>"><?=getStatus($status)?></span></a> </td>
                      
                    </tr>

                    <?php } } ?>
                                        
                  </tbody>
                  <tfoot>        
                  </tfoot>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
