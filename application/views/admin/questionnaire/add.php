<div class="page-content pad-top-zero">
    <div class="content container">      
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-align-left"></i>
                        <h3>Add Service Attribute </h3>
                    </div>

                    <div class="widget-content">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <fieldset>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="normal-field" class="control-label">Question</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" value="<?=set_value('name'); ?>" id="normal-field">
                                            <?php echo form_error('name'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Service Category</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <?php echo form_dropdown('service_id', $serviceOptions,set_value('service_id',isset($service_id)?$service_id:""),'id="service_id" class="form-control"'); ?>
                                                <?php echo form_error('service_id'); ?>
                                        </div>
                                    </div>
                                </div> 
                                
                               <div class="control-group">
                                    <div class="col-md-2">
                                        <label for="disabled-input" class="control-label ">Parent</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <?php echo form_dropdown('parent', $parentOptions,set_value('parent',isset($parent)?$parent:""),'id="parent" class="form-control"'); ?>
                                                <?php echo form_error('parent'); ?>
                                        </div>
                                    </div>
                                </div>                                                             
                            </fieldset>

                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-success" type="submit">Save </button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>