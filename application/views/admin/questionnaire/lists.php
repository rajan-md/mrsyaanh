<?php $controller = $this->uri->segment(2); ?>
<div class="page-content">
    <div class="content container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-title">Service Questionnaires <?=AddButton('add')?></h2>
                <?php getNotificationHtml(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header"> <i class="icon-table"></i>
                        <h3>Questionnaires Management</h3>
                    </div>

                    <div class="widget-content">
                        <div class="body">
                            <table class="table table-striped table-images" id="example">
                                <thead>
                                    <tr>
                                      <th width="50">ID</th>
                                      <th>Question</th>
                                      <th width="200">Created</th>
                                      <th width="80">Status</th>
                                      <th width="120">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  //pre($records);
                                  if(!empty($records))
                                  {
                                    foreach ($records as $rec)
                                    {
                                      //pre($rec);
                                      ?>
                                      <tr>
                                          <td><?=$rec['id'];?></td>
                                          <td><?=$rec['name'];?></td>
                                          <td class="hidden-xs"><?=date('d M, Y h:i A',strtotime($rec['created'])); ?></td>
                                          <td><?=statusButton($controller,'status',$rec['status'],$rec['id']); ?></td>
                                         <td><?=AllowedAction($controller, 'edit', 'delete', 'Sure you want to delete this record ?', $rec['id']) ?></td>
                                      </tr>
                                      <?php
                                    }
                                  }
                                  ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="6"> </th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>