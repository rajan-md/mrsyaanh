<div class="page-content pad-top-zero">
    <div class="content container">
      <?php 
      //print_r($record);
      if(!empty($record))
      {
        extract($record);
      }
      ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="widget">
                        <div class="widget-header"> <i class="icon-align-left"></i>
                            <h3>Edit Service Questionnaire </h3>
                        </div>
                        <div class="widget-content">
                            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                                <fieldset>

                                    <div class="control-group">
                                        <div class="col-md-2">
                                            <label for="normal-field" class="control-label">Question</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control" value="<?=set_value('name', isset($name)?$name:" "); ?>">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <div class="col-md-2">
                                            <label for="disabled-input" class="control-label ">Service Category</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <?php echo form_dropdown('service_id', $serviceOptions,set_value('service_id',isset($service_id)?$service_id:""),'id="service_id" class="form-control"'); ?>
                                                    <?php echo form_error('service_id'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                      <div class="col-md-2">
                                          <label for="disabled-input" class="control-label ">Parent</label>
                                      </div>
                                      <div class="col-md-9">
                                          <div class="form-group">
                                              <?php echo form_dropdown('parent', $parentOptions,set_value('parent',isset($parent)?$parent:""),'id="parent" class="form-control"'); ?>
                                                  <?php echo form_error('parent'); ?>
                                          </div>
                                      </div>
                                  </div>


                                  <div class="field_wrapper">
                                    <?php
                                    if(!empty($choices))
                                    {
                                      $i = 1;
                                      $choicesArr = explode(",",$choices);
                                      if(!empty($choicesArr))
                                      {
                                        foreach ($choicesArr as $val)
                                        {
                                          ?>
                                          <div class="control-group">
                                              <div class="col-md-2">
                                                  <label for="disabled-input" class="control-label "><?=($i==1)?'Options':'';?></label>
                                              </div>
                                              <div class="col-md-8">
                                                  <div class="form-group">
                                                      <?php echo form_input(array('id' => 'choices', 'name' => 'choices[]','class'=>'form-control input-md','placeholder'=>'Choice/Answer','value'=>(!empty($val)?$val:''))); ?>
                                                      <?php echo form_error('choices'); ?>                                                  
                                                  </div>
                                              </div>
                                              <div class="col-md-1">
                                                  <?php
                                                  if($i==1)
                                                  {
                                                    ?>
                                                    <a href="javascript:void(0);" class="add_button" title="Add field"><img src="<?=base_url('skin/admin/images/add.png');?>"/></a>
                                                    <?php
                                                  }
                                                  else
                                                  {
                                                    ?>
                                                    <a href="javascript:void(0);" class="remove_button" title="Delete field"><img src="<?=base_url('skin/admin/images/delete.png');?>"/></a>
                                                    <?php
                                                  }
                                                  ?>
                                              </div>
                                          </div>
                                          <?php
                                        $i++;
                                        }
                                      }
                                    }
                                    else
                                    {
                                      ?>
                                      <div class="control-group">
                                          <div class="col-md-2">
                                              <label for="disabled-input" class="control-label ">Options</label>
                                          </div>
                                          <div class="col-md-8">
                                              <div class="form-group">
                                                  <?php echo form_input(array('id' => 'choices', 'name' => 'choices[]','class'=>'form-control input-md','placeholder'=>'Options/Choices','value'=>(!empty($val)?$val:''))); ?>
                                                  <?php echo form_error('choices'); ?>                                                  
                                              </div>
                                          </div>
                                          <div class="col-md-1">
                                              <a href="javascript:void(0);" class="add_button" title="Add field"><img src="<?=base_url('skin/admin/images/add.png');?>"/></a>
                                          </div>
                                      </div>
                                      <?php
                                    }
                                    ?>    
                                  </div>
                                  
                                </fieldset>

                                <div class="form-actions">
                                    <div>
                                        <button class="btn btn-success" type="submit">Save </button>
                                        <button class="btn btn-default" type="button">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="clearfix"></div><div class="control-group"><div class="col-md-2"><label for="disabled-input" class="control-label "></label></div><div class="col-md-8"><div class="form-group"><input type="number" name="donation_amount[]" value="" id="donation_amount" class="form-control input-md" placeholder="Choice/Answers"></div></div><div class="col-md-1"><a href="javascript:void(0);" class="remove_button" title="Delete field"><img src="<?=base_url('skin/admin/images/delete.png');?>"/></a></div></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
      alert(123);
        e.preventDefault();
        //alert( $(this).closest(".col-md-12").html());
        $(this).parent(".row").remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>