
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Reports</h2> 
          <?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="label-msg  btn-success">
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Reports Management</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                <table class="table table-striped table-images">
                    <tr>
                      <td>
                        <?php echo form_dropdown('serviceprovider', $serviceProviderOptions,set_value('serviceprovider',isset($serviceprovider)?$serviceprovider:""),'id="serviceprovider" class="form-control"'); ?>
                        </td>
                      <td><?php echo form_dropdown('duration', $durationOptions,set_value('duration',isset($duration)?$duration:""),'id="duration" class="form-control"'); ?></td>                 
                      <td><input type="text" name="from" class="datepicker form-control" placeholder="From date"></td>
                      <td><input type="text" name="to" class="datepicker form-control" placeholder="To date"></td>
                      <td><input type="submit" name="submit" class="btn btn-primary" value="Filter"></td>
                      <td><a href="" class="btn btn-success">Generate Statement</a></td>
                      <td>&nbsp;</td>
                    </tr>
                </table>
                <table class="table table-striped table-images" id="example2">
                  <thead>                   
                    <tr>
                      <th>Id</th>                      
                      <th>Service</th>
                      <th>Customer</th>
                      <th>Service Provider</th>
                      <th>Charges</th>
                      <th>Service Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($records))
                    {
                      foreach ($records as $data)
                      {
                        ?>
                          <tr>
                            <td><?=$data->id; ?></td>
                            <td><?=getServicesName($data->services); ?></td>
                            <td><?=getUserName($data->user_id); ?></td>
                            <td><?=!empty($data->vendor_id)?getBusinessName($data->vendor_id):''; ?></td>
                            <td><?=getPriceFormate($data->amount); ?></td>
                            <td><?=$data->booking_datetime; ?></td>
                            <td>
                              <?php $status_label = getServiceBookingStatusLabel($data->status); ?>
                              <?php $status_class = getServiceBookingStatusClass($status_label); ?>
                              <label class="<?=$status_class?>"><?=$status_label?></label></td>                            
                          </tr>
                          <?php
                        }
                      }
                      ?>                                        
                  </tbody>
                  <tfoot>
            <tr>
              <th colspan="7"> </th>
            </tr>
        </tfoot>
                </table>
               <div class="paginations"><?php echo $paginations; ?></div> 
              </div>
            </div>
          </div>
        </div>
      </div>
     
      
      


    </div>
  </div>
