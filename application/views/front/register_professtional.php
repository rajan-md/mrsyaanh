<div class="inner-banner">
	<div class="title">
		<h3><?=$this->lang->line('PROVIDER_SIGNUP_HEADING'); ?></h3>
	</div>
	<div class="breadcrumb-top">
		<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
		<li class="breadcrumb-item active"><?=$this->lang->line('PROVIDER_SIGNUP_HEADING'); ?></li>
		</ol>
	</div>
	
</div>
<div class="become-pro-service">
  		<div class="container">
  		<div class="form-box col-md-8 col-centered">
   		    <div class="f1">

				<!--div class="f1-steps">
					<div class="f1-progress">
						<div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="far fa-sun"></i></div>
						<p><?=$this->lang->line('CHOOSE_SERVICES');?></p>
					</div>
					<div class="f1-step active">
						<div class="f1-step-icon"><i class="far fa-user"></i></div>
						<p><?=$this->lang->line('PERSONAL_DETAILS');?></p>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="fa fa-check"></i></div>
						<p><?=$this->lang->line('SUCCESS');?></p>
					</div>
				</div-->
                 <div id="loadError"></div>
                <div id="professtionalRequest">

                    <fieldset>
                        <div class="row">

                            <?php
                            if(!empty($serviceRequest['bussiness_holder_fname']))
                            {
                            ?>
                            <div class="col-sm-12">                  
                                <div class="form-group">                         
                                    <p><strong><?=$this->lang->line('BUSSINESS_HOLDER_NAME');?> : </strong>
                                    <?=!empty($serviceRequest['bussiness_holder_fname'])?$serviceRequest['bussiness_holder_fname'].' '.$serviceRequest['bussiness_holder_lname']:''?>
                                    </p>                                    
                                </div>
                            </div>
                            <?php
                            }
                            if(!empty($serviceRequest['business']) && $serviceRequest['business']=='yes')
                            {
                              ?>
                              <div class="col-sm-6">                  
                                <div class="form-group">                         
                                    <p><strong><?=$this->lang->line('BUSINESS_NAME');?> : </strong>
                                    <?=!empty($serviceRequest['business_name'])?$serviceRequest['business_name']:''?>
                                    </p>                                    
                                </div>
                              </div>
                              <div class="col-sm-6">                  
                                <div class="form-group">                         
                                    <p><strong><?=$this->lang->line('BUSINESS_TYPE');?> : </strong>
                                    <?=!empty($serviceRequest['business_type'])?$serviceRequest['business_type']:''?>
                                    </p>                                    
                                </div>
                              </div>
                              <?php
                            }
                            ?>
                            
                            <div class="col-sm-6">                  
                                <div class="form-group">                         
                                    <p><strong><?=$this->lang->line('SERVICE');?> : </strong>
                                    <?=getServicesName(!empty($serviceRequest['service_id'])?$serviceRequest['service_id']:'')?>
                                    </p>                                    
                                </div>
                            </div>                     
                             <div class="col-sm-6">
                                <div class="form-group">
                                     <p><strong><?=$this->lang->line('ZIPCODE');?> : </strong>
                                    <?=!empty($serviceRequest['service_zip_code'])?$serviceRequest['service_zip_code']:''?>
                                    </p>
                                </div>
                             </div>
                        </div>
                    </fieldset><br>

                    <?php getNotificationHtml(); ?>

                    <?php //echo "<pre>"; print_r($serviceRequest); echo "</pre>"; ?>

                    <form role="form" id="serviceprovider_signup" class="personalDetails" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="srid" name="srid" value="<?=$request_id;?>"/>
                        <input type="hidden" id="token" name="token" value="<?=$token;?>"/>
                        <input type="hidden" name="route" id="route">
                        <input type="hidden" id="latitude" name="site_latitude">
                        <input type="hidden" id="longitude" name="site_longitude">
                        <input type="hidden" id="mobbox" name="mobbox">
                        <input type="hidden" id="telephonebox" name="telephonebox">
                        <input type="hidden" id="bussmobbox" name="bussmobbox">
                        <fieldset>

                        <h4><?=$this->lang->line('PERSONAL_DETAILS');?></h4>
                        <hr>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>
                                    <input type="text" name="fname" placeholder="First Name" class="f1-email form-control" id="f-name" value="<?=!empty($serviceRequest['bussiness_holder_fname'])?$serviceRequest['bussiness_holder_fname']:''?>" required="">
                                </div>
                            </div>                     
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="profile_field"><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>
                                    <input type="text" name="lname" placeholder="Last Name" class="f1-email form-control" id="l-name" value="<?=!empty($serviceRequest['bussiness_holder_lname'])?$serviceRequest['bussiness_holder_lname']:''?>" required="">

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="profile_field"><?=$this->lang->line('GENDER');?><span class="redstar">*</span></label> &nbsp;
                                    <label class="cus_radio">
                                      <input type="radio" name="gender" checked="checked" value="male"><?=$this->lang->line('MALE');?>
                                    </label>
                                    <label class="cus_radio">
                                      <input type="radio" name="gender" value="female"><?=$this->lang->line('FEMALE');?>
                                    </label>
                                    <label class="cus_radio">
                                      <input type="radio" name="gender" value="Others"><?=$this->lang->line('OTHERS');?>
                                    </label>
                                </div> 
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('DATE_OF_BIRTH');?><span class="redstar">*</span></label>
                                <input type="text" name="dob" class="form-control" id="dob" placeholder="yyyy-mm-dd" required=""  autocomplete="off">
                              </div>
                             </div>
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>
                                  <input type="email" id="f1-email" name="useremail" placeholder="Email..." pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" class="f1-email form-control" id="f1-email" value="<?=set_value('email',isset($serviceRequest['business_email'])?$serviceRequest['business_email']:"")?>" required="">
                              </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group personal_phonebox">
                                    <label class="profile_field"><?=$this->lang->line('MOBILE');?><span class="redstar">*</span></label>
                                    <input type="text" name="mobile" placeholder="<?=$this->lang->line('MOBILE');?>" class="form-control" id="phonecode" value="<?=set_value('mobile',!empty($serviceRequest['business_mobile'])?$serviceRequest['business_mobile']:"")?>" required="">
                                </div>
                            </div>                            
                        </div>
                        
                        <br>
                        <h4><?=$this->lang->line('HOME_ADDRESS');?></h4>
                        <hr>

                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('COUNTRY');?><span class="redstar">*</span></label>
                                <?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($country)?$country:""),'id="home_country" class="f1-email form-control" required=""'); ?>
                                </div>
                             </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('STATE_PROVINCE');?><span class="redstar">*</span></label>
                                    <?php echo form_dropdown('state', array(''=>$this->lang->line('SELECT_STATE')),set_value('state',isset($state)?$state:""),'id="home_state" class="form-control" required=""'); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('ADDRESS');?><span class="redstar">*</span></label>
                                <input type="text" onclick="initialize();" placeholder="<?=$this->lang->line('ADDRESS');?>" name="site_location"  class="f1-last-name form-control" id="site_location" autocomplete="off" required="">
                              </div>
                            </div>                        
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('APT_UNIT_NO');?></label>
                                <input type="text" name="aptno" placeholder="<?=$this->lang->line('APT_UNIT_NO');?>" class="f1-email form-control" id="aptno">
                              </div>
                            </div>                       
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('STREET_NO');?><span class="redstar">*</span></label>
                                <input type="text" name="street_number" placeholder="<?=$this->lang->line('STREET_NO');?>" class="f1-email form-control" id="street_number" required="">
                              </div>
                            </div>                         
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('STREET_NAME');?><span class="redstar">*</span></label>
                                <input type="text" name="street_name" placeholder="<?=$this->lang->line('STREET_NAME');?>" class="f1-email form-control" id="street_name" required="">
                              </div>
                            </div>                         
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('STREET_TYPE');?><span class="redstar">*</span></label>
                                <?php echo form_dropdown('street_type', streetTypeOption(),set_value('street_type',isset($street_type)?$street_type:""),'id="street_type" class="form-control" required=""'); ?>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('DIRECTION');?></label>
                                <?php echo form_dropdown('direction', directionOption(),set_value('direction',isset($direction)?$direction:""),'id="direction" class="form-control"'); ?>
                              </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('CITY');?><span class="redstar">*</span></label>
                                    <input type="text" name="city" placeholder="<?=$this->lang->line('CITY');?>" class="f1-email form-control" id="locality" value="<?=set_value('city',isset($city)?$city:"")?>" required="">
                                </div>
                            </div>                         
                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label class="profile_field"><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>
                                   <input type="text" name="postal_code" placeholder="<?=$this->lang->line('ZIP_CODE');?>" class="f1-email form-control" id="postal_code" value="<?=set_value('postal_code',isset($serviceRequest['service_zip_code'])?$serviceRequest['service_zip_code']:"")?>" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group telephone_box">
                                    <label class="profile_field"><?=$this->lang->line('HOME_TEL_NO');?><span class="redstar">*</span></label>
                                    <input type="text" name="telephone" placeholder="<?=$this->lang->line('HOME_TEL_NO');?>" class="form-control" id="telephone" value="<?=set_value('telephone',!empty($serviceRequest['business_mobile'])?$serviceRequest['business_mobile']:"")?>" required="">
                                </div>
                            </div>
                        </div>

                        <br>
                        <h4><?=$this->lang->line('BUSINESS_INFORMATION'); ?></h4>
                        <hr>

                       
                        <div style="height:20px"></div>
                        <?php
                        $business=isset($serviceRequest['business'])?$serviceRequest['business']:"no";
                        $field_require = (!empty($business) && $business=='yes')?'required="required"':'';
                        ?>
                        <div class="form-group">
                            <label><?=$this->lang->line('DO_YOU_HAVE_OWN_A_BUSINESS_NOW');?>?<span class="redstar">*</span></label>
                            <div>                                
                                <label class="cus_radio">
                                  <input type="radio" name="business" value="no" required="" <?php if($business=='no') { ?> checked="checked" <?php } ?> >
                                  <?=$this->lang->line('NO');?></label>
                                  <label class="cus_radio">
                                  <input type="radio" name="business" value="yes" required="" <?php if($business=='yes') { ?> checked="checked" <?php } ?>>
                                  <?=$this->lang->line('YES');?></label>
                            </div>
                        </div>


                        <div id="businessDetails" class="w-100 mt20" style="display: <?php if($business=='yes') { echo 'block'; } else { echo 'none'; } ?>">
                              <div class="row">
                                  <div class="col-sm-12">
                                    <div class="form-group">                           
                                      <label class="profile_field"><?=$this->lang->line('BUSINESS_NAME');?><span class="redstar">*</span></label>
                                      <input type="text" name="business_name" class="form-control" id="business_name" value="<?=set_value('business_name',isset($serviceRequest['business_name'])?$serviceRequest['business_name']:"")?>" <?=$field_require;?>>
                                    </div>
                                  </div>                         
                                  <div class="col-sm-6">
                                    <div class="form-group">                           
                                        <label class="profile_field"><?=$this->lang->line('BUSINESS_TYPE');?><span class="redstar">*</span></label>
                                        <?php echo form_dropdown('business_type', bussinessTypeOption(),set_value('business_type',isset($serviceRequest['business_type'])?$serviceRequest['business_type']:""),'id="business_type" class="form-control" '.$field_require); ?>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">                           
                                        <label class="profile_field"><?=$this->lang->line('DATE_STARTED');?></label>
                                        <input type="text" name="start_date" class="datepicker form-control" autocomplete="off" placeholder="yyyy-mm-dd" id="start_date">
                                    </div>
                                  </div>
                              </div>
                              <?php $license=isset($serviceRequest['license'])?$serviceRequest['license']:"no"; ?>
                              <div class="form-group">
                              <label><?=$this->lang->line('DO_YOU_HAVE_A_LICENSE');?>?<span class="redstar">*</span></label>
                              <div>
                               <label class="cus_radio">
                                <input type="radio" name="license" value="no" required="" <?php if($license=='no') { ?> checked="checked" <?php } ?>>No
                              </label>
                              <label class="cus_radio">
                                <input type="radio" name="license" value="yes" required="" <?php if($license=='yes') { ?> checked="checked" <?php } ?>>Yes
                              </label>                           
                              </div>
                              </div>
                              <?php $license_require = (!empty($license) && $license=='yes')?'required="required"':''; ?>
                              <div id="licenseDetails" style="display:<?php if($license=='yes') { echo 'block'; } else { echo 'none'; } ?>">
                              <div class="form-group">
                                <label class="profile_field"><?=$this->lang->line('LICENSE_DETAILS');?><span class="redstar">*</span></label>
                                <input type="text" id="license_details" name="license_details" placeholder="License Details" class="f1-last-name form-control" value="<?=set_value('license_details',isset($serviceRequest['license_details'])?$serviceRequest['license_details']:"")?>" <?=$license_require;?>>
                              </div>
                              </div>
                        

                        <br>
                        <h4><?=$this->lang->line('BUSSINESS_ADDRESS'); ?> <span class="same_as_home"><?=$this->lang->line('SAME_AS_HOME_ADDRESS'); ?> <input type="checkbox" name="same_address" id="same_address" value="1"></span></h4>
                        <hr>

                        <div class="row">

                          <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="profile_field"><?=$this->lang->line('COUNTRY');?><span class="redstar">*</span></label>
                                    <?php echo form_dropdown('business_country', getCountriesOptions(),set_value('business_country',isset($business_country)?$business_country:""),'id="business_country" class="form-control" '.$field_require); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('STATE_PROVINCE');?><span class="redstar">*</span></label>
                                    <?php echo form_dropdown('business_state', array(''=>$this->lang->line('SELECT_STATE')),set_value('business_state',isset($business_state)?$business_state:""),'id="business_state" class="form-control" '.$field_require); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('APT_UNIT_NO');?></label>
                                    <input type="text" name="business_aptno" class="form-control" id="business_aptno">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('STREET_NO');?><span class="redstar">*</span></label>
                                    <input type="text" name="business_street_no" class="form-control" id="business_street_no" <?=$field_require;?>>
                                </div>
                            </div>                         
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('STREET_NAME');?><span class="redstar">*</span></label>
                                <input type="text" name="business_street_name" class="form-control" id="business_street_name" <?=$field_require;?>>
                              </div>
                            </div>                         
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('STREET_TYPE');?><span class="redstar">*</span></label>
                                    <?php echo form_dropdown('business_street_type', streetTypeOption(),set_value('business_street_type',isset($business_street_type)?$business_street_type:""),'id="business_street_type" class="form-control" '.$field_require); ?>
                                </div>
                            </div>                         
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('DIRECTION');?></label>
                                    <?php echo form_dropdown('business_direction', directionOption(),set_value('business_direction',isset($business_direction)?$business_direction:""),'id="business_direction" class="form-control" '); ?>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('CITY');?><span class="redstar">*</span></label>
                                <input type="text" name="business_city" class="form-control" id="business_city" placeholder="<?=$this->lang->line('CITY');?>" value="<?=set_value('business_city',isset($business_city)?$business_city:"")?>" <?=$field_require;?>>
                              </div>
                            </div>                        
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                <label class="profile_field"><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>
                                <input type="text" name="business_zip" class="form-control" id="business_zip" value="<?=set_value('business_zip',isset($serviceRequest['service_zip_code'])?$serviceRequest['service_zip_code']:"")?>" <?=$field_require;?>>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group bussiness_phonebox">                         
                                <label class="profile_field"><?=$this->lang->line('BUSINESS_TEL_NO');?><span class="redstar">*</span></label>
                                <input type="text" name="business_mobile" class="form-control" id="bussphonecode" value="<?=set_value('bussphonecode',!empty($serviceRequest['business_mobile'])?$serviceRequest['business_mobile']:"")?>" <?=$field_require;?>>
                              </div>
                            </div>                         
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('BUSINESS_FAX');?></label>
                                    <input type="text" name="business_fax" class="form-control" id="business_fax">
                                </div>
                            </div>                         
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="profile_field"><?=$this->lang->line('BUSINESS_EMAIL');?><span class="redstar">*</span></label>
                                <input type="text" name="business_email" class="form-control" id="business_email" value="<?=set_value('business_email',isset($serviceRequest['business_email'])?$serviceRequest['business_email']:"")?>" <?=$field_require;?>>
                              </div>
                            </div>                        
                        </div> 

                        </div>

                        <!--br>
                        <h4><?=$this->lang->line('CAR_SPECIFICATION'); ?></h4>
                        <hr>
                        
                        <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('DRIVER_LICENCE');?></label>
                                  <input type="text" name="driver_licence" class="form-control" id="driver_licence" placeholder="<?=$this->lang->line('DRIVER_LICENCE');?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('MADE');?></label>
                                  <input type="text" name="made" class="form-control" id="made" placeholder="<?=$this->lang->line('MADE');?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="profile_field"><?=$this->lang->line('MODEL');?></label>
                                  <input type="text" name="model" class="form-control" id="model" placeholder="<?=$this->lang->line('MODEL');?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="profile_field"><?=$this->lang->line('YEAR');?></label>
                                  <input type="text" name="year" class="form-control" id="year" placeholder="<?=$this->lang->line('YEAR');?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="profile_field"><?=$this->lang->line('COLOR');?></label>
                                  <input type="text" name="color" class="form-control" id="color" placeholder="<?=$this->lang->line('COLOR');?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="profile_field"><?=$this->lang->line('LICENCE_PLATE_NO');?></label>
                                  <input type="text" name="licence_plate_no" class="form-control" id="licence_plate_no" placeholder="<?=$this->lang->line('LICENCE_PLATE_NO');?>">
                              </div>
                          </div>         
                        </div-->                        

                        <br>
                        <h4><?=$this->lang->line('PASSWORD'); ?></h4>
                        <hr>

                        <div class="row">
                            
                            <div class="col-sm-6">
                            <div class="form-group">
                                <label class="profile_field"><?=$this->lang->line('PASSWORD');?><span class="redstar">*</span></label>
                                <input type="password" name="password" placeholder="<?=$this->lang->line('PASSWORD');?>..." class="f1-password form-control" id="password" required="">
                                </div>
                             </div>
                             
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="profile_field"><?=$this->lang->line('CONFIRM_PASSWORD');?><span class="redstar">*</span></label>
                                    <input type="password" name="confirm_password" placeholder="<?=$this->lang->line('CONFIRM_PASSWORD');?>..." class="f1-repeat-password form-control" id="confirm_password" required="">
                                </div>
                             </div>

                        </div>                   
                        <div class="form-group mt20">
                            <input type="submit" name="submit" class="btn common-btn serviceRequestStep2" id="submit" value="Submit">
                        </div>

                        </div>

                        </fieldset>
                    </form>
			    
             </div>
			</div>  
	       </div>			
		<div class="sky-3col">
	    	<div class="row">
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('GROW_YOUR_BUSINESS');?></h4>
				<p><?=$this->lang->line('GET_NEW_CUSTOMERS');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('WORK_ON_YOUR_OWN');?></h4>
				<p><?=$this->lang->line('QUOTE_YOUR_PRICE');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('BUSINESS_TOOLS');?></h4>
				<p><?=$this->lang->line('GET_BUSINESS_TOOLS');?>.</p>
				</div>
			</div>
		</div>
  		</div>
</div>
  	
</div>