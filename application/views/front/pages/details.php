<?php if(!empty($pageContent)) { extract($pageContent); } ?>

<div class="inner-banner">
	<div class="title">
		<h3><?php if(!empty($pageContent)) { echo $name; } else { echo 'Page Not Found'; } ?></h3>
	</div>
	<div class="breadcrumb-top">
		<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
		<li class="breadcrumb-item active"><?php if(!empty($pageContent)) { echo $name;  } else { echo 'Page Not Found'; } ?></li>
		</ol>
	</div>
</div>


<section class="pageDetails">
  <div class="container">    
    <div class="row">  
      <div class="col-md-12">		   
	   <div class="inner-about-text">
         <?php if(!empty($pageContent)) { echo $description; } else { echo 'Page Not Found'; } ?> 
       </div>         
      </div>                                       
    </div>   
  </div>
</section>

