<div class="become-professional-wrapper">
     <div class="become-pro-banner">
        <div class="container">
			
  			<div class="become-pro-banner-text text-center">
  			<h1><?=$this->lang->line('EXPAND_YOUR_SERVICE'); ?>.</h1>
  			<p><?=$this->lang->line('JOIN_A_COMMUNITY');?>.</p>
  			</div>
  			 
  			 <div class="bp-how-fce-work text-center">
  				<h3><?=$this->lang->line('HOW_IT_WORK');?>?</h3>	
  				<div class="row">			
				 <div class="col-sm-4">
						<i class="fas fa-check"></i>
						<h4><?=$this->lang->line('GET_VERIFIED_TITLE');?>.</h4>
					    <p><?=$this->lang->line('GET_CONNECTED_DESCRIPTION');?>.</p>
					</div>

 				<div class="col-sm-4">
					<i class="fas fa-dollar-sign"></i>
			          <h4><?=$this->lang->line('PAY_TO_TITLE');?></h4>
					  <p><?=$this->lang->line('PAY_ONLY_DESCRIPTION');?>.</p>
				</div>
		
 					<div class="col-sm-4">
						<i class="fas fa-comment-dots"></i>
						<h4><?=$this->lang->line('CHAT_AND_TITLE');?></h4>
						<p><?=$this->lang->line('CHAT_WITH_DESCRIPTION');?>.</p>
					</div>
  				</div>	
  			</div>
  		</div>		
  	</div><!--become-pro-banner-->
  	<div class="become-pro-service">
  		<div class="container">
  		<div class="form-box col-md-8 col-centered">
   		    <div class="f1">
   		    	<?php getNotificationHtml(); ?>
				<div class="f1-steps">
					<div class="f1-progress">
						<div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="far fa-sun"></i></div>
						<p><?=$this->lang->line('CHOOSE_SERVICES');?></p>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="far fa-user"></i></div>
						<p><?=$this->lang->line('PERSONAL_DETAILS');?></p>
					</div>
					<div class="f1-step active">
						<div class="f1-step-icon"><i class="fa fa-check"></i></div>
						<p><?=$this->lang->line('SUCCESS');?></p>
					</div>
				</div>
        <div id="professtionalRequest">
	       <fieldset>                    
               <div class="bus-part-success">
                <i class="fa fa-check"></i>
                <h4>Congratulations!</h4>
                <h5>You have successfully signed up as a partner on First Choice Everything</h5>
                <p>Thank you for requesting information. We will email you the infor mation you need.</p>
                <p>In that information pavkage we will explain how our system work and a form (Form 102)</p>
                <p>If you need more information you will fill that form and we will send you more deatails.</p>
               </div>
            </fieldset>
        </div>
			</div>  
	       </div>			
		<div class="sky-3col">
	    	<div class="row">
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('GROW_YOUR_BUSINESS');?></h4>
				<p><?=$this->lang->line('GET_NEW_CUSTOMERS');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('WORK_ON_YOUR_OWN');?></h4>
				<p><?=$this->lang->line('QUOTE_YOUR_PRICE');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('BUSINESS_TOOLS');?></h4>
				<p><?=$this->lang->line('GET_BUSINESS_TOOLS');?>.</p>
				</div>
			</div>
		</div>
  		</div>
  	</div>
  	
</div>