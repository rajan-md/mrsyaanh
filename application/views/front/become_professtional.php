<div class="become-professional-wrapper">
     <div class="become-pro-banner">
        <div class="container">
			
  			<div class="become-pro-banner-text text-center">
  			<h1><?=$this->lang->line('EXPAND_YOUR_SERVICE'); ?>.</h1>
  			<p><?=$this->lang->line('JOIN_A_COMMUNITY');?>.</p>
  			</div>
  			 
  			 <div class="bp-how-fce-work text-center">
  				<h3><?=$this->lang->line('HOW_IT_WORK');?>?</h3>	
  				<div class="row">			
				 <div class="col-sm-4">
						<i class="fas fa-check"></i>
						<h4><?=$this->lang->line('GET_VERIFIED_TITLE');?>.</h4>
					    <p><?=$this->lang->line('GET_CONNECTED_DESCRIPTION');?>.</p>
					</div>

 				<div class="col-sm-4">
					<i class="fas fa-dollar-sign"></i>
			          <h4><?=$this->lang->line('PAY_TO_TITLE');?></h4>
					  <p><?=$this->lang->line('PAY_ONLY_DESCRIPTION');?>.</p>
				</div>
		
 					<div class="col-sm-4">
						<i class="fas fa-comment-dots"></i>
						<h4><?=$this->lang->line('CHAT_AND_TITLE');?></h4>
						<p><?=$this->lang->line('CHAT_WITH_DESCRIPTION');?>.</p>
					</div>
  				</div>	
  			</div>
  		</div>		
  	</div><!--become-pro-banner-->
  	<div class="become-pro-service">
  		<div class="container">
  		<div class="form-box col-md-8 col-centered">
   		    <div class="f1">
   		    	<?php getNotificationHtml(); ?>
				<!--div class="f1-steps">
					<div class="f1-progress">
						<div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
					</div>
					<div class="f1-step active">
						<div class="f1-step-icon"><i class="far fa-sun"></i></div>
						<p><?=$this->lang->line('CHOOSE_SERVICES');?></p>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="far fa-user"></i></div>
						<p><?=$this->lang->line('PERSONAL_DETAILS');?></p>
					</div>
					<div class="f1-step">
						<div class="f1-step-icon"><i class="fa fa-check"></i></div>
						<p><?=$this->lang->line('SUCCESS');?></p>
					</div>
				</div-->
                <div id="loadError"></div>
                <div id="professtionalRequest">
			    <form role="form" id="service_request" action="" method="post" >
			    <input type="hidden" id="mobbox" name="mobbox" value="+1">
				<fieldset> 
                    <div class="row">

	                	<div class="col-sm-12"> 
							<div class="form-group">
								<label><?=$this->lang->line('SELECT_SERVICE');?><span class="redstar">*</span></label>
								 <div class="form-group">
								 <?php echo form_dropdown('chooseserices', $serviceOptions,set_value('chooseserices'),'id="chooseserices" class="form-control" required=""'); ?>
								 <?php echo form_error('chooseserices'); ?>
				                </div>
							</div>
						</div>	

						<div class="col-sm-12"> 
							<div class="form-group">
								<label><?=$this->lang->line('DO_YOU_HAVE_OWN_A_BUSINESS_NOW');?><span class="redstar">*</span></label>
								<div>									
					                <label class="cus_radio">
					                  <input type="radio" name="business" value="no" required="" checked="checked">
					                  <?=$this->lang->line('NO');?></label>
					                  <label class="cus_radio">
					                  <input type="radio" name="business" value="yes" required="">
					                  <?=$this->lang->line('YES');?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div id="businessDetails" style="display: none;">
									<div class="form-group">
										<input type="text" id="business_name" name="business_name" placeholder="<?=$this->lang->line('BUSINESS_NAME');?>" class="business_name form-control" >
									</div>
									<div class="form-group">
										<?php echo form_dropdown('business_type', bussinessTypeOption(),set_value('business_type',isset($business_type)?$business_type:""),'id="business_type" class="form-control"'); ?>
			                      		<?php echo form_error('business_type'); ?>
									</div>
									<div class="form-group">
										<label><?=$this->lang->line('DO_YOU_HAVE_A_LICENSE');?><span class="redstar">*</span></label>
										<div>
										 	<label class="cus_radio"><input type="radio" name="license" value="no" required="" checked="checked"><?=$this->lang->line('NO');?></label>
											<label class="cus_radio"><input type="radio" name="license" value="yes" required=""><?=$this->lang->line('YES');?></label>
										</div>
									</div>

								<div id="licenseDetails" style="display:none">
									<div class="form-group">
										<input type="text" id="license_details" name="license_details" placeholder="<?=$this->lang->line('LICENSE_DETAILS');?>" class="f1-last-name form-control" >
									</div>
								</div>					
		                    </div>
		                </div>

		                <div class="col-sm-6"> 
							<div class="form-group">
		                    	<label><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>
								<input type="text" name="business_holder_fname" placeholder="<?=$this->lang->line('FIRST_NAME');?>" class="f1-fname form-control" id="business_holder_fname" required="">
								<?php echo form_error('business_holder_fname'); ?>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
		                    	<label><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>
								<input type="text" name="business_holder_lname" placeholder="<?=$this->lang->line('LAST_NAME');?>" class="f1-lname form-control" id="business_holder_lname" required="">
								<?php echo form_error('business_holder_lname'); ?>
							</div>
						</div>

	                    <div class="col-sm-12">
		                    <div class="form-group">
		                    	<label><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>
								<input type="email" name="business_email" placeholder="<?=$this->lang->line('EMAIL');?>" class="f1-email form-control" id="business_email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" required="">
								<?php echo form_error('business_email'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group personal_phonebox">
								<label><?=$this->lang->line('PHONE_NO');?><span class="redstar">*</span></label>
								<input name="phonecode" placeholder="<?=$this->lang->line('ENTER_PHONE_NO');?>." class="f1-last-name form-control" id="phonecode" type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}" title="Invalid Mobile No" required="">
								<?php echo form_error('phonecode'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>
								<input type="text" name="service_zipcode" placeholder="<?=$this->lang->line('ZIP_CODE');?>" class="f1-last-name form-control" id="service_zipcode" required="">
								<?php echo form_error('service_zipcode'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label><?=$this->lang->line('MESSAGE');?></label>
								<textarea name="message" class="f1-last-name form-control"></textarea>
								<?php echo form_error('message'); ?>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="f1-buttons">
								<button type="submit" class="btn btn-submit btn-gray serviceRequestStep1">
									<?=$this->lang->line('SUBMIT');?>
								</button>
							</div>
						</div>
					</div>
				</fieldset>
			  </form>
             </div>
			</div>  
	       </div>			
		<div class="sky-3col">
	    	<div class="row">
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('GROW_YOUR_BUSINESS');?></h4>
				<p><?=$this->lang->line('GET_NEW_CUSTOMERS');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('WORK_ON_YOUR_OWN');?></h4>
				<p><?=$this->lang->line('QUOTE_YOUR_PRICE');?>.</p>
				</div>
				<div class="col-sm-4">
				<i class="fas fa-check"></i>
				<h4><?=$this->lang->line('BUSINESS_TOOLS');?></h4>
				<p><?=$this->lang->line('GET_BUSINESS_TOOLS');?>.</p>
				</div>
			</div>
		</div>
  		</div>
  	</div>
  	
</div>