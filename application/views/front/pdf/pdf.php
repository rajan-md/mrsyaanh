<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">-->
<title>Lapsmile</title>
<!-- Bootstrap core CSS -->
<link href="<?=base_url('skin/front')?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url('skin/front')?>/css/pdf-style.css" rel="stylesheet">
<link href="<?=base_url('skin/front')?>/css/font-awesome.css" rel="stylesheet">
<link href="<?=base_url('skin/front')?>/css/font-awesome.min.css" rel="stylesheet">
</head>
<body style="font-size: 14px">
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="margin-bottom: 15px; text-transform: uppercase; font-size: 24px; text-align: center;">
      <span style="border-bottom: 1px solid #000">
      <?=$this->lang->line('CONTRACT_HEADING')?>
      </span>  
      </div>
      <div class="col-md-6"  style="float:left;width:120px">
        <div class="logo"><img style="width:120px" src="<?=base_url('skin/front')?>/images/logo.png"></div>
      </div>
      <div class="col-md-6" style="float:right; text-align:right; margin-top:15px">
      <div style="float:right; text-align:right">
      <img style="height:45px" src="<?=base_url(getAdvanceOption('insurace_logo'))?>">
      </div>
      <div><?=getAdvanceOption('tagline')?></div>
      </div>

      <div style="clear: both;"></div>
    

    </div>
  </div>
</header>
<?php 
/*echo '<pre>';
print_r($requestDeta);
echo '</pre>';*/

extract($requestDeta);
$acceptbyData=getUserProfileDataByID($accept_by);
$requestbyData=getUserProfileDataByID($request_by);
$user_id = $this->session->userdata('userID');
$login_id= getUserIdByVehicleId($vehicle_id);
if($request_type=='swap'){
  
  $vehicleData=getVehicleDetailsById($vehicle_id);
  $vehicleRequestData=getVehicleDetailsById($request_vehicle_id);
  
} else {
   $vehicleData=getVehicleDetailsById($vehicle_id);
}

/*echo '<pre>';
print_r($vehicleData);
echo '</pre>';*/
?>
<div class="main">
  <div class="container">
     <div class="row">
      <div class="col-sm-12">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" >
          <tr>
            <td align="left" valign="top" style="padding: 8px; width: 50%"><strong><?=$this->lang->line('DATE')?></strong> .....</td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('REQUEST_ID')?> : <?=$request_id?> </td>
          </tr>
          <tr>
            <th  scope="col" style="padding:8px;"><h4><?=$this->lang->line('REQUEST_DETAILS')?> : <?=$request_type?> 
            <?php 
               if($maintenance==1){
                echo ' ( '.$this->lang->line('MAINTENANCE').' )';
               }
              ?>
            </h4></th>
            <th  scope="col" style="padding:8px;"><h4><?=$this->lang->line('REQUEST_KMS')?> : <?=$kms?> km </h4></th>
            <!-- <th scope="col" style="padding: 8px;">Emprunteur / borrower</th> -->
          </tr>
          
          
          
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><strong><?=$this->lang->line('PICK_UP')?> </strong></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$pick_up?> <strong><?=$this->lang->line('DATE')?></strong> : <?=date("M j, Y @ g:i a", strtotime($pick_up_date)); ?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><strong><?=$this->lang->line('DROP_OFF')?></strong></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$drop_off?> <strong><?=$this->lang->line('DATE')?></strong> : <?=date("M j, Y @ g:i a", strtotime($drop_off_date)); ?></td>
          </tr>
          
          <tr>
            <td  align="left" valign="top" style="padding: 8px;"><strong><?=$this->lang->line('DISCRIPTION')?></strong></td>
            <td  align="left" valign="top" style="padding: 8px;"><?=$request_description; ?></td>
          </tr>
         
          
        </table>
      </div>
    </div>
    <div class="row spaceTop">
      <div class="col-sm-12">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" >
          <tr>
            <th scope="col" style="padding:8px;width:50%"><h4><?=$this->lang->line('GIVER')?></h4></th>
            <th scope="col" style="padding:8px;"><h4><?=$this->lang->line('RECEIVER')?></h4></th>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('NAME')?> : <strong><?=getCustomerName($accept_by)?></strong>
              <?php  $acceptGen=$acceptbyData['gender']; if($acceptGen=='male') { ?>  
                <span><img src="<?=base_url('skin/front/images/man.png')?>"></span>
              <?php } else { ?>
                <span><img src="<?=base_url('skin/front/images/woman.png')?>"></span>
              <?php }  ?>
            </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('NAME')?> : <strong><?=getCustomerName($request_by)?></strong>
              <?php  $reqGen=$requestbyData['gender']; if($reqGen=='male') { ?>  
                <span><img src="<?=base_url('skin/front/images/man.png')?>"></span>
              <?php } else { ?>
                <span><img src="<?=base_url('skin/front/images/woman.png')?>"></span>
              <?php }  ?>
             
            </td>
          </tr>
          
          
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('MOBILE')?> : <strong><?=$acceptbyData['mobile']?></strong></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('MOBILE')?> : <strong><?=$requestbyData['mobile']?></strong></td>
          </tr>

          
          <tr>
          <?php if($acceptbyData['profile_type']=='personal'){ ?>
            
          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('IDENTIFICATION_NUMBER')?> : <strong><?=$acceptbyData['identification_number']?></strong></td>          
          <?php } ?> 
          <?php if($requestbyData['profile_type']=='personal') { ?>
          
          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('IDENTIFICATION_NUMBER')?> : <strong><?=$requestbyData['identification_number']?></strong></td>          
          <?php } ?>           
          </tr>

          <tr>
          <?php if($acceptbyData['profile_type']=='personal') {?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('DOB')?> : <strong><?=date("M j, Y", strtotime($acceptbyData['dob'])); ?></strong></td>
          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BUSINESS')?> : <strong><?=$acceptbyData['company_type']?></strong></td>          
          <?php } ?> 
          <?php if($requestbyData['profile_type']=='personal') {?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('DOB')?> : <strong><?=date("M j, Y", strtotime($requestbyData['dob'])); ?></strong></td>
          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BUSINESS')?> : <strong><?=$requestbyData['company_type']?></strong></td>          
          <?php } ?>           
          </tr>

          <tr>
          <?php if($acceptbyData['profile_type']=='personal'){ ?>
            <td align="left" valign="top" style="padding: 8px;">
            
            <?php if($acceptbyData['driving_license']=='yes'){ ?>
            <?=$this->lang->line('TYPES_DRIVING_LICENSE')?> : <strong><?=get_driving_license_types($acceptbyData['driving_license_types'])?></strong><br>
            <?=$this->lang->line('TYPE_OF_DRIVING_LICENSE')?> : <strong><?=get_license_type($acceptbyData['license_type'])?></strong><br>
            <?=$this->lang->line('EXPIRY_DATE')?> : <strong><?=date("M j, Y", strtotime($acceptbyData['license_expir']))?></strong>
            <?php } ?>
            </td>
          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('NAME_OF_REPRESENTER')?> : <strong><?=$acceptbyData['representer']?></strong></td>          
          <?php } ?> 
          <?php if($requestbyData['profile_type']=='personal') { ?>
            <td align="left" valign="top" style="padding: 8px;">
            
            <?php if($requestbyData['driving_license']=='yes'){ ?>
            <?=$this->lang->line('TYPES_DRIVING_LICENSE')?> : <strong><?=get_driving_license_types($requestbyData['driving_license_types'])?></strong><br>
            <?=$this->lang->line('TYPE_OF_DRIVING_LICENSE')?> : <strong><?=get_license_type($requestbyData['license_type'])?></strong><br>
            <?=$this->lang->line('EXPIRY_DATE')?> : <strong><?=date("M j, Y", strtotime($requestbyData['license_expir']))?></strong>
            <?php } ?>
            </td>

          <?php } else { ?>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('NAME_OF_REPRESENTER')?> : <strong><?=$requestbyData['representer']?></strong></td>          
          <?php } ?>           
          </tr>          
        </table>
        
      </div>
    </div>
    <?php $cat=getCatIdByVehicleId($vehicle_id);?>
    <div class="row spaceTop">
      <div class="col-sm-12">
      <?php 
      if($request_type=='swap'){ ?>


      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
          <tr>
            <th scope="col" style="padding:7px;"><h4><?=$this->lang->line('VEHICLE_SPECIFICATION')?></h4> </th>
            <th scope="col" style="padding:7px;"><h4><?=$this->lang->line('VEHICLE_SPECIFICATION')?></h4> </th>
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('VEHICLE_NAME')?> : <strong><?=$vehicleData->name?></strong> ( <?=getVehicleCategoryName($vehicleData->vehicle_category_id)?></strong> )
            </td>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('VEHICLE_NAME')?> : <strong><?=$vehicleRequestData->name?></strong> ( <?=getVehicleCategoryName($vehicleRequestData->vehicle_category_id)?> )
            </td>
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('VEHICLE_MODEL')?> & <?=$this->lang->line('COLOR')?> : <strong><?=getVehicleModelName($vehicleData->vehicle_models_id)?></strong> ( <strong><?=$vehicleData->vehicle_color?></strong> )
            </td>
            <td align="left" valign="top" style="padding: 8px;">
            <?=$this->lang->line('VEHICLE_MODEL')?> & <?=$this->lang->line('COLOR')?> : <strong><?=getVehicleModelName($vehicleRequestData->vehicle_models_id)?></strong> ( <strong><?=$vehicleRequestData->vehicle_color?></strong> )
            </td>
          </tr>

          <?php if($cat!=2) { ?>
          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('AUTO_OR_MANUAL')?> : <strong><?=$vehicleData->automatic?></strong> ( <strong><?=$vehicleData->type_of_fuel?></strong> )
            </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AUTO_OR_MANUAL')?> : <strong><?=$vehicleRequestData->automatic?></strong> ( <strong><?=$vehicleRequestData->type_of_fuel?></strong> )
            </td>
          </tr> 

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('COUNTRY_OF_REGISTRATIOB')?> & <?=$this->lang->line('DATE')?>: <strong><?=getCountryName($vehicleData->reg_country)?></strong> , <strong><?=date("M j, Y", strtotime($vehicleData->registration_date))?></strong>
            </td>
            <td align="left" valign="top" style="padding: 8px;">
            <?=$this->lang->line('COUNTRY_OF_REGISTRATIOB')?> & <?=$this->lang->line('DATE')?> : <strong><?=$vehicleRequestData->reg_country?></strong> , <strong><?=date("M j, Y", strtotime($vehicleRequestData->registration_date))?></strong>
            </td>
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('PLAQUE_NUMBER')?> :<strong><?=$vehicleData->rc_certificate?></strong></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('PLAQUE_NUMBER')?> :<strong><?=$vehicleRequestData->rc_certificate?></strong></td>
          </tr>
          <?php } ?>
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('TECHNICAL_INSPECTION')?> : <strong>-------</strong></td>
            <td  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('TECHNICAL_INSPECTION')?> : <strong>-------</strong></td>
           </tr>
           <?php if($cat!=2) { ?>
           <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('INSURANCE')?> : 
             <?php $insurancef=$vehicleData->insurance;
              if($insurancef=='yes'){ ?>
            <?=$vehicleData->assurance_name?> , <?=$vehicleData->assurance_type?> 
            <?php } else { echo '.....'; }  ?>
            </td>
            <td  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('INSURANCE')?> : 
            <?php $insurance=$vehicleRequestData->insurance;
              if($insurance=='yes'){ ?>
                 <?=$vehicleRequestData->assurance_name?> , <?=$vehicleRequestData->assurance_type?></td>

              <?php } else { echo '.....'; } ?>
           </tr>
            <?php } ?>
           <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('DO_YOUR_VEHICLE_FAULT')?> : <strong>------</strong></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('DO_YOUR_VEHICLE_FAULT')?> : <strong>------</strong></td>
            </tr>        
        </table>

        


      <?php } else { ?>


           <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
          <tr>
            <th scope="col" style="padding:7px;"><h4><?=$this->lang->line('VEHICLE_SPECIFICATION')?></h4> </th>
            
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('VEHICLE_NAME')?> : <strong><?=$vehicleData->name?></strong> ( <?=getVehicleCategoryName($vehicleData->vehicle_category_id)?></strong> )
            </td>
           
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('VEHICLE_MODEL')?> & <?=$this->lang->line('COLOR')?> : <strong><?=getVehicleModelName($vehicleData->vehicle_models_id)?></strong> ( <strong><?=$vehicleData->vehicle_color?></strong> )
            </td>
           
          </tr>

          <?php if($cat!=2) { ?>
          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('AUTO_OR_MANUAL')?> : <strong><?=$vehicleData->automatic?></strong> ( <strong><?=$vehicleData->type_of_fuel?></strong> )
            </td>
            
          </tr> 

          <tr>
            <td align="left" valign="top" style="padding: 8px;width:50%">
            <?=$this->lang->line('COUNTRY_OF_REGISTRATIOB')?> & <?=$this->lang->line('DATE')?>: <strong><?=getCountryName($vehicleData->reg_country)?></strong> , <strong><?=date("M j, Y", strtotime($vehicleData->registration_date))?></strong>
            </td>
           
          </tr>

          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('PLAQUE_NUMBER')?> :<strong><?=$vehicleData->rc_certificate?></strong></td>
            
          </tr>
          <?php } ?>
          <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('TECHNICAL_INSPECTION')?> : <strong>-------</strong></td>
            
           </tr>

           <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('DO_YOUR_VEHICLE_FAULT')?> : <strong>------</strong></td>
           
            </tr>        
        </table>
        <div style="clear: both; height: 30px"></div>
        <?php } ?>
        

        <div style="clear: both; height: 30px"></div>


        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
        <tr>
            <th width="220" align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('RECOMMENDED')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_RIDE')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_RIDE')?></th>
        </tr> 
         
        
        
         <tr>  
            <td align="left" valign="top" style="padding: 8px;"> How clean is the vehicle?</td>
            <td align="left" valign="top" style="padding: 8px;"> Low,  Medium,  High</td>
            <td align="left" valign="top" style="padding: 8px;"> Low,  Medium,  High</td>
            </tr> 
         <tr>
            <td align="left" valign="top" style="padding: 8px;">You have any extra materials?</td>
            <td align="left" valign="top" style="padding: 8px;">......</td>
            <td align="left" valign="top" style="padding: 8px;">......</td>
           </tr> 
        <?php if($request_vehicle_id>0) { 
            //$request_by_user_id= getUserIdByVehicleId($request_by);
            $cat2=getCatIdByVehicleId($request_vehicle_id);
          ?>
           <?php if($user_id==$request_by) { ?>
           <?php if($cat==3) { ?>
           <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Motercycle body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              </tr>
              </table>
            </td>
           </tr>           
           <?php } else { ?>
           <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Car body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              </tr>
              </table>
            </td>
           </tr>           
           <?php }  ?>

           <?php } else { ?>

            <?php if($cat2==3) { ?>
           <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Motercycle body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              </tr>
              </table>
            </td>
           </tr>           
           <?php } else { ?>
           <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Car body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              </tr>
              </table>
            </td>
           </tr>           
           <?php }  ?>
           <?php } ?>


        <?php } else { ?>
        <?php if($cat==1) { ?>
        <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Car body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/vehicle-empty.png')?>"></td>
              </tr>
              </table>
            </td>
        </tr>
        <?php } else if($cat==2) { ?>
               
                <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Cycle body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/cycle-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/cycle-empty.png')?>"></td>
              </tr>
              </table>
            </td>
        </tr>

        <?php } else { ?>
        <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">

              <div style="padding-bottom:10px;">Motercycle body situation?</div>
              
              <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              <td style="border:0px;"><img style="width:40%; margin:20px 30px" src="<?=base_url('skin/front/images/bike-empty.png')?>"></td>
              </tr>
              </table>
            </td>
        </tr>
        <?php } } ?>

        <?php  if($cat!=2) { ?>
        <tr>
             <td colspan="3" align="left" valign="top" style="padding: 8px;">
              <div style="padding-bottom:10px;">Fuel level</div>

               <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td style="border:0px;"><img style="width:30%; margin:10px 60px" src="<?=base_url('skin/front/images/fueltank.png')?>"></td>
              <td style="border:0px;"><img style="width:30%; margin:10px 60px" src="<?=base_url('skin/front/images/fueltank.png')?>"></td>
              </tr>
              </table>
            </td>
        </tr>
       <?php } ?>
        
        <tr>
            <td colspan="3" align="left" valign="top" style="padding: 8px;">
             <div style="padding-bottom:20px;">Counter Km </div>
             <table style="border:0px" width="100%" border="0" cellspacing="0" cellpadding="10" class="table table-bordered">
              <tr style="border:0px">
              <td>
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              </td>
              <td style="border:0px; padding-left: 120px">
              
                
              <img width="30px;" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              <img width="30px" src="<?=base_url('skin/front/images/check-km-box.png')?>"> 
              </td>
              </tr>
              </table>
            </td>
           
        </tr>
        </table>
        <!--  <div style="clear: both; height: 30px"></div> -->

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
        <?php  if($cat!=2) { ?>
        <tr>
            <th width="200" align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_RIDE')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('CHECK_LIST')?></th> 
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_RIDE')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('CHECK_LIST')?></th>
        </tr> 
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_IDENTIFICATION')?> </td>
            <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_PICK_UP')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
         </tr> 
         
         <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_LICENSE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_DROP_OFF')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
          </tr> 
          

         <tr>  
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_DOCUMENTATION')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_KMS')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            </tr> 
         <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_VEHICLE_TYPE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_FUEL_LEVEL')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
           </tr> 
         <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_PLAQUE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_CLEANING')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_TECHNICAL_INSPECTION')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_MISSING_EXTRA_MATERIAL')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_INSURANCE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_PENALIZATION')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_EXTRA_MATERIAL')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_DAMAGE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_DAMAGE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td colspan="2"  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_OTHER')?> .....</td>
        </tr>
        <tr>
            <td colspan="2"  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_OTHER')?> .....</td>
            <td colspan="2"  align="left" valign="top" style="padding: 8px;"></td>
           
        </tr>
        <?php  } else { ?>


              <tr>
            <th width="200" align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_RIDE')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('CHECK_LIST')?></th> 
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_RIDE')?></th>
            <th align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('CHECK_LIST')?></th>
        </tr> 
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_IDENTIFICATION')?> </td>
            <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_PICK_UP')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
         </tr> 
         
         <tr>
             <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_DOCUMENTATION')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_DROP_OFF')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
          </tr> 
          

          
         <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_VEHICLE_TYPE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_CLEANING')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
           </tr> 
        
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_EXTRA_MATERIAL')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_MISSING_EXTRA_MATERIAL')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_INSURANCE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_PENALIZATION')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_DAMAGE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_DAMAGE')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
        </tr>
        <tr>
            
            <td colspan="2"  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('BEFORE_OTHER')?> .....</td>
            <td colspan="2"  align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_OTHER')?> .....</td>
        </tr>
        <tr>  
             <td colspan="2"  align="left" valign="top" style="padding: 8px;"></td>
            <td align="left" valign="top" style="padding: 8px;"><?=$this->lang->line('AFTER_KMS')?></td>
             <td align="left" valign="top" style="padding: 8px;"> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('YES')?> <img src="<?=base_url('skin/front/images/check-box-empty.png')?>"> <?=$this->lang->line('NO')?> </td>
            </tr>


        <?php  }  ?>

        </table>
         <div style="clear: both; height: 30px"></div>


        <div>
        <?=$this->lang->line('SPECIAL_MENTIONS')?> : <?=getAdvanceOption('special_description')?>
        </td>
          
        </div>
      </div>
    </div>
    <div class="row spaceTop">
      <div class="col-md-4" style="float:left ; width:45%">
        <div class="sign1">
          <div class="sign2">
          <?=getCustomerName($accept_by)?><br>
          <?=$this->lang->line('GIVER_SIGNATURE')?></div>
        </div>
      </div>
      <div class="col-md-4" style="float:right ;">
        <div class="sign1">

          <div class="sign2">
          <?=getCustomerName($request_by)?><br>
          <?=$this->lang->line('RECEIVER_SIGNATURE')?></div>
        </div>
      </div>
    </div>
   
    
  </div>
</div>
</body>
</html>

