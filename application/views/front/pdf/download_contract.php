<html>
<body>

<div class="main_container" style="margin: auto; width: 100%; border: 1px solid #e7e7e7;">
<div class="top_bg" style="background: #fff; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); display: inline-block; width: 100%; padding: 8px 0;"}>
<?php
// echo '<pre>';
// print_r($businessSettings);
// echo '</pre>';
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td align="left" style="float: left; margin: 0 0 0 14px;">
      &nbsp;&nbsp;<img src="<?=base_url('skin/front/images/logo.png')?>">
      
      </td>
      <td align="right" style="float: right;font-family: arial;margin: 16px 13px;font-size: 20px;">
      
      Service Provider Contract Form&nbsp;&nbsp;&nbsp;
      
      </td>
    </tr>
  </tbody>
</table>



	
</div>

<div style="margin: 0 20px;">

	<div>
	
	<h3 style="font-family: arial;font-size: 24px;">Form SP-102</h3>
	
		<p style="font-family: arial;line-height: 27px;">This form is the sent after we receive form 101 and shoulde be sent automatically with the information that we want to give the prospect franchisee which may vary from service to service.
</p>
	
	
<div style="font-family: arial;margin: auto;width: 35%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Request Information	
		
	</div>
	
	
<table style="font-family: arial;border: 1px solid #e4e4e4;" width="100%" cellspacing="0" cellpadding="7" border="0">
  <tbody>
    <tr>
      <td colspan="2" style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Services Category :&nbsp;&nbsp;<span style="font-weight: normal">Heating &amp; Air-conditioning</span></h3></td>
      </tr>
    <tr>
      <td width="53%" style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;">     
      
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="19%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Name :</h3></td>
      <td width="81%" align="left"><?=!empty($userDetails->username)?$userDetails->username:""?></td>
    </tr>
  </tbody>
</table>
      
</td>
      <td width="47%" style="border-bottom: 1px solid #e4e4e4;">
      
      
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="44%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Phone No :</h3></td>
      <td width="56%"><?=!empty($userDetails->mobile)?$userDetails->mobile:""?></td>
    </tr>
  </tbody>
</table>

      
      </td>
    </tr>
    <tr>
      <td style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;">     
      
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="22%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Gender :</h3></td>
      <td width="78%"><?=!empty($userDetails->gender)?$userDetails->gender:""?></td>
    </tr>
  </tbody>
</table>
</td>
<td style="border-bottom: 1px solid #e4e4e4;">     
      
      
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="44%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Date of Birth :</h3></td>
      <td width="56%"><?=!empty($userDetails->dob)?$userDetails->dob:""?></td>
    </tr>
  </tbody>
</table>
      
      
</td>
    </tr>
    <tr>
      <td style="border-right: 1px solid #e4e4e4;">
      
      
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
  
    <tr>
      <td colspan="2">
      <h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Email :<span style="font-weight: normal">&nbsp;&nbsp;<?=!empty($userDetails->email)?$userDetails->email:""?></span></h3></td>
      </tr>
    
    
  </tbody>
</table>
      
      
      </td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Request date :</h3></td>
            <td><?=!empty($businessDetails->request_date)?$businessDetails->request_date:""?></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
	
	<br><br>
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Personal & Business Information	
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
            <td colspan="2">

              <table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
              <td width="19%" >Address :</td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->site_location)?$userDetails->site_location:""?></td>
          </tr>
        </tbody>
      </table> 
            </td>
          </tr>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          
          <tr>
            <td width="38%">Apt/Unit No :</td>
            <td width="62%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->aptno)?$userDetails->aptno:""?></td>
          </tr>
          <tr>
            <td>Street No :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->street_number)?$userDetails->street_number:""?></td>
            </tr>
          <tr>
            <td>Street Name :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->street_name)?$userDetails->street_name:""?></td>
            </tr>
          <tr>
            <td>Type :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->street_type)?$userDetails->street_type:""?></td>
            </tr>
          <tr>
            <td>Direction :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->direction)?$userDetails->direction:""?></td>
            </tr>
          
          
          
          </tbody>
        </table></td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>

            <td>City : </td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->city)?$userDetails->city:""?></td>
            
            </tr>
            <tr>
            <td>State/Prov :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->state)?$userDetails->state:""?></td>
            </tr>
            <tr>
              <td>Country : </td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->country)?getCountryName($userDetails->country):""?></td>
            </tr>

            <tr>
              <td>Zip/Postal :</td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($userDetails->postal_code)?$userDetails->postal_code:""?></td>
              </tr>
            <tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
 
    <tr>
      <td height="180">&nbsp;</td>
    </tr>

</table>

		
		
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Business Information	
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
       <td colspan="2">
            <table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
                <tbody>
                  <tr>
                      <td width="22%" >Business Name :</td>
                      <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_name)?$businessDetails->business_name:""?></td>
                  </tr>          
                </tbody>
            </table> 
        </td>
    </tr>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>          
          <tr>
            <td width="39%">Apt/Unit No :</td>
            <td width="61%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_aptno)?$businessDetails->business_aptno:""?></td>
          </tr>         
          <tr>
            <td>Street Name :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_street_name)?$businessDetails->business_street_name:""?></td>
          </tr>          
          <tr>
            <td>Direction :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_direction)?$businessDetails->business_direction:""?></td>
          </tr>
          <tr>
            <td>State/Prov :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_state)?getStateName($businessDetails->business_state):""?></td>
          </tr>

          <tr>
              <td>Zip/Postal  :</td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_zip)?$businessDetails->business_zip:""?></td>
          </tr>
          
          <tr>
            <td width="40%">Business Fax :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_fax)?$businessDetails->business_fax:""?></td>
          </tr>
        </tbody>
      </table></td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>          
          <tr>
             <td>Street No :</td>
             <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_street_no)?$businessDetails->business_street_no:""?></td>
          </tr>
          <tr>
             <td>Street Type :</td>
             <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_street_type)?$businessDetails->business_street_type:""?></td>
          </tr>                 
          <tr>
            <td>City :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_city)?$businessDetails->business_city:""?></td>
          </tr>        
           <tr>
              <td>Country :</td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_country)?getCountryName($businessDetails->business_country):""?></td>
            </tr>

            <tr>
              <td>Business Email : </td>
              <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_email)?$businessDetails->business_email:""?></td>            
            </tr>

          <tr>
            <td width="40%">Tel No :</td>
            <td width="60%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->business_mobile)?$businessDetails->business_mobile:""?></td>
          </tr> 

          
          
          </tbody>
          </table></td>
        </tr>
  
    </tbody>
</table>

<br><br>
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Service Information & Charges
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
              <tbody>
                <tr>
                  <td width="38%">Start Date :</td>
                  <td width="62%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->start_date)?$businessSettings->start_date:""?></td>
                  </tr>
                <tr>
                  <td>Service :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=getServicesName(!empty($businessSettings->service_category_code)?$businessSettings->service_category_code:"0")?></td>
                  </tr>
                <tr>
                  <td>Code :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_category_code)?$businessSettings->service_category_code:""?></td>
                  </tr>
                </tbody>
              </table></td>
            <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
              <tbody>
                <tr>
                  <td width="40%">Operator No :</td>
                  <td width="60%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->operator_no)?$businessSettings->operator_no:""?></td>
                  </tr>
                <tr>
                  <td>Zipcodes :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_zip_code)?$businessSettings->service_zip_code:""?></td>
                </tr>

                <tr>
                  <td width="37%">Currency  :</td>
                  <?php
                  $currency_id = !empty($businessSettings->currency)?$businessSettings->currency:"";
                  $currencySymbol = getPriceSymbolById($currency_id);
                  ?>
                  <td width="63%" style="border-bottom: 1px solid #adaaaa;"><?=getCurrencySignById($currency_id) .' ('.$currencySymbol.') ';?></td>
                </tr>
                </tbody>
              </table></td>
          </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>



<br><br>
	<div style="font-family: arial;margin: auto;width: 30%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	H.O. Charges
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
              <tbody>
                <tr>
                  <td width="45%">H.O. Fees (%) :</td>
                  <td width="55%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_fees)?$businessSettings->ho_fees.'%':""?></td>
                  </tr>
                <tr>
                  <td>Per Order Charges :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_per_order_charge)?$currencySymbol.$businessSettings->ho_per_order_charge:""?></td>
                  </tr>
                <tr>
                  <td>Advertising % :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_advertising)?$businessSettings->ho_advertising.'%':""?></td>
                  </tr>
                </tbody>
              </table></td>
            <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
              <tbody>
                <tr>
                  <td width="40%">Fix weekly charges :</td>
                  <td width="60%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_fix_weekly_charges)?$currencySymbol.$businessSettings->ho_fix_weekly_charges:""?></td>
                  </tr>
                <tr>
                  <td>Other Charges :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_other_charge)?$currencySymbol.$businessSettings->ho_other_charge:""?></td>
                </tr>
                <tr>
                  <td>Credit/Debit Card Charges :</td>
                  <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->ho_credit_debit_card_charges)?$businessSettings->ho_credit_debit_card_charges.'%':""?></td>
                </tr>
                </tbody>
              </table></td>
          </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
 
    <tr>
      <td height="100">&nbsp;</td>
    </tr>

</table>
	<div style="font-family: arial;margin: auto;width: 30%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Taxes
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Tax :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->tax)?$businessSettings->tax.'%':""?></td>
            </tr>
            <tr>
            <td>Tax name :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->tax_name)?$businessSettings->tax_name:""?></td>
            </tr>
          <tr>
            <td>Other Information :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->tax_other_info)?$businessSettings->tax_other_info:""?></td>
          </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>

<br><br>

	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Service Charge (Working Hours)
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Service Charge:</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_charge)?$currencySymbol.$businessSettings->service_charge:""?></td>
            </tr>
          <tr>
            <td>Service Duration :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_duration)?getDuration($businessSettings->service_duration,'minute'):""?></td>
          </tr>
          <tr>
            <td>Charges after that :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_charges_after)?$currencySymbol.$businessSettings->service_charges_after:""?></td>
          </tr>
          <tr>
            <td>For how long :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_duration_after)?getDuration($businessSettings->service_duration_after,'hours'):""?></td>
          </tr>
          <tr>
            <td>Charges after <?=!empty($businessSettings->service_duration_after)?getDuration($businessSettings->service_duration_after,'hours'):""?> :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_additional_charge)?$currencySymbol.$businessSettings->service_additional_charge:""?></td>
          </tr>

          <tr>
            <td>For how long :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_additional_hours)?getDuration($businessSettings->service_additional_hours,'hours'):"";?></td>
          </tr>

          <tr>
            <td>Hourly charges after that :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_hourly_charge)?$currencySymbol.$businessSettings->service_hourly_charge:""?></td>
          </tr>

          
          <tr>
            <td>Additional worker charges (Hourly) : </td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->service_additional_worker)?$currencySymbol.$businessSettings->service_additional_worker:""?></td>
          </tr>

          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>
	
	<br><br>

	<div style="font-family: arial;margin: auto;width: 70%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">	Weekend & After Working Hours Charges.</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Service Charge:</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_charge)?$currencySymbol.$businessSettings->weekend_charge:""?></td>
            </tr>
          <tr>
            <td>Service Duration :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_duration)?getDuration($businessSettings->weekend_duration,'minute'):""?></td>
          </tr>
          <tr>
            <td>Charges after that :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_charges_after)?$currencySymbol.$businessSettings->weekend_charges_after:""?></td>
          </tr>
          <tr>
            <td>For how long :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_duration_after)?getDuration($businessSettings->weekend_duration_after,'hours'):""?></td>
          </tr>
          <tr>
            <td>Charges after <?=!empty($businessSettings->weekend_duration_after)?getDuration($businessSettings->weekend_duration_after,'hours'):""?> :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_additional_charge)?$currencySymbol.$businessSettings->weekend_additional_charge:""?></td>
          </tr>

          <tr>
            <td>For how long :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_additional_hours)?getDuration($businessSettings->weekend_additional_hours,'hours'):"";?></td>
          </tr>

          <tr>
            <td>Hourly charges after that :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_hourly_charge)?$currencySymbol.$businessSettings->weekend_hourly_charge:""?></td>
          </tr>

          
          <tr>
            <td>Additional worker charges (Hourly) : </td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessSettings->weekend_additional_worker)?$currencySymbol.$businessSettings->weekend_additional_worker:""?></td>
          </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>	

<br><br>

<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
    
  Car Specification  
    
  </div>
  
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Driver Licence : </td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->driver_licence)?$businessDetails->driver_licence:""?></td>
            
            </tr>
          
          <tr>
           <td>Model :</td>
           <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->car_model)?$businessDetails->car_model:""?></td>
         </tr>
         
          

      <tr>
           <td>Color :</td>
           <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->car_color)?$businessDetails->car_color:""?></td>
         </tr>
         
          <tr>
            <td width="40%">Car Picture :</td>
            <td style="border-bottom: 1px solid #adaaaa;">
              <?php
              if(!empty($businessDetails->car_picture))
              {
                echo '<img src="'.base_url($businessDetails->car_picture).'" width="50" height="50">';
              }
              else
              {
                echo '';
              }
              ?></td>
          </tr>
     
        </tbody>
      </table>
    </td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          
         <tr>
            <td width="39%">Made :</td>
            <td width="61%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->car_made)?$businessDetails->car_made:""?></td>
          </tr>

          <tr>
        <td>Year :</td>
        <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->car_year)?$businessDetails->car_year:""?></td>
      </tr>

         <tr>
            <td>Licence Plate No :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->car_number)?$businessDetails->car_number:""?></td>
          </tr>       
      </tbody>
      </table></td>
    </tr>
  
    </tbody>
</table>

<br><br>

<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
    
  Bank Information 
    
  </div>
  
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Bank Name : </td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->bank_name)?$businessDetails->bank_name:""?></td>
            
            </tr>
          
          <tr>
           <td>Bank Code :</td>
           <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->bank_code)?$businessDetails->bank_code:""?></td>
         </tr>
         
          

      <tr>
           <td>SWIFT No :</td>
           <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->swift_no)?$businessDetails->swift_no:""?></td>
         </tr>
         
          <tr>
            <td width="40%">Account Type :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->account_type)?$businessDetails->account_type:""?></td>
          </tr>
     
        </tbody>
      </table>
    </td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          
         <tr>
            <td width="39%">Bank Address :</td>
            <td width="61%" style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->bank_address)?$businessDetails->bank_address:""?></td>
          </tr>

          <tr>
        <td>Account No :</td>
        <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->account_no)?$businessDetails->account_no:""?></td>
      </tr>

         <tr>
            <td>Transition No :</td>
            <td style="border-bottom: 1px solid #adaaaa;"><?=!empty($businessDetails->transition_no)?$businessDetails->transition_no:""?></td>
          </tr>       
      </tbody>
      </table></td>
    </tr>
  
    </tbody>
</table>

<br><br><br><br>
	
	
	
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
  <tbody>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Your Signature</h3></td>
    </tr>
  </tbody>
</table>

	
<br><br><br><br>		


	

	</div>	
	
</div>




</div>

</body>
</html>
