
<html>


<body>

<div class="main_container" style="margin: auto; width: 100%; border: 1px solid #e7e7e7;">
<div class="top_bg" style="background: #fff; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); display: inline-block; width: 100%; padding: 8px 0;"}>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td align="left" style="float: left; margin: 0 0 0 14px;">
      &nbsp;&nbsp;<img src="https://markupdesigns.info/firstchoice/skin/front/images/logo.png">
      
      </td>
      <td align="right" style="float: right;font-family: arial;margin: 16px 13px;font-size: 20px;">
      
      Service Provider Contract Form&nbsp;&nbsp;&nbsp;
      
      </td>
    </tr>
  </tbody>
</table>



	
</div>

<div style="margin: 0 20px;">

	<div>
	
	<h3 style="font-family: arial;font-size: 24px;">Form SP-102</h3>
	
		<p style="font-family: arial;line-height: 27px;">This form is the sent after we receive form 101 and shoulde be sent automatically with the information that we want to give the prospect franchisee which may vary from service to service.
</p>
	
	
<div style="font-family: arial;margin: auto;width: 35%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Request Information	
		
	</div>
	
	
<table style="font-family: arial;border: 1px solid #e4e4e4;" width="100%" cellspacing="0" cellpadding="7" border="0">
  <tbody>
    <tr>
      <td colspan="2" style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Services Category :&nbsp;&nbsp;<span style="font-weight: normal">Heating &amp; Air-conditioning</span></h3></td>
      </tr>
    <tr>
      <td width="53%" style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;">     
      
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="22%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Name :</h3></td>
      <td width="78%">Atul Kumar</td>
    </tr>
  </tbody>
</table>
      
</td>
      <td width="47%" style="border-bottom: 1px solid #e4e4e4;">
      
      
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Phone No :</h3></td>
      <td>8945632178</td>
    </tr>
  </tbody>
</table>

      
      </td>
    </tr>
    <tr>
      <td style="border-bottom: 1px solid #e4e4e4;border-right: 1px solid #e4e4e4;">     
      
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="22%"><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Gender :</h3></td>
      <td width="78%">Male</td>
    </tr>
  </tbody>
</table>
</td>
<td style="border-bottom: 1px solid #e4e4e4;">     
      
      
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Date of Birth :</h3></td>
      <td>18 Nov 1984</td>
    </tr>
  </tbody>
</table>
      
      
</td>
    </tr>
    <tr>
      <td style="border-right: 1px solid #e4e4e4;">
      
      
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
  
    <tr>
      <td colspan="2">
      <h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Email :<span style="font-weight: normal">&nbsp;&nbsp;info@markupdesigns.com</span></h3></td>
      </tr>
    
    
  </tbody>
</table>
      
      
      </td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td><h3 style="font-family: arial;font-size: 16px; padding: 0;margin: 0;">Request date :</h3></td>
            <td>2018-11-24 06:12:56</td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
	
	<br><br><br>
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Personal & Business Information	
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td width="38%">Apt/Unit No :</td>
            <td width="62%" style="border-bottom: 1px solid #adaaaa;">1405</td>
          </tr>
         <tr>
            <td>Date of Birth :</td>
            <td style="border-bottom: 1px solid #adaaaa;">18 Nov 1984</td>
          </tr>
          <tr>
            <td>Type :</td>
            <td style="border-bottom: 1px solid #adaaaa;">Business</td>
          </tr>
          <tr>
            <td>State/Prov :</td>
            <td style="border-bottom: 1px solid #adaaaa;">U.P</td>
          </tr>
          <tr>
            <td>Street No :</td>
            <td style="border-bottom: 1px solid #adaaaa;">0121 35689712</td>
          </tr>
      <tr>
            <td>Street Name :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
          </tr>
        </tbody>
      </table></td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td width="40%">Home Tel No :</td>
            <td width="60%" style="border-bottom: 1px solid #adaaaa;">25605</td>
          </tr>
          <tr>
            <td>Direction :</td>
            <td style="border-bottom: 1px solid #adaaaa;">Noida</td>
          </tr>
          <tr>
            <td>Zip/Postal :</td>
            <td style="border-bottom: 1px solid #adaaaa;">1100021</td>
          </tr>
          <tr>
            <td>City : </td>
            <td style="border-bottom: 1px solid #adaaaa;">Noida</td>
            
          </tr>
<tr>
            <td>Country : </td>
            <td style="border-bottom: 1px solid #adaaaa;">India</td>
            
            </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    </tbody>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
 
    <tr>
      <td height="150">&nbsp;</td>
    </tr>

</table>

		
		
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Business Information	
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="95%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td>Name :</td>
            <td style="border-bottom: 1px solid #adaaaa;">Markup Designs Pvt Ltd</td>
          </tr>
          <tr>
            <td width="39%">Apt/Unit No :</td>
            <td width="61%" style="border-bottom: 1px solid #adaaaa;">1405</td>
          </tr>
         <tr>
           <td>Street Type :</td>
           <td style="border-bottom: 1px solid #adaaaa;">Business</td>
         </tr>
          <tr>
            <td>State/Prov :</td>
            <td style="border-bottom: 1px solid #adaaaa;">U.P</td>
          </tr>
          <tr>
            <td>Street No :</td>
            <td style="border-bottom: 1px solid #adaaaa;">0121 35689712</td>
          </tr>
      <tr>
        <td>Street Name :</td>
        <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxxx</td>
      </tr>
      <tr>
        <td>City :</td>
        <td style="border-bottom: 1px solid #adaaaa;">Noida</td>
      </tr>
      <tr>
        <td>Country :</td>
        <td style="border-bottom: 1px solid #adaaaa;">India</td>
      </tr>
        </tbody>
      </table></td>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
        <tr>
          <td>Address :</td>
          <td style="border-bottom: 1px solid #adaaaa;">Markup Designs Pvt Ltd</td>
        </tr>
          <tr>
            <td width="40%">Tel No :</td>
            <td width="60%" style="border-bottom: 1px solid #adaaaa;">0121 35689712</td>
          </tr>
          <tr>
            <td width="40%">Business Fax :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxxxx</td>
          </tr>
          <tr>
            <td>Direction :</td>
            <td style="border-bottom: 1px solid #adaaaa;">Noida</td>
          </tr>
          <tr>
            <td>Zip/Postal  :</td>
            <td style="border-bottom: 1px solid #adaaaa;">1100021</td>
          </tr>
          <tr>
            <td>Personal Email : </td>
            <td style="border-bottom: 1px solid #adaaaa;">info@markupdesigns.com</td>
            
            </tr>
          <tr>
            <td>Car Specif</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxxxx</td>
          </tr>
          </tbody>
      </table></td>
    </tr>
  
    </tbody>
</table>


<br><br><br>
	<div style="font-family: arial;margin: auto;width: 50%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Service Information & Charges
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td width="28%">Start Date :</td>
            <td width="72%" style="border-bottom: 1px solid #adaaaa;">YYYY-MM-DD</td>
            </tr>
          <tr>
            <td>Service Category :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Service Code :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Operator No :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Service Zipcodes :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>

<br><br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 
    <tr>
      <td height="180">&nbsp;</td>
    </tr>

</table>
	<div style="font-family: arial;margin: auto;width: 30%;background: #212121;text-align: center;color: #fff;padding: 7px 0;border-radius: 30px;font-size: 20px;margin-bottom: 40px;">
		
	Promtions
		
	</div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="50%" valign="top"><table style="font-family: arial;font-size: 15px;" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tbody>
          <tr>
            <td width="31%">Coupons :</td>
            <td width="69%" style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Discount Promotions :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>FC. Bonus Point % :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Minimum $ to use points :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Service Zipcodes :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
            </tr>
          <tr>
            <td>Point expiry in years :</td>
            <td style="border-bottom: 1px solid #adaaaa;">xxxxxxxx</td>
          </tr>
          </tbody>
        </table></td>
    </tr>
    </tbody>
</table>

	

	</div>	
	
</div>




</div>

</body>
</html>
