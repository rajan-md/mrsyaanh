<div class="container gridSysTop">
  <div class="row">
    <div class="col-sm-12">
      <div class="title">
        <h3><?=$this->lang->line('DASHBOARD');?></h3>
      </div>
    </div>
  </div>
  <div class="row gridSys">
      <?php $userRole=getLoginUserRole(); ?>    
      <div class="col-sm-4"> 
        <a href="<?=base_url('my-account')?>">
          <figure class="das-ico"> <i class="far fa-user"></i>
            <figcaption><?=$this->lang->line('MY_ACCOUNT');?></figcaption>
          </figure>
        </a> 
      </div>
      <?php
      if($userRole==2)
      {
        ?>
        <div class="col-sm-4"> <a href="<?=base_url('my-bookings')?>">
          <figure class="das-ico"> <i class="far fa-list-alt"></i>
            <figcaption><?=$this->lang->line('MY_BOOKING');?></figcaption>
          </figure>
          </a>
        </div>
        <div class="col-sm-4"> <a href="<?=base_url('wishlist')?>">
          <figure class="das-ico"> <i class="far fa-heart"></i>
            <figcaption><?=$this->lang->line('WISHLIST');?></figcaption>
          </figure>
          </a>
        </div>
        <div class="col-sm-4"> <a href="<?=base_url('orders')?>">
          <figure class="das-ico"> <i class="far fa-file-alt"></i>
            <figcaption><?=$this->lang->line('ORDERS');?></figcaption>
          </figure>
          </a>
        </div>
        <?php
      }
      if($userRole==5)
      {
      ?>
      <div class="col-sm-4"> <a href="<?=base_url('service-request')?>">
        <figure class="das-ico"> <i class="fa fa-file-alt"></i>
          <figcaption><?=$this->lang->line('MY_SERVICE');?></figcaption>
        </figure>
        </a>
      </div>
      <div class="col-sm-4"> <a href="<?=base_url('operators')?>">
        <figure class="das-ico"> <i class="fa fa-users"></i>
          <figcaption><?=$this->lang->line('MY_OPERATORS');?></figcaption>
        </figure>
        </a>
      </div>
    	<div class="col-sm-4"> <a href="<?=base_url('customer-request')?>">
      	<figure class="das-ico"> <i class="far fa-sun"></i>
      	<figcaption><?=$this->lang->line('CUSTOMER_REQUEST');?></figcaption>
      	</figure>
      	</a>
      </div>
      <div class="col-sm-4"> <a href="<?=base_url('contract-form-102')?>">
        <figure class="das-ico"> <i class="fa fa-file-alt"></i>
          <figcaption><?=$this->lang->line('CONTRACT_FORM');?></figcaption>
        </figure>
        </a>
      </div>
      <?php
      }
      if($userRole==8)
      {
      ?>
      <div class="col-sm-4"> <a href="<?=base_url('service-orders')?>">
        <figure class="das-ico"> <i class="far fa-sun"></i>
        <figcaption><?=$this->lang->line('ORDERS');?></figcaption>
        </figure>
        </a>
      </div>
      <?php
      }
      if($userRole==9)
      {
      ?>
      <div class="col-sm-4"> <a href="<?=base_url('jobs')?>">
        <figure class="das-ico"> <i class="far fa-sun"></i>
        <figcaption><?=$this->lang->line('MY_JOBS');?></figcaption>
        </figure>
        </a>
      </div>
      <?php
      }
      ?>
    <?php
    if($userRole!=5)
    {
      ?>
      <div class="col-sm-4"> <a href="<?=base_url('edit-profile')?>">
        <figure class="das-ico"> <i class="far fa-edit"></i>
          <figcaption><?=$this->lang->line('UPDATE_PROFILE');?></figcaption>
        </figure>
        </a>
      </div>     
      <?php
    }
    ?>
    <div class="col-sm-4"> <a href="<?=base_url('change-password')?>">
        <figure class="das-ico"> <i class="far fa-edit"></i>
          <figcaption><?=$this->lang->line('CHANGE_PASSWORD');?></figcaption>
        </figure>
        </a>
    </div>   
  </div>
</div>


