<?php //echo "<pre>"; print_r($servicesData); echo "</pre>"; ?>
    <div class="col-sm-8"> 

        <div class="dashboarCcontainer table-responsive">

        <table class="table  services-tbl" style="margin:0"> 

            <!-- <thead>

                <tr>

                  <th>Services Location</th>                      

                  <th>Request date</th>

                  <th>Status</th>                  

                </tr>

              </thead>  -->              

            <tbody>

            <?php if(!empty($servicesData)) {

            foreach ($servicesData as $services) { ?> 

            <tr>

                <th><?=$this->lang->line('SERVICE_CATEGORY'); ?></th>

                <td><?=getServicesName(isset($services->services_type)?$services->services_type:"")?></td> 

            </tr>

            <tr>

                <th><?=$this->lang->line('REQUEST_DATE');?></th> 

                <td class="text-left"><?=isset($services->request_date)?$services->request_date:""?></td>

            </tr>

            

            <?php 

            $business=!empty($services->business)?$services->business:'';

            if($business=='yes') { ?>

            <tr>

                <th><?=$this->lang->line('BUSINESS_NAME');?> </th>

                <td><?=isset($services->business_name)?$services->business_name:""?></td>

            </tr>

            <tr>

                <th><?=$this->lang->line('BUSINESS_TYPE');?> </th>

                <td><?=isset($services->business_type)?$services->business_type:""?></td>

            </tr> 

            <?php } ?>



            <?php 

            $license=!empty($services->license)?$services->license:'';

            if($license=='yes') { ?>

            <tr>

                <th><?=$this->lang->line('LICENSE_INFORMATION');?></th>

                <td><?=isset($services->license_details)?$services->license_details:""?></td>

            </tr>

             

            <?php } ?>



            <tr>

                <th><?=$this->lang->line('REQUEST_STATUS');?></th>

                <td class="text-left">

                    <?php $status=!empty($services->service_status)?$services->service_status:0; ?>
                    <span class="btn btn-<?php if($status==1) { echo 'success'; } else {

                        $status=2;

                        echo 'danger'; 



                       } ?>"><?=getStatus($status)?></span> 

                </td>

            </tr>

            <?php if($status==1) { ?>

            <tr>

                <th><?=$this->lang->line('REQUEST_CONTRACT');?> </th>

                <td><a target="_blank" href="<?=base_url('download-contract')?>"><span class="btn btn-info"><?=$this->lang->line('DOWNLOAD_CONTRACT');?> </span></a></td>

            </tr> 

            <?php } ?>



            

            <?php } }  else { ?>



        

        <div class="text-center"><p>service request is Empty</p></div>

        <div class="mb40"></div>

        <?php }  ?>

                </tbody>

            </table>

        </div>

        </div>

    