<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">
		<form method="post" autocomplete="off" enctype="multipart/form-data">	
        <input type="hidden" id="mobbox" name="mobbox">
		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="fname" class="form-control" id="fname" value="<?=set_value('fname'); ?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="lname" class="form-control" id="lname" value="<?=set_value('lname'); ?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="email" class="form-control" id="email" value="<?=set_value('email'); ?>" required="">

			</div>

		</div>

		<div class="form-group row personal_phonebox">

			<label class="col-md-3 profile_field"><?=$this->lang->line('MOBILE');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="mobile" class="form-control" id="phonecode"  minlength="10" maxlength="10" value="<?=set_value('phonecode'); ?>" required="">

			</div>

		</div>
		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('GENDER');?><span class="redstar">*</span></label>

			<div class="col-md-8">
				<label class="cus_radio"><input type="radio" name="gender" checked="checked" value="male"><?=$this->lang->line('MALE');?></label>
                <label class="cus_radio"><input type="radio" name="gender" value="female"><?=$this->lang->line('FEMALE');?></label>
                <label class="cus_radio"><input type="radio" name="gender" value="others"><?=$this->lang->line('OTHERS');?></label>
			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"></label>

			<div class="col-md-2">

				<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>">

			</div>

		</div>

	</form>

	</div>

</div>