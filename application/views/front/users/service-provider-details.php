<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">   
    <div class="col-md-6">
      <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('PERSONAL_DETAILS');?></h4>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('NAME');?> : </strong><?=!empty($userDetails->username)?$userDetails->username:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('EMAIL');?> : </strong><?=!empty($userDetails->email)?$userDetails->email:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('PHONE_NO');?> : </strong><?=!empty($userDetails->mobile)?$userDetails->mobile:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('GENDER');?> : </strong><?=!empty($userDetails->gender)?$userDetails->gender:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('DATE_OF_BIRTH');?> : </strong><?=!empty($userDetails->dob)?$userDetails->dob:""?></p></div>
          </div>
          <h5><?=$this->lang->line('HOME_ADDRESS');?></h5>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('ADDRESS');?> : </strong><?=!empty($userDetails->site_location)?$userDetails->site_location:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('APT_UNIT_NO');?> : </strong><?=!empty($userDetails->aptno)?$userDetails->aptno:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_NO');?> : </strong><?=!empty($userDetails->street_number)?$userDetails->street_number:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_NAME');?> : </strong><?=!empty($userDetails->street_name)?$userDetails->street_name:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_TYPE');?> : </strong><?=!empty($userDetails->street_type)?$userDetails->street_type:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('DIRECTION');?> : </strong><?=!empty($userDetails->direction)?$userDetails->direction:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('CITY');?> : </strong><?=!empty($userDetails->city)?$userDetails->city:""?></p></div>
          </div>

          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STATE_PROVINCE');?> : </strong><?=!empty($userDetails->state)?getStateName($userDetails->state):""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('COUNTRY');?> : </strong><?=!empty($userDetails->country)?getCountryName($userDetails->country):""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('ZIP_CODE');?> : </strong><?=!empty($userDetails->postal_code)?$userDetails->postal_code:""?></p></div>
          </div>  
      </div>
      <br>
      <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('BUSINESS_INFORMATION');?></h4>          
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('HAVE_BUSSINESS');?> : </strong><?=(!empty($businessDetails->business) && $businessDetails->business=='yes')?$this->lang->line('YES'):$this->lang->line('NO'); ?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('HAVE_LICENSE');?> : </strong><?=(!empty($businessDetails->license) && $businessDetails->license=='yes')?$this->lang->line('YES'):$this->lang->line('NO'); ?></p></div>
          </div>
          <?php if(!empty($businessDetails->business) && $businessDetails->business=='yes'){ ?>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('BUSINESS_NAME');?> : </strong><?=!empty($businessDetails->business_name)?$businessDetails->business_name:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('BUSINESS_TYPE');?> : </strong><?=!empty($businessDetails->business_type)?$businessDetails->business_type:""?></p></div>
          </div>
          <?php } ?>
          <?php if(!empty($businessDetails->license) && $businessDetails->license=='yes'){ ?>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('LICENSE_DETAILS');?> : </strong><?=!empty($businessDetails->license_details)?$businessDetails->license_details:''; ?></p></div>
          </div>
          <?php } ?>
          <div class="row">
            <div class="col-sm-12"><p><strong><?=$this->lang->line('BUSINESS_EMAIL');?> : </strong><?=!empty($businessDetails->business_email)?$businessDetails->business_email:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_TEL_NO');?> : </strong><?=!empty($businessDetails->business_mobile)?$businessDetails->business_mobile:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_FAX');?> : </strong><?=!empty($businessDetails->business_fax)?$businessDetails->business_fax:""?></p></div>
          </div>
          <h5><?=$this->lang->line('BUSSINESS_ADDRESS');?></h5>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('APT_UNIT_NO');?> : </strong><?=!empty($businessDetails->business_aptno)?$businessDetails->business_aptno:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_NO');?> : </strong><?=!empty($businessDetails->business_street_no)?$businessDetails->business_street_no:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_NAME');?> : </strong><?=!empty($businessDetails->business_street_name)?$businessDetails->business_street_name:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STREET_TYPE');?> : </strong><?=!empty($businessDetails->business_street_type)?$businessDetails->business_street_type:""?></p></div>
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('DIRECTION');?> : </strong><?=!empty($businessDetails->business_direction)?$businessDetails->business_direction:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('CITY');?> : </strong><?=!empty($businessDetails->business_city)?$businessDetails->business_city:""?></p></div>            
          </div>

          <div class="row">            
            <div class="col-sm-6"><p><strong><?=$this->lang->line('STATE_PROVINCE');?> : </strong><?=!empty($businessDetails->business_state)?getStateName($businessDetails->business_state):""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('COUNTRY');?> : </strong><?=!empty($businessDetails->business_country)?getCountryName($businessDetails->business_country):""?></p></div>
          </div>

          <div class="row">            
            <div class="col-sm-6"><p><strong><?=$this->lang->line('ZIP_CODE');?> : </strong><?=!empty($businessDetails->business_zip)?$businessDetails->business_zip:""?></p></div>
          </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="price_detail2 service-info clearfix">
          <h4><?=$this->lang->line('SERVICE_INFO_CHARGES');?></h4>

          <h5><?=$this->lang->line('SERVICE_INFO');?></h5>
          <div class="row">
                <div class="col-sm-12"><p><strong><?=$this->lang->line('SERVICE_CATEGORY');?> : </strong> <?=getServicesName(!empty($businessSettings->service_category_code)?$businessSettings->service_category_code:"0")?></p></div>    
          </div>
          <div class="row">
                <div class="col-sm-12"><p><strong><?=$this->lang->line('SERVICE_AREA_ZIPCODES');?> : </strong><?=!empty($businessSettings->service_zip_code)?$businessSettings->service_zip_code:""?></p></div>                
          </div>
          <div class="row">
                <div class="col-sm-6"><p><strong><?=$this->lang->line('REQUEST_DATE');?> : </strong> <?=!empty($businessDetails->request_date)?date('d-m-Y',strtotime($businessDetails->request_date)):""?></p></div>
                <div class="col-sm-6"><p><strong><?=$this->lang->line('START_DATE');?> : </strong> <?=!empty($businessSettings->start_date)?$businessSettings->start_date:""?></p></div>                
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('SERVICE_CODE');?> : </strong> <?=!empty($businessSettings->service_category_code)?$businessSettings->service_category_code:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('OPERATOR_NO');?> : </strong><?=!empty($businessSettings->operator_no)?$businessSettings->operator_no:""?></p></div>                
          </div>

          <h5><?=$this->lang->line('CURRENCY');?></h5>
          <?php
          $currency_id = !empty($businessSettings->currency)?$businessSettings->currency:"";
          $currency_symbol = getPriceSymbolById($currency_id);
          ?>
          <div class="row">
              <div class="col-sm-12"><p><strong><?=$this->lang->line('CURRENCY');?> : </strong><?=getCurrencySignById($currency_id)?> (<?=getPriceSymbolById($currency_id) ?>)</p></div>
          </div>

          <h5><?=$this->lang->line('HO_CHARGES');?></h5>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('HO_FEES');?> : </strong><?=!empty($businessSettings->ho_fees)?$businessSettings->ho_fees.'%':""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('PER_ORDER_CHARGES');?> : </strong><?=!empty($businessSettings->ho_per_order_charge)?$currency_symbol.$businessSettings->ho_per_order_charge:""?></p></div>
          </div>
          <div class="row">            
            <div class="col-sm-6"><p><strong><?=$this->lang->line('ADVERTISING_PER');?> : </strong><?=!empty($businessSettings->ho_advertising)?$businessSettings->ho_advertising.'%':""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('FIX_WEEKLY_CHARGES');?> : </strong><?=!empty($businessSettings->ho_fix_weekly_charges)?$currency_symbol.$businessSettings->ho_fix_weekly_charges:""?></p></div>
          </div> 
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('OTHER_CHARGES');?> : </strong><?=!empty($businessSettings->ho_other_charge)?$currency_symbol.$businessSettings->ho_other_charge:""?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('CARD_CHARGES');?>: </strong><?=!empty($businessSettings->ho_credit_debit_card_charges)?$businessSettings->ho_credit_debit_card_charges.'%':""?></p></div>
          </div>

          <h5><?=$this->lang->line('TAXES');?></h5>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('TAX');?> : </strong><?=!empty($businessSettings->tax)?$businessSettings->tax.'%':""?></p>
            </div>
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('TAX_NAME');?> : </strong><?=!empty($businessSettings->tax_name)?$businessSettings->tax_name:""?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('OTHER_INFO');?> : </strong><?=!empty($businessSettings->tax_other_info)?$businessSettings->tax_other_info.'%':""?></p>
            </div>
          </div>

          <h5><?=$this->lang->line('WORKING_HOURS_SERVICE_CHARGES');?></h5>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('SERVICE_CHARGES');?> : </strong><?=!empty($businessSettings->service_charge)?$currency_symbol.$businessSettings->service_charge:""?></p>
            </div>
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('SERVICE_DURATION');?> : </strong><?=!empty($businessSettings->service_duration)?getDuration($businessSettings->service_duration,'minute'):""?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('CHARGE_AFTER_THAT');?> : </strong><?=!empty($businessSettings->service_charges_after)?$currency_symbol.$businessSettings->service_charges_after:""?></p>
            </div>
            <div class="col-sm-6">
              <?php
              if(!empty($businessSettings->service_duration_after) && $businessSettings->service_duration_after>1)
              {
                $service_duration_after = $businessSettings->service_duration_after.' Hours';
              }
              elseif(!empty($businessSettings->service_duration_after) && $businessSettings->service_duration_after==1)
              {
                $service_duration_after = $businessSettings->service_duration_after.' Hour';
              }
              else
              {
                $service_duration_after = '';
              }
              ?>
              <p><strong><?=$this->lang->line('FOR_HOW_LONG');?> : </strong><?=$service_duration_after;?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('CHARGES_AFTER').' '.$service_duration_after;?> : </strong><?=!empty($businessSettings->service_additional_charge)?$currency_symbol.$businessSettings->service_additional_charge:""?></p>
            </div>
            <div class="col-sm-6">
              <?php
              if(!empty($businessSettings->service_additional_hours) && $businessSettings->service_additional_hours>1)
              {
                $service_additional_hours = $businessSettings->service_additional_hours.' Hours';
              }
              elseif(!empty($businessSettings->service_additional_hours) && $businessSettings->service_additional_hours==1)
              {
                $service_additional_hours = $businessSettings->service_additional_hours.' Hour';
              }
              else
              {
                $service_additional_hours = '';
              }
              ?>
                <p><strong><?=$this->lang->line('FOR_HOW_LONG');?> : </strong><?=$service_additional_hours;?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('HOURLY_CHARGE_AFTER');?> : </strong><?=!empty($businessSettings->service_hourly_charge)?$currency_symbol.$businessSettings->service_hourly_charge:""?></p>
            </div>
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('ADDITIONAL_WORKER_CHARGES');?> : </strong><?=!empty($businessSettings->service_additional_worker)?$currency_symbol.$businessSettings->service_additional_worker:""?></p>
            </div>
          </div>

          <h5><?=$this->lang->line('NON_WORKING_HOURS_SERVICE_CHARGES');?></h5>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('SERVICE_CHARGES');?> : </strong><?=!empty($businessSettings->weekend_charge)?$currency_symbol.$businessSettings->weekend_charge:""?></p>
            </div>
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('SERVICE_DURATION');?> : </strong><?=!empty($businessSettings->weekend_duration)?getDuration($businessSettings->weekend_duration,'minute'):""?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('CHARGE_AFTER_THAT');?> : </strong><?=!empty($businessSettings->weekend_charges_after)?$currency_symbol.$businessSettings->weekend_charges_after:""?></p>
            </div>
            <div class="col-sm-6">
              <?php
              if(!empty($businessSettings->weekend_duration_after) && $businessSettings->weekend_duration_after>1)
              {
                $weekend_duration_after = $businessSettings->weekend_duration_after.' Hours';
              }
              elseif(!empty($businessSettings->weekend_duration_after) && $businessSettings->weekend_duration_after==1)
              {
                $weekend_duration_after = $businessSettings->weekend_duration_after.' Hour';
              }
              else
              {
                $weekend_duration_after = '';
              }
              ?>
              <p><strong><?=$this->lang->line('FOR_HOW_LONG');?> : </strong><?=$weekend_duration_after;?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('CHARGES_AFTER').' '.$weekend_duration_after;?> : </strong><?=!empty($businessSettings->weekend_additional_charge)?$currency_symbol.$businessSettings->weekend_additional_charge:""?></p>
            </div>
            <div class="col-sm-6">
              <?php
              if(!empty($businessSettings->weekend_additional_hours) && $businessSettings->weekend_additional_hours>1)
              {
                $weekend_additional_hours = $businessSettings->weekend_additional_hours.' Hours';
              }
              elseif(!empty($businessSettings->weekend_additional_hours) && $businessSettings->weekend_additional_hours==1)
              {
                $weekend_additional_hours = $businessSettings->weekend_additional_hours.' Hour';
              }
              else
              {
                $weekend_additional_hours = '';
              }
              ?>
                <p><strong><?=$this->lang->line('FOR_HOW_LONG');?> : </strong><?=$weekend_additional_hours;?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('HOURLY_CHARGE_AFTER');?> : </strong><?=!empty($businessSettings->weekend_hourly_charge)?$currency_symbol.$businessSettings->weekend_hourly_charge:""?></p>
            </div>
            <div class="col-sm-6">
                <p><strong><?=$this->lang->line('ADDITIONAL_WORKER_CHARGES');?> : </strong><?=!empty($businessSettings->weekend_additional_worker)?$currency_symbol.$businessSettings->weekend_additional_worker:""?></p>
            </div>
          </div>
      </div>     
    </div>
</div>