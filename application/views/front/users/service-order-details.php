<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">
    <?php if(!empty($bookingData)){ //echo "<pre>"; print_r($bookingData); echo "</pre>"; ?>
    <div class="col-md-8">
      <div class="payCcontainer">
        <?php
        $status = !empty($bookingData->status)?$bookingData->status:0;
        echo getServiceBookingStatusHeading($status);
        $currency_code = !empty($bookingData->code)?$bookingData->code:"";
        ?>
        <div class="serv-view row">
          <div class="col-md-4 padd-rght">
            <div class="serv-detail">      
              <strong><?=$this->lang->line('TYPE_OF_SERVICE');?></strong>
              <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
              <ul>
              <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
                if($otherServices!=0){
                $otherData=explode(',', $otherServices);
                $count=count($otherData);              
                foreach ($otherData as $key => $value) { ?>                
                  <li><?=getOtherServicesName($value)?></li>
                <?php } } ?>
                </ul>
            </div>        
          </div>      
          <div class="col-md-8 padd-rght">
            <div class="serv-detail">     
            <strong><?=$this->lang->line('ADDRESS');?></strong>
              <p><?=getServicesAddress(isset($bookingData->address_id)?$bookingData->address_id:"")?></p>  
            </div>        
          </div>             
        </div>

        <?php
        if(!empty($bookingData->additional_operator))
        {
          ?>
          <br>
          <div class="row">
            <div class="col-md-12 text-center additional_operator_box">
              <p>Booking is with "<strong>Additional Operator</strong>".</p>
            </div>
          </div>
          <?php
        }
        ?> 

        <div class="tech-user">
          <i class="fas fa-user-circle"></i>
          Your <?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?> technician will be assigned <strong>1 hour</strong> before the scheduled time.
        </div> 

        <div class="row">
          <div class="col-sm-8">
              <div class="time_wrapper">
                <small><?=$this->lang->line('SERVICE_TIME');?></small>
                <time><?=date("j M, Y h:i A", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?></time>  
              </div>      
          </div>
        
          <div class="col-sm-4">            
          </div>
        </div>
      </div>
      <?php   
      if(!empty($bookingData->vendor_id))
      {
        ?>
        <div class="price_detail2 clearfix">
            <h4><?=$this->lang->line('SERVICE_PROVIDER_DETAILS');?></h4>  
            <?php $sp_info = getServiceProviderDetailsById($bookingData->vendor_id); 
            //echo "<pre>"; print_r($sp_info); echo "</pre>"; ?>
              <div class="row">
                  <div class="col-sm-12"> 
                    <p><strong><?=$this->lang->line('NAME');?> :</strong> <?=!empty($sp_info->username)?$sp_info->username:''; ?></p>    
                  </div>
              </div>
              <div class="row">
                  <div class="col-sm-6"> 
                    <p><strong><?=$this->lang->line('EMAIL');?> : </strong><?=!empty($sp_info->email)?$sp_info->email:''; ?></p>
                  </div>
                  <div class="col-sm-6"> 
                    <p><strong><?=$this->lang->line('CONTACT');?> : </strong><?=!empty($sp_info->mobile)?$sp_info->mobile:''; ?></p>
                  </div>  
              </div>

              <div class="row">
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('HAVE_BUSSINESS');?> : </strong> <?=(!empty($sp_info->business) && $sp_info->business=='yes')?$this->lang->line('YES'):$this->lang->line('NO'); ?></p></div>
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('HAVE_LICENSE');?> : </strong> <?=(!empty($sp_info->license) && $sp_info->license=='yes')?$this->lang->line('YES'):$this->lang->line('NO'); ?></p></div>
              </div>
              <?php if(!empty($sp_info->business) && $sp_info->business=='yes'){ ?>
              <div class="row">
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_NAME');?> : </strong> <?=!empty($sp_info->business_name)?$sp_info->business_name:''; ?></p></div>
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_TYPE');?> : </strong> <?=!empty($sp_info->business_type)?$sp_info->business_type:''; ?></p></div>
              </div>
            <?php } ?>
            <?php if(!empty($sp_info->license) && $sp_info->license=='yes'){ ?>
              <div class="row">
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('LICENSE_DETAILS');?> : </strong> <?=!empty($sp_info->license_details)?$sp_info->license_details:''; ?></p></div>
              </div>
            <?php } ?>
              <div class="row">
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_EMAIL');?> : </strong> <?=!empty($sp_info->business_email)?$sp_info->business_email:''; ?></p></div>
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_TEL_NO');?> : </strong> <?=!empty($sp_info->business_mobile)?$sp_info->business_mobile:''; ?></p></div>                
              </div>

              <div class="row">
                  <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_FAX');?> : </strong> <?=!empty($sp_info->business_fax)?$sp_info->business_fax:''; ?></p></div>
                  <div class="col-sm-6 text-right"><p><a href="<?=base_url('service-provider-details/'.base64_encode($bookingData->vendor_id));?>" class="btn btn-info" target="_blank"><?=$this->lang->line('KNOW_MORE');?></a></p></div>
              </div>            
        </div>
      <?php
      }
      if(!empty($is_service_started) && !empty($is_service_ended))
      {
      ?>
      <br>
      <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('SERVICE_SUMMARY');?></h4>    
          <?php
          if(!empty($is_service_started) && !empty($is_service_ended))
          {
            ?>
            <div class="row">
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('STARTED_TIME');?> :</strong> <?=date('d M, Y h:i A',strtotime($service_start_time)); ?></p>    
                </div>
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('ENDED_TIME');?> : </strong><?=date('d M, Y h:i A',strtotime($service_end_time)); ?></p>
                </div>   
            </div>
            <div class="row">
              <div class="col-sm-6"><p><strong><?=$this->lang->line('TIME_SPENT');?> : </strong> <?=$time_spent; ?></p></div>
              <div class="col-sm-6"><p><strong><?=$this->lang->line('ALLOTTED_TIME');?> : </strong> <?=$service_allocated; ?></p></div>
            </div>
            <?php
            if(!empty($additional_time) && $additional_time>0)
            {
              ?>
              <div class="row">
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('ADDITIONAL_TIME');?> :</strong> <?=$additional_time; ?></p>    
                </div>
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('ADDITIONAL_AMOUNT');?> : </strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>
                </div>             
              </div>
              <?php
            }
            if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
            {
              ?>
              <div class="row">
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('OUTSTANDING_PAYMENT_STATUS');?> : </strong>Paid via Paypal</p>
                </div>
              </div>
              <?php
            }
          }
          ?>
      </div>
      <?php
      }
      ?>

    </div>

    <div class="col-md-4">
        <div class="price_detail2">
          <h4><?=$this->lang->line('CUSTOMER_DETAILS');?></h4>
          <?php
          $customer = getCustomerDetailsById(isset($bookingData->user_id)?$bookingData->user_id:"");
          //echo "<pre>" ; print_r($customer); echo "</pre>";
          ?>
          <div class="row">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('NAME');?> : </p>
            </div>  
              <div class="col-sm-8"> 
                <p><?=(!empty($customer->fname)?$customer->fname:"").' '.(!empty($customer->lname)?$customer->lname:"")?></p></div>
          </div>    
          <div class="row total">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('EMAIL');?> : </p>
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->email)?$customer->email:""?></p></div>   
          </div>
          <div class="row total">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('MOBILE');?> : </p>
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->mobile)?$customer->mobile:""?></p></div>   
          </div>  
          <div class="row paid">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('ADDRESS');?> : </p>
            </div> 
            <div class="col-sm-8">
              <p><?=getServicesAddress(!empty($bookingData->address_id)?$bookingData->address_id:"")?></p>    
            </div>   
          </div>
      </div>
      <br>
      <div class="price_detail2">
      <h4><?=$this->lang->line('PAYMENT_SUMMARY');?></h4>    
      <div class="row">
          <?php
            $amount = !empty($bookingData->amount)?$bookingData->amount:0;
            $sub_total = !empty($bookingData->total_amount)?$bookingData->total_amount:'';
            if(empty($sub_total))
            {
                $sub_total = !empty($bookingData->amount)?$bookingData->amount:0;
            }
            $currency_code = !empty($bookingData->code)?$bookingData->code:"";
            $coupon = !empty($bookingData->coupon)?$bookingData->coupon:"";
            $discount = !empty($bookingData->discount)?$bookingData->discount:0;
            $tax = !empty($bookingData->tax)?$bookingData->tax:"";
            $other_tax = !empty($bookingData->other_tax)?$bookingData->other_tax:0;
            $tax_name = !empty($bookingData->tax_name)?$bookingData->tax_name:"";
            $tax_amount = !empty($bookingData->tax_amount)?$bookingData->tax_amount:"";
          ?>
    
          <div class="col-sm-8">   
              <p><?=$this->lang->line('SERVICE_CHARGES');?></p>   
          </div>    
          <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$amount)?></p>    
          </div>   
      </div>
      <?php
      if(!empty($coupon) && !empty($discount))
      {
        ?>
        <div class="row total">
          <div class="col-sm-8 "> 
            <p><?=$this->lang->line('COUPON');?> (<?=$coupon;?>)</p>
          </div>
          
          <div class="col-sm-4 text-right"> 
            <p><?=getFormatedPriceByCode($currency_code,$discount)?></p>
          </div>   
        </div> 
        <?php
      }
      ?>
      <?php
      if(!empty($tax_amount))
      {
        ?>
        <div class="row total">
          <div class="col-sm-8 "> 
            <?php $oTax = !empty($other_tax)?' + '.$other_tax.'%':''; ?>
            <p><?=$tax_name.' ('.$tax.'%'.$oTax.')';?></p>   
          </div>
          
          <div class="col-sm-4 text-right"> 
            <p><?=getFormatedPriceByCode($currency_code,$tax_amount)?></p>    
          </div>   
        </div> 
        <?php
      }
      ?>    
      <div class="row total">
        <div class="col-sm-8 "> 
          <p><?=$this->lang->line('SUB_TOTAL');?></p>   
        </div>
        
        <div class="col-sm-4 text-right"> 
          <p><?=getFormatedPriceByCode($currency_code,$sub_total)?></p>    
        </div>   
      </div> 
      
      <div class="row paid">
        <div class="col-sm-7"> 
          <p><?=$this->lang->line('AMOUNT_TO_BE_PAID');?></p>
        </div>
      
        <div class="col-sm-5 text-right"> 
          <p><strong><?=getFormatedPriceByCode($currency_code,$sub_total)?></strong></p>    
        </div>   
      </div> 
    </div>

      <?php
      if(!empty($additional_time) && $additional_time>0)
      {
        ?>
        <br>
      <div class="price_detail2">
        <h4><?=$this->lang->line('ADDITIONAL_PAYMENT_SUMMARY');?></h4>
        <div class="row">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SERVICE_CHARGES');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_amount)?></p>    
          </div>
        </div>
        <?php
        if(!empty($additional_tax))
        {
          ?>
          <div class="row total">
            <div class="col-sm-8 "> 
              <?php $oTax = !empty($other_tax)?' + '.$other_tax.'%':''; ?>
              <p><?=$tax_name.' ('.$tax.'%'.$oTax.')';?></p>    
            </div>
            
            <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$additional_tax)?></p>    
            </div>   
          </div> 
          <?php
        }
        ?>
        <div class="row total">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SUB_TOTAL');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>    
          </div>
        </div>

        <?php
          if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
          {
            ?>
            <div class="row paid">
              <div class="col-sm-7"> 
                <p><?=$this->lang->line('AMOUNT_HAS_PAID');?></p>   
              </div>    
              <div class="col-sm-5 text-right"> 
                <p><strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></strong></p>    
              </div>   
            </div>
            <?php
          }
          else
          {
            ?>
            <div class="row paid">
              <div class="col-sm-7"> 
                <p><?=$this->lang->line('AMOUNT_NEED_PAY');?></p>   
              </div>    
              <div class="col-sm-5 text-right"> 
                <p><strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></strong></p>    
              </div>   
            </div>
            <?php
          }
          ?>
      </div>

      <?php $total_paid_amt = $sub_total + $additional_subtotal; ?>
      <br>
      <div class="price_detail2">
       <div class="row grandtotal">
          <div class="col-sm-8"> 
            <p><strong><?=$this->lang->line('GRAND_TOTAL');?> : </strong></p>   
          </div>    
          <div class="col-sm-4 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$total_paid_amt)?></strong></p>    
          </div>   
        </div>
      </div>
      
        <?php
      }
      ?>

    </div>
    <?php } ?>
</div>
<a title="<?=$this->lang->line('BACK');?>" class="cmst btn btn-secondary" href="<?=base_url('service-orders') ?>" ><?=$this->lang->line('BACK');?></a>