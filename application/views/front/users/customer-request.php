<div class="col-sm-8"> 
    <div class="dashboarCcontainer table-responsive"> 
    <form method="post">
    <table class="table">
      <thead>
        <tr>
          <th>
            <?php echo form_dropdown('service', getServicesOptions('All Services',1),set_value('service'),'id="service" class="form-control"'); ?>
          </th>
          <th width="150">
            <?php echo form_dropdown('days', getFilterDaysOptions('All Days'),set_value('days'),'id="days" class="form-control"'); ?>
          </th>
          <th width="150">
            <?php echo form_dropdown('status', getServiceBookingStatusOptions('All Status'),set_value('status'),'id="status" class="form-control"'); ?>
          </th>
         <th width="150">
            <?php echo form_dropdown('leads', getFilterLeadsOptions('All Leads'),set_value('leads'),'id="leads" class="form-control"'); ?>
          </th>
          <th width="50"><button class="btn btn-success">Filter</button></th>
        </tr>
      </thead>
    </table>
    </form>
    <table class="table customer-request">
    <thead>
      <tr>
        <th>#ID</th>
        <th><?=$this->lang->line('SERVICE_TIME');?></th>
        <th><?=$this->lang->line('SERVICES');?></th>
        <th><?=$this->lang->line('AMOUNT');?></th>
        <th><?=$this->lang->line('STATUS');?></th>
        <th width="50"></th>        
      </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($bookingList))
    {
      foreach ($bookingList as $bookings)
      { 
      $code=base64_encode(isset($bookings->id)?$bookings->id:"");
      $amount = !empty($bookings->total_amount)?$bookings->total_amount:'';
      if(empty($amount))
      {
        $amount = !empty($bookings->amount)?$bookings->amount:0;
      }
      $currency_code = !empty($bookings->code)?$bookings->code:"";
      ?>
          <tr>
            <td>#<?=isset($bookings->id)?$bookings->id:""?></td>
          	<td><?=date("j M, Y h:i A", strtotime(isset($bookings->booking_datetime)?$bookings->booking_datetime:"")); ?></td>
            <td><?=getServicesName(isset($bookings->services)?$bookings->services:"0")?></td>
            <td><?=getFormatedPriceByCode($currency_code,$amount)?></td>
            <td width="50" class="text-right">
              <?php
              $status = requestStatus($bookings->id);
              $statusLabel = getServiceBookingStatusLabel($status);
              $statusClass = getServiceBookingStatusClass(!empty($statusLabel)?$statusLabel:'');
              ?>
              <span class="<?=$statusClass;?>"><?=$statusLabel;?></span>              
            </td>
            <td width="50" class="text-right">
              <!-- <a title="Cancel Request" class="cmst btn btn-danger" href="<?=base_url('provider-cancel-request/'.$code) ?>" ><i class="fa fa-times" aria-hidden="true"></i></a> -->
            	<a title="<?=$this->lang->line('VIEW_REQUEST');?>" class="dmst btn btn-info" href="<?=base_url('customer-request-details/'.$code) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
        	  </td>
          </tr> 
          <?php
        }
      }
      else
      {
        ?>
        <tr>
          <td colspan="6"> No any customer requests has founds. </td>
        </tr>
        <?php
      }
      ?>     
      
      
    </tbody>
  </table>
  </div>

  <div class="clerfix"></div>
  <div class="addOpt"><a href="<?=base_url('export-leads') ?>" class="cmst btn btn-success"><i class="fa fa-file-o" aria-hidden="true"></i> <?=$this->lang->line('EXPORT');?></a></div>
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 

</div>
  