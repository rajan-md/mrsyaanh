<div class="col-sm-8">

	<div class="dashboarCcontainer">

		<div class="inner-dash-space">

			<div class="service-provide-col">

				<div class="text-center sec-head">
					<h3>Section B</h3>
					<p>Service Information & Charges</p>
				</div>


				<div class="service-provide-form">

					<form method="post" autocomplete="off">



						<input type="hidden" name="route" id="route">

						<input type="hidden" id="latitude" name="site_latitude">

						<input type="hidden" id="longitude" name="site_longitude">

					    <div class="row">
					    
						<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">First Name</label>

								<input type="text" name="fname" class="form-control" id="fname" value="<?=set_value('fname',isset($userDetails->fname)?$userDetails->fname:" ")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">
						  <label class="profile_field">Last Name</label>
						  <input type="text" name="lname" class="form-control" id="lname" value="<?=set_value('lname',isset($userDetails->lname)?$userDetails->lname:" ")?>" required="">
						 </div>
						 
						  <div class="col-sm-12">
						   <label class="profile_field col-sm-1 no-padding">Gender</label>
						  <?php $gender=set_value('gender',isset($userDetails->gender)?$userDetails->gender:""); ?>

								<label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='male'){ echo 'checked="checked"'; } ?>  value="male">

                  Male</label>

								<label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='female'){ echo 'checked="checked"'; } ?> value="female">

                  Female</label>
                  
                  <label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='others'){ echo 'checked="checked"'; } ?> value="Others">

                  Others</label>
                  
						  </div>
						 
						</div><!--row-->
						
						<h4>Home Address</h4>

						<div class="row">
						
						<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Apt/Unit No</label>

		<input type="text" name="aptno" class="form-control" id="aptno" value="<?=set_value('aptno',isset($userDetails->aptno)?$userDetails->aptno:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street No</label>

		<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street Name</label>

		<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street Type</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Direction</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">City</label>

		<input type="text" name="city" class="form-control" id="locality" value="<?=set_value('city',isset($userDetails->city)?$userDetails->city:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">State/Province</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Zip/Postal Code</label>

		<input type="text" name="postal_code" class="form-control" id="postal_code" value="<?=set_value('postal_code',isset($userDetails->postal_code)?$userDetails->postal_code:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Country</label>

		<input type="text" name="country" class="form-control" id="country" value="<?=set_value('country',isset($userDetails->country)?$userDetails->country:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Home Tel. No.</label>

		<input type="text" name="mobile" class="form-control" id="mobile" value="<?=set_value('mobile',isset($userDetails->mobile)?$userDetails->mobile:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Personal Email</label>

		<input type="text" name="email" class="form-control" id="email" value="<?=set_value('email',isset($userDetails->email)?$userDetails->email:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Date of Birth</label>

		<input type="text" name="dob" class="form-control" id="dob" value="<?=set_value('dob',isset($userDetails->dob)?$userDetails->dob:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Business Name</label>

		<input type="text" name="business-name" class="form-control" id="dob" value="<?=set_value('dob',isset($userDetails->dob)?$userDetails->dob:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Business Type</label>

		<input type="text" name="business-type" class="form-control" id="dob" value="<?=set_value('dob',isset($userDetails->dob)?$userDetails->dob:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 </div><!--row-->
						 
						<!---------------------------business-section----------------------------->
						 
						   <label class="mt10 cus-check">
						  <input type="checkbox" name="business-info" > Business Address
						 </label>
						 
						 <div id="business-logic" class="w-100 mt20"  style="display: none">
						 
						 <div class="row">
						 
						<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Apt/Unit No</label>

		<input type="text" name="aptno" class="form-control" id="aptno" value="<?=set_value('aptno',isset($userDetails->aptno)?$userDetails->aptno:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street No</label>

		<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street Name</label>

		<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Street Type</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Direction</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">City</label>

		<input type="text" name="city" class="form-control" id="locality" value="<?=set_value('city',isset($userDetails->city)?$userDetails->city:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">State/Province</label>

		<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Zip/Postal Code</label>

		<input type="text" name="postal_code" class="form-control" id="postal_code" value="<?=set_value('postal_code',isset($userDetails->postal_code)?$userDetails->postal_code:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Country</label>

		<input type="text" name="country" class="form-control" id="country" value="<?=set_value('country',isset($userDetails->country)?$userDetails->country:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Business Tel. No.</label>

		<input type="text" name="mobile" class="form-control" id="mobile" value="<?=set_value('mobile',isset($userDetails->mobile)?$userDetails->mobile:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Business Fax</label>

		<input type="text" name="fax" class="form-control" id="fax" value="<?=set_value('mobile',isset($userDetails->mobile)?$userDetails->mobile:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Business Email</label>

		<input type="text" name="email" class="form-control" id="email" value="<?=set_value('email',isset($userDetails->email)?$userDetails->email:"")?>" required="">

							</div>
						 </div><!--col-sm-6-->
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field">Car Specification</label>

		<input type="text" name="car_specification" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required=""><!--hem-dropdown-->

							</div>
						 </div><!--col-sm-6-->
						 
						
						</div><!--business-logic-->
						
						</div><!--row-->
						
						<!---------------------------end business-section----------------------------->
	
						
						<div class="form-group mt20">
								<input type="submit" name="submit" class="btn common-btn" id="submit" value="Submit">
						</div>

					</form>


				</div>
				<!--service-provide-form-->


			</div>
			<!--service-provide-col-->

		</div>

	</div>

</div>