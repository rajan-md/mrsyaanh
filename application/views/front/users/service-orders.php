<div class="col-md-8">
  <div class="dashboarCcontainer table-responsive"> 
    <table class="table">
      <thead>
        <form method="get">
        <tr>
          <th></th>
          <th><input type="email" id="email" name="email" value="<?=set_value('email',!empty($email)?$email:''); ?>" class="form-control" placeholder="<?=$this->lang->line('EMAIL');?>"></th>
          <th><input type="text" id="phone" name="phone" value="<?=set_value('phone',!empty($phone)?$phone:''); ?>" class="form-control" placeholder="<?=$this->lang->line('PHONE_NO');?>"></th>
          <th colspan="2" class="text-right"><input type="text" id="oid" name="oid" value="<?=set_value('oid',!empty($oid)?$oid:''); ?>" class="form-control" placeholder="<?=$this->lang->line('SERVICE_ORDER_ID');?>"></th>
          <th class="text-right"><button type="submit" class="btn btn-info"><?=$this->lang->line('FILTER');?></button></th>
        </tr>
      </form>
        <tr>
          <th>#ID</th>
          <th><?=$this->lang->line('SERVICE_TIME');?></th>
          <th><?=$this->lang->line('SERVICES');?></th>
          <th><?=$this->lang->line('AMOUNT');?></th>
          <th width="100"><?=$this->lang->line('STATUS');?></th>
          <th width="120"></th>
        </tr>
      </thead>
      <tbody>
          <?php
          if(!empty($bookingList))
          {
            foreach ($bookingList as $bookings)
            { 
            $code=base64_encode(isset($bookings->id)?$bookings->id:"");
            ?>
            <tr>
            	<td>#<?=isset($bookings->id)?$bookings->id:""?></td>
              <td><?=date("j M, Y h:i A", strtotime(isset($bookings->booking_datetime)?$bookings->booking_datetime:"")); ?></td>
              <td><?=getServicesName(isset($bookings->services)?$bookings->services:"0")?></td>
              <td><?=getPriceFormate(isset($bookings->total_amount)?$bookings->total_amount:"0")?></td>
              <td  class="text-right">
              <?php
              $status = !empty($bookings->status)?$bookings->status:'';
              $statusLabel = getServiceBookingStatusLabel(!empty($status)?$status:'');
              $statusClass = getServiceBookingStatusClass(!empty($statusLabel)?$statusLabel:'');
              ?>
              <span class="<?=$statusClass;?>"><?=$statusLabel;?></span>
              </td>
              <td  class="text-right">
                <a title="<?=$this->lang->line('VIEW_REQUEST');?>" class="dmst btn btn-info" href="<?=base_url('service-order-details/'.$code) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>               
          	  </td>
            </tr>
            <?php
            }
          }
          else
          {
            ?>
            <tr>
              <td colspan="5">No any orders found for searched key</td>
            </tr>
            <?php
          }
          ?> 
      </tbody>
    </table>
  </div>

  <div class="clerfix"></div> 
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 
  
</div>