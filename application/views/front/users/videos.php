    <div class="col-md-8 col-lg-8">
    	<div class="row">
    		<?php
		    //print_r($records);
			if(!empty($records)) {
			    foreach ($records as $data)
			    {
			    	?>      		     	
				    <div class="col-md-4">
				        <a href="<?=base_url('service-video-play/'.base64_encode($data->id)); ?>">
					        <figure class="customprice">
					        	<div class="imgbx">
					        	<?php $thumbnail =  video_thumbnail($data->video,$data->video_source); ?>
					        	<img src="<?=!empty($thumbnail)?$thumbnail:'';?>" class="img-responsive" style="height: 100%;">
					        	</div>
					        </figure>
				        </a>
				    </div>     
			        <?php
			    }
			}
			else
			{
				?>
				<div class="col-md-12">
			        No any videos available for your service category.
			    </div>
			    <?php
			}
			?>
    	</div>
    	<div class="clerfix"></div> 
    	<div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
	</div> 