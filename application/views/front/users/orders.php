<div class="col-md-8 col-lg-8"> 
    <div class="dashboarCcontainer table-responsive">
        <table class="table" style="margin:0">            
            <?php
            if(!empty($records))
            {                
                ?>
                <thead>
                    <tr>
                        <th width="20">#OrdId</th>
                        <th>Customer</th>
                        <th width="20">Amount</th>
                        <th width="20">Payment</th>
                        <th width="20">Status</th>
                        <th width="180">Order Date</th>
                        <th width="100">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>                
                <?php
                foreach ($records as $record)
                {
                    ?>                    
                    <tr>
                        <td><?=!empty($record->id)?$record->id:'';?></td>
                        <td><?=getCustomerName(!empty($record->user_id)?$record->user_id:'');?></td>
                        <td><?=!empty($record->total)?getPriceFormate($record->total):'';?></td>
                        <th><?=!empty($record->payment_method)?strtoupper($record->payment_method):'';?></th>
                        <td><?=!empty($record->orderstatus)?getStatus($record->orderstatus):'';?></td>
                        <td><?=!empty($record->orderdata)?$record->orderdata:'';?></td>
                        <td class="text-right">
                            <a title="Order Details" class="dmst btn btn-info" href="<?=base_url('order-details/'.base64_encode($record->id)); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                    </tr>                    
                    <?php
                }
                ?>
                </tbody>
                <?php
            }
            else
            {
                ?>
                <tbody>
                <div class="text-center mt20"><p><?=$this->lang->line('EMPTY_ORDER');?></p>
                    <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10"><?=$this->lang->line('CONTINUE_SHOPPING');?></a>
                </div>
                <div class="mb40"></div>
                </tbody>
            <?php
            }
            ?>
        </table>
    </div>
    <?php
    if(!empty($records) && !empty($pagination))
    {
        echo $pagination;
    }
    ?>
</div>