<div class="col-sm-8"> 
    <div class="dashboarCcontainer table-responsive"> 

    <table class="table customer-request">
    <thead>
      <tr>
        <tr>
          <th>Invoice Id</th>
          <th style="text-align:right;">Invoiced Amount</th>
          <th>Invoiced Date</th>
          <th>Payment</th>
          <th>Action</th>
        </tr>
      </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($records))
    {
      foreach ($records as $rec)
      { 
          ?>
          <tr>
            <td>#<?=$rec->id; ?></td>                            
            <td align="right"><?=getFormatedPriceByCode($rec->currency_code,$rec->payable_amount); ?></td>                              
            <td><?=date('d M, Y h:i A', strtotime($rec->created)); ?></td>
            <td>
              <?php
              if(empty($rec->is_paid))
              {
                ?>
                <label class="badge badge-warning">Unpaid</label>
                <?php
              }
              else
              {
                ?>
                  <label class="badge badge-success">Paid</label>
                <?php
              }
              ?>
            </td>
            <td>
              <a href="<?=base_url('download-invoice/'.$rec->id);?>" title="Download Invoice" class="btn btn-xs btn-success"> <i class="fa fa-download"></i> </a>
            </td>                              
          </tr> 
          <?php
      }
    }
    else
    {
      ?>
      <tr>
        <td colspan="6">No any jobs has assigned to you.</td>
      </tr>
      <?php
    }
    ?>
    </tbody>
  </table>
  </div>

  <div class="clerfix"></div> 
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 

</div>
  