<html>
<body>

  <div class="main_container" style="margin: auto; width: 100%; border: 1px solid #e7e7e7; margin-bottom: 50px;">
    <div class="top_bg" style="background: #fff; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); display: inline-block; width: 100%; padding: 8px 0; margin-bottom: 30px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td align="left" style="float: left; margin: 0 0 0 14px;">
            &nbsp;&nbsp;<img src="<?=base_url('skin/front/images/logo.png')?>">
            
            </td>
            <td align="right" style="float: right;font-family: arial;margin: 16px 13px;font-size: 20px;">
            
            Service Invoice&nbsp;&nbsp;&nbsp;
            
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    	   
    <table style="font-family: arial; " width="100%" cellspacing="0" cellpadding="7" border="0">
        <tbody>
          <tr><td colspan="2" align="left" valign="top"><p><strong>Service Date : </strong><?=!empty($service_datetime)?$service_datetime:''; ?></p></td></tr>
          <tr>
            <td width="50%" align="left" valign="top">
              <?php if(!empty($name)){ ?><h3><?=$name;?></h3><?php } ?>
              <p>
                <?php if(!empty($email)){ ?><abbr title="Work email">E-mail:</abbr> <?=$email;?><br><?php } ?>
                <?php if(!empty($contact)){ ?><abbr title="Work Phone">Phone:</abbr> <?=$contact;?><br><?php } ?>                              
              </p>
            </td>
            <td width="50%" align="right" valign="top">
              <h3>Address</h3>
              <?=!empty($address)?$address:''; ?>            
            </td>
          </tr>
        </tbody>
    </table>
	
  	<div style="font-family: arial;margin: auto; width: 25%; background: #212121;text-align: center;color: #fff;padding: 5px 0;border-radius: 30px;font-size: 14px; margin-top: 15px; margin-bottom: 15px;">Booking Id : <?='#'.$oid;?> </div>

    <table style="font-family: arial;border: 1px solid #e4e4e4;" width="100%" cellspacing="0" cellpadding="7" border="0">
      <tbody>

        <tr>
            <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="left">Description</th>
            <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Service Charges</th>
            <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Discount</th>
            <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Tax</th>
            <th style="border: 1px solid #e4e4e4; background: #e4e4e4;" align="right">Sub Total</th>
        </tr>

        <tr>          
            <td style="border: 1px solid #e4e4e4;" align="left">Initial Minimum Charges</td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_prepaid_charges); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_prepaid_discount); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_prepaid_tax_amount); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_prepaid_sub_amount); ?></td>                                                   
        </tr>

        <tr>          
            <td style="border: 1px solid #e4e4e4;" align="left">Post Service Charges</td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_postpaid_charges); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_postpaid_discount); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_postpaid_tax_amount); ?></td>
            <td style="border: 1px solid #e4e4e4;" align="right"><?=getFormatedPriceByCode($currency_code,$service_postpaid_sub_amount); ?></td>                                                   
        </tr>

        <tr>
          <th style="background: #e4e4e4;" colspan="2">&nbsp;</td>        
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right" colspan="2"><strong>Grand Total</strong></td>
          <td style="border: 1px solid #e4e4e4; padding: 5px;" align="right"><strong><?=getFormatedPriceByCode($currency_code,$grand_total); ?></strong></td>
        </tr>

      </tbody>
    </table>

    <br><br>
    <h4>Note :-</h4>
    <p>Above declared amount against respective booking Id has marked as invoiced and  that are carring both records pre and post payment of service.</p>
  </div>
</body>
</html>
