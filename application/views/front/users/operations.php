<div class="col-sm-8"> 
    <div class="dashboarCcontainer"> 

    <table class="table customer-request">
    <thead>
      <tr>
        <th>#ID</th>
        <th>Service Time</th>        
        <th>Services</th>
        <th>Customer</th>
        <th>Operator</th>
        <th width="150"></th>
        
      </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($records))
    {
      foreach ($records as $record)
      { 
      $code=base64_encode(isset($record->id)?$record->id:"");
      ?>
          <tr>
            <td>#<?=isset($record->id)?$record->id:""?></td>
          	<td><?=date("j M, Y h:i A", strtotime(isset($record->booking_datetime)?$record->booking_datetime:"")); ?></td>
            <td><?=getServicesName(isset($record->services)?$record->services:"0")?></td>
            <td><?=getCustomerName(isset($record->user_id)?$record->user_id:"0")?></td>
            <td><?=getCustomerName(isset($record->operator_id)?$record->operator_id:"0")?></td>
            <td>
              <a title="Send rervice OTP to customer" class="dmst btn btn-primary" href="<?=base_url('send-operations-otp/'.$code) ?>"><i class="fas fa-envelope" aria-hidden="true"></i></a>
            	<a title="View Request" class="dmst btn btn-info" href="<?=base_url('operation_details/'.$code) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
        	  </td>
          </tr> 
          <?php
        }
      }
      ?>     
      
      
    </tbody>
  </table>
  </div>

  <div class="clerfix"></div> 
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 

</div>
  