<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">



		<form method="post">

		<div class="form-group">

              <label class="col-md-12" for="old_password"><?=$this->lang->line('OLD_PASSWORD'); ?><span class="redstar">*</span></label>

              <div class="col-md-12">

                <input id="old_password" name="old_password" type="password"  class="form-control" >

                <?php echo form_error('old_password'); ?>

              </div>

            </div>

            <div class="form-group">

              <label class="col-md-12" for="userpass"><?=$this->lang->line('NEW_PASSWORD'); ?><span class="redstar">*</span></label>

              <div class="col-md-12">

                <input id="userpass" name="userpass" type="password"  class="form-control" >

                <?php echo form_error('userpass'); ?>

              </div>

            </div>

           <div class="form-group">

              <label class="col-md-12" for="userconfpass"><?=$this->lang->line('CONFIRM_NEW_PASSWORD'); ?><span class="redstar">*</span></label>

              <div class="col-md-12">

                <input id="userconfpass" name="userconfpass" type="password"  class="form-control" >

                <?php echo form_error('userconfpass'); ?>

              </div>

            </div>



            <div class="form-group">

			<label class="col-md-3 profile_field"></label>

			<div class="col-md-2">

				<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT'); ?>">

			</div>

		</div>

	</form>

	</div>

</div>