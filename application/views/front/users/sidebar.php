   <div class="col-md-4 col-lg-4">
   	<div class="sidebarContainer">
   	<div class="userPic">
   		<?php $segment = $this->uri->segment(1); ?>
   		<?=getCustomerPicture()?>
   	
   		<div class="userDe">
   		<?php $sidebarInfo=getCustomerSidebar();
   		$username = !empty($sidebarInfo->username)?$sidebarInfo->username:'unknown user';
   		$email = !empty($sidebarInfo->email)?$sidebarInfo->email:'';
   		$mobile = !empty($sidebarInfo->mobile)?$sidebarInfo->mobile:'unknown user';
   		echo '<strong>'.$username.'</strong><br/>';
   		echo $email.'<br/>';
   		echo $mobile.'<br/>';
   		?>
   		
   	</div>
   	</div>
	<?php $userRole=getLoginUserRole(); ?>
	<ul class="dashboard-sidebar">
		<li <?=($segment=='my-account')?'class="active"':'';?>>
			<a href="<?=base_url('my-account')?>">
				<figcaption><i class="fa fa-user"></i> <?=$this->lang->line('MY_ACCOUNT');?></figcaption>
			</a>
		</li>
       		
		<?php
		if($userRole==5)
		{
		?>

		<li <?=($segment=='service-request')?'class="active"':'';?>>
			<a href="<?=base_url('service-request')?>">
				<figcaption><i class="fa fa-cog"></i> <?=$this->lang->line('MY_SERVICE');?></figcaption>
			</a>
		</li>

		<li <?=($segment=='operators')?'class="active"':'';?>>
			<a href="<?=base_url('operators')?>">
				<figcaption><i class="fa fa-users"></i> <?=$this->lang->line('MY_OPERATORS');?></figcaption>
			</a>
		</li>

		<li <?=($segment=='customer-request')?'class="active"':'';?>>
			<a href="<?=base_url('customer-request')?>">
				<figcaption><i class="fa fa-file"></i> <?=$this->lang->line('CUSTOMER_REQUEST');?></figcaption>
			</a>			
		</li>

		<li <?=($segment=='my-invoices')?'class="active"':'';?>>
			<a href="<?=base_url('my-invoices')?>">
				<figcaption><i class="fa fa-list"></i> <?=$this->lang->line('MY_INVOICE');?></figcaption>
			</a>
		</li>

		<li <?=($segment=='contract-form-102')?'class="active"':'';?>>
			<a href="<?=base_url('contract-form-102')?>">
				<figcaption><i class="fa fa-file"></i> <?=$this->lang->line('CONTRACT_FORM');?></figcaption>
			</a>			
		</li>

		<li <?=($segment=='service-video')?'class="active"':'';?>>
			<a href="<?=base_url('service-video')?>">
				<figcaption><i class="fa fa-youtube"></i> <?=$this->lang->line('VIDEOS');?></figcaption>
			</a>
		</li>

		<?php
		}
		elseif($userRole==9)
		{
			?>
			<li <?=($segment=='jobs')?'class="active"':'';?>>
				<a href="<?=base_url('jobs')?>">
					<figcaption><i class="fa fa-list"></i> <?=$this->lang->line('MY_JOBS');?></figcaption>
				</a>
			</li>
			<?php
		}
		elseif($userRole==8)
		{
			?>
			<li <?=($segment=='service-orders')?'class="active"':'';?>>
				<a href="<?=base_url('service-orders')?>">
					<figcaption><i class="fa fa-list"></i> <?=$this->lang->line('ORDERS');?></figcaption>
				</a>
			</li>
			<?php
		}
		else		
		{
		?>
			<li <?=($segment=='my-bookings')?'class="active"':'';?>>
				<a href="<?=base_url('my-bookings')?>">
					<figcaption><i class="fa fa-list"></i> <?=$this->lang->line('MY_BOOKING');?></figcaption>
				</a>
			</li>
			
			<li <?=($segment=='wishlist')?'class="active"':'';?>>
				<a href="<?=base_url('wishlist')?>">
					<figcaption><i class="fa fa-heart"></i> <?=$this->lang->line('WISHLIST');?></figcaption>
				</a>
			</li>

			<li <?=($segment=='orders')?'class="active"':'';?>>
				<a href="<?=base_url('orders')?>">
					<figcaption><i class="fa fa-file"></i> <?=$this->lang->line('ORDERS');?></figcaption>
				</a>
			</li>
			<li <?=($segment=='complaint')?'class="active"':'';?>>
				<a href="<?=base_url('complaint')?>">
					<figcaption><i class="fa fa-list-alt"></i> <?=$this->lang->line('COMPLAINT');?></figcaption>
				</a>
			</li>
		<?php
		}
		?>

		<?php
		if($userRole!=5)
		{
			?>
			<li <?=($segment=='edit-profile')?'class="active"':'';?>>
				<a href="<?=base_url('edit-profile')?>">
					<figcaption><i class="fa fa-edit"></i> <?=$this->lang->line('UPDATE_PROFILE');?></figcaption>
				</a>
			</li>
			<?php
		}
		?>
		
		<li <?=($segment=='change-password')?'class="active"':'';?>>
			<a href="<?=base_url('change-password')?>">
				<figcaption><i class="fa fa-lock"></i> <?=$this->lang->line('CHANGE_PASSWORD');?></figcaption>
			</a>
		</li>
	</ul>
</div>
</div>