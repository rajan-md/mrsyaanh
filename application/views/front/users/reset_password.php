<aside class="col-md-3"></aside>
<aside class="col-md-6">
    <div class="col-md-12"><h2><?=$this->lang->line('RESET_PASSWORD'); ?></h2>
    
    </div>
	<form method="post">
		<div class="form-group">
              <label class="col-md-12" for="email"><?=$this->lang->line('NEW_PASSWORD'); ?></label>
              <div class="col-md-12">
                <input id="email" name="password" type="password"  class="form-control" required>
                <?php echo form_error('password'); ?>
              </div>
            </div>
           <div class="form-group">
              <label class="col-md-12" for="email"><?=$this->lang->line('CONFIRM_NEW_PASSWORD'); ?></label>
              <div class="col-md-12">
                <input id="email" name="confirm_password" type="password"  class="form-control" required>
                <?php echo form_error('confirm_password'); ?>
              </div>
            </div>

            <div class="form-group">
            <div class="col-md-12">
            
            </div>
              <div class="col-md-12">
                <div class="sBtn"><button type="submit"><?=$this->lang->line('RESET_PASSWORD'); ?></button></div>
              </div>
            </div>
	</form>
</aside>
<aside class="col-md-3"></aside>