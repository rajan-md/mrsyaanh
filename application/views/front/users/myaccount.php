<div class="col-md-8 col-lg-8"> 

    <div class="dashboarCcontainer"> 

<div class="inner-dash-space">
	
	<p><?=$this->lang->line('HELLO');?> <strong><?php
	$id=$this->session->userdata('userID');
	echo getCustomerName($id); ?>
	</strong>. <?=$this->lang->line('MY_ACCOUNT_DESC');?></p>

<p><a href="<?=base_url('edit-profile')?>"><?=$this->lang->line('DASHBOARD_LABEL');?></a></p>
<p><?=$this->lang->line('ADDRESS_DESC');?></p>
	
	<div class="row">
	    <?php $userRole=getLoginUserRole(); ?>
	    <?php if($userRole==1){ ?>
		<div class="col-md-6">
		 <div class="acc-bill-sec">
			<h3><?=$this->lang->line('BILLING_ADDRESS');?></h3>
			<p><a href="<?=base_url('billing-address')?>"><?=$this->lang->line('EDIT');?></a></p>
			<p><?=$this->lang->line('ADDRESS_HINT');?></p>
		 </div>
		</div>
		
		<div class="col-md-6">
		  <div class="acc-bill-sec">
			<h3><?=$this->lang->line('SHIPPING_ADDRESS');?></h3>
			<p><a href="<?=base_url('shipping-address')?>"><?=$this->lang->line('EDIT');?></a></p>
			<p><?=$this->lang->line('ADDRESS_HINT');?></p>
		  </div>
		</div>
		<?php }
		/*if($userRole==5) { ?>
		<div class="col-md-6">
		  <div class="acc-bill-sec">
			<h3> <?=$this->lang->line('ACCOUNT_DETAILS');?></h3>
			<p><a href="<?=base_url('edit-profile')?>"><?=$this->lang->line('EDIT');?></a></p>
			
		  </div>
		</div>
		<div class="col-md-6">
		  <div class="acc-bill-sec">
			<h3> <?=$this->lang->line('CONTRACT_FORM_102');?></h3>
			<p><a href="<?=base_url('contract-form-102')?>"><?=$this->lang->line('EDIT');?></a></p>
			
		  </div>
		</div>

		<?php } */ ?>
		
	</div>
	
	
</div>



    </div>

    </div>