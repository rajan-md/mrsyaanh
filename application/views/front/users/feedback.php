<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">
		<form method="post" autocomplete="off" enctype="multipart/form-data" class="newservice">
				<?php getNotificationHtml(); ?>
					<div class="row">						
						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="support_concern"><?=$this->lang->line('CUSTOMER_FEEDBACK');?><span class="redstar">*</span></label>
								<?php
								echo form_textarea(array('id'=>'customer_feedback','name' => 'customer_feedback','class'=>'form-control','rows' => 10,'cols' => 25,'placeholder'=>$this->lang->line('TYPE_COMMENT'),'value' => set_value('customer_feedback', isset($customer_feedback)?$customer_feedback:"" ),'required'=>'required'));
		                      	echo form_error('customer_feedback');
		                      	?>
							</div>
						</div>


					 </div><!--row-->

				<div class="row">
					<div class="col-md-12 form-group">
						<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>">
					</div>
				</div>

	</form>

	</div>

</div>