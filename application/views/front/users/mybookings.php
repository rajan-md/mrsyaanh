<div class="col-md-8">
  <?php getNotificationHtml(); ?>
  <div class="dashboarCcontainer table-responsive"> 
    <table class="table">
      <thead>
        <tr>
          <th>#ID</th>
          <th><?=$this->lang->line('SERVICE_TIME');?></th>
          <th><?=$this->lang->line('SERVICES');?></th>
          <th><?=$this->lang->line('AMOUNT');?></th>
          <th width="100"><?=$this->lang->line('STATUS');?></th>
          <th width="240"></th>
        </tr>
      </thead>
      <tbody>
          <?php
          if(!empty($bookingList))
          {
            foreach ($bookingList as $bookings)
            { 
              $code=base64_encode(isset($bookings->id)?$bookings->id:"");

              $amount = !empty($bookings->total_amount)?$bookings->total_amount:'';
              if(empty($amount))
              {
                $amount = !empty($bookings->amount)?$bookings->amount:0;
              }
              //$amount = (empty($amount) && !empty($bookings->amount))?$bookings->amount:0;
              $currency_code = !empty($bookings->code)?$bookings->code:"";
            ?>
            <tr>
            	<td>#<?=isset($bookings->id)?$bookings->id:""?></td>
              <td><?=date("j M, Y h:i A", strtotime(isset($bookings->booking_datetime)?$bookings->booking_datetime:"")); ?></td>
              <td><?=getServicesName(isset($bookings->services)?$bookings->services:"0")?></td>
              <td><?=getFormatedPriceByCode($currency_code,$amount)?></td>
              <td  class="text-right">
              <?php
              $status = !empty($bookings->status)?$bookings->status:'';
              $statusLabel = getServiceBookingStatusLabel(!empty($status)?$status:'');
              $statusClass = getServiceBookingStatusClass(!empty($statusLabel)?$statusLabel:'');
              ?>
              <span class="<?=$statusClass;?>"><?=$statusLabel;?></span>
              </td>
              <td  class="text-right">
                <a title="<?=$this->lang->line('VIEW_REQUEST');?>" class="dmst btn btn-info" href="<?=base_url('booking-details/'.$code) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>                                   
                  <?php
                  if(!empty($status) && $status==5)
                  {
                      $feedbackUrl = base_url('feedback/'.$code);
                      $invoiceUrl = base_url('invoice/'.$code);
                      $disabled = '';
                  }
                  else
                  {
                      $feedbackUrl = 'javascript:;';
                      $invoiceUrl = 'javascript:;';
                      $disabled = 'disabled';
                  }
                  ?>
                   <a title="<?=$this->lang->line('CUSTOMER_FEEDBACK');?>" class="cmst btn btn-warning <?=$disabled;?>" href="<?=$feedbackUrl; ?>"><i class="fa fa-comments" aria-hidden="true"></i></a>
                   <a title="<?=$this->lang->line('INVOICE');?>" class="cmst btn btn-success <?=$disabled;?>" href="<?=$invoiceUrl; ?>"><i class="fas fa-file-invoice" aria-hidden="true"></i></a>
                   <?php
                  if(empty($status) || $status==0 || $status==1)
                  {
                      $canelUrl = base_url('customer-cancel-request/'.$code);
                      $disabled = '';
                  }
                  else
                  {
                      $canelUrl = 'javascript:;';
                      $disabled = 'disabled';
                  }
                  ?>
                  <a title="<?=$this->lang->line('CANCEL_REQUEST');?>" class="cmst btn btn-danger <?=$disabled;?>" href="<?=$canelUrl; ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                   
          	  </td>
            </tr>
            <?php
            }
          }
          ?> 
      </tbody>
    </table>
  </div>

  <div class="clerfix"></div> 
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 
  
</div>

  