<!-- Profile Edit -->
<section class="editableBox">
  <div class="container">
    <div class="row">
      <div class="editBox"> <img src="<?=base_url('skin/front')?>/images/shape1.png"> <a href="#" class="edit"><span>Edit Profile</span> <img src="<?=base_url('skin/front')?>/images/edit-icon.png"></a>
        <div class="userProfile">
          <figure>
            <div class="profileImg"><img src="<?=base_url('skin/front')?>/images/profile-image.jpg"></div>
            <div class="status1"><span class="online"></span>Online</div>
            <div class="u_name"> <a href="#" class="chat_icons"><img src="<?=base_url('skin/front')?>/images/chat-icon1.png"><img src="<?=base_url('skin/front')?>/images/chat-icon2.png"></a>
              <div class="clear"></div>
              Ujjwal kumar <span>Delhi</span></div>
          </figure>
        </div>
      </div>
      
      <div class="editBox editBox1"> <img src="<?=base_url('skin/front')?>/images/shape2.png"> <a href="#" class="edit"><span>Edit Vechicle</span> <img src="<?=base_url('skin/front')?>/images/edit-icon.png"></a>
        <div id="profile" class="owl-carousel owl-theme">
          <div class="item">
            <figure><img src="<?=base_url('skin/front')?>/images/slide1.jpg"></figure>
          </div>
          <div class="item">
            <figure><img src="<?=base_url('skin/front')?>/images/slide2.jpg"></figure>
          </div>
        </div>
      </div>
      <div class="requests">
        <div class="shapeImg"><img src="<?=base_url('skin/front')?>/images/shape3.png"></div>
        <div class="alerts"><span>Alert</span> <img src="<?=base_url('skin/front')?>/images/alert.png"></div>
        <div class="req_lists">
          <h3>Recent Requests</h3>
          <div class="content mCustomScrollbar">
            <ul>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
              <li>
                <h4>Jabir want to swap your vehicles</h4>
                <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="vehicleSearch">
        <div class="searchField">
          <input name="" type="text" class="vtext" placeholder="lapin vehicles">
          <input name="" type="submit" class="vsubmit">
        </div>
        <div class="lapsCount">
          <ul>
            <li><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></li>
            <li><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></li>
            <li><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></li>
            <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
            <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
          </ul>
          <div class="lapsleft">laps : 000551<i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
          <div class="lapsright">miles : 000255 <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
        </div>
      </div>
      <div class="swaps">
        <ul>
          <li class="active">Swap <i class="fa fa-long-arrow-left"></i></li>
          <li>Swap/Borrow <i class="fa fa-long-arrow-left"></i></li>
          <li>Borrow <i class="fa fa-long-arrow-left"></i></li>
        </ul>
        <figure><img src="<?=base_url('skin/front')?>/images/gear1.png"></figure>
      </div>
    </div>
  </div>
</section>

<!-- User Profile -->
<section class="userDetails">
  <div class="container">
    <aside class="col-md-4 col-sm-4">
      <div class="shadow_box">
        <h3>Ujjwal Identification Documents</h3>
        <ul class="docsList">
          <li>
            <label>National identification
              <input type="checkbox" name="signup[]" value="" checked="checked">
            </label>
          </li>
          <li>
            <label>Driving license
              <input type="checkbox" name="signup[]" value="">
            </label>
          </li>
          <li>
            <label>Insurance type
              <input type="checkbox" name="signup[]" value="">
            </label>
          </li>
          <li>
            <label>Vehicle (s ) registration card
              <input type="checkbox" name="signup[]" value="">
            </label>
          </li>
          <li>
            <label>Technical inspection
              <input type="checkbox" name="signup[]" value="">
            </label>
          </li>
        </ul>
      </div>
      <div class="shadow_box">
        <h3>Ujjwal’s Vehicles &amp; accessories</h3>
        <ul class="accList">
          <li><img src="<?=base_url('skin/front')?>/images/vehicle-img1.jpg" class="img-responsive"></li>
          <li><img src="<?=base_url('skin/front')?>/images/vehicle-img2.jpg" class="img-responsive"></li>
          <li><img src="<?=base_url('skin/front')?>/images/vehicle-img3.jpg" class="img-responsive"></li>
          <li><a href="#"><i class="fa fa-plus"></i><span>Add vehicles</span></a></li>
        </ul>
      </div>
      <div class="shadow_box">
        <h3>last laps</h3>
        <div class="formGroup">
          <input id="datepicker" name=""  class="date" type="text">
        </div>
        <div class="formGroup">
          <input id="datepicker" name=""  class="date" type="text">
        </div>
        <button type="submit"><img src="<?=base_url('skin/front')?>/images/icon-search.png"> LAPIN</button>
        <ol class="lapsList">
          <li>Dehli to Agra
            <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
            <span>( Mr. jax )</span> </li>
          <li>Dehli to Noida
            <p>Delhi - Delhi 28 July 14:20 - 28 July 14:20</p>
            <span>( Mr. jax )</span> </li>
        </ol>
      </div>
    </aside>
    <aside class="col-md-8 col-sm-8">
      <h2>last laps</h2>
      <div class="content mCustomScrollbar">
        <ul class="commentList">
          <li>
            <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
            <div class="commentText">
              <h6>Mr. jax</h6>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
              <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
              <button>Reply</button>
            </div>
            <div class="clear"></div>
            <ul>
              <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
              <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
            </ul>
          </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
          <li>
                <figure><img src="<?=base_url('skin/front')?>/images/user-image.jpg"></figure>
                <div class="commentText">
                  <h6>Mr. jax</h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="smiley"> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-green.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> <span><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></span> </div>
                  <button>Reply</button>
                </div>
              </li>
        </ul>
      </div>
      <div class="postComent">
        <h4>Comments</h4>
        <ul>
          <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
          <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
          <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
          <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
          <li><img src="<?=base_url('skin/front')?>/images/smiley-grey.png"></li>
        </ul>
        <textarea></textarea>
        <input name="" type="submit" value="Submit" class="csubmit">
      </div>
    </aside>
  </div>
</section>
     