<div class="col-sm-8"> 
    <div class="dashboarCcontainer table-responsive"> 

    <table class="table customer-request">
    <thead>
      <tr>
        <th>#ID</th>
        <th width="200">Service Time</th>        
        <th>Services</th>
        <th>Address</th>
        <th>Status</th>
        <th width="50"></th>        
      </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($records))
    {
      foreach ($records as $record)
      { 
          $code=base64_encode(isset($record->id)?$record->id:"");
          ?>
          <tr>
            <td>#<?=isset($record->id)?$record->id:""?></td>
          	<td><?=date("j M, Y h:i A", strtotime(isset($record->booking_datetime)?$record->booking_datetime:"")); ?></td>
            <td><?=getServicesName(isset($record->services)?$record->services:"0")?></td>
            <td><?=getServicesAddress(isset($record->address_id)?$record->address_id:"0")?></td>
            <td  class="text-right">
            <?php
            $status = !empty($record->status)?$record->status:'';
            $statusLabel = getServiceBookingStatusLabel(!empty($status)?$status:'');
            $statusClass = getServiceBookingStatusClass(!empty($statusLabel)?$statusLabel:'');
            ?>
            <span class="<?=$statusClass;?>"><?=$statusLabel;?></span>
            </td>
            <td>
            	<a title="View Request" class="dmst btn btn-info" href="<?=base_url('job_details/'.$code) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
        	  </td>
          </tr> 
          <?php
      }
    }
    else
    {
      ?>
      <tr>
        <td colspan="6">No any jobs has assigned to you.</td>
      </tr>
      <?php
    }
    ?>
    </tbody>
  </table>
  </div>

  <div class="clerfix"></div> 
  <div class="pagination"><?=isset($pagination)?$pagination:""?></div>  
  <div class="clerfix"></div> 

</div>
  