<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">
    <?php
    if(!empty($bookingData))
    {
    //echo "<pre>"; print_r($bookingData); echo "</pre>"; ?>
    <div class="col-md-4">
      <div class="payCcontainer">
        <?php
        if($userRole==5)
        {
            if(!empty($is_other_accepted) && $is_other_accepted==1)
            {
            echo '<h2><i class="fa fa-times-circle"></i> Booking Accepted by other</h2>';
            }
            elseif(!empty($request_desicion) && $request_desicion==2)
            {
                echo '<h2><i class="fa fa-times-circle"></i> Booking Rejected</h2>';
            }
            elseif(!empty($request_desicion) && $request_desicion==1)
            {
                echo '<h2><i class="fa fa-check-circle"></i> Booking Accepted</h2>';
            }
            else
            {
                echo '<h2><i class="fa fa-question-circle"></i> Booking Pending</h2>';
            }          
        }
        else
        {
              if(!empty($bookingData->status) && $bookingData->status==1)
              {
                echo '<h2><i class="fa fa-check-circle"></i> Booking Accepted</h2>';
              }
              elseif(!empty($bookingData->status) && $bookingData->status==2)
              {
                 echo '<h2><i class="fa fa-times-circle"></i> Booking Rejected</h2>';
              }
              else
              {
                  echo '<h2><i class="fa fa-question-circle"></i> Pending Booking</h2>';
              } 
        }
      ?>
      <div class="serv-view row">      
        <div class="col-md-12">
          <div class="serv-detail">      
            <strong>Service</strong>
            <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
            <ul>
            <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
              if($otherServices!=0){
              $otherData=explode(',', $otherServices);
              $count=count($otherData);              
              foreach ($otherData as $key => $value) { ?>                
                <li><?=getOtherServicesName($value)?></li>
              <?php } } ?>
              </ul>
          </div>        
        </div>      
      </div>         
     
      <div class="row">
        <div class="col-sm-12">
            <div class="serv-detail">
              <strong>Service Time</strong>
              <p><time><?=date("j M, Y h:i A", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?></time></p>
            </div>      
        </div>
       </div>
      </div>
      <br>
      <div class="price_detail2">
        <h4>Payment Summary</h4>    
        <div class="row">
            <div class="col-sm-8">
              <p>Service charge</p>   
            </div>
            <div class="col-sm-4 text-right"> 
              <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
            </div>
        </div>    
        <div class="row total">
          <div class="col-sm-8"> 
            <p>SubTotal</p>   
          </div>    
          <div class="col-sm-4 text-right"> 
            <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
          </div>   
        </div>    
        <div class="row paid">
          <div class="col-sm-7"> 
            <p>Amount to be paid</p>   
          </div>    
          <div class="col-sm-5 text-right"> 
            <p><strong><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></strong></p>    
          </div>   
        </div>
      </div> 
    </div>
   
    <div class="col-md-8">

        <div class="price_detail2">
          <h4>Customer Details</h4>
          <?php
          $customer = getCustomerDetailsById(isset($bookingData->user_id)?$bookingData->user_id:"");
          //echo "<pre>" ; print_r($customer); echo "</pre>";
          ?>
          <div class="row">
            <div class="col-sm-4"> 
              <p>Name : </p>   
            </div>  
              <div class="col-sm-8"> 
                <p><?=(!empty($customer->fname)?$customer->fname:"").' '.(!empty($customer->lname)?$customer->lname:"")?></p></div>
          </div>    
          <div class="row total">
            <div class="col-sm-4"> 
              <p>Email Id : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->email)?$customer->email:""?></p></div>   
          </div>
          <div class="row total">
            <div class="col-sm-4"> 
              <p>Mobile Number : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->mobile)?$customer->mobile:""?></p></div>   
          </div>  
          <div class="row paid">
            <div class="col-sm-4"> 
              <p>Address : </p>   
            </div> 
            <div class="col-sm-8">
              <p><?=getServicesAddress(!empty($bookingData->address_id)?$bookingData->address_id:"")?></p>    
            </div>   
          </div>
        </div>  

        <br>

        <div class="price_detail2" id="operations_prestart">
          <div class="row">
            <div class="col-sm-12 text-center">
              <a href="<?=base_url('send-operations-otp/'.$code) ?>" class="btn btn-primary"><i class="fa fa-envelope" aria-hidden="true"></i> Send OTP</a>
              <a id="assignToOperator" href="javascript:;" class="btn btn-success"><i class="fa fa-user-circle" aria-hidden="true"></i> Assign Operator</a>
            </div>
          </div>
        </div>

        <div id="assignBox" class="boxWithBg" style="display: none">
          <?php
          if(!empty($bookingData->operator_id))
          {
            ?>
            <div class="row">        
              <div class="col-sm-3"><strong>Assigned To : </strong></div>
              <div class="col-sm-9"><?=getCustomerName($bookingData->operator_id);?></div>
            </div>
            <br>
            <?php  
          }
          ?>
          <div class="row">
          <?php
          if(!empty($operators))
          {
            ?>     
              <div class="col-sm-12 alert-msg"></div>
              <div class="col-sm-3">Choose Operator</div>
              <div class="col-sm-6">
                <?php
                if(!empty($operators))
                {                  
                  echo '<div class="operator-assign-box">
                  <select id="operator" name="operator" class="form-control" data-id="'.$rawId.'">
                  <option value="">------Assign Operator-----</option>';
                  foreach ($operators as $operator)
                  {
                    $selected = (!empty($bookingData->operator_id) && $bookingData->operator_id==$operator->id)?'selected="selected"':'';
                    echo '<option value="'.$operator->id.'" '.$selected.'>'.$operator->username.'</option>';
                  }             
                  echo '</select>
                  </div>';
                }
                ?>
              </div>
              <div class="col-sm-3"><a class="btn common-btn" id="assignbtn" href="javascript:;">Save</a></div>
            <?php
          }
          else
          {
            ?>                   
              <div class="col-sm-12"><p>You have no operators which you can assign this service. <a href="<?=base_url('add-operator')?>">Add Operator</a> and then try to assign again.</p></div>                      
            <?php
          }
          ?>
        </div>

        </div>
        <?php /* ?>
        <div class="price_detail2" id="operations_started">
          <h4>Job Summary</h4>    
          <?php
          if(!empty($service_start_time) && !empty($service_end_time))
          {
            ?>
            <div class="row">
                <div class="col-sm-6"> 
                  <p><strong>Started Time :</strong> <?=date('Y-m-d h:i:s',strtotime($service_start_time)); ?></p>    
                </div>
                <div class="col-sm-6"> 
                  <p><strong>Ended Time : </strong><?=date('Y-m-d h:i:s',strtotime($service_end_time)); ?></p>
                </div>   
            </div>
            <div class="row">
                <div class="col-sm-6"><p><strong>Allocated Time : </strong> 1 Hour</p></div>
                <div class="col-sm-6"><p><strong>Expended Time :</strong> <?=$service_time_spent; ?></p></div>
            </div>
            <?php
            if(!empty($additional_time) && $additional_time>0)
            {
                ?>
                <div class="row">
                  <div class="col-sm-6"> 
                    <p><strong>Additional Time :</strong> <?=$additional_time; ?></p>    
                  </div>
                  <div class="col-sm-6"> 
                    <p><strong>Additional Amount : </strong><?=$additional_amount; ?></p>
                  </div>   
                </div>
                <?php
            }
          }
          elseif(!empty($is_otp_verified) && !empty($is_service_started))
          {
              ?>
                <div class="row paid">
                  <div class="col-sm-12" id="endwrapper"></div>
                  <div class="col-sm-9"> 
                    <p><strong>Started Time : <?=date('Y-m-d h:i:s',strtotime($service_start_time)); ?></strong></p>    
                  </div>
                  <div class="col-sm-3"> 
                    <p><input type="button" id="end_job" name="end_job" value="End" data-attr-id="<?=$rawId?>" class="btn btn-danger"></p>    
                  </div>   
                </div>
              <?php
          }
          elseif(!empty($is_otp_verified))
          {
            ?>    
            <div class="row total">
              <div class="col-sm-12 text-center" id="startwrapper"> 
                <p><input type="button" id="start_job" name="start_job" value="Start" data-attr-id="<?=$rawId?>" class="btn btn-success"></p>    
              </div>   
            </div>
            <?php
          }
          else
          {
            ?>
            <div class="row total">
                <div class="col-sm-6"> 
                    <p>Before Start,<br> Ask to customer and put here OTP for varification.</p>   
                </div>  
                <div class="col-sm-6" id="otpwrapper">
                  <div class="row" style="border: none;">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"><input type="button" id="verifyotp" name="verify" value="Send OTP" data-attr-id="<?=$rawId?>" class="btn btn-primary"></div>
                  </div>
                </div>
            </div>
            <?php
          }
          ?>
        </div>
        <?php */ ?>

      
    </div>
    <?php
    }
    ?>   
  </div>
<a title="Back" class="cmst btn btn-secondary" href="<?=base_url('operations') ?>" >Back</a>