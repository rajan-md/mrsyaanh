<div class="col-sm-8">
	<div class="dashboarCcontainer">
		<div class="inner-dash-space">
			<div class="service-provide-col">
				<div class="text-center sec-head">
					<h3><?=$this->lang->line('SECTION_A');?></h3>
					<p><?=$this->lang->line('PERSONAL_BUSINESS_INFORMATION');?></p>
				</div>
				<div class="service-provide-form">
					<?php 
					//echo "<pre>"; print_r($businessDetails); echo "</pre>";
					$service_status=isset($businessDetails->service_status)?$businessDetails->service_status:"";
					if($service_status!=1)
					{
					?>
					<form method="post" autocomplete="off" enctype="multipart/form-data">
						<input type="hidden" name="service_id" id="service_id" value="<?=set_value('service_id',isset($businessDetails->id)?$businessDetails->id:" ")?>">
						<input type="hidden" id="latitude" name="latitude" value="<?=set_value('latitude',isset($userDetails->latitude)?$userDetails->latitude:" ")?>">
						<input type="hidden" id="longitude" name="longitude" value="<?=set_value('longitude',isset($userDetails->longitude)?$userDetails->longitude:" ")?>">
						<input type="hidden" id="mobbox" name="mobbox">
						<input type="hidden" id="telephonebox" name="telephonebox">
						<input type="hidden" id="bussmobbox" name="bussmobbox">

						<h4><?=$this->lang->line('PERSONAL_DETAILS');?></h4>

					    <div class="row">					    
							<div class="col-sm-6">
								<div class="form-group">						 
								<label class="profile_field"><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>
									<input type="text" name="fname" class="form-control" id="fname" value="<?=set_value('fname',isset($userDetails->fname)?$userDetails->fname:" ")?>" required="">
								</div>
							 </div>						 
							 <div class="col-sm-6">
							  	<label class="profile_field"><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>
							  	<input type="text" name="lname" class="form-control" id="lname" value="<?=set_value('lname',isset($userDetails->lname)?$userDetails->lname:" ")?>" required="">
							 </div>						 
							<div class="col-sm-12">
								<div class="form-group">
								   <label class="profile_field col-sm-1 no-padding"><?=$this->lang->line('GENDER');?><span class="redstar">*</span></label>
								  	<?php $gender=set_value('gender',isset($userDetails->gender)?$userDetails->gender:""); ?>
									<label class="cus_radio">
		                  				<input type="radio" name="gender" <?php if($gender=='male'){ echo 'checked="checked"'; } ?>  value="male"><?=$this->lang->line('MALE');?>
		                  			</label>
									<label class="cus_radio">
					                  	<input type="radio" name="gender" <?php if($gender=='female'){ echo 'checked="checked"'; } ?> value="female"><?=$this->lang->line('FEMALE');?>
					                </label>                  
					                <label class="cus_radio">
					                  	<input type="radio" name="gender" <?php if($gender=='others'){ echo 'checked="checked"'; } ?> value="Others"><?=$this->lang->line('OTHERS');?>
					                </label> 
				                </div>                 
							</div>

							<div class="col-sm-6">
	                              <div class="form-group">                         
		                                <label class="profile_field"><?=$this->lang->line('DATE_OF_BIRTH');?><span class="redstar">*</span></label>
		                                <input type="text" name="dob" class="form-control" id="dob" value="<?=set_value('dob',isset($userDetails->dob)?$userDetails->dob:"")?>" placeholder="yyyy-mm-dd" required="" autocomplete="off">
	                              </div>
                             </div>
                             
                            <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>
                                  <input type="email" id="f1-email" name="useremail" placeholder="Email..." pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" class="f1-email form-control" id="f1-email" value="<?=set_value('email',isset($userDetails->email)?$userDetails->email:"")?>" disabled="disabled" required="">
                              </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group personal_phonebox">
                                    <label class="profile_field"><?=$this->lang->line('MOBILE');?><span class="redstar">*</span></label>
                                    <input type="text" name="mobile" placeholder="<?=$this->lang->line('MOBILE');?>" class="form-control" id="phonecode" value="<?=set_value('mobile',isset($userDetails->mobile)?$userDetails->mobile:"")?>" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">                         
                                    <label class="profile_field"><?=$this->lang->line('PROFILE_PIC');?></label>
                                    <input type="file" name="featured_img" id="featured_img">
                                </div>
                            </div>

						</div>
						
						<h4><?=$this->lang->line('HOME_ADDRESS');?></h4>

						<div class="row">

						<div class="col-sm-6">					
							<div class="form-group">							 
								<label class="profile_field"><?=$this->lang->line('COUNTRY');?><span class="redstar">*</span></label>
								<?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($userDetails->country)?$userDetails->country:""),'id="country" class="form-control" required=""'); ?>
							</div>
						</div>

						<div class="col-sm-6">					
							<div class="form-group">						 
								<label class="profile_field"><?=$this->lang->line('STATE_PROVINCE');?><span class="redstar">*</span></label>
								<?php
								$stateOptions = !empty($userDetails->country)?getStatesOptions($userDetails->country):array(''=>'---Choose State/Province---');
								echo form_dropdown('state',$stateOptions,set_value('state',isset($userDetails->state)?$userDetails->state:""),'id="administrative_area_level_1" class="form-control" required=""');
								?>
							</div>
						 </div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="profile_field"><?=$this->lang->line('ADDRESS');?><span class="redstar">*</span></label>
								<input type="text" onclick="initialize();" name="site_location" class="form-control" id="site_location" value="<?=set_value('site_location',isset($userDetails->site_location)?$userDetails->site_location:"")?>" required="">
							</div>
						</div>
						
						<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('APT_UNIT_NO');?></label>

							<input type="text" name="aptno" class="form-control" id="aptno" value="<?=set_value('aptno',isset($userDetails->aptno)?$userDetails->aptno:"")?>">

							</div>
						 </div>
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_NO');?><span class="redstar">*</span></label>

							<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_NAME');?><span class="redstar">*</span></label>

							<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required="">

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_TYPE');?><span class="redstar">*</span></label>
							<?php echo form_dropdown('street_type', streetTypeOption(),set_value('street_type',isset($userDetails->street_type)?$userDetails->street_type:""),'id="street_type" class="form-control" required=""'); ?>

							</div>
						 </div>
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('DIRECTION');?></label>
							<?php echo form_dropdown('direction', directionOption(),set_value('direction',isset($userDetails->direction)?$userDetails->direction:""),'id="direction" class="form-control"'); ?>

							</div>
						 </div>						
						 
						<div class="col-sm-6">					
							<div class="form-group">						 
								<label class="profile_field"><?=$this->lang->line('CITY');?><span class="redstar">*</span></label>
								<input type="text" name="city" class="form-control" id="locality" value="<?=set_value('city',isset($userDetails->city)?$userDetails->city:"")?>" required="">
							</div>
						 </div> 
						 
						<div class="col-sm-6">					
							<div class="form-group">						 
								<label class="profile_field"><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>
								<input type="text" name="postal_code" class="form-control" id="postal_code" value="<?=set_value('postal_code',isset($userDetails->postal_code)?$userDetails->postal_code:"")?>" required="">
							</div>
						 </div>
						
						<div class="col-sm-6">
                            <div class="form-group telephone_box">
                                <label class="profile_field"><?=$this->lang->line('HOME_TEL_NO');?><span class="redstar">*</span></label>
                                <input type="text" name="telephone" placeholder="<?=$this->lang->line('HOME_TEL_NO');?>" class="form-control" id="telephone" value="<?=set_value('telephone',isset($userDetails->telephone)?$userDetails->telephone:"")?>" required="">
                            </div>
                        </div>
						 
						 
						 </div><!--row-->

						<h4><?=$this->lang->line('BUSINESS_INFORMATION'); ?></h4>

						<!---------------------------business-section----------------------------->
						<?php
                        $business=isset($businessDetails->business)?$businessDetails->business:"no";
                        $field_require = (!empty($business) && $business=='yes')?'required="required"':'';
                        ?>
						<div class="form-group">
						<label><?=$this->lang->line('DO_YOU_HAVE_OWN_A_BUSINESS_NOW');?>?<span class="redstar">*</span></label>
						<div>
							
			                <label class="cus_radio">
			                  <input type="radio" name="business" value="no" required="" <?php if($business=='no') { ?> checked="checked" <?php } ?> >
			                  <?=$this->lang->line('NO');?></label>
			                  <label class="cus_radio">
			                  <input type="radio" name="business" value="yes" required="" <?php if($business=='yes') { ?> checked="checked" <?php } ?>>
			                  <?=$this->lang->line('YES');?></label>
						</div>
					</div>
						 
						 <div id="businessDetails" class="w-100 mt20"  style="display: <?php if($business=='yes') { echo 'block'; } else { echo 'none'; } ?>">
						 <div class="row">
							 <div class="col-sm-12">					
									<div class="form-group">						 
										<label class="profile_field"><?=$this->lang->line('BUSINESS_NAME');?><span class="redstar">*</span></label>
										<input type="text" name="business_name" class="form-control" id="business_name" value="<?=set_value('business_name',isset($businessDetails->business_name)?$businessDetails->business_name:"")?>"  <?=$field_require;?>>
									</div>
							 </div>
						 
						 <div class="col-sm-6">					
								<div class="form-group">						 
								<label class="profile_field"><?=$this->lang->line('BUSINESS_TYPE');?><span class="redstar">*</span></label>
								<?php echo form_dropdown('business_type', bussinessTypeOption(),set_value('business_type',isset($businessDetails->business_type)?$businessDetails->business_type:""),'id="business_type" class="form-control" '.$field_require); ?>
								</div>
						 </div>
						 
						<div class="col-sm-6">
	                        <div class="form-group">                           
	                            <label class="profile_field"><?=$this->lang->line('DATE_STARTED');?><span class="redstar">*</span></label>
	                            <input type="text" name="start_date" class="datepicker form-control" autocomplete="off" placeholder="yyyy-mm-dd" id="start_date" value="<?=set_value('start_date',isset($businessDetails->start_date)?$businessDetails->start_date:"")?>" <?=$field_require;?>>
	                        </div>
	                      </div>
						 
						 <div class="col-sm-12">
						<?php $license=isset($businessDetails->license)?$businessDetails->license:"no"; ?>
						<div class="form-group">
						<label><?=$this->lang->line('DO_YOU_HAVE_A_LICENSE');?>?<span class="redstar">*</span></label>
						<div>
							 <label class="cus_radio">
			                  <input type="radio" name="license" value="no" required="" <?php if($license=='no') { ?> checked="checked" <?php } ?>>
			                  <?=$this->lang->line('NO');?></label>
							<label class="cus_radio">
			                  <input type="radio" name="license" value="yes" required="" <?php if($license=='yes') { ?> checked="checked" <?php } ?>>
			                  <?=$this->lang->line('YES');?></label>
			               
						</div>
						</div>
					</div>
					<div class="col-sm-12">
						<?php $license_require = (!empty($license) && $license=='yes')?'required="required"':''; ?>
						<div id="licenseDetails" style="display:<?php if($license=='yes') { echo 'block'; } else { echo 'none'; } ?>">
							<div class="form-group">
								<label class="profile_field"><?=$this->lang->line('LICENSE_DETAILS');?><span class="redstar">*</span></label>
								<input type="text" id="license_details" name="license_details" placeholder="License Details" class="f1-last-name form-control" value="<?=set_value('license_details',isset($businessDetails->license_details)?$businessDetails->license_details:"")?>" <?=$license_require;?>>
							</div>
						</div>
					</div>
				</div>
					<h4><?=$this->lang->line('BUSSINESS_ADDRESS'); ?></h4>
					<div class="row">
					<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('COUNTRY');?><span class="redstar">*</span></label>

							<?php echo form_dropdown('business_country', getCountriesOptions(),set_value('business_country',isset($businessDetails->business_country)?$businessDetails->business_country:""),'id="business_country" class="form-control" '.$field_require); ?>

							</div>
						 </div>

						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STATE_PROVINCE');?><span class="redstar">*</span></label>
							<?php
								$bStateOptions = !empty($businessDetails->business_country)?getStatesOptions($businessDetails->business_country):array(''=>'---Choose State/Province---');
								echo form_dropdown('business_state',$bStateOptions,set_value('business_state',isset($businessDetails->business_state)?$businessDetails->business_state:""),'id="business_state" class="form-control" '.$field_require);
								?>
							</div>
						 </div>

						<div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('APT_UNIT_NO');?></label>

							<input type="text" name="business_aptno" class="form-control" id="business_aptno" value="<?=set_value('business_aptno',isset($businessDetails->business_aptno)?$businessDetails->business_aptno:"")?>">

							</div>
						 </div>
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_NO');?><span class="redstar">*</span></label>

							<input type="text" name="business_street_no" class="form-control" id="business_street_no" value="<?=set_value('business_street_no',isset($businessDetails->business_street_no)?$businessDetails->business_street_no:"")?>" <?=$field_require;?>>

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_NAME');?><span class="redstar">*</span></label>

							<input type="text" name="business_street_name" class="form-control" id="business_street_name" value="<?=set_value('business_street_name',isset($businessDetails->business_street_name)?$businessDetails->business_street_name:"")?>" <?=$field_require;?>>

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('STREET_TYPE');?><span class="redstar">*</span></label>

						<?php echo form_dropdown('business_street_type', streetTypeOption(),set_value('business_street_type',isset($businessDetails->business_street_type)?$businessDetails->business_street_type:""),'id="business_street_type" class="form-control" '.$field_require); ?>

							</div>
						 </div>
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('DIRECTION');?></label>

							<?php echo form_dropdown('business_direction', directionOption(),set_value('business_direction',isset($businessDetails->business_direction)?$businessDetails->business_direction:""),'id="business_direction" class="form-control" '); ?>
							</div>
						 </div>

						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('CITY');?><span class="redstar">*</span></label>

							<input type="text" name="business_city" class="form-control" id="business_locality" value="<?=set_value('business_city',isset($businessDetails->business_city)?$businessDetails->business_city:"")?>" <?=$field_require;?>>

							</div>
						 </div>
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>

							<input type="text" name="business_zip" class="form-control" id="business_zip" value="<?=set_value('business_zip',isset($businessDetails->business_zip)?$businessDetails->business_zip:"")?>" <?=$field_require;?>>

							</div>
						 </div>
						 
						 
						 
						 <div class="col-sm-6">					
						<div class="form-group bussiness_phonebox">
						 
							<label class="profile_field"><?=$this->lang->line('BUSINESS_TEL_NO');?><span class="redstar">*</span></label>

							<input type="text" name="business_mobile" class="form-control" id="bussphonecode" value="<?=set_value('business_mobile',isset($businessDetails->business_mobile)?$businessDetails->business_mobile:"")?>" <?=$field_require;?>>

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('BUSINESS_FAX');?></label>

							<input type="text" name="business_fax" class="form-control" id="business_fax" value="<?=set_value('business_fax',isset($businessDetails->business_fax)?$businessDetails->business_fax:"")?>">

							</div>
						 </div>
						 
						 <div class="col-sm-6">					
						<div class="form-group">
						 
							<label class="profile_field"><?=$this->lang->line('BUSINESS_EMAIL');?><span class="redstar">*</span></label>

							<input type="text" name="business_email" class="form-control" id="business_email" value="<?=set_value('business_email',isset($businessDetails->business_email)?$businessDetails->business_email:"")?>" <?=$field_require;?>>

							</div>
						 </div>
					 
						
						</div>

						<h4><?=$this->lang->line('BANK_INFORMATION'); ?></h4>
                        
                        <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('BANK_NAME');?></label>
                                  <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder="<?=$this->lang->line('BANK_NAME');?>" value="<?=set_value('bank_name',isset($businessDetails->bank_name)?$businessDetails->bank_name:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('BANK_ADDRESS');?></label>
                                  <input type="text" name="bank_address" class="form-control" id="bank_address" placeholder="<?=$this->lang->line('BANK_ADDRESS');?>" value="<?=set_value('bank_address',isset($businessDetails->bank_address)?$businessDetails->bank_address:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('BANK_CODE');?></label>
                                  <input type="text" name="bank_code" class="form-control" id="bank_code" placeholder="<?=$this->lang->line('BANK_CODE');?>" value="<?=set_value('bank_code',isset($businessDetails->bank_code)?$businessDetails->bank_code:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('ACCOUNT_NO');?></label>
                                  <input type="text" name="account_no" class="form-control" id="account_no" placeholder="<?=$this->lang->line('ACCOUNT_NO');?>" value="<?=set_value('account_no',isset($businessDetails->account_no)?$businessDetails->account_no:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('SWIFT_NO');?></label>
                                  <input type="text" name="swift_no" class="form-control" id="swift_no" placeholder="<?=$this->lang->line('SWIFT_NO');?>" value="<?=set_value('swift_no',isset($businessDetails->swift_no)?$businessDetails->swift_no:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('TRANSITION_NO');?></label>
                                  <input type="text" name="transition_no" class="form-control" id="transition_no" placeholder="<?=$this->lang->line('TRANSITION_NO');?>" value="<?=set_value('transition_no',isset($businessDetails->transition_no)?$businessDetails->transition_no:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('ACCOUNT_TYPE');?></label>
                                  <?php echo form_dropdown('account_type', array("Chequing"=>"Chequing","Saving"=>"Saving"),set_value('account_type',isset($businessDetails->account_type)?$businessDetails->account_type:""),'id="account_type" class="form-control" '); ?>
                              </div>
                          </div>
                        </div>			
						

                        <h4><?=$this->lang->line('CAR_SPECIFICATION'); ?></h4>
                        
                        <div class="row">
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('DRIVER_LICENCE');?></label>
                                  <input type="text" name="driver_licence" class="form-control" id="driver_licence" placeholder="<?=$this->lang->line('DRIVER_LICENCE');?>" value="<?=set_value('driver_licence',isset($businessDetails->driver_licence)?$businessDetails->driver_licence:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('MADE');?></label>
                                  <input type="text" name="made" class="form-control" id="made" placeholder="<?=$this->lang->line('MADE');?>" value="<?=set_value('made',isset($businessDetails->car_made)?$businessDetails->car_made:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('MODEL');?></label>
                                  <input type="text" name="model" class="form-control" id="model" placeholder="<?=$this->lang->line('MODEL');?>" value="<?=set_value('model',isset($businessDetails->car_model)?$businessDetails->car_model:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('YEAR');?></label>
                                  <input type="text" name="year" class="form-control" id="year" placeholder="<?=$this->lang->line('YEAR');?>" value="<?=set_value('year',isset($businessDetails->car_year)?$businessDetails->car_year:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('COLOR');?></label>
                                  <input type="text" name="color" class="form-control" id="color" placeholder="<?=$this->lang->line('COLOR');?>" value="<?=set_value('color',isset($businessDetails->car_color)?$businessDetails->car_color:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('LICENCE_PLATE_NO');?></label>
                                  <input type="text" name="licence_plate_no" class="form-control" id="color" placeholder="<?=$this->lang->line('LICENCE_PLATE_NO');?>" value="<?=set_value('licence_plate_no',isset($businessDetails->car_number)?$businessDetails->car_number:"")?>">
                              </div>
                          </div>
                          <div class="col-sm-6">                 
                              <div class="form-group">                         
                                  <label class="profile_field"><?=$this->lang->line('PICTURE');?></label>
                                  <input type="file" name="picture" class="form-control" id="picture" placeholder="<?=$this->lang->line('PICTURE');?>">
                                  <?php
                                  if(!empty($businessDetails->car_picture))
	                              {
	                              	echo '<img src="'.base_url($businessDetails->car_picture).'" width="80">';
	                              }
	                              ?>
                              </div>
                          </div>
                        </div>
                    </div>
		
						<!---------------------------end business-section----------------------------->						
						<div class="form-group mt20">
							<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>">
						</div>
					</form>

				<?php
				}
				else
				{
				?>
					<div class="underProgress">
						<?=$this->lang->line('CONTRACT_INFO');?>
						<br><br>
	                	<span class="btn btn-success"><?=$this->lang->line('REQUEST_STATUS');?> : <?=$this->lang->line('APPROVED');?></span>
	                	<br><br>
	                	<a target="_blank" href="<?=base_url('download-contract');?>"><span class="btn btn-info"><?=$this->lang->line('DOWNLOAD_CONTRACT');?></span></a>
					</div>
				<?php
				}
				?>
				</div>
				<!--service-provide-form-->


			</div>
			<!--service-provide-col-->

		</div>

	</div>

</div>