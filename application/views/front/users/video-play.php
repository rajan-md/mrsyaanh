    <div class="col-md-8 col-lg-8">
    	<div class="row">
    		<?php
    		$source = !empty($record->video_source)?$record->video_source:'Youtube';
    		$video_url = !empty($record->video)?$record->video:'';
    		if(!empty($video_url) && !empty($source))
    		{
    			if($source == 'Vimeo')
	    		{
	    			$video_id = vimeo_video_id($video_url);
	    			if(!empty($video_id))
	    			{
	    			?>
	    				<iframe src="https://player.vimeo.com/video/<?=$video_id;?>" width="100%" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	    				
	    			<?php
	    			}
	    		}
	    		else
	    		{
	    			$video_id = youtube_video_id($video_url);
	    			if(!empty($video_id))
	    			{
	    			?>
	    				<iframe width="100%" height="450" src="https://www.youtube.com/embed/<?=$video_id;?>" frameborder="0" allow="autoplay;" allowfullscreen></iframe>
	    			<?php
	    			}
	    		}
    		}    		
    		?>
    		<div class="addOpt"><a href="<?=base_url('service-video');?>" class="cmst btn btn-success"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?=$this->lang->line('BACK');?></a></div>
    	</div>
	</div> 