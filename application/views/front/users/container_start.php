<div class="container">
  <div class="row">
    <div class="col-sm-12 dashboardMessageDispaly">
      <div class="title">
        <h3><?=isset($page_title)?$page_title:"Not Found"?></h3>
      </div>
       

        <?php $success= $this->session->flashdata('message'); 
        if(!empty($success)) { ?>
        <div class="alert alert-success">
        <?php echo $this->session->flashdata('message'); ?>
        </div>
        <div style="height:20px; clear:both;"></div>
        <?php } ?>
        <?php $danger= $this->session->flashdata('error'); 
        if(!empty($danger)) { ?>
        <div class="alert alert-danger" style="padding: 8px;">
        <?php echo $this->session->flashdata('error'); ?>
        </div>
        <div style="height:20px; clear:both;"></div>
        <?php } ?>

        
    </div>
  </div>
  <div class="row">