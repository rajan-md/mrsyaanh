<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">


		<form method="post" autocomplete="off" enctype="multipart/form-data">	

		

	    <input type="hidden" name="route" id="route">                

	    <input type="hidden" id="latitude" name="latitude" value="<?=set_value('latitude',isset($userDetails->latitude)?$userDetails->latitude:"")?>">          

	    <input type="hidden" id="longitude" name="longitude" value="<?=set_value('longitude',isset($userDetails->longitude)?$userDetails->longitude:"")?>">
        <input type="hidden" id="mobbox" name="mobbox">
		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('FIRST_NAME');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="fname" class="form-control" id="fname" value="<?=set_value('fname',isset($userDetails->fname)?$userDetails->fname:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('LAST_NAME');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="lname" class="form-control" id="lname" value="<?=set_value('lname',isset($userDetails->lname)?$userDetails->lname:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('EMAIL');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="email" class="form-control" id="email" value="<?=set_value('email',isset($userDetails->email)?$userDetails->email:"")?>" required="">

			</div>

		</div>

		<div class="form-group row personal_phonebox">

			<label class="col-md-3 profile_field"><?=$this->lang->line('MOBILE');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="mobile" class="form-control" id="phonecode" value="<?=set_value('mobile',isset($userDetails->mobile)?$userDetails->mobile:"")?>" required="">

			</div>

		</div>
		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('GENDER');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<?php $gender=set_value('gender',isset($userDetails->gender)?$userDetails->gender:""); ?>

				<label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='male'){ echo 'checked="checked"'; } ?>  value="male">

                  <?=$this->lang->line('MALE');?></label>

                <label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='female'){ echo 'checked="checked"'; } ?> value="female">

                  <?=$this->lang->line('FEMALE');?></label>
                  <label class="cus_radio">

                  <input type="radio" name="gender" <?php if($gender=='others'){ echo 'checked="checked"'; } ?> value="others">

                  <?=$this->lang->line('OTHERS');?></label>

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('DATE_OF_BIRTH');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="dob" class="form-control" id="dob" value="<?=set_value('dob',isset($userDetails->dob)?$userDetails->dob:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('ADDRESS');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" onclick="initialize();" name="site_location" class="form-control" id="site_location" value="<?=set_value('site_location',isset($userDetails->site_location)?$userDetails->site_location:"")?>" required="">

			</div>

		</div>		

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('APT_UNIT_NO');?></label>

			<div class="col-md-8">

				<input type="text" name="aptno" class="form-control" id="aptno" value="<?=set_value('aptno',isset($userDetails->aptno)?$userDetails->aptno:"")?>">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('STREET_NO');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($userDetails->street_number)?$userDetails->street_number:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('STREET_NAME');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($userDetails->street_name)?$userDetails->street_name:"")?>" required="">

			</div>

		</div>



		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('STREET_TYPE');?><span class="redstar">*</span></label>

			<div class="col-md-8">
				<?php echo form_dropdown('street_type', streetTypeOption(),set_value('street_type',isset($userDetails->street_type)?$userDetails->street_type:""),'id="street_type" class="form-control" required=""'); ?>
			</div>

		</div>



		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('DIRECTION');?></label>

			<div class="col-md-8">

				<?php echo form_dropdown('direction', directionOption(),set_value('direction',isset($userDetails->direction)?$userDetails->direction:""),'id="direction" class="form-control"'); ?>

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('COUNTRY');?><span class="redstar">*</span></label>

			<div class="col-md-8">
				<?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($userDetails->country)?$userDetails->country:""),'id="country" class="f1-email form-control" required=""'); ?>
			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('STATE_PROVINCE');?><span class="redstar">*</span></label>

			<div class="col-md-8">
				<?php
				$stateOptions = !empty($userDetails->country)?getStatesOptions($userDetails->country):array(''=>'---Choose State/Province---');
				echo form_dropdown('state',$stateOptions,set_value('state',isset($userDetails->state)?$userDetails->state:""),'id="administrative_area_level_1" class="form-control" required=""');
				?>
			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('CITY');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="city" class="form-control" id="locality" value="<?=set_value('city',isset($userDetails->city)?$userDetails->city:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('ZIP_CODE');?><span class="redstar">*</span></label>

			<div class="col-md-8">

				<input type="text" name="postal_code" class="form-control" id="postal_code" value="<?=set_value('postal_code',isset($userDetails->postal_code)?$userDetails->postal_code:"")?>" required="">

			</div>

		</div>

		<div class="form-group row">

			<label class="col-md-3 profile_field"><?=$this->lang->line('PROFILE_PIC');?></label>

			<div class="col-md-8">

				<input type="file" name="featured_img" id="featured_img">

			</div>

		</div>

		

		

		<div class="form-group row">

			<label class="col-md-3 profile_field"></label>

			<div class="col-md-2">

				<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>">

			</div>

		</div>

	</form>

	</div>

</div>