<div class="col-sm-8"> 
    <div class="dashboarCcontainer table-responsive">
    <table class="table customer-request">
    <thead>
      <tr>
        <th><?=$this->lang->line('NAME');?></th>
        <th><?=$this->lang->line('EMAIL');?></th>
        <th><?=$this->lang->line('CONTACT');?></th>
        <th><?=$this->lang->line('STATUS');?></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($operators))
    {
      foreach ($operators as $operator)
      { 
        $code=base64_encode(isset($operator->id)?$operator->id:"");
        ?>
        <tr>
          <td><?=!empty($operator->username)?$operator->username:""?></td>
          <td><?=!empty($operator->email)?$operator->email:""?></td>
          <td><?=!empty($operator->mobile)?$operator->mobile:""?></td>
          <td>
          <?php $status=isset($operator->status)?$operator->status:""; 
          if($status==0) { ?>
            <a title="Click to Activate" href="<?=base_url('operator-status/'.$code) ?>"><span class="badge badge-warning">Deactive</span></a>
            <?php } else { ?>
            <a title="Click to Deactivate" href="<?=base_url('operator-status/'.$code) ?>"><span class="badge badge-success">Active</span></a>
          <?php } ?>
          </td>
          <td class="text-right">
          	<a title="Edit Operator" class="dmst btn btn-info" href="<?=base_url('edit-operator/'.$code) ?>">
              <i class="far fa-edit"></i>
            </a>
            <a title="Delete Operator" class="cmst btn btn-danger" onclick="return confirm('Sure you want to delete this operator')"  href="<?=base_url('delete-operator/'.$code) ?>" >
              <i class="fa fa-times" aria-hidden="true"></i>
            </a>
      	  </td>
        </tr>
      <?php
    }
  }
  else
  {
    ?>
    <tr><td colspan="5">No operators available....</td></tr>
    <?php
  }
  ?>      
    </tbody>
  </table>
 </div>
<div class="clerfix"></div> 

<div class="addOpt"><a href="<?=base_url('add-operator') ?>" class="cmst btn btn-success"><i class="fa fa-user-plus" aria-hidden="true"></i> <?=$this->lang->line('ADD_OPERATOR');?></a></div>
<div class="pagination"><?=isset($pagination)?$pagination:""?></div> 
<div class="clerfix"></div> 

</div>