<?php 
$editprofile=getUserProfileData(); ?>


  <div class="infoRight">
    <h3><img src="<?=base_url('skin/front')?>/images/icon-arrow.png"><?=$this->lang->line('INBOX'); ?>  </h3>   
   
   <div class="table-responsive">
  <table class="table">
 <tr>
 	<th><?=$this->lang->line('TITLE'); ?></th>
 	<th><?=$this->lang->line('TYPE'); ?></th>
 	<th width="75"><?=$this->lang->line('ACTION'); ?></th>
 </tr>
 <?php 
 //print_r($inboxdata);
 if(!empty($inboxdata)) {
 foreach ($inboxdata as $inbox) { 
    $type=$inbox->type;
     $read=$inbox->read;
  ?>
 <tr>
 	<td>
  <?php if($read==1){ ?>
   <b><?=$inbox->title?></b> &nbsp;<img src="<?=base_url('skin/front/images/new_icon.gif')?>"/>
  <?php } else { ?>
  <?=$inbox->title?>
  <?php }  ?>
  </td>
 	<td><?php 
    
     if(($type==1) && ($read==1)){ 
     	echo '<b>Emergency Triangle</b>';
     } else if(($type==1) && ($read==0)){
     	echo 'Emergency Triangle';
     } else {
     	echo 'Nornal';
     }

 	?></td>
 	
 	<td><a title="View" class="label label-success" href="<?=base_url('profile/inbox/view/'.$inbox->id)?>"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;
    <a title="<?=$this->lang->line('DELETE_TEXT');?>" class="label label-danger" onclick="return confirm('Sure want to delete this message')"  href="<?=base_url('profile/inbox/delete/'.$inbox->id)?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
 	</td>
 </tr>
 <?php } } ?>
 </table>
 </div>  
  
  </div>

