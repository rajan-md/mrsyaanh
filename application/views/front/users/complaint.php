<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">
		<form method="post" autocomplete="off" enctype="multipart/form-data" class="newservice">
				<?php getNotificationHtml(); ?>
					<div class="row">
						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="service_orders"><?=$this->lang->line('CHOOSE_SERVICE_ORDER');?><span class="redstar">*</span></label>
								<?php                       
		                      	echo form_dropdown('service_order',$service_orders,set_value('service_order',isset($records->service_id)?$records->service_id:''),'id="service_order" class="form-control" required');
		                      	echo form_error('service_order');
		                      	?>
							</div>
						</div>

						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="support_title"><?=$this->lang->line('COMPLAINT_SUBJECT');?><span class="redstar">*</span></label>
								<?php echo form_input(array('id' => 'complaint_subject', 'name' => 'complaint_subject','class'=>'form-control','placeholder'=>$this->lang->line('COMPLAINT_SUBJECT'),'value' => set_value('complaint_subject', isset($complaint_subject)?$complaint_subject:"" ),'required'=>'required')); ?>
								<?php echo form_error('complaint_subject'); ?>
							</div>
						</div>

						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="support_concern"><?=$this->lang->line('COMPLAINT_CONCERN');?><span class="redstar">*</span></label>
								<?php
								echo form_textarea(array('id'=>'complaint_concern','name' => 'complaint_concern','class'=>'form-control','rows' => 5,'cols' => 25,'placeholder'=>$this->lang->line('TYPE_COMMENT'),'value' => set_value('complaint_concern', isset($complaint_concern)?$complaint_concern:"" ),'required'=>'required'));
		                      	echo form_error('complaint_concern');
		                      	?>
							</div>
						</div>


					 </div><!--row-->

				<div class="row">
					<div class="col-md-12 form-group">
						<input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>">
					</div>
				</div>

	</form>

	</div>

</div>