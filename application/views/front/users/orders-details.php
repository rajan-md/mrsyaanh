<?php $userRole=getLoginUserRole(); ?>
<div class="col-md-8">
  <div class="row ser-rep">
      <?php if(!empty($orderDetails)){ ?>
      <div class="col-md-12">
        <div class="payCcontainer">
          <h2><i class="fa fa-check-circle"></i> <?=$this->lang->line('ORDERED_PLACED');?> <?=!empty($orderDetails->id)?': #'.$orderDetails->id:"";?></h2>
          <div class="serv-view row">
            <div class="col-md-6 padd-rght">
              <div class="serv-detail">      
                <p><strong><?=$this->lang->line('ORDER_ID');?> : #<?=!empty($orderDetails->id)?$orderDetails->id:"";?></strong></p>

                <p><strong><?=$this->lang->line('ORDERED_DATE');?> : </strong><?=date("j M, Y h:i A", strtotime(isset($orderDetails->orderdata)?$orderDetails->orderdata:"")); ?></p>

                <p><strong><?=$this->lang->line('STATUS');?> : </strong><?=!empty($orderDetails->orderstatus)?getStatus($orderDetails->orderstatus):""; ?></p>

                <?php
                $shippingMethod = shippingMethodOption();
                $paymentMethod = paymentMethodOption();
                ?> 

                 <p><strong><?=$this->lang->line('SHIPPING_METHOD');?> : </strong><?=!empty($orderDetails->shipping_method)?$shippingMethod[$orderDetails->shipping_method]:""; ?></p>
                <p><strong><?=$this->lang->line('PAYMENT_METHOD');?> : </strong><?=!empty($orderDetails->payment_method)?$paymentMethod[$orderDetails->payment_method]:""; ?></p>
              </div>
            </div>      
            <div class="col-md-6 padd-rght">
              <div class="serv-detail">     
                <strong><?=$this->lang->line('SHIPPING');?></strong>
                <hr>
                <p><?=shippingAddress(isset($orderDetails->id)?$orderDetails->id:"")?></p>                               
              </div>         
            </div>             
          </div>
          <div class="col-sm-12"> 
          <table cellpadding="5" cellspacing="5" width="100%" class="table">
            <thead>
              <tr>
                <th colspan="2"><?=$this->lang->line('PRODUCT');?></th>
                <th><?=$this->lang->line('QUANTITY');?></th>
                <th><?=$this->lang->line('UNIT_PRICE');?></th>
                <th><?=$this->lang->line('TOTAL');?></th>
              </tr>
            </thead>
            <?php
            if(!empty($orderItems))
            {
              ?>
              <tbody>
                  <?php
                  foreach ($orderItems as $item)
                  {
                    ?>
                    <tr>
                      <td width="80"><?=getProductImage($item->product_id); ?></td>
                      <td><?=getProductName($item->product_id); ?></td>
                      <td><?=$item->qty; ?></td>
                      <td><?=getPriceFormate($item->unit_price); ?></td>
                      <td><?=getPriceFormate($item->prices); ?></td>
                    </tr>
                    <?php
                  }
                  ?>

                  <tr>
                    <td colspan="2" rowspan="4">&nbsp;</td>
                    <td colspan="2" align="right"><strong><?=$this->lang->line('SUB_TOTAL');?></strong></td>
                    <td><?=isset($orderDetails->subtotal)?getPriceFormate($orderDetails->subtotal):""; ?></td>
                  </tr>

                  <tr>
                    <td colspan="2" align="right"><strong><?=$this->lang->line('DISCOUNT');?></strong></td>
                    <td><?=isset($orderDetails->discount)?getPriceFormate($orderDetails->discount):""; ?></td>
                  </tr>

                  <tr>
                    <td colspan="2" align="right"><strong><?=$this->lang->line('SHIPPING');?></strong></td>
                    <td><?=isset($orderDetails->shipping_amount)?getPriceFormate($orderDetails->shipping_amount):""; ?></td>
                  </tr>

                  <tr>
                    <td colspan="2" align="right"><strong><?=$this->lang->line('GRAND_TOTAL');?></strong></td>
                    <td><?=isset($orderDetails->total)?getPriceFormate($orderDetails->total):""; ?></td>
                  </tr>
                </tbody>
              <?php
            }
            ?>            
          </table>           
          </div>


          </div>
        </div>

      </div>

      <?php
    }
    ?>

     
  </div>
  <a title="<?=$this->lang->line('BACK');?>" class="cmst btn btn-secondary" href="<?=base_url('orders') ?>" ><?=$this->lang->line('BACK');?></a>
</div>