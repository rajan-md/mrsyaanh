<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">
    <?php if(!empty($bookingData)){ //echo "<pre>"; print_r($bookingData); echo "</pre>"; ?>
    <div class="col-md-8">
      <div class="payCcontainer">
        <?php
        $status=requestStatus($bookingData->id);
        echo getServiceBookingStatusHeading($status);
        $currency_code = !empty($bookingData->code)?$bookingData->code:"";
      ?>
      <div class="serv-view row">      
        <div class="col-md-4 padd-rght">
          <div class="serv-detail">      
            <strong><?=$this->lang->line('TYPE_OF_SERVICE');?></strong>
            <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
            <ul>
            <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
              if($otherServices!=0){
              $otherData=explode(',', $otherServices);
              $count=count($otherData);              
              foreach ($otherData as $key => $value) { ?>                
                <li><?=getOtherServicesName($value)?></li>
              <?php } } ?>
              </ul>
          </div>        
        </div>      
        <div class="col-md-8 padd-rght">
          <div class="serv-detail">     
          <strong><?=$this->lang->line('ADDRESS');?></strong>
            <p><?=getServicesAddress(isset($bookingData->address_id)?$bookingData->address_id:"")?></p>  
          </div>        
        </div>             
      </div>

      <?php
      if(!empty($bookingData->additional_operator))
      {
        ?>
        <br>
        <div class="row">
          <div class="col-md-12 text-center additional_operator_box">
            <p>Booking is with "<strong>Additional Operator</strong>".</p>
          </div>
        </div>
        <?php
      }
      ?> 
           
      <div class="tech-user">        
      <i class="fas fa-user-circle"></i>Your <?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?> technician will be assigned <strong>1 hour</strong> before the scheduled time        
      </div>      
      <div class="row">
        <div class="col-sm-8">
            <div class="time_wrapper">
              <small><?=$this->lang->line('SERVICE_TIME');?></small>
              <time><?=date("j M, Y h:i A", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?></time>  
            </div>      
        </div>
      
      <div class="col-sm-4">
        <?php      
        if(!empty($status) && $status==1)
        {
            echo '<div class="time_sch" id="time_sch"><button id="assignToOperator">'.$this->lang->line('ASSIGN_OPERATOR').'</button></div>';
        }
        elseif(empty($status))
        {
          ?>
          <a title="<?=$this->lang->line('ACCEPT_REQUEST');?>" class="btn btn-success" href="<?=base_url('provider-accept-request/'.$code) ?>" >
          <?=$this->lang->line('ACCEPT');?></a>
          <a title="<?=$this->lang->line('REJECT_REQUEST');?>" class="btn btn-danger" href="<?=base_url('provider-reject-request/'.$code) ?>" ><?=$this->lang->line('REJECT');?></a>
        <?php
        }
        ?>
      </div>
      
      <div class="col-sm-12">
        <div id="assignBox" class="boxWithBg" style="display: none">
            <?php            
            if(!empty($bookingData->operator_id))
            {
              ?>
              <div class="row">        
                <div class="col-sm-3"><strong><?=$this->lang->line('ASSIGNED_TO');?> : </strong></div>
                <div class="col-sm-9"><?=getCustomerName($bookingData->operator_id);?></div>
              </div>
              <br>
              <?php  
            }
            ?>
            <div class="row">

            <?php
            if(!empty($operators))
            {
            ?>     
              <div class="col-sm-12 alert-msg"></div>
              <div class="col-sm-3"><?=$this->lang->line('CHOOSE_OPERATOR');?></div>
              <div class="col-sm-6">
                <?php
                if(!empty($operators))
                {                  
                  echo '<div class="operator-assign-box">
                  <select id="operator" name="operator" class="form-control" data-id="'.$rawId.'">
                  <option value="">------'.$this->lang->line('ASSIGN_OPERATOR').'-----</option>';
                  foreach ($operators as $operator)
                  {
                    $selected = (!empty($bookingData->operator_id) && $bookingData->operator_id==$operator->id)?'selected="selected"':'';
                    echo '<option value="'.$operator->id.'" '.$selected.'>'.$operator->username.'</option>';
                  }             
                  echo '</select>
                  </div>';
                }
                ?>
              </div>
              <div class="col-sm-3"><a class="btn common-btn" id="assignbtn" href="javascript:;"><?=$this->lang->line('SAVE');?></a></div>
            <?php
            }
            else
            {
              ?>
                <div class="col-sm-12"><p><?=$this->lang->line('NO_OPERATORS_TO_ASSIGN');?></p></div>                      
              <?php
            }
            ?>
        </div>
       </div>


       </div>
       </div>
      </div>

    <?php
    $provider_id = !empty($bookingData->vendor_id)?$bookingData->vendor_id:0;
    $operator_id = !empty($bookingData->operator_id)?$bookingData->operator_id:0;
    $user_id=$this->session->userdata('userID');
    if(!empty($provider_id) && !empty($operator_id) && $provider_id==$user_id)
    {
      ?>
        <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('ASSIGNED_OPERATOR');?></h4>    
          <div class="row">
              <div class="col-sm-12"> 
                <p><strong><?=$this->lang->line('NAME');?> :</strong> <?=getCustomerName($operator_id); ?></p>    
              </div>  
          </div>
          <div class="row">
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('EMAIL');?> :</strong> <?=getCustomerEmail($operator_id); ?></p>    
              </div>
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('CONTACT');?> : </strong><?=getCustomerContact($operator_id); ?></p>
              </div>   
          </div>                 
        </div>
        <br>
      <?php
    }
    ?>

    <?php
    if(!empty($is_service_started) && !empty($is_service_ended) && !empty($is_valid_vendor))
    {
    ?>
      <div class="price_detail2 clearfix">
        <h4><?=$this->lang->line('JOB_SUMMARY');?></h4>    
          <div class="row">
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('STARTED_TIME');?> :</strong> <?=date('d M, Y h:i A',strtotime($service_start_time)); ?></p>    
              </div>
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('ENDED_TIME');?> : </strong><?=date('d M, Y h:i A',strtotime($service_end_time)); ?></p>
              </div>   
          </div>
          <div class="row">
            <div class="col-sm-6"><p><strong><?=$this->lang->line('TIME_SPENT');?> : </strong> <?=$time_spent; ?></p></div>
            <div class="col-sm-6"><p><strong><?=$this->lang->line('ALLOTTED_TIME');?> : </strong> <?=$service_allocated; ?></p></div>
          </div>
          <?php
          if(!empty($additional_time) && $additional_time>0)
          {
            ?>
            <div class="row">
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('ADDITIONAL_TIME');?> :</strong> <?=$additional_time; ?></p>    
              </div>
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('ADDITIONAL_AMOUNT');?> : </strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>
              </div>
            </div>
            <?php
              if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
              {
                ?>
                <div class="row">
                  <div class="col-sm-6"> 
                    <p><strong><?=$this->lang->line('OUTSTANDING_PAYMENT_STATUS');?>: </strong>Paid via Paypal</p>
                  </div>
                  <div class="col-sm-6"> 
                    <p><strong><?=$this->lang->line('TRANSACTION_NUMBER');?>: </strong><?=$operation_row['transaction_id'];?></p>
                  </div>
                </div>
                <?php            
              }
              elseif(!empty($additional_amount) && empty($operation_row['transaction_id']))
              {
                ?>
                <div class="row">
                  <div class="col-sm-12 text-center"> 
                    <p style="color:#FF0000;"><strong>Still outstanding payment is pending by customer.</strong></p>
                  </div>
                </div>
                <?php
              }
          }

          if($status!=5)
          {
            ?>
            <div class="row">
                <div class="col-sm-12 text-center"> 
                  <a href="<?=base_url('close-service-request').'/'.$code;?>" id="serviceclose" class="btn btn-dark"><?=$this->lang->line('CLOSE_SERVICE_REQUEST');?></a>
                </div>
            </div>
            <?php
          }
          ?>       
      </div>
    <?php
    }
    ?>
    
    </div>
   
    <div class="col-md-4">
      <div class="price_detail2">
        <h4><?=$this->lang->line('PAYMENT_SUMMARY');?></h4>
        <?php
        $amount = !empty($bookingData->amount)?$bookingData->amount:0;
        $sub_total = !empty($bookingData->total_amount)?$bookingData->total_amount:'';
        if(empty($sub_total))
        {
            $sub_total = !empty($bookingData->amount)?$bookingData->amount:0;
        }
        $currency_code = !empty($bookingData->code)?$bookingData->code:"";
        $coupon = !empty($bookingData->coupon)?$bookingData->coupon:"";
        $discount = !empty($bookingData->discount)?$bookingData->discount:0;
        $tax = !empty($bookingData->tax)?$bookingData->tax:"";
        $other_tax = !empty($bookingData->other_tax)?$bookingData->other_tax:0;
        $tax_name = !empty($bookingData->tax_name)?$bookingData->tax_name:"";
        $tax_amount = !empty($bookingData->tax_amount)?$bookingData->tax_amount:"";
        ?>
        <div class="row">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SERVICE_CHARGES');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$amount)?></p>    
          </div>
        </div>
        <?php
        if(!empty($coupon) && !empty($discount))
        {
          ?>
          <div class="row total">
            <div class="col-sm-8 "> 
              <p><?=$this->lang->line('COUPON');?> (<?=$coupon;?>)</p>
            </div>
            
            <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$discount)?></p>
            </div>   
          </div> 
          <?php
        }
        ?>
        <?php
        if(!empty($tax_amount))
        {
          ?>
          <div class="row total">
            <div class="col-sm-8 "> 
              <?php $oTax = !empty($other_tax)?' + '.$other_tax.'%':''; ?>
            <p><?=$tax_name.' ('.$tax.'%'.$oTax.')';?></p>  
            </div>
            
            <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$tax_amount)?></p>    
            </div>   
          </div> 
          <?php
        }
        ?>
        <div class="row total">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SUB_TOTAL');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$sub_total)?></p>    
          </div>
        </div>
        <div class="row paid">
          <div class="col-sm-7"> 
            <p><?=$this->lang->line('AMOUNT_TO_BE_PAID');?></p>   
          </div>    
          <div class="col-sm-5 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$sub_total)?></strong></p>    
          </div>   
        </div>
      </div>

      <?php
      if(!empty($additional_time) && $additional_time>0)
      {
        ?>
        <br>
      <div class="price_detail2">
        <h4><?=$this->lang->line('ADDITIONAL_PAYMENT_SUMMARY');?></h4>
        <div class="row">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SERVICE_CHARGES');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_amount)?></p>    
          </div>
        </div>
        <?php
        if(!empty($additional_tax))
        {
          ?>
          <div class="row total">
            <div class="col-sm-8 "> 
              <?php $oTax = !empty($other_tax)?' + '.$other_tax.'%':''; ?>
              <p><?=$tax_name.' ('.$tax.'%'.$oTax.')';?></p>  
            </div>
            
            <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$additional_tax)?></p>    
            </div>   
          </div> 
          <?php
        }
        ?>
        <div class="row total">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SUB_TOTAL');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>    
          </div>
        </div>
        <div class="row paid">
          <div class="col-sm-7"> 
            <p><?=$this->lang->line('AMOUNT_TO_BE_PAID');?></p>   
          </div>    
          <div class="col-sm-5 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></strong></p>    
          </div>   
        </div>
      </div>

      <?php $total_paid_amt = $sub_total + $additional_subtotal; ?>
      <br>
      <div class="price_detail2">
       <div class="row grandtotal">
          <div class="col-sm-8"> 
            <p><strong><?=$this->lang->line('GRAND_TOTAL');?> : </strong></p>   
          </div>    
          <div class="col-sm-4 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$total_paid_amt)?></strong></p>    
          </div>   
        </div>
      </div>
        <?php
      }
      ?>

      <br>
      
      <div class="price_detail2">
          <h4><?=$this->lang->line('CUSTOMER_DETAILS');?></h4>
          <?php
          $customer = getCustomerDetailsById(isset($bookingData->user_id)?$bookingData->user_id:"");
          //echo "<pre>" ; print_r($customer); echo "</pre>";
          ?>
          <div class="row">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('NAME');?> : </p>   
            </div>  
              <div class="col-sm-8"> 
                <p><?=(!empty($customer->fname)?$customer->fname:"").' '.(!empty($customer->lname)?$customer->lname:"")?></p></div>
          </div>    
          <div class="row total">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('EMAIL');?> : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->email)?$customer->email:""?></p></div>   
          </div>
          <div class="row total">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('MOBILE');?> : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->mobile)?$customer->mobile:""?></p></div>   
          </div>  
          <div class="row paid">
            <div class="col-sm-4"> 
              <p><?=$this->lang->line('ADDRESS');?> : </p>   
            </div> 
            <div class="col-sm-8">
              <p><?=getServicesAddress(!empty($bookingData->address_id)?$bookingData->address_id:"")?></p>    
            </div>   
          </div>
      </div>
    </div>
    <?php } ?>
   
  </div>

  <a title="<?=$this->lang->line('BACK');?>" class="cmst btn btn-secondary" href="<?=base_url('customer-request') ?>" ><?=$this->lang->line('BACK');?></a>