<div class="container">
  <div class="form-box col-md-8 col-centered">      
      <div class="f1">
        <?php getNotificationHtml(); ?>
      <fieldset>
      <div class="col-md-12">
        <h2><?=$this->lang->line('RESET_PASSWORD'); ?></h2>
      	<form method="post">
      		<div class="form-group">
              <label class="col-md-12" for="email"><?=$this->lang->line('NEW_PASSWORD'); ?></label>
              <div class="col-md-12">
                <?php echo form_input(array('type'=>'password','id' => 'password', 'name' => 'password','class'=>'form-control','placeholder'=>'Password...','value' => set_value('password', isset($password)?$password:"" ))); ?>
                <?php echo form_error('password'); ?>
              </div>
          </div>
         <div class="form-group">
            <label class="col-md-12" for="email"><?=$this->lang->line('CONFIRM_NEW_PASSWORD'); ?></label>
            <div class="col-md-12">
              <?php echo form_input(array('type'=>'password','id' => 'repassword', 'name' => 'repassword','class'=>'form-control','placeholder'=>'Confirm Password...','value' => "")); ?>
              <?php echo form_error('repassword'); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <button type="submit" class="btn btn-info"><?=$this->lang->line('RESET_PASSWORD'); ?></button>
            </div>
          </div>
      	</form>
      </div>
    </fieldset>
  </div>
  </div>
</div>