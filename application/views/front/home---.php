<?php $success= $this->session->flashdata('message'); 
          if(!empty($success)) { ?>
          <div class="text-center alert alert-success alert-dismissible" style="margin:0; border-radius:0">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php } ?>
<div class="banner">  
  <div class="row no-gutters">  
    <div class="col-md-6 left-banner">      
      <div class="inner-left-banner">      
      <h2><?=$this->lang->line('READY_TO_LIVE'); ?></h2>
      <p><?=$this->lang->line('SEARCH_INSTRUCTION1'); ?></p>
      <p><?=$this->lang->line('SEARCH_INSTRUCTION2'); ?></p>      
      <form class="banner-search" id="zipcodeSearch" method="post" >
        <input type="text" class="form-control" id="servicesarea" name="servicesarea" placeholder="<?=$this->lang->line('ZIP_CODE'); ?>" title="Please enter the zip / postal code" required="required" autocomplete="off">
        <button  type="submit" class="btn btn-search"><i class="fas fa-search"></i><?=$this->lang->line('SEARCH'); ?></button>
      </form>  
          
     <!--  <div data-toggle="modal" data-target="#popup-service2" class="gps-location"><a href="#"><i class="material-icons">gps_fixed</i> <?=$this->lang->line('USE_CURRENT_LOCATION'); ?></a></div> -->      
      </div><!--inner-left-banner-->      
    </div>    
    <div class="col-md-6 right-banner">      
      <div class="inner-right-banner">
        <h1><?=$this->lang->line('ONLINE_PRODUCT'); ?> / <br><?=$this->lang->line('SUPPLIES_ORDERING'); ?> </h1>
        <a href="<?=base_url('shop')?>" class="shop-btn"><?=$this->lang->line('SHOP_NOW'); ?> <i class="fas fa-angle-right"></i></a>
      </div> 
    </div>
  </div>    
</div>

<section class="how-it-works">
  <div class="container">    
    <div class="title">
      <h3><?=$this->lang->line('HOW_IT_WORK'); ?></h3>
    </div>    
    <div class="row">      
      <div class="col-md-4 text-center">
        <img src="<?=base_url('skin/front/images/how-1.jpg')?>">
        <h3><?=$this->lang->line('BOOK_TITLE'); ?></h3>
        <p><?=$this->lang->line('BOOK_DESCRIPTION'); ?></p>
      </div>      
      <div class="col-md-4 text-center">
        <img src="<?=base_url('skin/front/images/how-2.jpg')?>">
        <h3><?=$this->lang->line('SCHEDULE_TITLE'); ?></h3>
        <p><?=$this->lang->line('SCHEDULE_DESCRIPTION'); ?></p>
      </div>      
      <div class="col-md-4 text-center">
        <img src="<?=base_url('skin/front/images/how-3.jpg')?>">
        <h3><?=$this->lang->line('RELAX_TITLE'); ?></h3>
        <p><?=$this->lang->line('RELAX_DESCRIPTION'); ?></p>
      </div>     
    </div>   
  </div>  

</section>

<section class="service-gray">
  <div class="container">    
    <div class="title">
      <h3><?=$this->lang->line('FEATURED_PRODUCTS'); ?></h3>
    </div>    
    <div class="owl-carousel owl-theme"> 
     <?php if(!empty($featureproducts)) { 
          foreach ($featureproducts as $featureproductsData) { ?>          
        <div class="item">
          <div class="feature-item">              
            <div class="feature-item-pic align-items-center">
              <a href="<?=base_url('shop/'.$featureproductsData->slug)?>">
              	<?=getProductImage($featureproductsData->id)?>
              </a>
            </div>
            <div class="feature-item-text">        
              <a href="<?=base_url('shop/'.$featureproductsData->slug)?>"><p class="price-tag d-inline-block"><?=isset($featureproductsData->name)?$featureproductsData->name:""?> </p></a>
            </div>
          </div>
        </div>
        <?php } } ?> 
      </div><!--owl slider--> 
     </div>
 </section>



<section>
  <div class="container">
    
    <div class="title">
      <h3><?=$this->lang->line('FEATURED_SERVICES'); ?></h3>
    </div>
    
    <div class="owl-carousel owl-theme">
           <?php if(!empty($featureServices)) { 
          foreach ($featureServices as $servicesData) { ?>
            <div class="item">
             <div class="feature-item">
            <div class="feature-item-pic align-items-center">
        <a class="footer_services_data" href="javascript:void(0)" service-data="<?=$servicesData->id; ?>">
        	<?=getServicesImage($servicesData->id)?>
        </a>

        </div>

         <div class="feature-item-text">
        
        <p class="price-tag d-inline-block"><?=isset($servicesData->name)?$servicesData->name:""?></p>

        </div>
            </div>
            </div>
           <?php } } ?>
           </div><!--owl slider--> 
         </div>
 </section>


<section class="app-sec">
  <div class="app-text">
    <?php 
     $footerContent=getFooterHomeData();
      if(isset($footerContent)){ 
          extract($footerContent); ?>
          <h3><?=$sitetitle?></h3>
        <p><?php echo substr($footerdescription,0,60);?></p>
       <p><a href="<?=$googleplay?>"><img src="<?=base_url('skin/front/images/app-btn-1.png')?>"></a>
      <a href="<?=$appstore?>""><img src="<?=base_url('skin/front/images/app-btn-2.png')?>"></a></p>
    <?php  } ?>
    </div>
</section>


<section class="about-sec">
  <div class="container">
    
    <div class="col-12">
    <div class="about-title">
      <h3><?=$this->lang->line('WHAT_THEY_SAY_ABOUT_US'); ?></h3>
      <p><?=$this->lang->line('WHAT_THEY_SAY_ABOUT_DESCRIPTION');?></p>
      
    </div>
    </div>
    
  <div class="row">
  
  <div class="col-md-6">
    
    <div class="about-text">
      <?php 
      if(isset($pageContent)){ 
          extract($pageContent); ?>
        <p><?php echo substr($description,0,589); ?></p>
        <?php  } ?>
    
    <a href="<?=base_url('about-us')?>" class="main-btn"><?=$this->lang->line('KNOW_MORE'); ?> <i class="fas fa-angle-right"></i></a>
      
    </div>
    
  </div>
  
    <div class="col-md-6">
    
    <div class="about-slider">
      
      <div id="about-slide" class="owl-carousel owl-theme">
          <?php if(!empty($testimonialContent)) { 
          foreach ($testimonialContent as $testimonialData) { ?>
            <div class="item">
            <div class="test-text">
            <p><?=substr(strip_tags(isset($testimonialData->description)?$testimonialData->description:""),0,190)?></p>
                </div>
              <div class="test-pic text-center">
             <?=getTestimonialImage($testimonialData->id)?> 
            <p><?=isset($testimonialData->name)?$testimonialData->name:""?></p>
           </div>
          </div>
         <?php } } ?> 
        </div>
      </div>
     </div>
    </div>
   </div>            
  </div>
</section>