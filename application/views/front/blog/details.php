<div class="inner-banner">
	<div class="title">
		<h3>Blog</h3>
	</div>
	<div class="breadcrumb-top">
		<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
		<li class="breadcrumb-item active">Blog</li>
		</ol>
	</div>
</div>

<section class="blogLists">
  <div class="container">   
    <div class="row">     
	    <div class="col-md-12">
	    	<div class="all-blog-list">
			    <?php
			    //print_r($record);
			    if(!empty($record))
			    {
			      	?> 
			      	<div class="row"> 
				        <div class="col-md-3">
					        <figure class="imgbx">
					        	<?=getBlogImage(!empty($record->id)?$record->id:"0")?>
					        </figure>
					    </div>
					    <div class="col-md-9">
					        <div class="blogSummary">	
					        	<h2><?=!empty($record->name)?$record->name:"";?></h2>
					        	<p><strong><i>Posted On </i></strong> : <i><?=!empty($record->created)?date('d M, Y h:i A', strtotime($record->created)):"";?></i></p>
					        	<?=!empty($record->description)?$record->description:"";?> 
					        </div>
				        </div> 
				    </div>    
			    	<?php
				}
				?>  
		    </div>
	    </div>   
  	</div>  
 </div>
</section>

