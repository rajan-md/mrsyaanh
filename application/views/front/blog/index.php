<div class="inner-banner">
	<div class="title">
		<h3>Blog</h3>
	</div>
	<div class="breadcrumb-top">
		<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
		<li class="breadcrumb-item active">Blog</li>
		</ol>
	</div>
</div>

<section class="blogLists">
  	<div class="container">
	    <div class="row">
		    <div class="col-md-12">
			    <div class="all-blog-list">
				    <?php
				    if(!empty($records))
				    {
				      	foreach ($records as $data)
				      	{
				      		?> 
					      	<div class="row">
						        <div class="col-md-3">
							        <a href="<?=base_url('blog/'.$data->slug)?>">
								        <figure class="imgbx">
								        		<?=getBlogImage(!empty($data->id)?$data->id:"0")?>
								        </figure>
							        </a>
							    </div>
							    <div class="col-md-9">
							        <div class="blogSummary">
							        	<h3><?=!empty($data->name)?$data->name:"";?></h3>
							        	<p><strong><i>Posted On </i></strong> : <i><?=!empty($data->created)?date('d M, Y h:i A', strtotime($data->created)):"";?></i></p>
							        	<?=!empty($data->description)?substr($data->description,0,400):"";?>
							        	<p><a href="<?=base_url('blog/'.$data->slug)?>" class="label label-success">Read more....</a></p>     
							        </div>
						        </div> 
						    </div>    
				    		<?php
						}
					}
					if(!empty($pagination))
					{
						?>			     
				        <div class="clerfix"></div> 
				        <div class="pagination"><?=$pagination; ?></div>  
				        <div class="clerfix"></div>
				        <?php
				    }
				    ?>
				</div>
			</div>   
		</div>
 	</div>
</section>

