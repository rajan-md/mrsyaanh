<div class="container">
  <div class="title"><h3><?=$this->lang->line('CHOOSE_PAYMENT_METHOD') ?></h3></div>       
  <div id="accordion">
  <?php $user_id = $this->session->userdata('userID'); ?>  
    
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a href="<?=base_url('checkout')?>" class="jbtn btn-link collapsed">
          <?=$this->lang->line('SHIPPINNG_INFORMATION')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a  href="<?=base_url('shipping-method')?>" class="jbtn btn-link collapsed" >
         <?=$this->lang->line('SHIPPING_METHOD')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('PAYMENT_METHOD')?>
        </a>
      </h5>
    </div>
    <div class="collapse show">
      <div class="card-body">       
       <form class="payment-form" method="post" autocomplete="off">        
        <div class="form-check">
        <input class="form-check-input" type="radio" name="payment_method" value="paypal" required="">
        <label class="form-check-label" for="exampleRadios1"><img src="http://markupdesigns.info/firstchoice/skin/front/images/paypal-logo.jpg"></label>
        </div>                
        <div class="form-check">
        <input class="form-check-input" type="radio" name="payment_method" value="cod" required="">
        <label class="form-check-label" for="exampleRadios2"><img src="http://markupdesigns.info/firstchoice/skin/front/images/cod-logo.jpg"></label>
        </div>
        <div class="col-md-12">
          <div class="text-right mt30">
            <button type="submit" class="btn login-btn">
              <?=$this->lang->line('CONTINUE') ?> <span class="glyphicon glyphicon-play"></span>
            </button>
          </div>
        </div>        
       </form>
        
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('ORDER_REVIEW')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
</div>
               
              

                <div class="col-12 text-right mt30 mb50">
                    
                </div>
           
        </div>