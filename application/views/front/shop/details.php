<section class="pgeDetails">
	<div class="container">
	 <div class="inner-sec">
	 	<div class="row">
			<?php if(!empty($shopData)) {  //print_r($shopData);  ?>
	 		<div class="col-md-4 text-center">
	 		<div class="main-product">	
				<div class="exzoom hidden" id="exzoom">
					<div class="exzoom_img_box">
					<ul class='exzoom_img_ul'>
				    <?php $imgList=getProductAllImage(isset($shopData->id)?$shopData->id:"0"); 
				    if(!empty($imgList)){
				    	foreach ($imgList as $imgData) { ?>				    	
							<li><img src="<?=isset($imgData->file_name)?$imgData->file_name:""?>" class="img-responsive"> </li>
					<?php } } ?>
					</ul>
					</div>
				<div class="exzoom_nav"></div>
					<p class="exzoom_btn">
					<a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
					<a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
					</p>
				</div>
	 		</div>
	 		</div>
	 		<div class="col-md-5">
	 			<div class="detail-pro-text">
	 				<h3><?=isset($shopData->name)?$shopData->name:""?></h3>
	 				<p><?=substr(strip_tags(isset($shopData->description)?$shopData->description:""),0,200) ?></p>
	 				<h4 class="price"><?=getPriceFormate(isset($shopData->price)?$shopData->price:"0")?></h4>
	 				<?php
	 				    $quantity=isset($shopData->quantity)?$shopData->quantity:"0";
	 					if($quantity>0){
	 						echo '<p class="stock">'.$this->lang->line('IN_STOCK').'</p>';
	 					} else {
	 						echo '<p class="out_of_stock">'.$this->lang->line('OUT_OF_STOCK').'</p>';
	 					}
	 				?>
	 								
	 									
					<div class="quatity">
					  <p><?=$this->lang->line('QUANTITY'); ?></p>					  
						<span>
						<input type="button" value="-" class="qtyminus" field="quantity" />
						<input type="text" name="quantity" value="1" class="qty" />
						<input type="button" value="+" class="qtyplus" field="quantity" />
						</span>						
					</div>	 				
	 				<div class="cart-btns">
	 					<a href="javascript:void(0)" product_id="<?=isset($shopData->id)?$shopData->id:"0"?>" qty="1"  class="addtocart buy-btn "><?=$this->lang->line('ADD_TO_CART'); ?></a>
	 					<a href="javascript:void(0)" class="addtowishlist" product_id="<?=isset($shopData->id)?$shopData->id:"0"?>" ><?=$this->lang->line('ADD_TO_WISHLIST'); ?></a>	 					
	 				</div>	 				
						 				
	 			</div>	 			
	 		</div> 		
			<div class="col-md-3">			
				<div class="right-detail-col">					
				<h3><?=$this->lang->line('FEATURED_PRODUCTS'); ?></h3>					
					<div class="featured-right-list">	
					 <?php if(!empty($featureproducts)) { 
          				foreach ($featureproducts as $featureproductsData) { ?> 			  
					  	<a href="<?=base_url('shop/'.$featureproductsData->slug)?>">
					  		<div class="row">
				              	<div class="col-4">
				                	<?=getProductImage(isset($featureproductsData->id)?$featureproductsData->id:"0")?>
				           
				                </div>
				                <div class="col-8 pl-0">
				                	<h6 class="mb-1"><?=substr(strip_tags(isset($featureproductsData->description)?$featureproductsData->description:""),0,20) ?>...</h6>
				                	<p><?=substr(strip_tags(isset($featureproductsData->description)?$featureproductsData->description:""),0,25) ?>...</p>
				                	
				                </div>
			            	</div>
			            </a>
            			<?php } } ?> 
            
				  	
				  	
				  </div>
				  
				</div>				
			</div> 			
	 	   <?php } ?>
	 	   
	 	   
	 	   <div class="product-desc">
	 	   	<div class="col-12">
	 	   	
	 	   	<h3><?=$this->lang->line('DESCRIPTION'); ?></h3>	
	 	   	<?=isset($shopData->description)?$shopData->description:"" ?>
	 	   </div>	
	 	   </div>
	 	   
	 	   
	 	    
	 	   
	 	</div><!--row-->
	 	</div>
	 	</div>
</section>

