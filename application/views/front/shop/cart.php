<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="dashboarCcontainer table-responsive cart-box">
               <?php 
                $total=0;
                $subtotal=0;
                $shipping=0;
                if ($this->cart->total_items() > 0)
                {
                ?>
                    <table class="table checkout-tbl">
                        <thead>
                            <tr>
                                <th width="50" class="text-right"> <?=$this->lang->line('DELETE')?> </th>
                                <th ><?=$this->lang->line('PRODUCT')?></th>
                                <th width="50"><?=$this->lang->line('QUANTITY')?></th>
                                <th width="70" class="text-right"><?=$this->lang->line('PRICE')?></th>
                                <th width="70" class="text-right"><?=$this->lang->line('SUB_TOTAL')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                // echo '<pre>';
                                // print_r($this->cart->contents());
                                // echo '</pre>';

                                foreach ($this->cart->contents() as $cartItem) { ?>
                            <tr>
                                <td class="text-left">
                                <button type="button" class="btn btn-danger removeCart" row_id="<?=isset($cartItem['rowid'])?$cartItem['rowid']:""?>">
                                    <span class="fa fa-remove"></span> <i class="fas fa-times"></i>
                                </button> 
                                </td>
                                <td>
                                <div class="media">
                                    <?=getProductImage(isset($cartItem['id'])?$cartItem['id']:"0")?> 
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="#"><?=getProductName(isset($cartItem['id'])?$cartItem['id']:"0")?></a></h4>
                                        
                                        <span><?=$this->lang->line('STATUS')?>: </span><span class="text-success"><strong><?=getProductStock(isset($cartItem['id'])?$cartItem['id']:"0")?></strong></span>
                                    </div>
                                </div></td>
                                <td class="text-center">
                                <input type="number" class="form-control updateqty" row_id="<?=isset($cartItem['rowid'])?$cartItem['rowid']:""?>" id="exampleInputEmail1" value="<?=isset($cartItem['qty'])?$cartItem['qty']:"0"?>">
                                </td>
                                <td class="text-right"><strong><?=getPriceFormate(isset($cartItem['price'])?$cartItem['price']:"0")?></strong></td>
                                <td class="text-right"><strong><?=getPriceFormate((isset($cartItem['qty'])?$cartItem['qty']:"0")*(isset($cartItem['price'])?$cartItem['price']:"0"))?></strong></td>
                                
                            </tr>
                           <?php 
                            $subtotal+=(isset($cartItem['qty'])?$cartItem['qty']:"0")*(isset($cartItem['price'])?$cartItem['price']:"0");
                           } 

                           $total=$subtotal+$shipping;
                           ?> 
                           
                            
                            
                            <tr>
                                <td colspan="4" class="text-right"><h3><?=$this->lang->line('SUB_TOTAL')?></h3></td>
                                <td class="text-right"><h3><strong><?=getPriceFormate(isset($total)?$total:"0")?></strong></h3></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td >
                                <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10">
                                    <span class="fa fa-shopping-cart"></span> <?=$this->lang->line('CONTINUE_SHOPPING')?>
                                </a></td>
                                <td>
                                <a href="<?=base_url('checkout')?>" class="btn login-btn mt10 mb10">
                                    <?=$this->lang->line('CHECKOUT') ?> <span class="fa fa-play"></span>
                                </a></td>
                            </tr>
                        </tbody>
                    </table>
                <?php
                }
                else
                {
                ?>
                    <div class="title"><h3><?=$this->lang->line('CART_EMPTY')?></h3></div>
                    <div class="text-center"><p>Shopping Cart is Empty, You have no items in your shopping cart. <a href="<?=base_url('shop')?>" > Click here </a> to continue shopping</p>
                    <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10">
                        <span class="fa fa-shopping-cart"></span><?=$this->lang->line('CONTINUE_SHOPPING')?>
                    </a>
                    </div>
                    <div class="mb40"></div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

