<div class="container">
  <div class="title"><h3><?=$this->lang->line('CHECKOUT') ?></h3></div>       
  <div id="accordion">
  <?php 
  $user_id=$this->session->userdata('userID');
  if(!empty($user_id) && !empty($userdata))
  {
    extract($userdata);
  }
  //print_r($userdata);
  ?>
  <?php $user_id = $this->session->userdata('userID');
  if(empty($user_id)){ ?>  
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="btn btn-link">
         <?=$this->lang->line('CHECKOUT_METHOD')?>
        </a>
      </h5>
    </div>
    <div  class="show">
      <div class="card-body">       
        <div class="inner-login-form">             
             <div class="row">
             <div class="col-sm-6">                           
              <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
               <button data-dismiss="modal" data-toggle="modal" data-target="#login" class="shop-btn"><?=$this->lang->line('LOGIN'); ?> / <?=$this->lang->line('REGISTRATION'); ?></button>               
              </div>
              
              <div class="col-sm-6">
              <p class="text-center"><?=$this->lang->line('LOGIN_WITH_SOCIAL')?>:</p>                 
                <div class="form-social-ico">
                  <a href="javascript:void(0)" class="fblogin login-fb"><i class="fab fa-facebook-f"></i> Facebook</a>
                  <a href="javascript:void(0)" class="gpluslogin login-gplus" id="gpluslogin"><i class="fab fa-google-plus-g"></i> Google+ </a>
                </div>              
             
              </div>
                            
            </div><!--row-->
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
          <?=$this->lang->line('SHIPPINNG_INFORMATION')?>
        </a>
      </h5>
    </div>
    <div class="collapse <?php if(!empty($user_id)){ echo 'show'; }?>">     
      <div class="card-body">

          <div id="shippingmethod" class="checkout_details_area mt-50 clearfix">                  
                        
                        <form method="post" autocomplete="on"><div class="billing-address" >                          
                          <input type="hidden" name="route" id="route" value="<?=set_value('route',isset($route)?$route:"")?>">
                          <input type="hidden" id="latitude" name="site_latitude" value="<?=set_value('route',isset($latitude)?$latitude:"")?>">
                          <input type="hidden" id="longitude" name="site_longitude" value="<?=set_value('route',isset($longitude)?$longitude:"")?>">
                          <input type="hidden" id="mobbox" name="mobbox">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name"><?=$this->lang->line('FIRST_NAME');?><span class="required">*</span> </label>
                                    <input type="text" class="form-control" name="firstname" id="firstname" placeholder="<?=$this->lang->line('FIRST_NAME');?>" value="<?=set_value('firstname',isset($fname)?$fname:"")?>" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name"><?=$this->lang->line('LAST_NAME');?><span class="required">*</span></label>
                                    <input type="text" class="form-control" name="lastname" id="lastname" placeholder="<?=$this->lang->line('LAST_NAME');?>" value="<?=set_value('lastname',isset($lname)?$lname:"")?>" required="">
                                </div>
                                
                                <div class="col-md-6 mb-3">
                                    <label for="email_address"><?=$this->lang->line('EMAIL');?><span class="required">*</span></label>
                                    <input type="email" class="form-control" name="emailaddress" id="emailaddress" placeholder="<?=$this->lang->line('EMAIL');?>" value="<?=set_value('emailaddress',isset($email)?$email:"")?>" required="">
                                </div>
                                <div class="col-md-6 mb-3 personal_phonebox">
                                    <label for="phone_number"><?=$this->lang->line('MOBILE');?><span class="required">*</span></label>
                                    <input type="tel" class="form-control" name="phonecode" id="phonecode" min="0" placeholder="<?=$this->lang->line('MOBILE');?>" value="<?=set_value('phonecode',isset($mobile)?$mobile:"")?>" required="">
                                </div>
                                
                                <div class="col-md-12 mb-3">
                                    <label for="street_address"><?=$this->lang->line('ADDRESS');?><span class="required">*</span></label>
                                    <input type="text" onclick="initialize();" name="site_location" class="form-control" autocomplete="off" id="site_location" value="<?=set_value('site_location',isset($site_location)?$site_location:"")?>" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="apartment_suite"><?=$this->lang->line('APT_UNIT_NO');?></label>
                                    <input type="text" name="aptno" class="form-control" id="aptno" value="<?=set_value('aptno',isset($aptno)?$aptno:"")?>" placeholder="<?=$this->lang->line('APT_UNIT_NO');?>">                              
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="street_number"><?=$this->lang->line('STREET_NO');?><span class="required">*</span></label>
                                    <input type="text" name="street_number" class="form-control" id="street_number" value="<?=set_value('street_number',isset($street_number)?$street_number:"")?>" placeholder="<?=$this->lang->line('STREET_NO');?>" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="street_name"><?=$this->lang->line('STREET_NAME');?><span class="required">*</span></label>
                                    <input type="text" name="street_name" class="form-control" id="street_name" value="<?=set_value('street_name',isset($street_name)?$street_name:"")?>" placeholder="<?=$this->lang->line('STREET_NAME');?>" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="street_number"><?=$this->lang->line('STREET_TYPE');?><span class="required">*</span></label>
                                    <?php echo form_dropdown('street_type', streetTypeOption(),set_value('street_type',isset($street_type)?$street_type:""),'id="street_type" class="form-control" required=""'); ?>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="street_number"><?=$this->lang->line('DIRECTION');?></label>
                                    <?php echo form_dropdown('direction', directionOption(),set_value('direction',isset($direction)?$direction:""),'id="direction" class="form-control"'); ?>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="city"><?=$this->lang->line('CITY');?> <span class="required">*</span></label>
                                    <input type="text" name="city" class="form-control" id="locality" placeholder="<?=$this->lang->line('CITY');?>" value="<?=set_value('city',isset($city)?$city:"")?>" required="">
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="country"><?=$this->lang->line('COUNTRY');?></label>
                                    <?php echo form_dropdown('country', getCountriesOptions(),set_value('country',isset($country)?$country:""),'id="country" class="f1-email form-control" required=""'); ?>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="country"><?=$this->lang->line('STATE_PROVINCE');?><span class="required">*</span></label>
                                    <?php
                                    $stateOptions = !empty($country)?getStatesOptions($country):array(''=>$this->lang->line('SELECT_STATE'));
                                    echo form_dropdown('state',$stateOptions,set_value('state',isset($state)?$state:""),'id="administrative_area_level_1" class="form-control" required=""');
                                    ?>
                                </div>
                                
                                <div class="col-md-6 mb-3">
                                    <label for="postcode"><?=$this->lang->line('ZIP_CODE');?><span class="required">*</span></label>
                                   <input type="text" name="postal_code" class="form-control" id="postal_code" value="<?=set_value('postal_code',isset($postal_code)?$postal_code:"")?>" placeholder="<?=$this->lang->line('ZIP_CODE');?>" required="">
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="postcode"><?=$this->lang->line('FAX');?></label>
                                   <input type="text" name="fax" class="form-control" id="fax" placeholder="<?=$this->lang->line('FAX');?>" value="<?=set_value('fax',isset($fax)?$fax:"")?>" >
                                </div>
                                
                            
                            <div class="col-md-6">
                            <!-- <div class="different-address mt30">
                                <div class="ship-different-title">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Ship to a different address?</label>
                                    </div>
                                </div>                              
                            </div> -->
                            </div>
                            <div class="col-md-6">
                              <div class="text-right mt30">
                                <button type="submit" class="btn login-btn">
                                  <?=$this->lang->line('CONTINUE') ?> <span class="glyphicon glyphicon-play"></span>
                                </button>
                              </div>
                            </div>
                            </div>

                           </div>
                        </form>
                    </div>
                  </div>
      
    </div>
  </div>
  
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('SHIPPING_METHOD')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>

  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed" >
         <?=$this->lang->line('PAYMENT_METHOD')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('ORDER_REVIEW')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
</div>
               
              

       <div class="col-12 text-right mt30 mb50"></div>         
           
        </div>