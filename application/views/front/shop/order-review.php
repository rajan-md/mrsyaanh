<div class="container">
    <div class="title"><h3><?=$this->lang->line('ORDER_REVIEW') ?></h3></div>       
  <div id="accordion">
  <?php $user_id = $this->session->userdata('userID'); ?>  
    
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a href="<?=base_url('checkout')?>" class="jbtn btn-link collapsed">
          <?=$this->lang->line('SHIPPINNG_INFORMATION')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a  href="<?=base_url('shipping-method')?>" class="jbtn btn-link collapsed" >
         <?=$this->lang->line('SHIPPING_METHOD')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a href="<?=base_url('payment-method')?>" class="jbtn btn-link collapsed">
         <?=$this->lang->line('PAYMENT_METHOD')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('ORDER_REVIEW')?>
        </a>
      </h5>
    </div>
    <div class="collapse show">
    <div class="row">
        <div class="col-sm-12">
           <?php 
            $total=0;
            $subtotal=0;
            $shipping=0;
            if ($this->cart->total_items() > 0) { ?>
            <form method="post">
            <table class="table checkout-tbl">
                <thead>
                    <tr>
                        
                        <th ><?=$this->lang->line('PRODUCT')?></th>
                        <th width="50"><?=$this->lang->line('QUANTITY')?></th>
                        <th width="70" class="text-right"><?=$this->lang->line('PRICE')?></th>
                        <th width="70" class="text-right"><?=$this->lang->line('SUB_TOTAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($this->cart->contents() as $cartItem) { ?>
                    <tr>
                        
                        <td>
                        <div class="media">
                            <?=getProductImage(isset($cartItem['id'])?$cartItem['id']:"0")?> 
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#"><?=getProductName(isset($cartItem['id'])?$cartItem['id']:"0")?></a></h4>
                                
                                <span><?=$this->lang->line('STATUS')?>: </span><span class="text-success"><strong><?=getProductStock(isset($cartItem['id'])?$cartItem['id']:"0")?></strong></span>
                            </div>
                        </div></td>
                        <td class="text-center">
                        <?=isset($cartItem['qty'])?$cartItem['qty']:"0"?>
                        </td>
                        <td class="text-right"><strong><?=getPriceFormate(isset($cartItem['price'])?$cartItem['price']:"0")?></strong></td>
                        <td class="text-right"><strong><?=getPriceFormate((isset($cartItem['qty'])?$cartItem['qty']:"0")*(isset($cartItem['price'])?$cartItem['price']:"0"))?></strong></td>
                        
                    </tr>
                   <?php 
                    $subtotal+=(isset($cartItem['qty'])?$cartItem['qty']:"0")*(isset($cartItem['price'])?$cartItem['price']:"0");
                   } 

                   $total=$subtotal+$shipping;
                   ?> 
                   
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-right"><h5><?=$this->lang->line('SUB_TOTAL')?></h5></td>
                        <td class="text-right"><h5><strong><?=getPriceFormate(isset($subtotal)?$subtotal:"0")?></strong></h5></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><h5><?=$this->lang->line('SHIPPING')?></h5></td>
                        <td class="text-right"><h5><strong><?=getPriceFormate(isset($shipping)?$shipping:"0")?></strong></h5></td>
                    </tr>
                    <tr>
                    <tr>
                        <td colspan="3" class="text-right"><h5><?=$this->lang->line('PAYMENT_METHOD')?></h5></td>
                        <td class="text-right"><h5><strong><?=isset($payment_method)?$payment_method:""?></strong></h5></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><h3><?=$this->lang->line('TOTAL')?></h3></td>
                        <td class="text-right"><h3><strong><?=getPriceFormate(isset($total)?$total:"0")?></strong></h3></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td >
                        <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10">
                            <span class="fa fa-shopping-cart"></span> <?=$this->lang->line('CONTINUE_SHOPPING')?>
                        </a></td>
                        <td>
                        <button type="submit" class="btn login-btn mt10 mb10">
                            <?=$this->lang->line('PLACE_ORDER') ?> <span class="fa fa-play"></span>
                        </button></td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="payamount" value="<?=isset($total)?$total:"0"?>">
            </form>
        <?php } else { ?>

            <div class="title"><h3><?=$this->lang->line('CART_EMPTY')?></h3></div>
            <div class="text-center"><p>Shopping Cart is Empty, You have no items in your shopping cart. <a href="<?=base_url('shop')?>" > Click here </a> to continue shopping</p>
            <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10">
                            <span class="fa fa-shopping-cart"></span><?=$this->lang->line('CONTINUE_SHOPPING')?>
                        </a>
            </div>
            <div class="mb40"></div>
        <?php }  ?>
        </div>
    </div>
    </div>
  </div>
</div>
</div>
<div class="col-12 text-right mt30 mb50"></div>         
           
        </div>
