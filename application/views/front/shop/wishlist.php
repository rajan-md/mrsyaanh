<div class="col-md-8 col-lg-8"> 
    <div class="dashboarCcontainer">
        <div class="table-responsive">
            <table class="table  checkout-tbl" style="margin:0">
                <tbody>
                    <?php if(!empty($wishlistContent))
                    {
                        foreach ($wishlistContent as $wishlistData)
                        {
                        ?>
                            <tr>
                                <td>
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="<?=base_url('shop/'.isset($wishlistData->slug)?$wishlistData->slug:"")?>"> <?=getProductImage(isset($wishlistData->id)?$wishlistData->id:"0")?> </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="<?=base_url('shop/'.isset($wishlistData->slug)?$wishlistData->slug:"")?>"><?=isset($wishlistData->name)?$wishlistData->name:""?></a></h4>                                
                                            <span>Status: </span><span class="text-success"><strong><?=getProductStock(isset($wishlistData->id)?$wishlistData->id:"0")?></strong></span>
                                        </div>
                                    </div>
                                </td>
                                
                                <td><strong><?=getPriceFormate(isset($wishlistData->price)?$wishlistData->price:"0")?></strong></td>
                                <td width="65" class="text-right"><strong><a href="javascript:void(0)" product_id="<?=isset($wishlistData->id)?$wishlistData->id:"0"?>" title="Add to cart" class="addtocart wishlist-add-bod"> <span class="fa fa-shopping-cart"></span></a></strong></td>
                                <td width="50" class="text-right">
                                    <button wishlist_id="<?=isset($wishlistData->wishlist_id)?$wishlistData->wishlist_id:"0"?>" type="button" class="remove_wishlist btn btn-danger">
                                        <span class="fa fa-remove"></span> <i class="fas fa-times"></i>
                                    </button>
                                </td>                       
                            </tr>
                        <?php
                        }
                    }
                    else
                    {
                    ?>            
                        <div class="text-center"><p>Wishlist is Empty, You have no items in your wishlist. <a href="<?=base_url('shop')?>" > Click here </a> to continue add item in wishlist</p>
                            <a href="<?=base_url('shop')?>" class="btn login-btn  mt10 mb10">
                                <span class="fa fa-shopping-cart"></span> Continue Shopping
                            </a>
                        </div>
                        <div class="mb40"></div>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>            
    </div>
</div>
    