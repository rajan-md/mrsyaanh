<div class="container">
  <div class="title"><h3><?=$this->lang->line('SHIPPING_METHOD') ?></h3></div>       
  <div id="accordion">
  <?php $user_id = $this->session->userdata('userID');
  if(empty($user_id)){ ?>  
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <button class="btn btn-link">
         <?=$this->lang->line('CHECKOUT_METHOD')?>
        </button>
      </h5>
    </div>
    <div  class="collapse"></div>
  </div>
  <?php } ?>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a href="<?=base_url('checkout')?>" class="jbtn btn-link collapsed">
          <?=$this->lang->line('SHIPPINNG_INFORMATION')?><span class="float-right">Edit</span>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         <?=$this->lang->line('SHIPPING_METHOD')?>
        </a>
      </h5>
    </div>
    <div class="collapse show">
      <div class="card-body">
         <div class="checkout_details_area mt-50 clearfix">                  
                        
                        <form method="post" autocomplete="off"><div class="shipping-method" >                         
                            <div class="row">
                              <div class="col-md-12 mb-3">                                    
                                      <input type="radio" name="shipping_method" value="free" required="">
                                      <label>Free Shipping </label>                                    
                              </div>   
                            <div class="col-md-12">
                              <div class="text-right mt30">
                                <button type="submit" class="btn login-btn">
                                  <?=$this->lang->line('CONTINUE') ?> <span class="glyphicon glyphicon-play"></span>
                                </button>
                              </div>
                            </div>
                            </div>

                           </div>
                        </form>
                    </div>
                  </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         <?=$this->lang->line('PAYMENT_METHOD')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
  <div class="card">
    <div class="card-header">
      <h5 class="mb-0">
        <a class="jbtn btn-link collapsed">
         <?=$this->lang->line('ORDER_REVIEW')?>
        </a>
      </h5>
    </div>
    <div class="collapse"></div>
  </div>
</div>
               
              

       <div class="col-12 text-right mt30 mb50"></div>         
           
        </div>