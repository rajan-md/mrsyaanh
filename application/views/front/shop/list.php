﻿<section class="productList">
  <div class="container">   
  <form method="get" id="filterDetails"> 
    <div class="row">     
    <div class="col-md-3">
    	<aside class="h-100">
    		<div class="shop-filters">    			
    			<h3><?=$this->lang->line('ALL_CATEGORIES'); ?></h3>    			
		    	<?php 
		    	$catitem=@$this->input->get('cat[]');
		    	if(empty($catitem)){
		    		$catitem=array();
		    	}
		    	if(!empty($categoryList)){
		    	foreach ($categoryList as $datacat) { ?>
					<label class="cus-in"><?=isset($datacat->name)?$datacat->name:""?>
		    			<input type="checkbox" name="cat[]" value="<?=isset($datacat->id)?$datacat->id:""?>" <?php if(in_array(isset($datacat->id)?$datacat->id:"0", $catitem))
  { echo 'checked="checked"';} ?> class="changeby" >
				  		<span class="checkmark"></span>
				  </label>	
		    	<?php }	} ?>				  
				    			
    		</div>
    	</aside>   	
    </div>   
    <div class="col-md-9">
    <div class="all-product-list">   
    	
<div class="row">        
<div class="col-12">
	<div class="right-filter">
		<div class="row">			
			<div class="col-9 col-md-6 col-lg-6">
			<p><?=$this->lang->line('ITEMS')?> <small>(Showing <?=isset($startview)?$startview:"0"?> – <?=isset($limitview)?$limitview:"0"?> of <?=isset($totalResult)?$totalResult:"0"?> Total)</small></p>
			</div>
			<div class="col-6 col-md-3 col-lg-2 text-center">
				<p><?=$this->lang->line('SHOW'); ?>
				 <?php $sortby=$this->input->get('sortby'); ?>
				 <?php $perpage=$this->input->get('perpage'); ?>
				 <select name="perpage" class="changeby">
					<option  value="15">15</option>
					<option <?php if($perpage==30) { echo 'selected';} ?> value="30">30</option>
					<option <?php if($perpage==45) { echo 'selected';} ?> value="45">45</option>
					<option <?php if($perpage==60) { echo 'selected';} ?> value="60">60</option>
					<option <?php if($perpage==75) { echo 'selected';} ?> value="75">75</option>
				</select></p>
			</div>
			<div class="col-6 col-md-3 col-lg-3 text-right align-self-center">
				 <select name="sortby" class="changeby">
					<option value=""><?=$this->lang->line('SORT_BY')?></option>
					<option <?php if($sortby=="price") { echo 'selected';} ?> value="price"><?=$this->lang->line('PRICE')?></option>
					<option <?php if($sortby=="asc") { echo 'selected';} ?> value="asc"><?=$this->lang->line('ASCENDING')?> </option>
					<option <?php if($sortby=="desc") { echo 'selected';} ?> value="desc"><?=$this->lang->line('DESCENDING')?> </option>
				</select>
			</div>
		</div>
	</div>
</div>
	    
	    
	    <?php 
	      if(!empty($shopData)) {
	      	foreach ($shopData as $data) { ?>      		     	
		        <div class="col-md-4">
			        <a href="<?=base_url('shop/'.$data->slug)?>">
			        <figure class="customprice">
			        	<div class="imgbx">
			        	<?=getProductImage(isset($data->id)?$data->id:"0")?>
			        	</div>			        	
			        </figure>
			        </a>

			        <div class="producDescription">
			        	<a data-toggle="tooltip" title="<?=isset($data->name)?$data->name:""?>" href="<?=base_url('shop/'.isset($data->slug)?$data->slug:"")?>"><h3><?php $str=isset($data->name)?$data->name:""; echo substr($str, 0,25);?></h3></a>		         
			         <div class="pro-icons">
			         	<span data-toggle="tooltip"  data-original-title="Price" class="p-detail"><?=getPriceFormate(isset($data->price)?$data->price:"0")?></span>
						<a href="javascript:void(0)" data-toggle="tooltip" class="addtocart" product_id="<?=isset($data->id)?$data->id:"0"?>" data-original-title="Add to cart"><i class="fa fa-shopping-cart"></i></a>
						<a href="javascript:void(0)" data-toggle="tooltip" class="addtowishlist" product_id="<?=isset($data->id)?$data->id:"0"?>" data-original-title="Add to Wishlist"><i class="fa fa-heart"></i></a>						
			            		     
				     </div>
			        		
			        				
			        </div>
			        
		        </div>     
	        <?php } } ?>  
	    </div>  
	        <div class="clerfix"></div> 
	        <div class="pagination"><?=isset($pagination)?$pagination:""?>  </div>  
	        <div class="clerfix"></div> 
	    </div><!--all-product-list--> 
    </div>   
  </div>  
 </form>
 </div>
</section>

