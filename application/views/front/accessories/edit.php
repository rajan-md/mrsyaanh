  <?php $user_id = CI()->session->userdata('userID');   
  if(!empty($vehicleaccessories)) { extract($vehicleaccessories); } ?>
  
  <div class="infoRight">
    <h3><img src="<?=base_url('skin/front')?>/images/icon-arrow.png"><?=$this->lang->line('UPDATE_ACCESSORIES'); ?></h3>
    <form method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="form-group">
      
      <div class="col-md-12 col-xs-12">
        <label><?=$this->lang->line('VEHICLE_NAME'); ?> <span class="required">*</span> </label>
         <?php 
         $vehicle_id=set_value('vehicle_id',isset($vehicle_id)?$vehicle_id:"");
         echo form_dropdown('vehicle_id', getVehicleByUserId($user_id),set_value('vehicle_models_id',isset($vehicle_id)?$vehicle_id:""),'id="vehicle_id" class="form-control"'); ?>
          <?php echo form_error('vehicle_id'); ?>
      </div>       
    
      <div class="col-md-12 col-xs-12">
        <label><?=$this->lang->line('ACCESSORIES_NAME'); ?> <span class="required">*</span></label>
        <input type="text" name="accessories_name" class="inp1" value="<?=set_value('accessories_name',isset($accessories_name)?$accessories_name:""); ?>" id="normal-field">
        <?php echo form_error('accessories_name'); ?>
      </div>
      
      <div class="col-md-12 col-xs-12">
    <label><?=$this->lang->line('ACCESSORIES_DISCRIPTION'); ?> <span class="required">*</span></label>
        <textarea name="accessories_discription" id="accessories_discription" placeholder="Please write here"><?=set_value('accessories_discription',isset($accessories_discription)?$accessories_discription:""); ?></textarea>
        <?php echo form_error('accessories_discription'); ?>
      </div>
         
      <div class="col-md-12 col-xs-12">
        <label><?=$this->lang->line('UPLOAD_ACCESSORIES_PIC'); ?> (<?=$this->lang->line('OPTIONAL'); ?>)</label>
         <input type="file" name="vehicleimg"  id="vehicleimg" class="inp2">
      </div>
    </div>
    <h4><?=$this->lang->line('ACCESORIES_AVALAIBLES'); ?> (<?=$this->lang->line('OPTIONAL'); ?>)</h4>
    <div class="form-group">     
      <div class="col-md-6 col-xs-12">
        <label><?=$this->lang->line('FROM'); ?></label>
        <?php $dtst=date_create(isset($availability_start)?$availability_start:""); 
              $stdate= date_format($dtst,"Y-m-d"); ?>
        <input type="text" name="availability_start" class="date inp1" placeholder="From ( yy-mm-dd )" value="<?=set_value('availability_start',$stdate); ?>"  id="datepicker">
                       <?php echo form_error('availability_start'); ?>
      </div>
      <div class="col-md-6 col-xs-12">
        <label><?=$this->lang->line('TO'); ?></label>
        <?php $dten=date_create(isset($availability_end)?$availability_end:""); 
              $endate= date_format($dten,"Y-m-d"); ?>
         <input type="text" name="availability_end" placeholder="To ( yy-mm-dd )" class="date inp1" value="<?=set_value('availability_end',$endate); ?>" id="datepicker1">
                       <?php echo form_error('availability_end'); ?>
      </div>     
    </div>
    
    <div class="col-md-12 col-xs-12">
      <div class="form-group">
        <label>
          <?php $conditions=set_value('conditions',isset($conditions)?$conditions:""); ?>
          <input type="checkbox" name="conditions"  <?php if($conditions=='yes') { ?> checked="checked" <?php } ?> value="yes">
          <?=$this->lang->line('AGREE'); ?> <span class="required">*</span></label>
          <div class="clearfix"></div>
          <?php echo form_error('conditions'); ?>
      </div>

    </div>
    <div class="form-group">
    <div class="col-md-12 col-xs-12">
      <div class="nxtBtn">
        <button><?=$this->lang->line('SUBMIT'); ?></button>
      </div>
    </div>
    </div>
    </form>
  </div>