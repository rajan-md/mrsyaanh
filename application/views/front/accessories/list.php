<?php 
$user_id = CI()->session->userdata('userID');
$accessories_list=getAccessoriesByUserID($user_id);
?>


  <div class="infoRight">
    <h3><img src="<?=base_url('skin/front')?>/images/icon-arrow.png"><?=$this->lang->line('ACCESSORIES_MANAGEMENT'); ?> <a class="add_button pull-right" href="<?=base_url('vehicle/accessories/add')?>"><?=$this->lang->line('ADD_ACCESSORIES'); ?></a> </h3>   
   
   <div class="table-responsive">
  <table class="table">
 <tr>
 	<th><?=$this->lang->line('IMAGE'); ?></th>
 	<th><?=$this->lang->line('ACCESSORIES_NAME'); ?></th>
 	<th><?=$this->lang->line('VEHICLE'); ?></th> 	
 	<th><?=$this->lang->line('ACTION'); ?></th>
 </tr>
 <?php 
 //print_r($accessories_list);
 if(!empty($accessories_list)) {
 foreach ($accessories_list as $VehicleData) { ?>
 <tr>
 	<td>
 		<?php if(!empty($VehicleData->vehicleimg)) { ?>
            <img width="40" src="<?=base_url($VehicleData->vehicleimg)?>">
            <?php } else { ?>
            <img width="40" src="<?=base_url('skin/front')?>/images/no_image.jpg">
            <?php } ?>
 	</td>
 	<td><?=$VehicleData->accessories_name?></td>
 	<td><?=getVehicleName($VehicleData->vehicle_id)?></td>
 	
 	<td><a class="label label-success" href="<?=base_url('vehicle/accessories/edit/'.$VehicleData->id)?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
    <a class="label label-danger" onclick="return confirm('Sure you want to delete this vehicle accessories')"  href="<?=base_url('vehicle/accessories/delete/'.$VehicleData->id)?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
 	</td>
 </tr>
 <?php } } ?>
 </table>
 </div>  
  
  </div>

