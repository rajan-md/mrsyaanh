<div class="row ser-rep">
    <?php if(!empty($bookingData)){ ?>
    <div class="col-sm-8">
      <div class="payCcontainer">       
      <h2><i class="fa fa-check-circle"></i> Booking Accepted</h2>     
      <div class="serv-view row">      
        <div class="col-sm-4 padd-rght">
          <div class="serv-detail">      
            <strong>Type of Service</strong>
            <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
            <ul>
            <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
              if($otherServices!=0){
              $otherData=explode(',', $otherServices);
              $count=count($otherData);              
              foreach ($otherData as $key => $value) { ?>                
                <li><?=getOtherServicesName($value)?></li>
              <?php } } ?>
              </ul>
          </div>        
        </div>      
        <div class="col-sm-8 padd-rght">
          <div class="serv-detail">     
          <strong>Address</strong>
            <p><?=getServicesAddress(isset($bookingData->address_id)?$bookingData->address_id:"")?></p>  
          </div>        
        </div>             
      </div>         
      <div class="tech-user">        
      <i class="fas fa-user-circle"></i>Your <?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?> technician will be assigned <strong>1 hour</strong> before the scheduled time        
      </div>      
      <div class="row">
      <div class="col-sm-8">      
      <div class="time_wrapper">
      <small>Booking Date</small>
      <time><?=date("M j, Y", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?>
        </time>  
        
      </div>
      
    </div>
      
      <div class="col-sm-4">
    <div class="time_sch">
    
    <button>Reschedule</button> 
      
    </div>
      
      
    </div>
       </div>
      
      </div>
    
    </div>
   
    <div class="col-sm-4">
    <div class="price_detail2"> 
    <h4>Payment Summary</h4>    
    <div class="row">  
    <div class="col-sm-8">    
    <p>Service charge</p>   
    </div>    
    <div class="col-sm-4 text-right"> 
    <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
     </div>   
    </div>  
    <div class="row total">
    <div class="col-sm-8 "> 
    <p>SubTotal</p>   
    </div>    
    <div class="col-sm-4 text-right"> 
    <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
     </div>   
    </div>    
    <div class="row paid">
    <div class="col-sm-7"> 
    <p>Amount to be paid</p>   
    </div>    
    <div class="col-sm-5 text-right"> 
    <p><strong><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></strong></p>    
     </div>   
    </div>    
    </div>
    </div>
    <?php } ?>
   
  </div>