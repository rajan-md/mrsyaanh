<div class="col-sm-8">
    <div class="dashboarCcontainer"> 
    	<?php 
    	getNotificationHtml();
    	//echo "<pre>"; print_r($record); echo "</pre>";
		if(!empty($record))
		{
			$statusArr = array(0=>'Closed',1=>'Open',2=>'Under review',3=>'Assiged to Team',4=>'Under Process',5=>'Rejected');
			$typeArr = array('products'=>'Product Orders','services'=>'Service Orders');
			$code=base64_encode(isset($record->id)?$record->id:"");
		    ?>
		    <div class="supportTicketBlock">
		    	<h1><?=isset($record->title)?$record->title:""?></h1>
				<div class="headerInfo">

					<p><strong>Ticket Id: </strong>#<?=isset($record->id)?$record->id:""?>, <strong>Related to: </strong><?=isset($record->support_for)?$typeArr[$record->support_for]:""?>, <strong>Order: </strong>#<?=isset($record->order_id)?$record->order_id:0?>, <strong>Status: </strong><?=isset($record->status)?$statusArr[$record->status]:"";?>, <strong>Submitted By: </strong> <?=isset($record->user_id)?getCustomerName($record->user_id):""?></p>
				</div>
				<div class="SupportContentBox">
					<div class="supportMainContent">
						<p><?=isset($record->description)?$record->description:""?></p>
					</div>
				</div>

				<?php
				//echo "<pre>"; print_r($responses); echo "</pre>";
				if(!empty($responses))
				{
					foreach ($responses as $response)
					{
						$role = getUserRole(!empty($response->responder_id)?$response->responder_id:0);
						$roleLabel = getRoleLabel(!empty($role)?$role:0);
						$responderRoleLabel = !empty($roleLabel)?' ( '.$roleLabel.' ) ':'';
						$response_time = !empty($response->response_time)?$response->response_time:'';
						$submit_time = !empty($response_time)?'<strong>on</strong> '.date('d M, Y h:i A',strtotime($response_time)):'';
						$class = (!empty($role) && $role==8)?'class="highlight"':'';
						echo '<blockquote '.$class.'><div class="infostrip"><strong>By </strong> '.getCustomerName($response->responder_id).' '.$responderRoleLabel.' '.$submit_time.'</div><div class="commentwrapper">'.$response->response.'</div></blockquote>';
					}
				}
				?>



				<hr>
				<h2>Reply Or Comment</h2>
				<hr>
				<div class="supportCommentBox">

					<form method="post">
						<div class="row">							
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" id="response" name="response" rows="10" required="required" placeholder="Please type here ..."></textarea>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="submit" name="submit" class="btn btn-success" value="Submit">
								</div>
							</div>
						</div>			
					</form>
					
				</div>
			</div>
		    <?php
		}
		?>
	</div>
</div>
