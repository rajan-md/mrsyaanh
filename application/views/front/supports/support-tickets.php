<div class="col-sm-8">
    <div class="dashboarCcontainer"> 
    	<?php 
    		getNotificationHtml();
    		//echo "<pre>"; print_r($servicesData); echo "</pre>";
    	?>
    	<table id="myTable2" class="table customer-request">
	    <thead>
	      <tr>
	        <th width="50">#ID</th>
	        <th>Customer</th>
	        <th>Type of Support</th>
	        <th width="50">Order</th>
	        <th width="70">Status</th>
	        <th width="120"></th>
	    </thead>
	    <tbody>
		<?php
		if(!empty($records))
		{
			$statusArr = array(0=>'Closed',1=>'Open',2=>'Under review',3=>'Assiged to Team',4=>'Under Process',5=>'Rejected');
			$typeArr = array('products'=>'Product Orders','services'=>'Service Orders');
		    foreach ($records as $rec)
		    { 
		        $code=base64_encode(isset($rec->id)?$rec->id:"");
		      	?>
			    <tr>
		      	<td>#<?=isset($rec->id)?$rec->id:""?></td>
		      	<td><?=isset($rec->support_for)?getCustomerName($rec->user_id):""?></td>
		      	<td><?=isset($rec->support_for)?$typeArr[$rec->support_for]:""?></td>
		        <td>#<?=isset($rec->order_id)?$rec->order_id:0?></td>
		        <td><?=isset($rec->status)?$statusArr[$rec->status]:"";?></td>
		    	<td>		        	
		        	<a title="View Support Ticket" class="dmst btn btn-info" href="<?=base_url('support-request-details/'.$code) ?>">
		        		<i class="fa fa-eye" aria-hidden="true"></i>
		        	</a>
		        	<?php $code2 = !empty($rec->order_id)?base64_encode($rec->order_id):0; ?>
		        	<a title="View Order History" class="dmst btn btn-warning" href="<?=base_url('support-order-details/'.$code2) ?>">
		        		<i class="fa fa-file" aria-hidden="true"></i>
		        	</a>
		        	<!--a title="Close Support Ticket" class="dmst btn btn-danger" href="<?=base_url('close-support-request/'.$code) ?>">
		        		<i class="fa fa-eye-slash" aria-hidden="true"></i>
		        	</a-->
		    	</td>
			    </tr> 
		    <?php
		  	}
		}
		else
		{
			?>
			<tr><td colspan="4">No any support ticket requested by you.</td></tr>
			<?php
		}
		?>
		</tbody>
		</table>
	</div>
</div>
