<div class="col-sm-8">
    <div class="dashboarCcontainer"> 
    	<?php 
    	getNotificationHtml();
    	//echo "<pre>"; print_r($servicesData); echo "</pre>";
    	?>
    	<table class="table customer-request">
	    <thead>
	      <tr>
	        <th width="50">#ID</th>
	        <th width="150">Type of Support</th>
	        <th>Title</th>
	        <th width="50">Order</th>
	        <th width="100">Status</th>
	        <th width="60"></th>
	    </thead>
	    <tbody>
		<?php
		if(!empty($records))
		{
			$statusArr = array(0=>'Closed',1=>'Open',2=>'Under review',3=>'Assiged to Team',4=>'Under Process',5=>'Rejected');
			$typeArr = array('products'=>'Product Orders','services'=>'Service Orders');
		    foreach ($records as $rec)
		    { 
		        $code=base64_encode(isset($rec->id)?$rec->id:"");
		      	?>
			    <tr>
		      	<td>#<?=isset($rec->id)?$rec->id:""?></td>
		      	<td><?=isset($rec->support_for)?$typeArr[$rec->support_for]:""?></td>
		      	<td><?=isset($rec->title)?$rec->title:""?></td>
		        <td>#<?=isset($rec->order_id)?$rec->order_id:0?></td>
		        <td><?=isset($rec->status)?$statusArr[$rec->status]:"";?></td>
		    	<td>
		        	<a title="View details" class="dmst btn btn-info" href="<?=base_url('support-request-details/'.$code) ?>">
		        		<i class="fa fa-eye" aria-hidden="true"></i>
		        	</a>
		        	<!--a title="Delete ticket" class="dmst btn btn-danger" href="<?=base_url('delete-support-ticket/'.$code) ?>">
		        		<i class="fa fa-times" aria-hidden="true"></i>
		        	</a-->
		    	</td>
			    </tr> 
		    <?php
		  	}
		}
		else
		{
			?>
			<tr><td colspan="4">No any support ticket requested by you.</td></tr>
			<?php
		}
		?>
		</tbody>
		</table>
	</div>

<div class="clerfix"></div> 

<div class="addOpt">
	<a title="Add New Support Ticket" href="<?=base_url('request-support');?>" class="cmst btn btn-success">
		<i class="fa fa-plus" aria-hidden="true"></i> Add a new Support Ticket</a>
</div>
<div class="pagination"><?=isset($pagination)?$pagination:""?></div> 
<div class="clerfix"></div> 

</div>
