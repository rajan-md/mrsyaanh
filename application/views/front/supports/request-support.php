<div class="col-md-8 col-lg-8"> 

	<div class="dashboarCcontainer profileDetails">
		<form method="post" autocomplete="off" enctype="multipart/form-data" class="newservice">
				<?php getNotificationHtml(); ?>
					<div class="row">
						<div class="col-md-6">					
							<div class="form-group">						 
								<label class="profile_field">Type of Support</label>
								<?php
								$support_type = array(''=>'------Choose----','products'=>'Products Related','services'=>'Services Related');
		                      	echo form_dropdown('support_type', !empty($support_type)?$support_type:array(),set_value('support_type',isset($records->support_for)?$records->support_for:''),'id="support_type" class="form-control" required');
		                      	echo form_error('support_type');
		                      	?>
							</div>
						</div>

						<div class="col-md-6">					
							<div class="form-group">						 
								<label class="service_orders">Orders</label>
								<?php                       
		                      	echo form_dropdown('orders',array(''=>'Choose your order'),set_value('orders',isset($records->service_id)?$records->service_id:''),'id="orders" class="form-control" required');
		                      	echo form_error('orders');
		                      	?>
							</div>
						</div>

						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="support_title">Support Title</label>
								<?php echo form_input(array('id' => 'support_title', 'name' => 'support_title','class'=>'form-control','placeholder'=>'Support Title','value' => set_value('support_title', isset($support_title)?$support_title:"" ),'required'=>'required')); ?>
								<?php echo form_error('support_title'); ?>
							</div>
						</div>

						<div class="col-md-12">					
							<div class="form-group">						 
								<label class="support_concern">Support Concern</label>
								<?php
								echo form_textarea(array('id'=>'support_concern','name' => 'support_concern','class'=>'form-control','rows' => 5,'cols' => 25,'placeholder'=>'Support Concern','value' => set_value('support_concern', isset($support_concern)?$support_concern:"" ),'required'=>'required'));
		                      	echo form_error('support_concern');
		                      	?>
							</div>
						</div>


					 </div><!--row-->

				<div class="row">
					<div class="col-md-12 form-group">
						<input type="submit" name="submit" class="btn common-btn" id="submit" value="Submit">
					</div>
				</div>

	</form>

	</div>

</div>