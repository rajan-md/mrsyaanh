<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">
    <?php if(!empty($bookingData)){ //echo "<pre>"; print_r($bookingData); echo "</pre>"; ?>
    <div class="col-md-8">
      <div class="payCcontainer">
        <?php
        $status = !empty($bookingData->status)?$bookingData->status:0;
        echo getServiceBookingStatusHeading($status);
        ?>
        <div class="serv-view row">
          <div class="col-md-4 padd-rght">
            <div class="serv-detail">      
              <strong>Type of Service</strong>
              <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
              <ul>
              <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
                if($otherServices!=0){
                $otherData=explode(',', $otherServices);
                $count=count($otherData);              
                foreach ($otherData as $key => $value) { ?>                
                  <li><?=getOtherServicesName($value)?></li>
                <?php } } ?>
                </ul>
            </div>        
          </div>      
          <div class="col-md-8 padd-rght">
            <div class="serv-detail">     
            <strong>Address</strong>
              <p><?=getServicesAddress(isset($bookingData->address_id)?$bookingData->address_id:"")?></p>  
            </div>        
          </div>             
        </div>

        <div class="tech-user">
          <i class="fas fa-user-circle"></i>
          Your <?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?> technician will be assigned <strong>1 hour</strong> before the scheduled time.
        </div> 

        <div class="row">
          <div class="col-sm-8">
              <div class="time_wrapper">
                <small>Service Time</small>
                <time><?=date("j M, Y h:i A", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?></time>  
              </div>      
          </div>
        
          <div class="col-sm-4">            
          </div>
        </div>

        <br>
        <div class="price_detail2 clearfix">
          <h4>Job Summary</h4>    
          <?php
          if(!empty($is_service_started) && !empty($is_service_ended))
          {
            ?>
            <div class="row">
                <div class="col-sm-6"> 
                  <p><strong>Started Time :</strong> <?=date('d M, Y h:i A',strtotime($service_start_time)); ?></p>    
                </div>
                <div class="col-sm-6"> 
                  <p><strong>Ended Time : </strong><?=date('d M, Y h:i A',strtotime($service_end_time)); ?></p>
                </div>   
            </div>
            <div class="row">
                <div class="col-sm-6"><p><strong>Allocated Time : </strong> 1 Hour</p></div>
                <div class="col-sm-6"><p><strong>Expended Time :</strong> <?=$expended_time; ?></p></div>
            </div>
            <?php
            if(!empty($additional_time) && $additional_time>0)
            {
              ?>
              <div class="row">
                <div class="col-sm-6"> 
                  <p><strong>Additional Time :</strong> <?=$additional_time; ?></p>    
                </div>
                <div class="col-sm-6"> 
                  <p><strong>Additional Amount : </strong><?=getPriceFormate($additional_amount); ?></p>
                </div>             
              </div>
              <?php
            }
            if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
            {
              ?>
              <div class="row">
                <div class="col-sm-6"> 
                  <p><strong>Outstanding Payment Status: </strong>Paid via Paypal</p>
                </div>
              </div>
              <?php
            }
          }
          ?>
        </div>

      </div>
    </div>

    <div class="col-md-4">
      <div class="price_detail2">
        <h4>Payment Summary</h4>
        <div class="row">
          <div class="col-sm-8">
            <p>Service charge</p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
          </div>
        </div>
        <div class="row total">
          <div class="col-sm-8">
            <p>SubTotal</p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></p>    
          </div>
        </div>
        <div class="row paid">
          <div class="col-sm-7"> 
            <p>Amount to be paid</p>   
          </div>    
          <div class="col-sm-5 text-right"> 
            <p><strong><?=getPriceFormate(isset($bookingData->amount)?$bookingData->amount:"")?></strong></p>    
          </div>   
        </div>
      </div>

      <br>
      <?php
      if(!empty($additional_time) && $additional_time>0)
      {
        ?>
        <div class="price_detail2">
          <h4>Additional Payment Summary</h4>
          <div class="row">
            <div class="col-sm-8"> 
              <p>Additional Time </p>    
            </div>
            <div class="col-sm-4 text-right">
              <p><?=$additional_time; ?></p>    
            </div>
          </div>
          <div class="row total">
            <div class="col-sm-8"> 
              <p>Additional Amount </p>
            </div> 
            <div class="col-sm-4 text-right">
              <p><?=getPriceFormate($additional_amount); ?></p>    
            </div>            
          </div>
          <?php
          if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
          {
            ?>
            <div class="row paid">
              <div class="col-sm-7"> 
                <p>Amount has been paid</p>   
              </div>    
              <div class="col-sm-5 text-right"> 
                <p><strong><?=getPriceFormate($additional_amount); ?></strong></p>    
              </div>   
            </div>
            <?php
          }
          else
          {
            ?>
            <div class="row paid">
              <div class="col-sm-7"> 
                <p>Amount need to Pay</p>   
              </div>    
              <div class="col-sm-5 text-right"> 
                <p><strong><?=getPriceFormate($additional_amount); ?></strong></p>    
              </div>   
            </div>
            <?php
          }
          ?>
          
        </div>
        <br>
        <?php
      }
      ?>
      <div class="price_detail2">
          <h4>Customer Details</h4>
          <?php
          $customer = getCustomerDetailsById(isset($bookingData->user_id)?$bookingData->user_id:"");
          //echo "<pre>" ; print_r($customer); echo "</pre>";
          ?>
          <div class="row">
            <div class="col-sm-4"> 
              <p>Name : </p>   
            </div>  
              <div class="col-sm-8"> 
                <p><?=(!empty($customer->fname)?$customer->fname:"").' '.(!empty($customer->lname)?$customer->lname:"")?></p></div>
          </div>    
          <div class="row total">
            <div class="col-sm-4"> 
              <p>Email Id : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->email)?$customer->email:""?></p></div>   
          </div>
          <div class="row total">
            <div class="col-sm-4"> 
              <p>Mobile : </p>   
            </div>    
            <div class="col-sm-8"> 
              <p><?=!empty($customer->mobile)?$customer->mobile:""?></p></div>   
          </div>  
          <div class="row paid">
            <div class="col-sm-4"> 
              <p>Address : </p>   
            </div> 
            <div class="col-sm-8">
              <p><?=getServicesAddress(!empty($bookingData->address_id)?$bookingData->address_id:"")?></p>    
            </div>   
          </div>
      </div>
    </div>
    <?php } ?>
</div>
<a title="Back" class="cmst btn btn-secondary" href="<?=base_url('support-tickets') ?>" >Back</a>