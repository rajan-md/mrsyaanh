<?php
$user_id = $this->session->userdata('userID');
if(empty($user_id))
{
?>
  <div class="modal fade" id="login">
      <div class="modal-dialog modal-lg">
        <div class="modal-content login-form">        
          <div class="modal-header">
            <h5 class="modal-title text-left"><?=$this->lang->line('LOGIN_ACCOUNT'); ?></h5>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>                       
          <div class="modal-body">         
           <div class="row">          
            <div class="col-md-6">
              <div class="login-pic">
                <img src="<?=base_url('skin/front/images/login-popup.jpg')?>">              
              </div>
            </div>          
            <div class="col-md-6">
              <div class="inner-login-form"> 
                <div id="loginMsg"></div>             
                <form method="post" id="myLogin">
                  <input name="useremail" type="email" placeholder="<?=$this->lang->line('EMAIL'); ?> *" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" value="<?=!empty($this->input->cookie('uemail', TRUE))?$this->input->cookie('uemail', TRUE):'';?>" required="" class="form-control">
                  <input type="password" name="password" class="form-control" value="<?=!empty($this->input->cookie('upassword', TRUE))?$this->input->cookie('upassword', TRUE):'';?>" placeholder="<?=$this->lang->line('PASSWORD'); ?> *" required>       
                  <label class="rem-span w-100">
                    <?php $checked = !empty($this->input->cookie('rememberme', TRUE))?'checked="checked"':''; ?>
                   <input name="rememberme" type="checkbox" id="rememberme" value="forever" <?=$checked;?>>  <span><?=$this->lang->line('REMEMBER_ME'); ?></span>
                    <span class="lost-password d-inline-block">
                      <a data-dismiss="modal" href="javascript:void(0)" data-toggle="modal" data-target="#forgotpass">
                        <?=$this->lang->line('LOST_YOUR_PASSWORD'); ?>?</a></span>
                   </label>                 
                  <button type="submit" class="shop-btn"><?=$this->lang->line('LOGIN'); ?></button>                 
                </form>               
                <p class="text-center"><?=$this->lang->line('LOGIN_WITH_SOCIAL'); ?>:</p>                 
                  <div class="form-social-ico">
                    <a href="javascript:void(0)"  class="fblogin login-fb"><i class="fab fa-facebook-f"></i> Facebook</a>
                    <a href="javascript:void(0)" class="gpluslogin login-gplus" id="gpluslogin"><i class="fab fa-google-plus-g"></i> Google+ </a>
                  </div>            
              </div>        
            </div>          
           </div>         
          
          <div class="modal-footer">
            <p class="text-center"><?=$this->lang->line('DONT_HAVE_ACCOUNT'); ?> <a data-dismiss="modal" href="javascript:void(0)" data-toggle="modal" data-target="#register"><?=$this->lang->line('REGISTER_NOW'); ?></a></p>
          </div>

          </div>       
        </div>
      </div>
    </div> 

  <div class="modal fade" id="register">
      <div class="modal-dialog modal-lg">
        <div class="modal-content login-form">        
          <div class="modal-header">
            <h5 class="modal-title text-left"><?=$this->lang->line('REGISTER_NOW'); ?></h5>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>
          <div class="modal-body">         
           <div class="row">          
            <div class="col-md-6">
              <div class="login-pic">
                <img src="<?=base_url('skin/front/images/login-popup.jpg')?>">
              </div>
            </div>          
            <div class="col-md-6">
              <div class="inner-login-form">  
                <div id="registrationMsg"></div>            
                <form method="post" id="myRegistration">
                  <input type="hidden" id="mobbox" name="mobbox" value="+1">

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <select name="role" id="role" class="form-control" required="">
                          <option value="2">SignUp as Customer</option>
                          <option value="5">SignUp as Syaanh</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row" id="companybox" style="display:none;">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" name="companyname" id="companyname" class="form-control" placeholder="Company Name" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Full name" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" name="useremail" id="useremail" class="form-control" placeholder="Email Id" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group personal_phonebox">
                        <input name="phonecode" id="phonecode" class="form-control" type="tel" minlength="10" maxlength="10" pattern="[0-9]{10}"  title="Invalid Mobile No" required="" placeholder="Phone number" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="password" name="userpass" id="userpass" class="form-control" placeholder="<?=$this->lang->line('PASSWORD'); ?>" autocomplete="false" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="password" name="userconfpass" id="userconfpass" class="form-control" placeholder="<?=$this->lang->line('CONFIRM_PASSWORD'); ?>" required>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="submit" class="shop-btn"><?=$this->lang->line('REGISTRATION'); ?></button>
                      </div>
                    </div>
                  </div>

                </form>                 
              </div>        
            </div>          
           </div>         
          </div> 

          <div class="modal-footer">
            <p class="text-center"><?=$this->lang->line('IF_YOU_HAVE_AN_ACCOUNT'); ?> <a data-dismiss="modal" href="javascript:void(0)" data-toggle="modal" data-target="#login">
            	<?=$this->lang->line('LOGIN_NOW'); ?></a></p>
          </div>

        </div>
      </div>
    </div>

  <div class="modal fade" id="forgotpass">
      <div class="modal-dialog modal-lg">
        <div class="modal-content login-form">        
          <div class="modal-header">
            <h5 class="modal-title text-left"><?=$this->lang->line('FORGOTPASS_ACCOUNT'); ?></h5>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>                       
          <div class="modal-body">         
           <div class="row">          
            <div class="col-md-6">
              <div class="login-pic">
                <img src="<?=base_url('skin/front/images/login-popup.jpg')?>">
              </div>
            </div>          
            <div class="col-md-6">
              <div class="inner-login-form"> 
                <div id="fpMsg"></div>             
                <form method="post" id="forgotpassword">
                  <input name="useremail" type="email" placeholder="<?=$this->lang->line('EMAIL'); ?> *" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" required="" class="form-control">
                  <button type="submit" class="shop-btn"><?=$this->lang->line('SUBMIT'); ?></button>                 
                </form>             
              </div>        
            </div>          
           </div>         
          </div> 
          <div class="modal-footer">
            <p class="text-center"><?=$this->lang->line('IF_YOU_HAVE_AN_ACCOUNT'); ?> <a data-dismiss="modal" href="javascript:void(0)" data-toggle="modal" data-target="#login"><?=$this->lang->line('LOGIN_NOW'); ?></a> OR <a data-dismiss="modal" href="javascript:void(0)" data-toggle="modal" data-target="#register"><?=$this->lang->line('REGISTER_NOW'); ?> </a></p>
          </div>     
        </div>
      </div>
  </div> 
<?php
}
?>
<div class="modal fade" id="popupservice">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="min-height:259px">
      <div class="text-center" id="zipcodeMsg"></div>    
    </div>
  </div>
</div>

<div class="modal fade" id="popup-service2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header text-center">
      <button class="back"><a href="#"><i class="fa fa-arrow-left"></i></a></button>
      <h4 class="modal-title">Plumbing Service</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
    <div class="modal-body">
      <div class="service-popup-links">
        <p>Professional ABC plumbing services are available in your area. Your service is guaranteed by ABC for 30 days. If the charges are agreeable, press next and to the next step to schedule a service. 
        (All applicable taxes in your area are extra.)</p>
        <div class="table">
          <p><span class="l-one">Minimum Service</span> <span class="l-two">30 Minutes</span>  <span class="l-three">$50.00</span></p>
          <p><span class="l-one">Hourly rate for the next 3 hours</span>  <span class="l-two">1 Hr.</span>  <span class="l-three">$50.00</span></p>
          <p class="border-0"><span class="l-one">Hourly Charge after that:</span>  <span class="l-two">1 Hr.</span>  <span class="l-three">$45.00</span></p>
        </div>
        <p>Time starts when the Service Provider arrives to your location and ends when job is completed.</p>
        <p class="m-0">Service Provider has some common parts with him, if he has to go and get the parts and come back we charge only 30 minutes for picking up the part not for the full time that Service Provider is away</p>
      </div>
    </div>
      <div class="modal-footer text-center">
        <a href="#" class="next-full-btn">Next <i class="fa fa-arrow-right"></i> </a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade popup1" id="checkout_popup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="checkout_responce text-center">
      <div id="checkout_responce" class="text-center">Empty cart </div>
      <div style="height:10px"></div>
            <a class="btn btn-info" href="<?=base_url('shop')?>">Continue Shopping</a> 
            <a class="btn btn-info" href="<?=base_url('cart')?>">Checkout</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade popup1" id="wishlist_popup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="wishlist_responce text-center">
      <div id="wishlist_responce" class="text-center">Empty cart </div>
      <div style="height:10px"></div>
            <a class="btn btn-info" href="<?=base_url('user/wishlist')?>">View Wishlist</a> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade popup1" id="checkout_qtyupdate" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="wishlist_responce text-center">
      <div id="updatecart_responce" class="text-center">Empty cart </div>
      <div style="height:10px"></div>
            
      </div>
    </div>
  </div>
</div>

