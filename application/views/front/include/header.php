<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
  <title>Mr.Syaanh</title>
  <link rel="stylesheet" href="<?=base_url('skin/front/css/bootstrap.min.css'); ?>" type="text/css"/>
  <link rel="stylesheet" href="<?=base_url('skin/front/css/style.css'); ?>" type="text/css"/>
  <link href="<?=base_url('skin/front/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,400i,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'); ?>">
  <link rel="shortcut icon" href="<?=base_url('skin/front/images/favicon.png'); ?>">
  <link rel="stylesheet" href="<?=base_url('skin/front/css/owl.carousel.min.css'); ?>">
  <link rel="stylesheet" href="<?=base_url('skin/front/css/owl.theme.default.min.css'); ?>">
  <link rel="stylesheet" href="<?=base_url('skin/front/css/intlTelInput.css'); ?>">
  <link rel="stylesheet" href="<?=base_url('skin/front/css/custom.css'); ?>">
 </head>
<body data-url="<?=base_url(); ?>">
<header> 
  <div class="header-main">
    <div class="container">
        <div class="row">        
          <nav id="header" class="navbar navbar-inverse">
            <div id="header-container" class="navbar-container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a id="brand" class="navbar-brand" href="<?=base_url();?>"><img src="<?=base_url('skin/front/images/logo.png'); ?>"></a> </div>
              <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                  <li><a href="<?=base_url(); ?>">Home</a></li>
                  <li><a href="<?=base_url('about-us'); ?>">About Us</a></li>
                  <li><a href="<?=base_url('explore'); ?>">Explore</a></li>
                  <li><a href="<?=base_url('blog'); ?>">Blog</a></li>
                  <li><a href="<?=base_url('contact-us'); ?>">Contact Us</a></li>
                  <?php
                  $user_id = $this->session->userdata('userID');
                  if(!empty($user_id))
                  {
                    ?>
                    <li class="top-right"><a href="<?=base_url('my-account')?>"><?=$this->lang->line('DASHBOARD'); ?></a></li>
                    <li class="top-right"><a href="<?=base_url('login/logout')?>"><?=$this->lang->line('LOGOUT'); ?></a></li>
                    <?php
                  }
                  else
                  {
                      ?>
                      <li class="top-right">
                        <a class="nav-link login-btn" href="javascript:void(0)" data-dismiss="modal" data-toggle="modal" data-target="#login"><?=$this->lang->line('LOGIN'); ?></a>
                      </li>
                      <li class="top-right">
                        <a class="nav-link login-btn" href="javascript:void(0)" data-dismiss="modal" data-toggle="modal" data-target="#register"><?=$this->lang->line('REGISTRATION'); ?></a>
                      </li>
                      <?php
                  }
                  ?>         
                </ul>
              </div>
            </div>
          </nav>  
        </div>
    </div>
  </div>
</header>

<section class="cat-bg"> 
   <div class="container">
      <div class="row">
          <div class="dropdown">
              <button class="dropbtn">PRODUCT CATEGORIES<b class="caret"></b></button>
              <div class="dropdown-content">
                <a href="#">Maintenance Services</a>
                <a href="#">AC Services</a>
                <a href="#">Home Improvement</a>
                <a href="#">Moving</a>
                <a href="#">Cleaning</a>
                <a href="#">Business Services</a>
              </div>
          </div>      
          <form class="banner-search">
                <input type="text" class="form-control" id="servicesarea" placeholder="Search for services">
                <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
          </form>
        </div>
    </div> 
</section>