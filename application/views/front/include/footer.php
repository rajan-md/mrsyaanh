<div class="service-support">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <div class="support-box">
                    <div class="col-md-3 support-pic">
                        <img src="<?=base_url('skin/front/images/supp01.png'); ?>" alt="" />
                    </div>

                    <div class="col-md-9 no-padding">
                        <div class="support-txt">
                            <strong>High Quality & Professionals</strong>
                            <p>Lorem ipsum dolor sit amet, Nam consectetur adipiscing elit…</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="support-box">
                    <div class="col-md-3 support-pic">
                        <img src="<?=base_url('skin/front/images/supp02.png'); ?>" alt="" />
                    </div>

                    <div class="col-md-9 no-padding">
                        <div class="support-txt">
                            <strong>24/7 Technical support</strong>
                            <p>Lorem ipsum dolor sit amet, Nam consectetur adipiscing elit…</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="support-box">
                    <div class="col-md-3 support-pic">
                        <img src="<?=base_url('skin/front/images/supp03.png'); ?>" alt="" />
                    </div>

                    <div class="col-md-9 no-padding">
                        <div class="support-txt">
                            <strong>Customer Loyalty Programs</strong>
                            <p>Lorem ipsum dolor sit amet, Nam consectetur adipiscing elit…</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <span class="boot-logo"><img src="<?=base_url('skin/front/images/logo.png'); ?>" alt=""></span>

                <div class="cont-details">

                    <ul>
                        <li><span>Call Us:</span> <a href="#">888-519-4010</a></li>
                        <li><span>Mail Us:</span> <a href="#"> info@services.com</a></li>
                        <li><span>Our Location:</span> <a href="#">182 California, United States MA 02472</a></li>

                    </ul>
                </div>

            </div>

            <div class="col-lg-3">
                <h5>POPULAR SERVICES</h5>
                <ul>
                    <li><a class="footer_services_data" href="javascript:void(0)" service-data="10">- Home Services in Dubai</a></li>
                    <li><a class="footer_services_data" href="javascript:void(0)" service-data="10">- Pest Control in Dubai</a></li>
                    <li><a class="footer_services_data" href="javascript:void(0)" service-data="10">- Cleaning Services in Dubai</a></li>
                    <li><a class="footer_services_data" href="javascript:void(0)" service-data="10">- Maids in Dubai</a></li>
                    <li><a class="footer_services_data" href="javascript:void(0)" service-data="10">- Laundry in Dubai</a></li>
                </ul>

            </div>

            <div class="col-lg-2">
                <h5>CUSTOMERS</h5>
                <ul>
                    <li><a href="<?=base_url('my-service-requests'); ?>">- My service requests</a></li>
                    <li><a href="<?=base_url('how-it-works'); ?>">- How it works?</a></li>
                    <li><a href="<?=base_url('careers'); ?>">- Careers</a></li>
                    <li><a href="<?=base_url('blog'); ?>">- Our Blog</a></li>
                    <li><a href="<?=base_url('faqs'); ?>">- FAQ</a></li>
                    <li><a href="<?=base_url('terms-conditions')?>">- <?=$this->lang->line('TERMS_CONDITION'); ?></a></li>
                    <li><a href="<?=base_url('privacy-policies')?>">- <?=$this->lang->line('PRIVACY_POLICY'); ?></a></li>
                </ul>

            </div>

            <div class="col-lg-3">

                <h5>WE ARE SOCIAL</h5>
                <div class="footer-social">
                    <a href="<?=getSetting('facebook'); ?>"><img src="<?=base_url('skin/front/images/foot01.jpg'); ?>" alt=""></a>
                    <a href="<?=getSetting('twitter'); ?>"><img src="<?=base_url('skin/front/images/foot02.jpg'); ?>" alt=""></a>
                    <a href="<?=getSetting('rss'); ?>"><img src="<?=base_url('skin/front/images/foot03.jpg'); ?>" alt=""></a>
                    <a href="<?=getSetting('instagram'); ?>"><img src="<?=base_url('skin/front/images/foot04.jpg'); ?>" alt=""></a>

                </div>
            </div>

        </div>
    </div>

</footer>

<div class="copyright-main">
    <div class="copyright">

        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <?php if(isset($footerContent)){ extract($footerContent); ?><p><?=$copyright?></p><?php } ?>
                </div>
                <div class="col-md-6 text-right">
                    <p>Powered by <a href="#">Markup Designs</a></p>
                </div>

            </div>
        </div>

    </div>
</div>

<script src="<?=base_url('skin/front/js/jquery.js'); ?>"></script>
<script src="<?=base_url('skin/front/js/bootstrap.min.js'); ?>"></script>
<script src="<?=base_url('skin/front/js/owl.carousel.js'); ?>"></script>
<script src="<?=base_url('skin/front/js/intlTelInput.js'); ?>"></script>
<script src="<?=base_url('skin/front/js/honey-custom.js'); ?>"></script>
<script src="<?=base_url('skin/front/js/custom.js'); ?>"></script>
</body>

</html>