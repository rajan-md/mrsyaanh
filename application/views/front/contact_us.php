<section class="mapPanel">
  <div class="contactPanel">
    <div class="container">
      <aside class="col-md-4">
        <div class="searchArea">
        <form id="searchformdata" method="post" action="">          
          <div class="col-md-12">
            <div class="formGroup">
              <label><?=$this->lang->line('NAME'); ?></label>
              <input type="text" name="username" id="username" placeholder="<?=$this->lang->line('NAME'); ?>" class="form-control" value="" >
            </div>
          </div>
          <div class="col-md-12">
            <div class="formGroup">
             <label><?=$this->lang->line('EMAIL'); ?></label>
             <input type="text" name="email" id="email" placeholder="<?=$this->lang->line('EMAIL'); ?>" class="form-control" value="" >
             
            </div>
          </div>
          <div class="col-md-12">
            <div class="formGroup">
              <label><?=$this->lang->line('MOBILE'); ?></label>
             <input type="text" name="mobile" id="mobile" placeholder="<?=$this->lang->line('MOBILE'); ?>" class="form-control" value="" >
            
            </div>
          </div>
          <div class="col-md-12">
            <div class="formGroup">
              <label><?=$this->lang->line('SUBJECTS'); ?></label>
             <input type="text" name="subjects" id="subjects" placeholder="<?=$this->lang->line('SUBJECTS'); ?>" class="form-control" value="" >
            
            </div>
          </div>
           <div class="col-md-12">
            <div class="formGroup">
              <label><?=$this->lang->line('MESSAGE'); ?></label>
             <textarea name="message" id="message" placeholder="<?=$this->lang->line('MESSAGE'); ?>" class="form-control" ></textarea>
            
            </div>
          </div>
          <div class="md-12">           
            <div class="formGroup">
              <button type="submit"><img src="<?=base_url('skin/front')?>/images/icon-search.png"> <?=$this->lang->line('SUBMIT'); ?></button>
            </div>
          </div>
          </form>
        </div>
      </aside>
      <aside class="col-md-8">
      <div id="map"></div>
      </aside>
    </div>
  </div>
</section>