<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
<link rel="shortcut icon" href="<?=base_url('skin/front')?>/images/favicon.ico" type="image/x-icon" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?=$this->lang->line('SITE_TITLE'); ?></title>
<!-- Bootstrap -->
<link href="<?=base_url('skin/front')?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url('skin/front')?>/css/style.css" rel="stylesheet" type="text/css">
<link href="<?=$this->lang->line('STYLE'); ?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('skin/front')?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/css/chat-app.css" rel="stylesheet">
<link rel='stylesheet' href='<?=base_url('skin/front')?>/css/owl.carousel.min.css'>
<link rel='stylesheet' href='<?=base_url('skin/front')?>/css/owl.theme.default.min.css'>
<link href="<?=base_url('skin/front')?>/css/animate.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?=base_url('skin/front')?>/css/jquery.mCustomScrollbar.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
  var site_url = "<?=base_url()?>"; 
</script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
	div#loading_view .loadingbox {
    width: 500px;
    margin: 0 auto;
    position: fixed;
    left: 31%;
    text-align: center;
    top: 30%;
}
div#loading_view .loadingbox img {
    width: 100px;
}
</style>  
</head>

<body>
<?php if($amount>0) {  ?>
<div id="loading_view">
  <div class="loadingbox">
    <img src="<?=base_url('skin/front/images/rabbit-animation.gif')?>">
  <div style="color: #fff; text-align: center;"><?=$this->lang->line('PAYMENT_PROCESS_MESSAGE'); ?></div>
  </div>
</div>
<?php 
$mode=getAdvanceOption('live_paypal');
if($mode==1){
  $action='https://www.paypal.com/cgi-bin/webscr';
} else {
  $action='https://www.sandbox.paypal.com/cgi-bin/webscr';
}
$paypal_id=getAdvanceOption('paypal');
$message ='<form method="post" action="'.$action.'" name="paypal_auto_form" id="frm1">';
$message .='<input type="hidden" name="business" value="'.$paypal_id.'">';
$message .='<input type="hidden" name="currency_code" value="'.$code.'">';
$message .='<input type="hidden" name="quantity" value="1">';
$message .='<input type="hidden" name="item_name" value="membership">';
$message .='<input type="hidden" name="cmd" value="_xclick">';
$message .='<input type="hidden" name="item_number" value="1">';
$message .='<input type="hidden" name="amount" value="'.$amount.'">';
$message .='<input type="hidden" name="first_name" value="'.$userData['username'].'">';
$message .='<input type="hidden" name="last_name" value="">';
$message .='<input type="hidden" name="email" value="'.$userData['email'].'">';
$message .='<input type="hidden" name="custom" value="'.$user_id.':'.$month.':months:0">'; 
$message .='<input type="hidden" name="return" value="'.base_url('thank-you').'">';
$message .='<input type="hidden" name="cancel_return" value="'.base_url('cancel').'">';
$message .='<input type="hidden" name="notify_url" value="'.base_url('payment/notify').'">';
//$message .='<p><input type="submit" name="pp_submit" value="Pay Now">';
$message.='</form>';
echo $message; 

?>
<script type="text/javascript">
    $(document).ready(function(){
     $("#frm1").submit();
    })
</script>
<?php } else { ?>
<div id="loading_view">
  <div class="loadingbox">
    <img src="<?=base_url('skin/front/images/warning.png')?>"> 
    <div style="height: 10px"></div>
  <div style="color: #fff; text-align: center;"><?=$this->lang->line('UNABLE_PROCESS_MESSAGE'); ?></div>
    <div style="height:50px"></div>
    <div class="acceptreq">
    <button onclick="goBack()">Go Back</button>
    </div>
  </div>
</div>
<?php } ?>
<script>
function goBack() {
    window.history.back();
}
</script>
</body>
</html>