<?php $userRole=getLoginUserRole(); ?>
<div class="row ser-rep">
    <?php if(!empty($bookingData)){ ?>
    <div class="col-md-8">
      <div class="payCcontainer">
      <?php
      $status = !empty($bookingData->status)?$bookingData->status:0;
      echo getServiceBookingStatusHeading($status);
      $currency_code = !empty($bookingData->code)?$bookingData->code:"";
      ?>
      <div class="serv-view row">      
        <div class="col-md-4 padd-rght">
          <div class="serv-detail">      
            <strong><?=$this->lang->line('TYPE_OF_SERVICE');?></strong>
            <p><?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?></p> 
            <ul>
            <?php $otherServices=isset($bookingData->other_services)?$bookingData->other_services:""; 
              if($otherServices!=0){
              $otherData=explode(',', $otherServices);
              $count=count($otherData);              
              foreach ($otherData as $key => $value) { ?>                
                <li><?=getOtherServicesName($value)?></li>
              <?php } } ?>
              </ul>
          </div>        
        </div>      
        <div class="col-md-8 padd-rght">
          <div class="serv-detail">     
          <strong><?=$this->lang->line('ADDRESS');?></strong>
            <p><?=getServicesAddress(isset($bookingData->address_id)?$bookingData->address_id:"")?></p>  
          </div>        
        </div>             
      </div>         
      <div class="tech-user">        
      <i class="fas fa-user-circle"></i>Your <?=getServicesName(isset($bookingData->services)?$bookingData->services:"")?> technician will be assigned <strong>1 hour</strong> before the scheduled time        
      </div>      
      <div class="row">
        <div class="col-sm-8">
            <div class="time_wrapper">
              <small><?=$this->lang->line('SERVICE_TIME');?></small>
              <time><?=date("j M, Y h:i A", strtotime(isset($bookingData->booking_datetime)?$bookingData->booking_datetime:"")); ?></time>  
            </div>      
        </div>
      
      <div class="col-sm-4">
      <?php
      if(empty($status) || $status<2)
      {
      ?>
      <div class="time_sch" id="time_sch"><button id="rescheduleMe"><?=$this->lang->line('RESCHEDULE');?></button></div>
      <?php
      }
      ?>
      </div>
      
      <div class="col-sm-12">
        <div id="rescheduleBox" class="text-right" style="display: none"> 
          <form method="post" action="<?=base_url('reschedule/'.$this->uri->segment(2))?>"> 
            <div class="row">        
              <div class="col-sm-4"><?=$this->lang->line('CHOOSE_DATE');?></div>
              <div class="col-sm-8">
                <div class="rescheduleMe">
                  <div id="datpicker" class="dtp_main"><span><?=date("Y-m-d")?></span><i class="fa fa-calendar ico-size"></i><span><?=date("H:i")?></span><i class="fa fa-clock-o ico-size"></i></div>
                  <div id="picker"></div>
                  <input type="hidden" name="reschedule_date" id="reschedule_date" value="" />
                </div>
              </div>       
              
              <div class="col-sm-4"><?=$this->lang->line('REASON');?></div>
              <div class="col-sm-8"><input name="resion" type="text"></div>
              <div class="col-sm-12"><input type="submit" name="submit" class="btn common-btn" id="submit" value="<?=$this->lang->line('SUBMIT');?>"></div>
            </div>
          </form>
        </div>
       </div>
       </div>
      </div>
      <?php
      if(!empty($bookingData->vendor_id))
      {
        ?>
        <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('SERVICE_PROVIDER_DETAILS');?></h4>  
          <?php $sp_info = getServiceProviderDetailsById($bookingData->vendor_id); 
          //echo "<pre>"; print_r($sp_info); echo "</pre>"; ?>
            <div class="row">
                <div class="col-sm-12"> 
                  <p><strong><?=$this->lang->line('NAME');?> :</strong> <?=!empty($sp_info->username)?$sp_info->username:''; ?></p>    
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('EMAIL');?> : </strong><?=!empty($sp_info->email)?$sp_info->email:''; ?></p>
                </div>
                <div class="col-sm-6"> 
                  <p><strong><?=$this->lang->line('CONTACT');?> : </strong><?=!empty($sp_info->mobile)?$sp_info->mobile:''; ?></p>
                </div>  
            </div>

            <?php if(!empty($sp_info->business) && $sp_info->business=='yes'){ ?>
            <div class="row">
                <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_NAME');?> : </strong> <?=!empty($sp_info->business_name)?$sp_info->business_name:''; ?></p></div>
                <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_TYPE');?> : </strong> <?=!empty($sp_info->business_type)?$sp_info->business_type:''; ?></p></div>
            </div>
          <?php } ?>

          <?php if(!empty($sp_info->license) && $sp_info->license=='yes'){ ?>
            <div class="row">
                <div class="col-sm-6"><p><strong><?=$this->lang->line('LICENSE_DETAILS');?> : </strong> <?=!empty($sp_info->license_details)?$sp_info->license_details:''; ?></p></div>
            </div>
          <?php } ?>
            <div class="row">
                <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_EMAIL');?> : </strong> <?=!empty($sp_info->business_email)?$sp_info->business_email:''; ?></p></div>
                <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_TEL_NO');?> : </strong> <?=!empty($sp_info->business_mobile)?$sp_info->business_mobile:''; ?></p></div>                
            </div>

            <div class="row">
                <div class="col-sm-6"><p><strong><?=$this->lang->line('BUSINESS_FAX');?> : </strong> <?=!empty($sp_info->business_fax)?$sp_info->business_fax:''; ?></p></div>
            </div>            
        </div>
    <?php
    }
    $provider_id = !empty($bookingData->vendor_id)?$bookingData->vendor_id:0;
    $operator_id = !empty($bookingData->operator_id)?$bookingData->operator_id:0;
    if(!empty($provider_id) && !empty($operator_id))
    {
      ?>
      <br>
        <div class="price_detail2 clearfix">
          <h4><?=$this->lang->line('ASSIGNED_OPERATOR');?></h4>    
          <div class="row">
              <div class="col-sm-12"> 
                <p><strong><?=$this->lang->line('NAME');?> :</strong> <?=getCustomerName($operator_id); ?></p>    
              </div>  
          </div>
          <div class="row">
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('EMAIL');?> :</strong> <?=getCustomerEmail($operator_id); ?></p>    
              </div>
              <div class="col-sm-6"> 
                <p><strong><?=$this->lang->line('CONTACT');?> : </strong><?=getCustomerContact($operator_id); ?></p>
              </div>   
          </div>                 
        </div>
        <br>
      <?php
    }
    ?>

    <?php
    if(!empty($is_service_started) && !empty($is_service_ended) && !empty($is_valid_user))
    {
    ?>
    <div class="price_detail2 clearfix">
      <h4><?=$this->lang->line('SERVICE_SUMMARY');?></h4>    
        <div class="row">
            <div class="col-sm-6"> 
              <p><strong><?=$this->lang->line('STARTED_TIME');?> :</strong> <?=date('d M, Y h:i A',strtotime($service_start_time)); ?></p>    
            </div>
            <div class="col-sm-6"> 
              <p><strong><?=$this->lang->line('ENDED_TIME');?> : </strong><?=date('d M, Y h:i A',strtotime($service_end_time)); ?></p>
            </div>   
        </div>
        <div class="row">
          <div class="col-sm-6"><p><strong><?=$this->lang->line('TIME_SPENT');?> : </strong> <?=$time_spent; ?></p></div>
          <div class="col-sm-6"><p><strong><?=$this->lang->line('ALLOTTED_TIME');?> : </strong> <?=$service_allocated; ?></p></div>
        </div>
        <?php
        if(!empty($additional_time) && $additional_time>0)
        {
          ?>
          <div class="row">
            <div class="col-sm-6"> 
              <p><strong><?=$this->lang->line('ADDITIONAL_TIME');?> :</strong> <?=$additional_time; ?></p>    
            </div>
            <div class="col-sm-6"> 
              <p><strong><?=$this->lang->line('ADDITIONAL_AMOUNT');?> : </strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>
            </div>             
          </div>
          <?php
        }
        if(!empty($additional_amount) && !empty($operation_row['transaction_id']))
        {
          ?>
          <div class="row">
            <div class="col-sm-6"><strong><?=$this->lang->line('PAYMENT_STATUS');?> : </strong>Paid</div>
            <div class="col-sm-6"><strong><?=$this->lang->line('PAID_VIA');?> : </strong>Paypal</div>
          </div>
          <?php
        }
        if(!empty($additional_amount) && empty($operation_row['transaction_id']))
        {
          ?>
          <div class="row">
            <div class="col-sm-12 text-center"><a href="<?=base_url('pay-outstanding').'/'.$code; ?>" class="btn btn-success"> <?=$this->lang->line('PAY');?> <?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></a></div>
          </div>
          <?php
        }
      ?>
    </div>
    <?php
    }
    ?>

    
    </div>
   
    <div class="col-md-4">
    
    <div class="price_detail2">
      <h4><?=$this->lang->line('PAYMENT_SUMMARY');?></h4>    
      <div class="row">
          <?php
            $amount = !empty($bookingData->amount)?$bookingData->amount:0;
            $sub_total = !empty($bookingData->total_amount)?$bookingData->total_amount:'';
            if(empty($sub_total))
            {
                $sub_total = !empty($bookingData->amount)?$bookingData->amount:0;
            }
            $currency_code = !empty($bookingData->code)?$bookingData->code:"";
            $tax = !empty($bookingData->tax)?$bookingData->tax:"";
            $tax_name = !empty($bookingData->tax_name)?$bookingData->tax_name:"";
            $tax_amount = !empty($bookingData->tax_amount)?$bookingData->tax_amount:"";
          ?>
    
          <div class="col-sm-8">   
              <p><?=$this->lang->line('SERVICE_CHARGES');?></p>   
          </div>    
          <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$amount)?></p>    
          </div>   
      </div>  
      <?php
      if(!empty($tax_amount))
      {
        ?>
        <div class="row total">
          <div class="col-sm-8 "> 
            <p><?=$this->lang->line('TAX');?> (<?=$tax_name;?> - <?=$tax.'%';?>)</p>   
          </div>
          
          <div class="col-sm-4 text-right"> 
            <p><?=getFormatedPriceByCode($currency_code,$tax_amount)?></p>    
          </div>   
        </div> 
        <?php
      }
      ?>    
      <div class="row total">
        <div class="col-sm-8 "> 
          <p><?=$this->lang->line('SUB_TOTAL');?></p>   
        </div>
        
        <div class="col-sm-4 text-right"> 
          <p><?=getFormatedPriceByCode($currency_code,$sub_total)?></p>    
        </div>   
      </div> 
      
      <div class="row paid">
        <div class="col-sm-7"> 
          <p><?=$this->lang->line('AMOUNT_TO_BE_PAID');?></p>
        </div>
      
        <div class="col-sm-5 text-right"> 
          <p><strong><?=getFormatedPriceByCode($currency_code,$sub_total)?></strong></p>    
        </div>   
      </div> 
    </div>
    <?php
      if(!empty($additional_time) && $additional_time>0)
      {
        ?>
        <br>
      <div class="price_detail2">
        <h4><?=$this->lang->line('ADDITIONAL_PAYMENT_SUMMARY');?></h4>
        <div class="row">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SERVICE_CHARGES');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_amount)?></p>    
          </div>
        </div>
        <?php
        if(!empty($additional_tax))
        {
          ?>
          <div class="row total">
            <div class="col-sm-8 "> 
              <p><?=$this->lang->line('TAX');?> (<?=$tax_name;?> - <?=$tax.'%';?>)</p>   
            </div>
            
            <div class="col-sm-4 text-right"> 
              <p><?=getFormatedPriceByCode($currency_code,$additional_tax)?></p>    
            </div>   
          </div> 
          <?php
        }
        ?>
        <div class="row total">
          <div class="col-sm-8">
            <p><?=$this->lang->line('SUB_TOTAL');?></p>
          </div>
          <div class="col-sm-4 text-right">
            <p><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></p>    
          </div>
        </div>
        <div class="row paid">
          <div class="col-sm-7"> 
            <p><?=$this->lang->line('AMOUNT_TO_BE_PAID');?></p>   
          </div>    
          <div class="col-sm-5 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$additional_subtotal)?></strong></p>    
          </div>   
        </div>
      </div>

      <?php $total_paid_amt = $sub_total + $additional_subtotal; ?>
      <br>
      <div class="price_detail2">
       <div class="row grandtotal">
          <div class="col-sm-8"> 
            <p><strong><?=$this->lang->line('GRAND_TOTAL');?> : </strong></p>   
          </div>    
          <div class="col-sm-4 text-right"> 
            <p><strong><?=getFormatedPriceByCode($currency_code,$total_paid_amt)?></strong></p>    
          </div>   
        </div>
      </div>

        <?php
      }
      ?>
    <?php } ?>
   </div>
  </div>

  <a title="<?=$this->lang->line('BACK');?>" class="cmst btn btn-secondary" href="<?=base_url('my-bookings') ?>" ><?=$this->lang->line('BACK');?></a>