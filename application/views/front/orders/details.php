<div class="row ser-rep">
   
    <div class="col-md-8">
      <div class="payCcontainer"> 
      
      <h2><i class="fa fa-check-circle"></i> Booking Accepted</h2>
      
     
      <div class="serv-view row">
      
      <div class="col-md-4 padd-rght">
      <div class="serv-detail">
     
      <strong>Type of Service</strong>

      <p>Tap Servicing</p>	
      </div>	     	
      </div>
      
      <div class="col-md-4 padd-rght">
      <div class="serv-detail">
     
      <strong>Plumber Service</strong>
              <p>Tap Servicing</p>	
      </div>	     	
      </div>
      
      
      <div class="col-md-4 padd-rght">
      <div class="serv-detail">
     
      <strong>Address</strong>
              <p>A -62, Noida, Gautam Buddh Nagar, Uttar Pradesh, India, Landmark : NA	</p>
      </div>	     	
      </div>
      
      				
      </div>
       
        
         
          
       <div class="tech-user">
      	
      <i class="fas fa-user-circle"></i>Your AC technician will be assigned <strong>1 hour</strong> before the scheduled time
      	
      </div>
      
      <div class="row">
      <div class="col-md-8">
      
      <div class="time_wrapper">
      <small>Professional will arrive between</small>
      <time>1:00 pm - 2:00 pm on Sat, 1st Dec</time>	
      	
      </div>
      
	  </div>
      
      <div class="col-md-4">
	  <div class="time_sch">
	  
	  <button>Reschedule</button>	
	  	
	  </div>
      
      
	  </div>
       </div>
      
      </div>
    </div>
   
    <div class="col-md-4">
<div class="price_detail2"> 
    
    
    <h4>Payment Summary</h4>
    
     <div class="row">
     
     
  
    <div class="col-sm-8"> 
   
    <p>Service charge</p>   
    </div>
    
    <div class="col-sm-4 text-right"> 
    <p>Rs. 349</p>    
		 </div>   
    </div>  
    

    
    
    <div class="row total">
    <div class="col-sm-8 "> 
    <p>SubTotal</p>
   
    </div>
    
    <div class="col-sm-4 text-right"> 
    <p>Rs. 858</p>    
		 </div>   
    </div> 
    
        <div class="row paid">
    <div class="col-sm-8"> 
    <p>Amount to be paid</p>
   
    </div>
    
    <div class="col-sm-4 text-right"> 
    <p><strong>Rs. 858</strong></p>    
		 </div>   
    </div> 
    
    
    
    
    </div>
    </div>
   
  </div>