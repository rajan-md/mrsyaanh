<section class="innerBlock">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        
          <?php if(!empty($pageContent))   {
           extract($pageContent); ?>
          <h2><?=$name ?></h2>
          <?=$description ?>
          <?php } else { ?>
           <h2>Page Not Found!</h2>
          <?php  } ?>
        </div>
       
      </div>
    </div>
</section>